trigger ExceptionLogEventTrigger on Exception_Log_Event__e (after insert)
{  
    List<Exception_Log__c> logList = new List<Exception_Log__c>();
    Messaging.SingleEmailMessage message;
    List <string> exceptionEmailIds;
    List <Messaging.SingleEmailMessage> msgList=new List <Messaging.SingleEmailMessage>();
    
    Exception_Log__c log;
    for(Exception_Log_Event__e singleExceptionlogevent : trigger.new)
    {
        log = new Exception_Log__c();
        log.Exception_DateTime__c= singleExceptionlogevent.Exception_DateTime__c;
        log.Exception_Message__c= singleExceptionlogevent.Exception_Message__c;
        log.Exception_Source__c= singleExceptionlogevent.Exception_Source__c;
        log.Exception_Type__c= singleExceptionlogevent.Exception_Type__c;
        log.Logged_User__c=singleExceptionlogevent.Logged_User__c;
        log.New_Record__c=singleExceptionlogevent.New_Record__c;
        log.Old_Record__c=singleExceptionlogevent.Old_Record__c;
        log.Notify_Admin__c=singleExceptionlogevent.Notify_Admin__c;
        
        if(log.Notify_Admin__c){
           
           message = new Messaging.SingleEmailMessage();
        // exceptionEmailIds = system.label.Exception_Notification_EmailIds.split(';');
         exceptionEmailIds = 'sauravsundriyal@gmail.com'.split(';');

        message.toAddresses = exceptionEmailIds;
        message.subject = 'Error Processing Records in '+log.Exception_Source__c;
        message.plainTextBody = 'Please contact your system Administrator  \n'+log.exception_source__c;
        msgList.add(message);
        }
        
        logList.add(log);
    }
    
    if(logList.size()>0){
      insert logList;
    }
    
    if(msgList.size()>0 && !test.isrunningtest()){
      Messaging.SendEmailResult[] results = Messaging.sendEmail(msgList);
    }
    
}