/*
Testclass: Account_Trigger_test
*/

trigger Contact_trigger on Contact(before insert, before update, after insert) {

    App_settings__c appsett = App_settings__c.getInstance(userInfo.getUserId());
   try{
    if (!appsett.Disable_Contact_Trigger__c) {
        if (UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger == 0) {
            

            if (trigger.isInsert) {

                if (trigger.isBefore) {

                    Contact_Trigger_Handler.handleBeforeInsert(trigger.new);

                }
                
                if (trigger.isAfter) {
                    UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
                    Contact_Trigger_Handler.handleAfterInsert(trigger.newMap);

                }

            }

            if (trigger.isUpdate) {

                if (trigger.isBefore) {
                   UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
                   Contact_Trigger_Handler.handleBeforeUpdate(trigger.newMap, trigger.oldMap);

                }

            }


        }

    }
  }
  Catch(Exception e){
       FSMLogger.LogErrorMessage('Contact_trigger',e,trigger.new,trigger.old,trigger.operationType+'');
  }
}