trigger ProcessFSMLogEvent on FSMLogEvent__e(after insert) {
    List <FSMLog__c> logList = new List <FSMLog__c> ();
    Messaging.SingleEmailMessage message;
    List <string> exceptionEmailIds;
    List <Messaging.SingleEmailMessage> msgList = new List <Messaging.SingleEmailMessage> ();

    FSMlog__c l;
    for (FSMLogEvent__e singleFSMLogEvent: Trigger.New) {
        l = new FSMlog__c();
        l.FSMLogType__c = singleFSMLogEvent.FSMlogType__c;
        l.Message__c = singleFSMLogEvent.message__c;
        l.Source__c = singleFSMLogEvent.source__c;
        l.FSMLogDateTime__c = singleFSMLogEvent.FSMlogDateTime__c;
        l.Logged_User__c = singleFSMLogEvent.Logged_User__c;
        l.New_Record__c = singleFSMLogEvent.New_Record__c;
        l.Old_Record__c = singleFSMLogEvent.Old_Record__c;
        l.triggerEvent__c = singleFSMLogEvent.triggerEvent__c ;
        l.Notify_Admin__c = singleFSMLogEvent.Notify_Admin__c;
        l.SourceComponent__c = singleFSMLogEvent.SourceComponent__c;
        
        //insert l;


        if (l.Notify_Admin__c) {

            message = new Messaging.SingleEmailMessage();
            // exceptionEmailIds = system.label.Exception_Notification_EmailIds.split(';');
            exceptionEmailIds = 'ayush123rastogi@gmail.com'.split(';');

            message.toAddresses = exceptionEmailIds;
            message.subject = 'Error Processing Records in ' + l.Source__c;
            message.plainTextBody = 'Please contact your system Administrator  \n' + l.Source__c;
            msgList.add(message);
        }
        logList.add(l);
    }
    if (logList.size()> 0) {
        insert logList;
    }

    if (msgList.size()> 0 && !test.isrunningtest()) {
        Messaging.SendEmailResult[] results = Messaging.sendEmail(msgList);
    }
}