/**
@   Name        : InvoiceTrigger
@   Description : Trigger on Invoice__c for DML operations insert & update (both after)
@   Created at :    20/07/2018
*/
trigger InvoiceTrigger on Invoice__c(after insert, after update){
    
       /*if((!System.isBatch()) && (!System.isFuture())){
           InvoiceTriggerHandler handler = new InvoiceTriggerHandler();
            //--Check if Trigger is called after DML operation
            if (Trigger.isAfter) {
                //--Check if Trigger is called after insert operation
                if (Trigger.isInsert) {
                    handler.insertMethod(Trigger.new);
                }
                //--Check if Trigger is called after update operation
                //Commented out for testing ProcessMCSRevenue class didn't need the invoice to continuously sync back to QBO
                //Need to update this trigger to prevent it from firing when an update is made to these fields
                if (Trigger.isUpdate) {
                    handler.updateMethod(Trigger.newMap, Trigger.oldMap);
                }
            }
       }  */
}