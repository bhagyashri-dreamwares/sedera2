/*
Test class : Case_Trigger_Test
*/
trigger Case_Trigger on Case(before insert, before update, after Update, after undelete, before delete, after delete) {

try{
    App_settings__c appsett = App_settings__c.getInstance(userInfo.getUserId());

    if (!appsett.Disable_Case_Trigger__c) {

        if (UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger == 0) {
            if (trigger.Isinsert) {

                if (trigger.isBefore) {

                    Case_Trigger_Handler.handleBeforeInsert(trigger.new);
                    UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 1;
                    
                }
            }


            if (trigger.IsUpdate) {
                if (trigger.IsBefore) {
                    Case_Trigger_Handler.handleBeforeUpdate(trigger.newMap, trigger.OldMap);
                }

                if (trigger.isafter) {
                    Case_Trigger_Handler.handleAfterUpdate(trigger.newMap, trigger.OldMap);
                }

            }




            if (trigger.isUndelete) {

                if (trigger.isAfter) {
                    Case_Trigger_Handler.handleAfterUndelete(trigger.newMap);

                }

            }


            if (trigger.isdelete) {

                if (trigger.isBefore) {
                    Case_Trigger_Handler.handleBeforedelete(trigger.OldMap);

                }
                if (trigger.Isafter) {
                    Case_Trigger_Handler.handleAfterdelete(trigger.OldMap);

                }

            }


            if (trigger.isAfter && !trigger.isUndelete) {
                UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 1;
            }

        }
    }
    
   
    }
    catch(Exception e){
     ExceptionLogger.LogErrorMessage(e,trigger.new,trigger.old);
    }
}