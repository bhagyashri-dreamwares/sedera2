Trigger SDocTrigger on SDOC__SDoc__c (After insert, Before update) {
    
    if(Trigger.isBefore && Trigger.isUpdate){
        
        SDocTriggerHandler.updateFields(Trigger.New, Trigger.isInsert);
    }else if (Trigger.isAfter && Trigger.isInsert){
    
        List<SDOC__SDoc__c> sDocList = SDocTriggerHandler.getSDocList(Trigger.New);
        SDocTriggerHandler.updateFields(sDocList, Trigger.isInsert);
    }
}