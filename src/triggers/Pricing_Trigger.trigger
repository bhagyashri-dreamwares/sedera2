/*
TestClass: Pricing_Trigger_Test
*/
trigger Pricing_Trigger on Pricing__c(after insert, after Update) {

    App_settings__c appsett = App_settings__c.getInstance(userInfo.getUserId());

    if (!appsett.Disable_Pricing_Trigger__c) {

        if (UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger == 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 1;

            if (trigger.isInsert) {

                if (trigger.isAfter) {

                    Pricing_Trigger_Handler.handleAfterInsert(trigger.newMap);

                }

            }

            if (trigger.isUpdate) {

                if (trigger.isAfter) {

                   Pricing_Trigger_Handler.handleAfterUpdate(trigger.newMap, trigger.oldMap);

                }

            }


        }

    }
    
}