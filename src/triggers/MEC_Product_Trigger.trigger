/*
TestClass:Mec_Product_trigger_Test
*/


trigger MEC_Product_Trigger on MEC_Product__c(after Update) {
 
   App_settings__c appsett = App_settings__c.getInstance(userInfo.getUserId());

    if (!appsett.Disable_Mec_Product_Trigger__c) {

        if (UtilityClass_For_Static_Variables.CheckRecursiveForMecProductTrigger == 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForMecProductTrigger = 1;

            if (trigger.isUpdate) {

                if (trigger.isAfter) {

                   Mec_Product_Trigger_Handler.handleAfterUpdate(trigger.newMap, trigger.oldMap);

                }

            }


        }

    }

}