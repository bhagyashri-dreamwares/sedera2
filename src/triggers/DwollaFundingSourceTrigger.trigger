/**
* @TriggerName  : DwollaFundingSourceTrigger
* @Purpose      : Before Insert: Deactivate all Dwolla_Funding_Source__c record who are linked to Account.
* @Object       : Dwolla_Funding_Source__c
* @FireOn       : before insert
* @CreatedDate  : 21/09/2018
* @ModifiedDate : 21/09/2018
*/


trigger DwollaFundingSourceTrigger on Dwolla_Funding_Source__c (before insert, before update, after insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            //Commented out was causing issues with verified customer data coming back from Dwolla
            //DwollaFundingSourceTriggerHandler.deactivateCurrentActive(Trigger.new);
        }else if(Trigger.isUpdate){            
            FundingSourceTriggerHandler.createFundindingSource(Trigger.New);    
        }
    }else{        
        if(Trigger.isInsert){
            FundingSourceTriggerHandler.createFundindingSource(Trigger.New);
        }    
    }
    
    
}