/*
@
*/
trigger AccountTrigger on Account (after insert,after update) {
   
   if(UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB==0){
        if((!System.isBatch()) && (!System.isFuture()) && !System.isQueueable()){
        AccountTriggerHandler handler = new AccountTriggerHandler();
        
        //--Check if Trigger is called after DML operation
        if (Trigger.isAfter) {
            //--Check if Trigger is called after insert operation
            if(Trigger.isInsert) {
                handler.insertMethod(Trigger.new);
                
            }
            //--Check if Trigger is called after update operation
            if(Trigger.isUpdate) {
                handler.updateMethod(Trigger.newMap, Trigger.oldMap);
            }
        }
    }else{
        if(Trigger.isUpdate && CheckRecursive.runOnce() && !System.isQueueable()) {
            AccountTriggerHandler handler = new AccountTriggerHandler();
            handler.updateQBHierarchy(Trigger.newMap, Trigger.oldMap);
        }
    }
    }
}