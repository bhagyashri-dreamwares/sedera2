/*
Test class : Account_trigger_Test
*/

trigger Account_trigger on Account(before insert, after insert, before update, after update, before delete, after undelete) 
{
    try{
       App_settings__c appsett = App_settings__c.getInstance(userInfo.getUserId());

        if ( !appsett.Disable_Account_Trigger__c) {
            If(UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger == 0) {

                if (trigger.isInsert) {


                    if (trigger.isBefore) {
                     Account_trigger_handler.handleBeforeInsert(trigger.new);                                                    
                    }

                    if (trigger.isAfter) {
                     Account_trigger_handler.handleAfterInsert(trigger.newMap);                                                    
                    }


                }



                if (trigger.isUpdate) {


                    if (trigger.isbefore) {
                     Account_trigger_handler.handleBeforeUpdate(trigger.newMap,trigger.oldMap);                                                    
                    }

                    if (trigger.isAfter) {
                     Account_trigger_handler.handleAfterUpdate(trigger.newMap,trigger.oldMap);                                                    
                    }


                }


                if (trigger.isDelete) {

                    if (trigger.isBefore) {
                     Account_trigger_handler.handleBeforeDelete(trigger.oldMap);                                                                            
                    }

                }


                if (trigger.isUnDelete) {

                    if (trigger.isAfter) {
                      Account_trigger_handler.handleAfterUnDelete(trigger.newMap);                                                                            
                    }

                }


                if (trigger.isAfter) {

                    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;

                }

            }


            /*if (UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB == 0 && ((trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) || (trigger.isBefore && trigger.isBefore))) {
 
                    if ((!System.isBatch()) && (!System.isFuture()) && !System.isQueueable()) {
                        AccountTriggerHandler handler = new AccountTriggerHandler();

                        //--Check if Trigger is called after DML operation
                        if (Trigger.isAfter) {
                            //--Check if Trigger is called after insert operation
                            if (Trigger.isInsert) {
                                handler.insertMethod(Trigger.new);
                                DwollaAccountTriggerHandler.createDwollaCustomer(Trigger.new, Trigger.isUpdate, Trigger.oldMap);
                            }
                            //--Check if Trigger is called after update operation
                            if (Trigger.isUpdate) {
                                handler.updateMethod(Trigger.newMap, Trigger.oldMap);

                            }
                        }
                    } else {
                        if (Trigger.isUpdate && CheckRecursive.runOnce() && !System.isQueueable()) {
                            AccountTriggerHandler handler = new AccountTriggerHandler();
                            //handler.updateQBHierarchy(Trigger.newMap, Trigger.oldMap);
                        }
                    }

                    System.debug('Trigger.isBefore :::' + Trigger.isBefore);
                    System.debug('Trigger.isUpdate :::' + Trigger.isUpdate);

                    if (Trigger.isBefore && Trigger.isUpdate) {
         
                        DwollaAccountTriggerHandler.createDwollaCustomer(Trigger.new, Trigger.isUpdate, Trigger.oldMap);
                        //DwollaAccountTriggerHandler.sendPlidLinkToCustomer(Trigger.new, Trigger.isUpdate,Trigger.oldMap);
                    }

                    if (DwollaAccountTriggerHandler.isFirstTime) {
                        DwollaAccountTriggerHandler.isFirstTime = false;
                        if (Trigger.isBefore && Trigger.isUpdate) {
                            DwollaAccountTriggerHandler.sendPlidLinkToCustomer(Trigger.new, Trigger.isUpdate, Trigger.oldMap);
                        }
                    }
                }*/

            }
            
        }
        catch(Exception e){
        FSMLogger.LogErrorMessage('Account_trigger',e,trigger.new,trigger.old,trigger.operationType+'');
     } 

   }