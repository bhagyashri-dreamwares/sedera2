trigger Case_Trigger2 on Case (before insert,before update,after Update,after undelete,before delete,after delete){

if(system.label.Case_Trigger_Switch=='true')
{

if(UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger==0)
{
 if(trigger.Isinsert)
 {
  Case_Helper_Class2.Setting_IUA_Attributes_On_Before_Insert(trigger.new);
  Case_Helper_Class2.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
  UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger++;
 }
 
 
 if(trigger.IsUpdate)
 {  
   if(trigger.IsBefore)
   {
   Case_Helper_Class2.Setting_IUA_attributes_On_Before_Update(trigger.new,trigger.oldMap);
   }
   
   if(trigger.isafter)
   {
   Case_Helper_Class2.Setting_IUA_attributes_On_After_Events();
   Case_Helper_Class2.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
   Case_Helper_Class2.Setting_Bills_On_Change_Of_IUA(trigger.oldmap,trigger.newmap.keyset());
   UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger++;
   }
 
 }




if(trigger.isUndelete)
{
  Case_Helper_Class2.Setting_IUA_Attributes_for_Undelete(trigger.new);
  Case_Helper_Class2.Setting_IUA_attributes_On_After_Events();
  Case_Helper_Class2.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
  UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger++;
}




if(trigger.isdelete)
{

  if(trigger.isBefore)
  {
     Case_Helper_Class2.Setting_IUA_attributes_On_Before_Delete(trigger.old); 
  }
  if(trigger.Isafter)
  {
   Case_Helper_Class2.Setting_IUA_attributes_On_After_Events();
   Case_Helper_Class2.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
   UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger++;
  }

}
}

}

}