/*
Test class : Case_Trigger_Test
*/
trigger Bill_trigger on bill__c(Before insert) {

    App_settings__c appsett = App_settings__c.getInstance(userInfo.getUserId());

    if (!appsett.Disable_Bill_Trigger__c) {

        if (UtilityClass_For_Static_Variables.CheckRecursiveForBillTrigger == 0) {
         UtilityClass_For_Static_Variables.CheckRecursiveForBillTrigger = 1;   

            if (trigger.isInsert) {

                if (trigger.isBefore) {
                    Bill_Trigger_Handler.handleBeforeInsert(trigger.new);
                    
                }

            }
        }

    }

}