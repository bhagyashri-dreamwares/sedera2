trigger BillIUA on bill__c(after insert, after update) {

    //callingtriggeronce.once so that this trigger does not run again when update is made from inside of its body

    if (UtilityClass_For_Static_Variables.CheckRecursiveForBillTrigger == 0) {
        UtilityClass_For_Static_Variables.CheckRecursiveForBillTrigger = 1;
        set <id> caseids = new set <id> ();

        //collect all the ids of cases associated to different bills

        for (bill__c b: trigger.new) {
            if (trigger.isinsert && B.approval_status__c == 'APPROVED')
                caseids.add(b.case__C);
            else if (trigger.isupdate) {
                if (b.Total_Patient_Payable_Amount__c != trigger.oldmap.get(b.id).Total_Patient_Payable_Amount__c)
                    caseids.add(b.case__C);
                if (b.approval_status__c != trigger.oldmap.get(b.id).approval_status__c && (b.approval_status__c == 'APPROVED' || trigger.oldmap.get(b.id).approval_status__c == 'APPROVED'))
                    caseids.add(b.case__C);
                if (UtilityClass_For_Static_Variables.changeinIUA == 1) {
                    caseids.add(b.case__C);
                }

            }
        }
        if (UtilityClass_For_Static_Variables.changeinIUA == 1)
            UtilityClass_For_Static_Variables.changeinIUA = 0;
        /***
        SOQL query to get all the concerned cases along with their bills,

        We need all the bills associated to a case even if its only bill is updated/inserted because there needs to be an update, if condition matches in each bill connected to the parent case.

        This is also the reason a SOQL Query is created on cases after getting them from updated/inserted bills
        ***/


        list <case> Cases = [select Initial_Unsharable_Amount__c, (select Total_Patient_Payable_Amount__c, case__c, Payable_to_member__c from bills__r where approval_status__c = 'APPROVED' order by createddate asc) from case where id in :caseids];



        /****
        Logic:

        Suppose a case has 4 bills and we update the 3rd bill such that sum of "Total_Patient_Payable_Amount__c,case__c" on bills 

        First case:

        (i)Crosses initial unsharable amount on case then the bill which sailed it through(could be three or four) will have a difference of (sum of Total_Patient_Payable_Amount__c till this bill -IUA on case) in payable to member.

        (ii)All the Bills before above bill will have null in payable to member.

        (iii)All the bills after bill in first point will have payable to member equal to Total_Patient_Payable_Amount__c.

        Second Case:

        (i)sum of Total_Patient_Payable_Amount__c on all bills Drops below Initial_Unsharable_Amount__c.

        (ii)All the bills will have payable to member equal to null.

        Same will happen for Insert 

        I have looped through all the bills of a case, checked them keeping in mind the above conditions, collected them in a list and then made an update to all bills.
        ****/



        List <bill__c> mmm = new list <bill__c> ();
        for (case loopingcase: cases) {
            integer checkonebill = 0;
            decimal k = 0;
            integer xyz = 0;
            for (bill__c z: loopingcase.bills__r) {

                if (z.Total_Patient_Payable_Amount__c == null) {
                    k = k + 0;
                } else {
                    k = k + z.Total_Patient_Payable_Amount__c;
                }

                if (xyz == 1) {
                    z.Payable_to_member__c = z.Total_Patient_Payable_Amount__c;
                } else if (k>= loopingcase.Initial_Unsharable_Amount__c && xyz == 0) {
                    z.Payable_to_member__c = k - loopingcase.Initial_Unsharable_Amount__c;
                    xyz = 1;
                } else {
                    z.Payable_to_member__c = null;
                }
                mmm.add(z);
            }
        }


        update mmm;


    }

}