/**  
 * @Purpose      : Test Class for InvoiceDetailController
 * @Author       : Dreamwares 
 * @Created Date : 20/08/2018
 */
@isTest
public class InvoiceDetailControllerTest {
    
    /**
     * @Purpose : Method to create test data
     */
    @TestSetup
    public static void createTestData(){
        /*Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account employerAccount = new Account(Name = 'Sofsyst', RecordtypeId = recordtypeId, Phone = '9898989890',
                                              Default_Product__c = 'Sedera Access', Available_IUA_Options__c = '1000',
                                              MS_Pricing__c = 'New Pricing');
        insert employerAccount;
        
        recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();       
        
        Contact contact = new Contact(FirstName = 'Test', LastName = 'Sample', AccountId = employerAccount.Id, RecordtypeId = recordtypeId);
        insert contact;
        
        List<Account> memberAccountList = new List<Account>();
        
        recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account account = new Account(Name = 'Test1', RecordtypeId = recordtypeId, Phone = '9898989890', Account_Employer_Name__c = employerAccount.Id);
        memberAccountList.add(account);
            
        recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        
        account = new Account(Name='Test2', RecordtypeId = recordtypeId, First_Name__c = 'Ali', Last_Name__c = 'Jorg', Phone = '9898989890', 
                              Account_Employer_Name__c = employerAccount.Id, Subscription_Status__c = 'Active');
        memberAccountList.add(account);
                        
        Account account1 = new Account(Name='Test3', RecordtypeId = recordtypeId, First_Name__c = 'Jorge', Last_Name__c = 'Bey', Phone = '8087869012', 
                              		   Account_Employer_Name__c = employerAccount.Id, Subscription_Status__c = 'Pending Start Date');
        memberAccountList.add(account1);
                
        recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Plan').getRecordTypeId();
        
        account = new Account(Name = 'Test Account1', RecordtypeId = recordtypeId, Phone = '8888989890');
        memberAccountList.add(account);
            
        insert memberAccountList;
        
        Invoice__c invoice1 = new Invoice__c(Account__c = employerAccount.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 25000);
        insert invoice1;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        
        Invoice__c invoice = new Invoice__c(Account__c = account1.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 20000, Invoice_Number__c = 'I1111', Submission_Status__c = 'Approved');
        invoiceList.add(invoice);   
        
        invoice = new Invoice__c(Account__c = account1.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(50),
                                 Paid_Amount__c = 20000, Invoice_Number__c = 'I1122');
        invoiceList.add(invoice); 
        
        insert invoiceList;    
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus');
        insert product;
        
        Asset sederaPrduct = new Asset(Name = 'Test Asset', Account = account1, Product2 = product, Employer_Account__c = employerAccount.Id);
        insert sederaPrduct;
        System.debug('s AccountId::'+sederaPrduct.AccountId);
        Sedera_Product_Report__c sederaProduct1 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'Test', Member_Account__c = account1.Id,
                                                                             Invoice__c = invoice1.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id);
        insert sederaProduct1;
        
        Sedera_Product_Report__c sederaProduct2 = new Sedera_Product_Report__c(Amount__c = 500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'Test', Member_Account__c = account1.Id,
                                                                             Invoice__c = invoice1.Id, Primary_Age__c = 28, 
                                                                             Subscription_Status__c = 'Active', Sedera_Product__c = sederaPrduct.Id);
        insert sederaProduct2;*/
        
        //UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 1;
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
        AccessPricing[0].name='Default Access Pricing';
        AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        insert AccessPricing[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 0;
        
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass1);
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        emp.name = 'Test';
        update emp;


        List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(1, false);
        for (integer i = 0; i < MemberAccountList.size(); i++) {
            MemberAccountList[i].Account_Employer_name__C = emp.id;
            MemberAccountList[i].subscription_status__c='Active';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        
        insert MemberAccountList;
        
        Invoice__c invoice = new Invoice__c(Account__c = emp.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 20000, Invoice_Number__c = 'I1111', Submission_Status__c = 'Approved');
        insert invoice;
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus');
        insert product;
        
        Asset sederaPrduct = new Asset(Name = 'Test Asset', Account = MemberAccountList[0], Product2 = product, Employer_Account__c = emp.Id);
        insert sederaPrduct;
        System.debug('s AccountId::'+sederaPrduct.AccountId);
        Sedera_Product_Report__c sederaProduct1 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'Test', Member_Account__c = emp.Id,
                                                                             Invoice__c = invoice.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id);
        insert sederaProduct1;
        
        Sedera_Product_Report__c sederaProduct2 = new Sedera_Product_Report__c(Amount__c = 500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'Test', Member_Account__c = emp.Id,
                                                                             Invoice__c = invoice.Id, Primary_Age__c = 28, 
                                                                             Subscription_Status__c = 'Active', Sedera_Product__c = sederaPrduct.Id);
        insert sederaProduct2;
    }
    
    /**
     * @Purpose: Test page Invoice 
     */
    static testMethod void testInvoiceDetailsPage() {
        Account account = [SELECT Id FROM Account WHERE Name = 'Test' LIMIT 1];
        Invoice__C invoice = [SELECT Id FROM Invoice__c WHERE Account__c =: account.Id LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.InvoiceDetail;
        Test.setCurrentPageReference(pageRef);
        Apexpages.currentpage().getparameters().put('Id', String.valueOf(account.Id) + '-' + String.valueOf(invoice.Id));
        InvoiceDetailController controller = new InvoiceDetailController();
        
        Test.stopTest();
        
        System.assertEquals(3000, controller.memberAccountWrapper[0].amount); 
    }
    
    /**
     * @Purpose: Test page 
     */
    static testMethod void testNegCase() {
                
        Test.startTest();
        PageReference pageRef = Page.InvoiceDetail;
        Test.setCurrentPageReference(pageRef);
        InvoiceDetailController controller = new InvoiceDetailController();   
        Boolean isValidId = controller.isValidSalesforceId('0021D00000Imspr10B', Account.class);
        Test.stopTest();
    }
}