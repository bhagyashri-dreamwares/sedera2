/*
Test class: Pricing_Trigger_Test
*/
public class Pricing_Helper_Class {

    public static void MCSprodcalculation(Set <Id> AccEmpIds) {

        list <Account> MembersToUpdate = new list <Account> ();

        for (Account EmpAcc: [Select id, RecordTypeId, (Select MEC_Product__r.Name, MEC_Product__r.Id, MEC_Product__r.Discount_Tier__c from AccountsMECsAssociation__r where Default_MEC_Product__c = true limit 1), (Select id, parent_product__c, subscription_status__c, Account_employer_name__c, teladoc_direct__c, Dependent_status__c, member_discount_tier_manual__c, RecordTypeId, SecondMD__c, Iua_chosen__c, primary_Age__c from Accounts__r) from Account where id in: AccEmpids and(contract_status__c != 'terminated'
                and contract_status__c != 'contract withdrawn')]) {

            for (Account memAcc: EmpAcc.Accounts__r) {
                //system.debug(empacc);
                //memAcc.Member_Discount_Tier_Manual__c = EmpAcc.AccountsMECsAssociation__r[0].MEC_Product__r.Discount_Tier__c;
                MembersToUpdate.add(memAcc);
            }
        }
        if (MembersToUpdate.size()> 0) {
           // There is one SOQL in this method which will pull updated pricing info(After event) 
            Account_Helper_class.MCSproductcalculation(MembersToUpdate, null);
        }

        if (MembersToUpdate.size()> 100) {
            system.debug('Triggering Queueable');
            system.EnqueueJob(new Queueable_UpdateAccountsinbulk(MembersToUpdate, true, true));
        } else if (MembersToUpdate.size()> 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
            Update MembersToUpdate;
        }



    }


   /*** Throw an error if there is no employer linked to the pricing or if there are two pricing for an employer(This rule is not valid for Base price records) ***/
    public static void errorOnTwoPricings(List <pricing__c> priceList) {
       
       Set <Id> AccEmpids = new Set <Id> ();
        
        for (pricing__c pr: priceList) {
            if (pr.Employer_Name__c != null) {
                AccEmpids.add(pr.Employer_Name__c);
            } else if(!pr.is_base_price_record__c){
                Exception e=new NoSuchElementException();
                e.setMessage('No employer exist for some of the pricing records.');   
                throw e;
            }

        }
        for (Account acc: [select id, (select id from pricings__r limit 2) from account where id in: AccEmpids]) {
            if (acc.pricings__r.size()> 1)
                priceList[0].addError('There can never be 2 pricing on one Employer.');

        }

    }

   /*** If any of the concerned field's(controls pricing at member level) value changes, then all the assosciated members are Updated for new Product code and amount ***/   
   /*** Also expecting base price record changes only from UI(or one at a time) and not in bulk with other price records. ***/
    public static void changeInPricingFieldsValue(List <pricing__c> priceList, Map <id, pricing__c> priceOldMap) {

        Set <Id> AccEmpids = new Set <Id> ();
        List<pricing__c> priceOld=priceOldMap.values();
        
        // Map of Base price record name(Key) and corresponding pricing(value) , used in case of change in base records field's value
        Map <string, pricing__c> BprecnameTypeMap = new Map <string, pricing__c> ();
        List <pricing__c> priceUpdate = new List <pricing__c> ();
        Set <string> MCSFieldsChange = new Set <string> ();
        string defaultPricing = '';
        for (pricing__c pr: priceList) {
           //Prevent Employer Change
            if (priceOldMap!= null && (priceOldMap.get(pr.Id).Employer_name__c != null || pr.Employer_name__c == null) && pr.employer_name__c != priceOldMap.get(pr.Id).Employer_name__c) {
                pr.addError('You cannot change Employer');
            }
            
          }

        List <Account> memAccs = new List <Account> ();
        Pricing__c oldRecord;
        Pricing__c newRecord;

        Map <String, Schema.SObjectType> m = Schema.getGlobalDescribe();
        Schema.SObjectType s = m.get('Pricing__c');
        Schema.DescribeSObjectResult r = s.getDescribe();
        Map <String, Schema.SObjectField> fields = r.fields.getMap();
        List <String> lstrequiredfields = new List <String> ();
        //Fetching all required/concerned fields as we only want recalculation of codes for change in certain fields of pricing object
        for (String f: fields.keyset()) {
            Schema.DescribeFieldResult describeResult = fields.get(f).getDescribe();
            system.debug(describeResult);
            if (describeResult.isCreateable() && describeResult.isCustom() && describeResult.isUpdateable()) {
                //This is mandatory / required field
                lstrequiredfields.add(f);

            }
        }
        system.debug(lstrequiredfields);
        
        for (Integer index = 0; index <priceList.size(); index++) {
            oldRecord = priceOld[index];
            newRecord = priceList[index];
            for (String field: lstrequiredfields) {
             //Checking change in field values for non base price records
                if ((field.contains('old') || field.contains('tier') || field.contains('iua') || field.startsWith('x') || field.startsWith('mcs')) && (trigger.Isinsert || oldRecord.get(field) != newRecord.get(field)) && !priceList[index].Is_Base_Price_Record__c) {
                    if (newRecord.get('Employer_Name__c') != null)
                        AccEmpids.add(newRecord.get('Employer_Name__c') + '');
                    break;
                }
             //Checking change in field values for non base price records
                if (priceOldMap!= null && priceList[index].Is_Base_Price_Record__c && oldRecord.get(field) != newRecord.get(field)) {
                    defaultPricing = priceList[index].Name.toLowercase();

                    BprecnameTypeMap.put(defaultPricing, priceList[index]);
                    if (field.contains('tier') || field.contains('old') || field.contains('iua') || field.startsWith('x') || field.startsWith('mcs')) {
                        MCSFieldsChange.add(defaultPricing);
                    }
                    defaultPricing = '';
                    break;
                }
            }

        }
        
        // This will only run if concerned field's value changes in non base price record
        // AccEmpIds pulled above is passed to MCSproductcalculation method for processing calculation for all linked members 
        if (AccEmpids.size()> 0)
            MCSprodcalculation(AccEmpids);
        
        // This will only run if concerned field's value changes in base price record
        if (BprecnameTypeMap.size()> 0) {
            AccEmpids.clear();
            Pricing__c prc = new pricing__c();
            //pulling all pricings which are not base price records
            for (pricing__c pc: [Select Name, Default_Pricing_Name__c, Employer_Name__r.Id, Employer_Name__r.name, recordTypeid from pricing__c where Employer_Name__c != null and is_Base_price_record__c = false and Default_Pricing_Name__c in: BprecnameTypeMap.keyset()]) {

                if (MCSFieldsChange.contains(pc.Default_Pricing_Name__c))
                    AccEmpids.add(pc.Employer_Name__r.Id);
                //cloning based on name stored in default price naming field(This is populated at the time of insertion)
                prc = BprecnameTypeMap.get(pc.Default_Pricing_Name__c).clone();
                prc.Employer_name__c = pc.Employer_name__c;
                prc.is_Base_price_record__c = false;
                prc.Id = pc.id;
                prc.name = pc.Employer_Name__r.name + ' Pricing';
                priceUpdate.add(prc);
            }

            Update priceUpdate;
          //AccEmpIds pulled above inside ** if (BprecnameTypeMap.size()> 0) { ** is passed to MCSproductcalculation method for processing calculation for all linked members 

            if (AccEmpids.size()> 0)
                MCSprodcalculation(AccEmpids);

        }

    }

}