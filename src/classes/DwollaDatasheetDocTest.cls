/**
* @Purpose: Test class for DwollaDatasheetDocController 
* @Date : 16/01/2019
*/
@isTest
public class DwollaDatasheetDocTest{

    /**
    * To set initial data
    */
    @testSetup
    public static void initData(){
        
         // Insert Dwolla API Configuration Custom Setting
        DwollaAPIConfiguration__c dwollaAPIConfigurationObj = new DwollaAPIConfiguration__c();
        dwollaAPIConfigurationObj.Access_Token__c = 'MKGrMhOxWxMZt7OWrp98IFFopbQLjHrhd6Zlyj2kqK5w2dRKmO';
        dwollaAPIConfigurationObj.Access_Token_Expiry__c = DateTime.now();
        dwollaAPIConfigurationObj.Authorization_URL__c = 'https://sandbox.dwolla.com/oauth/v2/token';
        dwollaAPIConfigurationObj.Client_Key__c = 'UBUubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        dwollaAPIConfigurationObj.Is_Sandbox__c = true;
        dwollaAPIConfigurationObj.Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        dwollaAPIConfigurationObj.Sandbox_Endpoint_Url__c = 'https://api-sandbox.dwolla.com';
        dwollaAPIConfigurationObj.Secret_Key__c = '7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        
        insert dwollaAPIConfigurationObj;
        
        // insert Account
        Account account = new Account();
        account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        account.Name = 'Test Acc';
        account.Dwolla_ID__c = '23133';
        
        insert account;
        
        // insert DataSheet
        Dwolla_Datasheet__c dataSheet = new Dwolla_Datasheet__c();
        dataSheet.Account__c = account.Id;
        dataSheet.SSN__c = '12365478963';
        dataSheet.EIN__c = '1236547896';
        dataSheet.Controller_SSN__c = '12365478963';
        insert dataSheet;    
    }
    
    /**
    * To test positive functionality
    */ 
    public static testMethod void testPositiveFunctionality(){
        
        Dwolla_Datasheet__c dataSheet = [SELECT Id
                                         FROM Dwolla_Datasheet__c
                                         LIMIT 1];
        
        String parameters = '{"parentId" : "'+dataSheet .Id+'" , "attachmentId" : null , "attachmentBody" : "Test" , "attachmentName" : "Test.png" ,'+
                            '"attachmentType" : "idCard" , "contentType" : "image/png"}';   
        //Test.setMock(HttpCalloutMock.class, new DwollaTrasnferBatchMockRsponse());  
        system.debug('parameters ::'+parameters );

        Test.startTest();
        
        DwollaDatasheetDocController.Response  response = DwollaDatasheetDocController.uploadAttachment(parameters);
        
        String parameters2 = '{"parentId" : "'+response.recordId+'" , "attachmentId" : "'+response.data+'"}';
        DwollaDatasheetDocController.isPostiveTesting = true;
        DwollaDatasheetDocController.isTestValidToken = true;
        DwollaDatasheetDocController.Response  response2 = DwollaDatasheetDocController.createDwollaDocument(parameters2);
        
        Test.stopTest();
        
        List<DwollaDatasheetAttachment__c> dataSheetAttList = [SELECT Id, Dwolla_Document_Id__c
                                                               FROM DwollaDatasheetAttachment__c]; 
        
        System.assertEquals(true, response.isSuccess);
        System.assertEquals(true,response2.isSuccess);
        System.assertEquals('11fe0bab-39bd-42ee-bb39-275afcc050d0',dataSheetAttList[0].Dwolla_Document_Id__c);
    }
    
    /**
    * To test negative functionality
    */
    public static testMethod void testNegativeFunctionality(){
        
        Dwolla_Datasheet__c dataSheet = [SELECT Id
                                         FROM Dwolla_Datasheet__c
                                         LIMIT 1];
                                         
        
        String parameters = '{"parentId" : "'+dataSheet .Id+'" , "attachmentId" : null , "attachmentBody" : "Test" , "attachmentName" : "Test.png" ,'+
                            '"attachmentType" : "idCard" , "contentType" : "image/png"}';
                            
        system.debug('parameters ::'+parameters );
        
        Test.startTest();
        DwollaDatasheetDocController.Response  response = DwollaDatasheetDocController.uploadAttachment(parameters);
        
        String parameters2 = '{"parentId" : "'+response.recordId+'" , "attachmentId" : "'+response.data+'"}';
        
        DwollaDatasheetDocController.isTestValidToken = false;
        DwollaDatasheetDocController.Response  response2 = DwollaDatasheetDocController.createDwollaDocument(parameters2);
        
        DwollaDatasheetDocController.isTestValidToken = true;
        DwollaDatasheetDocController.Response  response3 = DwollaDatasheetDocController.createDwollaDocument(parameters2);
        
        List<Dwolla_Error_Response__c> errorList = [SELECT Id
                                                    FROM Dwolla_Error_Response__c];
        Test.stopTest();
        
        System.assertEquals(true, response.isSuccess);
        System.assertEquals(false,response2.isSuccess);
        System.assertEquals(false,response3.isSuccess);
        System.assertNotEquals(0, errorList.size());
    }

}