@isTest
private class AffiliatesReport_Cntrl_Test {
    @testSetup
    static void setupTestData() {
        test.startTest();
        Id EmployerRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Id ReferralAffiliateRecid = Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Referral Affiliate').getRecordTypeId();
        //Id StrategicAffiliateRecid= Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Strategic Affiliate').getRecordTypeId();
        //Id LeadGeneratorAffiliateRecid= Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Lead Generator').getRecordTypeId();
        //Id InternalSalespersonRecid= Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Internal Salesperson').getRecordTypeId();
        
        //TestDataFactory.Create_Pricing(null,1,true);


        Account account_Obj = new Account(BillingPostalCode = 'z', BillingStreet = 'z', billingcity = 'z', billingstate = 'z', billingcountry = 'z', Account_MEC_Administrator__c = 'Allied', Sedera_Rate_Structure__c = 'standard', Name = 'Name691', RecordTypeId = EmployerRecId,  Enrollment_Date__c = Date.today(), Dependent_Status__c = 'EO', Health_Care_Sharing__c = false, HSA__c = false, C_MEC__c = false, P_MEC__c = false, Cancelled__c = false, Member_Benefits__c = false, SecondMD__c = false, Take_Shape_for_Life__c = false, Tobacco_Use__c = false, Resend_Coverdell_Data__c = false, Good_Shepherd_Health__c = false, DPC__c = false, Teladoc_Direct__c = false, Contract_Status__c = 'Executed', MS_Premier__c = false, MS_Plus__c = false, Executed_Services_Agreement__c = false, COBRA__c = false, New_DPC__c = false, Update_Record__c = false, MS_Select__c = false, Liberty_Rx__c = false, IUA500__c = false, Adobe_Signup__c = false, IUA_1000__c = false, Invoice_not_paid__c = false, PMA__c = false);
        Insert account_Obj;
        Affiliate__c affiliate_Obj = new Affiliate__c(Name = 'Name880', recordtypeid = ReferralAffiliateRecid);
        Insert affiliate_Obj;
        AffiliateAccountAssociation__c affiliateaccountassociation_Obj = new AffiliateAccountAssociation__c(Employer_Account__c = account_Obj.id, Affiliate__c = affiliate_Obj.id);
        Insert affiliateaccountassociation_Obj;
        test.stopTest();
    }
    public static testmethod void testAffiliatesReport_Cntrl() {
        Pagereference pg = new pagereference('/apex/AffiliatesReport');
        Test.setcurrentpage(pg);
        ApexPages.currentPage().getParameters().put('AffiliateRecordTypeName', 'Referral Affiliate');

        AffiliatesReport_Cntrl cntrl = new AffiliatesReport_Cntrl();
    }
}