public class Account_Trigger_Handler {


    public static void handleBeforeInsert(List <Account> accList) {
    
        /***  Default Values for Converted accounts  ***/
        Account_Helper_Class.convertedAccDefaultValues(accList);
        
        /*** S2S connection Account Employer population  ***/
        Account_Helper_Class.S2Semployeronmember(accList);

        /*** Format Mobile Number ***/
        Account_Helper_Class.Format_Mobile_Number(accList, true, null);

        /***  Populate Member Account Fields(Event-Insert)  ***/
        Account_Helper_Class.PopulateFields_MemberAccount(accList);

        /***Process on filling of MCS_completed_on for Accounts processed via MobileApp  ***/
        Account_Helper_Class.MCScompletedOnProcess(accList, null);

        /*** MCS Product Calculation ***/
        Account_Helper_Class.MCSproductcalculation(accList, null);

        

    }
    public static void handleAfterInsert(Map <Id, Account> accNewMap) {
        /*** Creating Primary Contact and Storing Primary Contacts Id in Accounts Primary Contact Id field   ***/
        Account_Helper_Class.Create_Primary_Contact(accNewMap.values());

        /*** CODE TO LINK PRICING WITH EMPLOYER ACCOUNT ***/
        Account_Helper_Class.EmployerPricingLinking(accNewMap.values(), null);
        
        /*** CODE TO LINK DEFAULT MEC PRODUCT WITH EMPLOYER ACCOUNT ***/
        Account_Helper_Class.EmployerDefaultMecProdLinking(accNewMap.values());

        /*** CODE TO SEND SMS's ON ACCOUNT CREATION ***/
        if (userinfo.getprofileid() != Label.Profile_ID_of_client_services) {
            Account_Helper_Class.Send_SMS_On_Account_Creation(accNewMap.values(), accNewMap.keyset());
        }

        /*** CODE TO SEND SMS's ON  Completion OF HCS on Account ***/
        Account_Helper_Class.Send_SMS_On_HCS_Completion(accNewMap, null);
        
        /*** Setting Members For Employer Account Records  ***/
        Account_Helper_Class.Set_Members_For_Employer_Accounts(accNewmap, null); 
        
        /*** Affiliate Pricing Linking ***/
        Account_Helper_Class.AffiliatePricingLinking(accNewMap.values(), null);    
        
    }
    
    public static void handleBeforeUpdate(Map <Id, Account> accNewMap, Map <Id, Account> accOldMap) {
        /*** Format Mobile Number ***/
        Account_Helper_Class.Format_Mobile_Number(accNewMap.values(), false, accoldMap);

        /*** Updating Primary Contact's Phone FIelds  ***/
        Account_Helper_Class.Update_Primary_Contacts_Number(accNewMap, accOldMap);

        /*** Update memberdiscountiermanual if dpc changes  ***/
        Account_Helper_Class.ChangeinDPC(accNewMap.values(), accoldmap);

        /*** Process on filling of MCS_completed_on for Accounts processed via MobileApp  ***/
        Account_Helper_Class.MCScompletedOnProcess(accNewMap.values(), accOldMap);

        /*** MCS Product Calculation ***/
        Account_Helper_Class.MCSproductcalculation(accNewMap.values(), accoldMap);

        /*** Error on Reactivation ***/
        Account_Helper_Class.ThrowErrorOnReActivation(accNewMap.values(), accOldMap);

        /*** Change in MEC default ***/
        //Account_Helper_Class.ChangeInDefaultMEC(accNewMap.values(), accoldMap);

        /*** CODE TO LOCK MEMBER CASES ON IUA CHANGE ***/
        Account_Helper_Class.LockCasesonIUAChange(accNewMap, accOldMap);
        
        /*** Shooting an email to designtaed contacts of employer when contract status is set to execute  ***/
        Account_Helper_Class.emailcontractExecuted(accNewMap.values(), accOldMap);
        
        /*** Affiliate Pricing Linking ***/
        Account_Helper_Class.AffiliatePricingLinking(accNewMap.values(), accOldMap);
        
    }
    public static void handleAfterUpdate(Map <Id, Account> accNewMap, Map <Id, Account> accOldMap) {
        /*** Setting Members For Employer Account Records  ***/
        Account_Helper_Class.Set_Members_For_Employer_Accounts(accNewmap, accOldMap);

        /*** Code to link pricing with employer ***/
        Account_Helper_Class.EmployerPricingLinking(accNewMap.values(), accoldMap);

        /*** CODE TO SEND SMS's ON  Completion OF HCS on Account ***/
        Account_Helper_Class.Send_SMS_On_HCS_Completion(accNewMap, accoldMap);

        /*** CODE TO UPDATE MEMBER PRODUCTS ON Renewaldate and MSpricing CHANGE on Employer   ***/
        Account_Helper_Class.UpdateMemProductsOnMsPricingChange(accNewMap, accoldMap);

        /*** Code for creating proProfs User after manual approval for training ***/
        Account_Helper_Class.createProProfsUser(accNewMap.values(), accOldMap);
        
    }
    public static void handleBeforeDelete(Map <Id, Account> accOldMap) {
        /*** Setting Members For Employer Account Records  ***/
        Account_Helper_Class.Set_Members_For_Employer_Accounts(null, accOldMap);

    }

    public static void handleAfterUnDelete(Map <Id, Account> accNewMap) {

        /*** Setting Members For Employer Account Records  ***/
        Account_Helper_Class.Set_Members_For_Employer_Accounts(accNewmap, null);

    }


}