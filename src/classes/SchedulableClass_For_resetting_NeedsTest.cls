/**
 
 Test class for SchedulableClass_For_resetting_NoOfNeeds         
  
**/



@IsTest
public class SchedulableClass_For_resetting_NeedsTest
{

public static testmethod void TestMethod_For_resetting_Needs_Test1() {

        Test.startTest();
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        List <Account> EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        emp.default_product__c='sedera select';
        update emp;


        List <MEC_Product__c> Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T1';
        update Mec_Product_List[1];

        List <AccountMECAssociation__c> Mec_Assosciation_List = new List <AccountMECAssociation__c> ();


        for (integer j = 0; j <1; j++) {

            for (Integer i = 0; i <3; i++) {

                List <AccountMECAssociation__c> MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        List <Account> MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(3, false);       

        for (Account mem: MemberAccountList) {

            mem.Account_Employer_name__c = emp.id;
            mem.Dependent_Status__c = 'EO';
            mem.first_name__c = 'Test Account';
            mem.Enrollment_Date__c = date.today().addyears(-1);
            mem.Subscription_Status__c = 'Active';
            mem.Cancellation_Date__c = date.today();
        }

        insert  MemberAccountList ;
        
        
        
        
        List <Contact> ContactList = TestDataFactory.Create_Contact_Of_Sedera_Health_Contact_Type(3, true);
        Map<id,id> conAccMap=new Map<id,id>();
        Set <Id> Contact_Ids = new Set <Id> ();
      
        for (Integer j = 0; j <ContactList.size(); j++) {
            contactList[j].AccountId = MemberAccountList [j].id;
            Contact_Ids.add(contactList[j].Id);
            conAccMap.put(contactList[j].id,contactList[j].AccountId);
        }
        
        UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 0;
        Update ContactList;
     
         
        Map<id,List<case>> caseMap=TestDataFactory.Create_Cases(2, Contact_Ids, false);
        List<case> caseList=new List<case>();
        for(Id conId: caseMap.keyset()){       
           for(case cas:caseMap.get(conId)){
            cas.Accountid=conAccMap.get(cas.contactId);  
            caseList.add(cas);
           }      
        }
                     
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        insert caseList;

        
        SchedulableClass_For_resetting_NoOfNeeds sc=new SchedulableClass_For_resetting_NoOfNeeds();
        system.schedule('TestRun','0 0 00 1,15 * ?' ,sc);


        Test.StopTest();


    }




/*public static testmethod void TestMethod_For_resetting_Needs_Test2() {

        Test.startTest();
        
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        List <Account> PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Premier', true);

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        List <Account> EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.parentid = PlanAccountList[0].id;
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        update emp;


        List <MEC_Product__c> Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T1';
        update Mec_Product_List[1];

        List <AccountMECAssociation__c> Mec_Assosciation_List = new List <AccountMECAssociation__c> ();


        for (integer j = 0; j <1; j++) {

            for (Integer i = 0; i <3; i++) {

                List <AccountMECAssociation__c> MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        
        List <Account> MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(3, false); 
        List <Contact> ContactList = TestDataFactory.Create_Contact_Of_Sedera_Health_Contact_Type(3, true);

        for (Account mem: MemberAccountList) {

            mem.Account_Employer_name__c = emp.id;
            mem.Dependent_Status__c = 'EO';
            mem.first_name__c = 'mm';
            mem.Dependent_Status__c = 'EF';
            mem.Subscription_Status__c = 'Pending Cancellation';
            mem.Dependent_Status__c = 'EC';
            mem.Subscription_Status__c = 'Active';         
            mem.Cancellation_Date__c = date.today();
            
        }
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        
        insert MemberAccountList;

        Set <Id> Contact_Ids = new Set <Id> ();

        for (Integer j = 0; j <ContactList.size(); j++) {
            contactList[j].AccountId = MemberAccountList[j].id;
            Contact_Ids.add(contactList[j].Id);
        }

        Update ContactList;


        Map <id, List <Case>> Map_Of_Contact_And_Case_List = TestDataFactory.Create_Cases(2, Contact_Ids, true);

        
        SchedulableClass_For_resetting_NoOfNeeds sc=new SchedulableClass_For_resetting_NoOfNeeds();
        system.schedule('TestRun','0 0 00 1,15 * ?' ,sc);
        
        Test.StopTest();


    }

*/
}