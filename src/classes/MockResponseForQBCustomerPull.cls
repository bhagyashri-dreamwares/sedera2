/**
 * Purpose : Class to generate mock response for http callout
*/
@isTest
public class MockResponseForQBCustomerPull implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        if( req.getEndpoint() == 'https://oauth.platform.intuit.com/oauth2/v1/token' ) {
            res.setBody('{"refresh_token":"refresh_tokenhjbhsbvhbsdvhjbdsvh","token_type":"token_typedgdd","access_token":"access_tokendgdd","expires_in":100,"x_refresh_token_expires_in":100,"realm_id":"realm_iddgdd"}');
        }
        else if(req.getEndpoint().contains('https://sandbox-quickbooks.api.intuit.com/v3/company/123145860463979/query') &&  req.getEndpoint().contains('Invoice')){
            res.setBody('{"QueryResponse":{"Invoice":[{"Deposit":0,"AllowIPNPayment":false,"AllowOnlinePayment":false,"AllowOnlineCreditCardPayment":false,"AllowOnlineACHPayment":false,"domain":"QBO","sparse":false,"Id":"1","SyncToken":"1","MetaData":{"CreateTime":"2018-02-12T20:00:38-08:00","LastUpdatedTime":"2018-02-12T20:13:09-08:00"},"CustomField":[{"DefinitionId":"1","Name":"Crew #","Type":"StringType"},{"DefinitionId":"2","Name":"temp field","Type":"StringType"},{"DefinitionId":"3","Name":"Service Period","Type":"StringType"}],"TxnDate":"2018-02-12","CurrencyRef":{"value":"USD","name":"United States Dollar"},"PrivateNote":"Opening Balance","LinkedTxn":[{"TxnId":"1132","TxnType":"Payment"}],"Line":[{"Id":"1","LineNum":1,"Description":"Opening Balance","Amount":1000.00,"DetailType":"SalesItemLineDetail","SalesItemLineDetail":{"ItemRef":{"value":"1","name":"Services"},"TaxCodeRef":{"value":"NON"}}},{"Amount":1000.00,"DetailType":"SubTotalLineDetail","SubTotalLineDetail":{}}],"TxnTaxDetail":{"TotalTax":0},"CustomerRef":{"value":"1","name":"00000 SF-QB Test 958"},"BillAddr":{"Id":"3119","Line1":"SF street 1009","City":"SF city","Country":"United States","CountrySubDivisionCode":"Alaska","PostalCode":"12345"},"ShipAddr":{"Id":"3118","Line1":"shipping","City":"city","Country":"United States","CountrySubDivisionCode":"Alabama","PostalCode":"12345"},"DueDate":"2018-02-12","TotalAmt":1000.00,"ApplyTaxAfterDiscount":false,"PrintStatus":"NotSet","EmailStatus":"NotSet","Balance":950.00}],"startPosition":1,"maxResults":1,"totalCount":1},"time_qb":"2018-02-13T03:57:36.094-08:00"}');
        }
        else{
          res.setBody('{ "QueryResponse": { "Customer": [{ "Taxable": true, "BillAddr": { "Id": "1", "Line1": "4581 Finch St. 16545 12321", "City": "Bayshore", "CountrySubDivisionCode": "CA", "PostalCode": "55587", "Lat": "INVALID", "Long": "INVALID" },"ShipAddr": { "Id": "1", "Line1": "4581 Finch St. 16545 12321", "City": "Bayshore", "CountrySubDivisionCode": "CA", "PostalCode": "55587", "Lat": "INVALID", "Long": "INVALID" }, "Notes": "hi16 updated record", "Job": false, "BillWithParent": false, "Balance": 17839.0, "BalanceWithJobs": 17839.0, "CurrencyRef": { "value": "USD", "name": "United States Dollar" }, "PreferredDeliveryMethod": "Print", "domain": "QBO", "sparse": false, "Id": "1", "SyncToken": "31", "MetaData": { "CreateTime": "2017-06-16T16:48:43-07:00", "LastUpdatedTime": "2017-09-12T02:42:32-07:00" }, "GivenName": "Amyuu", "FamilyName": "Lauterbachuu", "FullyQualifiedName": "Amys Bird Sanctuary", "CompanyName": "Amys Bird Sanctuary", "DisplayName": "Amys Bird Sanctuary", "PrintOnCheckName": "Amys Bird Sanctuary", "Active": true, "PrimaryPhone": { "FreeFormNumber": "(650) 555-3311" }, "Mobile": { "FreeFormNumber": "(123) 35453777" }, "PrimaryEmailAddr": { "Address": "Birds@Intuit.com" }, "DefaultTaxCodeRef": { "value": "2" } }], "startPosition": 1, "maxResults": 1 }, "time": "2017-09-18T08:47:11.389-07:00" }');       
        }    

        res.setStatusCode(200);
        System.debug('Repsopds  bfdsgfdsgsdgsd@@@@@@@@@@@@@@@@@@@@@@@@@@@');
        return res;
    }    
}