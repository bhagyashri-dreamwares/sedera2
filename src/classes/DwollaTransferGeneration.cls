/**
* @Purpose : Batch class to create the DwollaTransfer for the newly generated Invoice objects
* @Date : 27/12/2018
*/
global class DwollaTransferGeneration implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    
    private List<ID> invoiceIDList= new List<ID>(); 
    global final List<ID> dwollaTransferIDList;
     
    public DwollaTransferGeneration (List<ID> invoiceIDs){
        if(invoiceIDs != null && !invoiceIDs.isEmpty()){
      
            dwollaTransferIDList= new List<ID>();
            invoiceIDList = invoiceIDs;
        }
    }
    
     public Database.QueryLocator start(Database.BatchableContext info){
         //rather than going through all the invoices, we're going to limit it to the invoices associated with three active Dwolla funding sources
         String query = 'select name, id, Invoice_Date__c, account__c, amount__c from invoice__C where id in :invoiceIDList and Account__r.Active_Dwolla_Funding_Source_Count__c = 3';
         system.debug('start query');
         system.debug(query);
         return Database.getQueryLocator(query); 
     }
     
     public void execute(Database.BatchableContext bc, List<Invoice__c> invoiceList){
            system.debug('in execute');
         //get all of the accounts referenced in the invoiceList
         list<ID> accounts = new list<id>();
         for (invoice__c invoice : invoiceList){
             accounts.add(invoice.account__c);
         }
         
         //query all the Customer and Balance funding sources for the accounts in the invoiceList
         list<dwolla_Funding_source__c> fundingSourceList  = [select id, Type__c, Dwolla_Funding_Source_ID__c, Employer_Account__c from dwolla_Funding_source__c where Employer_Account__c in :accounts and 
                                                             (Type__c = 'Customer' or Type__c = 'Balance') AND Dwolla_Funding_Source_ID__c != null AND Active__c = TRUE];

         //go through each funding source, split into customer and balance
         map<id, Dwolla_Funding_Source__c> customerFundingSourceMap = new map<id, Dwolla_Funding_Source__c>();
         map<id, Dwolla_Funding_Source__c> balanceFundingSourceMap = new map<id, Dwolla_Funding_Source__c>();
         
         for (dwolla_funding_source__c dfs : fundingSourceList) {
             if (dfs.type__c == 'Customer') {
                 customerFundingSourceMap.put(dfs.Employer_Account__c, dfs);
             }
             else if (dfs.type__c =='Balance') {
                 balanceFundingSourceMap.put(dfs.Employer_Account__c, dfs);
             }
         }
         
         string balanceFundingSourceID;
         string customerFundingSourceID;
         list<dwolla_transfer__c> dwollaTransfers = new list<dwolla_transfer__c>();
         Dwolla_transfer__c dwollaTransfer;

         //create a list of dwolla transfers and populate it with invoice, account, customer funding source id, balance funding source id, and amount
         for (invoice__c invoice : invoiceList){
                     system.debug('in for loop on invoiceList');
             //need to clear out the funding source IDs from previous entries, and then later check we have values for both before creating transfer record
             balanceFundingSourceID = '';
             customerFundingSourceID = '';
    
             //get the Balance funding source
             if(balanceFundingSourceMap.get(invoice.account__c) != null)
                 balanceFundingSourceID = balanceFundingSourceMap.get(invoice.account__c).ID;
    
             //get the customer funding source
             if(customerFundingSourceMap.get(invoice.account__c) != null)
                 customerFundingSourceID = customerFundingSourceMap.get(invoice.account__c).ID;
    
             //create a new dwolla transfer record in the list
             //confirm that both FundingSourceIDs were found
             if (customerFundingSourceID != '' && balanceFundingSourceID != ''){
                 dwollaTransfer = new dwolla_Transfer__c();
                 dwollaTransfer.Source_Dwolla_Funding_Source__c = customerFundingSourceID;
                 dwollaTransfer.Destination_Dwolla_Funding_Source__c = balanceFundingSourceID;
                 dwollaTransfer.invoice__c = invoice.id;
                 dwollaTransfer.amount__c = invoice.amount__c;
                 dwollaTransfers.add(dwollaTransfer);
             }
             else //Should probably log a message but continue executing for the rest of the batch. Right now just a place holder
                 system.debug('Either Customer or Balance funding source could not be found for invoice ' + invoice.name + ' - ' + invoice.Name);
    
         }

         insert dwollaTransfers;    
         
         for (Dwolla_Transfer__c singleDwollaTransfer : dwollaTransfers) {

             dwollaTransferIDList.add(singleDwollaTransfer.ID);
                      system.debug(singleDwollaTransfer.id);
         }
     }
     
     public void finish(Database.BatchableContext BC){

         
         //add code here to execute the next batch to process transfers and pass in dwollaTransferIDList

         system.debug('finish method');
         system.debug(dwollaTransferIDList.size());
         if (!Test.isRunningTest())
         {
             DwollaTransfersBatch dwollaTransBatch = new DwollaTransfersBatch (dwollaTransferIDList);
             Database.executebatch(dwollaTransBatch, 30);
         }
     }
     
}