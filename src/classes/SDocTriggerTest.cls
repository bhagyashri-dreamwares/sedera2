/**
*@Purpose : Test method for SDocTrigger
*@Date : 01/08/2018
*/
@isTest
public class SDocTriggerTest{
    
    /*
    * To initialise data
    */    
    @testSetup
    private static void initData(){
        
        //insert custom setting record
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'Test@test.com';
        config.Site_Url__c = 'test@test.com';
        insert config;
        
        //insert Account
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account emp = new Account();
        emp.recordTypeId = accRecordTypeId;
        emp.Default_Product__c = 'Sedera Access';
        emp.MS_Pricing__c = 'New Pricing';
        emp.Available_IUA_Options__c = '500';
        emp.Name = 'Test'; 
        emp.Default_Product__c = 'Sedera Access';       
        insert emp;
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        Account account = new Account();
        account.recordTypeId = devRecordTypeId;
        account.Name = 'Test AccountName';
        account.First_Name__c = 'FName';
        account.Last_Name__c = 'LName';
        account.Account_Employer_Name__c = emp.Id;
        account.Primary_Email_Address__c = 'test@test.com';
        insert account;
        
        // insert contact
        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.LastName = 'Test Name';
        contact.Email = 'test@test.com';
        insert contact;
        
        // insert Case
        Case caseRecord = new Case();
        caseRecord.ContactId = contact.Id;
        caseRecord.Status = 'New';
        caseRecord.Origin = 'Phone';
        caseRecord.Type = 'HCS';
        caseRecord.Correspondence_Email__c = 'test@test.com';
        insert caseRecord;
        
        // insert Attachment
        Attachment attach = new Attachment(); 
        attach.Name='Test.pdf'; 
        attach.body= Blob.valueOf('Unit Test Attachment Body'); 
        attach.parentId = caseRecord.Id; 
        attach.ContentType = 'application/pdf'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        insert attach;
        
    }
    
    /*
    * To test Email sending Functionality
    */
    private static testMethod void testEmailFunctionality(){
        
        Attachment attachment = [SELECT Id, Name, ParentId
                                 FROM Attachment
                                 LIMIT 1];
        
        SDOC__SDoc__c sDoc = new SDOC__SDoc__c();
        sDoc.SDOC__Attachment_ID__c = attachment.Id;
        sDoc.SDOC__Attachment_Name__c = 'SDocAttachment';
        sDoc.SDOC__ObjectID__c = attachment.ParentId;
        
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailHelperMock());
        Test.startTest();
        
        insert sDoc;
        PauBoxEmailHelper.sendEmail(new List<String>{sDoc.id});
         
        Test.stopTest();
        List<SDOC__SDoc__c> sDocRecordList = [SELECT Id, SDOC__Attachment_Name__c FROM SDOC__SDoc__c];
        
        System.assertEquals(sDocRecordList[0].SDOC__Attachment_Name__c, 'Test.pdf');
        
    }
    
    public class  PauBoxEmailHelperMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"sourceTrackingId": "3d38ab13-0af8-4028-bd45-52e882e0d584","data": "Service OK"}');
            response.setStatusCode(200);
            
            return response;
        }
    }
}