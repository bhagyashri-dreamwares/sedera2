@istest
public class testinglightningclasses
{
@testsetup
public static void setup()
{
Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
Id devRecordTypeId2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
Id devRecordTypeId3 = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Sedera Health Contact').getRecordTypeId();

list<Account> bb=new list<Account>();

Account a=new Account(name='ddd',recordtypeid=devRecordTypeId2,First_Name__c = 'dhdh', Last_Name__c = 'dhdh');
bb.add(a);

Account a2=new Account(name='test',recordtypeid=devRecordTypeId2,First_Name__c = 'dhdh', Last_Name__c = 'dhdh');
bb.add(a2);

Account b=new Account(name='testt',Account_Employer_name__c=a.id,Dependent_Status__c='EO',recordtypeid=devRecordTypeId,Subscription_Status__c='Active',First_Name__c = 'dhdh', Last_Name__c = 'dhdh');
bb.add(b);
Account b2=new Account(name='testt',Dependent_Status__c='EO',recordtypeid=devRecordTypeId,enrollment_date__c=system.now().date(),First_Name__c = 'dhdh', Last_Name__c = 'dhdh');
bb.add(b2);
Account b3=new Account(name='testt',Account_Employer_name__c=a.id,Dependent_Status__c='EO',recordtypeid=devRecordTypeId,Subscription_Status__c='Active',First_Name__c = 'dhdh', Last_Name__c = 'dhdh');
bb.add(b3);
Account b4=new Account(name='testt',Account_Employer_name__c=a.id,Dependent_Status__c='EO',recordtypeid=devRecordTypeId,enrollment_date__c=system.now().date(),First_Name__c = 'dhdh', Last_Name__c = 'dhdh');
bb.add(b4);

insert bb;
Employer__kav article = new Employer__kav();
article.put('title', 'test');
article.put('language','en_US');
//article.put('publishStatus','Online');
String Urlname = 'test'+''+System.now().getTime();
article.put('UrlName',Urlname);

insert article;

Employer__kav article1 = new Employer__kav();
article1.put('title', 'test');

article1.put('language','en_US');
//article1.put('publishStatus','Online');
String Urlname1 = 'test'+''+System.now().getTime();
article1.put('UrlName',Urlname1);

insert article1;
list<employer__kav> knowledgearticlelist=[SELECT Id,KnowledgeArticleid  FROM employer__kav WHERE PublishStatus = 'Draft'  AND Language = 'en_US' limit 2];
for(employer__kav  k:knowledgearticlelist){
KbManagement.PublishingService.publishArticle(k.KnowledgeArticleid, true);
}
 Employer__DataCategorySelection dc1=new Employer__DataCategorySelection();
dc1.DataCategoryGroupName='Employer_Knowledge_Base';
dc1.DataCategoryName='All';
dc1.parentid=article1.id;  
insert dc1;
Employer__DataCategorySelection dc=new Employer__DataCategorySelection();
dc.DataCategoryGroupName='Employer_Knowledge_Base';
dc.DataCategoryName='All';
dc.parentid=article.id;  
insert dc;
Topic Tx=new Topic(name='My Resources');
insert Tx;
TopicAssignment Ty=new TopicAssignment();
Ty.EntityId=article.id;
Ty.topicid=Tx.id;
insert Ty;
TopicAssignment Ty1=new TopicAssignment();
Ty1.EntityId=article1.id;
Ty1.topicid=Tx.id;
insert Ty1;
//KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
//KbManagement.PublishingService.publishArticle(article1.KnowledgeArticleId, true);

ArticleAttachment.getArticleandAttachment(Tx.id,false,false,0);
ArticleAttachment.getArticleandAttachment(Tx.id,true,false,1);
system.debug('???????????');
ArticleAttachment.getArticle(article1.id);
system.debug('???????????');

ArticleAttachment.getArticleandAttachment(Tx.id,false,true,0);


ArticleAttachment.getTopicidfromsalesforce();
/*Attachment att=new Attachment(body=Blob.valueof('xxxx'),name='saurav');
insert att;*/

               /*******************   Creating Community User  ******************************/
               
Set<String> customerUserTypes = new Set<String> {'CSPLiteUser','PowerPartner', 'PowerCustomerSuccess','CustomerSuccess','CspLitePortal'};
Account acc = new Account (
Name = 'newAcc1'
);  
insert acc;
Contact con = new Contact (
AccountId = acc.id,
LastName = 'portalTestUser'
);
insert con;
Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
 
User newUser = new User(
profileId = p.id,
username = 'newcommunityUser@yahoo.com',
email = 'pb@ff.com',
emailencodingkey = 'UTF-8',
localesidkey = 'en_US',
languagelocalekey = 'en_US',
timezonesidkey = 'America/Los_Angeles',
alias='nuser',
lastname='lastname',
contactId = con.id
);
insert newUser;
}

public static testmethod void test1(){
Id devRecordTypeId2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();

Account a=[select id from Account where recordtypeid=:devRecordTypeId2 limit 1 ];
getaccountforcommunity.getMemberAccountsfromsalesforce(false,false,0,a.id);
getaccountforcommunity.getMemberAccountsfromsalesforce(false,true,1,a.id);
getaccountforcommunity.getMemberAccountsfromsalesforce(true,false,0,a.id);
User u=[select id from user where username='newcommunityUser@yahoo.com'];
system.runas(u){
getaccountforcommunity.getAccountforAccountList();


}
getaccountforcommunity.getAccountfromsalesforce(a.id);
getaccountforcommunity.setAccountinsalesforce(a);
getaccountforcommunity.getAccountforAccountList();
id idd=getaccountforcommunity.getTopicidfromsalesforce();
createCases c=new createCases();
c=createcases.getcreatedcase('kk','jjj');
id f=createcases.getTopicidfromsalesforce();
id i=FileControllerforAttachment.saveTheFile(c.cass.id,'kkk','jjj','jpeg');
id v=FileControllerforAttachment.saveTheChunk(c.cass.id,'kkk','jjj','jpeg',i);
createCases.testexception=999976;

createcases.getcreatedcase('kk','m');

}







}