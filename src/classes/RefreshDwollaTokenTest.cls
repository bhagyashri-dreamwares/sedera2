@isTest
public class RefreshDwollaTokenTest{
    
    @TestSetup
    public static void setup(){
        
        // Created a record of DwollaAPIConfiguration__c custom Setting.
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Client_Key__c='IHLoutgdersu7jWYfr9uy2I8PJ9M5bT0GTRweTWKnoLyubrLpo';
        customSettingObj.Secret_Key__c='IHLoutgnfhhnfdgklwusklkjayuhfkkiGTRweTWKnoLyubrLpo';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Access_Token_Expiry__c=system.today()+7;
        insert customSettingObj;
        
    }
    
     public static testMethod void testScheduler(){
       
        Test.startTest(); 
                
            String strCronExp = '0 0 1 * * ?'; 
            String jobID = system.schedule('Test scheduler', strCronExp, new RefreshDwollaToken());
        	DwollaAPIConfiguration__c config = [SELECT Id, Access_Token_Expiry__c
                                            FROM DwollaAPIConfiguration__c
                                            LIMIT 1];
            DwollaAccessTokenHelper.updateAPIConfiguration(config);
        Test.stopTest();
       
        System.assert(DateTime.now() <= config.Access_Token_Expiry__c);
    }
}