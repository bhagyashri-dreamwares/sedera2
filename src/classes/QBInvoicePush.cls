/**
*@Author       : Dreamwares
*@Created Date : 19/07/2018
*@Purpose      : Post Updated Invoices from salesforce to 
*/
public with sharing class QBInvoicePush{
    
    IParser parserImpl = new QBInvoiceParser(); 
    Map<String,String> batchIdRecordIdMap = new Map<String, String>();
    Map<String,List<String>> invoiceLineItemIdMap = new Map<String, List<String>>();
    Map<String,DateTime> batchIdToDateTime = new Map<String,DateTime>();
    public Set<String> needCreditReviewInvoiceIds = new Set<String>();
    public Set<String> needAutoPayInvoiceIds = new Set<String>();
    public final Integer MAXRECORDS = 150;
    public Integer batchNo =0;
    
    public List<SObject> fetchRecords(){
        DateTime fromTime = DateTime.now().addHours(-12);
        List<Invoice__c> invoicesList = new List<Invoice__c>();
        return invoicesList;
    }
    
    public BAPIResponseWrapper doPost(List<SObject> recordsList, String currentQuickBookOrgName){
        BAPIResponseWrapper response = new BAPIResponseWrapper(false, currentQuickBookOrgName);
       
        if(recordsList != null && recordsList.size() > 0){
            batchNo++;
            /*
            // Hardcore code for Demo
            List<String> quickbooks2ProductIDs = new List<String>{'19','20','21','22'} ;
            List<Invoice_Line_Item__c> inviceLineItemList = [SELECT Id, QBO2_Product__c, Quickbooks2_Product_ID__c, Line_Item_Total__c, Quantity__c 
                                                             FROM Invoice_Line_Item__c 
                                                             WHERE Quickbooks2_Product_ID__c IN: quickbooks2ProductIDs
                                                             LIMIT 4];
List<QBWrappers.QBInvoiceDTO> invoicesList = (List<QBWrappers.QBInvoiceDTO>)parserImpl.parseToDTOList(recordsList,currentQuickBookOrgName,inviceLineItemList);
            */
            List<QBWrappers.QBInvoiceDTO> invoicesList = (List<QBWrappers.QBInvoiceDTO>)parserImpl.parseToDTOList(recordsList,currentQuickBookOrgName);
             System.debug('recordsList ::: '+recordsList);
             System.debug('invoicesList ::: '+invoicesList);
            if(invoicesList != null && invoicesList.size() > 0){
                
                QBInvoiceBatchRequestWrapper qbBatchRequestObj = new QBInvoiceBatchRequestWrapper();
                Integer i = 1;
                DateTime currentTime = system.now();
                
                for(QBWrappers.QBInvoiceDTO invoice :invoicesList){
                    
                    List<Id> lineItemIdList = new List<Id>();
                    invoice__c invoiceRecord = (invoice__c)recordsList[i-1];
                    
                    for(Invoice_Line_Item__c lineItemRecord : invoiceRecord.Invoice_Line_Items__r){
                        lineItemIdList.add(lineItemRecord.Id);  
                    }
                    
                    invoiceLineItemIdMap.put(recordsList[i-1].Id, lineItemIdList);
                    
                    String batchId = 'bid' + batchNo +  i;
                    
                    QBInvoiceBatchRequestWrapper.QBBatchInvoiceObject batchObject;
                    
                    if(String.isNotBlank(invoice.Id)){
                        batchObject = new QBInvoiceBatchRequestWrapper.QBBatchInvoiceObject(batchId, 'update');
                        
                    }
                    else {
                        batchObject = new QBInvoiceBatchRequestWrapper.QBBatchInvoiceObject(batchId, 'create');
                        batchIdToDateTime.put(batchId,currentTime);
                    }
                    
                    batchIdRecordIdMap.put(batchId, recordsList[i-1].Id);
                    
                    batchObject.Invoice = invoice;
                    qbBatchRequestObj.BatchItemRequest.add(batchObject);
                    i++;
                }
                
                QBConfigurationWrapper qbConfiguration = new QBConfigurationWrapper(currentQuickBookOrgName);
                QBAPIGateway qbGateway = new QBAPIGateway(qbConfiguration);
                String responseBody    = qbGateway.batch(JSON.serialize(qbBatchRequestObj));
                System.debug('responseBody::'+responseBody);
                if(String.isNotBlank(responseBody) || Test.isRunningTest()){ 
                    System.debug('responseBody :: '+Json.serialize(responseBody));
                    QBBatchResponse batchResponse = new QBBatchResponse();
                    try{
                        
                        batchResponse = (QBBatchResponse)JSON.deserialize(responseBody, QBBatchResponse.class);
                        response                = new BAPIResponseWrapper(true, '');
                        response.records        = batchResponse.BatchItemResponse;
                        response.responseTime   = QBSyncHelper.dateFromString(batchResponse.time_qb);
                    }
                    catch(Exception e){
                        QuickbookDebugServices.trackException(e);
                        System.debug('Error deserializing batch response :: ' + e.getMessage());
                    }
                }
                else{
                    response.message ='Blank response from QB for Invoices';
                }
                
            }
        }
        return response;
    }
    
    public void save(List<SObject> records){}
    
    public void updateRecords(List<Object> records, DateTime lastSyncDate, String currentQuickBookOrgName){
        if(records != null && records.size() > 0){
            
            System.debug('records ::: '+JSON.serialize(records));
            Map<String, QBWrappers.QBInvoiceDTO>  batchIdInvoiceMap = new Map<String, QBWrappers.QBInvoiceDTO>();
            
            Map<String,QBBatchFault> faultRecordMap = new Map<String,QBBatchFault>();
            List<String> faultRecordsList = new List<String>();
            List<QB_Sync_Errors__c> quickbookDebugList = new List<QB_Sync_Errors__c>();
            List<InvoiceToLineItemWrapper>  invoiceWithLineItems;
            for(QBBatchResponseObject record : (List<QBBatchResponseObject>)records){
                
                if(record.Invoice != null){
                    // Invoice record
                    batchIdInvoiceMap.put(record.bId, record.Invoice);
                }
                else if(record.Fault != null){
                    // fault
                    faultRecordMap.put(record.bId,record.Fault);
                    faultRecordsList.add(record.bId);
                }
            }
            // Send Mail from here
            System.debug('Sending mail ::: ');
            /*SendMailsFromQuickBook sendInvoices = new SendMailsFromQuickBook();
            sendInvoices.sendMailForInvoice(batchIdInvoiceMap.values());*/
            
            Map<Integer, Invoice__c> indexInvoiceMap = new Map<Integer, Invoice__c>();
            Map<Integer, List<Invoice_Line_Item__c>> indexInvoiceLineItemsMap = new Map<Integer, List<Invoice_Line_Item__c>>();
           
            if(batchIdInvoiceMap.size() > 0 || Test.isRunningTest()){
                invoiceWithLineItems = (List<InvoiceToLineItemWrapper>)parserImpl.parseToObjectList(batchIdInvoiceMap.values(),currentQuickBookOrgName);  
              
                if(invoiceWithLineItems != null && invoiceWithLineItems.size() > 0){
                    Integer invoiceRecordsCount = invoiceWithLineItems.size();
                    if(invoiceRecordsCount > 0){
                        Integer index = 0;
                        // Map Success Records from quickboox to salesforce invoice records
                        for(String bId: batchIdInvoiceMap.keySet()){
                            //invoiceWithLineItems[index].invoice.Last_Sync_Date__c = lastSyncDate;
                            invoiceWithLineItems[index].invoice.Id = batchIdRecordIdMap.get(bId);
                            if(batchIdToDateTime.containskey(bId)){
                                if(currentQuickBookOrgName == 'Quickbooks'){
                                  invoiceWithLineItems[index].invoice.Quickbooks_Created_Date__c = batchIdToDateTime.get(bId);    
                                }else if(currentQuickBookOrgName == 'Quickbooks2'){
                                  
                                  invoiceWithLineItems[index].invoice.Quickbooks2_Created_Date__c = batchIdToDateTime.get(bId);    
                                }  
                            }
                            
                            
                            indexInvoiceMap.put(index, invoiceWithLineItems[index].invoice);    
                            indexInvoiceLineItemsMap.put(index, invoiceWithLineItems[index].invoiceLineItems);
                            index++;
                        }
                    }
                }  
            }
            
            List<Invoice__c> faultInvoiceList = new List<Invoice__c>();
            
            // Map fault Records from quickboox to salesforce invoice records
            for(String bId :faultRecordsList){
                if(batchIdRecordIdMap.containsKey(bId)){
                    Invoice__c invoiceRecord = new Invoice__c();
                    invoiceRecord.id =  batchIdRecordIdMap.get(bId);
                    if(currentQuickBookOrgName == 'Quickbooks'){
                      invoiceRecord.Quickbooks_Creation_Status__c = 'Error';    
                    }
                    
                    if(currentQuickBookOrgName == 'Quickbooks2'){
                      invoiceRecord.Quickbooks2_Creation_Status__c = 'Error';    
                    }
                    //invoiceRecord.Last_Sync_Date__c = lastSyncDate;
                    //invoiceRecord.SF_Last_Modified_Date__c = lastSyncDate; 
                    
                    if(faultRecordMap.containsKey(bId)){
                      
                        QB_Sync_Errors__c qbDebugRecord = new QB_Sync_Errors__c();
                        qbDebugRecord.Name = 'Failed Invoices During Sync Id::::'+batchIdRecordIdMap.get(bId);
                        qbDebugRecord.Type__c = 'Failed In Sync';
                        qbDebugRecord.Invoice__c = batchIdRecordIdMap.get(bId);
                        if(faultRecordMap.get(bId) != null){
                            QBBatchFault errorRecord = faultRecordMap.get(bId);
                            if(errorRecord.Error[0] != null){
                                if(currentQuickBookOrgName == 'Quickbooks'){
                                    invoiceRecord.Quickbooks_Error_Message__c = errorRecord.Error[0].Detail+errorRecord.Error[0].Message;
                                    qbDebugRecord.ErrorMessage__c = errorRecord.Error[0].Detail+errorRecord.Error[0].Message;
                                }
                                if(currentQuickBookOrgName == 'Quickbooks2'){
                                    invoiceRecord.Quickbooks2_Error_Message__c = errorRecord.Error[0].Detail+errorRecord.Error[0].Message;
                                    qbDebugRecord.ErrorMessage__c = errorRecord.Error[0].Detail+errorRecord.Error[0].Message;
                                }
                                
                            }
                            quickbookDebugList.add(qbDebugRecord);
                        }   
                    }
                    
                    faultInvoiceList.add(invoiceRecord);
                }
            } 
            List<Invoice__c> toBeUpdateInvoices = new List<Invoice__c>();
            toBeUpdateInvoices.addAll(indexInvoiceMap.values());
            if(!faultInvoiceList.isEmpty()){
                toBeUpdateInvoices.addAll(faultInvoiceList);
            }
            try{
                if(!toBeUpdateInvoices.isEmpty()){
                    Database.SaveResult[] invoiceUpdateResult = Database.update(toBeUpdateInvoices, false);
                    QuickbookDebugServices.trackUpdateResult('Invoice__c', invoiceUpdateResult);
                }
                if(!quickbookDebugList.isEmpty()){
                    Insert quickbookDebugList;
                }
                
                if(currentQuickBookOrgName == 'Quickbooks'){ 
                
                    List<Invoice_Line_Item__c> lineItemsList = new List<Invoice_Line_Item__c>();
                   // List<Invoice__c> InvoiceListForQB2 = new List<Invoice__c>();
                    Set<Invoice_Line_Item__c> lineItemsSet = new Set<Invoice_Line_Item__c>();
                    // update line items parent invoice Ids
                    for(Integer i = 0;  i < indexInvoiceMap.size() ;i++){
                        
                        
                        
                        Integer lineItemCount = indexInvoiceLineItemsMap.get(i).size();
                        for(Integer j= 0; j<lineItemCount; j++){
                            Invoice__c invoiceRecord = new Invoice__c();
                            Invoice_Line_Item__c invoiceLineItem = new Invoice_Line_Item__c();
                            invoiceLineItem.Invoice__c = indexInvoiceMap.get(i).Id;
                            List<String> lineItemIds = invoiceLineItemIdMap.get(indexInvoiceMap.get(i).Id);
                            invoiceLineItem.Id = lineItemIds[lineItemIds.size() - 1];
                                                     
                                invoiceLineItem.QB_Line_Item_Id__c = indexInvoiceLineItemsMap.get(i)[indexInvoiceLineItemsMap.get(i).size() - 1].QB_Line_Item_Id__c;                            
                            
                                                    
                            lineItemsSet.add(invoiceLineItem);
                        }
                    }
                    if(currentQuickBookOrgName == 'Quickbooks'){
                        lineItemsList.addAll(lineItemsSet);
                        System.debug('lineItemsList::'+JSON.serialize(lineItemsList));
                        Database.SaveResult[] lineItemUpdateResult = Database.update(lineItemsList, false);
                        System.debug('lineItemUpdateResult::'+JSON.serialize(lineItemUpdateResult));
                        QuickbookDebugServices.trackUpdateResult('Invoice_Line_Item__c', lineItemUpdateResult);
                    }
                }               
                
            }
            catch(Exception e){
                QuickbookDebugServices.trackException(e);
                System.debug('Error upserting invoices :: ' + e.getMessage());
            }  
        }    
    }
}