public class CreateUpdateAssetExtension {

    public Asset asset;
    public String UpdateInsert{get;set;}
    public CreateUpdateAssetExtension(ApexPages.StandardController controller) {
         asset = (Asset) controller.getRecord();
        asset.name = 'Asset';
        UpdateInsert=ApexPages.currentPage().getParameters().get('id');
    }

public pageReference SaveAndRedirectToParent()
{
if(UpdateInsert==null || UpdateInsert=='')
{
Account a=[select Account_Employer_Name__c from Account where id =:asset.AccountId limit 1];
asset.Employer_Account__c=a.Account_Employer_Name__c;
insert asset;
}
else
update asset;

pagereference pg=new pagereference('/'+asset.AccountId);
pg.setRedirect(true);
return pg;
}
   
   public pageReference CancelAndRedirectToParent()
{
pagereference pg=new pagereference('/'+asset.AccountId);
pg.setRedirect(true);
return pg;
}
}