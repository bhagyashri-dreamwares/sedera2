/**
* @Author      : Dreamwares
* @Purpose      : - Callback handler for OAuth 2.0
*            - Assumption : Provider name is in state paramater
* @Created Date  : 18/07/2018
*/
public class QBOAuthCallbackController {
    public Boolean isRedirected{get;set;}
    public String currentQBOrg{get;set;}
    public String callbackMethod{get;set;}
    
    public QBOAuthCallbackController(){
        //
    }
    
    public void handleOAuthCallback(){
        Map<String, String> authorizationParametersMap = ApexPages.currentPage().getParameters();
        
        // check if verification callback
        if( authorizationParametersMap.containsKey('state') && 
           authorizationParametersMap.containsKey('code') && 
           authorizationParametersMap.containsKey('realmId')){
               isRedirected = TRUE;
               OAuth2 auth2;
               if(authorizationParametersMap.get('state') == 'Quickbooks'){
                    currentQBOrg = authorizationParametersMap.get('state');                   
               }
               QBConfigurationWrapper qbConfig = new QBConfigurationWrapper('Quickbooks');
               auth2 = new OAuth2(qbConfig);
               
               if(auth2 != null){
                   // Get Access Token
                   Response response = auth2.getAccessToken(authorizationParametersMap.get('state'), 
                                                            authorizationParametersMap.get('code'),
                                                            authorizationParametersMap.get('realmId'));
                   
                   if(response.Success){
                       OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                       authTokenResponse.realm_id = authorizationParametersMap.get('code');
                       
                       // Update Access Token, refresh token, access token expiry, refresh token expiry
                       QBConfigurationWrapper.saveTokens(authTokenResponse,'Quickbooks');
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Authorized Successfully!'));
                   }else{
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,response.Message));
                   }
               }
           }else{
               // no call back redirect
               isRedirected = FALSE;
           }
    }
    
    /*
* @Purpose      : Quickbooks Login(Authorization redirection)
*/
    public Pagereference loginToQB(){
        QBConfigurationWrapper qbConfig = new QBConfigurationWrapper('Quickbooks');
        OAuth2 auth2 = new OAuth2(qbConfig);
        String scope = 'com.intuit.quickbooks.accounting';
        String state = 'Quickbooks';
        return auth2.authorize(scope, state);
        
    }
    
    
    public void handleOAuthCallback2(){
        Map<String, String> authorizationParametersMap = ApexPages.currentPage().getParameters();
        
        // check if verification callback
        if( authorizationParametersMap.containsKey('state') && 
           authorizationParametersMap.containsKey('code') && 
           authorizationParametersMap.containsKey('realmId')){
               isRedirected = TRUE;
               OAuth2 auth2;
               if(authorizationParametersMap.get('state') == 'Quickbooks2'){
                    currentQBOrg = authorizationParametersMap.get('state');                   
               }
               QBConfigurationWrapper qbConfig = new QBConfigurationWrapper('Quickbooks2');
               auth2 = new OAuth2(qbConfig);
               
               if(auth2 != null){
                   // Get Access Token
                   Response response = auth2.getAccessToken(authorizationParametersMap.get('state'), 
                                                            authorizationParametersMap.get('code'),
                                                            authorizationParametersMap.get('realmId'));
                   
                   if(response.Success){
                       OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                       authTokenResponse.realm_id = authorizationParametersMap.get('code');
                       
                       // Update Access Token, refresh token, access token expiry, refresh token expiry
                       QBConfigurationWrapper.saveTokens(authTokenResponse,'Quickbooks2');
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Authorized Successfully!'));
                   }else{
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,response.Message));
                   }
               }
           }else{
               // no call back redirect
               isRedirected = FALSE;
           }
    }
    
     /*
* @Purpose      : Quickbooks Login(Authorization redirection)
*/
    public Pagereference loginToQB2(){
        QBConfigurationWrapper qbConfig = new QBConfigurationWrapper('Quickbooks2');
        OAuth2 auth2 = new OAuth2(qbConfig);
        String scope = 'com.intuit.quickbooks.accounting';
        String state = 'Quickbooks';
        return auth2.authorize(scope, state);
    }
    
    public void enableAuth(){
        isRedirected = FALSE;
    }
    
}