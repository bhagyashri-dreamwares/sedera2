/*
 * @Purpose       :Mock response class for test class name add DwollaTransfersBatchTest
 * @Created Date  :28/12/2018
 */
@isTest
global class DwollaTrasnferBatchMockRsponse implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Location', 'https://api-sandbox.dwolla.com/transfers/74c9129b-d14a-e511-80da-0aa34a9b2388');
        response.setStatusCode(200);
        response.setBody('{ "access_token": "access-sandbox-1486021f-fbb7-4789-8b5b-fd8daa3da4c7", "item_id": "jBKRByQvX8SkVAjRXdDktD1K16yzawu15eAX7", "request_id": "YkOzNGxGBgHh400" }');
        return response;
    }
}