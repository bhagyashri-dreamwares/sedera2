/*

*/
@isTest
private class QBOAuthCallbackControllerTest{

    @testSetup
    static void createSetupData(){
        createConfig();
    }
    
    static testmethod void validate1(){
        Test.setCurrentPageReference(new PageReference('Page.QBAuthorize')); 
        System.currentPageReference().getParameters().put('state','Quickbooks');
        System.currentPageReference().getParameters().put('code','xyz');
        System.currentPageReference().getParameters().put('realmId','xyz');
        QBOAuthCallbackController obj = new QBOAuthCallbackController();
        obj.handleOAuthCallback();
        obj.loginToQB();
         
        QBConfigurationWrapper config = new QBConfigurationWrapper('Quickbooks');
        OAuth2 oAuth2Obj = new OAuth2(config);
        oAuth2Obj.refreshToken();
        HttpRequest request = new HttpRequest();
        oAuth2Obj.prepareRequest(request);
        
        obj.enableAuth();
    }
    
    static testmethod void validate2(){
        Test.setCurrentPageReference(new PageReference('Page.QBAuthorize')); 
        System.currentPageReference().getParameters().put('state','Quickbooks2');
        System.currentPageReference().getParameters().put('code','xyz');
        System.currentPageReference().getParameters().put('realmId','xyz');
        QBOAuthCallbackController obj = new QBOAuthCallbackController();
        obj.handleOAuthCallback2();
        obj.loginToQB2();
         
        QBConfigurationWrapper config = new QBConfigurationWrapper('Quickbooks2');
        OAuth2 oAuth2Obj = new OAuth2(config);
        oAuth2Obj.refreshToken();
        HttpRequest request = new HttpRequest();
        oAuth2Obj.prepareRequest(request);
        
        obj.enableAuth();
    }
    
    static void createConfig(){

        Quick_Book_App_Configuration__c setting = new Quick_Book_App_Configuration__c(
                Name = 'Quickbooks',
                Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
                Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
                Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
                Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
                Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
                Is_Sandbox__c = true,
                Production_Endpoint_Url__c = 'https://sandbox-quickbooks.api.intuit.com/v3/company/', 
                QB_Company_Id__c = '123145860463979',
                Redirect_URI__c = 'https://c.cs77.visual.force.com/apex/QBAuthorize',
                Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
                Refresh_Token_Expiry__c = DateTime.now().addDays(1),                
                Sandbox_Endpoint_Url__c = 'https://sandbox-quickbooks.api.intuit.com/v3/company/'
        );
        insert setting;
        
        setting = new Quick_Book_App_Configuration__c(
                Name = 'Quickbooks2',
                Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
                Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
                Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
                Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
                Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
                Is_Sandbox__c = true,
                Production_Endpoint_Url__c = 'https://sandbox-quickbooks.api.intuit.com/v3/company/', 
                QB_Company_Id__c = '123145860463979',
                Redirect_URI__c = 'https://c.cs77.visual.force.com/apex/QBAuthorize',
                Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
                Refresh_Token_Expiry__c = DateTime.now().addDays(1),                
                Sandbox_Endpoint_Url__c = 'https://sandbox-quickbooks.api.intuit.com/v3/company/'
        );
        insert setting;
        
        /*QB_Sync_Data__c syncData = new QB_Sync_Data__c( Name = 'Test Object');
        insert syncData;*/
    
    }
}