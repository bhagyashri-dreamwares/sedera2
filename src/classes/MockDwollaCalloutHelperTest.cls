/* 
* @purpose : Mock HttpResponse ganerator for DwollaCalloutHelperTest, CalloutHelperTest test classes.
*/
@isTest
global class MockDwollaCalloutHelperTest implements HttpCalloutMock  {
    integer flag = 0;
    Boolean isGetAccessToken;
    
    //constructor to set the flag value based on condition
    global MockDwollaCalloutHelperTest(integer inputflag){
        //based on flag value the response will send.
        this.flag = inputflag;   
    }
    
    public MockDwollaCalloutHelperTest(integer inputflag, Boolean isAccessToken) {
        this.flag = inputflag; 
		this.isGetAccessToken = isAccessToken;
    }

    
    global HTTPResponse respond(HTTPRequest req) {
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        system.debug('MockDwollaCalloutHelperTest called'+ isGetAccessToken);
        if(flag == 1){
            //flag value 1 for setStatusCode = 00.
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }');
            res.setStatusCode(200);  
        }
        if(flag == 0){
            //flag value 0 for setStatusCode = 201.
            res.setHeader('Content-Type', 'application/json');
            res.setHeader('Location', 'test setheader customers/ test set header');
            res.setBody('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }');
            res.setStatusCode(201);
            //res.setStatus('true');
        }
        if(flag == 2){
            res.setHeader('Location', 'beneficial-owners/3213213');
            res.setStatusCode(201);
        }
        if(flag == NULL){
            //flage = NULL set response as NULL. 
            res.setStatusCode(404);
        }
        system.debug('http res'+res);
        return res;
    }
}