/**
 * Purpose : Class to generate mock response for http callout
*/
@isTest    
  
public class MockResponseForQBAccountSyncPush implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        if( req.getEndpoint() == 'https://oauth.platform.intuit.com/oauth2/v1/token' ) {
            res.setBody('{"refresh_token":"refresh_tokenhjbhsbvhbsdvhjbdsvh","token_type":"token_typedgdd","access_token":"access_tokendgdd","expires_in":100,"x_refresh_token_expires_in":100,"realm_id":"realm_iddgdd"}');
        }
        else{
            
            res.setBody('{"BatchItemResponse":[{"Customer":{"Taxable":true,"BillAddr":{"Id":"110","Line1":"4950 SE 216th Ave","City":"Morriston","Country":"United States","CountrySubDivisionCode":"Florida","PostalCode":"32668"},"ShipAddr":{"Id":"267","Line1":"4950 SE 216th Ave","City":"Morriston","Country":"United States","CountrySubDivisionCode":"Florida","PostalCode":"32668"},"Notes":"from sf","Job":false,"BillWithParent":false,"Balance":2783.48,"BalanceWithJobs":2783.48,"CurrencyRef":{"value":"USD","name":"United States Dollar"},"PreferredDeliveryMethod":"None","domain":"QBO","sparse":false,"Id":"80","SyncToken":"13","MetaData":{"CreateTime":"2017-09-14T22:43:07-07:00","LastUpdatedTime":"2017-10-16T07:15:32-07:00"},"FullyQualifiedName":"Brad Pitt","CompanyName":"Brad Pitt","DisplayName":"Brad Pitt","PrintOnCheckName":"Brad Pitt","Active":true,"Fax":{"FreeFormNumber":"1234567890"},"DefaultTaxCodeRef":{"value":"2"}},"bId":"bid11"}],"time":"2017-10-16T07:15:32.859-07:00"}');
        }        
        res.setStatusCode(200);
        return res;
    }    
}