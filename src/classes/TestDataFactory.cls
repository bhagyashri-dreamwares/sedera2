@Istest
public class TestDataFactory {


    Public static List <Account> Create_Account_Of_Plan_type(Integer Numer_Of_Accounts, String Name, Boolean Isinsert) {
        List <Account> Accounts_To_Be_Inserted = new List <Account> ();
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Plan').getRecordTypeId();

        for (integer i = 0; i <Numer_Of_Accounts; i++) {
            Account a = new Account(name = Name, recordtypeid = devRecordTypeId);
            Accounts_To_Be_Inserted.add(a);
        }
        if (Accounts_To_Be_Inserted.size()> 0) {
            If(IsInsert == true) {
                insert Accounts_To_Be_Inserted;
                return Accounts_To_Be_Inserted;

            }
            else {
                return Accounts_To_Be_Inserted;

            }

        } else {
            return null;
        }

    }



    Public static List <Account> Create_Account_Of_Member_type(Integer Numer_Of_Accounts, Boolean IsInsert) {
        List <Account> Accounts_To_Be_Inserted = new List <Account> ();
        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        
        for (integer i = 0; i <Numer_Of_Accounts; i++) {
            Account a = new Account(name = 'Member Test ' + i + 1,Enrollment_Date__c = myTestdate,iua_chosen__c = 500, recordtypeid = devRecordTypeId, Last_Name__c = 'Member Test ' + i + 1);
            Accounts_To_Be_Inserted.add(a);
        }
        if (Accounts_To_Be_Inserted.size()> 0) {
            if (isInsert) {
                insert Accounts_To_Be_Inserted;
                return Accounts_To_Be_Inserted;
            } else {
                return Accounts_To_Be_Inserted;

            }

        } else {
            return null;
        }

    }


    Public static List <Account> Create_Account_Of_Employer_type(Integer Numer_Of_Accounts, Boolean IsInsert) {
        List <Account> Accounts_To_Be_Inserted = new List <Account> ();

        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        for (integer i = 0; i <Numer_Of_Accounts; i++) {
            Account a = new Account(name = 'Employer Test ' + i + 1,Employer_SMS_Preferences__c='All Communications', recordtypeid = devRecordTypeId,Default_Product__c='Sedera Select',Text_Account_Creation__c
='Test',Text_Application_Reminder__c='Test',Text_HCS_Completion__c='Test',Text_Principles_Reminder__c='Test',MS_Pricing__c='New Pricing',Available_IUA_Options__c='500; 1000; 2500',Open_Enrollment_Starts__c=date.Today()-3,Open_Enrollment_ends__c=date.Today()+5);
            Accounts_To_Be_Inserted.add(a);
        }
        if (Accounts_To_Be_Inserted.size()> 0) {
            if (IsInsert == true) {
                insert Accounts_To_Be_Inserted;
                return Accounts_To_Be_Inserted;
            } else {
                return Accounts_To_Be_Inserted;
            }

        } else {
            return null;
        }
    }
    
    
     Public static List <Account> Create_Account_Of_Affiliate_type(Integer Numer_Of_Accounts, Boolean IsInsert) {
        List <Account> Accounts_To_Be_Inserted = new List <Account> ();

        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Affiliate').getRecordTypeId();
        for (integer i = 0; i <Numer_Of_Accounts; i++) {
            Account a = new Account(name = 'Affiliate Test ' + i + 1,Affiliate_type__c='Referral Affiliate',Primary_email_Address__c='test@test.com',recordTypeId=devRecordTypeId ,last_name__c='Test',first_name__C='Test');
            Accounts_To_Be_Inserted.add(a);
        }
        if (Accounts_To_Be_Inserted.size()> 0) {
            if (IsInsert == true) {
                insert Accounts_To_Be_Inserted;
                return Accounts_To_Be_Inserted;
            } else {
                return Accounts_To_Be_Inserted;
            }

        } else {
            return null;
        }
    }
    
    Public static List <Account> Create_Account_Of_Sub_Employer_type(String EmpId, Boolean IsInsert) {
        List <Account> Accounts_To_Be_Inserted = new List <Account> ();

        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
            Account a = new Account(name = 'Employer Test ', recordtypeid = devRecordTypeId,Parent_employer_Account__c=EmpId,Sub_Employer_SMS_Preference__c=true,Text_Account_Creation__c
='Test',Text_Application_Reminder__c='Test',Text_HCS_Completion__c='Test',Text_Principles_Reminder__c='Test');
            Accounts_To_Be_Inserted.add(a);
        
        if (Accounts_To_Be_Inserted.size()> 0) {
            if (IsInsert == true) {
                insert Accounts_To_Be_Inserted;
                return Accounts_To_Be_Inserted;
            } else {
                return Accounts_To_Be_Inserted;
            }

        } else {
            return null;
        }
    }


    Public static List <Contact> Create_Contact_Of_Sedera_Health_Contact_Type(Integer Numer_Of_Contacts, Boolean IsInsert) {
        List <Contact> Contacts_To_Be_Inserted = new List <Contact> ();

        Id devRecordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Sedera Health Contact').getRecordTypeId();
        for (integer i = 0; i <Numer_Of_Contacts; i++) {
            contact c = new contact(Work_Phone__c ='11234567890',mobile_phone__c ='11234567890',home_phone__c ='11234567890',lastname = 'Sedera Health Contact Test ' + i + 1, recordtypeid = devRecordTypeId);
            Contacts_To_Be_Inserted.add(c);
        }
        if (Contacts_To_Be_Inserted.size()> 0) {
            if (Isinsert) {
                insert Contacts_To_Be_Inserted;
                return Contacts_To_Be_Inserted;
            } else {
                return Contacts_To_Be_Inserted;
            }
        } else {
            return null;
        }
    }



    Public static List <Case> Create_Cases(Integer Numer_Of_Cases, Id ContactId, Boolean IsInsert) {

        List <Case> Cases_To_Be_Inserted = new List <Case> ();

        for (integer i = 0; i <Numer_Of_Cases; i++) {
            Case c = new Case(Subject = 'Case Test ' + i + 1, Contactid = ContactId, Type = 'MEC');
            Cases_To_Be_Inserted.add(c);
        }
        if (Cases_To_Be_Inserted.size()> 0) {
            if (isInsert == true) {
                insert Cases_To_Be_Inserted;
                return Cases_To_Be_Inserted;
            } else {
                return Cases_To_Be_Inserted;
            }
        } else {
            return null;
        }




    }


    Public static Map <Id, List <Case>> Create_Cases(Integer Numer_Of_Cases, set <Id> ContactId, Boolean isInsert) {

        List <Case> Cases_To_Be_Inserted = new List <Case> ();

        Map <Id, list <Case>> Map_Of_Cases_And_Contacts = new Map <Id, list <Case>> ();

        for (Id id: ContactId) {
            for (integer i = 0; i <Numer_Of_Cases; i++) {
                Case c = new Case(Subject = 'Case Test ' + i + 1, Contactid = Id, Type = 'MEC');
                Cases_To_Be_Inserted.add(c);
            }
        }



        if (Cases_To_Be_Inserted.size()> 0) {
            if (isInsert == true) {
                insert Cases_To_Be_Inserted;
            }
            for (Case c: Cases_To_Be_Inserted) {
                If(Map_Of_Cases_And_Contacts.containsKey(c.ContactId)) {
                    List <Case> Case_List = Map_Of_Cases_And_Contacts.get(c.ContactId);
                    Case_List.add(c);
                    Map_Of_Cases_And_Contacts.put(c.contactId, Case_List);
                }
                else {
                    List <Case> case_List = new List <Case> ();
                    case_List.add(c);
                    Map_Of_Cases_And_Contacts.put(c.contactId, Case_List);

                }

            }

            If(Map_Of_Cases_And_Contacts.Keyset().size()> 0) {
                return Map_Of_Cases_And_Contacts;

            }




        }

        return null;
    }




    /***/




    Public static List <Bill__c> Create_Bills(Integer Numer_Of_Bills, Id CaseId, Boolean Isinsert) {

        List <Bill__c> Bills_To_Be_Inserted = new List <Bill__c> ();

        for (integer i = 0; i <Numer_Of_Bills; i++) {
            bill__c b = new bill__c(case__c = CaseId, status__c = 'Received-Not to be Negotiated',Discount__c=111);
            Bills_To_Be_Inserted.add(b);
        }
        if (Bills_To_Be_Inserted.size()> 0) {
            if (isinsert == true) {
                insert Bills_To_Be_Inserted;
            }
            return Bills_To_Be_Inserted;

        } else {
            return null;
        }




    }


    Public static Map <Id, List <Bill__c>> Create_Bills(Integer Numer_Of_Bills, set <Id> CaseId, Boolean isinsert) {

        List <Bill__c> Bills_To_Be_Inserted = new List <Bill__c> ();

        Map <Id, list <Bill__c>> Map_Of_Bills_And_Cases = new Map <Id, list <Bill__c>> ();

        for (Id id: CaseId) {
            for (integer i = 0; i <Numer_Of_Bills; i++) {
                bill__c b = new bill__c(case__c = Id, status__c = 'Received-Not to be Negotiated');
                bills_To_Be_Inserted.add(b);
            }
        }



        if (Bills_To_Be_Inserted.size()> 0) {
            if (isinsert == true) {
                insert Bills_To_Be_Inserted;
            }
            for (Bill__c b: Bills_To_Be_Inserted) {
                If(Map_Of_Bills_And_Cases.containsKey(b.Case__c)) {
                    List <Bill__c> Bills_List = Map_Of_Bills_And_Cases.get(b.Case__c);
                    Bills_List.add(b);
                    Map_Of_Bills_And_Cases.put(b.case__c, Bills_List);
                }
                else {
                    List <Bill__c> Bills_List = new List <Bill__c> ();
                    Bills_List.add(b);
                    Map_Of_Bills_And_Cases.put(b.case__c, Bills_List);

                }

            }

            If(Map_Of_Bills_And_Cases.Keyset().size()> 0) {
                return Map_Of_Bills_And_Cases;

            }




        }

        return null;
    }


    public static void CreateProducts(string TestProductCode) {

        Product2 producto = new Product2();
        producto.Name = 'test';
        producto.productCode = TestProductCode;
        producto.isActive = true;

        insert producto;

    }

    public static List <MEC_Product__c> Create_Mec_Product(Integer NumberOfMec, Boolean IsInsert) {
        List <MEC_Product__c> MecsToinsert = new List <MEC_Product__c> ();
        for (integer i = 0; i<NumberOfMec; i++) {
            MEC_Product__c x = new MEC_Product__c(name='ASI-Pilot', Discount_Tier__c = 'T1');
            MecsToinsert.add(x);
        }
        if (IsInsert) {
            insert MecsToinsert;
        }

        return MecsToinsert;

    }

    public static List <AccountMECAssociation__c> Create_Mec_Assosciation(Id EmployerId,Id MecProductId,Integer NumberOfMec, Boolean IsInsert) {
        List <AccountMECAssociation__c> MecsToinsert = new List <AccountMECAssociation__c> ();
        for (integer i = 0; i<NumberOfMec; i++) {
            AccountMECAssociation__c x = new AccountMECAssociation__c(Employer_Account__c=EmployerId,MEC_Product__c=MecProductId, Default_MEC_Product__c= false);
            MecsToinsert.add(x);
        }
        if (IsInsert) {
            insert MecsToinsert;
        }

        return MecsToinsert;

    }


     public static List <Pricing__c> Create_Pricing(Id EmployerId,Integer NumberOfPricing, Boolean IsInsert) {
        List <Pricing__c> PricingList = new List <Pricing__c> ();
        
        for (integer i = 0; i<NumberOfPricing; i++) {
           Pricing__c x=new Pricing__c(Employer_Name__c=EmployerId,EC_Tier__c=1.90, EF_Tier__c=2.90, EO_Tier__c=1.00, ES_Tier__c=2.00, Is_Base_Price_Record__c=true, Liberty_Rx__c=45.00, MCS_Additional__c=1.05, MCS_Base__c=168.00, MSEC__c=100.00, MSEF__c=150.00, MSEO__c=55.00, MSES__c=100.00, Name='Default Select Pricing', PSEC__c=49.82, PSEF__c=68.23, PSEO__c=31.41, PSES__c=49.82, RecordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Select Pricing').getRecordTypeId(),  Teladoc_Reduction__c=-4.35, Tobacco_Use__c=75.00, U30IUA500__c=0.70, U30IUA1000__c=0.65, U30IUA1500__c=0.55, U30IUA2500__c=0.45, U30IUA5000__c=0.25, U40IUA500__c=0.85, U40IUA1000__c=0.80, U40IUA1500__c=0.70, U40IUA2500__c=0.52, U40IUA5000__c=0.40, U50IUA500__c=1.00, U50IUA1000__c=0.85, U50IUA1500__c=0.75, U50IUA2500__c=0.62, U50IUA5000__c=0.49, U60IUA500__c=1.30, U60IUA1000__c=1.10, U60IUA1500__c=1.00, U60IUA2500__c=0.87, U60IUA5000__c=0.62, U65IUA500__c=2.65, U65IUA1000__c=2.30, U65IUA1500__c=2.15, U65IUA2500__c=1.92, U65IUA5000__c=1.49, X2nd_MD_Reduction__c=-6.00, X500T2__c=0.920, X500T3__c=0.840, X1000T2__c=0.925, X1000T3__c=0.850, X1500T2__c=0.925, X1500T3__c=0.850, X2500T2__c=0.925, X2500T3__c=0.850, X5000T2__c=0.913, X5000T3__c=0.825,X1500NewO30T1__c=6,X1500NewU30T1__c=7,Old1000T3__c=8,Old500T3__c=9,U30EOIUA500__c=3, U30ECIUA500__c=3, U30EFIUA500__c=3,U30ESIUA500__c=3, O30EOIUA500__c=3, O30ESIUA500__c=3, O30ECIUA500__c=3, O30EFIUA500__c=3, U30EOIUA1000__c=3, U30ESIUA1000__c=3, U30ECIUA1000__c=3, U30EFIUA1000__c=3, O30EOIUA1000__c=3, O30ESIUA1000__c=3, O30ECIUA1000__c=3, O30EFIUA1000__c=9);
            PricingList.add(x);
        }
        if (IsInsert) {
            insert PricingList;
        }

        return PricingList;

    }


}