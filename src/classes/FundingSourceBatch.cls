public class FundingSourceBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

    public List<Id> recordIdList;
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public DwollaAPIConfiguration__c config;
    public Boolean isAccessTokenUpdated = false;
    public List<Dwolla_Funding_Source__c> fundingSourceList = new List<Dwolla_Funding_Source__c>();
    public List<Log__c> logList = new List<Log__c>();
    
    public fundingSourceBatch (Set<Id> recordIdSet){
      
        this.recordIdList = new List<Id>();
        this.recordIdList.addAll(recordIdSet);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'SELECT Id, Name, Active__c, Bank_Account_Number__c, Dwolla_Funding_Source_ID__c, Funding_Account_Type__c,'+
                       'Dwolla_Funding_Source_Name__c, Employer_Account__c, Employer_Account__r.Dwolla_Id__c,'+
                       'Routing_Number__c, Sync_to_Dwolla__c, Type__c FROM Dwolla_Funding_Source__c WHERE Id IN :recordIdList';
        return Database.getQueryLocator(query);

    }
    
    public DwollaAPIConfiguration__c getLiveToken(DwollaAPIConfiguration__c config){
        
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        // Get Access Token
        Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
            'client_secret' => config.Secret_Key__c,
            'grant_type' => 'client_credentials'};
                
        Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
            Response response;
                                                                 
        response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
        
        System.debug('response 111  ::::'+response );
        
        if(response != null){
            if(response.Success){
                
                OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                
                config.Access_Token__c = authTokenResponse.access_token;
                config.Access_Token_Expiry__c = Datetime.now().addMinutes(55);
                
                return config;
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+response.Message;
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                    '\n\n headerParameterMap :\n'+headerParameterMap;
                errorWrapper.source = 'DwollaCustomerBatch.getLiveToken';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
            }
        }
        return null;
    }
    
    public void execute(Database.BatchableContext BC, List<Dwolla_Funding_Source__c> scope){
      
        Boolean isTokenLive = false;
        DwollaCalloutHelper dwollaCalloutHelper = new DwollaCalloutHelper();
        
        if(dwollaCalloutHelper.config.Access_Token_Expiry__c == null || 
           dwollaCalloutHelper.config.Access_Token_Expiry__c <= DateTime.now()){
               
           dwollaCalloutHelper.config = getLiveToken(dwollaCalloutHelper.config);
           
           if(dwollaCalloutHelper.config != null){
               isTokenLive = true;
               isAccessTokenUpdated = true;
               config = dwollaCalloutHelper.config;
           }
        }else{
           config = dwollaCalloutHelper.config;
           isTokenLive = true;
        }
        
        if(isTokenLive){
        
            for(Dwolla_Funding_Source__c fundingSource :scope){
            
                String fundingSourceId = FundingSourceHandler.createDwollaFundingSource('', config.Access_Token__c, 
                                          fundingSource.Routing_Number__c, fundingSource.Bank_Account_Number__c, 
                                          fundingSource.Funding_Account_Type__c, config, fundingSource.Employer_Account__r.Dwolla_Id__c,
                                          fundingSource.Name, errorList, LogList);
                if(String.isNotBlank(fundingSourceId)){
                    fundingSource.Dwolla_Funding_Source_ID__c = fundingSourceId;
                    fundingSourceList.add(fundingSource);    
                }
            }  
        }
    }
    
    public void finish(Database.BatchableContext BC){
       
         if(isAccessTokenUpdated && config != null){
            DwollaOAuthCallbackController.saveConfiguration(config);
        }
        
        if(!errorList.isEmpty()){
            DwollaErrorResponseHandler.saveDwollaErrorResponse(errorList);
        }  
        
        if(!fundingSourceList.isEmpty()){
            FundingSourceHandler.saveFundingSouce(fundingSourceList, logList);
        }
        if(!logList.isEmpty()){
           LogUtil.saveLogs(logList);
        }         
    }
}