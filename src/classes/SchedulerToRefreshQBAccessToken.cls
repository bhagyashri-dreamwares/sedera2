global class SchedulerToRefreshQBAccessToken implements Schedulable {
   public static void schedule(){
        SchedulerToRefreshQBAccessToken qbJob = new SchedulerToRefreshQBAccessToken();
        if(!Test.isRunningTest()){
             if(!Test.isRunningTest()){
                 system.schedule('JobToRefreshQBAccessToken1','0 0 * * * ?', qbJob);
                 system.schedule('JobToRefreshQBAccessToken2','0 30 * * * ?', qbJob);
             }
        }
   }    
   global void execute(SchedulableContext SC) {
        initiateProcess();
   }
   @future(callout=true) 
   global static void initiateProcess(){
       QBConfigurationWrapper config = new QBConfigurationWrapper('Quickbooks');
       OAuth2 oAuth2Obj = new OAuth2(config);
       Response response1 = oAuth2Obj.refreshToken();
       
      /* config = new QBConfigurationWrapper('Quickbooks2');
       OAuth2 oAuth2Obj2 = new OAuth2(config);
       Response response2 = oAuth2Obj2.refreshToken();       */
   }
}