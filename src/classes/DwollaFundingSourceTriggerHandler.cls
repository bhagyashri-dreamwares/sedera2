/**
 * @ClassName    : DwollaFundingSourceTriggerHandler
 * @Purpose      : Handling Functionality of DwollaFundingSourceTrigger
 * @CreatedDate  : 21/09/2018
 * @ModifiedDate : 28/09/2018
 * @author		 : VaibhavN
*/

public class DwollaFundingSourceTriggerHandler {    
    /*
     * @MethodName   : Deactivate the 
     * @Purpose      : To d
     * @Parameters   : New Records from Trigger
     * @Return       : -
     * @CreatedDate  : 21/09/2018
     * @ModifiedDate : 28/09/2018
    */
    
    public static void deactivateCurrentActive(List<Dwolla_Funding_Source__c> DFSList){
        try{
            Set<ID> accountIDList = new Set<ID>();
            Dwolla_Funding_Source__c temp;
            for(Dwolla_Funding_Source__c dfs:DFSList){
                accountIDList.add(dfs.Employer_Account__c);
            }
            
            List<Dwolla_Funding_Source__c> DFSListAll = [SELECT ID, Active__c, Employer_Account__c, Type__c
                                                         FROM Dwolla_Funding_Source__c
                                                         WHERE Employer_Account__c IN :accountIDList AND Active__c=true];
            
            Map<ID,Dwolla_Funding_Source__c> oldDFSList = new  Map<ID,Dwolla_Funding_Source__c>();
            Map<ID, Map<String, Dwolla_Funding_Source__c>> accountToDFSMap = new Map<ID, Map<String, Dwolla_Funding_Source__c>>();
            Map<ID, Map<String, Dwolla_Funding_Source__c>> newaccountToDFSMap = new Map<ID, Map<String, Dwolla_Funding_Source__c>>();
            
            for(Dwolla_Funding_Source__c dfs:DFSListAll){
                if(!accountToDFSMap.containskey(dfs.Employer_Account__c)) {
                    accountToDFSMap.put(dfs.Employer_Account__c , new Map<String , Dwolla_Funding_Source__c>());
                }
                accountToDFSMap.get(dfs.Employer_Account__c).put(dfs.Type__c,dfs);
                
            }
            System.debug(accountToDFSMap);
            
            for(Dwolla_Funding_Source__c dfs:DFSList){
                if(dfs.Active__c==false||dfs.Type__c==null||dfs.Type__c==''){
                    dfs.Active__c=false;
                    dfs.Type__c=null;
                }
                
                if(!newaccountToDFSMap.containsKey(dfs.Employer_Account__c)) {
                    newaccountToDFSMap.put(dfs.Employer_Account__c , new map<String , Dwolla_Funding_Source__c>());
                    newaccountToDFSMap.get(dfs.Employer_Account__c).put(dfs.Type__c , dfs);
                    
                    if(accountToDFSMap.containsKey(dfs.Employer_Account__c) && dfs.Active__c== true){
                        if(accountToDFSMap.get(dfs.Employer_Account__c).containsKey(dfs.Type__c)){
                            temp = accountToDFSMap.get(dfs.Employer_Account__c).get(dfs.Type__c);
                        }
                    }
                }
                else{
                    if(!newaccountToDFSMap.get(dfs.Employer_Account__c).containsKey(dfs.Type__c)) {
                        newaccountToDFSMap.get(dfs.Employer_Account__c).put(dfs.Type__c,dfs); 
                        
                        if(accountToDFSMap.containsKey(dfs.Employer_Account__c) && dfs.Active__c== true && dfs.Type__c != null){
                            if(accountToDFSMap.get(dfs.Employer_Account__c).containsKey(dfs.Type__c)){
                                temp = accountToDFSMap.get(dfs.Employer_Account__c).get(dfs.Type__c);
                            }
                        }
                    } else{
                        newaccountToDFSMap.get(dfs.Employer_Account__c).get(dfs.Type__c).Active__c =false;
                        dfs.Active__c=true;
                        newaccountToDFSMap.get(dfs.Employer_Account__c).put(dfs.Type__c,dfs); 
                        
                        if(accountToDFSMap.containsKey(dfs.Employer_Account__c) && dfs.Active__c== true && dfs.Type__c != null){
                            if(accountToDFSMap.get(dfs.Employer_Account__c).containsKey(dfs.Type__c)){
                                temp = accountToDFSMap.get(dfs.Employer_Account__c).get(dfs.Type__c);
                            }
                        }
                    }
                }
                if(accountToDFSMap.containsKey(dfs.Employer_Account__c) && dfs.Active__c== true){
                    if(accountToDFSMap.get(dfs.Employer_Account__c).containsKey(dfs.Type__c)){
                        temp.Active__c = false;
                        accountToDFSMap.get(dfs.Employer_Account__c).get(dfs.Type__c).Active__c = false;
                        oldDFSList.put(temp.Id,temp);
                        System.debug(accountToDFSMap.get(dfs.Employer_Account__c).get(dfs.Type__c).ID);
                    }
                }
            }
            update oldDFSList.values();
        }
        catch(Exception exp){
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaFundingSourceTriggerHandler.deactivateRest', 
                                                                  exp, 
                                                                  '[DwollaFundingSource Records] : '+DFSList, 
                                                                  '	[Query] : SELECT ID, Active__c, Employer_Account__c, Type__c'+
                                                         		  ' FROM Dwolla_Funding_Source__c'+
                                                                  ' WHERE Employer_Account__c IN :accountIDList AND Active__c=true]',
                                                                  'Error'
                                                                )
                                              )});
        }
    } 
}