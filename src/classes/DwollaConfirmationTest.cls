/**
* @Purpose: test class for DwollaConfirmationController
* @Date: 04/12/2018
*/
@isTest
public class DwollaConfirmationTest{

    /**
    * To intialise data
    */
    @testSetup
    public static void initData(){
    
        Account account = new Account();
        account.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        account.Name = 'Test';
        account.Invoice_Email_Address__c = 'test@test.com';
        account.Dwolla_ID__c = '123456987';
        account.Create_Dwolla_Customer__c = true;
        account.Send_Plaid_Link__c = true;
        
        insert account;
        
        // insert Dwolla_Datasheet record
        Dwolla_Datasheet__c dwollaSheet = new Dwolla_Datasheet__c();
        dwollaSheet.Account__c = account.Id;
        dwollasheet.SSN__c = '111-11-1111';
        dwollasheet.EIN__c= '11-1111111';
        dwollasheet.Controller_SSN__c = '222-22-2222';
        
        insert dwollaSheet;
        
        // inser Funding source
        Dwolla_Funding_Source__c fundingSource = new Dwolla_Funding_Source__c();
        fundingSource.Active__c = true;
        fundingSource.Employer_Account__c = account.Id;
        fundingSource.Type__c = 'Customer';
        
        insert fundingSource;
    }
    
    /**
    * To test CustomerConfirmation method
    */
    public static testMethod void testCustomerConfirmation(){
        
        List<Account> accountList = [SELECT Id, Create_Dwolla_Customer__c, Name
                                     FROM Account];
        List<Dwolla_Funding_Source__c> fundingsourceList = [SELECT Id
                                                            FROM Dwolla_Funding_Source__c];
        
        Test.startTest();
        
        PageReference pageRef = Page.DwollaConfirmation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',accountList[0].id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        DwollaConfirmationController controller = new DwollaConfirmationController(sc);
        controller.isUserAcceptTerms = true;
        controller.customerConfirmation();
        controller.fundingSourceId = fundingsourceList[0].Id;
        controller.updateFundingSource();
        controller.checkTermsAndPlicy();
        
        Test.stopTest();
        
        List<Account> updatedAccountList = [SELECT Id, Is_Terms_And_Policy_Accepted__c, Name
                                            FROM Account];
        System.assertEquals(true, updatedAccountList[0].Is_Terms_And_Policy_Accepted__c);
    }
}