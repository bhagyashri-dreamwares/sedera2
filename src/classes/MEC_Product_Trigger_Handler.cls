/*

Test class : MEC_Product_Trigger_Test 

*/
public class MEC_Product_Trigger_Handler{

  public static void handleAfterUpdate(Map<id,Mec_Product__c> mecNewMap,Map<id,Mec_Product__c> mecOldMap){
  
   /*** All related member Accounts MCS product code and amount is recalculated if there is any change in MEC's discount Tier because of its use in member's product code ***/
    MEC_Product_Helper_Class.CalandUpdateMemDisTier(mecNewMap.values(),mecOldMap);
 
 }

}