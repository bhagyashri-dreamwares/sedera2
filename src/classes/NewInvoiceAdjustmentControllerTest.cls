/*
 * @Purpose: Test class for NewInvoiceAdjustmentController
 * @Created Date: 19/4/2019
 */
@isTest
public class NewInvoiceAdjustmentControllerTest {
    
    @TestSetup
    public static void createTestData() {
        
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        
        update PriceList[0];
        
        List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
        AccessPricing[0].name='Default Access Pricing';
        AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        
        insert AccessPricing[0];
        
        // Create Account
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account accountObj = new Account(RecordtypeId = recordtypeId,
                                         Name = 'Test Account',
                                         Default_Product__c = 'Sedera Select');
        insert accountObj;
        
        // Create Invoice
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        for(Integer index = 0; index < 5; index++){
        	invoiceList.add(new Invoice__c(Account__c = accountObj.Id, 
                                            Invoice_Date__c = date.parse('12/1/2018'),
                                            Invoice_Due_Date__c = Date.today().addMonths(1).toStartofMonth(),
                                            Paid_Amount__c = 20000, 
                                            Invoice_Number__c = 'I111'+index, 
                                            Submission_Status__c = 'Approved'));    
        }
        insert invoiceList;
        
        
        // Create Invoice Adjustment
        List<Invoice_Adjustment__c> invocieAdjustmentList = new List<Invoice_Adjustment__c>();
        for(Integer index = 0; index < 5; index++){
        	invocieAdjustmentList.add(new Invoice_Adjustment__c (Invoice__c = invoiceList[0].id,
                                                                             Employer_Account__c = accountObj.id,
                                                                             Amount__c =  10,
                                                                             mcs_bucket__c  = 'MCS_new',
                                                                             Description_of_Change__c = 'MCS New Adjustment',
                                                                             Date_Change_Requested__c = date.parse('11/19/2018'),
                                                                             Invoice_Period__c = date.parse('12/1/2018')));    
        }
        insert invocieAdjustmentList;                                                                
    }
    
    /*
     * @Purpose: Tset the fucntionality for fetchInvoiceAdjustments
     */
     public static testMethod void fetchInvoiceAdjustmentsTest() {
         
         Account account = [SELECT Id FROM Account LIMIT 1];
         List<Invoice__c> invoiceList = [SELECT Id, Name FROM Invoice__c];
         List<Invoice_Adjustment__c> invocieAdjustmentList = [SELECT Id FROM Invoice_Adjustment__c];
         
         Test.startTest();
             NewInvoiceAdjustmentController newInvoiceAdjustmentControllerObj = new NewInvoiceAdjustmentController();
             Response response = NewInvoiceAdjustmentController.fetchInvoiceAdjustments(account.Id, String.valueOf(date.parse('12/1/2018')), '');
             
             newInvoiceAdjustmentControllerObj.selectedAccountId = account.id;
             newInvoiceAdjustmentControllerObj.selectedInvoiceDate = Date.valueOf(date.parse('12/1/2018'));
             newInvoiceAdjustmentControllerObj.selectedTotalAdjustment = 0;
         Test.stopTest();
         
         List<NewInvoiceAdjustmentController.InvoiceWrapper> invoiceWrapperList = (List<NewInvoiceAdjustmentController.InvoiceWrapper>)response.Data;
         System.assertEquals(5, invoiceWrapperList.size());
     }
    
    /*
     * @Purpose: Tset the fucntionality for updateInvoiceAdjustment
     */
     public static testMethod void updateInvoiceAdjustmentTest() {
         
         Account account = [SELECT Id FROM Account LIMIT 1];
         List<Invoice__c> invoiceList = [SELECT Id, Name FROM Invoice__c];
         List<Invoice_Adjustment__c> invocieAdjustmentList = [SELECT Id FROM Invoice_Adjustment__c];
         
         Test.startTest();
             String jsonString = '[{"id":"0_'+invocieAdjustmentList[0].Id+'","invoiceName":"'+invoiceList[0].Id+'","description":"testing 1","dateChangedRequested":"2009-12-27","mscBucket":"None","amount":"12"}]';
             Response response = NewInvoiceAdjustmentController.updateInvoiceAdjustment(jsonString, new List<String>{}, account.Id);
         	 System.assertEquals(true, response.Success);	
         
             jsonString = '[{"id":"4_checkbox","invoiceName":"'+invoiceList[0].Id+'","description":"testing 1","dateChangedRequested":"2009-12-27","mscBucket":"None","amount":"12"}]';
         	 String strId = '0_'+invocieAdjustmentList[1].Id;
             response = NewInvoiceAdjustmentController.updateInvoiceAdjustment(jsonString, new List<String>{strId}, account.Id);
            System.assertEquals(true, response.Success);
         Test.stopTest();
         
     }
}