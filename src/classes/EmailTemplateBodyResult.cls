public class EmailTemplateBodyResult {
  @InvocableMethod(label='Fill Email Template' description='Fills the email template')
  public static List<String> fillEmailTemplate(List<emailTemplateInput> values) {

    //split the list into separate values
    //find the template and execute it
    //return the result
    string templateName;
    string templateResult;
 
    emailTemplateInput eti = values[0];   
    templateName = eti.templateName;
    
    List<string> results = new list<string>();
    List<string> bodies = new list<string>();

    for (EmailTemplate et: [select Body from EmailTemplate where name = :templateName ])
    {  
        bodies.add(et.Body); 
    }

    List<Messaging.RenderEmailTemplateBodyResult> resList = Messaging.renderEmailTemplate(eti.whoId, eti.whatId, bodies);

    templateResult = resList[0].getmergedbody();
    results.add(templateResult);

    return results;
  }
  
  public class emailTemplateInput{
      @InvocableVariable
      public Id whatId;
      
      @InvocableVariable
      public Id whoId;
      
      @InvocableVariable
      public string templateName;
      }
}