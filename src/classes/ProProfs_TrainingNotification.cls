@RestResource(urlMapping='/ProProfs/TrainingNotification')
global class ProProfs_TrainingNotification {
    
    @HttpGet
    global static void doget() {
    
    Map<String,String> params = RestContext.request.params;
    
         
    List<Contact> AffiliateCon=[Select id,ProProfs_Log__c,Relationship_To_Affiliate__c from Contact where recordType.name='Affiliate' and id=:params.get('id') limit 1];
          
    if(AffiliateCon.size()>0 && params.get('completed')=='100'){
             
       AffiliateCon[0].Training_Status__c='Training Completed- Passed';
       AffiliateCon[0].ProProfs_IsTrainingCompleted__c=true;
       
       if(!test.isRunningtest()){
        Id Agid=echosign_dev1.AgreementTemplateService.load(AffiliateCon[0].Relationship_To_Affiliate__c=='Self'?System.Label.SederaSelfAffiliateAgreementId:system.label.SederaSubAffiliateAgreementId,AffiliateCon[0].Id);
        AffiliateCon[0].AffiliateAgreementId__c=AgId;
       }
    }
    
    if(AffiliateCon.size()>0){
       
       AffiliateCon[0].Proprofs_Log__c=(AffiliateCon[0].Proprofs_Log__c==null?'':AffiliateCon[0].Proprofs_Log__c)+'\n\nCompletedDateTime: '+string.valueOf(system.now());
       for(string p:params.keySet()){
         if(p!='pincode' && p!='country' && p!='state' && p!='city' && p!='address'){
         AffiliateCon[0].Proprofs_Log__c = AffiliateCon[0].Proprofs_Log__c +'\n'+p+'   '+params.get(p);  
        }
      }  
       
       UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger=0;
       Update AffiliateCon[0];  
    
    }
    
  }
   
    
}