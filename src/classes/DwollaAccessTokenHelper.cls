public class DwollaAccessTokenHelper {
    
    public DwollaAPIConfiguration__c config;
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public Boolean isTestValidToken = false;
    
    public DwollaAccessTokenHelper(){
        this.config = getAPIConfiguration();
    }
    
     /*
    * @Purpose :get Live Token.
    */    
    public DwollaAPIConfiguration__c getLiveToken(DwollaAPIConfiguration__c config){
        
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        // Get Access Token
        Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
            'client_secret' => config.Secret_Key__c,
            'grant_type' => 'client_credentials'};
                
        Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
        Response response;
        
        if(Test.isRunningTest()){
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
            System.debug('response @@@### '+ response.Success);
            isTestValidToken = true;
            if(response.Success){
                response.Success =  true;
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(true, '', oAuthResponse); 
            }else{
                response.Success =  false;
                response  = new Response(false, 'Invalid Token', null);
            }
            
        }else{
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
        }
        if(response != null){
            if(response.Success){
                
                OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                
                config.Access_Token__c = authTokenResponse.access_token;
                config.Access_Token_Expiry__c = Datetime.now().addMinutes(45);
                
                return config;
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+response.Message;
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                    '\n\n headerParameterMap :\n'+headerParameterMap;
                errorWrapper.source = 'DwollaDocumentCalloutHelper.getLiveToken';
                DwollaErrorResponseHandler.saveDwollaErrorResponse(new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
            }
        }
        return null;
    }
    
    /**
    *@Purpose :get custom setting record
    */
    public static DwollaAPIConfiguration__c getAPIConfiguration(){
        return DwollaAPIConfiguration__c.getOrgDefaults();
    }
    
    /**
     * @Purpose : to update access Token in custom setting
     * */
    public static void updateAPIConfiguration(DwollaAPIConfiguration__c aPIConfig){
        try{
            
            update aPIConfig;    
        }catch(Exception exp){            
            System.debug('Exception :::'+exp.getMessage());
        }
    }
}