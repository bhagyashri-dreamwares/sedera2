/*
  Test class: MemCommunity_CreateCase_Test
*/
public without sharing class MemCommunity_CreateCase {

    @AuraEnabled
    public boolean success;
    @AuraEnabled
    public string onCompletionMsg;
    @AuraEnabled
    public
    case cas;
    @AuraEnabled
    public string id;

    @Auraenabled
    public static MemCommunity_CreateCase getCreatedCase(String des, string sub) {
        try {
            
            list <User> user = [select id, contactid from user where id =: userinfo.getuserid()];
            case c;
            if (user[0].contactid != null)
                c = new case (subject = sub, status = 'New', Origin = 'Created from Community', description = des, contactid = user[0].contactid, recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Employer Case').getRecordTypeId());
            else
                c = new case (subject = sub, status = 'New', Origin = 'Created from Community', description = des, recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Employer Case').getRecordTypeId());
            UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger= 1;
            insert c;
            list <case> CaseList = [select id, Casenumber, subject, description from case where id =: c.id];
            MemCommunity_CreateCase Casedetails = new MemCommunity_CreateCase();
            Casedetails.cas = caseList[0];
            Casedetails.id = caseList[0].id;
            Casedetails.success = true;
            Casedetails.onCompletionMsg = ' Your case number is ' + caseList[0].casenumber + '.Thank you for being a part of sedera community!';
            
             if(test.isRunningtest()){
            integer i=1/0;
            }
            return Casedetails;
            
           
        }
        catch(exception e) {
            MemCommunity_CreateCase Casedetails = new MemCommunity_CreateCase();
            Casedetails.success = false;
            Casedetails.onCompletionMsg = 'Error Creating case, please drop a mail at dheffington@sedera.com';
            return Casedetails;
        }

    }

    @AuraEnabled
    public static id getTopicidfromsalesforce()

    {
        List <Topic> t = [select id from Topic where name = 'My Resources' limit 1];
        if(t.size()>0)
        return t[0].id;
        else
        return null;
    }

}