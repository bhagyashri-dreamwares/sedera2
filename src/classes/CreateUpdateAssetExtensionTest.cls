@istest
public class CreateUpdateAssetExtensionTest 
{

 public static testmethod void TestMethod_for_extension()
 {
  Pagereference testPage=new pagereference('/apex/CreateAndUpdateAsset');
  test.setcurrentpage(testpage);
  
 
 
  UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        List < Account > PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        Account emp = EmployerAccountList[0];
        emp.parentid = PlanAccountList[0].id;
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        update emp;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(1, false);
        for(integer i=0;i<MemberAccountList.size();i++)
         {
           MemberAccountList[i].Account_Employer_name__C=emp.id;
         
         }
         
         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
         insert MemberAccountList;
         TestDataFactory.createProducts('Liberty Rx');
         CreateUpdateAssetExtension  ext=new CreateUpdateAssetExtension (new apexpages.standardController(new Asset(accountId=MemberAccountList[0].id)));
         ext.Asset.Product2Id =[Select id from product2 where productcode='Liberty Rx' limit 1].id;
         ext.SaveAndRedirectToParent();
                  Apexpages.currentpage().getparameters().put('id',[select id from Asset order by createddate DESC limit 1].id);

         ext.CancelAndRedirectToParent();
 
 }



}