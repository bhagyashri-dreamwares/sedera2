/**
*@Purpose : Helper class to translate the Dwolla Business Class Names to the Dwolla Business Class Values that their API uses
*@Date : 12/28/2018
*/
public class DwollaBusinessClassTranslationHelper {

    /**
    *@Purpose : This method will create the second transfer (balance to sedera) after the initial transfer (customer to balance) is completed
    */
    public static string Translate(string dwollaBusinessClassName){
        return [select dwolla_business_class_value__c from dwolla_business_class_translation__c where dwolla_Business_class_name__c = :dwollaBusinessClassName].dwolla_business_class_value__c ;
    }
}