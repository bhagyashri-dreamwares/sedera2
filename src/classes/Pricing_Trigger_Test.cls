/*
 Classes/Trigger it covers 
 
 Pricing_Trigger 
 Pricing_trigger_handler 
 Pricing_Helper_class
*/
@istest
public class Pricing_Trigger_Test{


  public static testmethod void Testmethod1(){
  
  
    List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
    PriceList[0].name='Default Old Select Pricing';
    Update PriceList[0];
    List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
    AccessPricing[0].name='Default Access Pricing';
    AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
    insert AccessPricing[0];   
    UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger=0;
    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
    List <Account> PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);

    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
    List <Account> EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);
    List<Account> memAccList=TestDataFactory.Create_Account_Of_Member_type(3,false);
    
    for(Account acc:memAccList){
     acc.Account_Employer_name__c=EmployerAccountList[0].id;    
     acc.subscription_status__c='Active';
     acc.date_Of_birth__c=date.today();
     acc.dependent_status__c='EO';
   //  acc.Iua_chosen__c=1000;
    }
    
    List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T1';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;

    
    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
    insert memAccList;
    
    //system.AssertEquals([select mcs_product_amount__c from Account where id=:memAccList[0].Id limit 1].mcs_product_amount__c,33);
    
    //U30EOIUA500T1
    
        
    UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger=0;
    Pricing__c pr=[Select id from pricing__c where Is_Base_Price_Record__c=false limit 1];
    pr.U30EOIUA500__c=22;
    Update pr;


    
    UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger=0;
    pr=[Select id from pricing__c where Is_Base_Price_Record__c=true limit 1];
    pr.MSEO__c=22;
    Update pr;
    
    
  }



}