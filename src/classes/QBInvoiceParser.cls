/**
*@Author       : Dreamwares
*@Created Date : 19/07/2018
*@Purpose      : InvoiceParser to map QBInvoiceDTO to Invoice__c && Invoice__c to QBInvoiceDTO
*/
public with sharing class QBInvoiceParser implements IParser{
    
    public List<Object> parseToDTOList(List<Object> recordsList){
        return null;
    }
    
    public List<Object> parseToDTOList(List<Object> recordsList, String currentQBOrg){
        
        List<QBWrappers.QBInvoiceDTO> DTOInvoiceList = new List<QBWrappers.QBInvoiceDTO>();
        if(recordsList != null && recordsList.size() > 0){
            
            // hold the Customer__c on invoices DueDate
            List<Id> accountIdList = new List<Id>();
            
            // hold the invoices Id
            List<Id> invoiceIdList = new List<Id>();
            
            // hold the Product Ids related to line items
            List<Id> itemIdList = new List<Id>();
            
            Map<Id, Account> customerIdtoRecMap = new Map<Id, Account>();
            Map<String, Product2> itemExternalIdsMap = new Map<String, Product2>();
            
            for( Invoice__c invoice : (List<Invoice__c>)recordsList ){
                invoiceIdList.add(invoice.Id);
                
                if(invoice.Account__c != null){
                    accountIdList.add(invoice.Account__c);  
                }  
                
                for(Invoice_Line_Item__c lineItem : invoice.Invoice_Line_Items__r){if(lineItem.Product__c != null){ itemIdList.add(lineItem.Product__c);
                    }    
                }    
            }
            Quick_Book_App_Configuration__c settings = Quick_Book_App_Configuration__c.getInstance(currentQBOrg);
            if(currentQBOrg == 'Quickbooks'){              
                
                for(Invoice__c invoice : (List<Invoice__c>)recordsList){
                    
                    QBWrappers.QBInvoiceDTO DTOInvoice = new QBWrappers.QBInvoiceDTO(); 
                    
                   
                    DTOInvoice.SyncToken = invoice.QB_Sync_Token__c;
                    DTOInvoice.Id = invoice.Quickbooks_Invoice_ID__c;
                    DTOInvoice.CustomerRef.value = invoice.Quickbooks_Customer_ID__c;
                    
                   
                    
                    //if(customerIdtoRecMap != NULL && customerIdtoRecMap.size() > 0){
                    
                    
                    //DTOInvoice.DocNumber = Integer.valueOf(invoice.Name.substring(2));
                    DTOInvoice.DocNumber = invoice.Name;
                    DTOInvoice.AllowOnlineCreditCardPayment = true;
                    DTOInvoice.AllowOnlineACHPayment = true;
                    DTOInvoice.AllowOnlinePayment = true;
                    DTOInvoice.AllowIPNPayment = true;
                    DTOInvoice.AllowOnlinePayment = true;
                    //DTOInvoice.TotalAmt = invoice.Amount__c;
                    //DTOInvoice.DueDate = invoice.Due_Date__c;
                    
                    DTOInvoice.TotalAmt = 20;
                    DTOInvoice.DueDate = invoice.Invoice_Due_Date__c; 
                    DTOInvoice.CustomerRef.name = invoice.Account__r.Name;
                    
                    if(invoice.Account__c != null && String.isNotBlank(invoice.Account__r.Invoice_Email_Address__c)){ DTOInvoice.BillEmail.Address = getChoppedEmails(invoice.Account__r.Invoice_Email_Address__c);
                    }
                    
                    if(!String.isBlank(invoice.QB_Terms_ID__c)){ DTOInvoice.SalesTermRef.value = invoice.QB_Terms_ID__c;
                    }
                    
                    if(invoice.Invoice_Date__c != NULL ){
                        DTOInvoice.TxnDate = invoice.Invoice_Date__c;
                    }
                    system.debug('parseToDTOList DTOInvoice.TxnDate '+DTOInvoice.TxnDate);
                    if(!String.isBlank(invoice.Service_Period__c)){
                        QBWrappers.CustomField customFieldObj = new QBWrappers.CustomField(); customFieldObj.DefinitionId = '3'; customFieldObj.Name  = 'Service Period'; customFieldObj.Type = 'StringType';  DTOInvoice.CustomField = new QBWrappers.CustomField[]{customFieldObj};
                            //DTOInvoice.CustomField.add(customFieldObj);
                            }
                    if(String.isNotBlank(invoice.Name)){   QBWrappers.CustomField customFieldObj = new QBWrappers.CustomField(); customFieldObj.DefinitionId = '1'; customFieldObj.Name  = 'SF Invoice Name'; customFieldObj.Type = 'StringType'; customFieldObj.StringValue = invoice.Name; DTOInvoice.CustomField = new QBWrappers.CustomField[]{customFieldObj};    
                            }
                    
                    system.debug('DTOInvoice '+DTOInvoice);
                    // DTOInvoice.Balance = invoice.Balance__c;
                    
                    String taxCode;
                    
                    Map<String, List<Invoice_Line_Item__c>> mapOfCodeToLineItems = new Map<String, List<Invoice_Line_Item__c>>();
                    for(Invoice_Line_Item__c lineItem : invoice.Invoice_Line_Items__r) { if(mapOfCodeToLineItems.containsKey(lineItem.QBO_Product__r.ProductCode)) { mapOfCodeToLineItems.get(lineItem.QBO_Product__r.ProductCode).add(lineItem); } else { mapOfCodeToLineItems.put(lineItem.QBO_Product__r.ProductCode, new List<Invoice_Line_Item__c> {lineItem}); }
                    }
                    System.debug('mapOfCodeToLineItems ::: ' + mapOfCodeToLineItems);
                    
                    for(String productCode: mapOfCodeToLineItems.keySet()) { List<Invoice_Line_Item__c> lineItems = mapOfCodeToLineItems.get(productCode);
                        QBWrappers.QBLine DTOLineItem = new QBWrappers.QBLine(); for(Invoice_Line_Item__c lineItem: lineItems) { QBWrappers.QBItemRef itemRef = new QBWrappers.QBItemRef(); QBWrappers.QBTaxCodeRef TaxCodeRef = new QBWrappers.QBTaxCodeRef(taxCode);
                            
                            Decimal rateAmount = 0; Decimal quantity = 0;  if(lineItem.Price__c != null) {  if(DTOLineItem.SalesItemLineDetail != NULL && DTOLineItem.SalesItemLineDetail.UnitPrice != NULL) { rateAmount = DTOLineItem.SalesItemLineDetail.UnitPrice + lineItem.Price__c;  } else { rateAmount = lineItem.Price__c;
                                }  } if(lineItem.Quantity__c != null) { if(DTOLineItem.SalesItemLineDetail != NULL && DTOLineItem.SalesItemLineDetail.Qty != NULL) { quantity = DTOLineItem.SalesItemLineDetail.Qty + lineItem.Quantity__c; } else {  quantity = lineItem.Quantity__c; } } Double amount; if(rateAmount != null) {  amount = Double.valueOf(rateAmount.setScale(2)); } else { amount = Double.valueOf(rateAmount);
                            }
                            
                            QBWrappers.QBSalesItemLineDetail saleLineDetail   = new QBWrappers.QBSalesItemLineDetail(itemRef,   amount, Double.valueOf(quantity), TaxCodeRef);
                            
                           
                            if(lineItem.QB_Line_Item_Id__c != null) {  String lineid = lineItem.QB_Line_Item_Id__c.split('-')[1];  DTOLineItem.Id = lineid;
                            }
                            
                            if(DTOLineItem.Amount != NULL) { DTOLineItem.Amount = DTOLineItem.Amount + lineItem.Line_Item_Total__c;  } else { DTOLineItem.Amount = lineItem.Line_Item_Total__c;
                            }
                            
                            if(DTOLineItem.Description != NULL) { DTOLineItem.Description = DTOLineItem.Description + '\n' + lineItem.Description__c;  } else { DTOLineItem.Description = lineItem.Description__c;
                            }
                            
                            DTOLineItem.SalesItemLineDetail = saleLineDetail;  DTOLineItem.DetailType = 'SalesItemLineDetail'; DTOLineItem.SalesItemLineDetail.ServiceDate = lineItem.CreatedDate.format('yyyy-MM-dd'); DTOLineItem.SalesItemLineDetail.ItemRef.value = lineItem.Quickbooks_Product_ID__c;  DTOLineItem.SalesItemLineDetail.ItemRef.name = lineItem.QBO_Product__r.Name;
                            
                                                   
                        }
                        
                        if(DTOLineItem.SalesItemLineDetail != NULL && DTOLineItem.Amount != NULL && DTOLineItem.Amount != 0) {  DTOLineItem.SalesItemLineDetail.UnitPrice = DTOLineItem.Amount / DTOLineItem.SalesItemLineDetail.Qty; }  DTOInvoice.Line.add(DTOLineItem);
                    }
                    
                    DTOInvoiceList.add(DTOInvoice);
                }
            }
            if(currentQBOrg == 'Quickbooks2'){ List<QuickBook2_LineItems_Configuration__c> defoultQuickbooks2LineItems = [SELECT Id,
                                                                                           Name, 
                                                                                           Quickbook2_Product_Id__c,
                                                                                           Quickbook2_Product_Name__c, 
                                                                                           Salesforce_Product_Id__c
                                                                                           FROM QuickBook2_LineItems_Configuration__c 
                                                                                           LIMIT 50000];
                for(Invoice__c invoice : (List<Invoice__c>)recordsList){  QBWrappers.QBInvoiceDTO DTOInvoice = new QBWrappers.QBInvoiceDTO();   DTOInvoice.SyncToken = invoice.QB_Sync_Token2__c;  DTOInvoice.Id = invoice.Quickbooks_Invoice_ID2__c; DTOInvoice.CustomerRef.value = invoice.Quickbooks_Customer_ID2__c;
                                        
                    //DTOInvoice.DocNumber = Integer.valueOf(invoice.Name.substring(2));
                    DTOInvoice.DocNumber = invoice.Name; DTOInvoice.AllowOnlineCreditCardPayment = true; DTOInvoice.AllowOnlineACHPayment = true; DTOInvoice.AllowOnlinePayment = true; DTOInvoice.AllowIPNPayment = true;  DTOInvoice.AllowOnlinePayment = true; DTOInvoice.TotalAmt = 20; DTOInvoice.DueDate = invoice.Invoice_Due_Date__c;   DTOInvoice.CustomerRef.name = invoice.Account__r.Name;
                    
                    if(invoice.Account__c != null && String.isNotBlank(invoice.Account__r.Invoice_Email_Address__c)){  DTOInvoice.BillEmail.Address = getChoppedEmails(invoice.Account__r.Invoice_Email_Address__c);
                    }
                    
                    if(!String.isBlank(invoice.QB_Terms_ID__c)){  DTOInvoice.SalesTermRef.value = invoice.QB_Terms_ID__c;
                    }
                    
                    if(invoice.Invoice_Date__c != NULL ){  DTOInvoice.TxnDate = invoice.Invoice_Date__c;
                    }
                    
                    if(!String.isBlank(invoice.Service_Period__c)) { QBWrappers.CustomField customFieldObj = new QBWrappers.CustomField(); customFieldObj.DefinitionId = '3'; customFieldObj.Name  = 'Service Period'; customFieldObj.Type = 'StringType'; customFieldObj.StringValue = invoice.Service_Period__c;  DTOInvoice.CustomField = new QBWrappers.CustomField[]{customFieldObj};
                            
                            }
                    if(String.isNotBlank(invoice.Name)){ QBWrappers.CustomField customFieldObj = new QBWrappers.CustomField(); customFieldObj.DefinitionId = '1'; customFieldObj.Name  = 'SF Invoice Name'; customFieldObj.Type = 'StringType'; customFieldObj.StringValue = invoice.Name; DTOInvoice.CustomField = new QBWrappers.CustomField[]{customFieldObj};    
                            }
                    
                    system.debug('DTOInvoice '+DTOInvoice);
                    // DTOInvoice.Balance = invoice.Balance__c;
                    
                    String taxCode;
                    
                    /* Not required for quickbook2
                    Map<String, List<Invoice_Line_Item__c>> mapOfCodeToLineItems = new Map<String, List<Invoice_Line_Item__c>>();
                    for(Invoice_Line_Item__c lineItem : invoice.Invoice_Line_Items__r) {
                        if(mapOfCodeToLineItems.containsKey(lineItem.QBO_Product__r.ProductCode)) {
                            mapOfCodeToLineItems.get(lineItem.QBO_Product__r.ProductCode).add(lineItem);
                        } else {
                            mapOfCodeToLineItems.put(lineItem.QBO_Product__r.ProductCode, new List<Invoice_Line_Item__c> {lineItem});
                        }
                    }
                    System.debug('mapOfCodeToLineItems ::: ' + mapOfCodeToLineItems);*/
                    
                    /*for(String productCode: mapOfCodeToLineItems.keySet()) {
                        List<Invoice_Line_Item__c> lineItems = mapOfCodeToLineItems.get(productCode);*/
                        
                        
                        //  List<QuickBook2_LineItems_Configuration__c> defoultQuickbooks2LineItems 
                        
                        
                        //for(Invoice_Line_Item__c lineItem: lineItems) {
                        for(QuickBook2_LineItems_Configuration__c lineItem: defoultQuickbooks2LineItems) {
                            
                            QBWrappers.QBLine DTOLineItem = new QBWrappers.QBLine(); Decimal rateAmount = 0; Decimal quantity = 0; QBWrappers.QBItemRef itemRef = new QBWrappers.QBItemRef();  QBWrappers.QBTaxCodeRef TaxCodeRef = new QBWrappers.QBTaxCodeRef(taxCode);
                                
                            if(lineItem.Name == 'Standard MCS Revenue (Existing)') { 
                                rateAmount = invoice.Standard_MCS_Revenue_Existing__c; 
                                if(String.isNotBlank(invoice.Standard_MCS_Revenue_Existing_QBLI_ID__c)){  
                                    DTOLineItem.Id = invoice.Standard_MCS_Revenue_Existing_QBLI_ID__c.split('-')[1];
                                }
                            } else if(lineItem.Name == 'Standard MCS Revenue (New)') { 
                                rateAmount = invoice.Standard_MCS_Revenue_New__c;   
                                if(String.isNotBlank(invoice.Standard_MCS_Revenue_New_QBLI_ID__c)){ 
                                    DTOLineItem.Id = invoice.Standard_MCS_Revenue_New_QBLI_ID__c.split('-')[1];
                                }
                            } else if(lineItem.Name == 'Startup MCS Revenue') {  
                                rateAmount = invoice.Startup_MCS_Revenue__c;  
                                if(String.isNotBlank(invoice.Startup_MCS_Revenue_QBLI_ID__c)){  
                                    DTOLineItem.Id = invoice.Startup_MCS_Revenue_QBLI_ID__c.split('-')[1];
                                }
                            } else if(lineItem.Name == 'MCS Member Services') {  
                                rateAmount = invoice.Member_Services_Revenue__c;  
                                if(String.isNotBlank(invoice.Member_Services_Revenue_QBLI_ID__c)){  
                                    DTOLineItem.Id = invoice.Member_Services_Revenue_QBLI_ID__c.split('-')[1]; } } else { rateAmount = 0;
                            }
                            
                          
                            quantity = 1;  
                            
                            Double amount;
                            amount = rateAmount;
                            /*if(rateAmount != null) {
                                amount = rateAmount;
                            } else {
                                amount = rateAmount;
                            }*/
                            
                            QBWrappers.QBSalesItemLineDetail saleLineDetail = new QBWrappers.QBSalesItemLineDetail(itemRef, amount,Double.valueOf(quantity),TaxCodeRef);                                                
                            if(DTOLineItem.Amount != NULL){DTOLineItem.Amount = rateAmount;} else { DTOLineItem.Amount = rateAmount;
                            }
                            
                            DTOLineItem.SalesItemLineDetail = saleLineDetail;
                            DTOLineItem.DetailType = 'SalesItemLineDetail';
                            if(invoice.CreatedDate != null){
                                DTOLineItem.SalesItemLineDetail.ServiceDate = invoice.CreatedDate.format('yyyy-MM-dd');
                            }
                            DTOLineItem.SalesItemLineDetail.ItemRef.value = lineItem.Quickbook2_Product_Id__c;
                            DTOLineItem.SalesItemLineDetail.ItemRef.name = lineItem.Name;DTOInvoice.Line.add(DTOLineItem);
                            
                            
                            
                        }                        
                        
                        
                   // }
                    
                    DTOInvoiceList.add(DTOInvoice);
                }                
            }            
            //DTOInvoiceList.add(DTOInvoice);
        }  
        system.debug('DTOInvoiceList '+JSON.serialize(DTOInvoiceList));
        return DTOInvoiceList;
    }
    
    public List<Object> parseToObjectList(List<Object> recordsList, String currentQBOrg){
        List<InvoiceToLineItemWrapper> invoiceWithLineItemList = new List<InvoiceToLineItemWrapper>();
        
        if(recordsList != null){
            List<String> customerNoList = new List<String>();
            List<String> qbItemIdList = new List<String>();
            Map<String,Id> qbCustomerToSFCustomerMap = new Map<String, Id>();
            Map<String,Id> qbItemIdToItemId = new Map<String,Id>();
            
            for(QBWrappers.QBInvoiceDTO qbInvoice : (List<QBWrappers.QBInvoiceDTO>)recordsList){                
                customerNoList.add(qbInvoice.CustomerRef.value);
                for(QBWrappers.QBLine qbLineItem : qbInvoice.Line){
                    if(qbLineItem.SalesItemLineDetail != null){
                        qbItemIdList.add(qbLineItem.SalesItemLineDetail.ItemRef.value);
                    }   
                }
            }            
            
            for(QBWrappers.QBInvoiceDTO qbInvoice : (List<QBWrappers.QBInvoiceDTO>)recordsList){
                
                List<Invoice_Line_Item__c> lineItemList = new List<Invoice_Line_Item__c>();
                List<Invoice__c> invoiceList = new List<Invoice__c>();
                if(qbCustomerToSFCustomerMap != NULL){
                    System.debug('Before If 1 :: '+currentQBOrg);
                    Invoice__c invoice = new Invoice__c();
                    if(currentQBOrg == 'Quickbooks'){
                        System.debug('Quickbooks 1 qbInvoice :: '+qbInvoice.Id);
                        invoice.Quickbooks_Invoice_ID__c = qbInvoice.Id;
                        invoice.QB_Sync_Token__c = qbInvoice.SyncToken;
                        invoice.Quickbooks_Creation_Status__c = 'Success';
                        invoice.Quickbooks_Error_Message__c = '';
                        if(qbInvoice.MetaData != null && String.isNotBlank(qbInvoice.MetaData.CreateTime)){ invoice.Quickbooks_Created_Date__c = convertStringToTime(qbInvoice.MetaData.CreateTime);
                        }
                    }
                    if(currentQBOrg == 'Quickbooks2'){
                        System.debug('Quickbooks 2 qbInvoice :: '+qbInvoice.Id);
                        invoice.Quickbooks_Invoice_ID2__c = qbInvoice.Id;
                        invoice.QB_Sync_Token2__c = qbInvoice.SyncToken;
                        invoice.Quickbooks2_Creation_Status__c = 'Success';
                        invoice.Quickbooks2_Error_Message__c = '';
                        
                        if(qbInvoice.MetaData != null && String.isNotBlank(qbInvoice.MetaData.CreateTime)){ invoice.Quickbooks2_Created_Date__c = convertStringToTime(qbInvoice.MetaData.CreateTime);
                        }
                    }
                    
                    
                    if(qbInvoice.DueDate != null){invoice.Invoice_Due_Date__c = qbInvoice.DueDate; 
                    }
                    system.debug('parseToObjectList qbInvoice.TxnDate '+qbInvoice.TxnDate);
                    if(qbInvoice.TxnDate != null){invoice.Invoice_Date__c = qbInvoice.TxnDate;
                    }
                    
                    if(qbCustomerToSFCustomerMap.get(qbInvoice.CustomerRef.value) != null){ invoice.Account__c = qbCustomerToSFCustomerMap.get(qbInvoice.CustomerRef.value);
                    }
                    
                    if(qbInvoice.CustomField != null && qbInvoice.CustomField.size() != 0){ for(QBWrappers.CustomField customFieldObj : qbInvoice.CustomField){if(String.isNotBlank(customFieldObj.DefinitionId) && String.isNotBlank(customFieldObj.StringValue) && customFieldObj.DefinitionId == '3'){ invoice.Service_Period__c = customFieldObj.StringValue;
                               }
                        }
                    }
                    
                    for(QBWrappers.QBLine qbLineItem : (List<QBWrappers.QBLine>)qbInvoice.Line){
                        Invoice_Line_Item__c sfLineItem = new Invoice_Line_Item__c();
                        //Invoice__c invoiceRecord = new Invoice__c();
                        if(qbLineItem.DetailType == 'SalesItemLineDetail'){
                            // sfLineItem.Line_Item_Total__c = qbLineItem.Amount;
                            sfLineItem.Description__c = qbLineItem.Description;                            
                            sfLineItem.Quantity__c = qbLineItem.SalesItemLineDetail.Qty;
                            sfLineItem.Price__c = qbLineItem.SalesItemLineDetail.UnitPrice;   
                            if(currentQBOrg == 'Quickbooks'){
                                sfLineItem.QB_Line_Item_Id__c = qbInvoice.id + '-' + qbLineItem.Id;
                            }
                            if(currentQBOrg == 'Quickbooks2'){
                                
                                //for(QBWrappers.QBLine qbrecord : qbInvoice.Line){
                                   //invoiceRecord = new Invoice__c();
                                    if(qbLineItem.SalesItemLineDetail != NULL){
                                        if(qbLineItem.SalesItemLineDetail.ItemRef != NULL){
                                            if(String.isNotBlank(qbLineItem.SalesItemLineDetail.ItemRef.name)){                                                
                                                if(qbLineItem.SalesItemLineDetail.ItemRef.name == 'Standard MCS Revenue (Existing)'){
                                                    invoice.Standard_MCS_Revenue_Existing_QBLI_ID__c = qbInvoice.id + '-' + qbLineItem.Id;
                                                }else if(qbLineItem.SalesItemLineDetail.ItemRef.name == 'Startup MCS Revenue'){
                                                    invoice.Startup_MCS_Revenue_QBLI_ID__c = qbInvoice.id + '-' + qbLineItem.Id;
                                                }else if(qbLineItem.SalesItemLineDetail.ItemRef.name == 'MCS Member Services'){
                                                    invoice.Member_Services_Revenue_QBLI_ID__c = qbInvoice.id + '-' + qbLineItem.Id;
                                                }else if(qbLineItem.SalesItemLineDetail.ItemRef.name == 'Standard MCS Revenue (New)'){
                                                    invoice.Standard_MCS_Revenue_New_QBLI_ID__c = qbInvoice.id + '-' + qbLineItem.Id;
                                                }
                                            }                                            
                                        }                                        
                                    }  
                                    //invoiceList.add(invoiceRecord);
                                    //break;
                                //}
                            }
                            
                            if(currentQBOrg == 'Quickbooks'){
                                lineItemList.add(sfLineItem);
                            }
                            
                        } 
                    }  
                    if(currentQBOrg == 'Quickbooks'){
                        invoiceWithLineItemList.add(new InvoiceToLineItemWrapper(invoice, lineItemList));
                    }else if(currentQBOrg == 'Quickbooks2'){
                    invoiceWithLineItemList.add(new InvoiceToLineItemWrapper(invoice, null));
                       // invoiceWithLineItemList.add(new InvoiceToLineItemWrapper(invoice, invoiceList, 'emptyString'));                        
                    } 
                    //invoiceWithLineItemList.add(new InvoiceToLineItemWrapper(invoice, lineItemList));
                }              
            }
        }
        System.debug('Invoice List parseToObjectList ::' + Json.serialize( invoiceWithLineItemList ));
        return invoiceWithLineItemList;        
    }   
    
    
    public DateTime convertStringToTime(String dateTimeValue){ //2017-10-11T01:19:15-07:00
        if(String.isNotBlank(dateTimeValue)){
            try{
                return (Datetime) JSON.deserialize('"'+dateTimeValue+'"', Datetime.class); } catch(Exception e){
                system.debug(e.getMessage());
            }
        }
        return null; 
    }
    
    public static String getChoppedEmails(String strEmails){
        if(String.isNotBlank(strEmails)){
            strEmails = strEmails.remove(' ');
            if(strEmails.length() > 100){
                return getChoppedEmails(strEmails.substringBeforeLast(','));
            }
            return strEmails;
        }
        return '';
    }
}