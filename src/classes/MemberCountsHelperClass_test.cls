@isTest
public class MemberCountsHelperClass_Test{

  public static testmethod void test(){
  
        Test.startTest();
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass1);
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        List <Account> EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List <Account> AffiliateAccountList = TestDataFactory.Create_Account_Of_Affiliate_type(2, false);
        AffiliateAccountList[1].Affiliate_Type__c='Strategic Affiliate';                
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        insert AffiliateAccountList;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        emp.Referral_Affiliate__c=AffiliateAccountList[0].Id;
        emp.Strategic_Affiliate__c=AffiliateAccountList[1].Id;
        update emp;
        
        

        List <MEC_Product__c> Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List <AccountMECAssociation__c> Mec_Assosciation_List = new List <AccountMECAssociation__c> ();


        for (integer j = 0; j <1; j++) {

            for (Integer i = 0; i <3; i++) {

                List <AccountMECAssociation__c> MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        List <Account> MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(1, false);
        for (integer i = 0; i <MemberAccountList.size(); i++) {
            MemberAccountList[i].Account_Employer_name__C = emp.id;
            MemberAccountList[i].Subscription_Status__c = 'Active';
            MemberAccountList[i].Dependent_Status__c = 'EO';

        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;        
        insert MemberAccountList;
        
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=0;
        
        for (integer i = 0; i <MemberAccountList.size(); i++) {
            MemberAccountList[i].Subscription_Status__c = 'Application In Process';

        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;        
        Update MemberAccountList;
 
        for (integer i = 0; i <MemberAccountList.size(); i++) {
            MemberAccountList[i].Subscription_Status__c = 'Active';

        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;        
        Update MemberAccountList;
        
        for (integer i = 0; i <MemberAccountList.size(); i++) {
            MemberAccountList[i].dependent_status__c= 'EF';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;        
        Update MemberAccountList;
        
        for (integer i = 0; i <MemberAccountList.size(); i++) {
            MemberAccountList[i].dependent_status__c= '';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;        
        Update MemberAccountList;
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;        
        delete MemberAccountList;

  }


}