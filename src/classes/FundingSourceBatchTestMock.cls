/*
* @description      : Positive mock response
* @author       : Navin (Dreamwares)
* @since        : 1 Oct 2018
* @date         : 1 Oct 2018 - Navin: created initial version
* @date         ${date} - ${user}: Change history
* @see      
*/
@isTest
public class FundingSourceBatchTestMock implements HttpCalloutMock{
    
    public HTTPResponse respond(HTTPRequest req) {
        HTTPResponse response = new HTTPResponse(); 
        if (req.getEndpoint().contains('https://sandbox.dwolla.com/oauth/v2/token')) {         
            response.setStatusCode(200);
            response.setBody('{ "processor_token": "processor-sandbox-7451e68e-0005-48bc-8b5b-fdb196122d29", "request_id": "IId5rIB1g9fr1ad" }');            
        }
        return response;
        
    }
    
}