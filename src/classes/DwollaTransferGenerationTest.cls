@isTest
public class DwollaTransferGenerationTest 
{
    
    @TestSetup
    static void makeData(){
        
        DwollaAPIConfiguration__c dwollaApiRecord = new DwollaAPIConfiguration__c(Access_Token__c = '5DeH2odJRoqJA21i6GH8BHeS0cEOJR0JaMgG3d32Aa6xeZ5MfV',
                                                                                  Access_Token_Expiry__c = system.today(), 
                                                                                  Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token',
                                                                                  Client_Key__c = 'UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp',
                                                                                  Is_Sandbox__c=true,
                                                                                  Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize',
                                                                                  Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com',
                                                                                  Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150');
        insert dwollaApiRecord;
      
        List<Dwolla_Funding_Source__c> dwollaFundingSourceList = new List<Dwolla_Funding_Source__c>();
        
        Account account = new Account(Name = 'test account', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId());
        account.Send_Plaid_Link__c = TRUE;
        
        insert account;
        
        Dwolla_Funding_Source__c dwollaFundingSource1 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
            Type__c = 'Customer', Dwolla_Funding_Source_ID__c = '093c94ff-2f58-430d-9ba5-ad319c4fb963', Active__c = true);
        dwollaFundingSourceList.add(dwollaFundingSource1);

        Dwolla_Funding_Source__c dwollaFundingSource2 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
            Type__c = 'Balance', Dwolla_Funding_Source_ID__c = '093c94ff-2f58-430d-9ba5-ad319c4fb962', Active__c = true);
        dwollaFundingSourceList.add(dwollaFundingSource2);
        
        Dwolla_Funding_Source__c dwollaFundingSource3 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
            Type__c = 'Sedera', Dwolla_Funding_Source_ID__c = '093c94ff-2f58-430d-9ba5-ad319c4fb961', Active__c = true);
        dwollaFundingSourceList.add(dwollaFundingSource3);

        insert dwollaFundingSourceList;
        
        for(Dwolla_Funding_Source__c dfs :dwollaFundingSourceList )
        {
            System.Debug(dfs.type__c);
            System.Debug(dfs.Employer_Account__c);
        }
        
        // Insert Invoice
        Invoice__c invoice = new Invoice__c(Account__c = account.Id,
                                            Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Gross_Monthly_Total_Existing__c = 1000,
                                            Gross_Monthly_Total_New__c  = 1000,
                                            MCS_Existing_Member_Revenue_Percent__c = 0.099,
                                            MCS_New_Member_Revenue_Percent__c = 1.0,
                                            ENA__c = 0.15);
        insert invoice;
        
        Product2 prod = new Product2(name = 'Test Product', isActive = TRUE, Price__c = 5000, Quickbooks_ID__C = '27');
        insert prod;
        
        Invoice_Line_Item__c invL = new Invoice_Line_Item__c(Invoice__c = invoice.id, price__c = 5000, quantity__c = 1, account__c = account.id, QBO_Product__c = prod.id);
        insert invL;
        
        Account_Balance__c accountBalance = new Account_Balance__c(Account__c = account.id);
        insert accountBalance;
       
    }
    
    /*
     * @Purpose : To check DwollaTransferGeneration
     */ 
    @isTest
    static void processEventTest1(){

        Test.startTest();
        
        List<Dwolla_Funding_Source__c> fsList = new List<Dwolla_Funding_Source__c>();
        Account account = [SELECT ID FROM Account LIMIT 1];
        fsList = [select id, Type__c from Dwolla_Funding_Source__c where Employer_Account__c = :account.Id];
        fsList[0].Type__c = 'Customer';
        fsList[1].Type__c = 'Balance';
        fsList[2].Type__c = 'Sedera';
        
        update fsList;

        List<Id> invoiceList = new List<Id>();
        Invoice__c inv = [SELECT ID FROM Invoice__c LIMIT 1];
        invoiceList.add(inv.id);
        DwollaTransferGeneration dtg = new DwollaTransferGeneration(invoiceList); 
        database.executebatch(dtg,200);
  
        Test.stopTest();
        
    }
}