public class FundingSourceHandler{
    
    public static List<Dwolla_Error_Response__c> errors = new List<Dwolla_Error_Response__c>();
    public static List<Log__c> logs = new List<Log__c>();
    /**
    *@Purpose : create funding source to Dwolla
    */
    public static String createDwollaFundingSource(String plaidProcessorToken, String dowllaAccessToken, String routingNumber,
                                            String accountNumber, String bankAccountType,
                                            DwollaAPIConfiguration__c dwollaConfig, String customerId, String findingSourceName,
                                            List<Dwolla_Error_Response__c> errorList, List<Log__c> logList){
    
        
        Map<String,String> endPointParameterMap = new Map<String,String>();
        Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/vnd.dwolla.v1.hal+json',
                                                                       'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                       'Authorization'=>'Bearer '+dowllaAccessToken};
        
        System.debug('headerParameterMap :::\n'+headerParameterMap);
        JSONGenerator gen;
        if(String.isNotBlank(plaidProcessorToken)){
        
             gen = getCustomerJsonForfundingSource(plaidProcessorToken, findingSourceName);
        }else{
            
             gen = getJsonForfundingSource(routingNumber, accountNumber, bankAccountType, findingSourceName);    
        }
        System.debug('Json :::\n'+gen.getAsString());
        
        String endpoint = dwollaConfig.Is_Sandbox__c ? dwollaConfig.Sandbox_Endpoint_Url__c : dwollaConfig.Production_Endpoint_Url__c;
        endPoint = endpoint+'/customers/'+customerId+'/funding-sources'; 
        
        try{
            HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
            
            if(res.getStatusCode() == 201){
                return res.getHeader('Location').substringAfter('funding-sources/');
                
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = 'Status Code: '+res.getStatusCode()+'\n Response: '+res.getBody();
                errorWrapper.requestBody = 'EndPoint: '+endPoint+'\n headerParameterMap: '+headerParameterMap+
                                           ' \n endPointParameterMap: '+endPointParameterMap;
                errorWrapper.source = 'FundingSourceHandler.createDwollaFundingSource';
                
                System.debug('errorList ::'+errorList );
                System.debug('DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper) ::'+DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));    
                errors.addAll(errorList);
            }
            
        }catch(Exception exp){
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            System.debug('Exception ::::'+exp.getMessage());
            /*logList.add(LogUtil.createLog('FundingSourceHandler.createDwollaFundingSource', exp, 'plaidProcessorToken: '+plaidProcessorToken+
                                              '\n\n dowllaAccessToken: '+dowllaAccessToken+'\n\n RoutingNumber: '+routingNumber+
                                              '\n\n accountNumber: '+accountNumber+'\n\n bankAccountType: '+bankAccountType+
                                              '\n\n DwollaAPIConfiguration: '+dwollaConfig+'\n\n customerId: '+customerId+
                                              '\n\n findingSourceName: '+findingSourceName, '', 'Error'));*/
            //logs.addAll(logList);
            
             /*
             String errorInfoStr = 'plaidProcessorToken: '+plaidProcessorToken+
                                              '\n\n dowllaAccessToken: '+dowllaAccessToken+'\n\n RoutingNumber: '+routingNumber+
                                              '\n\n accountNumber: '+accountNumber+'\n\n bankAccountType: '+bankAccountType+
                                              '\n\n DwollaAPIConfiguration: '+dwollaConfig+'\n\n customerId: '+customerId+
                                              '\n\n findingSourceName: '+findingSourceName;
            
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }
        System.debug('errorList Handler ::'+errorList);       
        return '';
    }
    
    /**
    *@Purpose : get funding source from Dwolla
    */
    public static Response updateDwollaCustomSetting(DwollaAPIConfiguration__c dwollaConfig, List<Log__c> logList){
       
        Response respose;
         
        try{            
            update dwollaConfig;
            
            respose = new Response(true,'',dwollaConfig);
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            respose = new Response(false,'',exp.getMessage());
            /*logList.add(LogUtil.createLog('FundingSourceHandler.updateDwollaCustomSetting', exp, 'DwollaAPIConfiguration: '+dwollaConfig, 
                                           '', 'Error'));*/
            //logs.addAll(logList);
            
            /*
            String errorInfoStr = 'DwollaAPIConfiguration: '+dwollaConfig;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }
        
        return respose;
    }
    
    /**
    *@Purpose : get funding source from Dwolla
    */
    public static PlaidDTO.FundingSourceDetails getFundingSource(String fundingSourceId, DwollaAPIConfiguration__c dwollaConfig,
                                                                 List<Dwolla_Error_Response__c> errorList, List<Log__c> logList){
    
        PlaidDTO.FundingSourceDetails fundingSourceDetails;
        Map<String,String> endPointParameterMap = new Map<String,String>();
        Map<String,String> headerParameterMap = new Map<String,String>{'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                       'Authorization'=>'Bearer '+dwollaConfig.Access_Token__c};
        
        System.debug('Funding source Header :::\n'+headerParameterMap);
        String endpoint = dwollaConfig.Is_Sandbox__c ? dwollaConfig.Sandbox_Endpoint_Url__c : dwollaConfig.Production_Endpoint_Url__c;
        endPoint = endpoint+'/funding-sources/'+fundingSourceId; 
        
        try{
            HttpResponse res = calloutHelper.httpGetRequest(endPoint, endPointParameterMap, headerParameterMap);
            
            if(res.getStatusCode() == 200){
                
                fundingSourceDetails = (PlaidDTO.FundingSourceDetails)Json.deserialize(res.getBody(), PlaidDTO.FundingSourceDetails.class);
                System.debug('fundingSourceDetails  ::::\n'+fundingSourceDetails);
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = 'Status Code: '+res.getStatusCode()+'\n Response: '+res.getBody();
                errorWrapper.requestBody = 'EndPoint: '+endPoint+'\n headerParameterMap: '+headerParameterMap+
                                           ' \n endPointParameterMap: '+endPointParameterMap;
                errorWrapper.source = 'FundingSourceHandler.getFundingSource';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));    
                errors.addAll(errorList);
            }
            
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            /*logList.add(LogUtil.createLog('FundingSourceHandler.getFundingSource', exp, 'fundingSourceId: '+fundingSourceId+
                                              '\n\n DwollaAPIConfiguration: '+dwollaConfig, '', 'Error'));
            logs.addAll(logList);*/
            /*
            String errorInfoStr = 'fundingSourceId: '+fundingSourceId+
                                              '\n\n DwollaAPIConfiguration: '+dwollaConfig;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }     
        return fundingSourceDetails;
    }
    
    /**
    *@Purpose : create funding source to Dwolla
    */
    public static Response saveDwollaFundingSource(PlaidDTO.FundingSourceDetails fundingSourceDetails, String accountId, 
                                                   List<Log__c> logList, String accessToken, String PlaidAccountId){
        
        Response respose;
        
        try{
            System.debug('fundingSourceDetails.id ::::'+fundingSourceDetails.id);
            System.debug('accountId.id ::::'+accountId);
            Dwolla_Funding_Source__c fundingSource = new Dwolla_Funding_Source__c();
            fundingSource.Active__c = true;
            fundingSource.Dwolla_Funding_Source_ID__c = fundingSourceDetails.id;
            fundingSource.Dwolla_Funding_Source_Name__c = fundingSourceDetails.name;
            fundingSource.Employer_Account__c = accountId;
            fundingSource.Plaid_Access_Token__c = accessToken;
            fundingSource.Plaid_Item_ID__c = PlaidAccountId;
            fundingSource.Type__c = 'Customer';
            fundingSource.Bank_Name__c = fundingSourceDetails.bankName;
            fundingSource.Funding_Account_Type__c = fundingSourceDetails.bankAccountType;
            
            respose = saveFundingSouce(new List<Dwolla_Funding_Source__c>{fundingSource}, logList);
            
        }catch(Exception exp){
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            respose = new Response(false,exp.getMessage(), null);
            /*logList.add(LogUtil.createLog('FundingSourceHandler.saveDwollaFundingSource', exp, 'PlaidDTO.FundingSourceDetails: '+fundingSourceDetails+
                                              '\n\n accountId: '+accountId, '', 'Error'));
            logs.addAll(logList);*/
            
            /*
            String errorInfoStr = 'PlaidDTO.FundingSourceDetails: '+fundingSourceDetails+
                                              '\n\n accountId: '+accountId;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }
        return respose;
    }
    
    /**
    *@Purpose : To get account Id
    */  
    public static string getPlaidAccountId(String accessToken, PlaidAPIConfiguration__c config, 
                                           List<Dwolla_Error_Response__c> errorList, Account account){
    
        Map<String,String> endPointParameterMap = new Map<String,String>();
        Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json'};
        PlaidDTO.PlaidAccountDetails plaidAccountDetails;
        
        JSONGenerator gen = getPlaidAccountJson(accessToken, config);
        
        String endPoint = config.Endpoint__c+'/auth/get'; 
        
        try{
            HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
            
            if(res.getStatusCode() == 200){
                
                plaidAccountDetails = (PlaidDTO.PlaidAccountDetails)Json.deserialize(res.getBody(), PlaidDTO.PlaidAccountDetails.class);
            }else if(res.getStatusCode() == 400 && res.getBody().containsIgnoreCase('PRODUCT_NOT_READY')){                
                
                updateAccount(account);
                return 'PRODUCT_NOT_READY';        
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = 'Status Code: '+res.getStatusCode()+'\n Response: '+res.getBody();
                errorWrapper.requestBody = 'EndPoint: '+endPoint+'\n headerParameterMap: '+headerParameterMap+
                                           ' \n endPointParameterMap: '+endPointParameterMap;
                errorWrapper.source = 'FundingSourceHandler.getPlaidAccountId';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));  
                errors.addAll(errorList);  
            }
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            
            /*logs.add(LogUtil.createLog('FundingSourceHandler.getPlaidAccountId', exp, 'accessToken: '+accessToken+
                                              '\n\n config: '+config+'\n \n Account'+account, '', 'Error'));*/
            /*
            String errorInfoStr = 'accessToken: '+accessToken+
                                              '\n\n config: '+config+'\n \n Account'+account;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }  
        
        if(plaidAccountDetails != null){
            for(PlaidDTO.AccountInfo accountInfo : plaidAccountDetails.accounts){
                if(String.isNotBlank(accountInfo.subtype) && (accountInfo.subtype.equalsIgnoreCase('checking') || 
                                                              accountInfo.subtype.equalsIgnoreCase('savings') )){
                    return accountInfo.account_id;
                }
            }
        }
        return '';
    }
    
    /**
    *@Purpose : To get Processor token
    */  
    public static string getProcessorToken(String accessToken, String accountId, PlaidAPIConfiguration__c config, 
                                           List<Dwolla_Error_Response__c> errorList, List<Log__c> logList){
    
        Map<String,String> endPointParameterMap = new Map<String,String>();
        Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json'};
        PlaidDTO.ProcessorTokenDetails processorTokenDetails;
        
        JSONGenerator gen = getProcessorTokenJson(accessToken, accountId, config);
        System.debug('Processor Token::: \n'+gen.getAsString());
        
        String endPoint = config.Endpoint__c+'/processor/dwolla/processor_token/create'; 
        
        try{
            HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
            System.debug('res ::: \n'+res.getBody());
            
            if(res.getStatusCode() == 200){
                processorTokenDetails = (PlaidDTO.ProcessorTokenDetails)Json.deserialize(res.getBody(), PlaidDTO.ProcessorTokenDetails.class);
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = 'Status Code: '+res.getStatusCode()+'\n Response: '+res.getBody();
                errorWrapper.requestBody = 'EndPoint: '+endPoint+'\n headerParameterMap: '+headerParameterMap+
                                           ' \n endPointParameterMap: '+endPointParameterMap;
                errorWrapper.source = 'FundingSourceHandler.getProcessorToken';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)); 
                errors.addAll(errorList);    
            }
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            /*logList.add(LogUtil.createLog('FundingSourceHandler.getProcessorToken', exp, 'accessToken: '+accessToken+
                                              '\n\n accountId: '+accountId+'\n\n PlaidAPIConfiguration : '+config, '', 'Error'));
            logs.addAll(logList); */
            
            /*
            String errorInfoStr = 'accessToken: '+accessToken+
                                              '\n\n accountId: '+accountId+'\n\n PlaidAPIConfiguration : '+config;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }  
        
        if(processorTokenDetails != null){
            
            return processorTokenDetails.processor_token;
        }
        return '';
    }
    
     /**
    *@Purpose : Json to get Plaid Processor token
    */
    public static JSONGenerator getProcessorTokenJson(String accessToken, String accountId, PlaidAPIConfiguration__c config){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('access_token', accessToken);
        gen.writeStringField('account_id', accountId);  
        gen.writeStringField('client_id', config.Client_Key__c);
        gen.writeStringField('secret', config.Secret_Key__c);
        
        gen.writeEndObject();
        
        return gen;
    }
    
    /**
    *@Purpose : Json for auth to get account details of Plaid
    */
    public static JSONGenerator getPlaidAccountJson(String accessToken, PlaidAPIConfiguration__c config){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('client_id', config.Client_Key__c);
        gen.writeStringField('secret', config.Secret_Key__c);
        gen.writeStringField('access_token', accessToken);
        gen.writeEndObject();
        
        return gen;
    }
    
     /**
    *@Purpose :Create JSON generater for API request
    */
    public static JSONGenerator getCustomerJson(PlaidAPIConfiguration__c config, String publicToken){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('client_id', config.Client_Key__c);
        gen.writeStringField('secret', config.Secret_Key__c);
        gen.writeStringField('public_token', publicToken);
      //  gen.writeStringField('type', 'unverified customer');
        gen.writeEndObject();
        
        return gen;
    }
    
    /**
    *@Purpose :Create JSON generater to create funding source creation API request using Processor Token
    */
    public static JSONGenerator getCustomerJsonForfundingSource(String processorToken, String sourceName){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('plaidToken', processorToken);
        gen.writeStringField('name', sourceName);
        gen.writeEndObject();
        
        return gen;
    }
    
    /**
    *@Purpose :Create JSON generater to create funding source creation API request using routing number and bank number
    */
    public static JSONGenerator getJsonForfundingSource(String routingNumber, String accountNumber, 
                                                                String bankAccountType, String sourceName){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('routingNumber', routingNumber);
        gen.writeStringField('accountNumber', accountNumber);
        gen.writeStringField('bankAccountType', bankAccountType);
        gen.writeStringField('name', sourceName);
        gen.writeEndObject();
        
        return gen;
    }
    
    /**
    *@Purpose: To save funding source records
    */
    public static Response saveFundingSouce(List<Dwolla_Funding_Source__c> fundingSourceList,
                                            List<Log__c> logList){
        
        Response respose;
        try{
            
            upsert fundingSourceList;
//             sendEmailToRecipient('Funding Source Message', fundingSourcelist[0], 'Customer Funding Source Added', 'aaron.burke@fastslowmotion.com');
            respose = new Response(true,'',fundingSourceList[0]);
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage());
            Object obj;
            respose = new Response(false,exp.getMessage(),obj);
            /*logList.add(LogUtil.createLog('FundingSourceHandler.saveFundingSouce', exp, 'fundingSourceList: '+fundingSourceList, 
                                           '', 'Error'));
            logs.addAll(logList); */
            
            /*
            String errorInfoStr = 'fundingSourceList: '+fundingSourceList;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }
        
        return respose;
    }
    
    
    
    
    public static void sendEmailToRecipient(String pauboxsMsgRecordType, SObject objectRec, 
                                                         String templateName, String recepientAddress){
        string attachmentName = '';
        Paubox_Message__c pauboxMessage;
        PauBoxConfiguration__c config = PauBoxEmailHelper.getPauBoxDetails();
        Paubox_Message_Template__c template = PauBoxEmailHelper.getPauboxMessageTemplateByName(templateName);
        
        System.debug('template :::'+template);
        System.debug('objectRec :::'+objectRec);
        
        
        if(template != null && objectRec != null && String.isNotBlank(recepientAddress)){
            
            String senderAddress = String.isNotBlank(template.From_Address__c) ? 
                                   template.From_Address__c :
                                   config.From_Email__c;
            
            String emailHeader = PauBoxEmailHelper.getHtmlHeader();
            String emailFooter = PauBoxEmailHelper.getHtmlFooter(senderAddress);
            String emailBody = template.Template_Body__c+' <br/>';
            emailBody = emailBody.replaceAll('"','\\\\"');
            emailBody = emailBody.replaceAll('<p[^>]*>','<div style=\\\\"padding-bottom:20px;text-align:left;\\\\">');
            emailBody = emailBody.replaceAll('</p[^>]*>','</div>');   
            
     
            
            String fullEmail = emailHeader + emailBody + emailFooter;
            fullEmail = fullEmail.replaceAll('>\\s*<', '><');

            System.debug('emailBody ::: \n'+fullEmail );
 
            String requestBody = '{"data": { "message": { "recipients": ["'+recepientAddress+'"], '
                                 +'"headers": { "subject": "'+template.Template_Subject__c+'", "from": "'+senderAddress+'", '
                                 +'"reply-to": "Sedera <'+senderAddress+'>" },"content": '
                                 +'{"text/html": "'+fullEmail+'"}}}}';  
            System.Debug('Request Body = ' + requestBody);   
            
            if(!Test.isRunningTest()){
                pauboxMessage = PauBoxEmailHelper.sendEmailRequest(requestBody, objectRec, config, '',attachmentName, 
                                                               pauboxsMsgRecordType, senderAddress);
            }
         
        }
        //return pauboxMessage;     
    }
    
    public static string getPublicToken(String accessToken, PlaidAPIConfiguration__c config, 
                                           List<Dwolla_Error_Response__c> errorList, List<Log__c> logList){
    
        Map<String,String> endPointParameterMap = new Map<String,String>();
        Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json'};
        PlaidDTO.PublicTokenDetails publicTokenDetails;
        
        JSONGenerator gen = getJsonForPublicToken(accessToken, config);
        System.debug('Processor Token::: \n'+gen.getAsString());
        
        String endPoint = config.Endpoint__c+'/item/public_token/create'; 
        
        try{
            HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
            System.debug('res ::: \n'+res.getBody());
            
            if(res.getStatusCode() == 200){
                publicTokenDetails = (PlaidDTO.PublicTokenDetails)Json.deserialize(res.getBody(), PlaidDTO.PublicTokenDetails.class);
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = 'Status Code: '+res.getStatusCode()+'\n Response: '+res.getBody();
                errorWrapper.requestBody = 'EndPoint: '+endPoint+'\n headerParameterMap: '+headerParameterMap+
                                           ' \n endPointParameterMap: '+endPointParameterMap;
                errorWrapper.source = 'FundingSourceHandler.getPublicToken';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));    
                errors.addAll(errorList); 
            }
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
            /*logList.add(LogUtil.createLog('FundingSourceHandler.getPublicToken', exp, 'accessToken: '+accessToken+
                                            '\n\n PlaidAPIConfiguration : '+config, '', 'Error'));
            logs.addAll(logList); */
            
            /*
            String errorInfoStr = 'accessToken: '+accessToken+
                                            '\n\n PlaidAPIConfiguration : '+config;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }  
        
        if(publicTokenDetails != null){
            
            return publicTokenDetails.public_token;
        }
        return '';
    }
    public static JSONGenerator getJsonForPublicToken(String accessToken, PlaidAPIConfiguration__c config){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('client_id', config.Client_Key__c);
        gen.writeStringField('secret', config.Secret_Key__c);
        gen.writeStringField('access_token', accessToken);
        gen.writeEndObject();
        
        return gen;
    }
    
    public static void updateAccount(Account account){
        
        try{            
            update account;
        }catch(Exception exp){
            System.debug('Exception :::::'+exp.getMessage());
            //logs.add(LogUtil.createLog('FundingSourceHandler.updateAccount', exp, 'account: '+account, '', 'Error')); 
            
            /*
            String errorInfoStr = 'account: '+account;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }
    }
    
    public static Account getAccount(String accountId){
        
        try{
            
            List<Account> accountList = [SELECT Id, Name, Plaid_Access_Token__c, Plaid_Account_Status__c, Dwolla_ID__c,
                                                Plaid_Public_Token__c, PlaidSameDayAccessToken__c
                                         FROM Account
                                         WHERE Id = :accountId];
            return !accountList.isEmpty() ? accountList[0]: null;
        }catch(Exception exp){
            System.debug('Exception ::::'+exp.getMessage());
            //logs.add(LogUtil.createLog('FundingSourceHandler.getAccount', exp, 'accountId: '+accountId, '', 'Error')); 
            
            /*
            String errorInfoStr = 'accountId: '+accountId;
            FSMLogger.LogErrorMessage(exp, errorInfoStr);
            */
        }
        return null;
    }
}