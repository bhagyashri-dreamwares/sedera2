/**
*@Purpose : Helper class having all Dwolla API related methods
*@Date : 07/09/2018
*/
public class DwollaCalloutHelper{
    
    public DwollaAPIConfiguration__c config;
    public DwollaCalloutHelper(){
        this.config = getAPIConfiguration();
    }
    
    /**
    *@Purpose :Create customr record at Dwolla
    */
    public String createCustomer(Account account){
        
        if(account != null && config != null){
        
            Map<String,String> endPointParameterMap = new Map<String,String>();
            Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json',
                                                                           'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                           'Authorization'=>'Bearer '+config.Access_Token__c};
            
            JSONGenerator gen = getCustomerJson(account);
            
            String endPoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c : config.Production_Endpoint_Url__c;
            endPoint = String.isNotBlank(endPoint) ? endPoint+'/customers' : '';
            
            try{
                HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
                       
                if(res.getStatusCode() == 201){
                    return res.getHeader('Location').substringAfter('customers/');
                }else{
                    
                     DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                     errorWrapper.errorMsg = 'Status Code :\n'+res.getStatusCode()+'\n \n Error :\n'+res.getBody();
                     errorWrapper.requestBody = 'endPoint: \n'+endPoint+' \n\n endPointParameterMap :\n'+endPointParameterMap+
                                                '\n \n headerParameterMap:\n'+headerParameterMap+'\n\n Json :\n'+gen.getAsString();
                     errorWrapper.source = 'DwollaCalloutHelper.createCustomer';
                     errorWrapper.accountId = account.Id;
                     
                     DwollaErrorResponseHandler.saveDwollaErrorResponse(
                                new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
                }
            }catch(Exception exp){
                system.debug('Exception'+exp);
                
                LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCalloutHelper.createCustomer', exp, 'Account: '+account, 
                                                                 '', 'Error'))});   
            }
        } 
        return '';       
    }
    
    /**
    *@Purpose :Save Account record Information
    */
    public static void saveCustomer(List<Account> customerList){
        
        try{        
            upsert customerList;
        }catch(Exception exp){
        
            System.debug('Exception :::'+exp.getMessage());
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCalloutHelper.saveCustomer', exp, 'customerList: '+customerList, 
                                                                 '', 'Error'))});  
        }
    }
    
    /**
    *@Purpose :Create JSON generater for API request
    */
    private JSONGenerator getCustomerJson(Account account){
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstName', account.Name);
        gen.writeStringField('lastName', account.Name);
        gen.writeStringField('email', account.Invoice_Email_Address__c);
      //  gen.writeStringField('type', 'unverified customer');
        gen.writeStringField('businessName', account.Name);
        gen.writeEndObject();
        
        return gen;
    }
    
    /**
    *@Purpose :get custom setting record
    */
    public static DwollaAPIConfiguration__c getAPIConfiguration(){
        
        return DwollaAPIConfiguration__c.getOrgDefaults();
    }
}