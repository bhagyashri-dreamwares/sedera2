public class DwollaDocumentCalloutHelper{
    
    public DwollaAPIConfiguration__c config;
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public Boolean isPostiveTesting = false;
    public Boolean isTestValidToken = false;
    
    public DwollaDocumentCalloutHelper(){
        this.config = getAPIConfiguration();
    }
    
    /*
    * @Purpose :get Live Token.
    */    
    public DwollaAPIConfiguration__c getLiveToken(DwollaAPIConfiguration__c config){
        
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        // Get Access Token
        Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
            'client_secret' => config.Secret_Key__c,
            'grant_type' => 'client_credentials'};
                
        Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
        Response response;
        
        if(Test.isRunningTest()){
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
            System.debug('response @@@ '+ response);
            if(isTestValidToken){
                response.Success =  true;
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(true, '', oAuthResponse); 
            }else{
                response.Success =  false;
                response  = new Response(false, 'Invalid Token', null);
            }
            
        }else{
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
        }
        if(response != null){
            if(response.Success){
                
                OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                
                config.Access_Token__c = authTokenResponse.access_token;
                config.Access_Token_Expiry__c = Datetime.now().addMinutes(55);
                
                return config;
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+response.Message;
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                    '\n\n headerParameterMap :\n'+headerParameterMap;
                errorWrapper.source = 'DwollaDocumentCalloutHelper.getLiveToken';
                DwollaErrorResponseHandler.saveDwollaErrorResponse(new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
            }
        }
        return null;
    }
    
    /*
    * @Purpose :To upload document for Dwolla Customer
    */ 
    public DwollaDatasheetDocController.Response uploadDwollaDocument(Attachment att, String documentType, String customerId, String dataSheetAttachmentId){
        
        
        if(att.Body != null && String.isNotBlank(documentType) && config != null && String.isNotBlank(customerId)){
        
            Blob fileToUploadBlob = att.body;
        
            string   formBoundary = '------WebKitFormBoundary7MA4YWxkTrZu0gW',
                     footer = '\r\n--'+formBoundary+'--',                
                     documemtType = '--'+formBoundary+'\r\nContent-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}\r\n',
                     headerFile = '--'+formBoundary+'\r\nContent-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n';
            
            string documentHexContent = EncodingUtil.convertToHex(blob.valueOf((string.format(documemtType, new list<String> {'documentType', documentType}))));
                        
            string fileHexContent = EncodingUtil.convertToHex(blob.valueOf(''+string.format(headerFile, new list<String> {'file',att.Name,att.ContentType})))+
                                                EncodingUtil.convertToHex(fileToUploadBlob)+
                                                EncodingUtil.convertToHex(blob.valueof(footer));
                                                
            Blob body = EncodingUtil.convertFromHex(documentHexContent+fileHexContent);
            String endPoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c: config.Production_Endpoint_Url__c;
            
            try{            
                
                HttpRequest req = new HttpRequest();            
                            req.setMethod('POST');
                            req.setEndpoint(endPoint+'/customers/'+customerId+'/documents');
                            req.setHeader('Authorization', 'Bearer '+config.Access_Token__c);
                            req.setHeader('Accept', 'application/vnd.dwolla.v1.hal+json');
                            req.setHeader('Cache-Control', 'no-cache');
                            req.setHeader('Content-Type','multipart/form-data; boundary='+formBoundary);
                            req.setBodyAsBlob(body);
                            req.setHeader('Content-Length',string.valueOf(req.getBodyAsBlob().size()));
                            HttpResponse res = new HttpResponse();
                if(Test.isRunningTest()){
                    
                    if(isPostiveTesting){
                        res.setHeader('Location', 'https://api-sandbox.dwolla.com/documents/11fe0bab-39bd-42ee-bb39-275afcc050d0');
                        res.setStatusCode(201);
                    }else{
                        res.setBody('Error');
                        res.setStatusCode(401);    
                    }
                }else{
                    res = new Http().send(req);
                }
                
               if(res.getStatusCode() != 201){
                   
                   DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                   errorWrapper.errorMsg = '\n Error: \n StatusCode:'+res.getStatusCode()+ '\n Message :'+res.getBody();
                   errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'/customers/'+customerId+'/documents'+
                        '\n\n headerParameterMap :\n Authorization: '+'Bearer '+config.Access_Token__c+'\nAccept: application/vnd.dwolla.v1.hal+json'+
                        '\nCache-Control: no-cache \nContent-Type: multipart/form-data; boundary='+formBoundary;
                   errorWrapper.source = 'DwollaDocumentCalloutHelper.uploadDwollaDocument';
                   DwollaErrorResponseHandler.saveDwollaErrorResponse(new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});    
               
                   return new DwollaDatasheetDocController.Response(false, 'Unable to Save document for Dwolla Customer. Please contact to Admin.', null);
                   
               }else{
                   
                   return DwollaDatasheetDocController.saveDataSheetInfo(new DwollaDatasheetAttachment__c(Id = dataSheetAttachmentId, Dwolla_Document_Id__c = res.getHeader('Location').substringAfter('documents/')));
                   //return new DwollaDatasheetDocController.Response(true, 'File successfully Uploaded at Dwolla', null);
               }
               
            }catch(Exception exp){
                
                System.debug('exp ::::'+exp.getMessage());
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+exp.getMessage();
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'/customers/'+customerId+'/documents'+
                    '\n\n headerParameterMap :\n Authorization: '+'Bearer '+config.Access_Token__c+'\nAccept: application/vnd.dwolla.v1.hal+json'+
                    '\nCache-Control: no-cache \nContent-Type: multipart/form-data; boundary='+formBoundary;
                errorWrapper.source = 'DwollaDocumentCalloutHelper.uploadDwollaDocument';
                DwollaErrorResponseHandler.saveDwollaErrorResponse(new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
                
                return new DwollaDatasheetDocController.Response(false, 'Exception occurred while Uploading file. Please contact to Admin. \n'+exp.getMessage(), null);
            }
                        
        }
        return new DwollaDatasheetDocController.Response(false, 'Please provide proper information to upload file. Please contact to Admin.', null);
    }
    
    /**
    *@Purpose :get custom setting record
    */
    public static DwollaAPIConfiguration__c getAPIConfiguration(){
        return DwollaAPIConfiguration__c.getOrgDefaults();
    }
}