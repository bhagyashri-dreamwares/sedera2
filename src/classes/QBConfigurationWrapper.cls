/*
*@Purpose     : - Quick book API integration app configuration wrapper        
*@Author      : - Dreamwares
*@Created Date : 18/07/2018
*/
public with sharing class QBConfigurationWrapper {
    public String consumerKey;
    public String consumerSecret;
    public String accessToken;
    public String refreshToken;
    public String realmId;
    
    public String authorizationURL;
    public String accessTokenURL;
    public String apiEndpoint;
    public String redirectURI;
    
    public DateTime accessTokenExpiry;
    public DateTime refreshTokenExpiry;
    
    public String state;
    public String currentQuickBookOrgName;
    
    
    /*
*@Description      : Fetch Quick Book app configuration from custom setting
*/
    public QBConfigurationWrapper(String currentQuickBoocOrg){
        currentQuickBookOrgName = currentQuickBoocOrg;
        System.debug('currentQuickBoocOrg :: '+currentQuickBoocOrg);
        Quick_Book_App_Configuration__c qbConfiguration = Quick_Book_App_Configuration__c.getInstance(currentQuickBookOrgName);
        
        this.consumerKey            = qbConfiguration.Consumer_Key__c;
        this.consumerSecret         = qbConfiguration.Consumer_Secrete__c;
        this.refreshToken         = qbConfiguration.Refresh_Token__c;
        this.realmId        = qbConfiguration.QB_Company_Id__c;
        
        this.authorizationURL       = qbConfiguration.Authorization_URL__c;
        this.accessTokenURL         = qbConfiguration.Access_Token_URL__c;
        this.redirectURI      = qbConfiguration.Redirect_URI__c;
        
        this.accessTokenExpiry    = qbConfiguration.Access_Token_Expiry__c;
        this.refreshTokenExpiry    = qbConfiguration.Refresh_Token_Expiry__c;
        
        // Access token is upto 700 - 1000 chars
        // Text field can store upto 255 chars, Hence access token is splitted into 4 fields
        String accessToken = '';
        if(String.isNotBlank(qbConfiguration.Access_Token_Part_1__c)){
            accessToken = accessToken + qbConfiguration.Access_Token_Part_1__c;
        }
        if(String.isNotBlank(qbConfiguration.Access_Token_Part_2__c)){
            accessToken = accessToken + qbConfiguration.Access_Token_Part_2__c;
        }   
        if(String.isNotBlank(qbConfiguration.Access_Token_Part_3__c)){
            accessToken = accessToken + qbConfiguration.Access_Token_Part_3__c; 
        }   
        if(String.isNotBlank(qbConfiguration.Access_Token_Part_4__c)){
            accessToken = accessToken + qbConfiguration.Access_Token_Part_4__c; 
        }   
        this.accessToken = accessToken;
        
        // For sandbox
        if(qbConfiguration.Is_Sandbox__c){
            this.apiEndpoint = qbConfiguration.Sandbox_Endpoint_Url__c + qbConfiguration.QB_Company_Id__c;
        }
        else{
            // Production Endpoint Url
            this.apiEndpoint = qbConfiguration.Production_Endpoint_Url__c + qbConfiguration.QB_Company_Id__c; 
        }
    }
    
    // Save tokens to custom setting
    public static void saveTokens(OAuth2TokenResponse qbTokenResponse,String currentQuickBookOrgNameStatic){
        Quick_Book_App_Configuration__c qbConfiguration = Quick_Book_App_Configuration__c.getInstance(currentQuickBookOrgNameStatic);
        
        if(qbConfiguration != null){
            
            qbConfiguration.Access_Token_Part_1__c ='';
            qbConfiguration.Access_Token_Part_2__c ='';
            qbConfiguration.Access_Token_Part_3__c ='';
            qbConfiguration.Access_Token_Part_4__c ='';
            
            // Access token is upto 700 - 1000 chars
            // Text field can store upto 255 chars, Hence access token is splitted into 4 fields
            if(qbTokenResponse.access_token.length() > 0 ){ 
                qbConfiguration.Access_Token_Part_1__c = qbTokenResponse.access_token.substring(0, Math.min(255, qbTokenResponse.access_token.length()));
            }
            
            if ( qbTokenResponse.access_token.length() > 255){ 
                qbConfiguration.Access_Token_Part_2__c = qbTokenResponse.access_token.substring(255, Math.min(510, qbTokenResponse.access_token.length()));
            }
            
            if ( qbTokenResponse.access_token.length() > 510){ 
                qbConfiguration.Access_Token_Part_3__c = qbTokenResponse.access_token.substring(510,  Math.min(765, qbTokenResponse.access_token.length()));
            }
            
            if ( qbTokenResponse.access_token.length() > 765){ 
                qbConfiguration.Access_Token_Part_4__c = qbTokenResponse.access_token.substring(765, Math.min(1020, qbTokenResponse.access_token.length()));
            }
            
            
            // Set Expiry of access token
            qbConfiguration.Access_Token_Expiry__c     = DateTime.now().addSeconds(qbTokenResponse.expires_in);
            qbConfiguration.Refresh_Token__c       = qbTokenResponse.refresh_token;
            qbConfiguration.Refresh_Token_Expiry__c   = DateTime.now().addSeconds(qbTokenResponse.x_refresh_token_expires_in);
            
            try{
                update qbConfiguration;
            }
            catch(Exception e){
                System.debug('Exception occured while saving Quickbooks tokens :: ' + e.getMessage());
            }
            
        }
    }
    /*
    updateQBConfig(JSON.serialize(qbConfiguration));
    
    @future
    private static void updateQBConfig(String qbConfigurationStr){
    try{
    Quick_Book_App_Configuration__c qbConfiguration = (Quick_Book_App_Configuration__c)JSON.deserialize(qbConfigurationStr,Quick_Book_App_Configuration__c.Class);
    update qbConfiguration;
    }
    catch(Exception e){
    System.debug('Exception occured while saving Quickbooks tokens :: ' + e.getMessage());
    }
    }*/
    
    /**
    *@Description    : Saves company Id(realmId)
    */
    public static void saveCompanyId(String realmId,String currentQuickBookOrgNameStatic){
        Quick_Book_App_Configuration__c qbConfiguration = Quick_Book_App_Configuration__c.getInstance(currentQuickBookOrgNameStatic);
        
        if(qbConfiguration != null){
            qbConfiguration.QB_Company_Id__c = realmId;
            
            try{
                update qbConfiguration;
            }
            catch(Exception e){
                System.debug('Exception occured while saving Quickbooks company id :: ' + e.getMessage());
            }
        }
    }
}