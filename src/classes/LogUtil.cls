/**  
 * @Purpose      : Class to create Log records
 * @Author       : Dreamwares 
 * @Created Date : 27/09/2018
 */
public class LogUtil {
    
    /**
    * @Purpose  : Function to create Log
    */    
    public static Log__c createLog(String programArea, Exception exp, String message, String query, String type){
        
        Log__c log = new Log__c();
        log.Date__c = System.now();
        log.Source__c = String.isNotBlank(programArea) ? programArea : '';
        
        if (exp != null){
            log.Exception_Type__c = exp.getTypeName();
            log.Description__c = 'Cause :'  + exp.getCause() + '\n StackTrace :' + exp.getStackTraceString() 
                                 + '\n Message :'+exp.getMessage() + '\n \n Input data :\n' + message;    
        }else{
            log.Description__c = String.isNotBlank(message) ? message : ''; 
        }
        
        log.Query__c = String.isNotBlank(query) ? query : ''; 
        log.Type__c = String.isNotBlank(type) ? type : ''; 
                        
        return log;
    }
    
    /**
    * @Purpose  : Function to insert Log
    */
    public static void saveLogs(List<Log__c> logList){
                
        if (logList != null){
            try{
                insert logList;
            }catch(Exception e) {
                System.debug('Exception occured at line number : ' + e.getLineNumber() + ' & error : ' + e.getMessage()); 
            }
        }
    }
}