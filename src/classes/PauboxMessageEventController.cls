/**
* @Purpose: Controller for pauboxMessage shared record event
*/
public class PauboxMessageEventController{
    
    /**
    *    To store email current status on ENOs shared record
    */
    @InvocableMethod(label='Fetch Email Status' description='Fetch email status from Paubox using Tracking Id')
    public static List<String> storeEmailStatus(List<String> objectIdList){
        
        if(!objectIdList.isEmpty()){
            
            List<Paubox_Message__c> pauboxMessageRecordList = PauBoxEmailHelper.getpauboxMessageRecord(objectIdList);   
            if(!pauboxMessageRecordList.isEmpty()){
                
            }
            Database.executeBatch(new PauboxMessageEventbatch(pauboxMessageRecordList), 1);
           
        }
        
        return new List<String>();
    }
}