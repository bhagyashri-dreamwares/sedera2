/**
*@Purpose: Batch class to fetch pauboxMessage related email status from paubox API
*@Date: 21/08/2018
*/
public without sharing class PauboxMessageEventBatch implements Database.Batchable<Paubox_Message__c>, Database.AllowsCallouts, Database.Stateful{

   public List<Paubox_Message__c> pauboxMessageRecordList;
   public List<Paubox_Message__c> pauboxMessageList;

   public pauboxMessageEventbatch(List<Paubox_Message__c> pauboxMessageRecordList){
      
      this.pauboxMessageList = new List<Paubox_Message__c>();
      this.pauboxMessageRecordList = new List<Paubox_Message__c>();
      this.pauboxMessageRecordList.addAll(pauboxMessageRecordList);       
   }

   public Iterable<Paubox_Message__c> start(Database.BatchableContext BC) {
        return pauboxMessageRecordList;
   }

   public void execute(Database.BatchableContext BC, List<Paubox_Message__c> pauboxMessageRecordList){
   
       for(Paubox_Message__c pauboxMessageRecord :pauboxMessageRecordList){            
            
            Paubox_Message__c pauboxMessage = PauBoxEmailHelper.getEmailStatus(pauboxMessageRecord.Tracking_ID__c, pauboxMessageRecord.Id);
            if(pauboxMessage != null){
                pauboxMessageList.add(pauboxMessage);
            }            
       }  
   }

   public void finish(Database.BatchableContext BC){
       
       if(!pauboxMessageList.isEmpty()){
           PauBoxEmailHelper.savepauboxMessageRecord(pauboxMessageList);
       }
   }
}