public class Queueable_UpdateAccountsinbulk implements Queueable {

    public Map <id,Account> AccsMap;
    public Boolean skipTriggerQB;
    public Boolean skipTriggerNonQB;
    public Set <Id> AccIds;


    public Queueable_UpdateAccountsinbulk(List<Account> Accs,Boolean skipTriggerQB,Boolean skipTriggerNonQB)
    {
      AccsMap=new Map <id,Account>();
     for(Account acc:Accs)
     {
        AccsMap.put(acc.Id,Acc);
     }
      AccIds=AccsMap.keyset();      
      this.skipTriggerNonQB=skipTriggerNonQB;
      this.skipTriggerQB=skipTriggerQB;
    }
    public void execute(QueueableContext context) {
        Integer Batchsize=0;
        if(test.isrunningtest())
        BatchSize=2000;
        else
        Batchsize=Integer.valueOf(Label.BatchSize_UpdateAccountsinbulk);
       database.executebatch(new Batch_UpdateAccountsinbulk(AccsMap,AccIds,skipTriggerQB,skipTriggerNonQB),BatchSize);
        
    }
}