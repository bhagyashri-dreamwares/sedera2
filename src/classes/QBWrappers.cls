/** 
 *@Author       : Dreamwares
 *@Created Date : 19/07/2018
 *@Purpose      : Wrapper classes for quick book 
 *@               
 */

public with sharing class QBWrappers {
    
    public class QBInvoiceDTO{
        
        public Double Deposit;
        public Boolean AllowIPNPayment;
        public Boolean AllowOnlinePayment; 
        public Boolean AllowOnlineCreditCardPayment;
        public Boolean AllowOnlineACHPayment;
        public String domain;
        public Boolean sparse;
        public String Id;
        public String SyncToken;
        public MetaData MetaData;
        public String DocNumber;
        public Date TxnDate;
        public CurrencyRef CurrencyRef;
        public List<QBLinkedTxn> LinkedTxn;
        public List<QBLine> Line;
        public QBTxnTaxDetail TxnTaxDetail;
        public QBCustomerRef CustomerRef;
        public QBCustomerMemo CustomerMemo;
        public QBSalesTermRef SalesTermRef;
        public BillEmail BillEmail;
        public Date DueDate;
        public Double TotalAmt;
        public Boolean ApplyTaxAfterDiscount;
        public String PrintStatus;
        public String EmailStatus;
        public Double Balance;
        public BillEmail BillEmailBcc;
        public BillEmail BillEmailCc;
        public CustomField[] CustomField;
        
        public QBInvoiceDTO(){
            MetaData = new MetaData();
            CurrencyRef = new CurrencyRef();
            LinkedTxn = new List<QBLinkedTxn>();
            Line = new List<QBLine>();
            TxnTaxDetail = new QBTxnTaxDetail();
            CustomerMemo = new QBCustomerMemo();
            SalesTermRef = new QBSalesTermRef();        
            CustomerRef = new QBCustomerRef();
            BillEmail = new BillEmail();
            BillEmailBcc = new BillEmail();
            BillEmailCc = new BillEmail();
        }
    }
    
    public class QBLinkedTxn{
        public String TxnId;    
        public String TxnType;  
        public QBLinkedTxn(){}
    }
    
    public class BillEmail{
        public String Address;
        public BillEmail(){}

        public BillEmail(String email){
            this.Address = email;
        }
    }
    /*public class EmailAddress {
        public String Address;
        public EmailAddress(){}

        public EmailAddress(String email){
            this.Address = email;
        }
    }*/
    
    
    public class QBLine{
        public String Id;
        public Double LineNum;
        public String Description;
        public Double Amount;
        public String DetailType;
        public QBSalesItemLineDetail SalesItemLineDetail;
        public QBSubTotalLineDetail SubTotalLineDetail; 
        public QBDiscountLineDetail DiscountLineDetail;
        public QBLinkedTxn[] LinkedTxn;
        public QBLineEx LineEx;
        
        public QBLine(){
            SalesItemLineDetail = new QBSalesItemLineDetail();
            SubTotalLineDetail = new QBSubTotalLineDetail();
            DiscountLineDetail = new QBDiscountLineDetail();
        }
        
        public QBLine( String Id, Double LineNum, String Description, Double Amount, String DetailType,
                       QBSalesItemLineDetail SalesItemLineDetail, QBSubTotalLineDetail SubTotalLineDetail, QBDiscountLineDetail QBDiscountLineDetail){          
            this.Id = Id;
            this.LineNum = LineNum;
            this.Description = Description;
            this.Amount = Amount;
            this.DetailType = DetailType;
            this.SalesItemLineDetail = SalesItemLineDetail;
            this.SubTotalLineDetail = SubTotalLineDetail;
            this.DiscountLineDetail = DiscountLineDetail;               
        }
    }
    
    public class QBSalesItemLineDetail{
        public QBItemRef ItemRef;
        public Double UnitPrice;
        public Double Qty;
        public QBTaxCodeRef TaxCodeRef;
        public String ServiceDate;
        
        public QBSalesItemLineDetail(){
            ItemRef = new QBItemRef();
            TaxCodeRef = new QBTaxCodeRef();
        }
        
        public QBSalesItemLineDetail(QBItemRef ItemRef, Double UnitPrice, Double Qty, QBTaxCodeRef TaxCodeRef){
            this.ItemRef = ItemRef;
            this.UnitPrice = UnitPrice;
            this.Qty = Qty;
            this.TaxCodeRef = TaxCodeRef;
        }
    }
    
    public class QBDiscountLineDetail{
        public Boolean PercentBased;
        public Decimal DiscountPercent;  

        public QBDiscountLineDetail(){
            PercentBased = false;
        }     

        public QBDiscountLineDetail(Boolean percentBased, Decimal discountPercent){            
            this.DiscountPercent = discountPercent;this.PercentBased = percentBased;
        }
    }
    
    public class QBItem{
        public String Id;
        public string Name;
        public string Sku;
        public string SyncToken;
        public string Description;
        public Double UnitPrice;
        public Double PurchaseCost;
        public string Type;
        public string domain = 'QBO'; 
        public Boolean Taxable = false; //false
        public Boolean Active;
        public string FullyQualifiedName;
        public Boolean TrackQtyOnHand;
        public QBItemRef IncomeAccountRef;
        public QBItemRef AssetAccountRef;
        public QBItemRef ExpenseAccountRef;
        public Date InvStartDate;
        public Double QtyOnHand;
        
        public QBItem(){
            IncomeAccountRef = new QBItemRef();
            AssetAccountRef = new QBItemRef();
            ExpenseAccountRef = new QBItemRef(); 
        }
    }     
    
    public class QBItemRef{
        public String value;
        public String name; 
        
        public QBItemRef(){}
        
        public QBItemRef(String value, String name){
            this.value = value;
            this.name = name;
        } 
    }
    
    public class QBTaxCodeRef{
        public String value;
        
        public QBTaxCodeRef(){}
        
        public QBTaxCodeRef(String value){
            this.value = value;
        } 
    }

    public class QBSubTotalLineDetail{
        
    }
    
    public class QBTxnTaxDetail{
        public Decimal TotalTax;
        public TxnTaxCodeRef TxnTaxCodeRef;
        
        public QBTxnTaxDetail(){
            TxnTaxCodeRef = new TxnTaxCodeRef();
        }
        
        public QBTxnTaxDetail(Decimal TotalTax){
            this.TotalTax = TotalTax;
        }
    }
    
    public class TxnTaxCodeRef {
        public String value;
        
        public TxnTaxCodeRef(){}
        
        public TxnTaxCodeRef(String value){
            this.value = value;
        } 
    }

    public class QBCustomerRef{
        public String value;
        public String name;
        
        public QBCustomerRef(){}
        
        public QBCustomerRef(String value,  String name){
            this.value = value;this.name = name;
        }
    }
    
    public class QBCustomerMemo{
        public String value;
        
        public QBCustomerMemo(){}
        
        public QBCustomerMemo(String value){
            this.value = value;
        }        
    }
    
    public class QBSalesTermRef{
        public String value;
        
        public QBSalesTermRef(){}
        
        public QBSalesTermRef(String value){
            this.value = value;
        }        
    }
    
    public class RequestTokenWrapper{
        public String token;
        public String tokenSecret;
        public Boolean callbackConfirmed;
        public String problem;
        
        public RequestTokenWrapper(){
            
        }
        
        public RequestTokenWrapper(String token, String tokenSecret, Boolean callbackConfirmed, String problem){
            this.token              = token;
            this.tokenSecret        = tokenSecret;
            this.callbackConfirmed  = callbackConfirmed;
            this.problem            = problem;
        }
    }

    public class QueryResponse{
        public List<QBCustomer> Customer;
        public List<QBInvoiceDTO> Invoice;
        public Integer startPosition;
        public Integer maxResults;
        
        public QueryResponse(){
            Customer = new List<QBCustomer>();Invoice = new List<QBInvoiceDTO> ();
            
        }
    }
    
    public class QBChartAccount{
        public String Name;
        public Boolean SubAccount;
        public String FullyQualifiedName;
        public Boolean Active;
        public String Classification;
        public String AccountSubType;
        public double CurrentBalance;
        public double CurrentBalanceWithSubAccounts;
        public CurrencyRef CurrencyRef;
        public QBObjectReference ParentRef;
        public String Id;
        public String SyncToken;

        public QBChartAccount(){
            ParentRef = new QBObjectReference();
            CurrencyRef = new CurrencyRef();
        }        
    }
    
    public class QBSalesReceipt{
        public String domain;
        public Boolean sparse;
        public String Id;
        public String SyncToken;
        public MetaData MetaData;
        public String DocNumber;
        public Date TxnDate;
        public CurrencyRef CurrencyRef;
        public List<QBLine> Line;
        public QBTxnTaxDetail TxnTaxDetail;
        public QBCustomerRef CustomerRef;
        public QBCustomerMemo CustomerMemo;
        public Address BillAddr;
        public Address ShipAddr;
        public Double TotalAmt;
        public Boolean ApplyTaxAfterDiscount;
        public String PrintStatus;
        public String EmailStatus;
        public Double Balance;
        public QBDepositToAccountRef DepositToAccountRef;
        public BillEmail BillEmail;
        public String PrivateNote;
        
        public QBSalesReceipt(){
            MetaData = new MetaData();
            CurrencyRef = new CurrencyRef();
            Line = new List<QBLine>();
            TxnTaxDetail = new QBTxnTaxDetail();
            CustomerMemo = new QBCustomerMemo();
            CustomerRef = new QBCustomerRef();
            DepositToAccountRef = new QBDepositToAccountRef();
            BillEmail = new BillEmail();
        }
    }
    
    public class Response{
        public QueryResponse QueryResponse;
        public String time_qb;
    }

    public class QBCustomer{
        public String Title;
        public Boolean Taxable;
        public Boolean Job;
        public Boolean BillWithParent;
        public Decimal Balance;
        public Decimal BalanceWithJobs;
        public CurrencyRef CurrencyRef;
        public String PreferredDeliveryMethod;
        public Boolean TDSEnabled;
        public String domain;
        public Boolean sparse;
        public String Id;
        public String GivenName;
        public String MiddleName;
        public String SyncToken;
        public String ResaleNum;
        // public MetaData MetaData;
        public String FamilyName;
        public String FullyQualifiedName;
        public String CompanyName;
        public String DisplayName;
        public String PrintOnCheckName;
        public String Suffix;
        public Boolean Active;
        public String Notes;
        
        // Addresses
        public Address BillAddr;
        public Address ShipAddr;
        
        // Phones
        public Phone PrimaryPhone;
        public Phone AlternatePhone;
        public Phone Mobile;
        public Phone Fax;
        public QBParentRef ParentRef;
        // Emails
        public Email PrimaryEmailAddr;

        // Web Addresses
        public WebAddr WebAddr;      // Website WebAddr

        public QBCustomer(){
            /*BillAddr = new Address();
            ShipAddr = new Address();
            PrimaryPhone = new Phone();
            AlternatePhone = new Phone();
            Mobile = new Phone();
            Fax = new Phone();*/
            //ParentRef = new QBParentRef();
        }
    }

    public class Email{
        public String Address;

        public Email(){}

        public Email(String address){
            this.Address = address;
        }
    }

    public class WebAddr{
        public String URI;

        public WebAddr(){}

        public WebAddr(String URI){
            this.URI = URI;
        }
    }

    public class Phone{
        public String FreeFormNumber;

        public Phone(){}

        public Phone(String no){
            this.FreeFormNumber = no;
        }
    }
    public class Address{
        public String Id;
        public String Line1;
        public String Line2;
        public String City;
        public String Country;
        public String CountrySubDivisionCode;
        public String PostalCode;

        public Address(){
            Line1 = '';
            Line2 = '';
        }

        public Address(String Line1, String City, 
                        String Country, String CountrySubDivisionCode, 
                        String PostalCode){
            this();
            this.Id = Id;
            this.Line1 = Line1;
            this.City = City;
            this.Country = Country;
            this.CountrySubDivisionCode = CountrySubDivisionCode;
            this.PostalCode = PostalCode;

        }
    }

    public class MetaData {
        public String CreateTime;
        public String LastUpdatedTime;
    }
    
    public class QBDepositToAccountRef{
        public String value;
        public String name;
        
        public QBDepositToAccountRef(){}
        
        public QBDepositToAccountRef(String value,  String name){
            this.value = value;this.name = name;
        }
    }

    public class CurrencyRef {
        public String value;
        public String name;
    }

    public class QBObjectReference{
        public String value;
        public String name;

        public QBObjectReference(){}
        
        public QBObjectReference(String name, String value){
            this.name = name;
            this.value = value;
        }
    }
    
    public class QBCreditMemo {
        public Double RemainingCredit;  //0
        public String domain;   //QBO
        public boolean sparse;
        public String Id;   //212
        public String SyncToken;    //2
        public MetaData MetaData;
        public CustomField[] CustomField;
        public String DocNumber;    //1088
        public String TxnDate;  //2017-09-21
        public CurrencyRef CurrencyRef;
        public QBLine[] Line;
        public QBTxnTaxDetail TxnTaxDetail;
        public QBCustomerRef CustomerRef;
        public QBCustomerMemo CustomerMemo;
        public Address BillAddr;
        public Double TotalAmt; //2
        public boolean ApplyTaxAfterDiscount;
        public String PrintStatus;  //NeedToPrint
        public String EmailStatus;  //EmailSent
        public BillEmail BillEmail;
        public Double Balance;  //0
        public DeliveryInfo DeliveryInfo;
    }
    
    public class CustomField {
        public String DefinitionId; //1
        public String Name; //Crew #
        public String Type; //StringType
        public String StringValue; //StringType
    }
    
    public class DeliveryInfo {
        public String DeliveryType; //Email
        public String DeliveryTime; //2017-09-21T06:18:45-07:00
    }
    
    public class QBPayment {
        public QBCustomerRef CustomerRef;
        public QBDepositToAccountRef DepositToAccountRef;
        public QBPaymentMethodRef PaymentMethodRef;
        public Decimal TotalAmt;    //2600
        public Decimal UnappliedAmt;    //0
        public boolean ProcessPayment;
        public String domain;   //QBO
        public boolean sparse;
        public String Id;   //214
        public String SyncToken;    //0
        public MetaData MetaData;
        public String TxnDate;  //2017-09-21
        public CurrencyRef CurrencyRef;
        public QBLine[] Line;
    }
    
    
    public class QBPaymentMethodRef {
        public String value;    //1
    }
    
    public class QBParentRef {
        public String value;    //1
        
        public QBParentRef(){
        }
        
        public QBParentRef(String val){
            this.value=val;
        }
    }
    
    public class QBLineEx {
        public QBany[] qbany;
    }
    public class QBany {
        public String name; //{http://schema.intuit.com/finance/v3}NameValue
        public String declaredType; //com.intuit.schema.finance.v3.NameValue
        public String scope;    //javax.xml.bind.JAXBElement$GlobalScope
        public QBvalue value;
        public boolean nil;
        public boolean globalScope;
        public boolean typeSubstituted;
    }
    public class QBvalue {
        public String Name; //txnId
        public String Value;    //181
    }
}