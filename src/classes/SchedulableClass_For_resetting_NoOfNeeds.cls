/**
TestClass:  TestSchedulableClass_For_resetting_Needs
Purpose  :  Resetting number of needs on Account as well as on contact after n(1,2,3...) years of enrollment date in addition to           
            locking existing case's IUA  
**/
public class SchedulableClass_For_resetting_NoOfNeeds implements schedulable {


    public void execute(schedulablecontext c) {

        /**** Pull all the Accounts and linked cases(Yearly_reset=True) Reaching Reset Date(formula field) ****/
        try {

            list <Account> AccountList_With_Linked_Cases;

            if (!test.isRunningTest()) {
                AccountList_With_Linked_Cases = [select id, Adding_years_to_enrollment_date__c, (Select id, sharable_need__c, Sedera_Parent_Account__c, contactid, initial_unsharable_amount__c from CASES where yearly_reset_flag__c = TRUE) from account where Reset_Date__c !=
                    null and recordType.name = 'Member'
                    and Reset_Date__c =: system.now().date() and subscription_status__c in ('Active', 'Pending start date', 'Pending Cancellation')
                ];
            } else {
                AccountList_With_Linked_Cases = [select id, Adding_years_to_enrollment_date__c, (Select id, sharable_need__c, Sedera_Parent_Account__c, contactid, initial_unsharable_amount__c from CASES where yearly_reset_flag__c = TRUE) from account where Reset_Date__c !=
                    null and recordType.name = 'Member'
                    and subscription_status__c in ('Active', 'Pending start date', 'Pending Cancellation')
                ];
            }
            system.debug(AccountList_With_Linked_Cases);
            list <Account> Accounts_To_Be_Updated = new list <Account>();
            set <id> Set_Of_Contact_ids = new set <id> ();
            list <contact> Contact_List_To_Be_Updated = new List <contact>();
            case ca;
            list <case>Case_List_To_Be_Updated = new list <case>();
            for (Account acc: AccountList_With_Linked_Cases) {

                for (case cas: acc.cases) {
                    ca = new case (id = cas.id);
                    ca.yearly_reset_flag__c = False; //Set Yearly Reset false so that they are not counted in Number of needs irrespective Of its sharable needs value
                    ca.case_locked__c = true; //Lock the case,Its IUA will never be changed Again
                    ca.lock_IUA__c = cas.initial_unsharable_amount__c;
                    Case_List_To_Be_Updated.add(ca);
                    if (cas.contactid != null) {
                        Set_Of_Contact_ids.add(cas.contactid);
                    }
                }

                Account a = new Account(id = acc.id);
                a.Number_of_Cases__c = 0;//Number of cases linked to all its contacts
                a.Adding_years_to_enrollment_date__c = acc.Adding_years_to_enrollment_date__c + 1; //Set the next resetting date
                Accounts_To_Be_Updated.add(a);
            }

            for (id id: Set_Of_Contact_ids) {
                Contact con = new Contact(id = id);
                con.number_of_needs__c = 0; //Set number Of needs to Zero
                Contact_List_To_Be_Updated.add(con);
            }

            if (Case_List_To_Be_Updated.size()> 0) {
                UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 1;
                update Case_List_To_Be_Updated;
                UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
            }

            if (Accounts_To_Be_Updated.size()> 50) {
                system.debug('Triggering Queueable');
                system.EnqueueJob(new Queueable_UpdateAccountsinbulk(Accounts_To_Be_Updated, true, true));
            } else if (Accounts_To_Be_Updated.size()> 0) {
                UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
                UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
                Update Accounts_To_Be_Updated;
                UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
                UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 0;
            }

            if (Contact_List_To_Be_Updated.size()> 0) {
                update Contact_List_To_Be_Updated;
            }

            if (test.isRunningTest()) {
                integer i = 1 / 0;
            }

        } catch (exception e) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {
                'alakshay@sedera.com'
            };
            message.subject = 'Error Processing Records in class SchedulableClass_For_resetting_NoOfNeeds ';
            message.plainTextBody = 'Please contact your system Administrator';
            Messaging.SingleEmailMessage[] messages =
                new List <Messaging.SingleEmailMessage> {
                    message
                };
            if (!test.isrunningtest())
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

        }
    }


}