/*  
@Purpose      : Test class for DwollaAccountTriggerHandler.
*/

@isTest
public class DwollaAccountTriggerHandlerTest {
    /*  
	@Purpose      : Create Test Data.
	*/
    
    @TestSetup
    public static void setup(){
        
        Id recordTypeIdAccountEmp = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account accountRecord = new Account();
        accountRecord.Name='Test Account';
        accountRecord.Create_Dwolla_Customer__c=true;
        accountRecord.Invoice_Email_Address__c='test@test.com';
        accountRecord.Dwolla_ID__c='12345'+accountRecord;
        accountRecord.Enrollment_Date__c=system.today();
        accountRecord.Member_Discount_Tier_Manual__c='T2';
        accountRecord.Member_MEC_Product__c='test';
        accountRecord.recordtypeid = recordTypeIdAccountEmp;
        insert accountRecord;
    }
    
    /*  
	@Purpose      : Test Method for createDwollaCustomer Method.
	*/
    @isTest
    static void createDwollaCustomerTest(){
        
        List<Account> accountList = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                                     Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c, Member_MEC_Product__c
                                     FROM Account
                                     WHERE Name LIKE 'Test Account%'];
        Test.startTest();
        
        DwollaAccountTriggerHandler.sendPlidLinkToCustomer(accountList, true);
        
        Test.stopTest();
    }
}