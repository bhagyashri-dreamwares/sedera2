@isTest
public class DwollaDatasheetControllerTest {

    @TestSetup
    static void makeData(){

        Account account = new Account(Name = 'test account', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId());
        insert account;

        Dwolla_Datasheet__c dwollaDataSheet = new Dwolla_Datasheet__c(SSN__c = '123-46-7890',
            EIN__c = '00-0000000', Controller_SSN__c = '123-46-7890',
            Email_Address__c = 'abc@test.com', Account__c = account.Id);
        insert dwollaDataSheet;

        //custom setting
        /*DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today() + 600;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj;*/
        
        PlaidAPIConfiguration__c plaidApiRecord = new PlaidAPIConfiguration__c(Client_Key__c = '5b6c8018a06f890011076a0a',
                                                                               Endpoint__c = 'https://sandbox.plaid.com', 
                                                                               Is_Sandbox__c=true,
                                                                               Plaid_Link__c='https://cs21.salesforce.com/servlet/servlet.ExternalRedirect?url=https%3A%2F%2Fcdn.plaid.com%2Flink%2Fv2%2Fstable%2Flink-initialize.js',
                                                                               Public_Key__c='694fb48cf78d39ec9ae2069e4379eb',
                                                                               Secret_Key__c='ca1a06681cecdd1c65af35b9da79eb',
                                                                               Valid_Bank_Account_Type__c='');
        insert plaidApiRecord;
        
        //insert custom setting record
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'test@test.com';
        config.Site_Url__c = 'Test@test.com';
        
        insert config;

        Dwolla_Business_Class_Translation__c translationRecord = new Dwolla_Business_Class_Translation__c();
        translationRecord.Dwolla_Business_Class_Name__c = 'Test';
        translationRecord.Dwolla_Business_Class_Value__c = 'Value';
        insert TranslationRecord;
    }

    @isTest
    static void getPageDataTest(){

        Account account = [SELECT Id
            FROM Account
            LIMIT 1];

        Test.startTest();

        DwollaDatasheetController.getPageData(account.Id);

        Test.stopTest();
    }

    @isTest
    static void createDatasheetRecordTest1(){
        
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today() - 600;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj;

        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));

        Account account = [SELECT Id
            FROM Account
            LIMIT 1];

        Dwolla_Datasheet__c dwollaDataSheet = [SELECT Id
            FROM Dwolla_Datasheet__c
            LIMIT 1];

        DwollaDatasheetController.DwollaDataSheetWrappper wrapper = new DwollaDatasheetController.DwollaDataSheetWrappper();
        wrapper.recordId = dwollaDataSheet.Id;
        wrapper.accountId = account.Id;
        wrapper.address1 = 'test address 1';
        wrapper.address2 = 'test address 2';
        wrapper.businessClassification = 'Test';
        wrapper.businessType = 'LLC';
        wrapper.certified = true;
        wrapper.certifiedDatetime = System.now();
        wrapper.city = 'test city';
        wrapper.controllerAddress1 = 'test controller address 1';
        wrapper.controllerAddress2 = 'test controller address 2';
        wrapper.controllerAddress3 = 'test controller address 3';
        wrapper.controllerCity = 'test controller city';
        wrapper.controllerDateofBirth = System.today();
        wrapper.controllerFirstName = 'test controller first name';
        wrapper.controllerLastName = 'test controller last name';
        wrapper.controllerPostalCode = '1234567890';
        wrapper.controllerSSN = '123456789012';
        wrapper.controllerState = 'OH';
        wrapper.controllerTitle = 'test controller title';
        wrapper.dateofBirth = System.today();
        wrapper.DBAName = 'test dba name';
        wrapper.EIN = '1234567890';
        wrapper.emailAddress = 'abc@example.com';
        wrapper.firstName = 'test first name';
        wrapper.lastName = 'test last name';
        wrapper.postalCode = '1234567890';
        wrapper.SSN = '123456789012';
        wrapper.State = 'OH';

        Test.startTest();
		DwollaDatasheetCalloutHelper.isPositiveTest = true;
        DwollaDatasheetController.createDatasheetRecord(JSON.serialize(wrapper));

        Test.stopTest();
    }
    
    @isTest
    static void createDatasheetRecordTest2(){

        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today() + 600;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj;

        Account account = [SELECT Id
            FROM Account
            LIMIT 1];

        DwollaDatasheetController.DwollaDataSheetWrappper wrapper = new DwollaDatasheetController.DwollaDataSheetWrappper();
        wrapper.accountId = account.Id;
        wrapper.address1 = 'test address 1';
        wrapper.address2 = 'test address 2';
        wrapper.businessClassification = 'Test';
        wrapper.businessType = 'LLC';
        wrapper.certified = true;
        wrapper.certifiedDatetime = System.now();
        wrapper.city = 'test city';
        wrapper.controllerAddress1 = 'test controller address 1';
        wrapper.controllerAddress2 = 'test controller address 2';
        wrapper.controllerAddress3 = 'test controller address 3';
        wrapper.controllerCity = 'test controller city';
        wrapper.controllerDateofBirth = System.today();
        wrapper.controllerFirstName = 'test controller first name';
        wrapper.controllerLastName = 'test controller last name';
        wrapper.controllerPostalCode = '1234567890';
        wrapper.controllerSSN = '123456789012';
        wrapper.controllerState = 'OH';
        wrapper.controllerTitle = 'test controller title';
        wrapper.dateofBirth = System.today();
        wrapper.DBAName = 'test dba name';
        wrapper.EIN = '1234567890';
        wrapper.emailAddress = 'abc@example.com';
        wrapper.firstName = 'test first name';
        wrapper.lastName = 'test last name';
        wrapper.postalCode = '1234567890';
        wrapper.SSN = '123456789012';
        wrapper.State = 'OH';

        Test.startTest();

        DwollaDatasheetController.createDatasheetRecord(JSON.serialize(wrapper));

        Test.stopTest();
    }

    @isTest
    static void uploadAttachmentTest(){

        Dwolla_Datasheet__c dwollaDataSheet = [SELECT Id
            FROM Dwolla_Datasheet__c
            LIMIT 1];

        String parameters = '{"parentId": "'
            + dwollaDataSheet.Id
            + '", "attachmentId": null, "attachmentBody": "test attachment body", "attachmentName": "test attachment name", "attachmentType": "emailAttachment"}';

        Test.startTest();

        DwollaDatasheetController.uploadAttachment(parameters);

        Test.stopTest();
    }
    
    @isTest
    static void updateAccountRecordTest(){
        
        Account account = [SELECT Id,
                Name
            FROM Account 
            LIMIT 1];
            
        Dwolla_Datasheet__c dwollaDatasheet = [SELECT Id 
            FROM Dwolla_Datasheet__c 
            LIMIT 1];
        
        Test.startTest();
        
        DwollaDatasheetController.updateAccountRecord(String.valueOf(account.Id), 'test', 'test', account.Name, String.valueOf(dwollaDatasheet.Id));
        
        Test.stopTest();
    }
    
    @isTest
    static void constructorTest(){
        
        Account account = [SELECT Id 
            FROM Account 
            LIMIT 1];
        
        ApexPages.currentPage().getParameters().put('id', account.Id);
        
        Test.startTest();
        
        DwollaDatasheetController controller = new DwollaDatasheetController();
        
        Test.stopTest();
    }
}