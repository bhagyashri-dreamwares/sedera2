public class Batch_UpdateMembers implements Database.batchable <sobject> {
    public Map <id,Account> memAccsToUpdateMap;
    public Boolean IsDeleteProductMS;
    public Boolean IsDeleteProductHCS;
    public Set <Id> memAccIds;

    public Batch_UpdateMembers(Map <id, Account> memAccsMap, Set <id> AccsIds, Boolean IsDeleteHCS, Boolean IsDeleteMS) {
        memAccIds = AccsIds;
        memAccsToUpdateMap = memAccsMap;
        IsDeleteProductHCS = IsDeleteHCS;
        IsDeleteProductMS = IsDeleteMS;
    }

    public Database.QueryLocator start(Database.BatchableContext info) {
        return Database.getQueryLocator('Select id from Account where id in: memAccIds');
    }

    public void execute(Database.BatchableContext info, List <Account> scope) {
        String ErrorIds = '';
        if (IsDeleteProductHCS)
            UtilityClass_For_Static_Variables.DeleteProductHCS = true;
        if (IsDeleteProductMS)
            UtilityClass_For_Static_Variables.DeleteProductMS = true;

        List <Account> AccListUpdate = new List <Account> ();
        for (Account a: scope) {
            AccListUpdate.add(memAccsToUpdateMap.get(a.Id));
            ErrorIds = ErrorIds + a.id + '';
        }
        try {
            if (AccListUpdate.size()> 0)
                Update AccListUpdate;
            if(test.isrunningtest())
              integer i=1/0;
        }
        Catch(Exception e) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {
                'alakshay@sedera.com',
                'saurav.sundriyal@gmail.com'
            };
            message.subject = 'Error Processing Records!!!!!!';
            message.plainTextBody = ErrorIds + '\n\n' + e.getMessage();
            Messaging.SingleEmailMessage[] messages =
                new List <Messaging.SingleEmailMessage> {
                    message
                };
            if(!test.isrunningtest())
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
           
        }

    }

    public void finish(Database.BatchableContext info) {

    }

}