public class FSMLogger{

 public static void LogErrorMessage(Exception ex)
    {
        LogMessage(null,ex.getStackTraceString()+' -FSM',  ex.GetMessage() , 'Error',null,null,'',false);

    }
    
    public static void LogErrorMessage(Exception ex, string additionalInformation)
    {
        LogMessage(null,ex.getStackTraceString()+' -FSM', ex.GetMessage() + ' | Additional Information: ' + additionalInformation, 'Error',null,null,'',false);

    }
    
    public static void LogInformationalMessage(string message)
    {
        LogMessage(null,getStackTrace()+' -FSM', message, 'Informational',null,null,'',false);
    }
    
    private static String getStackTrace()
    {
        string message = '';
        try { throw new DmlException(); }
        catch (DmlException e)
        {
            system.debug(e.getstacktracestring());
            return e.getStackTraceString().substringAfter('\n').substringAfter('\n');
        }
    }
    
    
    
    public static void LogErrorMessage(String sourceComponent,Exception ex,Boolean IsNotifyAdminChecked)
    {
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error',null,null,'',IsNotifyAdminChecked);
        throwError(ex);

    }
    
    public static void LogErrorMessage(String sourceComponent,Exception ex)
    {
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error',null,null,'',false);
        throwError(ex);

    }
    
    public static void LogErrorMessage(String sourceComponent,Exception ex,List<Sobject> NewRecords,Boolean IsNotifyAdminChecked)
    {
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error', JSON.serialize(NewRecords),null,'',IsNotifyAdminChecked);
        throwError(ex);

    }
    
    public static void LogErrorMessage(String sourceComponent,Exception ex,List<Sobject> NewRecords, List<Sobject> OldRecords,string triggerEvent,Boolean IsNotifyAdminChecked)
    {
       if(OldRecords!=null){
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error', JSON.serialize(NewRecords), JSON.serialize(OldRecords),triggerEvent,IsNotifyAdminChecked);
       }
       else{ 
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error', JSON.serialize(NewRecords),null,triggerEvent,isNotifyAdminChecked);
       }
        throwError(ex);

    }
    
    public static void LogErrorMessage(String sourceComponent,Exception ex,List<Sobject> NewRecords, List<Sobject> OldRecords,string triggerEvent)
    {
       
       if(OldRecords!=null){
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error', JSON.serialize(NewRecords), JSON.serialize(OldRecords),triggerEvent,false);
       }
       else{ 
        LogMessage(sourceComponent,ex.getStackTraceString(),  ex.GetMessage() , 'Error', JSON.serialize(NewRecords),null,triggerEvent,false);
       }
        throwError(ex);

    }
    
    private static void LogMessage(string sourceComponent,string source, string message, string type,string newRecs,string oldRecs,string triggerEvent,Boolean IsNotifyAdminChecked)
    {
        FSMLogEvent__e log_event = new FSMLogEvent__e();
        log_event.FSMLogDateTime__c=datetime.now();
        log_event.sourceComponent__c=sourceComponent;
        log_event.Message__c=message;
        log_event.Source__c=source;
        log_event.fsmlogtype__c =type;
        log_event.Logged_User__c=userinfo.getName()+' '+'('+userinfo.getuserid()+')';
        log_event.New_Record__c=newRecs;
        log_event.Old_Record__c=oldRecs;
        log_event.triggerEvent__c=triggerEvent;
        log_event.Notify_Admin__c=IsNotifyAdminChecked;
        Database.SaveResult sr = EventBus.publish(log_event);
    }
    
    private static void throwError(Exception ex){
      throw ex;
    }
 
}