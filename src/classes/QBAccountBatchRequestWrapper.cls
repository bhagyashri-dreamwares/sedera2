/*
 *@Purpose    :
 *@Author    : Dreamwares
 *@Created Date : 19/07/2018
 */
public with sharing class QBAccountBatchRequestWrapper {
  public List<QBBatchAccountObject> BatchItemRequest;

  public QBAccountBatchRequestWrapper (){
    BatchItemRequest = new List<QBBatchAccountObject>();
  }

  public class QBBatchAccountObject{
    public String bId;
    public String operation;
    public QBWrappers.QBCustomer Customer;
    
    public QBBatchAccountObject(){
      Customer = new QBWrappers.QBCustomer();
    }
    
    public QBBatchAccountObject(String bId, String operation){
      this();
      this.bId = bId;
      this.operation = operation;
    }
  }
}