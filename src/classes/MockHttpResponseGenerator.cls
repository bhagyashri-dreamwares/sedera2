/**
* @Purpose      : - MockHttpResponseGenerator will ganerate fake response for test class DwollaOAuthCallbackControllerTest.
*/
@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock{
    // Implement this interface method
    
    integer flag=0;
    
    /**
	* @Purpose      : - constructor set value for flage parameter in MockHttpResponseGenerator.
    * Parameters	: - inputflag flag for specific condition.
	*/
    global MockHttpResponseGenerator(integer inputflag){
        
        //based on flage value comming as parameter the response will set.
        flag = inputflag;
    }
    
    /**
	* @Purpose      : - Create fake response.
	*/
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        if(flag == 1){
            //flag = 1 set all right response.
            // Create a fake response
            
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }');
            res.setStatusCode(200);
        }
        if(flag == NULL){
            //flage = NULL set response as NULL. 
            res.setStatusCode(404);
        }
        return res;
    }
}