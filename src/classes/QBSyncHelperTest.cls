@isTest
public class QBSyncHelperTest {
    static Invoice__c createInvoice( ){
        return new Invoice__c( Account__c = [SELECT Id FROM Account LIMIT 1].Id,
                               Submission_Status__c = 'Approved');
    }
    
    static Invoice_Line_Item__c createInvoiceLineItem( Id invoceId, Product2 product){
        return new Invoice_Line_Item__c(Invoice__c = invoceId, 
                                        Price__c=10,Quantity__c=1,
                                       // Line_Item_Total__c=10, 
                                        Description__c='Test',
                                        QBO_Product__c = product.Id,
                                        QB_Line_Item_Id__c ='71-1'); 
    }
    
    static Account createAccount(String accName,String billingEmail){
        
        Account recordAccount1 = new Account(
            Name = accName,
            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
            ShippingStreet = '230 NE US Hwy 27',
            ShippingCity = 'Williston',
            ShippingState = 'Florida',
            ShippingCountry = 'United States',
            Invoice_Email_Address__c = billingEmail,
            //Sync_to_QBO__c = true,
            //QB_Sync_Token__c = 'QB sysnc Token',
            Phone = '7896541230',
            Fax = '(123) 487-7960',
            Description ='Bulk Records' );
        
        return recordAccount1;
    }
    
    @testSetup
    static void setupTestData(){
        List<Account> accounts = new List<Account>();
        for(Integer index = 0; index < 10; index++){
            accounts.add(createAccount('Test Account'+index,'test@gamil.com'));
        }
        insert accounts;
        List<Invoice__c> invoices = new List<Invoice__c>();
        for(Integer index = 0; index < 10; index++){
            invoices.add(createInvoice());
        }
        insert invoices;
        
        Product2 recordProduct = new Product2(Name = 'Product',
                                            Quickbooks_ID__c='1');
        insert recordProduct;
        
        System.assertEquals(10, [SELECT Id FROM Invoice__c].size());
        List<Invoice_Line_Item__c> invoiceLineItems = new List<Invoice_Line_Item__c>();
        for(Integer index = 0; index < 10; index++){
            invoiceLineItems.add(createInvoiceLineItem(invoices[0].Id, recordProduct));
        }
        insert invoiceLineItems;       
        
    }
    
    @isTest
    static void testGetInvoiceIdToLineItemsMap(){
        Test.startTest();
        //String date1 = '2017-12-21T03:50:00.1200-05:00';
        //QBSyncHelper.dateFromString(date1);
        List<Invoice_Line_Item__c> invoiceLineItems = [ SELECT Id, Invoice__c FROM Invoice_Line_Item__c ];
        List<Id> ids = new List<Id>();
        ids.add(invoiceLineItems[0].Invoice__c);
        QBSyncHelper.getInvoiceIdToLineItemsMap(ids);
    Test.stopTest();
    }
    
    @isTest
    static void testGetRelatedCustomer(){
        Test.startTest();
        //String date1 = '2017-12-21T03:50:00.1200-05:00';
        //QBSyncHelper.dateFromString(date1);
        List<Account> accounts = [ SELECT Id FROM Account ];
        List<Id> customerListIds = new List<Id>();
        for(Account account : accounts){
            customerListIds.add(account.Id);
        }
        QBSyncHelper.getRelatedCustomer(customerListIds);
    Test.stopTest();
    }
    
    @isTest
    static void testGetCustomerMap(){
        Test.startTest();
        //String date1 = '2017-12-21T03:50:00.1200-05:00';
        //QBSyncHelper.dateFromString(date1);
        List<Account> accounts = [ SELECT Id, QuickBooks_Customer_ID__c FROM Account ];
        List<String> customerListIds = new List<String>();
        for(Account account : accounts){
            customerListIds.add(account.QuickBooks_Customer_ID__c);
        }
        QBSyncHelper.getCustomerMap(customerListIds);
    Test.stopTest();
    }
    
    @isTest
    static void testGetItemIdToItemMap(){
        Test.startTest();
        //String date1 = '2017-12-21T03:50:00.1200-05:00';
        //QBSyncHelper.dateFromString(date1);
        List<Product2> products = [ SELECT Id FROM Product2 ];
        List<String> itemIds = new List<String>();
        for(Product2 product : products){
            itemIds.add(product.Id);
        }
        QBSyncHelper.getItemIdToItemMap(itemIds);
    Test.stopTest();
    }
    
    @isTest
    static void testGetQBItemIdToItemIdMap(){
        Test.startTest();
        //String date1 = '2017-12-21T03:50:00.1200-05:00';
        //QBSyncHelper.dateFromString(date1);
        List<Product2> products = [ SELECT Id, Quickbooks_ID__c FROM Product2 ];
        List<String> itemIds = new List<String>();
        for(Product2 product : products){
            itemIds.add(product.Quickbooks_ID__c);
        }
        QBSyncHelper.getQBItemIdToItemIdMap(itemIds);
    Test.stopTest();
    }
    
    @isTest
    static void dateFromStringTest(){
        Test.startTest();
        String date1 = '2017-12-21T03:50:00.1200-05:00';
        QBSyncHelper.dateFromString(date1);
        Test.stopTest();
    }
}