global class Sendsms

{

   public String phNumber{get;set;}

   public String smsBody{get;set;}
   
   public String Source{get;set;}
   
   public String RecordId{get;set;}
   
   public String Reason{get;set;}
   
   public String Description{get;set;}
   
   public Integer StatusCode{get;set;}

   public Boolean IsSuccess{get;set;}
   
   public String Name{get;set;}
   
   String accountSid;

   string token;

   String fromPhNumber;


   public sendsms(){

     // phNumber ='+'+Apexpages.currentpage().getparameters().get('phNumber');

       accountSid =Label.TwilioAccountSID;

       token = Label.Twilio_Token;
       fromPhNumber = Label.Twilio_Phone_Number;

   }

 public Integer processSms(){
      
       try{
       HttpRequest req = new HttpRequest();

       req.setEndpoint('https://api.twilio.com/2010-04-01/Accounts/'+accountSid+'/Messages.json');

       req.setMethod('POST');

       String VERSION  = '3.2.0';

       req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);

       req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);

       req.setHeader('Accept', 'application/json');

       req.setHeader('Accept-Charset', 'utf-8');

       req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf(accountSid+':' +token)));

       req.setBody('To='+EncodingUtil.urlEncode(phNumber,'UTF-8')+'&From='+EncodingUtil.urlEncode(fromPhNumber,'UTF-8')+'&Body='+smsBody);


       system.debug('mmmmmmmmmmmmmHTTP REQUESTmmmmmmmmmmmm'+req.getBody());

       Http http = new Http();
       Organization Org=[select isSandbox from organization limit 1];
      
       if(!(Org.isSandbox)){
       HTTPResponse res= http.send(req);

       System.debug(res.getBody());
       System.debug(res.getHeaderKeys());
       
       return res.getStatusCode();
       
       }else{
       
          return 201;
       }
    }Catch(exception e){
        return 400;      
    }   
       return 400;
  


   }

  

}