public class Sch_SendRemindersForEnrol implements Schedulable {

    public void execute(SchedulableContext SC) {

       try{
       
        Map <id, Account> AccsToUpdateMap = new Map <id, Account> ();
        List <SendSMS> ReminderSMS = new List <SendSMS> ();
        Account Act;
        SendSMS sms;
        Date enrolStartDate;
        Date enrolEndDate;


        /**** In renewal - Incomplete reminders ****/

        for (Account acc: [Select RecordTypeId, parent_product__c, Account_employer_name__c, teladoc_direct__c, Dependent_status__c,
                          member_discount_tier_manual__c, SecondMD__c, Subscription_Status__c, Iua_chosen__c, primary_Age__c,
                          Membership_Stage__c, Account_employer_name__r.Open_enrollment_ends__c, Account_employer_name__r.Open_Enrollment_Starts__c, 
                          Account_Employer_name__r.Employer_SMS_Preferences__c, Account_Employer_name__r.Text_Principles_Reminder__c,
                          Account_Employer_name__r.Text_Enrollment_Starts_Integration__c, sub_employer__c, sub_Employer__r.Sub_Employer_SMS_Preference__c, 
                          sub_Employer__r.Text_Mid_Enrollment_Integration__c, sub_Employer__r.Text_Mid_Enrollment__c, Account_Employer_name__r.Text_Mid_Enrollment_Integration__c,
                          Account_Employer_name__r.Text_Mid_Enrollment__c, sub_Employer__r.Text_Enrollment_Starts_Integration__c, sub_Employer__r.Text_Enrollment_Starts__c,
                          Account_Employer_name__r.Text_Enrollment_Starts__c, sub_Employer__r.Text_Enrollment_Ends_Integration__c, sub_Employer__r.Text_Enrollment_Ends__c, 
                          Account_Employer_name__r.Text_Enrollment_Ends_Integration__c, Account_Employer_name__r.Text_Enrollment_Ends__c, id, primary_phone_number__c,
                          CreatedById from Account where recordType.name = 'Member'
                          
                          /* We are pulling both incomplete and complete records because we need to do product recalculation on the enrollment end date*/
                         
                          and (Membership_Stage__c = 'In Renewal - Incomplete' or Membership_Stage__c = 'In Renewal - complete')
                          and account_employer_name__r.Open_enrollment_ends__c>= today

            ])

        {
            enrolStartDate = acc.Account_employer_name__r.Open_Enrollment_Starts__c;
            enrolEndDate = acc.Account_employer_name__r.Open_Enrollment_Ends__c;


            if (acc.account_employer_name__r.Open_enrollment_ends__c != date.today()) {
                if (acc.Membership_Stage__c == 'In Renewal - Incomplete') {
                    sms = new SendSMS();
                    if (acc.account_employer_name__r.Open_enrollment_starts__c == date.Today()) {

                        if (acc.sub_Employer__c != null) {
                            if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                                sms.smsBody = acc.CreatedByid == system.label.Integration_User ? acc.sub_Employer__r.Text_Enrollment_Starts_Integration__c : acc.sub_Employer__r.Text_Enrollment_Starts__c;
                            }
                        } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                            sms.smsBody = acc.CreatedByid == system.label.Integration_User ? acc.Account_Employer_name__r.Text_Enrollment_Starts_Integration__c : acc.Account_Employer_name__r.Text_Enrollment_Starts__c;
                        }
                        sms.Reason = 'Asking recipient to complete memberShip stage to In renewal - complete on employer\'s enrollment startdate';
                        sms.Name = 'In Renewal - Incomplete (First SMS)';
                        sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                        sms.RecordId = acc.Id;
                        if (acc.Primary_Phone_Number__c!=null){
                         sms.phNumber = acc.Primary_Phone_Number__c;
                         ReminderSMS.add(sms);
                       }
                    }
                    else If((enrolStartDate.addDays((enrolStartDate.daysbetween(enrolEndDate)) / 2)) == date.Today()) {

                        if (acc.sub_Employer__c != null) {
                            if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                                sms.smsBody = acc.CreatedByid == system.label.Integration_User ? acc.sub_Employer__r.Text_Mid_Enrollment_Integration__c : acc.sub_Employer__r.Text_Mid_Enrollment__c;
                            }
                        } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                            sms.smsBody = acc.CreatedById == system.label.Integration_User ? acc.Account_Employer_name__r.Text_Mid_Enrollment_Integration__c : acc.Account_Employer_name__r.Text_Mid_Enrollment__c;
                        }

                        sms.Reason = 'Asking recipient to complete memberShip stage to In renewal - complete, '+string.Valueof((enrolEndDate.daysbetween(enrolStartDate)) / 2)+' days from employer\'s enrollment startdate';
                        sms.Name = 'In Renewal - Incomplete (Second SMS)';
                        sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                        sms.RecordId = acc.Id;
                        if (acc.Primary_Phone_Number__c!=null){
                         sms.phNumber = acc.Primary_Phone_Number__c;
                         ReminderSMS.add(sms);
                       }
                    }
                    
                }
            } else {

                if (acc.Membership_Stage__c == 'In Renewal - Incomplete') {
                    sms = new SendSMS();
                    if (acc.sub_Employer__c != null) {
                        if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                            sms.smsBody = acc.CreatedByid == system.label.Integration_User ? acc.sub_Employer__r.Text_Enrollment_Ends_Integration__c : acc.sub_Employer__r.Text_Enrollment_Ends__c;
                        }
                    } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                        sms.smsBody = acc.CreatedById == system.label.Integration_User ? acc.Account_Employer_name__r.Text_Enrollment_Ends_Integration__c : acc.Account_Employer_name__r.Text_Enrollment_Ends__c;
                    }

                    sms.Reason = 'Auto Update of memberShip stage to In renewal - complete on employer\'s enrollment end date';
                    sms.Name = 'In Renewal - Incomplete (Third SMS)';
                    sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                    sms.RecordId = acc.Id;
                    if (acc.Primary_Phone_Number__c!=null){
                     sms.phNumber = acc.Primary_Phone_Number__c;
                     ReminderSMS.add(sms);
                    }
                }
                /**** In renewal - complete - Employers Enrollment end date - Product calculation ****/
                acc.membership_stage__c = 'In renewal - complete';
                AccsToUpdateMap.put(acc.Id, acc);

            }
        }

        if (AccsToUpdateMap.size()> 0) {
            Account_Helper_class.MCSproductcalculation(AccsToUpdateMap.values(), null);
        }


        /****        New Enrollment - Incomplete   reminders     *****/


        for (Account acc: [Select createddate,Primary_Member_principles__c,Principles_Signed_On__c,MCS_Summary_Flow__c ,enrollment_date__c  , RecordTypeId, parent_product__c, Account_employer_name__c, Membership_Stage__c,
                          Account_employer_name__r.Open_enrollment_ends__c, Account_employer_name__r.Open_Enrollment_Starts__c, Account_Employer_name__r.Employer_SMS_Preferences__c,
                          Account_Employer_name__r.Text_Principles_Reminder__c, sub_employer__c, sub_Employer__r.Sub_Employer_SMS_Preference__c, sub_Employer__r.Text_Principles_Reminder__c,
                          id, primary_phone_number__c, sub_Employer__r.Text_Account_Deletion__c,sub_Employer__r.Text_Application_Reminder__c,Account_Employer_name__r.Text_Application_Reminder__c, Account_Employer_name__r.Text_Account_Deletion__c from Account where recordType.name = 'member'
                          and membership_stage__c = 'New Enrollment - Incomplete'
                          and subscription_status__c = 'Application in process'
                          and MCS_Summary_Flow__c = 'Incomplete'
                          and enrollment_date__c>= today
            ])

        {

            /****     Primary Member principles - incomplete        *****/
            if (acc.Primary_Member_principles__c == 'Incomplete' ) {
                sms=new sendSms();
                if (acc.enrollment_date__c == date.Today()) {

                    acc.subscription_status__c = 'Incomplete';
                    
                    if (acc.sub_Employer__c != null) {
                        if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                            sms.smsBody = acc.sub_Employer__r.Text_Account_Deletion__c;
                        }
                    } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                        sms.smsBody = acc.Account_Employer_name__r.Text_Account_Deletion__c;
                    }
                    sms.Reason = 'Notifying recipients that your account will be marked as incomplete today as it hits the enrollment date';
                    sms.Name = 'New Enrollment - Incomplete';
                    sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                    sms.RecordId = acc.Id;
                    if (acc.Primary_Phone_Number__c!=null){
                     sms.phNumber = acc.Primary_Phone_Number__c;
                     ReminderSMS.add(sms);
                    }
                } else {
                    if (acc.sub_Employer__c != null) {
                        if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                            sms.smsBody = acc.sub_Employer__r.Text_Principles_Reminder__c;
                        }
                    } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                        sms.smsBody = acc.Account_Employer_name__r.Text_Principles_Reminder__c;

                    }
                    sms.Reason = 'Asking recipient to complete New Enrollment - Principles signed on - Null';
                    sms.Name = 'New Enrollment - Incomplete';
                    sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                    sms.RecordId = acc.Id;                    
                    if (acc.Primary_Phone_Number__c!=null){
                     sms.phNumber = acc.Primary_Phone_Number__c;
                     ReminderSMS.add(sms);
                    }
                }

                  
            }
            /****   After Principles signed on is completed       *****/
            else if (acc.Primary_Member_principles__c == 'COMPLETED' ) {

                sms=new sendSms();
                if (acc.enrollment_date__c == date.Today()) {

                    acc.subscription_status__c = 'Incomplete';
                    AccsToUpdateMap.put(acc.Id,acc);
                    if (acc.sub_Employer__c != null) {
                        if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                            sms.smsBody = acc.sub_Employer__r.Text_Account_Deletion__c;
                        }
                    } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                        sms.smsBody = acc.Account_Employer_name__r.Text_Account_Deletion__c;
                    }
                    sms.Reason = 'Notifying recipients that your account will be marked as incomplete today as it hits the enrollment date';
                    sms.Name = 'New Enrollment - Incomplete';
                    sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                    sms.RecordId = acc.Id;
                    if (acc.Primary_Phone_Number__c!=null){
                     sms.phNumber = acc.Primary_Phone_Number__c;
                     ReminderSMS.add(sms);
                    }

                } else if (acc.Principles_signed_on__c !=null && (acc.Principles_signed_on__c.addDays(1) == date.Today() || Math.mod((((acc.Principles_signed_on__c.addDays(1)).daysbetween(Date.today()))), 3) == 0)) {

                    if (acc.sub_Employer__c != null) {
                        if (acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                            sms.smsBody = acc.sub_Employer__r.Text_Application_Reminder__c;
                        }
                    } else if (acc.Account_Employer_name__r.Employer_SMS_Preferences__c != null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c == 'All Communications') {
                        sms.smsBody = acc.Account_Employer_name__r.Text_Application_Reminder__c;
                    }
                    sms.Reason = 'Asking recipient to complete New Enrollment - 3 days SMS';
                    sms.Name = 'New Enrollment - Incomplete';
                    sms.Source = 'Sch_SendRemindersForEnrol Apex Class';
                    sms.RecordId = acc.Id;
                    if (acc.Primary_Phone_Number__c!=null){
                     sms.phNumber = acc.Primary_Phone_Number__c;
                     ReminderSMS.add(sms);
                    }
                }
               
               
            }

            
        }


        if (AccsToUpdateMap.size()> 0 && AccsToUpdateMap.size() <100 && !test.isrunningtest()) {
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
            //UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
            Update AccsToUpdateMap.values();
        } else if (test.isrunningtest() || AccsToUpdateMap.size()>= 100) {
            system.EnqueueJob(new Queueable_UpdateAccountsinbulk(AccsToUpdateMap.values(), true, false));
        }

        if (ReminderSMS.size()> 0) {
            Id JobID2 = system.enQueueJob(new Queueable_SendSMS(ReminderSMS));
        }
       if (test.isRunningTest()) {
        integer i = 1 / 0;
      }
    } catch (exception e) {

            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {
                'alakshay@sedera.com'
            };
            message.subject = 'Error Processing Records in class Sch_ActivateandRenewAcc';
            message.plainTextBody = 'Please contact your system Administrator  '+e.getMessage();
            Messaging.SingleEmailMessage[] messages =
                new List <Messaging.SingleEmailMessage> {
                    message
                };
            if (!test.isrunningtest())
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

        }

    }

}