public class Sch_ActivateAccAndSendRemindersForEnrol implements Schedulable {

    public void execute(SchedulableContext SC) {

        Map<id,Account> AccsToUpdateMap = new Map<id,Account>();
        List <SendSMS> ReminderSMS = new List <SendSMS> ();
        list <Contact> ContactsToUpdate = new list <Contact> ();
        Account Act;
        
        
         /***** If EnrolmentDate=today,Activate Account ***/
        List<Account> AccListEnroll=[Select id, enrollment_date__c, RecordTypeId, parent_product__c, Account_employer_name__c, teladoc_direct__c, Dependent_status__c, member_discount_tier_manual__c, SecondMD__c,Subscription_Status__c, Iua_chosen__c, primary_Age__c, (Select id from Contacts) from Account where recordType.name = 'Member'
            and membership_stage__c = 'New Enrollment - Complete' and enrollment_date__c = today  and subscription_status__c = 'Pending Start Date' ];
       
        Map <Id, Account> MemAccsMap ;
        
        if (AccListEnroll.size()> 0) {
          
            for (Account acc: AccListEnroll)

            {
                acc.health_care_sharing__c = true;
                acc.Subscription_Status__c = 'Active';
                acc.Teladoc_Direct__c = true;
                acc.SecondMD__c = true;               
                acc.activation_date__c = date.Today();
                if (acc.Parent_Product__c == 'Sedera Select') {
                    if (!test.isrunningtest())
                        acc.parentId = Label.Sedera_Select_Accounts_Id;
                    else
                        acc.parentId = [select id from Account where name = 'Sedera Select'
                            limit 1
                        ][0].Id;
                } else if (acc.Parent_Product__c == 'Sedera Access') {
                    if (!test.isrunningtest())
                        acc.parentId = Label.Sedera_Access_Account_id;
                    else
                        acc.parentId = [select id from Account where name = 'Sedera Access'
                            limit 1
                        ][0].Id;

                }
                
                for (Contact con: acc.contacts) {
                    con.enrollment_date__c = acc.enrollment_date__c;
                    ContactsToUpdate.add(con);
                }
                
                AccsToUpdateMap.put(acc.Id,acc);
            }

           //Account_Helper_class.MCSproductcalculation(AccsToUpdateMap.values(),null);
            
        }
        
        /***** In Renewal Member *****/
       List<Account> accRenewalList1= [select id,CreatedById from Account where recordType.name='Member' and (subscription_status__c = 'active' OR  (membership_stage__c = 'New Enrollment - Complete' and enrollment_date__c = today  and subscription_status__c = 'Pending Start Date' )) and Account_employer_name__r.Open_Enrollment_Starts__c=today  ];
            
       if(accRenewalList1.size()>0){
        for (Account acc: accRenewalList1){
          
          if(AccsToUpdateMap.containsKey(acc.Id)){ 
            Act=AccsToUpdateMap.get(acc.Id);            
          }
          else{
            Act=acc;
          }
            Act.Membership_Stage__c = 'In Renewal - Incomplete';
            Act.Primary_Member_Principles__c = 'Incomplete';
            Act.Ancillary_Flow__c = 'Incomplete';
            Act.mcs_summary_flow__c = 'Incomplete';
            if(acc.createdbyId!=system.label.Integration_User){
            Act.adding_dependents__c ='Incomplete';   
            }else{
            Act.adding_dependents__c ='Not applicable';
            }                              
            AccsToUpdateMap.put(Act.Id,Act);
            Act=null;
           
        }
       
      }
      
      /*** Reset Renewal ***/
       List<Account> accRenewalList2= [select renewal_date__c,id,(select id,MCS_Product_Code__c,MCS_Product_amount__c,RecordTypeId,Subscription_Status__c, parent_product__c, Account_employer_name__c, teladoc_direct__c, Dependent_status__c, member_discount_tier_manual__c, SecondMD__c, Iua_chosen__c, primary_Age__c,Account_employer_name__r.Renewal_date__c from Accounts__r where recordType.name='Member' and ( subscription_status__c = 'active' or subscription_status__c = 'pending cancellation') ) from account where recordtype.name='Employer' and Renewal_date__c=today ];
       
       if(accRenewalList2.size()>0){
        List <Account> mcsCal=new list<Account>();
        for(Account acc:accRenewalList2){
        
             
             if(!AccsToUpdateMap.containsKey(acc.Id)){                
               Account emp=new Account(id=acc.id,renewal_date__c=acc.renewal_date__c.addYears(1));
               AccsToUpdateMap.put(acc.id,emp);
             }
              else{
              AccsToUpdateMap.get(acc.Id).renewal_date__c=acc.renewal_date__c.addYears(1);             
             }
         
         
         mcsCal=acc.accounts__r;
         if(mcsCal.size()>0){
           
           Account_Helper_class.MCSproductcalculation(mcscal,null);

         }
         for(Account memAcc:mcsCal){
         
         if(AccsToUpdateMap.containsKey(memAcc.Id)){ 
               AccsToUpdateMap.get(memAcc.Id).mcs_product_code__c=memAcc.mcs_product_code__c;  
               AccsToUpdateMap.get(memAcc.Id).mcs_product_amount__c=memAcc.mcs_product_amount__c;                
             }
             else{
               AccsToUpdateMap.put(memAcc.id,memAcc);
             }                                       
         
         }     
             
       }
                 
    }
        
            
        /***** For Sending Reminders Daily ****/
        Boolean DailySMS=false;
        for (Account acc: [Select Membership_Stage__c ,Primary_Member_Principles__c ,mcs_summary_flow__c,subscription_status__c,Account_employer_name__r.Open_enrollment_ends__c,Account_employer_name__r.Open_Enrollment_Starts__c,Account_Employer_name__r.Employer_SMS_Preferences__c, Account_Employer_name__r.Text_Principles_Reminder__c, Account_Employer_name__r.Text_Application_Reminder__c,sub_employer__c,sub_Employer__r.Sub_Employer_SMS_Preference__c, sub_Employer__r.Text_Principles_Reminder__c, sub_Employer__r.Text_Application_Reminder__c,id, Createddate, Principles_Signed_On__c,primary_phone_number__c,MCS_Completed_On__c from Account where (Parent_Product__c = 'Sedera Access' OR Parent_Product__c = 'Sedera Select')   and recordType.name = 'Member' 
                and ((Primary_Member_Principles__c = 'Incomplete' and MCS_Summary_Flow__c = 'Incomplete' 
                and subscription_status__c = 'Application in process' and Membership_Stage__c = 'In Renewal - Incomplete' 
                ) 
                OR (Membership_Stage__c = 'In Renewal - Incomplete' and mcs_summary_flow__c='Incomplete' and  Primary_Member_Principles__c = 'Incomplete' and subscription_status__c = 'Active'))])

        {
            
            if(acc.Membership_Stage__c =='In Renewal - Incomplete' && acc.Primary_member_principles__c== 'Incomplete' && acc.mcs_summary_flow__c=='Incomplete' && acc.subscription_status__c == 'Active' ){
              /*** Employer renewal daily reminders ***/
             If( (acc.account_employer_name__r.Open_enrollment_ends__c !=null && acc.account_employer_name__r.Open_enrollment_ends__c == date.today() ) || (acc.Account_employer_name__r.Open_Enrollment_Starts__c!=null && ((acc.Account_employer_name__r.Open_Enrollment_Starts__c.daysbetween(Date.today())) + 1) == 30)) {
               if(AccsToUpdateMap.containsKey(acc.Id)){ 
                Act=AccsToUpdateMap.get(acc.Id);            
               }
               else{
                Act=acc;
               }
               Act.Subscription_Status__c = 'Incomplete';
               AccsToUpdateMap.put(acc.Id,Act);
               Act=null;
             }else{
                SendSMS sms = new SendSMS();
                DailySMS=false;
                if (acc.sub_Employer__c!=null){
                if(acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                  DailySMS=true;
                    sms.smsBody = acc.sub_Employer__r.Text_Principles_Reminder__c;
                    sms.phNumber = acc.Primary_Phone_Number__c;
                 } 
                }else if(acc.Account_Employer_name__r.Employer_SMS_Preferences__c!=null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c=='All Communications'){
                  DailySMS=true;
                    sms.smsBody = acc.Account_Employer_name__r.Text_Principles_Reminder__c;
                    sms.phNumber = acc.Primary_Phone_Number__c;
                }
                
                if(DailySMS){
                sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol';
                sms.RecordId=acc.Id;
                sms.Reason='Daily Reminder for enrollment after employer Renewal(Enrollment Date Passes) Date passes';
                sms.Name='Daily Reminder after Employer Renewal Date passes';
                ReminderSMS.add(sms);
                }
             
             }   
           }else{
        
           /*** Principal Signed On daily reminder ***/
            if (((acc.Account_employer_name__r.Open_enrollment_starts__c.daysbetween(Date.today())) + 1) == 30) {
                if(AccsToUpdateMap.containsKey(acc.Id)){ 
                 Act=AccsToUpdateMap.get(acc.Id);           
                 }
                else{
                 Act=acc;
                }
                Act.Subscription_Status__c = 'Incomplete';
                AccsToUpdateMap.put(Act.Id,Act);
                Act=null;

            } else {
                SendSMS sms = new SendSMS();
                DailySMS=false;
                if (acc.sub_Employer__c!=null){
                 if(acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                 DailySMS=true;
                    sms.smsBody = acc.sub_Employer__r.Text_Principles_Reminder__c;
                    sms.phNumber = acc.Primary_Phone_Number__c;
                    sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol - Sub Employer Level';
                  }
                } else if(acc.Account_Employer_name__r.Employer_SMS_Preferences__c!=null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c=='All Communications') {
                 DailySMS=true;
                    sms.smsBody = acc.Account_Employer_name__r.Text_Principles_Reminder__c;
                    sms.phNumber = acc.Primary_Phone_Number__c;
                    sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol - Employer Level';
                }
                
                if(DailySMS){
                sms.RecordId=acc.Id;
                sms.Reason='Daily Reminder to complete Principles Signed On after account is created';
                sms.Name='Principal signed on Daily Reminder';
                ReminderSMS.add(sms);
                }
            }
            
           }

        }


        /***** For Sending Reminders on every 3 day ****/
        Boolean SMSDay3=false;
        for (Account acc: [Select id,Membership_Stage__c,createddate ,Primary_member_principles__c,mcs_summary_flow__c,subscription_status__c,Account_employer_name__r.Open_enrollment_ends__c,Account_employer_name__r.Open_Enrollment_Starts__c, Principles_Signed_On__c, MCS_Completed_On__c, Account_Employer_name__r.Employer_SMS_Preferences__c, Account_Employer_name__r.Text_Principles_Reminder__c, Account_Employer_name__r.Text_Application_Reminder__c, sub_employer__r.Sub_Employer_SMS_Preference__c,sub_employer__c, sub_employer__r.Text_Principles_Reminder__c, sub_employer__r.Text_Application_Reminder__c,primary_phone_number__c from Account where (Parent_Product__c = 'Sedera Access'
                    Or Parent_Product__c = 'Sedera Select')  and recordType.name = 'Member' 
                 and ((Primary_member_principles__c = 'Complete' and MCS_Summary_Flow__c = 'Incomplete' and  subscription_status__c ='Application in process'
                ) OR (Membership_Stage__c = 'In Renewal - Incomplete' and Primary_member_principles__c= 'complete' and mcs_summary_flow__c='Incomplete' and subscription_status__c = 'Active'))
            ])

        {
        
             /**** Employer Renewal 3 day Reminders *****/  
             if(acc.Membership_Stage__c =='In Renewal - Incomplete' && acc.Primary_member_principles__c== 'complete' && acc.mcs_summary_flow__c=='Incomplete' && acc.subscription_status__c == 'Active' ){
                if((acc.account_employer_name__r.Open_enrollment_ends__c !=null &&  acc.account_employer_name__r.Open_enrollment_ends__c == date.today()) || (acc.Account_employer_name__r.Open_Enrollment_Starts__c!=null &&  ((acc.Account_employer_name__r.Open_Enrollment_Starts__c.daysbetween(Date.today())) + 1) == 30)){
                  if(AccsToUpdateMap.containsKey(acc.Id)){ 
                    Act=AccsToUpdateMap.get(acc.Id);            
                   }
                  else{
                    Act=acc;
                   }
                  Act.Subscription_Status__c = 'Incomplete';
                  AccsToUpdateMap.put(Act.Id,Act);
                  Act=null;
                }else if(Math.mod(((acc.Account_employer_name__r.Open_Enrollment_Starts__c.daysbetween(Date.today())) + 1),3)==0){
                    SendSMS sms = new SendSMS();
                    SMSDay3=false;
                    if (acc.sub_Employer__c!=null ){
                    if(acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                        SMSDay3=true;
                        sms.smsBody = acc.sub_Employer__r.Text_Application_Reminder__c;
                        sms.phNumber = acc.Primary_Phone_Number__c;
                        sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol - Sub Employer Level';
                    }
                   } else if(acc.Account_Employer_name__r.Employer_SMS_Preferences__c!=null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c=='All Communications') {
                        SMSDay3=true;
                        sms.smsBody = acc.Account_Employer_name__r.Text_Application_Reminder__c;
                        sms.phNumber = acc.Primary_Phone_Number__c;
                        sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol - Employer Level';
                    }
                    
                    if(SMSDay3){
                    sms.Reason='Enrollment Reminder - Every 3rd Day Reminder SMS after Employer Renewal(Enrollment Date Passes) date Passes ';
                    sms.Name='Employer Renewal Reminder - Every 3rd Day SMS';
                    sms.RecordId=acc.Id;
                    ReminderSMS.add(sms);
                    }
                }
              }else{
            /****  Principal Completed On 3 day Reminders*******/
             if ( ((acc.Account_employer_name__r.Open_enrollment_starts__c.daysbetween(Date.today())) + 1) == 30)  {
                    if(AccsToUpdateMap.containsKey(acc.Id)){ 
                     Act=AccsToUpdateMap.get(acc.Id);           
                    }
                    else{
                     Act=acc;
                    }
                    Act.Subscription_Status__c = 'Incomplete';
                    AccsToUpdateMap.put(Act.Id,Act);
                    Act=null;
                } else if(Math.mod(((acc.Account_employer_name__r.Open_enrollment_starts__c.daysbetween(Date.today())) + 1),3)==0){
                    SendSMS sms = new SendSMS();
                    SMSDay3=false;
                    if (acc.sub_Employer__c!=null ){
                     if(acc.sub_Employer__r.Sub_Employer_SMS_Preference__c) {
                    SMSDay3=true;
                        sms.smsBody = acc.sub_Employer__r.Text_Application_Reminder__c;
                        sms.phNumber = acc.Primary_Phone_Number__c;
                        sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol - Sub employer Level';
                    } 
                   }else if(acc.Account_Employer_name__r.Employer_SMS_Preferences__c!=null && acc.Account_Employer_name__r.Employer_SMS_Preferences__c=='All Communications'){
                    SMSDay3=true;
                        sms.smsBody = acc.Account_Employer_name__r.Text_Application_Reminder__c;
                        sms.phNumber = acc.Primary_Phone_Number__c;
                        sms.Source = 'Sch_ActivateAccAndSendRemindersForEnrol - Employer Level';
                    }
                    
                    if(SMSDay3){                   
                    sms.Reason='Enrollment Reminder - Every 3rd Day reminder SMS after Principal signed On is completed';
                    sms.Name='Principal signed On Reminder - Every 3rd Day SMS';
                    sms.RecordId=acc.Id;
                    ReminderSMS.add(sms);
                    }
                }
                
             
              }  

            }

        

        if (AccsToUpdateMap.size()> 0 && AccsToUpdateMap.size() <100 && !test.isrunningtest()) {
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
            //UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
            Update AccsToUpdateMap.values();
        } else if (test.isrunningtest() || AccsToUpdateMap.size()>= 100) {
            system.EnqueueJob(new Queueable_UpdateAccountsinbulk(AccsToUpdateMap.values(),true,false));
        }
        if (ContactsToUpdate.size()> 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
            Update ContactsToUpdate;
        }
        if (ReminderSMS.size()> 0) {
            Id JobID2 = system.enQueueJob(new Queueable_SendSMS(ReminderSMS));
        }
     
    }
}