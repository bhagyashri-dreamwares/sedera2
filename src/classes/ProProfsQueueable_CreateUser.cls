public class ProProfsQueueable_CreateUser implements Queueable {

    public Set <Id> AccIds;

    public ProProfsQueueable_CreateUser(Set <Id> AccIds)
    {
       this.AccIds = AccIds;
    }
    public void execute(QueueableContext context) {
    
       database.executebatch(new ProProfsBatch_CreateUser(AccIds),1);
        
    }
}