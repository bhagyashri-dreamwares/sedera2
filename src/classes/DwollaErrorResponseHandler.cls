/**
*@Purpose : Handle for DwollaErrorResponse
*@Date : 07/09/2018
*/
public class DwollaErrorResponseHandler{

    /*
    *@Purpose : Create DwollaErrorResponse instance
    */
    public static Dwolla_Error_Response__c createDwollaErrorResponse(DwollaErrorWrapper dwollaErrorWrapper){
    
        Dwolla_Error_Response__c errorRes = new Dwolla_Error_Response__c();
        errorRes.Account__c = dwollaErrorWrapper.accountId;
        errorRes.Error_Message__c = dwollaErrorWrapper.errorMsg;
        errorRes.Error_Occurred_At__c = DateTime.now();
        errorRes.Request_Body__c = dwollaErrorWrapper.requestBody; 
        errorRes.source__c = dwollaErrorWrapper.source;
        
        return errorRes;
    }
    
    /*
    *@Purpose : save DwollaErrorResponse record
    */
    public static void saveDwollaErrorResponse(List<Dwolla_Error_Response__c> errorList){
        
        try{
            
            insert errorList;
        }catch(Exception exp){
            System.debug('Exception :::'+exp.GetMessage());
             LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaErrorResponseHandler.saveDwollaErrorResponse', exp, 'errorList: '+errorList, 
                                                                 '', 'Error'))}); 
        }
    }
    
    public class DwollaErrorWrapper{
        
        public String datasheetId;
        public String accountId;
        public String errorMsg;
        public String requestBody; 
        public String source;
    }
}