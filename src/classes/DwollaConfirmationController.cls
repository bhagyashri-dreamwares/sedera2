/**
* @Purpose: Controller for DwollaConfirmation Page
* @Date: 03/12/2018
*/
public class DwollaConfirmationController{
    
    public Boolean isUserAcceptTerms{get; set;}
    public String fundingSourceId{get; set;}
    
    public DwollaConfirmationController(ApexPages.StandardController controller){
        
        isUserAcceptTerms = false;
        
    }
    
    public PageReference checkTermsAndPlicy(){
    
        String accountId = ApexPages.currentPage().getParameters().get('Id');
        System.debug('accountId :::::'+accountId);
        Account account = getAccountInfo(accountId);
        System.debug('account  :::::'+account );
        
        String isPlaidError = ApexPages.currentPage().getParameters().get('isError');
        String message = ApexPages.currentPage().getParameters().get('message');
        
        if(account != null && account.Is_Terms_And_Policy_Accepted__c
           && account.TermsAndPolicyAcceptedTime__c != null &&
           String.isBlank(isPlaidError)){
            System.debug('In if'); 
            return redirectPage(accountId);
        }
         System.debug('Outif'); 
        return null;
    }
    
    private PageReference redirectPage(String accountId){
        
        PageReference pageRef = new PageReference('/DwollaConfirmation');
        pageRef.setRedirect(false);
        if(isDwollaSheetAvailable(accountId)){
        
            String newPageUrl = getNewPageUrl(accountId);
            System.debug('newPageUrl :::'+newPageUrl);
            if(String.isNotBlank(newPageUrl)){
               
                pageRef = new PageReference(newPageUrl);
                pageRef.setRedirect(true);
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to store your response.'+
                                     ' \n Please contact to Admin!!!'));
            }
        }else{
            
            pageRef = new PageReference('/DwollaDatasheet?id='+accountId);
            pageRef.setRedirect(true);
            
        }
        
        return pageRef;
    }
    
    /**
    * To submit user confirmation
    */
    public PageReference customerConfirmation(){
        
        
        PageReference pageRef = new PageReference('/DwollaConfirmation');
        pageRef.setRedirect(false);

        String accountId = ApexPages.currentPage().getParameters().get('Id');
        
        System.debug('accountId :::'+accountId);
        System.debug('isUserAcceptTerms :::'+isUserAcceptTerms);
        if(String.isNotBlank(accountId) && isUserAcceptTerms){
            
            Account account = new Account(Id = accountId, Is_Terms_And_Policy_Accepted__c = true, 
                                          TermsAndPolicyAcceptedTime__c = DateTime.now());    
            updateAccount(account);
            
            pageRef = redirectPage(accountId);
        }else{            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please check the confirmation box.'));
        }
        return pageRef;       
    }
    
    
    
    
    /**
    * To get Account Info
    */
    private static Account getAccountInfo(String accountId){
        
        try{
            List<Account> accountList = [SELECT Id, Is_Terms_And_Policy_Accepted__c, TermsAndPolicyAcceptedTime__c
                                         FROM Account
                                         WHERE Id = :accountId];
            return !accountList.isEmpty() ? accountList[0] : new Account();
        }catch(Exception exp){
        
            System.debug('Exception :::'+exp.getMessage());
        }
        return new Account();
    }
    
    
    /**
    * To get new page url
    */
    private static String getNewPageUrl(String accountId){
        
        String newPageUrl = '';
        Account account = getAccount(accountId);
        if(account != null){
            
            newPageUrl = Label.Site_Url+'fundingSource?id='+account.Id+'&customerId='+account.Dwolla_ID__c+
                                        '&accountName='+account.Name;

        }
        return newPageUrl;
    }
    
    /**
    * To update current Customer
    */
    private static void updateAccount(Account account){
        
        try{                    
            update account;
        }catch(Exception exp){            
            System.debug('Exception :::'+exp.getMessage());
        }
    }
    
    /**
    * To get current Customer
    */
    private static Account getAccount(String accountId){
        
        try{                    
            return [SELECT Id, Name, Dwolla_ID__c, Is_Terms_And_Policy_Accepted__c, TermsAndPolicyAcceptedTime__c
                    FROM Account
                    WHERE Id = :accountId];
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage());
        }
        return null;
    }
    
    
    /**
    * To check is Dwolla sheet record available for current Account
    */
    private static Boolean isDwollaSheetAvailable(String accountId){
        
        try{
            List<Dwolla_Datasheet__c> dataSheetList = [SELECT Id, Name, Account__c
                                                       FROM Dwolla_Datasheet__c
                                                       WHERE Account__c = :accountId];
            if(!dataSheetList.isEmpty()){
                
                return true;
            }
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage());
        }
        return false;
    }
    
    /**
    * To update Funding source
    */
    public PageReference  updateFundingSource(){
        
        PageReference pageRef = new PageReference('/DwollaConfirmation');
        pageRef.setRedirect(false);
        if(String.isNotBlank(fundingSourceId)){
            try{
                
                Dwolla_Funding_Source__c fundingSource = new Dwolla_Funding_Source__c();
                fundingSource.Id = fundingSourceId;
                fundingSource.On_demand_authorization_opt_in__c = true;
                
                update fundingSource;
            }catch(Exception exp){
                System.debug('Exception :::'+exp.getMessage());
            }
        }
        return null;
    }
    
}