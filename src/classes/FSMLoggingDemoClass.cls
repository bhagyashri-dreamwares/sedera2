public class FSMLoggingDemoClass
{
    public static void FSMLoggingDemoMethod () {

        string className;
        string methodName;
        
        className = 'DevConsole';
        methodName = 'ExampleMethod';
        
        try
        {
            FSMLogger.LogInformationalMessage('Starting method');
            
            //going to force an exception
            decimal d = 1/0;
            FSMLogger.LogInformationalMessage('Ending method');
        }
        catch (Exception ex)
        {
            FSMLogger.LogErrorMessage(ex);
            FSMLogger.LogErrorMessage(ex, 'Additional debug info to help determine cause');
            
            FSMException e = new FSMException ();
            e.SetMessage('Error encountered while processing. Please notify your administrator of this error.');
            //throwing this exception rolls back all the other DML from this apex if it's all in the same application context
            //throwing the exception also invalidates the test, so only throw it if we're not running a test
            if(!Test.isRunningTest()){
                throw e;
            }
        }
    }
}