/**
*@Purpose : Class having methods to Authentication API
*@Date : 06/09/2018
*
*/
public class DwollaOAuth2{

    public object aPIConfiguration;
    
    public DwollaOAuth2(object aPIConfiguration){
        
        this.aPIConfiguration = aPIConfiguration;
    }
    
    /**
    * To get authorize page url
    */
    public PageReference authorize(Map<String, String> pageParameterMap, String endPoint){
        
        String pageUrl = getPageUrl(pageParameterMap, endPoint);     
        
        if(String.isNotBlank(pageUrl)){
            PageReference authorizationPage = new PageReference(pageUrl);
            authorizationPage.setRedirect(true);
            return authorizationPage;
        }
        return null;
    }
    
    
   /**
   *@Purpose: Gets access token with exchanging authorization code
   */
    public Response getAccessToken(String endPoint, Map<String, string> endPointParameterMap, 
                                   Map<String, string> headerParameterMap)
    {
        Response response;
        
        
        // Instantiate a new http object
        Http http = new Http();
        HttpRequest accessTokenRequest = new HttpRequest();
        accessTokenRequest.setEndpoint(endPoint); 
        accessTokenRequest.setMethod('POST');
        String body = urlEncode(endPointParameterMap);
		system.debug('Body = ' + body);
        accessTokenRequest.setBody(body);
        
        for(String key :headerParameterMap.keySet()){            
            accessTokenRequest.setHeader(key, headerParameterMap.get(key));
        }
                                       
        system.debug('Request = ' + accessTokenRequest);
        
        try{
            // Send the request
            HttpResponse accessTokenResponse = http.send(accessTokenRequest);
            System.debug('accessTokenResponse' + accessTokenResponse);
            OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize(accessTokenResponse.getbody(), OAuth2TokenResponse.class);
            System.debug('oAuthResponse::'+oAuthResponse);
            if(accessTokenResponse.getStatusCode() == 200){
                response = new Response(true, '', oAuthResponse);
            }
            else{
                response = new Response(false, '', oAuthResponse);
            }
            
        } catch(Exception e){
             System.debug('@@@@ Exception '+ e.getMessage() +' @@@ '+ e.getLineNumber());
             response = new Response(false, 'Error Occured while Callout : ' + e.getMessage(), null);
             LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaOAuth2.getAccessToken', e, 'endPoint: '+endPoint+
                                                                  '\n\n endPointParameterMap: '+endPointParameterMap+
                                                                  '\n\n headerParameterMap:'+headerParameterMap, 
                                                                 '', 'Error'))});  
        }
        return response;
    }
    
    /**
    * @Purpose : To get page url
    */
    private String getPageUrl(Map<String, String> pageParameterMap, String url){
        
        String pageUrl = '';
        
        if(String.isNotBlank(url)){
            pageUrl = url + '?';
            
            // generate authorization URL
            for(String key :pageParameterMap.keySet()){            
                
                String mapValue = pageParameterMap.get(key);    
                pageUrl += String.isNotBlank(mapValue) ? 
                           key+'='+mapValue+'&' : 
                           '';
            }
            pageUrl = pageUrl.containsAny('&') ? 
                      pageUrl.substringBeforeLast('&') : 
                      pageUrl;
        
        }
        return pageUrl;
    }
    
    private static String urlEncode(Map<String, String> vals) 
    {
        String result = '';
        for(String thisKey : vals.keySet()) {
            result += EncodingUtil.urlEncode(thisKey, 'UTF-8') + '=' + EncodingUtil.urlEncode(vals.get(thisKey), 'UTF-8') + '&';
        }
        return result.removeEnd('&');    
    }
}