/**
* @Purpose      : - Test Class for DwollaOAuthCallbackController.
*/
@isTest
public class DwollaOAuthCallbackControllerTest {
    
    @TestSetup
    /**
    * @Purpose      : - Create Test Data.
    */
    public static void setup(){
        
        // Created a record of DwollaAPIConfiguration__c custom Setting.
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Client_Key__c='IHLoutgdersu7jWYfr9uy2I8PJ9M5bT0GTRweTWKnoLyubrLpo';
        customSettingObj.Secret_Key__c='IHLoutgnfhhnfdgklwusklkjayuhfkkiGTRweTWKnoLyubrLpo';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Access_Token_Expiry__c=system.today()+7;
        insert customSettingObj;
        
        //flage to set the reponse based on condition from MockHttpResponseGenerator.
        Boolean flag;
    }
    
    /**
    * @Purpose      : - Positive Test for handleOAuthCallback method.
    */     
    public static testmethod void handleOAuthCallbackTest(){
        //Set Page Parameters.
        ApexPages.currentPage().getParameters().put('state','Dowlla');
        
        //Mock Callout Set.
        //Parameter 1 gives the all right response from MockHttpResponseGenerator.
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        Test.startTest();
        
        DwollaOAuthCallbackController classobj = new DwollaOAuthCallbackController();
        classobj.handleOAuthCallback();
        
        Test.stopTest();
        
        //Check Response
        DwollaAPIConfiguration__c customSettingObj = [SELECT Access_Token_Expiry__c, Access_Token__c
                                                      FROM DwollaAPIConfiguration__c LIMIt 1];
        
       // system.assertEquals('SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw', customSettingObj.Access_Token__c);
       // system.assertEquals(Datetime.now().addMinutes(55), customSettingObj.Access_Token_Expiry__c);
        
        //assert the messages disply on page.
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean msgFound = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Authorized Successfully!')) msgFound = true;
        }
       // system.assert(msgFound);
    }
    
    /**
    * @Purpose      : - Negative Test for handleOAuthCallback method(page parameters not exists).
    */   
    public static testmethod void handleOAuthCallbackNTestNoParameter(){
        //page parameters are not set for this method.
        
        //Mock Callout Set.
        //Parameter 1 gives the all right response from MockHttpResponseGenerator.
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(1));
        Test.startTest();
        
        DwollaOAuthCallbackController classobj = new DwollaOAuthCallbackController();
        classobj.handleOAuthCallback();
        
        Test.stopTest();
    }
    
    /**
    * @Purpose      : - Negative Test for handleOAuthCallback method(page parameters not match).
    */       
    public static testmethod void handleOAuthCallbackNTestWrongParameter(){
        
        //Wrong page parameters are set for this method.
        ApexPages.currentPage().getParameters().put('state','test');
        
        //Mock Callout Set.
        //Parameter 1 gives the all right response from MockHttpResponseGenerator.
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        Test.startTest();
        
        DwollaOAuthCallbackController classobj = new DwollaOAuthCallbackController();
        classobj.handleOAuthCallback();
        
        Test.stopTest();
    }
    
    /**
    * @Purpose      : - Positive Test for  redirectPage and enableAuth methods.
    */     
    public static testmethod void redirectPageTest(){
        
        Test.startTest();
        
        DwollaOAuthCallbackController classobj = new DwollaOAuthCallbackController();
        classobj.redirectPage();  
        classobj.enableAuth();
        
        Test.stopTest();
    }
    
    /**
    * @Purpose      : - Negative Test for handleOAuthCallback method(Response Fail).
    */   
    public static testmethod void handleOAuthCallbackNTestResFail(){
        
        ApexPages.currentPage().getParameters().put('state','Dowlla');
        
        //Mock Callout Set.
        //Parameter NULL gives the fail response from MockHttpResponseGenerator.
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(Null));
        Test.startTest();
        
        DwollaOAuthCallbackController classobj = new DwollaOAuthCallbackController();
        classobj.handleOAuthCallback();
            
        Test.stopTest();
        
        //assert the messages shown by page 
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean msgFound = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains('Error Occured while Callout : No content to map to Object due to end of input')) msgFound = true;
        }
        system.assert(msgFound);
    }
 
}