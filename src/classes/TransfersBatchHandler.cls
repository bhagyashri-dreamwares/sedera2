/**
* @Purpose : Handler for TransfersBatch class
* @Date : 21/12/2018
*/
public class TransfersBatchHandler{

    /**
    * To transfer Amount from Source to destination in Dwolla
    */
    public static void transferAmount(List<Dwolla_Transfer__c> transferList, DwollaAPIConfiguration__c  config,
                                      List<Dwolla_Error_Response__c> dwollaErrorList){
        
        System.debug('config ::::'+config);  
        JSONGenerator gen;
        
        if(!transferList.isEmpty() && config != null){
            
            for(Dwolla_Transfer__c transferRec :transferList){
                
                gen = getJson(config, transferRec);
                System.debug('gen ::::'+gen );  
                if(gen != null){
                    HttpResponse res = sendRequest(config, transferRec, gen);
                    
                    System.debug('response ::::'+res);
                    transferRec.Transfer_Response__c = res.getBody();
                    System.debug('res.getStatuscode() ::::'+res.getStatuscode() );
                    if(res.getStatuscode() == 201){
                                        
                       transferRec.Dwolla_Transfer_ID__c = res.getHeader('Location').substringAfter('transfers/');
                       transferRec.Transfer_Submitted__c = DateTime.now();  
                       transferRec.Transfer_Status__c = 'Initiated';  
                    }else{
                        transferRec.Transfer_Status__c = 'Failed';
                        DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                        errorWrapper.errorMsg = '\n Response :'+res.getBody();
                        errorWrapper.requestBody = 'Request :\n'+gen.getAsString();
                        errorWrapper.source = 'TransfersBatchHandler.transferAmount';
                        dwollaErrorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
                    } 
                }  
            }            
            // update transfer records
            updateTransfers(transferList);
        }
    }
    
    /**
    * To update Transfers
    */
    private static void updateTransfers(List<Dwolla_Transfer__c> transferList){
    
        try{
            
            update transferList;
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage());
             LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('TransfersBatchHandler.updateTransfers', exp, 'transferList: '+transferList, 
                                                                 '', 'Error'))});   
            
        }
    }
    
    /**
    * To callout for Dwolla transfer
    */
    public static HttpResponse sendRequest(DwollaAPIConfiguration__c  config, Dwolla_Transfer__c transferRec, JSONGenerator gen){
        
        HttpResponse res = new HttpResponse();
        String endpoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c : config.Production_Endpoint_Url__c;
        try{
            if(transferRec != null){ 
            
                Http http = new Http();
                
                HttpRequest req = new HttpRequest();
                req.setTimeout(30000);
                req.setEndpoint(endpoint +'/transfers');
                req.setHeader('Accept', 'application/vnd.dwolla.v1.hal+json');
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('authorization', 'Bearer '+config.Access_Token__c);
                req.setHeader('Idempotency', transferRec.Id);
                req.setMethod('POST');
                req.setBody(gen.getAsString());
                res = http.send(req);
            }
        }catch(Exception exp){
        
            transferRec.Transfer_Status__c = 'Failed';
            /*DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
            errorWrapper.errorMsg = '\n Response :'+exp.getMessage();
            errorWrapper.requestBody = 'Request :\n'+gen.getAsString();
            errorWrapper.source = 'TransfersBatchHandler.sendRequest';
            DwollaErrorResponseHandler.saveDwollaErrorResponse(
                                new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});*/    
        }
        return res;
    }
    
    /**
    * To generate Json for Dwolla transfer request
    */
    private static JSONGenerator getJson(DwollaAPIConfiguration__c  config, Dwolla_Transfer__c transferRec){
        
        String endpoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c : config.Production_Endpoint_Url__c;
        JSONGenerator gen;
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        
        
        gen.writeFieldName('_links');
        gen.writeStartObject();
        
        gen.writeFieldName('source');
        gen.writeStartObject();
        gen.writeStringField('href', endpoint+'/funding-sources/'+transferRec.Source_Funding_Source_ID__c);
        gen.writeEndObject();
        gen.writeFieldName('destination');
        gen.writeStartObject();
        gen.writeStringField('href', endpoint+'/funding-sources/'+transferRec.Destination_Funding_Source_ID__c);
        gen.writeEndObject();
        
        gen.writeEndObject();
        
        gen.writeFieldName('amount');
        gen.writeStartObject();
        gen.writeStringField('currency', 'USD');
        gen.writeStringField('value', ''+transferRec.Amount__c);
        gen.writeEndObject();
        /*
        if(transferRec.Invoice__c != null){
            
            gen.writeFieldName('metadata');
            gen.writeStartObject();
            gen.writeStringField('paymentId', '');
            gen.writeStringField('note', 'Invoice' + transferRec.Invoice__r.Name);
            gen.writeEndObject();
        
            if(String.isNotBlank(transferRec.Invoice__r.Quickbooks_Invoice_ID2__c)){
                gen.writeStringField('correlationId', ''+transferRec.Invoice__r.Quickbooks_Invoice_ID2__c);
            }
        }*/
        gen.writeEndObject();
        return gen;
    }
}