/*
Test class : Case_Trigger_Test

Purpose: Validates Needs in addition to setting it's values
*/
public class Case_Helper_Class {
   /* 
      Account>>contacts>>cases>>bills
      
      Case is considered as need
      if it's sharable need and IUA met is checked
      Also a Contact can have a max of 3 needs after that case will not be considered as a need even if it meets the above criteria.
      Accounts can have any number of needs however after 5 needs, all needs will have IUA Zero.
      
      Fields:
      IUA2 is referenced in IUA formula field and is set in this class based on criteria
      causeinnumberofneeds is checked if the case meets all the criteria and is fit to be a need
      caselocked is used for locking cases whose IUA is not changed on any Update and this field is checked when chosenOnAcc is changed at member level
      Causeinnumberofneedsdate stores the time when the case is considered as a need the very first time it met the criteria.
      Ex: contact level >> 3 needs(case1,case2,case3) , a new case(case4) is created from this contact with  bills meeting all the criteria however 
          cannot be considered, as there is a limit of 3 at contact level, now we update 2nd need(case2) of this contact in a way that it not passes
          the criteria for need, now Case4 if passing all criteria will be considered as removal of Case2 paved way for it.
          After Update
          contact level >> 3 needs(case1,case3,case4)
          
          Now we again Update 2nd need so that it meets the criteria of a need 
          After Update  
          contact level >> 3 needs(case1,case2,case3)
   */
   
   
   

    public static List <case> caselist = new list <case>();
    
    /* Used for updating number of needs on Account and Contact in addition to checking criteria of 5 needs at account level and 3 needs at contact level*/
    public static map <id, Integer> MapOfCountOfAccountAndContactId = new map <id, integer> ();
    
    /*Collection of all cases(Ids) whose total amount paid by patient changes(RollUp from bill)/IUA changes.Total amount paid by patient field is referened in IUA met*/
    public static set<id> CasIdsForPayabletomemBills = new set<id>();
      
      
      
    public static Void Setting_Need_Attributes_On_Before_Insert(List <Case> triggernew) {

        /* IUA2 is calculated and set for needs record Type , for other Record Types it is equal to Zero*/
        Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
        Boolean ConsiderNeed = false;

        for (case cas: triggernew) {

            if (cas.recordTypeId == NeedsRecType && (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access')) {

                if (cas.IUA_exception__c) {
                    cas.Initial_unsharable_amount2__c = 0;
                } else If(cas.X2nd_MD_Consultation__c == true) {
                    cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c - 250;
                }
                else {
                    cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c;

                }

            } else {

                cas.Initial_unsharable_amount2__c = 0;

            }
        }




    }


    /*
      Changes in IUA exception(causes IUA change), 2ndMd Consultation(causes IUA change) , Sharable need ,total amount paid by patient or recordType needs 
      evaluation as it effects needs criteria     
      
      Also If a case is changed from need to not a need (vice versa) , cause in number of need is set to false , true for reverse case.
      
    */
    public static void Setting_Need_attributes_On_Before_Update(List <case>triggernew, Map <id,case>triggeroldmap) {

        Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
        Id PreventiveRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Preventive').getRecordTypeId();
        Boolean ConsiderNeedNew = false;
        Boolean ConsiderNeedOld = false;
        Date dt;
        Decimal sum = 0;
       

        for (case cas: triggernew) {

            
            if (cas.recordtypeid == NeedsRecType && NeedsRecType != triggerOldMap.get(cas.id).recordtypeid) {

                if (cas.IUA_exception__c) {
                    cas.Initial_unsharable_amount2__c = 0;
                } else If(cas.X2nd_MD_Consultation__c == true) {
                    cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c - 250;
                }
                else {
                    cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c;

                }
                if(cas.sharable_need__c && cas.IUA_met__c){
                CasIdsForPayabletomemBills.add(cas.Id);
                if(cas.Yearly_Reset_Flag__c){
                SetAccountandContactMap(cas.accountId, 'Zero');
                SetAccountandContactMap(cas.contactId, 'Zero');
                }
               }
                
            }

            if (cas.recordtypeid == NeedsRecType && (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access')) {
                If(triggeroldmap.get(cas.id).sharable_need__C && (triggeroldmap.get(cas.Id).IUA_met__c)) {
                    ConsiderNeedold = true;
                }
                else {
                    ConsiderNeedOld = false;
                }
                
                If(triggeroldmap.get(cas.id).Initial_unsharable_amount__c != cas.Initial_unsharable_amount__c) {
                    CasIdsForPayabletomemBills .add(cas.Id);
                }
                
                If(triggeroldmap.get(cas.id).sharable_need__C != cas.sharable_need__C) {
                    CasIdsForPayabletomemBills .add(cas.Id);
                }
                
                 If(triggeroldmap.get(cas.id).Total_Amount_Paid_By_Patient__c!= cas.Total_Amount_Paid_By_Patient__c) {
                    CasIdsForPayabletomemBills .add(cas.Id);
                }

                if (triggeroldmap.get(cas.id).IUA_exception__c == true && cas.IUA_exception__c == false) {
                    cas.Initial_unsharable_amount2__c = cas.X2nd_MD_Consultation__c ? cas.ChosenonAcc__c - 250 : cas.ChosenonAcc__c;
                    CasIdsForPayabletomemBills .add(cas.Id);

                } else if (triggeroldmap.get(cas.id).IUA_exception__c == false && cas.IUA_exception__c == true) {
                    cas.Initial_unsharable_amount2__c = 0;
                    CasIdsForPayabletomemBills .add(cas.Id);
                }


                if (cas.X2nd_MD_Consultation__c == true && triggeroldmap.get(cas.id).X2nd_MD_Consultation__c == false) {

                    dt = date.newinstance(2018, 09, 01);
                    If(cas.createddate>= dt){
                     cas.Initial_unsharable_amount2__c = cas.IUA_exception__c ? 0 : cas.ChosenonAcc__c - 250;
                     CasIdsForPayabletomemBills .add(cas.Id);
                    }
                    Else{
                     cas.Initial_unsharable_amount2__c = cas.IUA_exception__c ? 0 : cas.ChosenonAcc__c / 2;
                     CasIdsForPayabletomemBills .add(cas.Id);
                   }

                } else if (cas.X2nd_MD_Consultation__c == false && triggeroldmap.get(cas.id).X2nd_MD_Consultation__c == true) {
                    cas.Initial_unsharable_amount2__c = cas.IUA_exception__c ? 0 : cas.ChosenonAcc__c;
                    CasIdsForPayabletomemBills .add(cas.Id);

                }
                
                  
                If(cas.sharable_need__c && cas.IUA_met__c) {
                    ConsiderNeedNew = true;
                }
                else {
                    ConsiderNeedNew = false;
                }
                       
                if (cas.cause_in_number_of_needs__c == true && ConsiderNeedOld == true && ConsiderNeedNew == false) {
                    cas.cause_in_number_of_needs__c = false;
                    CasIdsForPayabletomemBills .add(cas.Id);
                    if(cas.Yearly_Reset_Flag__c){                
                    SetAccountandContactMap(cas.accountId, 'Zero');
                    SetAccountandContactMap(cas.contactId, 'Zero');
                    }
                } else if (cas.cause_in_number_of_needs__c == false && ConsiderNeedOld == false && ConsiderNeedNew == true) {
                    if(cas.Yearly_Reset_Flag__c){
                    SetAccountandContactMap(cas.accountId, 'Zero');
                    SetAccountandContactMap(cas.contactId, 'Zero');
                    }
                    CasIdsForPayabletomemBills .add(cas.Id);
                    if (cas.Causeinnumberofneeds_date__c == null) {
                        cas.Causeinnumberofneeds_date__c = system.now();
                    }

                }
                else if (cas.cause_in_number_of_needs__c == true && ConsiderNeedOld == false && ConsiderNeedNew == true) {
                   if(cas.Yearly_Reset_Flag__c){
                    SetAccountandContactMap(cas.accountId, 'Zero');
                    SetAccountandContactMap(cas.contactId, 'Zero');
                    }
                    CasIdsForPayabletomemBills .add(cas.Id);
                    if (cas.Causeinnumberofneeds_date__c == null) {
                        cas.Causeinnumberofneeds_date__c = system.now();
                    }

                }



            } else if (cas.recordtypeid != NeedsRecType  && NeedsRecType == triggerOldMap.get(cas.id).recordtypeid) {
                cas.Initial_unsharable_amount2__c = 0;
                cas.cause_in_number_of_needs__c = false;
                If(triggerOldMap.get(cas.Id).sharable_need__c && triggerOldMap.get(cas.Id).IUA_met__c) {
                    CasIdsForPayabletomemBills .add(cas.Id);
                    if(cas.Yearly_Reset_Flag__c){
                    SetAccountandContactMap(cas.accountId, 'Zero');
                    SetAccountandContactMap(cas.contactId, 'Zero');
                    }
                }
            }
            
            if (cas.recordtypeid == PreventiveRecType ) {

                If(triggeroldmap.get(cas.id).Total_Amount_Paid_By_Patient__c!= cas.Total_Amount_Paid_By_Patient__c) {
                    CasIdsForPayabletomemBills .add(cas.Id);
                }

            }

        }

    }

    /*
       If a case is changed from need to not a need , all cases and bills linked to the parent account are reevaluated
       in addition to update of needs number at contact and account level.
    */
    public static void Setting_Need_attributes_On_After_Events() {


        Map <id, Account> MapOfAccountsAndRelatedCases = new Map <id, Account> ();
        Integer num = 0;
        Integer num2 = 0;
        Integer num3 = 0;
        Date dt;

        if (MapOfCountOfAccountAndContactId.size()> 0) {
            MapOfAccountsAndRelatedCases = new map <id, Account> ([select id, Number_of_Cases__c, Parentid, (select id,caseNumber, IUAafter3__c,Initial_unsharable_amount__c, iua_met__c, case_locked__c, createddate, Sharable_Need__c, cause_in_number_of_needs__c, Type_of_Need__c, ChosenonAcc__c, ContactId, Accountid, Sedera_Parent_Account__c, IUA_exception__c, X2nd_MD_Consultation__c, Causeinnumberofneeds_date__c, total_amount_paid_by_patient__c, initial_unsharable_amount2__c, Third_Party_Paid_Amount__c, third_party_payer__c from cases where Yearly_Reset_Flag__c = TRUE and sharable_need__C = true and recordtype.name = 'Need Sharing'
                order by Causeinnumberofneeds_date__c ASC Nulls last) from Account where id in: MapOfCountOfAccountAndContactId.keySet()]);
        }

        if (MapOfAccountsAndRelatedCases.size()> 0) {

            for (Account acc: MapOfAccountsAndRelatedCases.values()) {
               
                 // system.debug(acc.cases.size());
                 // system.debug(acc.cases[0].Sedera_Parent_Account__c);
                 
                for (case cas: acc.cases) {
                    if (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access') {
                        system.debug('nnn 1 '+cas.sharable_need__c);
                        system.debug('nnn 2 '+cas.IUA_met__c);
                         system.debug('nnn 3 '+cas.caseNumber);
                          //system.AssertEquals(11,2);
                        if (cas.sharable_need__c == true && cas.IUA_met__c) {
                            num3 = 0;
                            num3 = SetAccountandContactMap(cas.contactid, 'AddOne');
                           
                            if (num3 <= 3) {
                                num2 = 0;
                                num2 = SetAccountandContactMap(cas.Accountid, 'AddOne');
                                if (cas.cause_in_number_of_needs__c == false) {
                                    cas.cause_in_number_of_needs__c = true;
                                }

                                if (num2 <= 5) {
                                    if (cas.IUA_exception__C != true && cas.Case_Locked__C == false) {

                                        if (cas.X2nd_MD_Consultation__c == true) {
                                            dt = date.newinstance(2018, 09, 01);
                                            If(cas.createddate>= dt){
                                             cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c - 250;
                                             CasIdsForPayabletomemBills .add(cas.Id);
                                            }
                                            Else{
                                             cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c / 2;
                                             CasIdsForPayabletomemBills .add(cas.Id);
                                            }
                                        } else {
                                            cas.initial_unsharable_Amount2__c = cas.ChosenonAcc__c;
                                            CasIdsForPayabletomemBills .add(cas.Id);
                                        }

                                        if (!(cas.IUA_met__c)) {
                                            SetAccountandContactMap(cas.contactid, 'MinusOne');
                                            SetAccountandContactMap(cas.Accountid, 'MinusOne');
                                            cas.cause_in_number_of_needs__c = false;

                                        }
                                    } else if (cas.Case_Locked__C == false) {
                                        cas.initial_unsharable_Amount2__c = 0;
                                        CasIdsForPayabletomemBills .add(cas.Id);
                                    }
                                } else if (cas.Case_Locked__C == false) {
                                    cas.initial_unsharable_Amount2__c = 0;
                                    CasIdsForPayabletomemBills .add(cas.Id);
                                }



                            } else {
                                SetAccountandContactMap(cas.contactid, 'MinusOne');
                                cas.cause_in_number_of_needs__c = false;
                                if (cas.Case_Locked__C == false) {
                                    cas.initial_unsharable_Amount2__c = 0;
                                    CasIdsForPayabletomemBills .add(cas.Id);
                                }

                            }
                            caseList.add(cas);
                        }


                    }


                }

            }

        }




    }


    public static void Setting_Need_attributes_On_Before_Delete(List <Case> TriggerOld) {
        Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();

        for (case cas: TriggerOld) {
            if (cas.recordtypeid == NeedsRecType && cas.cause_in_number_of_needs__c == true && (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access')) {              
                SetAccountandContactMap(cas.Accountid, 'Zero');
                SetAccountandContactMap(cas.Contactid, 'Zero');
            }


        }
    }


    public static void Setting_Need_Attributes_for_Undelete(List <case>triggernew) {
        Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
    
        for (case cas: triggernew) {
            
            if (cas.sharable_need__C == true  && cas.recordtypeid == NeedsRecType) {
                SetAccountandContactMap(cas.Accountid, 'Zero');
                SetAccountandContactMap(cas.Contactid, 'Zero');
            }
        }
    }


    /*Update of needs number at contact and account level*/
    public static void Setting_Number_Of_Needs_On_Accounts_And_Contacts() {

        String AccOrconId = '';
        Contact con;
        Account acc;
        List <Account> AccList = new List <Account> ();
        List <Contact> ConList = new List <contact> ();
        
        for (id id: MapOfCountOfAccountAndContactId.keyset()) {

            AccOrconId = id;
            system.debug('>>>>>>>>>>>>>>>>>>>>>' + MapOfCountOfAccountAndContactId);
            if (AccOrconId.startswith('003')) {
                con = new contact(id = AccOrconId, number_of_needs__C = MapOfCountOfAccountAndContactId.get(id));
                conList.add(con);
            } else if (AccOrconId.startswith('001')) {
                acc = new Account(id = AccOrconId, number_of_cases__C = MapOfCountOfAccountAndContactId.get(id));
                AccList.add(acc);
            }
        }




        if (accList.size()> 0) {
            MapOfCountOfAccountAndContactId.clear();
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerqb = 1;
            Update accList;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerqb = 0;
            accList.clear();

        }
        if (conList.size()> 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
            update conList;
            UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 0;
        }
        if (caseList.size()> 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 1;
            system.debug('mmmmmmmmmmmmmm mmmmmmmmmmmm m ' + CaseList);
            update caseList;
            UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
            caseList.clear();
        }
    }


    /*Re evaluation of all bills if there is a change in total paid by patient amount or IUA*/
    public static void SetBillsOnChngeOfttlAmtPaidPatient(Map <Id, Case> triggeroldmap, Set <Id> CaseIds) {
        
        if(CasIdsForPayabletomemBills.size()>0){
        
         List<Case> caseList=[select Initial_Unsharable_Amount__c, sharable_need__C, (select id,case__r.recordTypeId,case__r.Initial_Unsharable_Amount__c,case__r.sharable_need__C, Total_Patient_Payable_Amount__c, Approval_Status__c, case__c, Payable_to_member__c from bills__r  order by createddate asc) from
                case where id in: CasIdsForPayabletomemBills ]; 
                
         if(caseList.size()>0){
           Bill_Helper_class.calculatePayableToMember(caseList);
         }            
        
        }
         
    }

    public static Integer SetAccountandContactMap(id id, String Operation) {

        Integer count = 0;
        system.debug('mmmmmmmmmmmmm ' + id + '  ' + operation);
        if (MapOfCountOfAccountAndContactId.containskey(id)) {
            count = MapOfCountOfAccountAndContactId.get(id);
            if (Operation == 'AddOne') {
                MapOfCountOfAccountAndContactId.put(id, count + 1);
            } else if (Operation == 'MinusOne') {
                MapOfCountOfAccountAndContactId.put(id, count - 1);
            } else {
                MapOfCountOfAccountAndContactId.put(id, 0);
            }


            return MapOfCountOfAccountAndContactId.get(id);
        } else {
            if (Operation == 'AddOne')
                MapOfCountOfAccountAndContactId.put(id, 0 + 1);
            else if (Operation == 'MinusOne')
                MapOfCountOfAccountAndContactId.put(id, 0 - 1);
            else
                MapOfCountOfAccountAndContactId.put(id, 0);

            return MapOfCountOfAccountAndContactId.get(id);
        }

    }

    /*Clearing Maps*/
    public static void clearMaps() {
        MapOfCountOfAccountAndContactId.clear();
        caselist.clear();
    }


}