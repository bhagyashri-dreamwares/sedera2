global class EonsSchedule implements Schedulable 
{
    global void execute(SchedulableContext ctx) 
    {
        pdfGeneration pdf = new pdfGeneration();
        pdf.executeJobs();
    }
}