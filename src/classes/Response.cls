/**
 @Author    : Dreamwares
 @Created Date  : 18/07/2018
 @Purpose    : - For communication in methods
          - Standard format for Communication in server and client
*/
public class Response {
  public Boolean Success;
    public String Message;
    public Object Data;
    
    public Response(Boolean success, String message, Object data){
        this.Success =  success;
        this.Message =  message;
        this.Data =  data;
    }
}