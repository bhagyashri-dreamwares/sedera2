@isTest
public class DwollaBusinessClassTranslationHelperTest {
    @testsetup static void setup() {
        //insert a value into the translation table
        Dwolla_Business_Class_Translation__c translationRecord = new Dwolla_Business_Class_Translation__c();
        translationRecord.Dwolla_Business_Class_Name__c = 'Test';
        translationRecord.Dwolla_Business_Class_Value__c = 'Value';
        
        insert TranslationRecord;
    }
    
    @isTest static void TranslateBusinessClassification() {
        //make a call to the helper to get the value
        string expectedValue = 'Value';
        string testName = 'Test';
        string testValue = DwollaBusinessClassTranslationHelper.Translate(testName);

        
        system.assertEquals(expectedValue, testValue);
    
    }



}