/*
*@Purpose : Test method for EONSEventController class
*@Date : 31/07/2018
*/
@isTest
public class EONSEventControllerTest{
    
    /*
    * To initialise data
    */    
    @testSetup
    private static void initData(){
        
        //insert custom setting record
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'Test@test.com';
        insert config;
        
        //insert Account
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account emp = new Account();
        emp.recordTypeId = accRecordTypeId;
        emp.Name = 'Test'; 
        emp.Default_Product__c = 'Sedera Access';       
        insert emp;
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        Account account = new Account();
        account.recordTypeId = devRecordTypeId;
        account.Name = 'Test AccountName';
        account.First_Name__c = 'FName';
        account.Last_Name__c = 'LName';
        account.Account_Employer_Name__c = emp.Id;
        account.Primary_Email_Address__c = 'test@test.com';
        insert account;
        
        // insert contact
        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.LastName = 'Test Name';
        contact.Email = 'test@test.com';
        insert contact;
        
        // insert Case
        Case caseRecord = new Case();
        caseRecord.ContactId = contact.Id;
        caseRecord.Status = 'New';
        caseRecord.Origin = 'Phone';
        caseRecord.Type = 'HCS';
        caseRecord.Correspondence_Email__c = 'test@test.com';
        insert caseRecord;
        
        // insert Attachment
        Attachment attach = new Attachment(); 
        attach.Name='Test.pdf'; 
        attach.body= Blob.valueOf('Unit Test Attachment Body'); 
        attach.parentId = caseRecord.Id; 
        attach.ContentType = 'application/pdf'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        insert attach;
        
        //insert SDoc
        SDOC__SDoc__c sDoc = new SDOC__SDoc__c();
        sDoc.SDOC__Attachment_ID__c = attach.Id;
        sDoc.SDOC__Attachment_Name__c = attach.Name;
        sDoc.SDOC__ObjectID__c = caseRecord.ParentId;
        insert sDoc;
        
        //insert EONS Shared record
        EONS_Shared__c eONS = new EONS_Shared__c();
        eONS.Name = 'Test';
        eONS.Case__c = caseRecord.Id;
        eONS.SDoc__c = sDoc.Id;
        eONS.Tracking_ID__c = 'Test123';
        eONS.Date_Sent__c = System.now();
        eONS.From_Email_Address__c = 'Test@test.com';
        eONS.To_Email_Address__c = 'test@test.com'; 
        insert eONS;
        
    }
    
    /*
    * To test fetch Email statusFunctionality
    */
    private static testMethod void testFetchEmailStatusFunctionality(){
        
        EONS_Shared__c eOns = [SELECT Id
                             FROM EONS_Shared__c 
                             LIMIT 1];
        
        
        
        Test.setMock(HttpCalloutMock.class, new EONSEventMock ());
        Test.startTest();
        
        EONSEventController.storeEmailStatus(new List<String>{''+eOns.Id});
         
        Test.stopTest();
        
        EONS_Shared__c updatedEOns = [SELECT Id, Email_Status__c 
                                     FROM EONS_Shared__c 
                                     LIMIT 1];
        System.assertEquals('delivered', updatedEOns.Email_Status__c);
    }
    
    public class  EONSEventMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{ "sourceTrackingId": "Test123", "data": { "message": { "id": "<Test123@authorized_domain.com>", "message_deliveries": [ { "recipient": "test@test.com", "status": { "deliveryStatus":"delivered", "deliveryTime":"Mon, 23 Apr 2018 13:27:34 -0700", "openedStatus": "opened", "openedTime": "Mon, 23 Apr 2018 13:27:51 -0700" } } ] } } }');
            response.setStatusCode(200);
            
            return response;
        }
    }
}