/*
    @Author         : Dreamwares
    @Purpose        : Helper methods for debug class
    @Created Date   : 19/07/2018
*/

public with sharing class QuickbookDebugServices {
    
    /*
        @Description        : Check for Database.upsertResult result & for error condtion create new QB_Sync_Errors__c record
    */
    public static void trackUpsertResult(String objectName,List<Database.upsertResult> saveResultList){
        List<QB_Sync_Errors__c> quickbookList = new List<QB_Sync_Errors__c>(); 
        QB_Sync_Errors__c quickbookRecord;
        if(String.isNotBlank(objectName) && saveResultList != null && !saveResultList.isEmpty()){               
            for(Integer count = 0; count < saveResultList.size(); count++) {
                Database.upsertResult sr = saveResultList[count];
                if (!sr.isSuccess()) {
                    quickbookRecord = new QB_Sync_Errors__c();
                    String errorMsg  = objectName +  ' update/insert failed!';
                    for (Database.Error er : sr.getErrors()) {
                        errorMsg += 'Error (' +  er.getStatusCode() + '):' + er.getMessage();
                        errorMsg += '\r\n';
                    }
                    quickbookRecord.ErrorMessage__c = errorMsg;
                    quickbookRecord.Type__c ='Upsert failed';
                    quickbookList.add(quickbookRecord);
                }                   
            }
        }
        if(!quickbookList.isEmpty()){
            trackCreateResult(quickbookList);
        } 
    }
    
    /*
        @Description        : Check for Database.SaveResult result & for error condtion create new QB_Sync_Errors__c record
    */
    public static void trackUpdateResult(String objectName,List<Database.SaveResult> saveResultList){
        List<QB_Sync_Errors__c> quickbookList = new List<QB_Sync_Errors__c>(); 
        QB_Sync_Errors__c quickbookRecord;
        if(String.isNotBlank(objectName) && saveResultList != null && !saveResultList.isEmpty()){               
            for(Integer count = 0; count < saveResultList.size(); count++) {
                Database.SaveResult sr = saveResultList[count];
                if (!sr.isSuccess()) {
                    quickbookRecord = new QB_Sync_Errors__c();
                    String errorMsg  = objectName +  'Update failed!';
                    for (Database.Error er : sr.getErrors()) {
                        errorMsg += 'Error (' +  er.getStatusCode() + '):' + er.getMessage();
                        errorMsg += '\r\n';
                    }
                     System.debug('errorMsg:::::::'+errorMsg);
                    quickbookRecord.ErrorMessage__c = errorMsg;
                    quickbookRecord.Type__c ='Update failed';
                    quickbookList.add(quickbookRecord);
                }                   
            }
        }
        if(!quickbookList.isEmpty()){
            trackCreateResult(quickbookList);
        }
    }

    /*
        @Description        : Check for exception create new QB_Sync_Errors__c record
     */
    public static void trackException(exception exceptionInstance){
        List<QB_Sync_Errors__c> quickbookList = new List<QB_Sync_Errors__c>();
        if(exceptionInstance != null){
            QB_Sync_Errors__c quickbookRecord = new QB_Sync_Errors__c();
            quickbookRecord.ErrorMessage__c = exceptionInstance.getMessage()+String.valueof(exceptionInstance.getLineNumber())+exceptionInstance.getStackTraceString()+exceptionInstance.getTypeName();
            quickbookRecord.Type__c ='Exception';
            quickbookList.add(quickbookRecord);
        }
        if(!quickbookList.isEmpty()){
            trackCreateResult(quickbookList);
        }       
    }
    
    /*
        @Description        : Check for SystemDebug & create new QB_Sync_Errors__c record
     */
    public static void trackSystemDebug(string messagestr){
        system.debug('messagestr '+messagestr);
        List<QB_Sync_Errors__c> quickbookList = new List<QB_Sync_Errors__c>();
        if(String.isNotBlank(messagestr)){
                QB_Sync_Errors__c quickbookRecord = new QB_Sync_Errors__c();
                quickbookRecord.ErrorMessage__c = messagestr;
                quickbookRecord.Type__c ='SystemDebug';
                quickbookList.add(quickbookRecord);                   
        }
        system.debug('quickbookList '+quickbookList);
        if(!quickbookList.isEmpty()){
            trackCreateResult(quickbookList);
        }      
    }
    
    /*
        @Description        : create new QB_Sync_Errors__c records
     */
    public static void trackCreateResult(List<QB_Sync_Errors__c> quickbookList){
        system.debug('trackCreateResult ');
        if(quickbookList != null){
            try{
                Insert quickbookList;
                system.debug('trackCreateResult quickbookList '+quickbookList);
            }catch(Exception e){
                System.debug('The following exception has occurred' + e.getMessage());
            }
        }
    }   
}