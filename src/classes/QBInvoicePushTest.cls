/**
* Purpose : test class for QBCustomerPull Class
*
*/
@isTest
public class QBInvoicePushTest {    
    /*
* Purpose : method to create required data
*/
    @testSetup
    static void dataCreation(){
        DateTime setDate = datetime.newInstance(1970, 01, 01, 00, 00, 0);
        
        // insert test account record
        Account testAccount = createAccount('Amys Bird Sanctuary','example@exe.com');
        Product2 testProduct = createProduct();
        Invoice__c testInvoice = new Invoice__c( //Last_Sync_Date__c = System.now().addHours(-1), 
            //SF_Last_Modified_Date__c= System.now().addHours(1) 
            Account__c = testAccount.Id,/*Quickbooks_Invoice_ID__c ='71',QB_Sync_Token__c ='4',*/ Submission_Status__c = 'New',
            Service_Period__c = '1/1/2018 - 1/1/2018',
            Quickbooks_Invoice_ID__c = '70', Invoice_Date__c = Date.today());
        insert testInvoice;
        
        Invoice__c testInvoice1 = new Invoice__c( //Last_Sync_Date__c = System.now().addHours(-1), 
            //SF_Last_Modified_Date__c= System.now().addHours(1) 
            Account__c = testAccount.Id,/*Quickbooks_Invoice_ID__c ='71',QB_Sync_Token__c ='4',*/ Submission_Status__c = 'New',
            Service_Period__c = '1/1/2018 - 1/1/2018', Invoice_Date__c = Date.today(),
            Quickbooks_Invoice_ID__c = '60', Invoice_Due_Date__c = Date.today().addMonths(1));
        insert testInvoice1;
        
        Invoice_Line_Item__c testLineItem = new Invoice_Line_Item__c( Invoice__c = testInvoice.Id, //Default_PRICING__c = testItem.Id,
                                                                     Price__c=10,Quantity__c=1, Description__c='Test',/*,QB_Line_Item_Id__c ='71-1'*/
                                                                     Product__c = testProduct.Id, QBO_Product__c = testProduct.id);
        insert testLineItem;
        
        // insert congfig in custom setting  
        createConfig();
    }
    
    static testmethod void validateTrigger(){
        delete [select Id from Invoice_Line_Item__c];
        delete [select Id from Invoice__c];
        delete [select Id from Product2];
        delete [select Id from Account];
        
        // insert test account record
        Account testAccount = createAccount('Amys Bird Sanctuary','example@exe.com');
        Product2 testProduct = createProduct();
        Invoice__c testInvoice = new Invoice__c( //Last_Sync_Date__c = System.now().addHours(-1), 
            //SF_Last_Modified_Date__c= System.now().addHours(1) 
            Account__c = testAccount.Id, // Quickbooks_Invoice_ID__c ='71',QB_Sync_Token__c ='4',
            Submission_Status__c = 'New', Quickbooks_Invoice_ID__c = '72',
            QB_Sync_Token__c = 'T1');
        insert testInvoice;
        System.debug('testInvoice::'+testInvoice);
        
        testInvoice = new Invoice__c( //Last_Sync_Date__c = System.now().addHours(-1), 
            //SF_Last_Modified_Date__c= System.now().addHours(1) 
            Account__c = testAccount.Id, // Quickbooks_Invoice_ID__c ='71',QB_Sync_Token__c ='4',
            Submission_Status__c = 'Approved',Service_Period__c = '1/1/2018 - 1/1/2018',
            Quickbooks_Invoice_ID__c = '71');
        insert testInvoice;
        
        Invoice_Line_Item__c testLineItem = new Invoice_Line_Item__c( Invoice__c = testInvoice.Id, //Default_PRICING__c = testItem.Id,
                                                                     Price__c=10,Quantity__c=1, Description__c='Test', //,QB_Line_Item_Id__c ='71-1'
                                                                     QBO_Product__c = testProduct.Id, Product__c = testProduct.Id,
                                                                     QB_Line_Item_Id__c = 'T01-05');
        insert testLineItem;
        
        Invoice_Line_Item__c testLineItem1 = new Invoice_Line_Item__c( Invoice__c = testInvoice.Id, //Default_PRICING__c = testItem.Id,
                                                                      Price__c=50, Quantity__c=5, Description__c='Test line items', //,QB_Line_Item_Id__c ='71-1'
                                                                      QBO_Product__c = testProduct.Id, Product__c = testProduct.Id,
                                                                      QB_Line_Item_Id__c = 'T02-10');
        insert testLineItem1;        
    }
    /*
* Purpose : test creation of customers in QB
*/
    static testmethod void testQBInvoiceCreation(){ 
        List<Invoice__c> invoiceRecords = [SELECT Id, Account__c, QB_Sync_Token__c, Quickbooks_Invoice_ID__c, Name,
                                           Invoice_Due_Date__c, Account__r.Name, Quickbooks_Customer_ID__c,
                                           QB_Terms_ID__c, Invoice_Date__c, Service_Period__c,
                                           Account__r.Invoice_Email_Address__c FROM Invoice__c];
        
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());    
        
        Test.startTest();
        invoiceRecords[0].Submission_Status__c = 'Approved';
        update invoiceRecords;
        
        QBInvoiceBatchRequestWrapper wrapper = new QBInvoiceBatchRequestWrapper();
        
        QBWrappers.QBInvoiceDTO invoiceDto = new QBWrappers.QBInvoiceDTO();
        QBWrappers.QBLinkedTxn qbLinkedTxn = new QBWrappers.QBLinkedTxn();
        qbLinkedTxn.TxnId = 'T011';
        qbLinkedTxn.TxnType = 'Pay';
        invoiceDto.LinkedTxn = new List<QBWrappers.QBLinkedTxn>{qbLinkedTxn};
            
            QBInvoiceBatchRequestWrapper.QBBatchInvoiceObject qbBatch = new QBInvoiceBatchRequestWrapper.QBBatchInvoiceObject('B01', 'Test');
        
        QBInvoicePush invoicePushObj = new QBInvoicePush();
        BAPIResponseWrapper b = invoicePushObj.doPost(invoiceRecords,'Quickbooks');
        
        List<QBBatchResponseObject> qbBatchResponseObjectList = new List<QBBatchResponseObject>();
        QBBatchResponseObject qbBatchResponseObjectRec = new QBBatchResponseObject();
        qbBatchResponseObjectRec.Invoice = new QBWrappers.QBInvoiceDTO();
        qbBatchResponseObjectRec.Invoice.Line = new List<QBWrappers.QBLine>();
        QBWrappers.QBLine lineItem = new QBWrappers.QBLine();
        lineItem.SalesItemLineDetail = new QBWrappers.QBSalesItemLineDetail();
        lineItem.SalesItemLineDetail.ItemRef = new QBWrappers.QBItemRef('xyz','xyz');
        qbBatchResponseObjectRec.Invoice.Line.add(lineItem);
        qbBatchResponseObjectList.add(qbBatchResponseObjectRec);
        invoicePushObj.updateRecords(qbBatchResponseObjectList,system.now(),'Quickbooks');
        
        // error 
        QBBatchFault.QBBatchError faultError = new QBBatchFault.QBBatchError();
        faultError.Message = 'test message';
        faultError.Detail = 'Detail';
        qbBatchResponseObjectRec.Fault.Error.add(faultError);
        qbBatchResponseObjectRec.Invoice = null;
        qbBatchResponseObjectRec.bId = 'bid11';
        qbBatchResponseObjectList.add(qbBatchResponseObjectRec);
        
        
        //invoicePushObj.batchIdRecordIdMap.put('bid11',invoiceRecords[0].Id);
        
        invoicePushObj.updateRecords(qbBatchResponseObjectList,system.now(),'Qiockbooks');
        
        
        invoicePushObj.fetchRecords();
        Test.stopTest();
    }
    
    /*@isTest
static void testScheduler(){

Test.startTest();
QBScheduler.schedule();
Test.stopTest();  

}*/
    
    static void createConfig(){
        
        Quick_Book_App_Configuration__c setting = new Quick_Book_App_Configuration__c(Name = 'Quickbooks',
                                                                                      Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
                                                                                      Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
                                                                                      Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
                                                                                      Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
                                                                                      Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
                                                                                      Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/token',
                                                                                      Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
                                                                                      Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
                                                                                      Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
                                                                                      Is_Sandbox__c = true,
                                                                                      Production_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com', 
                                                                                      QB_Company_Id__c = '123145860463979',
                                                                                      Redirect_URI__c = 'https://air--fsmsandbox--c.cs14.visual.force.com',
                                                                                      Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
                                                                                      Refresh_Token_Expiry__c = DateTime.now().addDays(1),                
                                                                                      Sandbox_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com');
        insert setting;    
        
        setting = new Quick_Book_App_Configuration__c(Name = 'Quickbooks2',
                                                      Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
                                                      Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
                                                      Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
                                                      Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
                                                      Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
                                                      Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/token',
                                                      Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
                                                      Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
                                                      Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
                                                      Is_Sandbox__c = true,
                                                      Production_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com', 
                                                      QB_Company_Id__c = '123145860463979',
                                                      Redirect_URI__c = 'https://air--fsmsandbox--c.cs14.visual.force.com',
                                                      Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
                                                      Refresh_Token_Expiry__c = DateTime.now().addDays(1),                
                                                      Sandbox_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com');
        insert setting;    
        
        QuickBook2_LineItems_Configuration__c quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='MCS Member Services',
                                                                                                                          Quickbook2_Product_Id__c='22',
                                                                                                                          Quickbook2_Product_Name__c='MCS Member Services',
                                                                                                                          Salesforce_Product_Id__c='01tq0000002i0wT');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (New)',
                                                                                     Quickbook2_Product_Id__c='20',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (New)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wJ');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Startup MCS Revenue',
                                                                                     Quickbook2_Product_Id__c='21',
                                                                                     Quickbook2_Product_Name__c='Startup MCS Revenue',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wO');
        INSERT quickBook2LineItemsConfiguration;
    }
    
    testmethod static void testQBWrapper() {
        
        Test.startTest();
        
        QBWrappers.QBLinkedTxn linkedTxn = new QBWrappers.QBLinkedTxn();
        
        QBWrappers.BillEmail emailBill = new QBWrappers.BillEmail('BillEmail');
        
        QBWrappers.QBLine qbLine = new QBWrappers.QBLine( 'Id', 15487, 'Description', 100, 'DetailType',
                                                         new QBWrappers.QBSalesItemLineDetail(), new QBWrappers.QBSubTotalLineDetail(), new QBWrappers.QBDiscountLineDetail( true, 100));
        
        QBWrappers.QBItemRef itemRef = new QBWrappers.QBItemRef('name', 'value');
        
        QBWrappers.QBTaxCodeRef qBTaxCodeRef = new QBWrappers.QBTaxCodeRef('value');
        QBWrappers.QBTxnTaxDetail qBTxnTaxDetail = new QBWrappers.QBTxnTaxDetail(100);
        QBWrappers.TxnTaxCodeRef txnTaxCodeRef = new QBWrappers.TxnTaxCodeRef('value');
        QBWrappers.QBCustomerRef qBCustomerRef = new QBWrappers.QBCustomerRef('value', 'name');
        QBWrappers.QBCustomerMemo qBCustomerMemo = new QBWrappers.QBCustomerMemo('value');
        QBWrappers.QBSalesTermRef qBSalesTermRef = new QBWrappers.QBSalesTermRef('value');
        QBWrappers.RequestTokenWrapper requestTokenWrapper1 = new QBWrappers.RequestTokenWrapper();
        QBWrappers.RequestTokenWrapper requestTokenWrapper2 = new QBWrappers.RequestTokenWrapper('token', 'tokenSecret', true, 'problem');
        QBWrappers.QueryResponse queryResp = new QBWrappers.QueryResponse();
        QBWrappers.Email email1 = new QBWrappers.Email();
        QBWrappers.Email email2 = new QBWrappers.Email('URI');
        //QBWrappers.WebAddress webAddress1 = new QBWrappers.WebAddress();
        //QBWrappers.WebAddress webAddress2 = new QBWrappers.WebAddress('WebAddress');
        QBWrappers.Phone phone = new QBWrappers.Phone('phone');
        QBWrappers.Address address = new QBWrappers.Address('Line1', 'City', 'Country', 'CountrySubDivisionCode', 'PostalCode');
        QBWrappers.QBDepositToAccountRef qBDepositTOAccountRef = new QBWrappers.QBDepositToAccountRef('value', 'name');
        QBWrappers.QBObjectReference qBObject = new QBWrappers.QBObjectReference('name', 'value');
        
        Test.stopTest();        
    }
    
    testmethod static void testQBSyncHelper() {
        
        Test.startTest();
        
        DateTime lastResponseTime = system.now(); /*QBSyncHelper.getLastResponseTime('Test Object');
System.assert(lastResponseTime != null);
lastResponseTime = QBSyncHelper.getLastResponseTime('Test Object2');*/
        
        /*QBSyncHelper.updateResponseTime('Test Object', DateTime.now());
QBSyncHelper.updateResponseTime('Test Object1', DateTime.now());*/
        QBSyncHelper.dateFromString('01/01/2017');
        QBSyncHelper.dateFromString('01-01-2017');
        QBSyncHelper.getQBItemIdToItemIdMap(new List<String>{'1'});
        QBSyncHelper.getCustomerMap(new List<String>{'1'});
        
        Invoice__c testInvoice = [ SELECT Id FROM Invoice__c  LIMIT 1];
        /*Map<Id,List<Invoice_Line_Items__c>> invIdToLineItemMap =*/ QBSyncHelper.getInvoiceIdToLineItemsMap(new List<ID> { testInvoice.Id });  
        /*System.assert(!invIdToLineItemMap.isEmpty());*/
        Test.stopTest();        
    }
    
    testmethod static void testInvoiceWrapper(){
        InvoiceToLineItemWrapper obj = new InvoiceToLineItemWrapper(new Invoice__c(),new List<Invoice_Line_Item__c>());
    }
    
    testmethod static void testQBBatchResponse(){
        QBBatchResponse objRes = new QBBatchResponse();
        objRes.time_qb = 'asdsa';
    }
    
    testmethod static void testQBInvoiceParser(){
        List<QBWrappers.QBInvoiceDTO> dtoList = new List<QBWrappers.QBInvoiceDTO>();
        QBWrappers.QBInvoiceDTO dtoRecord = new QBWrappers.QBInvoiceDTO();
        dtoRecord.Line = new List<QBWrappers.QBLine>();
        QBWrappers.QBLine lineItem = new QBWrappers.QBLine();
        lineItem.SalesItemLineDetail = new QBWrappers.QBSalesItemLineDetail();
        lineItem.SalesItemLineDetail.ItemRef = new QBWrappers.QBItemRef('xyz','xyz');
        dtoRecord.Line.add(lineItem);
        dtoList.add(dtoRecord);
        QBInvoiceParser obj = new QBInvoiceParser();
        obj.parseToObjectList(dtoList,'Quickbooks');
    }
    
    /*testmethod static void testCreditMemo(){
QBCreditMemoPull qbObj = new QBCreditMemoPull();
qbObj.getObjectRecords(new QBQueryResponseWrapper());
qbObj.save(new List<Object>(),System.now());
}*/
    /*testmethod static void testPayment(){
QBPaymentPull qbObj = new QBPaymentPull();
qbObj.getObjectRecords(new QBQueryResponseWrapper());
qbObj.save(new List<Object>(),System.now());
}*/
    
    testmethod static void testOther() {
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());   
        
        List<QBWrappers.QBCreditMemo > creditMemoList = new List<QBWrappers.QBCreditMemo>();
        QBWrappers.QBCreditMemo credit = new QBWrappers.QBCreditMemo();
        credit.RemainingCredit = 1.0;
        credit.CustomerRef = new QBWrappers.QBCustomerRef();
        credit.CustomerRef.value = '1';
        creditMemoList.add(credit);
        
        List<QBWrappers.QBPayment > qbPaymentList = new List<QBWrappers.QBPayment>();
        QBWrappers.QBPayment payment = new QBWrappers.QBPayment();
        payment.UnappliedAmt = 1.0;
        payment.CustomerRef = new QBWrappers.QBCustomerRef();
        payment.CustomerRef.value = '1';
        qbPaymentList.add(payment);
        
        
        /* QBPaymentPull aqbObj = new QBPaymentPull();
aqbObj.parseQBQueryResponse('GEFUJ');
aqbObj.createQueryString('Account',1,new List<String>());*/
        
        QBInvoicePushBatch obj = new QBInvoicePushBatch();
        //obj.getQBCreditMemoMap(creditMemoList);
        //obj.getPaymentMap(qbPaymentList);
    }
    
    static testmethod void testInvoicePull(){
        Test.setMock(HttpCalloutMock.class, new MockResponseGenerator());   
        
        QBInvoicePull obj = new QBInvoicePull();
        obj.getObjectRecords(new QBQueryResponseWrapper());
        DateTime dt;
        obj.save(new List<Object>(),dt); 
        
        QBInvoicePushBatch obj2 = new QBInvoicePushBatch(new Set<ID>(),'Quickbooks');
        
        obj2.getMapOfSyncToken(new List<InvoiceToLineItemWrapper>(),'Quickbooks');
    }
    
    static Account createAccount(String accName,String billingEmail){
        
        Account recordAccount = new Account(Name = accName,
                                            Quickbooks_Customer_Id__c='1',
                                            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
                                            ShippingStreet = '230 NE US Hwy 27',
                                            ShippingCity = 'Williston',
                                            ShippingState = 'Florida',
                                            ShippingCountry = 'United States',
                                            Invoice_Email_Address__c = billingEmail,
                                            QB_Payment_Net_Terms__c = 'Due on receipt');
        insert recordAccount;
        return recordAccount;
    }
    static Product2 createProduct(){
        
        Product2 recordProduct = new Product2(Name = 'Product',
                                              Quickbooks_ID__c='1');
        insert recordProduct;
        return recordProduct;
    }
    
    //Class to set mock response
    public class MockResponseGenerator implements HttpCalloutMock {
        //Method to return mock response
        public HTTPResponse respond(HTTPRequest req) {
            //System.debug('In mock class');
            HttpResponse res = new HttpResponse();
            
            if (req.getEndpoint().equalsIgnoreCase('https://air--fsmsandbox--c.cs14.visual.force.com123145860463979/batch?minorversion=8') 
                && req.getMethod().equalsIgnoreCase('POST')){
                    res.setHeader('Content-Type', 'application/json');
                    String body = '{"Customer": {"Title": "Employee", "Taxable": true,"Job": true,"Balance": 5000,"domain": "Test",'+
                        '"Id": "C001","GivenName": "Ali","MiddleName": "Bek","FamilyName": "Jorg","CompanyName": "Softech",'+
                        '"Active": true,"PrimaryPhone": 9852654578},"bId": "B01","item": {"Id": "QI01","Name": "TestQBI",'+
                        '"SyncToken": "dsjfdjkgnkdfjgn","UnitPrice": 5000,"Type": "SalesItemLineDetail","FullyQualifiedName": "Softech",'+
                        '"Active": true},"Invoice": {"Deposit": 15000,"AllowIPNPayment": false,"AllowOnlinePayment": false,'+
                        '"AllowOnlineCreditCardPayment": true,"AllowOnlineACHPayment": true,"domain": "test","Id": "a1dq00000029aDlAAI","DocNumber": "D343","TxnDate": "28-08-2018"'+
                        '},"SalesReceipt": {"domain": "check","sparse": true,"ID": "12146","SyncToken": "rtrghfh","DocNumber": "10100"},"Account": {"Name": "Test Account",'+
                        '"SubAccount": false,"ID": "001q000000uRltyAAC","SyncToken": "rtrghfhty","FullyQualifiedName": "Test John","Active": true}}';
                    //System.debug('res.setBody::'+body);
                    res.setBody(body);
                    res.setStatusCode(200);
                }else if(req.getMethod().equalsIgnoreCase('POST')){
                    res.setHeader('Content-Type', 'application/json');  
                    res.setBody('{"bid": "B001"}');
                    res.setStatusCode(200);
                }
            
            return res;
        }
    }
    
    static testmethod void testMethod9(){
        Account account = new Account(Name = 'Test',
                                            Quickbooks_Customer_Id__c='10',
                                            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
                                            ShippingStreet = '230 NE US Hwy 27',
                                            ShippingCity = 'Williston',
                                            ShippingState = 'Florida',
                                            ShippingCountry = 'United States',
                                            Invoice_Email_Address__c = 'test@test.com',
                                            QB_Payment_Net_Terms__c = 'Due on receipt');
        insert account;
        
        Invoice__c invoice = new Invoice__c(Service_Period__c = 'test', Account__c = account.Id, Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30), Paid_Amount__c = 8000);
        insert invoice;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        invoiceList.add(invoice);
        
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                        price__c = 2000, Quickbooks_ID__c='28');
        insert product;
        // AccountUpdateBatchJob obj = new AccountUpdateBatchJob();

        Set<id> invoiceIdSet = new Set<Id>();
        for(Invoice__c invoiceRec : invoiceList){
            invoiceIdSet.add(invoiceRec.Id);
        }
        
        QBInvoicePushBatch obj = new QBInvoicePushBatch(invoiceIdSet,'Quickbooks');
        DataBase.executeBatch(obj);
        
        QBInvoicePushBatch obj2 = new QBInvoicePushBatch(invoiceIdSet,'Quickbooks2');
        DataBase.executeBatch(obj);

    }
    
    static testmethod void testMethod8(){
          QuickBook2_LineItems_Configuration__c quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='MCS Member Services',
                                                                                                                          Quickbook2_Product_Id__c='22',
                                                                                                                          Quickbook2_Product_Name__c='MCS Member Services',
                                                                                                                          Salesforce_Product_Id__c='01tq0000002i0wT');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (New)',
                                                                                     Quickbook2_Product_Id__c='20',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (New)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wJ');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Startup MCS Revenue',
                                                                                     Quickbook2_Product_Id__c='21',
                                                                                     Quickbook2_Product_Name__c='Startup MCS Revenue',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wO');
        INSERT quickBook2LineItemsConfiguration;
        
        Account account = createParentAccount('Test Account ', 'abc@gmail.com');
        
        Invoice__c invoice = new Invoice__c(Service_Period__c = 'test', Account__c = account.Id, Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30), Paid_Amount__c = 8000);
        insert invoice;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        invoiceList.add(invoice);
        
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                        price__c = 2000, Quickbooks_ID__c='28');
        insert product;
        
        List<Invoice_Line_Item__c> invoiceList1 = new  List<Invoice_Line_Item__c>();
        invoiceList1.add(new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                 Quantity__c = 5, Product__c = product.Id, QBO_Product__c = product.Id));
       
        List<QBWrappers.QBLine> lineList = new List<QBWrappers.QBLine>();  
        QBWrappers.QBLine qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Standard MCS Revenue (Existing)';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Startup MCS Revenue';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Member Services';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Standard MCS Revenue (New)';
        lineList.add(qbLineItem);  
          
        QBWrappers.QBInvoiceDTO qbInvoiceObj = new QBWrappers.QBInvoiceDTO();  
        qbInvoiceObj.Line = lineList;
        List<QBWrappers.QBInvoiceDTO> QBwrapperList = new  List<QBWrappers.QBInvoiceDTO>();
        QBwrapperList.add(qbInvoiceObj);
          
        QBInvoiceParser QBParser = new QBInvoiceParser();
          
        QBParser.parseToObjectList(QBwrapperList, 'Quickbooks2');
        QBParser.parseToObjectList(QBwrapperList, 'Quickbooks');
        //QBParser.parseToDTOList(invoiceList, 'Quickbooks2');
        QBParser.parseToDTOList(invoiceList, 'Quickbooks');  
        QBParser.convertStringToTime('2017-10-11T01:19:15-07:00');
        QBInvoiceParser.getChoppedEmails('test@test.com'); 
        QBParser.parseToDTOList(invoiceList);
        //invoiceList.add(invoice);
        //QBParser.parseToDTOList(invoiceList, 'Quickbooks2');   
    }
    
    static testmethod void testMethod10(){
          QuickBook2_LineItems_Configuration__c quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='MCS Member Services',
                                                                                                                          Quickbook2_Product_Id__c='22',
                                                                                                                          Quickbook2_Product_Name__c='MCS Member Services',
                                                                                                                          Salesforce_Product_Id__c='01tq0000002i0wT');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (New)',
                                                                                     Quickbook2_Product_Id__c='20',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (New)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wJ');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Startup MCS Revenue',
                                                                                     Quickbook2_Product_Id__c='21',
                                                                                     Quickbook2_Product_Name__c='Startup MCS Revenue',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wO');
        INSERT quickBook2LineItemsConfiguration;
        
        Account account = createParentAccount('Test Account ', 'abc@gmail.com');
        
        Invoice__c invoice = new Invoice__c(Service_Period__c = 'test', Account__c = account.Id, Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30), Paid_Amount__c = 8000);
        insert invoice;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        invoiceList.add(invoice);
        
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                        price__c = 2000, Quickbooks_ID__c='28');
        insert product;
        
        List<Invoice_Line_Item__c> invoiceList1 = new  List<Invoice_Line_Item__c>();
        invoiceList1.add(new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                 Quantity__c = 5, Product__c = product.Id, QBO_Product__c = product.Id));
       
        List<QBWrappers.QBLine> lineList = new List<QBWrappers.QBLine>();  
        QBWrappers.QBLine qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Standard MCS Revenue (Existing)';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Startup MCS Revenue';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Member Services';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Standard MCS Revenue (New)';
        lineList.add(qbLineItem);  
          
        QBWrappers.QBInvoiceDTO qbInvoiceObj = new QBWrappers.QBInvoiceDTO();  
        qbInvoiceObj.Line = lineList;
        List<QBWrappers.QBInvoiceDTO> QBwrapperList = new  List<QBWrappers.QBInvoiceDTO>();
        QBwrapperList.add(qbInvoiceObj);
          
        QBInvoiceParser QBParser = new QBInvoiceParser();
          
        QBParser.parseToObjectList(QBwrapperList, 'Quickbooks2');
        //QBParser.parseToObjectList(QBwrapperList, 'Quickbooks');
        //QBParser.parseToDTOList(invoiceList, 'Quickbooks2');
        //QBParser.parseToDTOList(invoiceList, 'Quickbooks');  
        //QBParser.convertStringToTime('2017-10-11T01:19:15-07:00');
        //QBInvoiceParser.getChoppedEmails('test@test.com'); 
        //QBParser.parseToDTOList(invoiceList);
        //invoiceList.add(invoice);
        QBParser.parseToDTOList(invoiceList, 'Quickbooks2');   
    }
    
     static Account createParentAccount(String accName,String billingEmail){
        
        
        Account recordAccount1 = new Account(
            Name = accName+'Parent',
            //QuickBooks_Customer_ID__c='80',
            QuickBooks_Customer_ID2__c='80',
            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
            ShippingStreet = '230 NE US Hwy 27',
            ShippingCity = 'Williston',
            ShippingState = 'Florida',
            ShippingCountry = 'United States',
            Website = 'gm.com',
            Invoice_Email_Address__c = billingEmail,
            Sync_to_QBO__c = true,
            QB_Sync_Token__c = '0',
            QB_Sync_Token2__c = '0',
            Phone = '7896541230',
            Fax = '(123) 487-7960',
            Description ='Bulk Records' );
        
        insert recordAccount1;
        return recordAccount1;
    }
    
}