/**
*@Author       : Dreamwares
*@Created Date : 19/07/2018
*@Purpose      : Batch to POST/push Invoices to Quick Book
*/
public with sharing class QBInvoicePushBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    public Set<Id> setInvoiceIds;
    public String currentQuickBookOrgName;
    
    public QBInvoicePushBatch(Set<Id> setInvoiceIds, String currentQBOrgName){
        this.setInvoiceIds = setInvoiceIds;
        currentQuickBookOrgName = currentQBOrgName;
    }
    public QBInvoicePushBatch(){   
        this.setInvoiceIds = new Set<Id>();
    }
    
    public Database.QueryLocator start(Database.BatchableContext info){
        String query = 'SELECT Id, Name, LastViewedDate, LastReferencedDate, Account__c,Account__r.Name, Invoice_Date__c, Submission_Status__c, '+
            'Invoice_Due_Date__c, Quickbooks_Invoice_ID__c, Paid_Date__c, Paid_Amount__c, Payment_Method__c, Payment_Reference_Number__c, '+
            'Is_Invoice_Paid__c, Quickbooks_Payment_Method_ID_From_QB__c, Quickbooks_Payment_Method_ID__c, Amount__c,'+
            'Standard_MCS_Revenue_Existing__c, Standard_MCS_Revenue_New__c, Startup_MCS_Revenue__c, Member_Services_Revenue__c,CreatedDate,'+
            'Quickbooks_Customer_ID__c, Quickbooks_Payment_Method__c, Quickbooks_Invoice_URL__c, Invoice_Number__c,'+ 
            'Quickbooks_Creation_Status__c, Quickbooks_Error_Message__c, Quickbooks2_Error_Message__c, Quickbooks_Created_Date__c, Auto_Send_Email_From_QBO__c,QB_Sync_Token__c, '+
            'QB_Terms_ID__c,Service_Period__c, Account__r.Invoice_Email_Address__c,Standard_MCS_Revenue_Existing_QBLI_ID__c,Standard_MCS_Revenue_New_QBLI_ID__c,Startup_MCS_Revenue_QBLI_ID__c,'+
            'Member_Services_Revenue_QBLI_ID__c,Quickbooks_Invoice_ID2__c, QB_Sync_Token2__c,Quickbooks2_Creation_Status__c,Quickbooks_Customer_ID2__c,'+
            ' (SELECT Id, Name, Invoice__c, Description__c, Line_Item_Total__c, Price__c,CreatedDate,QB_Line_Item_Id__c,'+
            'Quantity__c, Product__c,Product__r.Name, Invoice_ID__c, Quickbooks_Product_ID__c, QBO_Product__c, QBO_Product__r.Name, QBO_Product__r.ProductCode FROM Invoice_Line_Items__r)'+
            ' FROM Invoice__c WHERE Quickbooks_Customer_ID__c != NULL AND Id IN:setInvoiceIds';
        System.debug('Query +\n'+query);
        return Database.getQueryLocator(query);                           
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> invoiceListRec){
        QBInvoicePush invoiceSyncPush = new QBInvoicePush();
        List<Invoice__c> invoicesList = (List<Invoice__c>)invoiceListRec;
        system.debug('invoicesList '+invoicesList);
        List<QBBatchResponseObject> reponseRecordsList = new List<QBBatchResponseObject>();
        /*List<QBWrappers.QBCreditMemo > creditMemoList = new List<QBWrappers.QBCreditMemo >();
        List<QBWrappers.QBPayment > qbPaymentList = new List<QBWrappers.QBPayment>();*/
        
        invoicesList = updatedSyncTokensToInvoices(invoicesList,currentQuickBookOrgName);
        
        BAPIResponseWrapper response = new BAPIResponseWrapper();
        response = invoiceSyncPush.doPost(invoicesList,currentQuickBookOrgName);
        system.debug('## response '+ JSON.serialize(response));
        if(response != null && response.records != null ){
            reponseRecordsList.addAll((List<QBBatchResponseObject>)response.records);               
        }
        system.debug('## reponseRecordsList '+JSON.serialize(reponseRecordsList));
        if(!reponseRecordsList.isEmpty()){
            invoiceSyncPush.updateRecords(reponseRecordsList,null,currentQuickBookOrgName  );
        }
    }
    
    public void finish(Database.BatchableContext BC){
        //sendInvoiceMails();
    }
    
    public List<Invoice__c> updatedSyncTokensToInvoices(List<Invoice__c> invoiceList, String currentQBOrg){
        if(invoiceList != null && !invoiceList.isEmpty()){
            // create request parameter
            BRequestParams reqParams = new BRequestParams();
            reqParams.objectName = 'Invoice'; 
            reqParams.startIndex = 0;
            reqParams.qbIds = new List<String>();
            
            QBInvoicePull invoPull = new QBInvoicePull();
            BAPIResponseWrapper responseOfGetOpt;
            
            // populate Ids
            reqParams.qbIds = getListOfIds(invoiceList,currentQBOrg);
            
            system.debug('invoce sync pull reqParams '+reqParams);
            
            if(reqParams.qbIds != null && !reqParams.qbIds.isEmpty()){ responseOfGetOpt = invoPull.doGet(reqParams,currentQuickBookOrgName);
                system.debug('invoce sync pull responseOfGetOpt '+responseOfGetOpt);
                QBInvoiceParser parser = new QBInvoiceParser(); List<InvoiceToLineItemWrapper> invoiceToLineItems = (List<InvoiceToLineItemWrapper>)parser.parseToObjectList((List<Object>)responseOfGetOpt.records,currentQuickBookOrgName);
                system.debug('invoce sync pull invoiceToLineItems '+invoiceToLineItems);
                
  Map<String,String> mapOfSyncToken = getMapOfSyncToken(invoiceToLineItems,currentQBOrg);
                system.debug('invoce sync pull mapOfSyncToken '+mapOfSyncToken);
                // modify account list
                // 
                // 
                // 
                for(Invoice__c invRec : invoiceList){  if(currentQuickBookOrgName == 'Quickbooks'){  if(String.isNotBlank(invRec.Quickbooks_Invoice_ID__c) && mapOfSyncToken.containsKey(invRec.Quickbooks_Invoice_ID__c)){ invRec.QB_Sync_Token__c = mapOfSyncToken.get(invRec.Quickbooks_Invoice_ID__c); } }else if(currentQuickBookOrgName == 'Quickbooks2'){ if(String.isNotBlank(invRec.Quickbooks_Invoice_ID2__c) && mapOfSyncToken.containsKey(invRec.Quickbooks_Invoice_ID2__c)){ invRec.QB_Sync_Token2__c = mapOfSyncToken.get(invRec.Quickbooks_Invoice_ID2__c);
                        }
                    }
                    
                }
                
            }
            
        }
        system.debug('invoce sync pull invoiceList '+invoiceList);
        return invoiceList;
    } 
    public List<String> getListOfIds(List<Invoice__c> invoiceList, String currentQbOrg){
        List<String> idsList = new List<String>();
        if(invoiceList != null && !invoiceList.isEmpty() && currentQbOrg == 'Quickbooks'){
            for(Invoice__c invRec : invoiceList){
                if(String.isNotBlank(invRec.Quickbooks_Invoice_ID__c)){
                    idsList.add(invRec.Quickbooks_Invoice_ID__c);
                }
            }
        }
        
        if(invoiceList != null && !invoiceList.isEmpty() && currentQbOrg == 'Quickbooks2'){
            for(Invoice__c invRec : invoiceList){ if(String.isNotBlank(invRec.Quickbooks_Invoice_ID2__c)){  idsList.add(invRec.Quickbooks_Invoice_ID2__c);
                }
            }
        }
        return idsList;
    }
    
    public Map<String,String> getMapOfSyncToken(List<InvoiceToLineItemWrapper> invoiceToLineItemWrapperList,String currentQBOrg){
        Map<String,String> mapOfSyncToken = new Map<String,String>();
        if(invoiceToLineItemWrapperList != null && !invoiceToLineItemWrapperList.isEmpty()){
            for(InvoiceToLineItemWrapper invRec : invoiceToLineItemWrapperList){ if(currentQBOrg == 'Quickbooks'){  mapOfSyncToken.put(invRec.invoice.Quickbooks_Invoice_ID__c,invRec.invoice.QB_Sync_Token__c); }  if(currentQBOrg == 'Quickbooks2'){   mapOfSyncToken.put(invRec.invoice.Quickbooks_Invoice_ID2__c,invRec.invoice.QB_Sync_Token2__c); 
                }
            }
        }
        return mapOfSyncToken;
    }
    
    /*public void sendInvoiceMails(){
        if([SELECT count() 
             FROM AsyncApexJob 
             WHERE ApexClass.name ='SendEmailToNewInvoices' 
             AND (Status = 'Processing' OR Status = 'Preparing' OR Status = 'Holding')] < 1){ 
            
                 Database.executeBatch(new SendEmailToNewInvoicesBatch(), 10);
        }
    }*/
}