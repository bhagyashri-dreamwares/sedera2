public with sharing class QBQueryResponseWrapper {
    public BQBQueryResponse QueryResponse{get; set;}
    public String time_qb;
    
    public QBQueryResponseWrapper(){
        this.QueryResponse = new BQBQueryResponse();
    }
}