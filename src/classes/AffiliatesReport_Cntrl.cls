public without sharing class AffiliatesReport_Cntrl {

    public string AffiliateType {
        get;
        set;
    }
    public class customSOQldata {
        public String AffiliateName {
            get;
            set;
        }
        public List <Account> EmployerAccounts {
            get;
            set;
        }
        public List <Account> MemberAccounts {
            get;
            set;
        }

    }
    public list <customSOQldata> FormattedSOQLdataList {
        get;
        set;
    }
    public AffiliatesReport_Cntrl() {


        String AffiliateRecordTypeName = ApexPages.currentPage().getParameters().get('AffiliateRecordTypeName');
        system.debug(AffiliateRecordTypeName);

        AffiliateType = AffiliateRecordTypeName;
        List <Account> x = database.Query('Select name,Enrollment_Date__c,Dependent_Status__c, (Select affiliate__r.name,affiliate__r.id,affiliate__C from AffiliatesAccountsAssociation__r where Affiliate__r.RecordType.Name =\'' + AffiliateRecordTypeName + '\'), (Select name, Enrollment_date__c,Account_Employer_Name__r.name,Account_Employer_Name__r.default_product__c,Unique_Partner_ID__c,Sub_Affiliate__c, Dependent_status__c from accounts__r where (subscription_status__c = \'Active\' OR subscription_status__c = \'Pending Cancellation\') and health_care_sharing__c=true order by dependent_status__c) from account where recordtypeid = \'012o0000000GpW3\' AND ( Contract_Status__c =\'Renewal - Executed\' or Contract_Status__c =\'Renewal - In process\' or Contract_Status__c =\'Executed\')  order by name');



        list <Account> AccList = new list <Account> ();
        Map <id, List <Account>> MapAffiliateEmployer = new Map <id, List <Account>> ();
        Map <id, List <Account>> MapAffiliateMember = new Map <id, List <Account>> ();
        Map <id, string> MapAffiliateName = new Map <id, string> ();

        for (Account a: x) {
            for (AffiliateAccountAssociation__c AAA: a.AffiliatesAccountsAssociation__r) {
                if (AAA.affiliate__r.id != null) {
                    if (MapAffiliateEmployer.containsKey(AAA.affiliate__r.id)) {
                        List <Account> EmpAccs = MapAffiliateEmployer.get(AAA.affiliate__r.id);
                        EmpAccs.add(a);
                        MapAffiliateEmployer.put(AAA.affiliate__r.id, EmpAccs);

                        List <Account> MemAccs = MapAffiliateMember.get(AAA.affiliate__r.id);
                        MemAccs.addall(a.accounts__r);
                        MapAffiliateMember.put(AAA.affiliate__r.id, MemAccs);

                        string AffiliateName = MapAffiliateName.get(AAA.affiliate__r.id);
                        MapAffiliateName.put(AAA.affiliate__r.id, AAA.affiliate__r.name);

                    } else {
                        List <Account> EmpAccs = new List <Account> ();
                        EmpAccs.add(a);
                        List <Account> MemAccs = new List <Account> ();
                        MemAccs.addall(a.accounts__r);
                        MapAffiliateEmployer.put(AAA.affiliate__r.id, EmpAccs);
                        MapAffiliateMember.put(AAA.affiliate__r.id, MemAccs);
                        MapAffiliateName.put(AAA.affiliate__r.id, AAA.affiliate__r.name);
                    }
                }
            }


        }
        FormattedSOQLdataList = new list <customSOQldata> ();

        for (Id Id: MapAffiliateEmployer.keyset()) {
            customSOQldata CSD = new customSOQldata();
            CSD.EmployerAccounts = MapAffiliateEmployer.get(id);
            CSD.AffiliateName = MapAffiliateName.get(id);
            CSD.MemberAccounts = MapAffiliateMember.get(id);
            FormattedSOQLdataList.add(CSD);
        }
    }

}