/**
 * @Author      : Dreamwares
 * @Created Date  : 18/07/2018
 */
public class OAuth2 {
    private QBConfigurationWrapper config;
    
    public OAuth2(QBConfigurationWrapper config){
      this.config = config;    
    }
    
    /*
   *@Description    : - adds authorization header to request and returns
  */
    public HTTPRequest prepareRequest(HTTPRequest request){
        system.debug('isAccessTokenLive() '+isAccessTokenLive());
        if(isAccessTokenLive()){
            request.setHeader('Authorization', 'Bearer ' + config.accessToken);
        }
        else{
            Response tokenRefreshResponse = refreshToken();
            //OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)tokenRefreshResponse.Data;
            //QBConfigurationWrapper.saveTokens(oAuthResponse,config.currentQuickBookOrgName);
            if(tokenRefreshResponse.Success){
                OAuth2TokenResponse tokenDTO = (OAuth2TokenResponse)tokenRefreshResponse.Data;
                request.setHeader('Authorization', 'Bearer ' + tokenDTO.access_token);
                system.debug('tokenDTO.access_token '+tokenDTO.access_token);
            }
        }
        return request;
    }
    
    /**
    *@Description    : checks wheather access token is live or not (Expired or not)
   *@Returns      : Boolean
  */
    public Boolean isAccessTokenLive(){
      if(config.accessTokenExpiry > DateTime.now()){
          return true; 
        }
        return false;    
    }
    
    /**
    *@Description    : Refreshes access token
  */
    public Response refreshToken(){
        Response response;
        if(isRefreshTokenLive()){
            // Generate request body
            String requestBody = String.format('grant_type=refresh_token&refresh_token={0}', 
                                               new List<String>{ config.refreshToken});
            // Instantiate a new http object
            Http http = new Http();
            HttpRequest refreshTokenRequest = new HttpRequest();
            refreshTokenRequest.setEndpoint(config.accessTokenURL);
            refreshTokenRequest.setMethod('POST');
            refreshTokenRequest.setHeader('Accept', 'application/json');
            refreshTokenRequest.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            refreshTokenRequest.setHeader('Authorization', getAuthHeader());
            refreshTokenRequest.setBody(requestBody);
            try{
                // Send the request
                HttpResponse refreshTokenResponse = http.send(refreshTokenRequest);
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize(refreshTokenResponse.getbody(), OAuth2TokenResponse.class);
                
                if(refreshTokenResponse.getStatusCode() == 200){
                    
                    // Save tokens in database(Custom setting)
                    // This call is only for this specific scenario to store tokens in custon setting.
                    
                    QBConfigurationWrapper.saveTokens(oAuthResponse,config.currentQuickBookOrgName);
                    response = new Response(true, '', oAuthResponse);
                }
                else{
                    response = new Response(false, '', oAuthResponse);
                }
            } catch(Exception e){
                response = new Response(false, 'Error Occured while refresh token Callout : ' + e.getMessage(), null);
            }
        }
        else{
            response = new Response(false, 'Refresh token is expired', null);
        }
        return response;
    }
    
    /**
    *@Description    : checks wheather refresh token is live or not (Expired or not)
   *@Returns      : Boolean
  */
    public Boolean isRefreshTokenLive(){
        system.debug('config.refreshTokenExpiry  '+config.refreshTokenExpiry );
        system.debug('DateTime.now()  '+DateTime.now() );
        if(config.refreshTokenExpiry > DateTime.now()){
          return true; 
        }
        return false; 
    }
    
    public PageReference authorize(String scope, String state){
        config.state = state;
        
        // generate authorization URL
        String authURI = String.format('{0}?client_id={1}&response_type=code&scope={2}&redirect_uri={3}&state={4}', 
                                       new List<String>{config.authorizationURL,
                                                   config.consumerKey,
                                                   scope,
                                                   config.redirectURI,
                                                   config.state });
        
        PageReference authorizationPage = new PageReference(authURI);
        authorizationPage.setRedirect(true);
        return authorizationPage;
    }
    
    /*
   *@Description    : Gets access token with exchanging authorization code
   *@Returns      : Response 
   */
    public Response getAccessToken(String state, String code, String realmId){
        Response response;
        
        // Generate request body
        String requestBody = String.format('grant_type=authorization_code&code={0}&redirect_uri={1}', 
                                           new List<String>{code, EncodingUtil.urlEncode(config.redirectURI, 'UTF-8') });
        // Instantiate a new http object
        Http http = new Http();
        HttpRequest accessTokenRequest = new HttpRequest();
        accessTokenRequest.setEndpoint(config.accessTokenURL);
        accessTokenRequest.setMethod('POST');
        accessTokenRequest.setHeader('Accept', 'application/json');
        accessTokenRequest.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        accessTokenRequest.setHeader('Authorization', getAuthHeader());
        accessTokenRequest.setBody(requestBody);
        
        try{
            // Send the request
            HttpResponse accessTokenResponse = http.send(accessTokenRequest);
            System.debug('accessTokenResponse::'+accessTokenResponse);
            OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize(accessTokenResponse.getbody(), OAuth2TokenResponse.class);
            System.debug('oAuthResponse::'+oAuthResponse);
            if(accessTokenResponse.getStatusCode() == 200){
                response = new Response(true, '', oAuthResponse);
            }
            else{
                response = new Response(false, '', oAuthResponse);
            }
        } catch(Exception e){
            response = new Response(false, 'Error Occured while Callout : ' + e.getMessage(), null);
        }
        return response;
    }
    
    private String getAuthHeader(){
        String sign = EncodingUtil.base64Encode(Blob.valueOf(config.consumerKey + ':' + config.consumerSecret));
        return 'Basic ' + sign;
    }
}