/**
* @author:      Saurav Sundriyal
* @date:        2017-09-18
* @description: 
  @             Test class for SchedulableClass_For_resetting_NoOfNeeds         
  @            
  @             
* @log:
* -----------------------------------------------------------------------------
*   Developer                         Date                    Description
* -----------------------------------------------------------------------------
*  Saurav Sundriyal             2017-09-18            Newly Created
*                                                                      
*
*   
*  
**/



@IsTest
public class TestSchedulableClass_For_resetting_Needs
{

public static testmethod void TestMethod_For_resetting_Needs_Test1() {

        Test.startTest();
        recursivecheck.checkrecursive = 0;
        List < Account > PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Premier', true);

        recursivecheck.checkrecursive = 0;
        List < Account > PlanAccountList_Select = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);


        recursivecheck.checkrecursive = 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        recursivecheck.checkrecursive = 0;
        Account emp = EmployerAccountList[0];
        emp.parentid = PlanAccountList[0].id;
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        update emp;

        recursivecheck.checkrecursive = 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(3, true);

        recursivecheck.checkrecursive = 0;
        List < Account > Updated_Member_Acounts = new List < Account > ();
        Integer i = 0;

        List < Contact > ContactList = TestDataFactory.Create_Contact_Of_Sedera_Health_Contact_Type(3, true);

        for (Account mem: MemberAccountList) {

            mem.Account_Employer_name__c = emp.id;
            mem.Dependent_Status__c = 'EO';
            mem.parentid = PlanAccountList[0].id;
            mem.first_name__c = 'mm';
            mem.Enrollment_Date__c = myTestdate;
            if (i == 0) {
                mem.Dependent_Status__c = 'EF';
                mem.Subscription_Status__c = 'Pending Cancellation';
            } else if (i == 1) {
                mem.Dependent_Status__c = 'EC';
                mem.Subscription_Status__c = 'Active';
            }


            mem.Cancellation_Date__c = date.today();
            Updated_Member_Acounts.add(mem);
            i++;
        }

        Update Updated_Member_Acounts;

        Set < Id > Contact_Ids = new Set < Id > ();

        for (Integer j = 0; j < ContactList.size(); j++) {
            contactList[j].AccountId = Updated_Member_Acounts[j].id;
            Contact_Ids.add(contactList[j].Id);
        }

        Update ContactList;


        Map < id, List < Case >> Map_Of_Contact_And_Case_List = TestDataFactory.Create_Cases(2, Contact_Ids, true);

        
        SchedulableClass_For_resetting_NoOfNeeds sc=new SchedulableClass_For_resetting_NoOfNeeds();
        system.schedule('TestRun','0 0 00 1,15 * ?' ,sc);






        Test.StopTest();


    }




public static testmethod void TestMethod_For_resetting_Needs_Test2() {

        Test.startTest();
        recursivecheck.checkrecursive = 0;
        List < Account > PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Premier', true);

        recursivecheck.checkrecursive = 0;
        List < Account > PlanAccountList_Select = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);


        recursivecheck.checkrecursive = 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        recursivecheck.checkrecursive = 0;
        Account emp = EmployerAccountList[0];
        emp.parentid = PlanAccountList[0].id;
        
        emp.iua_chosen__c = 500;
        update emp;

        recursivecheck.checkrecursive = 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(3, true);

        recursivecheck.checkrecursive = 0;
        List < Account > Updated_Member_Acounts = new List < Account > ();
        Integer i = 0;

        List < Contact > ContactList = TestDataFactory.Create_Contact_Of_Sedera_Health_Contact_Type(3, true);

        for (Account mem: MemberAccountList) {

            mem.Account_Employer_name__c = emp.id;
            mem.Dependent_Status__c = 'EO';
            mem.parentid = PlanAccountList[0].id;
            mem.first_name__c = 'mm';
           
            if (i == 0) {
                mem.Dependent_Status__c = 'EF';
                mem.Subscription_Status__c = 'Pending Cancellation';
            } else if (i == 1) {
                mem.Dependent_Status__c = 'EC';
                mem.Subscription_Status__c = 'Active';
            }


            mem.Cancellation_Date__c = date.today();
            Updated_Member_Acounts.add(mem);
            i++;
        }

        Update Updated_Member_Acounts;

        Set < Id > Contact_Ids = new Set < Id > ();

        for (Integer j = 0; j < ContactList.size(); j++) {
            contactList[j].AccountId = Updated_Member_Acounts[j].id;
            Contact_Ids.add(contactList[j].Id);
        }

        Update ContactList;


        Map < id, List < Case >> Map_Of_Contact_And_Case_List = TestDataFactory.Create_Cases(2, Contact_Ids, true);

        
        SchedulableClass_For_resetting_NoOfNeeds sc=new SchedulableClass_For_resetting_NoOfNeeds();
        system.schedule('TestRun','0 0 00 1,15 * ?' ,sc);






        Test.StopTest();


    }


}