/*
* @description      : Positive mock response
* @author       : Navin (Dreamwares)
* @since        : 1 Oct 2018
* @date         : 1 Oct 2018 - Navin: created initial version
* @date         ${date} - ${user}: Change history
* @see      
*/
@isTest
public class PauBoxEmailBatchTestMock implements HttpCalloutMock{
    
    public HTTPResponse respond(HTTPRequest req) {
        HTTPResponse response = new HTTPResponse(); 
        if(req.getEndpoint().endsWith('messages'))
        {                    
            response.setStatusCode(200);
            response.setBody('{ "sourceTrackingId": "3d38ab13-0af8-4028-bd45-52e882e0d584", "data": "Service OK", }');
            
        }
        return response;
        
    }
    
}