/*
  MEC_Product_Trigger_Handler
  MEC_Product_Helper_Class
  MEC_Product_Trigger
*/

@istest
 public class Mec_Product_trigger_Test {
     public static testmethod void ChangeOfDiscountTierTest() {
         test.starttest();
         List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
         PriceList[0].name='Default Old Select Pricing';
         Update PriceList[0];
         List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
         AccessPricing[0].name='Default Access Pricing';
         AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
         insert AccessPricing[0];
         HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
         Test.setMock(HttpCalloutMock.class, Mockclass1);
         
         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         List < Account > PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(6, false);


         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         for (Account emp: EmployerAccountList) {
             emp.parentid = PlanAccountList[0].id;
         }
         insert EmployerAccountList;

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;

         for (Account emp: EmployerAccountList) {
             emp.parentid = PlanAccountList[0].id;
             emp.Original_Enrollment_Date__c = date.today();
         }
         Update EmployerAccountList;
         EmployerAccountList = [Select id from Account where recordtype.name = 'employer'
             order by Name ASC
         ];


         List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
         Mec_Product_List[1].Discount_Tier__c = 'T2';
         Mec_Product_List[1].name= 'ATestT2';
         update Mec_Product_List[1];
           
         List <AccountMECAssociation__c> Mec_Assosciation_List = new List <AccountMECAssociation__c> ();
         
         Mec_Product_List=[select id,name from mec_product__c order by name Asc];
        system.assertEquals(Mec_Product_List[0].name,'ASI-Pilot');
         for (integer j = 0; j < 6; j++) {
             for (Integer i = 0; i < 3; i++) {

                 List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                 if (i == 0 && j!=5) {
                     MecRec[0].Default_MEC_Product__c = true;
                 }else if(i==2 && j==5)
                 {
                  MecRec[0].Default_MEC_Product__c = true;
                 }

                 Mec_Assosciation_List.addAll(MecRec);

             }
         }


         insert Mec_Assosciation_List;

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(96, false);
         Integer memCount = 0;
         for (Account Employer: EmployerAccountList) {
             for (integer i = 1; i <= 16; i++) {
                 MemberAccountList[memCount].Account_Employer_name__C = Employer.id;
                 MemberAccountList[memCount].Date_of_Birth__c = date.today().addDays(-10);
                 MemberAccountList[memCount].subscription_status__c = 'Active';
                 MemberAccountList[memCount].IuA_Chosen__c = 500;
                 MemberAccountList[memCount].Dependent_status__C = 'EF';

                 memCount++;
                 if (Math.Mod(memCount, 16) == 0) {
                     break;
                 }
             }
         }

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         insert MemberAccountList;

         for(Account acc:MemberAccountList)
          acc.Health_Care_Sharing__c = true;

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         update MemberAccountList;
         
       // system.assertEquals([Select count() from Account where recordtype.name='Member' and Member_Discount_Tier_Manual__c='T2'],80);
         
         for (Account a: [Select id, Member_Discount_Tier_Manual__c  from Account where recordType.Name = 'Member' and Member_Discount_Tier_Manual__c='T2']) {
             system.assertEquals(a.Member_Discount_Tier_Manual__c, 'T2');

             
         }

         Set<Mec_product__C> MecProducts = new Set<Mec_product__C>();
         Set<Id> EmpIdList= new Set<Id>();

         for (AccountMECAssociation__c Amec: [Select id, MEC_Product__r.Id,Employer_Account__r.id from AccountMECAssociation__c where default_mec_product__c = true  and  MEC_Product__r.name='ATestT2']) {
             
             MecProducts.add(new Mec_product__C (id = Amec.MEC_Product__r.Id, discount_tier__C = 'T1'));
             EmpIdList.add(Amec.Employer_Account__r.id);
         }
         
         if (MecProducts.size() > 0) {
         List<Mec_product__C> MecList=new List<Mec_product__c>();
         MecList.addAll(MecProducts);
         UtilityClass_For_Static_Variables.CheckRecursiveForMECproductTrigger = 0;
          Update MecList;

          test.stoptest();
             for (Account a: [Select id, Account_employer_name__c, Member_Discount_Tier_Manual__c from Account where recordType.Name = 'Member' and Account_employer_name__c in:EmpIdList]) {                
                // system.assertEquals(a.Member_Discount_Tier_Manual__c, 'T1');
                 
             }
        //system.assertEquals([Select count() from Account where recordtype.name='Member' and Account_employer_name__c in:EmpIdList],80);
         }
     }

 }