global class RefreshDwollaToken implements Schedulable,Database.AllowsCallouts {

     global void execute(SchedulableContext ctx) {
        try {
           // Abort the job you have just scheduled via Setup 
           CronTrigger ct = [SELECT id, CronJobDetail.Name 
                             FROM CronTrigger 
                             WHERE id = :ctx.getTriggerId()];
           if(ct != null) {
               System.abortJob(ct.Id);
           }
       }
       catch(Exception e) {
           System.debug('No jobs scheduled ' + e.getMessage()); 
       }   
       RefreshDwollaToken.futureDwollaAccess();
    }
    
    public static void updateToken(DwollaAPIConfiguration__c config){
        
        try{
            update config;
        }catch(Exception exp){
            
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('RefreshDwollaToken.updateToken', exp, 'config : '+config, 
                                                                 '', 'Error'))});
        }
        reScedule();
    }
    
    @future(callout=true)
    public static void futureDwollaAccess() {
        system.debug('is this ::::::::');
        DwollaAPIConfiguration__c config = DwollaAccessTokenHelper.getAPIConfiguration();
        system.debug('is config this ::::::::'+config);
        DwollaAccessTokenHelper helper = new DwollaAccessTokenHelper();
        
        config = helper.getLiveToken(config);    
        updateToken(config);
    }
    
     public static void reScedule() {
       // start keepalive again in 2 mins
       Datetime sysTime = System.now().addSeconds(2700);
       system.debug('sysTime '+ sysTime);
       String chronExpression = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
       system.debug('chronExpression'+ chronExpression);
       if(!Test.isRunningTest()) {
           
           System.schedule( 'Refresh Dwolla Token', chronExpression, new RefreshDwollaToken() );
       }
    }     
}