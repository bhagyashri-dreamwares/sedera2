/*
 *@Purpose    :
 *@Author    : Dreamwares
 *@Created Date : 19/07/2018
 */
public with sharing class QBInvoiceBatchRequestWrapper {
  public List<QBBatchInvoiceObject> BatchItemRequest;

  public QBInvoiceBatchRequestWrapper(){
    BatchItemRequest = new List<QBBatchInvoiceObject>();
  }

  public class QBBatchInvoiceObject{
    public String bId;
    public String operation;
    public QBWrappers.QBInvoiceDTO Invoice;
    
    public QBBatchInvoiceObject(){
      Invoice = new QBWrappers.QBInvoiceDTO();
    }
    
    public QBBatchInvoiceObject(String bId, String operation){
      this();
      this.bId = bId;
      this.operation = operation;
    }
  }
}