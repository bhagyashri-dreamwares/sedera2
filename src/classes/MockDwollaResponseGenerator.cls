/**
* @Purpose      : - MockDwollaResponseGenerator will ganerate fake response for test class DwollaCustomerHelperTest.
*/
@isTest
global class MockDwollaResponseGenerator implements HttpCalloutMock{
    // Implement this interface method
    
    integer flag;
    
    /**
    * @Purpose      : - constructor set value for flage parameter in MockHttpResponseGenerator.
    * Parameters    : - inputflag flag for specific condition.
    */
    global MockDwollaResponseGenerator(integer inputflag){
        
        //based on flage value comming as parameter the response will set.
        this.flag = inputflag;
    }
    
    /**
    * @Purpose      : - Create fake response.
    */
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        if(flag == 1){
            //flag = 1 set all right response.
            // Create a fake response
            res.setBody('{ "_links": { "self": { "href": "https://api-sandbox.dwolla.com/customers/01344eb9-b22d-41cb-b539-6cfa6cbf4236/funding-sources", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "funding-source" }, "customer": { "href": "https://api-sandbox.dwolla.com/customers/01344eb9-b22d-41cb-b539-6cfa6cbf4236", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "customer" } }, "_embedded": { "funding-sources": [ { "_links": { "transfer-from-balance": { "href": "https://api-sandbox.dwolla.com/transfers", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "transfer" }, "self": { "href": "https://api-sandbox.dwolla.com/funding-sources/23b8f9c5-e2e8-4eef-989e-ae702257847e", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "funding-source" }, "transfer-to-balance": { "href": "https://api-sandbox.dwolla.com/transfers", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "transfer" }, "transfer-send": { "href": "https://api-sandbox.dwolla.com/transfers", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "transfer" }, "remove": { "href": "https://api-sandbox.dwolla.com/funding-sources/23b8f9c5-e2e8-4eef-989e-ae702257847e", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "funding-source" }, "customer": { "href": "https://api-sandbox.dwolla.com/customers/01344eb9-b22d-41cb-b539-6cfa6cbf4236", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "customer" }, "transfer-receive": { "href": "https://api-sandbox.dwolla.com/transfers", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "transfer" } }, "id": "23b8f9c5-e2e8-4eef-989e-ae702257847e", "status": "verified", "type": "bank", "bankAccountType": "savings", "name": "Testing1- Customer", "created": "2019-01-09T10:58:15.595Z", "removed": false, "channels": [ "ach" ], "bankName": "SANDBOX TEST BANK", "fingerprint": "f52aa53d8ac64ec8996a1a5f04f6a1b011f69aba76ef058344358710687d092a" }, { "_links": { "self": { "href": "https://api-sandbox.dwolla.com/funding-sources/e5edf3ad-18b5-46b3-839e-916ede21401e", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "funding-source" }, "balance": { "href": "https://api-sandbox.dwolla.com/funding-sources/e5edf3ad-18b5-46b3-839e-916ede21401e/balance", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "balance" }, "transfer-send": { "href": "https://api-sandbox.dwolla.com/transfers", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "transfer" }, "with-available-balance": { "href": "https://api-sandbox.dwolla.com/funding-sources/e5edf3ad-18b5-46b3-839e-916ede21401e", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "funding-source" }, "customer": { "href": "https://api-sandbox.dwolla.com/customers/01344eb9-b22d-41cb-b539-6cfa6cbf4236", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "customer" }, "transfer-receive": { "href": "https://api-sandbox.dwolla.com/transfers", "type": "application/vnd.dwolla.v1.hal+json", "resource-type": "transfer" } }, "id": "e5edf3ad-18b5-46b3-839e-916ede21401e", "status": "verified", "type": "balance", "name": "Balance", "created": "2019-01-09T10:57:20.280Z", "removed": false, "channels": []}]}}');    
            res.setStatusCode(200);
        }
        if(flag == 0){
            //flage = NULL set response as NULL. 
            res.setStatusCode(404);
             res.setBody('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }');
        }
        res.setHeader('Content-Type', 'application/json');
        return res;
    }
    
}