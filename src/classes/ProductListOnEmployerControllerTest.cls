@Istest

public class ProductListOnEmployerControllerTest{

    public static testmethod void Testmethod1() {
        Test.startTest();
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
        AccessPricing[0].name='Default Access Pricing';
        AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        insert AccessPricing[0];

        List < Account > PlanAcc = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > EmployerAcc = TestDataFactory.Create_Account_Of_Employer_type(1, true);
        List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAcc[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;

        List < Account > MemberAcc = TestDataFactory.Create_Account_Of_Member_type(2, false);

      for(Account acc:MemberAcc){
        acc.Account_Employer_Name__c = EmployerAcc[0].id;
        acc.Subscription_Status__c = 'Active';
        acc.Enrollment_Date__c = system.now().date();
        acc.C_MEC__c = true;        
        acc.HSA__c = true;
        acc.Teladoc_Direct__c = true;
        acc.SecondMD__c = true;        
        acc.MS_Select__c = true;
        acc.PMA__c = true;
        acc.Liberty_Rx__c = true;
        acc.Tobacco_use__c = true;
        acc.IUA_Chosen__C = 1000;
        acc.Date_of_Birth__c = system.now().date().adddays(-222);
        acc.Dependent_Status__c = 'EF';
       }
        
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        insert MemberAcc;

        PageReference pg = Page.ProductListOnEmployerPage;
        Test.setCurrentPage(pg);

        Apexpages.standardController sc = new Apexpages.standardController(EmployerAcc[0]);
        ProductListOnEmployerController Controller = new ProductListOnEmployerController(sc);
        controller.DownloadAsExcel();

        test.stoptest();


    }



}