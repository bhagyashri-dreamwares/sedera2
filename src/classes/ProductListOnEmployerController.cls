/*
TestClass: ProductListOnEmployerControllerTest
*/

public class ProductListOnEmployerController
{
 
//Map for showing Quantity of each product
public Map<string,Pricing> ProductCountMap{get;set;}
public integer ProductcountmapSize{get;set;}
public string DownloadAsExcel{get;set;}
public string EmpaccountName{get;set;}
  public ProductListOnEmployerController(Apexpages.standardcontroller sc)
  {
     //Pull all assets attached to this Employer record
    Date dtf=date.today().addMonths(1).toStartOfMonth();
    Set <string> PSMecProductname = new Set <string> ();
    for (PSMecProducts__mdt PS: [select id, DeveloperName, PSMecProductName__c from PSMecProducts__mdt]) {
         PSMecProductname.add(PS.PSMecProductName__c);
     }
    Account Employer=  [select id,name,(Select MEC_Product__r.Name, MEC_Product__r.Id, MEC_Product__r.Discount_Tier__c from AccountsMECsAssociation__r where Default_MEC_Product__c = true limit 1),(SELECT EC_Tier__c,EF_Tier__c,Employer_Name__c,EO_Tier__c,ES_Tier__c,Liberty_Rx__c,MSEC__c,MSEF__c,MSEO__c,MSES__c,Name,PSEC__c,PSEF__c,PSEO__c,PSES__c,Tobacco_Use__c FROM Pricings__r order by createddate desc limit 1 ),(select id,dependent_status__c,iua_chosen__c,MCS_Product_Code__c,MCS_Product_Amount__c,member_discount_tier_manual__c,liberty_rx__c,tobacco_use__c from Accounts__r where (Subscription_Status__c='Pending Start Date' or Subscription_Status__c='Active') and enrollment_date__c <=:dtf) from Account where id=:sc.getRecord().Id limit 1];
    EmpaccountName=Employer.name;
    ProductCountMap=new Map<string,Pricing>();
    system.debug('ccc '+Employer.pricings__r);
    
    if(Employer.pricings__r.size()>0){
    Pricing__c pr=Employer.pricings__r[0];
    for(Account a:Employer.Accounts__r)
    {
                           if(a.dependent_status__c!=null)
                              {
                                if(ProductCountMap.containsKey('MS'+a.dependent_status__c))
                                {
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('MS'+a.dependent_status__c+'__c')+'');
                                   x.Count=ProductCountMap.get('MS'+a.dependent_status__c).count+1;
                                   x.TotalPrice=x.count*x.UnitPrice;
                                   ProductCountMap.put('MS'+a.dependent_status__c,x);
                                }else{
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('MS'+a.dependent_status__c+'__c')+'');
                                   x.Count=1;
                                   x.TotalPrice=x.UnitPrice;
                                   ProductCountMap.put('MS'+a.dependent_status__c,x);
                                
                                 }
                                 if(a.MCS_Product_Code__c!=null){
                                 if(ProductCountMap.containsKey(a.MCS_Product_Code__c))
                                {
                                   Pricing x=new pricing();
                                   x.UnitPrice=a.MCS_Product_Amount__c;
                                   x.Count=ProductCountMap.get(a.MCS_Product_Code__c).count+1;
                                   x.TotalPrice=x.count*x.UnitPrice;
                                   ProductCountMap.put(a.MCS_Product_Code__c,x);
                                }else{
                                   Pricing x=new pricing();
                                   x.UnitPrice=a.MCS_Product_Amount__c;
                                   x.Count=1;
                                   x.TotalPrice=x.UnitPrice;
                                   ProductCountMap.put(a.MCS_Product_Code__c,x);
                                
                                 }
                                 }

                                 
                                 
                              } 
                                 if(a.Tobacco_Use__c && ProductCountMap.containsKey('Tobacco Use'))
                                {
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('Tobacco_Use__c')+'');
                                   x.Count=ProductCountMap.get('Tobacco Use').count+1;
                                   x.TotalPrice=x.count*x.UnitPrice;
                                   ProductCountMap.put('Tobacco Use',x);
                                }else if(a.Tobacco_Use__c){
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('Tobacco_Use__c')+'');
                                   x.Count=1;
                                   x.TotalPrice=x.UnitPrice;
                                   ProductCountMap.put('Tobacco Use',x);
                                
                                 }
                                
                                 
                                 
                                 if(a.Liberty_Rx__c && ProductCountMap.containsKey('Liberty Rx'))
                                {
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('Liberty_Rx__c')+'');
                                   x.Count=ProductCountMap.get('Liberty Rx').count+1;
                                   x.TotalPrice=x.count*x.UnitPrice;
                                   ProductCountMap.put('Liberty Rx',x);
                                }else if(a.Liberty_Rx__c){
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('Liberty_Rx__c')+'');
                                   x.Count=1;
                                   x.TotalPrice=x.UnitPrice;
                                   ProductCountMap.put('Liberty Rx',x);
                                
                                 }
                                 if(Employer.AccountsMECsAssociation__r.size()>0 && PSMecProductname.contains(Employer.AccountsMECsAssociation__r[0].MEC_Product__r.name) && ProductCountMap.containsKey('PS'+a.dependent_status__c))
                                {
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('PS'+a.dependent_status__c+'__c')+'');
                                   x.Count=ProductCountMap.get('PS'+a.dependent_status__c).count+1;
                                   x.TotalPrice=x.count*x.UnitPrice;
                                   ProductCountMap.put('PS'+a.dependent_status__c,x);
                                }else if(Employer.AccountsMECsAssociation__r.size()>0 && PSMecProductname.contains(Employer.AccountsMECsAssociation__r[0].MEC_Product__r.name)){
                                   Pricing x=new pricing();
                                   x.UnitPrice=Decimal.valueOf(pr.get('PS'+a.dependent_status__c+'__c')+'');
                                   x.Count=1;
                                   x.TotalPrice=x.UnitPrice;
                                   ProductCountMap.put('PS'+a.dependent_status__c,x);
                                
                                 }

                                
                                 
                      
                   
    
    } 
    
    for(Pricing pricing:ProductCountMap.Values())
    {
      if(pricing.TotalPrice!=null)
      pricing.TotalPrice=Math.round(pricing.TotalPrice);   
    }
    ProductcountmapSize=ProductCountMap.keyset().size();
  
  }else
  {
  ProductcountmapSize=0;
  
  }
  
  }
  
  public pagereference DownloadAsExcel()
  {
    
    DownloadAsExcel='application/vnd.ms-excel#'+EmpaccountName+'_Linked Products.xls';
    return null;
  
  }
 
 public class Pricing{
   public decimal TotalPrice{get;set;}
   public decimal UnitPrice{get;set;}
   public Integer Count{get;set;}

 }
}