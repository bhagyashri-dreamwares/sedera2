public interface IParser{
    List<Object> parseToDTOList(List<Object> recordsList);
    
    //List<Object> parseToObjectList(List<Object> recordsList); 
    
    List<Object> parseToDTOList(List<Object> recordsList, String currentQuickBookOrg);
    
    List<Object> parseToObjectList(List<Object> recordsList, String currentQuickBookOrg);
   
}