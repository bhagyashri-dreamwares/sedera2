@isTest
//Test class for all commission work
//Apex classes: CommissionDetailController, ProcessCommission, ProcessCommissionReport
public class CommissionDetailControllerTest 
{
	 @TestSetup
    public static void createTestData()
    {
        
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
        AccessPricing[0].name='Default Access Pricing';
        AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        insert AccessPricing[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 0;
        
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass1);
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        emp.name = 'Test';
        update emp;


        List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(1, false);
        for (integer i = 0; i < MemberAccountList.size(); i++) {
            MemberAccountList[i].Account_Employer_name__C = emp.id;
            MemberAccountList[i].subscription_status__c = 'Active';
            MemberAccountList[i].Enrollment_Date__c  = Date.today().addMonths(-3).toStartofMonth();
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        
        insert MemberAccountList;
        
        Invoice__c invoice = new Invoice__c(Account__c = emp.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(1).toStartofMonth(),
                                            Paid_Amount__c = 20000, Invoice_Number__c = 'I1111', Submission_Status__c = 'Approved');
        insert invoice;
        
        Invoice__c invoice2 = new Invoice__c(Account__c = emp.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(1).toStartofMonth(),
                                            Paid_Amount__c = 20000, Invoice_Number__c = 'I1112', Submission_Status__c = 'Approved');
        insert invoice2;
        
        Product2 product = new Product2(Name = 'Access', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus');
        insert product;
        
        Asset sederaPrduct = new Asset(Name = 'Access', Account = MemberAccountList[0], Product2 = product, Employer_Account__c = emp.Id);
        insert sederaPrduct;
        System.debug('s AccountId::' + MemberAccountList[0]);
  
        Affiliate__c aff = new Affiliate__c(Name = 'test affiliate');
        insert aff;
        
        Affiliate__c aff2 = new Affiliate__c(Name = 'test affiliate2');
        insert aff2;
        
        Sedera_Product_Report__c sederaProduct1 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'EO', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'ACCESS', Pricing__c = 'NEW',
                                                                             Doing_Frontline_Support__c = TRUE);
        insert sederaProduct1;
        
         Sedera_Product_Report__c sederaProduct5 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'ES', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'ACCESS', Pricing__c = 'NEW',
                                                                             Doing_Frontline_Support__c = TRUE);
        insert sederaProduct5;
        
        Sedera_Product_Report__c sederaProduct6 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'EC', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'ACCESS', Pricing__c = 'NEW',
                                                                             Doing_Frontline_Support__c = TRUE);
        insert sederaProduct6;
        
        Sedera_Product_Report__c sederaProduct7 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'EO', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'ACCESS', Pricing__c = 'NEW');
        insert sederaProduct7;
        
        Sedera_Product_Report__c sederaProduct8 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'ES', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'ACCESS', Pricing__c = 'NEW');
        insert sederaProduct8;
        
        Sedera_Product_Report__c sederaProduct9 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'EC', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'ACCESS', Pricing__c = 'NEW');
        insert sederaProduct9;
        
        Sedera_Product_Report__c sederaProduct4 = new Sedera_Product_Report__c(Amount__c = 2500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'EF', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 22, 
                                                                             Subscription_Status__c = 'Test', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id , Product__c = 'SELECT', Pricing__c = 'OLD');
        insert sederaProduct4;
        
        Sedera_Product_Report__c sederaProduct2 = new Sedera_Product_Report__c(Amount__c = 500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'EC', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 28, 
                                                                             Subscription_Status__c = 'Active', Sedera_Product__c = sederaPrduct.Id,
                                                                             Referral_Affiliate__c = aff.id, Product__c = 'SELECT', Pricing__c = 'NEW');
        insert sederaProduct2;
        
        Sedera_Product_Report__c sederaProduct3 = new Sedera_Product_Report__c(Amount__c = 500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'ES', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 28, 
                                                                             Subscription_Status__c = 'Active', Sedera_Product__c = sederaPrduct.Id,
                                                                             Internal_Salesperson__c = aff2.id, Product__c = 'SELECT', Pricing__c = 'OLD' );
        insert sederaProduct3;
        
        Sedera_Product_Report__c sederaProduct10 = new Sedera_Product_Report__c(Amount__c = 500, Date_Captured__c = Date.today(),
                                                                             Dependent_Status__c = 'ES', Member_Account__c = MemberAccountList[0].id,
                                                                             Invoice__c = invoice2.Id, Primary_Age__c = 28, 
                                                                             Subscription_Status__c = 'Active', Sedera_Product__c = sederaPrduct.Id,
                                                                             Internal_Salesperson__c = aff2.id, Product__c = 'ACCESS', Pricing__c = 'NEW' );
        insert sederaProduct10;
        
        Affiliate_Pricing__c apRecord = new Affiliate_Pricing__c(Affiliate__c = aff.id,ACCESS_RA_EC__c = 10,
                    		ACCESS_RA_EF__c = 10,
                            ACCESS_RA_EO__c = 10,
                            ACCESS_RA_ES__c = 10,
                            ACCESS_RA_NOT_EC__c = 10,
                            ACCESS_RA_NOT_EF__c = 10,
                            ACCESS_RA_NOT_EO__c = 10,
                            ACCESS_RA_NOT_ES__c = 10,
                            SELECT_NEW_100_EC__c = 10,
                            SELECT_NEW_100_EF__c = 10,
                            SELECT_NEW_100_EO__c = 10,
                            SELECT_NEW_100_ES__c = 10,  
                            SELECT_OLD_100_EC__c = 10,
                            SELECT_OLD_100_EF__c = 10,
                            SELECT_OLD_100_EO__c = 10,
                            SELECT_OLD_100_ES__c = 10,
                            New_Lives_in_Territory_Access__c = 10,
                            New_Lives_in_Territory_Select__c = 10,                                   
                            Effective_Date__c = Date.today().addDays(-50));
        insert apRecord;
        
        Affiliate_Pricing__c apRecord2 = new Affiliate_Pricing__c(Affiliate__c = aff2.id,ACCESS_RA_EC__c = 10,
                    		ACCESS_RA_EF__c = 10,
                            ACCESS_RA_EO__c = 10,
                            ACCESS_RA_ES__c = 10,
                            ACCESS_RA_NOT_EC__c = 10,
                            ACCESS_RA_NOT_EF__c = 10,
                            ACCESS_RA_NOT_EO__c = 10,
                            ACCESS_RA_NOT_ES__c = 10,
                            SELECT_NEW_100_EC__c = 10,
                            SELECT_NEW_100_EF__c = 10,
                            SELECT_NEW_100_EO__c = 10,
                            SELECT_NEW_100_ES__c = 10,  
                            SELECT_OLD_100_EC__c = 10,
                            SELECT_OLD_100_EF__c = 10,
                            SELECT_OLD_100_EO__c = 10,
                            SELECT_OLD_100_ES__c = 10,
                            New_Lives_in_Territory_Access__c = 10,
                            New_Lives_in_Territory_Select__c = 10,                                   
                            Effective_Date__c = Date.today().addDays(-50));
        insert apRecord2;
        
        
    }
    
    /**
     * @Purpose: Test page Invoice 
     */
    static testMethod void testCommissionDetailsPage() 
    {
        //Account account = [SELECT Id FROM Account WHERE Name = 'Test' LIMIT 1];
        //Invoice__C invoice = [SELECT Id FROM Invoice__c WHERE Account__c =: account.Id LIMIT 1];
        //Sedera_Product_Report__c spr = [SELECT Id FROM Sedera_Product_Report__c WHERE Employer_Account__c =: account.Id LIMIT 1];
        Affiliate__c aff = [SELECT Id FROM Affiliate__c WHERE Name = 'test affiliate' LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.CommissionDetail;
        Test.setCurrentPageReference(pageRef);
        Apexpages.currentpage().getparameters().put('id', String.valueOf(aff.Id));
        Apexpages.currentpage().getparameters().put('p1', String.valueOf(Date.today().toStartofMonth().month() ));
        Apexpages.currentpage().getparameters().put('p2', String.valueOf(Date.today().toStartofMonth().year() ));
        CommissionDetailController controller = new CommissionDetailController();
        
        Test.stopTest();
        
        //System.assertEquals(500, controller.totalCommission); 
    }
    
    static testMethod void testCommissionDetailsPage2() 
    {
        //Account account = [SELECT Id FROM Account WHERE Name = 'Test' LIMIT 1];
        //Invoice__C invoice = [SELECT Id FROM Invoice__c WHERE Account__c =: account.Id LIMIT 1];
        //Sedera_Product_Report__c spr = [SELECT Id FROM Sedera_Product_Report__c WHERE Employer_Account__c =: account.Id LIMIT 1];
        Affiliate__c aff = [SELECT Id FROM Affiliate__c WHERE Name = 'test affiliate2' LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.CommissionDetail;
        Test.setCurrentPageReference(pageRef);
        Apexpages.currentpage().getparameters().put('id', String.valueOf(aff.Id));
        Apexpages.currentpage().getparameters().put('p1', String.valueOf(Date.today().toStartofMonth().month() ));
        Apexpages.currentpage().getparameters().put('p2', String.valueOf(Date.today().toStartofMonth().year() ));
        CommissionDetailController controller = new CommissionDetailController();
        
        Test.stopTest();
        
        //System.assertEquals(500, controller.totalCommission); 
    }
    
    /**
     * @Purpose: Test page 
     */
    static testMethod void testNegCase() {
                
        Test.startTest();
        PageReference pageRef = Page.CommissionDetail;
        Test.setCurrentPageReference(pageRef);
        CommissionDetailController controller = new CommissionDetailController();   
        Boolean isValidId = controller.isValidSalesforceId('0021D00000Imspr10B', Account.class);
        Test.stopTest();
    }
    
    static testMethod void testProcessCommission() 
    {
        Test.startTest();
        
        Commission_Report__c cr = new Commission_Report__c(Year__c = String.valueOf(Date.today().toStartofMonth().year()), 
                                                           Month__c = String.valueOf(Date.today().toStartofMonth().month()) );
        insert cr;
        
        ProcessCommissionReport.executeBatchForReprocess(cr.id);
        
        Test.stopTest();
    }
}