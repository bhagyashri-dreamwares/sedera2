/**
*@Purpose : Webservice to catch event for PLaid Automated microdeposit auth Flow
*@Date : 08/04/2019
*/
@RestResource ( urlMapping = '/PlaidWebService/*' )
global class PlaidWebService {
    
    public static List<Dwolla_Error_Response__c> errorList;
    public static List<Log__c> logList;
    
    @HttpPost
    global static void postPlaidWebService() {
    
        errorList = new List<Dwolla_Error_Response__c>();
        logList = new List<Log__c>();
        RestRequest request = RestContext.request;        
        webhookResponse response = (webhookResponse)JSON.deserialize(request.requestBody.toString(), webhookResponse.class);
        
        String accountId = request.params.get('id');
        
        if(String.isNotBlank(accountId)){
            
            Account account = FundingSourceHandler.getAccount(accountId);
            if(response.webhook_code.equalsIgnoreCase('AUTOMATICALLY_VERIFIED')){
                
                account.Plaid_Account_Status__c = 'Verified';
                
                if(String.isNotBlank(account.Plaid_Access_Token__c)){
                    
                    assignDwollaFundingSource(account, response, errorList,logList);
                }else{
                    account.Description = 'Plaid Access token not found.';
                    logList.add(LogUtil.createLog('PlaidWebService.postPlaidWebService', null, 'Plaid Access token not found. \n\n Request Parameters:\nAccount: '+account, '', 'Error'));
                }                
            }else{                            
                account.Plaid_Account_Status__c = 'UnVerified';
            }   
            FundingSourceHandler.updateAccount(account);
        }else{
            logList.add(LogUtil.createLog('PlaidWebService.postPlaidWebService', null, 'Salesforce Account id not found in request parameters. \n\n Request Parameters:\nRequest.params: '+accountId, '', 'Error'));
        }
        
        if(!logList.isEmpty()){
           LogUtil.saveLogs(logList);
        }
        if(!errorList.isEmpty()){
           DwollaErrorResponseHandler.saveDwollaErrorResponse(errorList);
        }
    }
   
   /**
   * To assign funding source to Dwolla
   */ 
    private static void assignDwollaFundingSource(Account account, webhookResponse response, List<Dwolla_Error_Response__c> errorList,
                                            List<Log__c> logList){
        
        PlaidAPIConfiguration__c config = PlaidAPIConfiguration__c.getOrgDefaults();
        DwollaAPIConfiguration__c dwollaConfig = DwollaAPIConfiguration__c.getOrgDefaults();
        
        //Get Processor token
        String processorToken = FundingSourceHandler.getProcessorToken(account.Plaid_Access_Token__c, 
                                                                      response.account_id, config, errorList, logList);        
        
        if(String.isNotBlank(processorToken)){
            
            String fundingSourceId = assignFundingSource(processorToken, errorList, account.Name, logList, account, dwollaConfig);
            if(String.isNotBlank(fundingSourceId)){
                
                PlaidDTO.FundingSourceDetails fundingSourceDetails = FundingSourceHandler.getFundingSource(fundingSourceId, 
                                                                                             dwollaConfig, errorList, logList);
                if(fundingSourceDetails != null){
                           
                    Response fundingSourceResponse = FundingSourceHandler.saveDwollaFundingSource(fundingSourceDetails, account.Id, 
                                                                                                  logList, account.Plaid_Access_Token__c, 
                                                                                                  response.account_id);  
                }else{
                    logList.add(LogUtil.createLog('PlaidWebService.assignDwollaFundingSource', null, 'Dwolla fundingSource detail Not Found. \n\n Input Parameters :\nfundingSourceId : '+fundingSourceId+
                                                  '\ndwollaConfig :'+dwollaConfig, '', 'Error'));
                }
            }else{
                logList.add(LogUtil.createLog('PlaidWebService.assignDwollaFundingSource', null, 'Dwolla fundingSourceId Not Found. \n\n Input Parameters :\nprocessorToken: '+processorToken+
                                           '\naccount :'+account+'\n dwollaConfig :'+dwollaConfig, '', 'Error'));
            }
        }else{
            logList.add(LogUtil.createLog('PlaidWebService.assignDwollaFundingSource', null, 'processorToken Not Found. \n\n Input Parameters :\naccount.Plaid_Access_Token__c: '+account.Plaid_Access_Token__c+
                                           '\nresponse.account_id :'+response.account_id+'\nconfig :'+config, '', 'Error'));
        }
    }
   
    public static String assignFundingSource(String plaidProcessorToken, List<Dwolla_Error_Response__c> errorList, String accountName, 
                                      List<Log__c> logList, Account account, DwollaAPIConfiguration__c dwollaConfig){
    
        if(dwollaConfig != null){            
            return FundingSourceHandler.createDwollaFundingSource(plaidProcessorToken, dwollaConfig.Access_Token__c, '', '', '',
                                                                  dwollaConfig, account.Dwolla_ID__c, account.Name+'- Customer',
                                                                  errorList, logList);   
        }
        return '';
    }
    
    public class webhookResponse{
    
        public String webhook_type;
        public String webhook_code;
        public String item_id;
        public String account_id;
    }
}