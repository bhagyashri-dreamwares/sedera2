/*
TestClass:Prepareforrenewalaction_Test
*/

global class Prepareforrenewalaction {
    webservice static string Inrenewalmethod(Id empid) 
    {
        String message = 'All active records already prepared for renewal action or No active record/records found.';
        list <Account> memAccs = new list <Account> ();
        for (Account acc: [select id from Account where account_employer_name__c =: empid and subscription_status__c = 'active']) {
            acc.Membership_Stage__c = 'In Renewal - Incomplete';
            acc.Primary_Member_Principles__c = 'Incomplete';
            acc.Ancillary_Flow__c = 'Incomplete';
            acc.MCS__c = 'Incomplete';
            memAccs.add(acc);

        }
        if (memaccs.size()> 0) {


            if (memAccs.size()> 100) {
                system.EnqueueJob(new Queueable_UpdateAccountsinbulk(memAccs, true, true));
                Message = 'Member accounts (greater than 100) have been added to the queue and will get updated once system resources are available. So Update will not reflect right away however will not be more than 45 mins';
            } else {
                UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
                database.saveresult[] srlist = database.update(memAccs, false);

                Integer errorcount = 0;
                for (database.saveresult sr: srlist) {
                    if (!sr.issuccess())
                        Errorcount++;
                }

                if (errorcount == 0)
                    Message = 'All active record/records have been processed for renewal action.';
                else if ((memAccs.size() - errorcount)> 0)
                    Message = string.valueof(errorcount) + ' out of ' + memAccs.size() + ' rec/recs failed.Rest processed for renewal action.Contact your system admin for failed rec/recs.';
                else
                    Message = 'Update on all active record/records not in Inrenewal-Incomplete stage failed.Contact your system admin for failed records.';

            }


        }
        Return Message;

    }
    /*webservice static string CloneMemAccount(Id memId) {
        Try {
            List<Contact> cons = new List<Contact>();
            Account acc = [SELECT ID, Name,recordtypeId,subscription_status__c,Account_Employer_Name__c,dependent_status__c FROM Account WHERE Id = : memId];
            Account accCopy = acc.clone(false,true);
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
            insert accCopy;
            List<Contact> con = [SELECT Id, LastName, AccountId FROM Contact WHERE AccountId = : acc.Id];
            for(Contact c : con)
            {
             Contact conCopy = c.clone(false,true);
             conCopy.AccountId = accCopy.Id;
             cons.add(conCopy);
            }
            UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
        insert cons;
        
        return accCopy.Id;
    

        } catch (exception e) {
            return 'Error';
        }


    }*/
}