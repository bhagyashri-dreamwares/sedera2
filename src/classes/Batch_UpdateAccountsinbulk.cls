public class Batch_UpdateAccountsinbulk  implements Database.batchable <sobject> {
    public Map <id,Account> AccsMap;
    public Boolean skipTriggerQB;
    public Boolean skipTriggerNonQB;
    public Set <Id> AccIds;

    public Batch_UpdateAccountsinbulk(Map <id, Account> AccsMap, Set <id> AccsIds, Boolean skipTriggerQB,Boolean skipTriggerNonQB) {
        this.AccIds = AccsIds;
        this.AccsMap= AccsMap;
        this.skipTriggerQB=skipTriggerQB;
        this.skipTriggerNonQB=skipTriggerNonQB;
      }

    public Database.QueryLocator start(Database.BatchableContext info) {
        return Database.getQueryLocator('Select id from Account where id in: AccIds');
    }

    public void execute(Database.BatchableContext info, List <Account> scope) {
        String ErrorIds = '';
        if (!test.isRunningTest() && skipTriggerQB){
          UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB= 1;
            }
        if (skipTriggerNonQB){
          UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 1;
            }

        List <Account> AccListUpdate = new List <Account> ();
        for (Account a: scope) {
            AccListUpdate.add(AccsMap.get(a.Id));
            ErrorIds = ErrorIds + a.id + '';
        }
        try {
            if (AccListUpdate.size()> 0)
                Update AccListUpdate;
            if(test.isrunningtest())
              integer i=1/0;
        }
        Catch(Exception e) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {
                'alakshay@sedera.com'
            };
            message.subject = 'Error Processing Records.';
            message.plainTextBody = ErrorIds + '\n\n' + e.getMessage();
            Messaging.SingleEmailMessage[] messages =
                new List <Messaging.SingleEmailMessage> {
                    message
                };
            if(!test.isrunningtest())
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
           
        }

    }

    public void finish(Database.BatchableContext info) {

    }

}