/**
*@Author       : Dreamwares
*@Created Date : 
*@Purpose      : test class for QBAccountSyncPushBatch.
*/
@isTest
public class QBAccountSyncPushTest {
    
    @testSetup
    static void setupTestData(){
        Quick_Book_App_Configuration__c setting = new Quick_Book_App_Configuration__c(
            Name = 'Quickbooks',
            Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
            Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/token',
            Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
            Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
            Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
            Is_Sandbox__c = true,
            Production_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com', 
            QB_Company_Id__c = '123145860463979',
            Redirect_URI__c = 'https://air--fsmsandbox--c.cs14.visual.force.com',
            Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
            Refresh_Token_Expiry__c = DateTime.now().addDays(1),
            Access_Token_Expiry__c  = DateTime.now().addHours(1),            
            Sandbox_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com'
        );
        insert setting;
        
        setting = new Quick_Book_App_Configuration__c(
            Name = 'Quickbooks2',
            Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
            Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
            Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/token',
            Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
            Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
            Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
            Is_Sandbox__c = true,
            Production_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com', 
            QB_Company_Id__c = '123145860463979',
            Redirect_URI__c = 'https://air--fsmsandbox--c.cs14.visual.force.com',
            Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
            Refresh_Token_Expiry__c = DateTime.now().addDays(1),
            Access_Token_Expiry__c  = DateTime.now().addHours(1),            
            Sandbox_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com'
        );
        insert setting;
        
        QB_Sync_Errors__c syncData = new QB_Sync_Errors__c( Name = 'Test Object');
        insert syncData;
        
        syncData = new QB_Sync_Errors__c( Name = 'Test Object2');
        insert syncData;
    }
    
    static Account createParentAccount(String accName,String billingEmail){
        
        
        Account recordAccount1 = new Account(
            Name = accName+'Parent',
            //QuickBooks_Customer_ID__c='80',
            QuickBooks_Customer_ID2__c='80',
            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
            ShippingStreet = '230 NE US Hwy 27',
            ShippingCity = 'Williston',
            ShippingState = 'Florida',
            ShippingCountry = 'United States',
            Website = 'gm.com',
            Invoice_Email_Address__c = billingEmail,
            Sync_to_QBO__c = true,
            QB_Sync_Token__c = '0',
            QB_Sync_Token2__c = '0',
            Phone = '7896541230',
            Fax = '(123) 487-7960',
            Description ='Bulk Records' );
        
        insert recordAccount1;
        return recordAccount1;
    }
    
    static void createChildAccount(Account acc){
        Account recordAccount1 = new Account(
            Name = acc.Name+'Child',
            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
            //QuickBooks_Customer_ID__c='80',
            QuickBooks_Customer_ID2__c='80',
            ShippingStreet = '230 NE US Hwy 27',
            ShippingCity = 'Williston',
            ShippingState = 'Florida',
            ShippingCountry = 'United States',
            Website = 'gm.com',
            Sync_to_QBO__c = true,
            QB_Sync_Token__c = '0',
            QB_Sync_Token2__c = '0',
            Phone = '7896541230',
            ParentId = acc.Id,
            Fax = '(123) 487-7960',
            Description ='Bulk Records' );
        
        insert recordAccount1;
    }
    
    static Account createAccount(String accName,String billingEmail){
        
        Account recordAccount1 = new Account(
            Name = accName,
            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
            //QuickBooks_Customer_ID__c='80',
            QuickBooks_Customer_ID2__c='80',
            ShippingStreet = '230 NE US Hwy 27',
            ShippingCity = 'Williston',
            ShippingState = 'Florida',
            ShippingCountry = 'United States',
            Website = 'gm.com',
            Invoice_Email_Address__c = billingEmail,
            Sync_to_QBO__c = true,
            QB_Sync_Token__c = '0',
            QB_Sync_Token2__c = '0',
            Phone = '7896541230',
            Fax = '(123) 487-7960',
            Description ='Bulk Records' );
        
        return recordAccount1;
    }
    
    static testmethod void testQBAccountPush() {
        Set<Id> accountIdSet = new Set<Id>();
        Test.setMock(HttpCalloutMock.class, new MockResponseForQBCustomerPull());
        Test.startTest();
        insert createAccount('Test Account ', 'abc@gmail.com');
        Test.stopTest();
        
        List<Account> listacc = [SELECT Id, NAME FROM ACCOUNT ];
        
        System.assertEquals( 1, listacc.size() );
        
        listacc[0].name = 'Test Account1';
        update listacc[0];
        System.assertEquals( 1, listacc.size() );
        
        QBAccountSyncPush qbAccountSyncPush = new QBAccountSyncPush('Quickbooks');
        QBBatchResponseObject qbBatchResponseObj = new QBBatchResponseObject();
        qbBatchResponseObj.bId = '123';
        qbBatchResponseObj.Customer = NULL;
        
        
        QBBatchFault qbBatchfault = new QBBatchFault();
        QBBatchFault.QBBatchError batchError = new QBBatchFault.QBBatchError();
        batchError.Message = 'Test Error';
        batchError.Detail = 'Test Error Detail';
        batchError.code = '1234';
        batchError.element = 'Test Element';
        qbBatchfault.Error.add(batchError);
        qbBatchResponseObj.Fault = qbBatchfault;
        
        List<QBBatchResponseObject> records = new List<QBBatchResponseObject>();
        records.add(qbBatchResponseObj);
        
        qbAccountSyncPush.updateRecords(records, System.now(), 'Quickbooks');
    }
    /*
    static testmethod void testQBAccountParentChild() {
        Set<Id> accountIdSet = new Set<Id>();
        Test.setMock(HttpCalloutMock.class, new MockResponseForQBAccountSyncPush());
        Test.startTest();
        Account pAccount = createParentAccount('Test Account ', 'abc@gmail.com');
        Test.stopTest();
        createChildAccount(pAccount);
    }  
    static testmethod void testQBAccountParentChild1() {
        Set<Id> accountIdSet = new Set<Id>();
        Test.setMock(HttpCalloutMock.class, new MockResponseForQBAccountSyncPushFault());
        Test.startTest();
        Account pAccount = createParentAccount('Test Account ', 'abc@gmail.com');
        Test.stopTest();
        createChildAccount(pAccount);
    } */
    static testmethod void testCustomerPull(){
        QBCustomerPull obj = new QBCustomerPull();
        obj.getObjectRecords(new QBQueryResponseWrapper());
        DateTime dt;
        obj.save(new List<Object>(),dt); 
        
        QBAccountSyncPushBatch obj2 = new QBAccountSyncPushBatch(new Set<ID>(), 'Quickbooks');
        obj2.getMapOfSyncToken([SELECT Id FROM ACCOUNT], 'Quickbooks');
    }
    
    static testmethod void testMethod1(){
        List<Account> accountList = [Select Id,Name,QuickBooks_Customer_ID2__c,recordTypeId,ShippingStreet,ShippingState,ShippingCity,
                                     ShippingCountry,Website,Invoice_Email_Address__c,Sync_to_QBO__c,QB_Sync_Token__c,QB_Sync_Token2__c,
                                     Phone,Fax,Description 
                                     FROM Account
                                    LIMIT 50000];
        QBAccountSyncPush accObj = new QBAccountSyncPush('Test');
        accObj.doPost(accountList, 'Quickbook1');
    }
    
    static testmethod void testMethod2(){
        List<Account> accountList = [Select Id,Name,QuickBooks_Customer_ID2__c,recordTypeId,ShippingStreet,ShippingState,ShippingCity,
                                     ShippingCountry,Website,Invoice_Email_Address__c,Sync_to_QBO__c,QB_Sync_Token__c,QB_Sync_Token2__c,
                                     Phone,Fax,Description 
                                     FROM Account
                                    LIMIT 50000];
        QBAccountSyncPush accObj = new QBAccountSyncPush('Test');
        accObj.updateRecords(accountList, System.now(),'Quickbboks');
    }   
    
    // AccountTriggerHandler
    static testmethod void testMethod3(){
        List<Account> accountList = [Select Id
                                     FROM Account
                                     LIMIT 50000];
        Map<Id,Account> oldMap = new Map<Id,Account>();
        Map<Id,Account> newMap = new Map<Id,Account>();
        Set<id> accIds = new Set<Id>();
        for(Account acc:accountList){
            accIds.add(acc.Id);
            oldMap.put(acc.Id,acc);
            newMap.put(acc.Id,acc);
        }
        AccountTriggerHandler accObj = new AccountTriggerHandler();
        accObj.getChildIds(accIds);
        accObj.updateQBHierarchy(oldMap,newMap);
    }  
    static testmethod void testMethod4(){
        List<QBWrappers.QBCustomer> accountList = new  List<QBWrappers.QBCustomer>();
        accountList.add(new QBWrappers.QBCustomer());
        //insert accountList;
        QBCustomerParser accObj = new QBCustomerParser();
        accObj.parseToObjectList(accountList, 'Quickbooks');
        accObj.parseToObjectList(accountList, 'Quickbooks2');
    }
    
    static testmethod void testMethod5(){
        List<QBWrappers.QBCustomer> accountList = new  List<QBWrappers.QBCustomer>();
        accountList.add(new QBWrappers.QBCustomer());
        QBCustomerParser accObj = new QBCustomerParser();
        accObj.parseToDTOList(accountList);
    }
    
    static testmethod void testMethod6(){
        Account account = createParentAccount('Test Account ', 'abc@gmail.com');
        
        Invoice__c invoice = new Invoice__c(Account__c = account.Id, Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30), Paid_Amount__c = 8000);
        insert invoice;
        
        List<Invoice__c> accountlist = new List<Invoice__c>();
        accountlist.add(invoice);
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                        price__c = 2000, Quickbooks_ID__c='28');
        insert product;
        
        List<Invoice_Line_Item__c> invoiceList = new  List<Invoice_Line_Item__c>();
        invoiceList.add(new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                 Quantity__c = 5, Product__c = product.Id, QBO_Product__c = product.Id));
        
        QBInvoiceParser accObj = new QBInvoiceParser();
        accObj.parseToDTOList(accountlist,'Quickbooks');
    }
    
    /*static testmethod void testMethod7(){
        
         QuickBook2_LineItems_Configuration__c quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='MCS Member Services',
                                                                                                                          Quickbook2_Product_Id__c='22',
                                                                                                                          Quickbook2_Product_Name__c='MCS Member Services',
                                                                                                                          Salesforce_Product_Id__c='01tq0000002i0wT');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (New)',
                                                                                     Quickbook2_Product_Id__c='20',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (New)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wJ');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Startup MCS Revenue',
                                                                                     Quickbook2_Product_Id__c='21',
                                                                                     Quickbook2_Product_Name__c='Startup MCS Revenue',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wO');
        INSERT quickBook2LineItemsConfiguration;
        
        Account account = new Account(Name = 'Test');
        insert account;
        
        Invoice__c invoice = new Invoice__c(Service_Period__c = 'test', Account__c = account.Id, Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30), Paid_Amount__c = 8000);
        insert invoice;
        
        List<Invoice__c> accountlist = new List<Invoice__c>();
        accountlist.add(invoice);
        
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                        price__c = 2000, Quickbooks_ID__c='28');
        insert product;
        
        List<Invoice_Line_Item__c> invoiceList = new  List<Invoice_Line_Item__c>();
        invoiceList.add(new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                 Quantity__c = 5, Product__c = product.Id, QBO_Product__c = product.Id));
        QBInvoiceParser accObj = new QBInvoiceParser();
        accObj.parseToDTOList(accountlist,'Quickbooks2');
        accObj.parseToDTOList(accountlist,'Quickbooks');
    }*/
    
      
      static testmethod void testMethod8(){
          QuickBook2_LineItems_Configuration__c quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='MCS Member Services',
                                                                                                                          Quickbook2_Product_Id__c='22',
                                                                                                                          Quickbook2_Product_Name__c='MCS Member Services',
                                                                                                                          Salesforce_Product_Id__c='01tq0000002i0wT');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (Existing)',
                                                                                     Quickbook2_Product_Id__c='19',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (Existing)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wE');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Standard MCS Revenue (New)',
                                                                                     Quickbook2_Product_Id__c='20',
                                                                                     Quickbook2_Product_Name__c='Standard MCS Revenue (New)',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wJ');
        INSERT quickBook2LineItemsConfiguration;
        
        quickBook2LineItemsConfiguration = new QuickBook2_LineItems_Configuration__c(Name='Startup MCS Revenue',
                                                                                     Quickbook2_Product_Id__c='21',
                                                                                     Quickbook2_Product_Name__c='Startup MCS Revenue',
                                                                                     Salesforce_Product_Id__c='01tq0000002i0wO');
        INSERT quickBook2LineItemsConfiguration;
        
        Account account = createParentAccount('Test Account ', 'abc@gmail.com');
        
        Invoice__c invoice = new Invoice__c(Service_Period__c = 'test', Account__c = account.Id, Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30), Paid_Amount__c = 8000);
        insert invoice;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        invoiceList.add(invoice);
        
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                        price__c = 2000, Quickbooks_ID__c='28');
        insert product;
        
        List<Invoice_Line_Item__c> invoiceList1 = new  List<Invoice_Line_Item__c>();
        invoiceList1.add(new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                 Quantity__c = 5, Product__c = product.Id, QBO_Product__c = product.Id));
       
        List<QBWrappers.QBLine> lineList = new List<QBWrappers.QBLine>();  
        QBWrappers.QBLine qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Standard MCS Revenue (Existing)';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Startup MCS Revenue';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Member Services';
        lineList.add(qbLineItem);
        qbLineItem = new QBWrappers.QBLine();
        qbLineItem.DetailType = 'SalesItemLineDetail';  
        qbLineItem.SalesItemLineDetail.ItemRef.name ='Standard MCS Revenue (New)';
        lineList.add(qbLineItem);  
          
        QBWrappers.QBInvoiceDTO qbInvoiceObj = new QBWrappers.QBInvoiceDTO();  
        qbInvoiceObj.Line = lineList;
        List<QBWrappers.QBInvoiceDTO> QBwrapperList = new  List<QBWrappers.QBInvoiceDTO>();
        QBwrapperList.add(qbInvoiceObj);
          
        QBInvoiceParser QBParser = new QBInvoiceParser();
          
        QBParser.parseToObjectList(QBwrapperList, 'Quickbooks2');
        QBParser.parseToObjectList(QBwrapperList, 'Quickbooks');
        //QBParser.parseToDTOList(invoiceList, 'Quickbooks2');
        QBParser.parseToDTOList(invoiceList, 'Quickbooks');  
          
    }
    
    static testmethod void testMethod9(){
        QBInvoiceParser accObj = new QBInvoiceParser();
        accObj.convertStringToTime('2017-10-11T01:19:15-07:00');
        accObj.convertStringToTime('2017-10-11T01:0');
        accObj.convertStringToTime(NULL);
    }
}