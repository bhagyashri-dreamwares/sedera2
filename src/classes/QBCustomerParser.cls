/*
 * @Purpose  : Parser for QB customer to SF account Mapping
 *
 */
 
public with sharing class QBCustomerParser implements IParser{
    
    public List<Object> parseToDTOList(List<Object> recordsList){
        return null;
    }
    
    public List<Object> parseToObjectList(List<Object> recordsList){
        return null;
    }
    
    public List<Object> parseToDTOList(List<Object> recordsList, String currentQuickBookOrg){
    
        List<QBWrappers.QBCustomer> customersList = new List<QBWrappers.QBCustomer>();
        if(recordsList != null && !recordsList.isEmpty()){
            
            // fetch modified fields 
            // create map
            // if in map then Map it
            
            for(Account accountRec : (List<Account>)recordsList){
            
                QBWrappers.QBCustomer customer = new QBWrappers.QBCustomer();
                if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks' ){
                    if(String.isNotBlank(accountRec.QuickBooks_Customer_ID__c)){
                        customer.Id = accountRec.QuickBooks_Customer_ID__c;
                    }
                    if(String.isNotBlank(accountRec.QB_Sync_Token__c)){
                        customer.SyncToken = accountRec.QB_Sync_Token__c;
                    }
                }
                if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks2' ){
                    if(String.isNotBlank(accountRec.QuickBooks_Customer_ID2__c)){
                        customer.Id = accountRec.QuickBooks_Customer_ID2__c;
                    }
                    if(String.isNotBlank(accountRec.QB_Sync_Token2__c)){
                        customer.SyncToken = accountRec.QB_Sync_Token2__c;
                    }
                }
                if(String.isNotBlank(accountRec.Description)){
                    customer.Notes = accountRec.Description;
                }else{
                    customer.Notes = '';
                }
                               
                // Title, GivenName, MiddleName, FamilyName, DisplayName
                customer.DisplayName = accountRec.Name;
                customer.CompanyName = accountRec.Name;
                
                
                // Address fields mapping
                if(String.isNotBlank(accountRec.ShippingState)){
                    customer.ShipAddr = new QBWrappers.Address(accountRec.ShippingStreet, 
                                                                accountRec.ShippingCity, 
                                                                accountRec.ShippingCountry, 
                                                                accountRec.ShippingState, 
                                                                accountRec.ShippingPostalCode);
                }
                else{
                    customer.ShipAddr = new QBWrappers.Address(accountRec.BillingStreet, 
                                                accountRec.BillingCity, 
                                                accountRec.BillingCountry, 
                                                accountRec.BillingState, 
                                                accountRec.BillingPostalCode);
                }
                customer.BillAddr = new QBWrappers.Address(accountRec.BillingStreet, 
                                                            accountRec.BillingCity, 
                                                            accountRec.BillingCountry, 
                                                            accountRec.BillingState, 
                                                            accountRec.BillingPostalCode);
                
                if(accountRec.Parent != NULL && (String.isNotBlank( accountRec.Parent.QuickBooks_Customer_ID__c ) || 
                                                 String.isNotBlank( accountRec.Parent.QuickBooks_Customer_ID2__c ))){
                   if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks' ){
                       customer.ParentRef = new QBWrappers.QBParentRef(accountRec.Parent.QuickBooks_Customer_ID__c); 
                    }
                    if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks2' ){
                        customer.ParentRef = new QBWrappers.QBParentRef(accountRec.Parent.QuickBooks_Customer_ID2__c); 
                    }
                    
                    customer.BillWithParent = false;
                    customer.Job = true;
                }

                // Phone fields Mapping 
                if(String.isNotBlank(accountRec.Phone)){
                
                    customer.PrimaryPhone = new QBWrappers.Phone(String.valueOf(accountRec.Phone));
                    customer.AlternatePhone = new QBWrappers.Phone(String.valueOf(accountRec.Phone));
                }else{
                
                    customer.PrimaryPhone = new QBWrappers.Phone();
                    customer.AlternatePhone = new QBWrappers.Phone();
                }
                
                if(String.isNotBlank(accountRec.Website)){
                    //containsIgnoreCase
                    accountRec.Website = accountRec.Website.trim().deleteWhitespace();
                    if(!accountRec.Website.containsIgnoreCase('https://') &&
                       !accountRec.Website.containsIgnoreCase('http://')){
                        accountRec.Website = 'http://'+accountRec.Website;
                    }
                    customer.WebAddr = new QBWrappers.WebAddr(accountRec.Website);
                }else{
                    customer.WebAddr = new QBWrappers.WebAddr();
                }
                
                if(String.isNotBlank(accountRec.Fax)){
                    customer.Fax = new QBWrappers.Phone(accountRec.Fax);
                }else{
                    customer.Fax = new QBWrappers.Phone();
                }
                
                customer.sparse = true;
                customersList.add(customer);
            }
        }
        return customersList; 
    }
    
    public List<Object> parseToObjectList(List<Object> recordsList, String currentQuickBookOrg){
        List<Account> accountsList = new List<Account>();
        if(recordsList != null && !recordsList.isEmpty()){
            for(QBWrappers.QBCustomer qbCustomer : (List<QBWrappers.QBCustomer>)recordsList){
                Account accountRec = new Account();
                
                if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks' ){
                    accountRec.QuickBooks_Customer_ID__c = qbCustomer.Id;
                    accountRec.QB_Sync_Token__c = qbCustomer.SyncToken;
                } 
                if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks2' ){
                    accountRec.QuickBooks_Customer_ID2__c = qbCustomer.Id;
                    accountRec.QB_Sync_Token2__c = qbCustomer.SyncToken;
                }
                
                
                
                // accountRec.QB_Sync_Token__c = qbCustomer.SyncToken;
                // accountRec.Last_Sync_Time__c = (System.now()).addSeconds(2);
                // accountRec.Description = qbCustomer.Notes;
                // accountRec.Name = qbCustomer.DisplayName;
                
                
                //accountRec.Account_balance__c = qbCustomer.Balance;
                /*if(qbCustomer.CompanyName != null && qbCustomer.CompanyName != ''){
                    accountRec.Name = qbCustomer.CompanyName;
                }
                else{
                    accountRec.Name = qbCustomer.FullyQualifiedName;
                }*/
                
                /* temparary commented
                accountRec.First_Name__c = qbCustomer.GivenName;
                accountRec.Last_Name__c = qbCustomer.FamilyName;
                accountRec.QBSyncToken__c = qbCustomer.SyncToken;
                accountRec.Suffix__c = qbCustomer.Suffix;
                accountRec.Company__c = qbCustomer.CompanyName;
                accountRec.Notes__c = qbCustomer.Notes;
                accountRec.Title__c = qbCustomer.Title ;
                accountRec.Middle_Name__c = qbCustomer.MiddleName;
                if(qbCustomer.Taxable!= null){
                    accountRec.QB_Is_Taxable__c = qbCustomer.Taxable; 
                }     
                accountRec.Job__c = qbCustomer.Job ;
                accountRec.Preferred_Delivery_Method__c = qbCustomer.PreferredDeliveryMethod;
                accountRec.Tax_Registration_No__c = qbCustomer.ResaleNum;

                if(qbCustomer.TDSEnabled != null){
                    accountRec.QBTDSEnabled__c = qbCustomer.TDSEnabled;
                }   
                accountRec.Active_In_QB__c = qbCustomer.Active;  */
               /* 
                if(qbCustomer.ShipAddr != null){
                    accountRec.ShippingStreet = qbCustomer.ShipAddr.Line1;
                    accountRec.ShippingCity = qbCustomer.ShipAddr.City; 
                    accountRec.ShippingCountry = qbCustomer.ShipAddr.Country; 
                    accountRec.ShippingState = qbCustomer.ShipAddr.CountrySubDivisionCode;
                    accountRec.ShippingPostalCode = qbCustomer.ShipAddr.PostalCode;
                }
                if(qbCustomer.BillAddr != null){
                    accountRec.BillingStreet = qbCustomer.BillAddr.Line1;
                    accountRec.BillingCity = qbCustomer.BillAddr.City;
                    accountRec.BillingCountry = qbCustomer.BillAddr.Country;
                    accountRec.BillingState = qbCustomer.BillAddr.CountrySubDivisionCode; 
                    accountRec.BillingPostalCode = qbCustomer.BillAddr.PostalCode;  
                }*/  
                /* temparary commented
                accountRec.Opening_balance__c = qbCustomer.Balance;
                if(qbCustomer.Mobile != null){
                    accountRec.Mobile__c = qbCustomer.Mobile.FreeFormNumber;
                }else{
                    accountRec.Mobile__c = '';  
                }
                */
                /*if(qbCustomer.AlternatePhone != null){
                    accountRec.Phone = qbCustomer.AlternatePhone.FreeFormNumber;
                }else{
                    accountRec.Phone = '';
                }   
                
                if(qbCustomer.Fax != null){    
                    accountRec.Fax = qbCustomer.Fax.FreeFormNumber;
                }else{
                    accountRec.Fax = '';    
                }       
                if(qbCustomer.PrimaryEmailAddr != null){
                    //accountRec.Billing_Email__c = qbCustomer.PrimaryEmailAddr.Address;
                }else{
                    //accountRec.Billing_Email__c = '';
                }
                
                if(qbCustomer.WebAddr!= null){
                    accountRec.Website = qbCustomer.WebAddr.URI;
                }else{
                    accountRec.Website = '';
                }*/ 
                accountsList.add(accountRec);
            }
        }
        return accountsList;
    }
}