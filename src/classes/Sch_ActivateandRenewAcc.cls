/*
 TestClass: Sch_ActivateandRenewAcc_Test

*/
public class Sch_ActivateandRenewAcc implements Schedulable {

    public void execute(SchedulableContext SC) {

        try {
            Map <id, Account> AccsToUpdateMap = new Map <id, Account> ();
            List <SendSMS> ReminderSMS = new List <SendSMS> ();
            list <Contact> ContactsToUpdate = new list <Contact> ();
            Account Act;


            /***** If EnrolmentDate=today,Activate Account ***/
            List <Account> AccListEnroll = [Select id, enrollment_date__c, RecordTypeId, parent_product__c, Account_employer_name__c, teladoc_direct__c, Dependent_status__c, member_discount_tier_manual__c, SecondMD__c, Subscription_Status__c, Iua_chosen__c, primary_Age__c, (Select id from Contacts) from Account where recordType.name = 'Member'
                and membership_stage__c = 'New Enrollment - Complete'
                and enrollment_date__c = today and subscription_status__c = 'Pending Start Date'
            ];

            Map <Id, Account> MemAccsMap;

            if (AccListEnroll.size()> 0) {

                for (Account acc: AccListEnroll)

                {
                    acc.health_care_sharing__c = true;
                    acc.Subscription_Status__c = 'Active';
                    acc.Teladoc_Direct__c = true;
                    acc.SecondMD__c = true;
                    acc.activation_date__c = date.Today();
                    if (acc.Parent_Product__c == 'Sedera Select') {
                        if (!test.isrunningtest())
                            acc.parentId = Label.Sedera_Select_Accounts_Id;
                        else
                            acc.parentId = [select id from Account where name = 'Sedera Select'
                                limit 1
                            ][0].Id;
                    } else if (acc.Parent_Product__c == 'Sedera Access') {
                        if (!test.isrunningtest())
                            acc.parentId = Label.Sedera_Access_Account_id;
                        else
                            acc.parentId = [select id from Account where name = 'Sedera Access'
                                limit 1
                            ][0].Id;

                    }

                    for (Contact con: acc.contacts) {
                        con.enrollment_date__c = acc.enrollment_date__c;
                        ContactsToUpdate.add(con);
                    }

                    AccsToUpdateMap.put(acc.Id, acc);
                }

                //Account_Helper_class.MCSproductcalculation(AccsToUpdateMap.values(),null);

            }

            /***** In Renewal Member *****/
            List <Account> accRenewalList1 = [select id, CreatedById from Account where recordType.name = 'Member'
                and(subscription_status__c = 'active'
                    OR(membership_stage__c = 'New Enrollment - Complete'
                        and enrollment_date__c = today and subscription_status__c = 'Pending Start Date')) and Account_employer_name__r.Open_Enrollment_Starts__c = today
            ];

            if (accRenewalList1.size()> 0) {
                for (Account acc: accRenewalList1) {

                    if (AccsToUpdateMap.containsKey(acc.Id)) {
                        Act = AccsToUpdateMap.get(acc.Id);
                    } else {
                        Act = acc;
                    }
                    Act.Membership_Stage__c = 'In Renewal - Incomplete';
                    Act.Primary_Member_Principles__c = 'Incomplete';
                    Act.Ancillary_Flow__c = 'Incomplete';
                    Act.mcs_summary_flow__c = 'Incomplete';
                    if (acc.createdbyId != system.label.Integration_User) {
                        Act.adding_dependents__c = 'Incomplete';
                    } else {
                        Act.adding_dependents__c = 'Not applicable';
                    }
                    AccsToUpdateMap.put(Act.Id, Act);
                    Act = null;

                }

            }

            /*** Reset Renewal ***/
            List <Account> accRenewalList2 = [select renewal_date__c, id, (select id, MCS_Product_Code__c, MCS_Product_amount__c, RecordTypeId, Subscription_Status__c, parent_product__c, Account_employer_name__c, teladoc_direct__c, Dependent_status__c, member_discount_tier_manual__c, SecondMD__c, Iua_chosen__c, primary_Age__c, Account_employer_name__r.Renewal_date__c from Accounts__r where recordType.name = 'Member'
                    and(subscription_status__c = 'active'
                        or subscription_status__c = 'pending cancellation')) from account where recordtype.name = 'Employer'
                and Renewal_date__c = today
            ];

            if (accRenewalList2.size()> 0) {
                List <Account> mcsCal = new list <Account> ();
                for (Account acc: accRenewalList2) {


                    if (!AccsToUpdateMap.containsKey(acc.Id)) {
                        Account emp = new Account(id = acc.id, renewal_date__c = acc.renewal_date__c.addYears(1));
                        AccsToUpdateMap.put(acc.id, emp);
                    } else {
                        AccsToUpdateMap.get(acc.Id).renewal_date__c = acc.renewal_date__c.addYears(1);
                    }


                    mcsCal = acc.accounts__r;
                    if (mcsCal.size()> 0) {

                        Account_Helper_class.MCSproductcalculation(mcscal, null);

                    }
                    for (Account memAcc: mcsCal) {

                        if (AccsToUpdateMap.containsKey(memAcc.Id)) {
                            AccsToUpdateMap.get(memAcc.Id).mcs_product_code__c = memAcc.mcs_product_code__c;
                            AccsToUpdateMap.get(memAcc.Id).mcs_product_amount__c = memAcc.mcs_product_amount__c;
                        } else {
                            AccsToUpdateMap.put(memAcc.id, memAcc);
                        }

                    }

                }

            }




            if (AccsToUpdateMap.size()> 0 && AccsToUpdateMap.size() <100 && !test.isrunningtest()) {
                UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
                //UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
                Update AccsToUpdateMap.values();
            } else if (test.isrunningtest() || AccsToUpdateMap.size()>= 100) {
                system.EnqueueJob(new Queueable_UpdateAccountsinbulk(AccsToUpdateMap.values(), true, false));
            }
            if (ContactsToUpdate.size()> 0) {
                UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
                Update ContactsToUpdate;
            }
            if (test.isRunningTest()) {
                integer i = 1 / 0;
            }
        } catch (exception e) {

            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {
                'alakshay@sedera.com'
            };
            message.subject = 'Error Processing Records in class Sch_ActivateandRenewAcc';
            message.plainTextBody = 'Please contact your system Administrator  '+e.getMessage();
            Messaging.SingleEmailMessage[] messages =
                new List <Messaging.SingleEmailMessage> {
                    message
                };
            if (!test.isrunningtest())
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

        }

    }
}