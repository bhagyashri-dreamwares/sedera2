/*
  covers Sch_SendRemindersForEnrol Apex Class
*/

@istest
public class Sch_SendRemindersForEnrol_Test {



    @testSetup
    public static void InsertData() {

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List <Account> PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Access', true);
        List <Pricing__c> PriceList = TestDataFactory.Create_Pricing(null, 2, true);
        PriceList[0].name = 'Default Old Select Pricing';
        Update PriceList[0];
        List <Pricing__c> AccessPricing = TestDataFactory.Create_Pricing(null, 1, false);
        AccessPricing[0].name = 'Default Access Pricing';
        AccessPricing[0].recordTypeId = Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        insert AccessPricing[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 0;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List <Account> EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, false);
        EmployerAccountList[0].default_product__c = 'sedera Access';
        insert EmployerAccountList;

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.Enrollment_Date__c = myTestdate;
        emp.Original_Enrollment_Date__c = date.today();
        emp.Open_Enrollment_Starts__c = date.today();
        emp.Renewal_date__c = date.today();
        emp.iua_chosen__c = 500;
        update emp;
        List <MEC_Product__c> Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];
        List <AccountMECAssociation__c> Mec_Assosciation_List = new List <AccountMECAssociation__c> ();


        for (integer j = 0; j <1; j++) {

            for (Integer i = 0; i <3; i++) {

                List <AccountMECAssociation__c> MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List <Account> MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(2, false);
        for (integer i = 0; i <MemberAccountList.size(); i++) {
            MemberAccountList[i].Account_Employer_name__C = emp.id;
            MemberAccountList[i].Enrollment_Date__c = date.today();
            if (Math.mod(i, 2) == 0)
                MemberAccountList[i].Primary_Phone_Number__c = '1234567890';

        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        insert MemberAccountList;

    }




    public static testmethod void test1() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() + 2;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'In Renewal - Incomplete';
            memberAccountList[i].Primary_Member_Principles__c = 'Incomplete';
            memberAccountList[i].Ancillary_Flow__c = 'Incomplete';
            memberAccountList[i].MCS__c = 'Incomplete';
            MemberAccountList[i].subscription_status__c = 'Active';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }
    
     public static testmethod void test2() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() + 1;
        emp.Open_Enrollment_starts__c = date.today() - 1;

        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'In Renewal - Incomplete';
            memberAccountList[i].Primary_Member_Principles__c = 'Incomplete';
            memberAccountList[i].Ancillary_Flow__c = 'Incomplete';
            memberAccountList[i].MCS__c = 'Incomplete';
            MemberAccountList[i].subscription_status__c = 'Active';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }

   public static testmethod void test3() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() ;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'In Renewal - Incomplete';
            memberAccountList[i].Primary_Member_Principles__c = 'Incomplete';
            memberAccountList[i].Ancillary_Flow__c = 'Incomplete';
            memberAccountList[i].MCS__c = 'Incomplete';
            MemberAccountList[i].subscription_status__c = 'Active';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }

 public static testmethod void test4() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() ;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'New Enrollment - Incomplete';
            memberAccountList[i].subscription_status__c = 'Application in process';
            memberAccountList[i].MCS_Summary_Flow__c = 'Incomplete';
            memberAccountList[i].enrollment_date__c= date.today();
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }



   public static testmethod void test5() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() ;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'New Enrollment - Incomplete';
            memberAccountList[i].subscription_status__c = 'Application in process';
            memberAccountList[i].MCS_Summary_Flow__c = 'Incomplete';
            memberAccountList[i].enrollment_date__c= date.today()+4;
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }
    
    public static testmethod void test6() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() ;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'New Enrollment - Incomplete';
            memberAccountList[i].subscription_status__c = 'Application in process';
            memberAccountList[i].MCS_Summary_Flow__c = 'Incomplete';
            memberAccountList[i].enrollment_date__c= date.today()+4;
            memberAccountList[i].Primary_Member_principles__c='Completed';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }
    
     public static testmethod void test7() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() ;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'New Enrollment - Incomplete';
            memberAccountList[i].subscription_status__c = 'Application in process';
            memberAccountList[i].MCS_Summary_Flow__c = 'Incomplete';
            memberAccountList[i].enrollment_date__c= date.today()+4;
            memberAccountList[i].Primary_Member_principles__c='Completed';
            memberAccountList[i].Principles_signed_on__c = date.today()-1;
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }
   public static testmethod void test8() {
      
        Account emp = [Select id from Account where recordType.name = 'Employer'
            limit 1
        ];
        emp.Open_Enrollment_ends__c = date.today() ;
        Update emp;

        List <Account> MemberAccountList = [select id from Account where recordType.name = 'Member'];

        for (integer i = 0; i <MemberAccountList.size(); i++) {
            memberAccountList[i].Membership_Stage__c = 'New Enrollment - Incomplete';
            memberAccountList[i].subscription_status__c = 'Application in process';
            memberAccountList[i].MCS_Summary_Flow__c = 'Incomplete';
            memberAccountList[i].enrollment_date__c= date.today();
            memberAccountList[i].Primary_Member_principles__c ='Completed';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAccountList;

        Test.starttest();
        HttpCalloutMock Mockclass = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass);
        system.schedule('Test', '0 0 8 * * ?', new Sch_SendRemindersForEnrol());
        Test.stoptest();


    }








}