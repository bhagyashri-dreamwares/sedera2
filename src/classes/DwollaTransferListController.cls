/*
 * @Purpose       : Fetch the Dwolla Transfer for the Invoice
 * @Created Date  : 26/12/2018
 */ 
public class DwollaTransferListController {
    
    public List<DwollaTransferWrapper> dwollaTransferWrapperList{get;set;}
    
    // Constructot for class
    public DwollaTransferListController(){
        
        String urlPageParameter = Apexpages.currentpage().getparameters().get('id');
                
        if (String.isNotBlank(urlPageParameter) && urlPageParameter.contains('-')){
            List<String> pageParamList = urlPageParameter.split('-');
            
            if(pageParamList != NULL && !pageParamList.isEmpty() && pageParamList.size() == 2 && String.isNotBlank(pageParamList[1])){
                dwollaTransferWrapperList = new List<DwollaTransferWrapper>();
            	fetchDwollaTransfer(pageParamList[1]);    
            }
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide a valid parameter in the form Account Id - Invoice Id.!');
            ApexPages.addMessage(errorMsg);
        }
    }
    
   /*
    * @Purpose: Fetch the Dwolla Transfer for the Invoice
    */ 
    public void fetchDwollaTransfer(String invoiceId){
        
        if(String.isNotBlank(invoiceId)){
            try{
                List<Dwolla_Transfer__c> dwollaTransferList = [SELECT Id,Name, Transfer_Processed__c, Transfer_Status__c, Amount__c, 
                                                                      Source_Dwolla_Funding_Source__r.Name, Destination_Dwolla_Funding_Source__r.Name
                                                               FROM Dwolla_Transfer__c
                                                               WHERE Invoice__c = :invoiceId]; 
                
                if(dwollaTransferList.size() > 0){
                    for(Dwolla_Transfer__c dwollaTransferObj :dwollaTransferList){
                        
                        DwollaTransferWrapper dwollaTransferWrapperObj = new DwollaTransferWrapper();
                        
                        dwollaTransferWrapperObj.dwollaTransferName = dwollaTransferObj.Name; 
                        if(dwollaTransferObj.Source_Dwolla_Funding_Source__c != null){
                        	dwollaTransferWrapperObj.sourceFundingSourceName = dwollaTransferObj.Source_Dwolla_Funding_Source__r.Name != null ? dwollaTransferObj.Source_Dwolla_Funding_Source__r.Name : '';    
                        }
                        if(dwollaTransferObj.Destination_Dwolla_Funding_Source__r != null){
                        	dwollaTransferWrapperObj.destinationFundingSourceName = dwollaTransferObj.Destination_Dwolla_Funding_Source__r.Name != null ? dwollaTransferObj.Destination_Dwolla_Funding_Source__r.Name : '';      
                        } 
                        dwollaTransferWrapperObj.transferAmount = dwollaTransferObj.Amount__c; 
                        dwollaTransferWrapperObj.transferExecutedDate = dwollaTransferObj.Transfer_Processed__c != null ? (dwollaTransferObj.Transfer_Processed__c).format('MM/dd/yy') : '';
                        dwollaTransferWrapperObj.status = dwollaTransferObj.Transfer_Status__c;
                        
                        dwollaTransferWrapperList.add(dwollaTransferWrapperObj);
                        
                    }    
                }
            }catch(Exception ex){
            	System.debug('Exception occured while fetching Dwolla Transfer is::::'+ex.getMessage()); 
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while fetching Dwolla Transfer is::::'+ex.getMessage());
                ApexPages.addMessage(errorMsg);
            }
        }        
    }
    
   /*
    * @Purpose: Wrapper to hold the Dwolla Transfer Data
    */ 
    public class DwollaTransferWrapper{
        public String dwollaTransferName{get;set;}
        public String sourceFundingSourceName{get;set;}
        public String destinationFundingSourceName{get;set;}
        public Decimal transferAmount{get;set;}
        public String transferExecutedDate{get;set;}
        public String status{get;set;}
    }
}