@isTest
public class ProProfs_TestClass{

 public static testmethod void test(){
      Test.startTest();
      
      TestDataFactory.Create_Account_Of_Affiliate_type(1,true);
      
      Account acc=[Select id from Account limit 1];
      acc.Affiliate_Status__c='Approved';
      UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;

      
      Update acc;
      Test.stopTest();
      
      Contact con=[select id from contact limit 1];
      
      RestRequest req = new RestRequest(); 
      req.httpMethod = 'GET';
      req.addParameter('id',con.id);
      req.addParameter('completed','100');
            
      RestContext.request = req;
      
      ProProfs_TrainingNotification.doGet();
   }

}