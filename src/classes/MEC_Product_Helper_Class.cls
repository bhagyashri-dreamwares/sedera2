/*

Test class : MEC_Product_Trigger_Test 

*/
public class MEC_Product_Helper_Class {
  

    //Calculates and Updates Member_Discount_Tier_Manual__c on all members on change of discount Tier at MEC level.
    // Also calculates and Updates MCS prodcut code and MCS product amount to incorporate change Member_Discount_Tier_Manual__c 

    public static void CalandUpdateMemDisTier(List <MEC_Product__c> mecList, Map <id, MEC_Product__c> mecOldMap) {

        list <id> MecIds = new List <Id> ();
        Set <id> EmpIds = new Set <Id> ();

        for (MEC_Product__c MEC: mecList) {
            if (MEC.Discount_Tier__c != mecOldMap.get(MEC.id).Discount_Tier__c) {
                MecIds.add(MEC.id);
            }
        }

        if (MecIds.Size()> 0) {
            //Pulling all Employers if their default MEC's discount tier is changed
            // AccountEmployer, MECAssosciation(junction) and MEC product's discount tier is changed
            for (AccountMECAssociation__c AMec: [select id, Employer_Account__r.Id from AccountMECAssociation__c where Default_MEC_Product__c = true and MEC_Product__c in: MecIds]) {
                if (AMec.Employer_Account__r != null)
                    EmpIds.add(AMec.Employer_Account__r.Id);
            }


            if (EmpIds.size()> 0) {
                list <Account> MembersToUpdate = new list <Account> ();
                //pulling member Accs
                for (Account EmpAcc: [Select id, (Select MEC_Product__r.Name, MEC_Product__r.Id, MEC_Product__r.Discount_Tier__c from AccountsMECsAssociation__r where Default_MEC_Product__c = true limit 1), (Select id, parent_product__c, Account_employer_name__c, subscription_status__c, teladoc_direct__c, RecordTypeId, Dependent_status__c, member_discount_tier_manual__c, SecondMD__c, Iua_chosen__c, primary_Age__c from Accounts__r) from Account where id in: Empids]) {

                    for (Account memAcc: EmpAcc.Accounts__r) {
                        memAcc.Member_Discount_Tier_Manual__c = EmpAcc.AccountsMECsAssociation__r[0].MEC_Product__r.Discount_Tier__c;
                        MembersToUpdate.add(memAcc);
                    }
                }

                if (MembersToUpdate.size()> 0) {
                   //calculation new Product code and calculation
                    Account_Helper_class.MCSproductcalculation(MembersToUpdate, null);
                }

                if (MembersToUpdate.size()> 100 || test.isRunningTest()) {
                    system.debug('Triggering Queueable');
                    system.EnqueueJob(new Queueable_UpdateAccountsinbulk(MembersToUpdate, true, true));
                } else if (MembersToUpdate.size()> 0) {
                    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
                    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
                    system.debug(MembersToUpdate);
                    Update MembersToUpdate;
                }

            }
        }

    }

}