@Istest
public class Schedule_Send_Affiliates_Report_Test {


    public static testmethod void testReports() {
        test.startTest();
        Id EmployerRecId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Id ReferralAffiliateRecid = Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Referral Affiliate').getRecordTypeId();
        Id StrategicAffiliateRecid = Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Strategic Affiliate').getRecordTypeId();
        Id LeadGeneratorAffiliateRecid = Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Lead Generator').getRecordTypeId();
        Id InternalSalespersonRecid = Schema.SObjectType.Affiliate__c.getRecordTypeInfosByName().get('Internal Salesperson').getRecordTypeId();

        //TestDataFactory.Create_Pricing(null,1,true);
        
        Account account_Obj = new Account(BillingPostalCode = 'z', BillingStreet = 'z', billingcity = 'z', billingstate = 'z', billingcountry = 'z', Account_MEC_Administrator__c = 'Allied', Sedera_Rate_Structure__c = 'standard', Name = 'Name691', RecordTypeId = EmployerRecId,  Enrollment_Date__c = Date.today(), Dependent_Status__c = 'EO', Health_Care_Sharing__c = false, HSA__c = false, C_MEC__c = false, P_MEC__c = false, Cancelled__c = false, Member_Benefits__c = false, SecondMD__c = false, Take_Shape_for_Life__c = false, Tobacco_Use__c = false, Resend_Coverdell_Data__c = false, Good_Shepherd_Health__c = false, DPC__c = false, Teladoc_Direct__c = false, Contract_Status__c = 'Executed', MS_Premier__c = false, MS_Plus__c = false, Executed_Services_Agreement__c = false, COBRA__c = false, New_DPC__c = false, Update_Record__c = false, MS_Select__c = false, Liberty_Rx__c = false, IUA500__c = false, Adobe_Signup__c = false, IUA_1000__c = false, Invoice_not_paid__c = false, PMA__c = false);
        Insert account_Obj;
        Affiliate__c affiliate_Obj = new Affiliate__c(Name = 'Name880', recordtypeid = ReferralAffiliateRecid);
        Insert affiliate_Obj;
        AffiliateAccountAssociation__c affiliateaccountassociation_Obj = new AffiliateAccountAssociation__c(Employer_Account__c = account_Obj.id, Affiliate__c = affiliate_Obj.id);
        Insert affiliateaccountassociation_Obj;


        Affiliate__c affiliate_Obj2 = new Affiliate__c(Name = 'Name880', recordtypeid = StrategicAffiliateRecid);
        Insert affiliate_Obj2;
        AffiliateAccountAssociation__c affiliateaccountassociation_Obj2 = new AffiliateAccountAssociation__c(Employer_Account__c = account_Obj.id, Affiliate__c = affiliate_Obj2.id);
        Insert affiliateaccountassociation_Obj2;


        Affiliate__c affiliate_Obj3 = new Affiliate__c(Name = 'Name880', recordtypeid = LeadGeneratorAffiliateRecid);
        Insert affiliate_Obj3;
        AffiliateAccountAssociation__c affiliateaccountassociation_Obj3 = new AffiliateAccountAssociation__c(Employer_Account__c = account_Obj.id, Affiliate__c = affiliate_Obj3.id);
        Insert affiliateaccountassociation_Obj3;



        Affiliate__c affiliate_Obj4 = new Affiliate__c(Name = 'Name880', recordtypeid = InternalSalespersonRecid);
        Insert affiliate_Obj4;
        AffiliateAccountAssociation__c affiliateaccountassociation_Obj4 = new AffiliateAccountAssociation__c(Employer_Account__c = account_Obj.id, Affiliate__c = affiliate_Obj4.id);
        Insert affiliateaccountassociation_Obj4;

        system.schedule('test', '0 0 0 3 9 ? 2022', new Schedule_Send_Affiliates_Report());

        test.stoptest();

    }




}