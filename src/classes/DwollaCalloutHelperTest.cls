/* 
* @purpose : Test class to test DwollaCalloutHelper.
*/
@isTest
public class DwollaCalloutHelperTest {
    /* 
	* @purpose : create Test Data.
	*/
    @TestSetup
    public static void setup(){
        Id recordTypeIdAccountEmp = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account accountRecord1 = new Account();
        accountRecord1.Name='Test Account';
        accountRecord1.Create_Dwolla_Customer__c=true;
        accountRecord1.Invoice_Email_Address__c='test@test.com';
        accountRecord1.Dwolla_ID__c='12345';
        accountRecord1.Enrollment_Date__c=system.today();
        accountRecord1.Member_Discount_Tier_Manual__c='T2';
        accountRecord1.Member_MEC_Product__c='test';
        accountRecord1.recordtypeid = recordTypeIdAccountEmp;
        
        Account accountRecord2 = new Account();
        accountRecord2.Name='Test Account';
        accountRecord2.Create_Dwolla_Customer__c=true;
        accountRecord2.Invoice_Email_Address__c='test@test.com';
        accountRecord2.Dwolla_ID__c='123450';
        accountRecord2.Enrollment_Date__c=system.today();
        accountRecord2.Member_Discount_Tier_Manual__c='T2';
        accountRecord2.Member_MEC_Product__c='test';
        accountRecord2.recordtypeid = recordTypeIdAccountEmp;
        
        insert accountRecord1;
        insert accountRecord2;
        
        //custom setting 
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today()+6;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj; 
    }
    
    /* 
	* @purpose : Positive Test Method for createCustomer method .
	*/
    public static testmethod void createCustomerTest(){
        
        string retunString;
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        Account account = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                           Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                           Member_MEC_Product__c
                           FROM Account LIMIT 1];
        Test.startTest();
        DwollaCalloutHelper classObj = new DwollaCalloutHelper();
        retunString = classObj.createCustomer(account);
        Test.stopTest();
        system.assertEquals('', retunString);        
    }
    
    /* 
	* @purpose : Negative Test Method for createCustomer method .
	*/    
    public static testmethod void createCustomerNegativeTest(){
        
        string retunString;
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(0));
        
        Account account = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                           Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                           Member_MEC_Product__c
                           FROM Account LIMIT 1];
        Test.startTest();
        DwollaCalloutHelper classObj = new DwollaCalloutHelper();
        retunString = classObj.createCustomer(account);
        Test.stopTest();
        system.assertEquals(' test set header', retunString);
    }
    
    /* 
	* @purpose : Test Method for saveCustomer method .
	*/     
    public static testmethod void saveCustomerTest(){
        List<Account> accountListUpdate =new List<Account>();
        List<Account> accountList = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                                     Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                                     Member_MEC_Product__c
                                     FROM Account LIMIT 2];
        system.debug('accountList'+accountList);
        for(Account account : accountList){
            account.Name='';
            accountListUpdate.add(account);
        }
        Test.startTest();
        DwollaCalloutHelper.saveCustomer(accountListUpdate);
        Test.stopTest();
    }
}