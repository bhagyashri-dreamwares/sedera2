@isTest
public class plaidWebserviceMock{
        
        public HTTPResponse respond(HTTPRequest req){        
            HttpResponse response;
            
            if (req.getEndpoint().endsWith('processor_token/create')) {         
            
                response.setStatusCode(200);
                response.setBody('{ "processor_token": "processor-sandbox-7451e68e-0005-48bc-8b5b-fdb196122d29", "request_id": "IId5rIB1g9fr1ad" }');   
                    
            }
            return response;
        }
    }