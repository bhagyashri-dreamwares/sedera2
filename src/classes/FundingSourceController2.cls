/**
*@Purpose: Controller for Funding Source page
*/
public class FundingSourceController2{
    
    public PlaidAPIConfiguration__c config{get;set;}
    public DwollaAPIConfiguration__c dwollaConfig{get;set;}
    public String dataEnvironment{get;set;}
    public String customerId{get;set;}
    public string accountId{get;set;}
    public string firstName{get;set;}
    public string lastName{get;set;}
    public string accessToken{get;set;}
    public string email{get;set;}
    public string plaidAccountId{get; set;}    
    public string tempPublicToken{get; set;}
    public string accountName{get;set;}
    public string authType{get{
        if(String.isBlank(authType)){
            authType = '';
        }
        return authType;
    }set;}
    public Boolean isAccessTokenUpdated;
    public List<Dwolla_Error_Response__c> errorList;
    public List<Log__c> logList;
    
    public FundingSourceController2(ApexPages.StandardController controller){
        
        errorList = new List<Dwolla_Error_Response__c>();
        isAccessTokenUpdated = false;
        config = PlaidAPIConfiguration__c.getOrgDefaults();
        dwollaConfig = DwollaAPIConfiguration__c.getOrgDefaults();
        dataEnvironment = config.Is_Sandbox__c ? 'sandbox': 'production';
        logList = new List<Log__c>();
        customerId = ApexPages.currentPage().getParameters().get('customerId');
        assignDatasheetFields(customerId);
    }
    
    public PageReference getAccessToken(){
        
        PageReference pageRef;
        String publicToken = String.isNotBlank(tempPublicToken) ? 
                             tempPublicToken :
                             ApexPages.currentPage().getParameters().get('public_token');
        String plaidAccountId = ApexPages.currentPage().getParameters().get('account_id');
        System.debug('plaidAccountId  :::::::::'+plaidAccountId);
        Boolean isError = false;
        String nextPageUrl = '';
        Dwolla_Funding_Source__c fundingSourceRec;
        
        if(String.isNotBlank(publicToken) && config != null && String.isNotBlank(customerId) && String.isNotBlank(accountId)){
            
            //Step 2: exchange access token
            Map<String,String> endPointParameterMap = new Map<String,String>();
            Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json'};
            
            JSONGenerator gen = FundingSourceHandler.getCustomerJson(config, publicToken);
            
            String endPoint = config.Endpoint__c+'/item/public_token/exchange';
            
            try{
                HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
                
                if(res.getStatusCode() == 200){
                   
                   OAuth2TokenResponse  authTokenResponse = (OAuth2TokenResponse)JSON.deserialize(res.getBody(), OAuth2TokenResponse.class);
                   accessToken = authTokenResponse.access_token;
                   System.debug('accessToken :'+accessToken);
                   //FundingSourceHandler.updateAccount(new Account(Id = accountId, Plaid_Access_Token__c = accessToken));
                   
                   if(String.isBlank(authType)){
                       System.debug('No authType');
                       return createDwollaFundingSource();
                   }
                   System.debug('with authType');                   
                   tempPublicToken = FundingSourceHandler.getPublicToken(accessToken, config, errorList, logList);
                }else{                
                    
                    nextPageUrl = 'Exception :'+res.getBody()+'\n Please contact to Admin!!!';
                    isError = true;
                }
                
            }catch(Exception exp){
                System.debug('Exception :::'+exp.getMessage()+'\n StackTraceString : '+exp.getStackTraceString());
                
                nextPageUrl = 'Something went wrong. Please contact to Admin!!!';
                isError = true;
                logList.add(LogUtil.createLog('FundingSourceController.getAccessToken', exp, 'publicToken: '+publicToken+
                                              '\n\n Config: '+config+'\n\n CustomerId: '+customerId+' \n\n publicToken: '+publicToken+
                                              '\n\n accountId: '+accountId, '', 'Error'));
            }
        }
        //Step 8: update access token
        if(isAccessTokenUpdated){                                   
           FundingSourceHandler.updateDwollaCustomSetting(dwollaConfig, logList);
        }  
        System.debug('Final errorList :::'+errorList);
       
       
        //Step 9: store API exception
        if(!errorList.isEmpty()){
           DwollaErrorResponseHandler.saveDwollaErrorResponse(errorList);
        }
        
        //Step 10: store Logs
        if(!logList.isEmpty()){
           LogUtil.saveLogs(logList);
        }
        
        if(!isError){
            
            
            if(String.isNotBlank(authType) && authType.equalsIgnoreCase('SameDayAuth')){
              
               pageRef = null;
            }else{
                String fundingSourecId = fundingSourceRec.Id; 
                pageRef = new PageReference(Label.Site_Url+'DwollaConfirmation?id='+accountId+'&customerId='+customerId+'&isError='+isError+'&fundingSourecId='+fundingSourecId);
                pageRef.setRedirect(true);
            }
        }else{            
            pageRef = new PageReference(Label.Site_Url+'DwollaConfirmation?id='+accountId+'&customerId='+customerId+'&isError='+isError+'&message='+nextPageUrl);
            pageRef.setRedirect(true);
        } 
        System.debug('pageRef :::::'+pageRef);
        return pageRef;
    }
    
    private void assignDatasheetFields(String customerId){
        
        try{
            List<Dwolla_Datasheet__c> dataSheetList = [SELECT Id, First_Name__c, Last_Name__c, Email_Address__c
                                                       FROM Dwolla_Datasheet__c
                                                       WHERE Dwolla_ID__c = :customerId];
            
            if(!dataSheetList.isEmpty()){
                firstName = dataSheetList[0].First_Name__c;
                lastName = dataSheetList[0].Last_Name__c;
                email = dataSheetList[0].Email_Address__c;
            }
            
        }catch(Exception exp){
            
            logList.add(LogUtil.createLog('FundingSourceController.getDatasheetRecord', exp, '\n CustomerId: '+customerId, '', 'Error'));
        }
    }
    
    
    public PageReference assignDwollaFundingSource(){
        return createDwollaFundingSource();
    }
    
    
    public PageReference createDwollaFundingSource(){
        PageReference pageRef;
        Boolean isError = false;
        Dwolla_Funding_Source__c fundingSourceRec;
        String nextPageUrl = '';
         //Step 3: Get account id from Plaid
            
            System.debug('accessToken::::::::'+accessToken);
            String plaidAccountId = String.isNotBlank(plaidAccountId) ?
                                    plaidAccountId :
                                    FundingSourceHandler.getPlaidAccountId(accessToken, config, errorList, 
                                                                       new Account(Id = accountId, Plaid_Access_Token__c = accessToken));
            System.debug('plaidAccountId ::::::::'+plaidAccountId );
            if(String.isNotBlank(plaidAccountId) && !plaidAccountId.containsIgnoreCase('PRODUCT_NOT_READY')){
            
               //Step 4: get Processor token
               String processorToken = FundingSourceHandler.getProcessorToken(accessToken, 
                                                                              plaidAccountId, config, errorList, logList);
               if(String.isNotBlank(processorToken)){
                   
                   //Step 5: create funding Source
                   String fundingSourceId = assignFundingSource(processorToken, errorList, accountName, logList);
                   
                   if(String.isNotBlank(fundingSourceId)){
                       
                       //Step 6: get funding Source details
                       PlaidDTO.FundingSourceDetails fundingSourceDetails = FundingSourceHandler.getFundingSource(fundingSourceId, 
                                                                                             dwollaConfig, errorList, logList);
                       //Step 7: store funding Source info in salesforce
                       if(fundingSourceDetails != null){
                           
                           Response response = FundingSourceHandler.saveDwollaFundingSource(fundingSourceDetails, accountId, logList);  
                           
                           if(!response.Success){
                               
                               nextPageUrl = 'Unable to save Dwolla Funding Source records due to '+response.message+'. \n Please contact to Admin!!!';
                               isError = true;
                           }else{
                               fundingSourceRec = (Dwolla_Funding_Source__c)response.Data;
                           } 
                       }else{                                
                           
                           nextPageUrl = 'Unable to fetch Funding Source details. Please contact to Admin!!!';
                           isError = true;
                       }                           
                       
                   }else{                               
                       
                       nextPageUrl = 'Unable to create Funding Source. Please contact to Admin!!!';
                       isError = true;
                   }
               }else{                           
                   
                   nextPageUrl = 'Unable to fetch Processor token from Plaid. Please contact to Admin!!!';
                   isError = true;
               }
           }else if(plaidAccountId.containsIgnoreCase('PRODUCT_NOT_READY')){
               
               FundingSourceHandler.updateAccount(new Account(Id = accountId, Plaid_Access_Token__c = accessToken));
               nextPageUrl = 'Product is Not ready yet. We will update Account when it will be Verified.';
               isError = true;
           }else{                       
               
               nextPageUrl = 'There is no Account from Plaid. Please contact to Admin!!!';
               isError = true;
           } 
           
           if(!isError){
            
                String fundingSourecId = fundingSourceRec.Id; 
                pageRef = new PageReference(Label.Site_Url+'DwollaConfirmation?id='+accountId+'&customerId='+customerId+'&isError='+isError+'&fundingSourecId='+fundingSourecId);
                
            }else{          
                pageRef = new PageReference(Label.Site_Url+'DwollaConfirmation?id='+accountId+'&customerId='+customerId+'&isError='+isError+'&message='+nextPageUrl);
            } 
            
            pageRef.setRedirect(true);
            return pageRef;  
    }
    
    
    
    /*
    *@Purpose : Assign funding source to current Dwolla Customer
    */
    public String assignFundingSource(String plaidProcessorToken, List<Dwolla_Error_Response__c> errorList, String accountName, 
                                      List<Log__c> logList){
        
        Account account = new Account(Id = accountId);
        DwollaCustomerBatch customerBatch = new DwollaCustomerBatch(new List<Account>{account});
        
        if(dwollaConfig.Access_Token_Expiry__c == null || 
           dwollaConfig.Access_Token_Expiry__c <= DateTime.now()){
        
           dwollaConfig = customerBatch.getLiveToken(dwollaConfig);
           if(dwollaConfig != null){                      
               isAccessTokenUpdated = true;
           }
        }
        
        if(dwollaConfig != null){            
            return FundingSourceHandler.createDwollaFundingSource(plaidProcessorToken, dwollaConfig.Access_Token__c, '', '', '',
                                                                  dwollaConfig, customerId, accountName+'- Customer',
                                                                  errorList, logList);   
        }
        return '';
    }  
}