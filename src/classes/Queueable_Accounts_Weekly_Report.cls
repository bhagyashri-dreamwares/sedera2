public class Queueable_Accounts_Weekly_Report implements Queueable {

public Set <id> accIds;
 
    public Queueable_Accounts_Weekly_Report(Set <id> accIds){
      this.accIds=accIds; 
    }
    public void execute(QueueableContext context) {
       database.executeBatch(new Batch_Accounts_Weekly_Report(accIds),1);
    }
    
    
}