/*
* @Purpose         : FundingSourceController Test class
* @author          : Navin
* @since           : 28 Sep 2018
* @date            : 28 Sep 2018 - Navin: created initial version
* @see      
*/

@isTest
private class FundingSourceControllerTest {
    
    // Create testsetup records 
    @TestSetup
    private static void createTestRecord(){
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
        // Create Account record
        Id recoredTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account accountrecord = new Account(Name = 'testAccound', RecordTypeId = recoredTyId,
                                            Dwolla_ID__c='74d9b82a-2a54-4160-9f86-c718b8155b34',
                                            PlaidSameDayAccessToken__c = 'access-sandbox-581a5548-fe0e-43cc-930a-2309a420d948',
                                            Invoice_Email_Address__c = 'test@test.com');
        
        Insert accountrecord;
        
        
        
        PlaidAPIConfiguration__c plaidApiRecord = new PlaidAPIConfiguration__c(Client_Key__c = '5b6c8018a06f890011076a0a',
                                                                               Endpoint__c = 'https://sandbox.plaid.com', 
                                                                               Is_Sandbox__c=true,
                                                                               Plaid_Link__c='https://cs21.salesforce.com/servlet/servlet.ExternalRedirect?url=https%3A%2F%2Fcdn.plaid.com%2Flink%2Fv2%2Fstable%2Flink-initialize.js',
                                                                               Public_Key__c='694fb48cf78d39ec9ae2069e4379eb',
                                                                               Secret_Key__c='ca1a06681cecdd1c65af35b9da79eb',
                                                                               Valid_Bank_Account_Type__c='');
        insert plaidApiRecord;
        
        DwollaAPIConfiguration__c dwollaApiRecord = new DwollaAPIConfiguration__c(Access_Token__c = '5DeH2odJRoqJA21i6GH8BHeS0cEOJR0JaMgG3d32Aa6xeZ5MfV',
                                                                                  Access_Token_Expiry__c = system.today(), 
                                                                                  Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token',
                                                                                  Client_Key__c = 'UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp',
                                                                                  Is_Sandbox__c=true,
                                                                                  Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize',
                                                                                  Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com',
                                                                                  Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150');
        insert dwollaApiRecord;
        
        //insert custom setting record
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'test@test.com';
        config.Site_Url__c = 'Test@test.com';
        
        insert config;
        
        // insert Paubox_Message_Template
        Paubox_Message_Template__c template = new Paubox_Message_Template__c();
        template.Name = 'EONS Shared';
        template.Paubox_Message_Type__c = 'EONS Shared';
        template.Template_Body__c = 'Test';
        template.Template_Subject__c = 'EONS Shared Test';
        
        insert template;
        
        
        
    }
    
    // Positive test case
    @isTest
    private static void positiveTest() {
        
        // insert Paubox_Message_Template
        Paubox_Message_Template__c template2 = new Paubox_Message_Template__c();
        template2.Name = 'Invoice';
        template2.Paubox_Message_Type__c = 'Invoice';
        template2.Template_Body__c = 'Test';
        template2.Template_Subject__c = 'Invoice Test';
        
        insert template2;
        
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c
                                 FROM Account
                                 LIMIT 1];
        
        Dwolla_Datasheet__c dwollaSheet = new Dwolla_Datasheet__c();
        dwollaSheet.Account__c = accountRecord.Id;
        dwollasheet.SSN__c = '111-11-1111';
        dwollasheet.EIN__c= '11-1111111';
        dwollasheet.Controller_SSN__c = '222-22-2222';
        dwollasheet.Plaid_Email_Address__c = 'test@test.com';
        insert dwollaSheet;
        
        PageReference fundingSourcePage = Page.FundingSource; // Add your VF page Name here        
        Test.setCurrentPage(fundingSourcePage);
        ApexPages.currentPage().getParameters().put('customerId',accountRecord.Dwolla_ID__c);
        ApexPages.currentPage().getParameters().put('id', accountRecord.Id);
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerTestMock());
        Test.startTest(); 
        FundingSourceControllerTestMock.flow = 0;
        processFunctionality();
        
        PlaidDTO.AccountInfo plaidAccount = new PlaidDTO.AccountInfo();
        plaidAccount.account_id = '';
        plaidAccount.subtype = '';
        
        PlaidDTO.PlaidAccountDetails plaidAccountDetails = new PlaidDTO.PlaidAccountDetails();
        plaidAccountDetails.accounts.add(plaidAccount);
        
        PlaidDTO.FundingSourceDetails plaidFunding = new PlaidDTO.FundingSourceDetails();
        plaidFunding.bankAccountType = '';
        plaidFunding.bankName = '';
        plaidFunding.fingerprint = '';
        plaidFunding.id = '';
        plaidFunding.name = '';
        plaidFunding.status = '';
        
        PlaidDTO.ProcessorTokenDetails pliadProcess = new PlaidDTO.ProcessorTokenDetails();
        pliadProcess.processor_token = '';
        
        
        FundingSourceHandler.sendEmailToRecipient('invoice Message', new Contact(), 'Invoice', 'test@test.com');
        ApexPages.StandardController standardController = new ApexPages.StandardController(accountRecord);
        FundingSourceController fundingSourceController = new FundingSourceController(standardController);
        fundingSourceController.accessToken = 'access-sandbox-1486021f-fbb7-4789-8b5b-fd8daa3da4c7';
        fundingSourceController.assignDwollaFundingSource();
        fundingSourceController.accountId = accountRecord.Id;
        fundingSourceController.authType = 'SameDayAuth';
        fundingSourceController.getAccessToken();
        fundingSourceController.customerId = accountRecord.Dwolla_ID__c;
        fundingSourceController.authType = 'Auth';
        fundingSourceController.getAccessToken();
        
        
        Test.stopTest();
        
        
    }
    
    // Negative test case
    @isTest
    private static void negativeTest() {
        
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c
                                 FROM Account
                                 LIMIT 1];    
        Dwolla_Datasheet__c dwollaSheet = new Dwolla_Datasheet__c();
        dwollaSheet.Account__c = accountRecord.Id;
        dwollasheet.SSN__c = '111-11-1111';
        dwollasheet.EIN__c= '11-1111111';
        dwollasheet.Controller_SSN__c = '222-22-2222';
        dwollasheet.Plaid_Email_Address__c = 'test@test.com';
        insert dwollaSheet;
    
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerNegTestMock());
        Test.startTest(); 
        processFunctionality();
        FundingSourceHandler.createDwollaFundingSource('', '', '', '', '', new DwollaAPIConfiguration__c(), '', '', new List<Dwolla_Error_Response__c>(), new List<Log__c>());
        FundingSourceHandler.saveDwollaFundingSource(new PlaidDTO.FundingSourceDetails(), '', new List<Log__c>(), 'access-sandbox-581a5548-fe0e-43cc-930a-2309a420d948','ExAoBKgBRahJbraZLrmKiNe9o4PJkrCXqXKwK');
        FundingSourceHandler.getFundingSource('', new DwollaAPIConfiguration__c(), new List<Dwolla_Error_Response__c>(), new List<Log__c>());
        FundingSourceControllerNegTestMock mock = new FundingSourceControllerNegTestMock();
        HTTPRequest req = new HTTPRequest();
        req.setEndpoint('/create');
        mock.respond(req);
        
        
        req.setEndpoint('https://sandbox.dwolla.com/oauth/v2/token');
        mock.respond(req);
        
        
        req.setEndpoint('/funding-sources');
        mock.respond(req);
        Test.stopTest();
        
        
    }
    
    
    // Exception test case
    @isTest
    private static void exceptionTest() {
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerExcTestMock());
        Test.startTest(); 
        processFunctionality();
        Test.stopTest();
        
        
    }
    
    private static void processFunctionality(){
        
        
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c
                                 FROM Account
                                 LIMIT 1];
        PageReference fundingSourcePage = Page.FundingSource; // Add your VF page Name here
        Test.setCurrentPage(fundingSourcePage);
        ApexPages.currentPage().getParameters().put('id', accountRecord.Id);
        ApexPages.currentPage().getParameters().put('customerId', accountRecord.Dwolla_ID__c);
       /*
        Dwolla_Datasheet__c dwollaSheet = new Dwolla_Datasheet__c();
        dwollaSheet.Account__c = accountRecord.Id;
        dwollasheet.SSN__c = '111-11-1111';
        dwollasheet.EIN__c= '11-1111111';
        dwollasheet.Controller_SSN__c = '222-22-2222';
        dwollasheet.Plaid_Email_Address__c = 'test@test.com';
        insert dwollaSheet;*/
        
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(accountRecord);
        
        // Controller Instance
        FundingSourceController fundingSourceController = new FundingSourceController(standardController);
        fundingSourceController.customerId = accountRecord.Dwolla_ID__c;
        fundingSourceController.accountId = accountRecord.Id;
        fundingSourceController.accountName = accountRecord.Name;
        
        
        
        PlaidAPIConfiguration__c config = PlaidAPIConfiguration__c.getOrgDefaults();
        String accountId = accountRecord.Id;
        fundingSourcePage.getParameters().put('public_token', config.Public_Key__c);
        fundingSourcePage.getParameters().put('account_id', accountId);
        
        fundingSourceController.getAccessToken();
        //fundingSourceController.assignDwollaFundingSource();
        
    }
    
     public static testmethod void processFunctionality2(){
     
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c, PlaidSameDayAccessToken__c 
                                 FROM Account
                                 LIMIT 1];    
        Dwolla_Datasheet__c dwollaSheet = new Dwolla_Datasheet__c();
        dwollaSheet.Account__c = accountRecord.Id;
        dwollasheet.SSN__c = '111-11-1111';
        dwollasheet.EIN__c= '11-1111111';
        dwollasheet.Controller_SSN__c = '222-22-2222';
        dwollasheet.Plaid_Email_Address__c = 'test@test.com';
        insert dwollaSheet;
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerTestMock());
        
        Test.startTest(); 
        PageReference fundingSourcePage = Page.FundingSource; // Add your VF page Name here
        
        Test.setCurrentPage(fundingSourcePage);
        ApexPages.currentPage().getParameters().put('id', accountRecord.Id);
        ApexPages.currentPage().getParameters().put('authType', 'SameDayAuth');
        
        ApexPages.StandardController standardController = new ApexPages.StandardController(accountRecord);
        
        // Controller Instance
        FundingSourceController fundingSourceController = new FundingSourceController(standardController);
        fundingSourceController.customerId = accountRecord.Dwolla_ID__c;
        fundingSourceController.accountId = accountRecord.Id;
        fundingSourceController.accountName = accountRecord.Name;
        fundingSourceController.accessToken = '56465465465';
        
        
        PlaidAPIConfiguration__c config = PlaidAPIConfiguration__c.getOrgDefaults();
        String accountId = accountRecord.Id;
        fundingSourcePage.getParameters().put('public_token', config.Public_Key__c);
        fundingSourcePage.getParameters().put('account_id', accountId);
        fundingSourcePage.getParameters().put('id', accountId);
        
        fundingSourceController.getAccessToken();
        FundingSourceControllerTestMock.flow = 1;
        fundingSourceController.createDwollaFundingSource();
        FundingSourceControllerTestMock.flow = 2;
        fundingSourceController.createDwollaFundingSource();
        FundingSourceControllerTestMock.flow = 3;
        fundingSourceController.createDwollaFundingSource();
        Test.stopTest();
    }
   
}