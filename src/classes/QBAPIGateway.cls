public with sharing class QBAPIGateway {
    private HTTPRequest request;
    private QBConfigurationWrapper config;

    public QBAPIGateway(QBConfigurationWrapper config){
        this.request = new HTTPRequest();
        //this.config = new QBConfigurationWrapper();
        // Default headers
        Map<String, String> defaultHeaders = new Map<String, String>{'Accept' => 'application/json', 
                                                                     'Content-Type' => 'application/json'};
        for(String header: defaultHeaders.keySet()){
            request.setHeader(header, defaultHeaders.get(header));
        }
        this.config = config;
    }

    
    
    public HTTPResponse sendRequest(){
        HTTPResponse response = new HTTPResponse();
        try{
            HTTP httpService = new HTTP();
            response = httpService.send(request);
        }catch(Exception e){
            System.debug('Exception in API call :: ' + e.getMessage());
            QuickbookDebugServices.trackException(e); 
        }
        return response;
    }

    public String handleResponse(HttpResponse response){
        return response.getBody();
    }

    public HttpResponse get(){
        request.setMethod('GET');
        prepareRequest();
        return sendRequest();
    }

    public HttpResponse post(String body){
        request.setMethod('POST');
        request.setBody(body);
        request.setTimeout(60000);
        prepareRequest();
        return sendRequest();
    }

    public String query(String query){
        String responseBody = '';
        String endpoint = config.apiEndpoint + '/query?query=' + query;
        //String endpoint = '';
        request.setEndpoint(endpoint);
        HTTPResponse httpResponse = get();
        if(httpResponse.getStatusCode() == 200){
            responseBody = httpResponse.getBody().replaceAll('"time":"', '"time_qb":"');
        }
        else{
            //QuickBookDebugServices.trackSystemDebug('Response Status Code '+httpResponse.getStatusCode()+' Body::'+httpResponse.getBody());
            //ErrorHandler.debugHTTPResponse('QBAPIGAteway', 'Query', httpResponse);
        }
        return responseBody;
    }

    public String batch(String body){
        String responseBody = '';
        String endpoint = config.apiEndpoint + '/batch?minorversion=8';
        request.setEndpoint(endpoint);
        HTTPResponse httpResponse = post(body);
        if(httpResponse.getStatusCode() == 200){
            responseBody = httpResponse.getBody().replaceAll('"time":"', '"time_qb":"');
        }
        else{
            System.debug('responseBody::::::'+httpResponse.getBody()+httpResponse.getStatusCode());
            //QuickBookDebugServices.trackSystemDebug('Response Status Code '+httpResponse.getStatusCode()+' Body::'+httpResponse.getBody());
            //ErrorHandler.debugHTTPResponse('QBAPIGAteway', 'Query', httpResponse);
        }
        return responseBody;
    }

    private void prepareRequest(){
        OAuth2 oAuth2Service = new OAuth2(config);
        
        request = oAuth2Service.prepareRequest(request);
    }


}