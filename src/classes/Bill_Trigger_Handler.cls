/*
Test class: case_trigger_test
*/
public class Bill_Trigger_Handler {


  public static void handleBeforeInsert(List<Bill__c> billNew){
  
    /*** Calculation Of payable To member     ***/
    Bill_Helper_Class.calculatePayableToMember(billNew,null,null);
  
  
  }
  
  
}