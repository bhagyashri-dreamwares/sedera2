/**  
 * @Purpose      : Test Class for InvoiceController
 * @Author       : Dreamwares 
 * @Created Date : 17/08/2018
 */
@isTest
public class InvoiceControllerTest {
    /**
     * @Purpose : Method to create test data
     */
    @TestSetup
    public static void createTestData(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account account = new Account(Name = 'Test Account', RecordtypeId = recordtypeId, Phone = '9898989890');
        insert account;
        
        recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();       
        
        Contact contact = new Contact(FirstName = 'Test', LastName = 'Sample', AccountId = account.Id, RecordtypeId = recordtypeId);
        insert contact;
        
        Invoice__c invoice = new Invoice__c(Account__c = account.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 50000);
        insert invoice;
        
        Product2 product = new Product2(Name = 'Member Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus');
        insert product;
        
        Invoice_Line_Item__c invoiceLineItem = new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                                        Quantity__c = 1, Product__c = product.Id, QBO_Product__c = product.Id);
        
        Product2 product2 = new Product2(Name = 'Tobacco', ProductCode = 'Tobacco', isActive = true, Family='Assure Voice-Bus');
        insert product2;
        
        Invoice_Line_Item__c invoiceLineItem2 = new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                                        Quantity__c = 1, Product__c = product.Id, QBO_Product__c = product2.Id);
        
        Product2 product3 = new Product2(Name = 'Select', ProductCode = 'Select', isActive = true, Family='Assure Voice-Bus');
        insert product3;
        
        Invoice_Line_Item__c invoiceLineItem3 = new Invoice_Line_Item__c(Account__c = account.Id, Invoice__c = invoice.Id, Price__c = 5000,
                                                                        Quantity__c = 1, Product__c = product.Id, QBO_Product__c = product3.Id);
        
        insert invoiceLineItem;
        insert invoiceLineItem2;
        insert invoiceLineItem3;
    }
    
    /**
     * @Purpose: Test page Invoice 
     */
    static testMethod void testInvoicePage() {
        Account account = [SELECT Id FROM Account WHERE Name = 'Test Account' LIMIT 1];
        
        Invoice__c invoice = [SELECT Id FROM Invoice__c WHERE Account__c =: account.Id LIMIT 1];
        
        Test.startTest();
        PageReference pageRef = Page.Invoice;
        Test.setCurrentPageReference(pageRef);
        Apexpages.currentpage().getparameters().put('Id', String.valueOf(account.Id) + '-' + String.valueOf(invoice.Id));
        InvoiceController controller = new InvoiceController();
        
        Test.stopTest();
        
        System.assertEquals(invoice.Id, controller.invoiceWrapper.Invoice.Id); 
    }
    
    /**
     * @Purpose: Test page 
     */
    static testMethod void testNegCase() {
                
        Test.startTest();
        PageReference pageRef = Page.Invoice;
        Test.setCurrentPageReference(pageRef);
        Apexpages.currentpage().getparameters().put('Id', '0011D00000Imsrtr' + '-' + 'a2e1D0000006UhV');
        Apexpages.currentpage().getparameters().put('renderPageAs', 'PDF');
        InvoiceController controller = new InvoiceController();
        InvoiceController.InvoiceWrapper obj = controller.getInvoice(new List<String>{});       
        Test.stopTest();
    }
}