/**
*@Purpose : Helper class for all API request
*@Date : 06/09/2018
*/
public class CalloutHelper{
    
    /**
    *@Purpose: To process Http Post request
    */
    public static HttpResponse httpPostRequest(String endPoint, Map<String,String> endPointParameterMap,
                                       Map<String,String> headerParameterMap, JSONGenerator gen){
                                           
        HttpResponse respose;
        
        if(String.isNotBlank(endPoint) && endPointParameterMap != null && headerParameterMap != null){
            
            Http http = new Http();
            
            HttpRequest request = new HttpRequest();
            
            request.setEndpoint(getEndPointUrl(endPointParameterMap, endPoint));
            request.setMethod('POST');
            
            System.debug('request @@@ '+request);
            
            for(String key :headerParameterMap.keySet()){            
                request.setHeader(key, headerParameterMap.get(key));
            }
            
            if(gen != null){
                request.setBody(gen.getAsString());
            }
            try{
                // Send the request
                respose = http.send(request);
                System.debug('respose @@@ '+respose);
                
            } catch(Exception exp){
                LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('CalloutHelper.httpPostRequest', exp, 'endPoint: '+endPoint+
                                                                      '\n\n endPointParameterMap: '+endPointParameterMap+
                                                                      '\n\n headerParameterMap: '+headerParameterMap+
                                                                      '\n\n JSONGenerator : '+gen, 
                                                                 '', 'Error'))});
            }
        }   
        System.debug('respose :::'+respose); 
        return respose;
    }
    
    /**
    *@Purpose: To process Http Get request
    */
    public static HttpResponse httpGetRequest(String endPoint, Map<String,String> endPointParameterMap,
                                       Map<String,String> headerParameterMap){
                                           
        HttpResponse respose;
        
        if(String.isNotBlank(endPoint) && endPointParameterMap != null && headerParameterMap != null){
            
            Http http = new Http();
            
            HttpRequest request = new HttpRequest();
            
            request.setEndpoint(getEndPointUrl(endPointParameterMap, endPoint));
            request.setMethod('GET');
            
            for(String key :headerParameterMap.keySet()){            
                request.setHeader(key, headerParameterMap.get(key));
            }
            
            try{
                // Send the request
                respose = http.send(request);
                System.debug('respose @@@ '+respose);
                
            } catch(Exception exp){
                LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('CalloutHelper.httpGetRequest', exp, 'endPoint: '+endPoint+
                                                                      '\n\n endPointParameterMap: '+endPointParameterMap+
                                                                      '\n\n headerParameterMap: '+headerParameterMap, 
                                                                 '', 'Error'))}); 
                
            }
        }   
        System.debug('respose :::'+respose); 
        return respose;
    }
    
    
    /**
    *@Purpose To generate endPoint url
    */
    private static String getEndPointUrl(Map<String, String> pageParameterMap, String url){
        
        String pageUrl = '';
        
        if(String.isNotBlank(url)){
            pageUrl = url + '?';
            
            // generate URL
            for(String key :pageParameterMap.keySet()){            
                
                String mapValue = pageParameterMap.get(key);    
                pageUrl += String.isNotBlank(mapValue) ? 
                           key+'='+mapValue+'&' : 
                           '';
            }
            pageUrl = pageUrl.containsAny('&') ? 
                      pageUrl.substringBeforeLast('&') : 
                      pageUrl.remove('?');
        
        }
        return pageUrl;
    }
}