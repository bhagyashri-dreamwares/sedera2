/**
* @Purpose: Controller for EONS shared record event
*/
public class EONSEventController{
    
    /**
    *    To store email current status on ENOs shared record
    */
    @InvocableMethod(label='Fetch Email Status' description='Fetch email status from Paubox using Tracking Id')
    public static List<String> storeEmailStatus(List<String> objectIdList){
        
        if(!objectIdList.isEmpty()){
            
            List<EONS_Shared__c> eONSRecordList = PauBoxEmailHelper.getEONSRecord(objectIdList);   
            if(!eONSRecordList.isEmpty()){
                
            }
            Database.executeBatch(new EONSEventbatch(eONSRecordList), 1);
           
        }
        
        return new List<String>();
    }
}