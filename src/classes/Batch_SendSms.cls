public class Batch_SendSms implements Database.Batchable <Sendsms> ,database.stateful, Database.AllowsCallouts {

    public List <Sendsms> SendsmsList;
    public List <SMS_Log__c> Loglist;
    public Batch_SendSms(List<Sendsms> SendsmsList) {
        this.SendsmsList=SendsmsList;   
    }

    public Iterable<Sendsms> start(Database.BatchableContext BC) {
       Loglist=new List<SMS_log__c>();
       return SendsmsList;
    }

    public void execute(Database.BatchableContext BC, List <SendSMS> scope) {


        for (Sendsms sms: scope) {
            SMS_Log__c log=new SMS_Log__c();
            try{               
            Log.Account__c=sms.recordId; 
            Log.Description__c=sms.description;            
            Log.Reason__c=sms.Reason;            
            Log.SMS_Body__c=sms.smsbody;
            Log.Source__c=sms.source;
            Log.name=sms.Name;
            sms.phNumber = sms.phNumber.remove('(');
            sms.phNumber = sms.phNumber.remove(' ');
            sms.phNumber = sms.phNumber.remove(')');
            sms.phNumber = sms.phNumber.remove('-');
            sms.phNumber = sms.phNumber.remove('+1');
            sms.phNumber = '+1' + sms.phNumber;
            Log.Recipient_Phone_Number__c=sms.phnumber;
            Integer statusCode=sms.processSms();
            sms.statusCode=statusCode;
            if(statusCode==201)
            Log.IsSuccess__c=true;
            else
            Log.IsSuccess__c=false;
            }catch(Exception e)
            {
             Log.IsSuccess__c=false;
            }
            LogList.add(Log);

           }
    }

    public void finish(Database.BatchableContext BC) {
     
    try{ 
    
     If(loglist.size()>0)
      Insert loglist;
      }catch(Exception e){
       system.debug(e.getMessage());
      
      }
    }




}