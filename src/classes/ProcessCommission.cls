global class ProcessCommission implements Database.Batchable<SObject>, Database.Stateful
{
    private ID commissionReportId;
    private Date reportDate;
    private Date cancellationDate;
    private Date nextInvoiceDate;
    
    //Map of Affiliate member and commission total
    global final Map<ID,Map<String,Decimal>> affiliateMap = new Map<ID,Map<String,Decimal>>();
    
    global final Commission_Report__c cr;
    
    //Map of Affiliate to Affiliate Pricing
    global final Map<ID,Affiliate_Pricing__c> pricingMap = new Map<ID,Affiliate_Pricing__c>();
    
    //Map Affiliate to SPR's
    global final Map<ID,Map<String, List<Sedera_Product_Report__c>>> productSprMap = new Map<ID,Map<String, List<Sedera_Product_Report__c>>>();
    
    //Internal Sales Map
    global final Map<ID,ID> internalSalesMap = new Map<ID,ID>();
    
    //Existing line items from reprocess
    List<Commission_Report_Item__c> crItemListDelete = new List<Commission_Report_Item__c>();
    
    public ProcessCommission(ID commissionReportIdIncoming)
    {
        commissionReportId = commissionReportIdIncoming;
        cr = [SELECT Id, Month__c, Year__c FROM Commission_Report__c WHERE ID = :commissionReportId LIMIT 1];
		
        reportDate = Date.valueOf(cr.Year__c + '-' + cr.Month__c + '-01');
		Integer month = Integer.valueOf(cr.Month__c) + 1;
        nextInvoiceDate = Date.valueOf(cr.Year__c + '-' + String.valueOf(month) + '-01');
        cancellationDate = nextInvoiceDate.addDays(-1);
    }
   

    public Database.QueryLocator start(Database.BatchableContext context)
    {
    	return Database.getQueryLocator([SELECT Id, 
                                             Enrollment_Date__c, 
                                             Subscription_Status__c, 
                                             Dependent_Status__c, 
                                             Amount__c,
                                             Date_of_Birth__c, 
                                             ProductName__c, 
                                             Health_Care_Sharing__c, 
                                             Primary_Age__c,
                                             Member_Account__c, 
                                             Member_Account__r.id, 
                                             Member_Account__r.Name, 
                                             Invoice__r.Name,
                                             Member_Account__r.Primary_Contact_id__c, 
                                             Member_Account__r.IUA_Chosen__c, 
                                             Invoice__c, 
                                             Invoice__r.Account__c, 
                                             Employer_Account__r.Name, 
                                             Product__c, 
                                             Pricing__c,
                                             Doing_Frontline_Support__c,
                                         	 Member_Account__r.Months_Enrolled__c, 
                                             Member_Account__r.Enrollment_Date__c,
                                             Member_Account__r.Cancellation_Date__c, 
                                             Referral_Affiliate__c,
                                             Strategic_Affiliate__c,
                                         	 Lead_Generator__c,
                                             Internal_Salesperson__c,
                                          	 Brokerage_Firm__c,
                                          	 Months_Enrolled__c 
                                             FROM Sedera_Product_Report__c
                                             WHERE (Invoice__r.Invoice_Due_Date__c = :nextInvoiceDate
                                                 AND Product__c != NULL
                                                 AND Pricing__c != NULL
                                                 AND Member_Account__r.Enrollment_Date__c < :nextInvoiceDate
                                                 AND Member_Account__r.Cancellation_Date__c != :cancellationDate)
                                         	 OR (Invoice__r.Invoice_Due_Date__c = :reportDate
                                                 AND Product__c != NULL
                                                 AND Pricing__c != NULL
                                                 AND Member_Account__r.Cancellation_Date__c = :cancellationDate)
                                             ORDER BY Enrollment_Date__c, Employer_Account__r.Name, Member_Account__r.Name]);
    }

    public void execute(Database.BatchableContext context, List<Sedera_Product_Report__c> scope)
    {
        getPricingData(scope);
        
        
        
        for(Sedera_Product_Report__c spr :scope)
        {
            if(spr.Referral_Affiliate__c != null && pricingMap.get(spr.Referral_Affiliate__c) != null)
            {
                aggregateSPR(spr,spr.Referral_Affiliate__c);
            }
            
            if(spr.Strategic_Affiliate__c != null && pricingMap.get(spr.Strategic_Affiliate__c) != null)
            {
                aggregateSPR(spr,spr.Strategic_Affiliate__c);
            }
            
            if(spr.Lead_Generator__c != null && pricingMap.get(spr.Lead_Generator__c) != null)
            {
               	aggregateSPR(spr,spr.Lead_Generator__c);
            }
            
            if(spr.Internal_Salesperson__c != null && pricingMap.get(spr.Internal_Salesperson__c) != null)
            {
                internalSalesMap.put(spr.Internal_Salesperson__c,spr.Internal_Salesperson__c);
                aggregateSPR(spr,spr.Internal_Salesperson__c);
            }
            
            if(spr.Brokerage_Firm__c != null && pricingMap.get(spr.Brokerage_Firm__c) != null)
            {
                aggregateSPR(spr,spr.Brokerage_Firm__c);
            }
        }
        
        
    }

    public void finish(Database.BatchableContext context)
    {
		processSPRmap();
        
        List<Commission_Report_Item__c> criItemsToReuse = new List<Commission_Report_Item__c>();
        criItemsToReuse = [SELECT ID, Affiliate__c,Commission_Report__c FROM Commission_Report_Item__c WHERE Commission_Report__c = :commissionReportId];
        Map<ID,Commission_Report_Item__c> criItemsToReuseMap = new Map<ID,Commission_Report_Item__c>();
        if(criItemsToReuse.size() > 0)
        {
            for(Commission_Report_Item__c cri :criItemsToReuse)
            {
                criItemsToReuseMap.put(cri.Affiliate__c,cri);
            }
        }
        
        List<Commission_Report_Item__c> crItemList = new List<Commission_Report_Item__c>();
        for (ID affiliateId : affiliateMap.keySet()) 
        {
            Map<String,Decimal> mapList = affiliateMap.get(affiliateId);
            if(criItemsToReuseMap.get(affiliateId) == null)
            {
                Commission_Report_Item__c crItem = new Commission_Report_Item__c();
                crItem.Affiliate__c = affiliateId;
                crItem.Commission_Report__c  = cr.id;
                crItem.Member_Count__c = mapList.get('members');
                crItem.Total_Commission__c = mapList.get('commission');
                crItemList.add(crItem);
            }
            else
            {
                Commission_Report_Item__c crItem = criItemsToReuseMap.get(affiliateId);
                crItem.Member_Count__c = mapList.get('members');
                crItem.Total_Commission__c = mapList.get('commission');
                crItemList.add(crItem);
            }
        }
        System.debug(crItemList);
        upsert crItemList;
		
        cr.Processing__c = false;
        update cr;
    }
    
    public void processSPRmap()
    {
        for (String affiliateId : productSprMap.keySet()) 
        {
            if(internalSalesMap.get(affiliateId) == null)
            {
                Map<String, List<Sedera_Product_Report__c>> mapList = productSprMap.get(affiliateId);
                for (String key : mapList.keySet()) 
                {
                    switch on key
                    {
                        when 'ACCESSNEW'{configureAccessNew(mapList.get(key),pricingMap.get(affiliateId));}
                        when 'SELECTNEW'{configureSelectNew(mapList.get(key),pricingMap.get(affiliateId));}
                        when 'SELECTOLD'{configureSelectOld(mapList.get(key),pricingMap.get(affiliateId));}
                        when else {}
                    }
                }       
            }
            else
            {
                Map<String, List<Sedera_Product_Report__c>> mapList = productSprMap.get(affiliateId);
                for (String key : mapList.keySet()) 
                {
                    configureInternalCommission(mapList.get(key),pricingMap.get(affiliateId), key);
                }  
            }
                 
        }
    }
    
    private void configureInternalCommission(List<Sedera_Product_Report__c> mapList,Affiliate_Pricing__c apRecord, String productType)
    {
        if(affiliateMap.get(apRecord.Affiliate__c) == null)
        {
        	Map<String,Decimal> totalMap = new Map<String,Decimal>();
            totalMap.put('members',0);
            totalMap.put('commission',0.0);
            affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        }
        
        Map<String,Decimal> totalMap = affiliateMap.get(apRecord.Affiliate__c);
        
        for(Sedera_Product_Report__c spr :mapList)
        {
            totalMap.put('members', totalMap.get('members') + 1);
            if(productType.contains('ACCESS') && spr.Months_Enrolled__c == 0)
            {
                totalMap.put('commission',totalMap.get('commission') + apRecord.New_Lives_in_Territory_Access__c);
            }
            else if(productType.contains('SELECT') && spr.Months_Enrolled__c == 0)
            {
                totalMap.put('commission',totalMap.get('commission') + apRecord.New_Lives_in_Territory_Select__c);
            }
        }
        
        totalMap.put('commission',
                     totalMap.get('commission') + apRecord.Base_Sales_Commissions__c 
                     							+ apRecord.Over_Baseline_Commissions__c
                    							+ apRecord.Base_Mgmt_Commission__c
                    							+ apRecord.MGMT_Over_Baseline_Commissions__c);
        
    }
    
    private void configureAccessNew(List<Sedera_Product_Report__c> mapList, Affiliate_Pricing__c apRecord)
    {
        if(affiliateMap.get(apRecord.Affiliate__c) == null)
        {
        	Map<String,Decimal> totalMap = new Map<String,Decimal>();
            totalMap.put('members',0);
            totalMap.put('commission',0.0);
            affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        }
        
        Map<String,Decimal> totalMap = affiliateMap.get(apRecord.Affiliate__c);
        
        for(Sedera_Product_Report__c spr :mapList)
        {
            totalMap.put('members', totalMap.get('members') + 1);
            if(spr.Doing_Frontline_Support__c == TRUE)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_EC__c);}
                }
            } 
            else
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_NOT_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_NOT_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_NOT_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.ACCESS_RA_NOT_EC__c);}
                }
            }
        }
        
        affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        System.debug(affiliateMap.get(apRecord.Affiliate__r.id));
    }
    
    private void configureSelectNew(List<Sedera_Product_Report__c> mapList, Affiliate_Pricing__c apRecord)
    {
        if(affiliateMap.get(apRecord.Affiliate__c) == null)
        {
        	Map<String,Decimal> totalMap = new Map<String,Decimal>();
            totalMap.put('members',0);
            totalMap.put('commission',0.0);
            affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        }
        
        Map<String,Decimal> totalMap = affiliateMap.get(apRecord.Affiliate__c);
        Integer sprCount = 0;
        for(Sedera_Product_Report__c spr :mapList)
        {
            totalMap.put('members', totalMap.get('members') + 1);
            ++sprCount;
            if(sprCount <= 100)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_100_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_100_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_100_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_100_EC__c);}
                }
            } 
            else
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_101_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_101_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_101_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_NEW_101_EC__c);}
                }
            }
        }
        
        affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        System.debug(affiliateMap.get(apRecord.Affiliate__r.id));
    }
    
    private void configureSelectOld(List<Sedera_Product_Report__c> mapList, Affiliate_Pricing__c apRecord)
    {
        if(affiliateMap.get(apRecord.Affiliate__c) == null)
        {
        	Map<String,Decimal> totalMap = new Map<String,Decimal>();
            totalMap.put('members',0);
            totalMap.put('commission',0.0);
            affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        }
        
        Map<String,Decimal> totalMap = affiliateMap.get(apRecord.Affiliate__c);
        Integer sprCount = 0;
        for(Sedera_Product_Report__c spr :mapList)
        {
            totalMap.put('members', totalMap.get('members') + 1);
            ++sprCount;
            if(sprCount <= 100)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_100_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_100_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_100_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_100_EC__c);}
                }
            } 
            else if(sprCount <= 250)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_250_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_250_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_250_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_250_EC__c);}
                }
            }
            else if(sprCount <= 500)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_500_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_500_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_500_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_500_EC__c);}
                }
            }
            else 
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_501_EF__c);}
                    when 'EO'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_501_EO__c);}
                    when 'ES'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_501_ES__c);}
                    when 'EC'{totalMap.put('commission',totalMap.get('commission') + apRecord.SELECT_OLD_501_EC__c);}
                }
            }
        }
        
        affiliateMap.put(apRecord.Affiliate__r.id, totalMap);
        System.debug(affiliateMap.get(apRecord.Affiliate__r.id));
    }
    
    
    public void aggregateSPR(Sedera_Product_Report__c spr, ID affiliateId)
    {
        String key = spr.Product__c + spr.Pricing__c;
        System.debug('KEY = ' + key);
        if(productSprMap.get(affiliateId) == null)
        {
            Map<String, List<Sedera_Product_Report__c>> mapList = new Map<String, List<Sedera_Product_Report__c>>();
            List<Sedera_Product_Report__c> sprList = new List<Sedera_Product_Report__c>();
            sprList.add(spr);
            mapList.put(key,sprList);
            productSprMap.put(affiliateId,mapList);
        }
        else
        {
            Map<String, List<Sedera_Product_Report__c>> mapList = productSprMap.get(affiliateId);
            if(mapList.get(key) != null)
            {
                List<Sedera_Product_Report__c> sprList = mapList.get(key);
                sprList.add(spr);
                mapList.put(key,sprList);
                productSprMap.put(affiliateId,mapList);
            }
            else
            {
                List<Sedera_Product_Report__c> sprList = new List<Sedera_Product_Report__c>();
                sprList.add(spr);
                mapList.put(key,sprList);
                productSprMap.put(affiliateId,mapList);
            }
        }
    }
    
    public void getPricingData(List<Sedera_Product_Report__c> sprList)
    {
        Set<ID> affiliateIds = new Set<ID>();
		for(Sedera_Product_Report__c spr :sprList)
        {
            if(pricingMap.get(spr.Referral_Affiliate__c) == null)
            {
                affiliateIds.add(spr.Referral_Affiliate__c);
            }
            
            if(pricingMap.get(spr.Strategic_Affiliate__c) == null)
            {
                affiliateIds.add(spr.Strategic_Affiliate__c);
            }
            
            if(pricingMap.get(spr.Lead_Generator__c) == null)
            {
                affiliateIds.add(spr.Lead_Generator__c);
            }
            
            if(pricingMap.get(spr.Internal_Salesperson__c) == null)
            {
                affiliateIds.add(spr.Internal_Salesperson__c);
            }
            
            if(pricingMap.get(spr.Brokerage_Firm__c) == null)
            {
                affiliateIds.add(spr.Brokerage_Firm__c);
            }
        }
        
        if(affiliateIds.size() > 0)
        {
            List<Affiliate_Pricing__c> apList = new List<Affiliate_Pricing__c>();
            
            String query = '';
            String SobjectApiName = 'Affiliate_Pricing__c';
            Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
            
            String commaSepratedFields = '';
            for(String fieldName : fieldMap.keyset()){
                if(commaSepratedFields == null || commaSepratedFields == ''){
                    commaSepratedFields = fieldName;
                }else{
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
            }
            
            query = 'select Affiliate__r.id, ' + commaSepratedFields + ' from ' + SobjectApiName + ' WHERE Affiliate__c IN :affiliateIds';
            
            apList = Database.query(query);
            
            
            /*apList = [SELECT Id,
                    		ACCESS_RA_EC__c,
                    		ACCESS_RA_EF__c,
                            ACCESS_RA_EO__c,
                            ACCESS_RA_ES__c,
                            ACCESS_RA_NOT_EC__c,
                            ACCESS_RA_NOT_EF__c,
                            ACCESS_RA_NOT_EO__c,
                            ACCESS_RA_NOT_ES__c,
                            Affiliate__c,
                      		Affiliate__r.id,
                            Effective_Date__c,
                            SELECT_NEW_100_EC__c,
                            SELECT_NEW_100_EF__c,
                            SELECT_NEW_100_EO__c,
                            SELECT_NEW_100_ES__c,
                            SELECT_NEW_101_EC__c,
                            SELECT_NEW_101_EF__c,
                            SELECT_NEW_101_EO__c,
                            SELECT_NEW_101_ES__c,
                            SELECT_OLD_100_EC__c,
                            SELECT_OLD_100_EF__c,
                            SELECT_OLD_100_EO__c,
                            SELECT_OLD_100_ES__c,
                            SELECT_OLD_250_EC__c,
                            SELECT_OLD_250_EF__c,
                            SELECT_OLD_250_EO__c,
                            SELECT_OLD_250_ES__c,
                            SELECT_OLD_500_EC__c,
                            SELECT_OLD_500_EF__c,
                            SELECT_OLD_500_EO__c,
                            SELECT_OLD_500_ES__c,
                            SELECT_OLD_501_EC__c,
                            SELECT_OLD_501_EF__c,
                            SELECT_OLD_501_EO__c,
                            SELECT_OLD_501_ES__c
                   			FROM Affiliate_Pricing__c
                   			WHERE Affiliate__c IN :affiliateIds ];*/
            
            for(Affiliate_pricing__c ap :apList)
            {
                pricingMap.put(ap.Affiliate__c,ap);
            }
        }
    }

}