public virtual class  BQBQueryResponse {
    public Integer startPosition{get; set;}
    public Integer maxResults{get; set;}
    public List<QBWrappers.QBCustomer> Customer;
    public List<QBWrappers.QBItem> Item;
    public List<QBWrappers.QBSalesReceipt> SalesReceipt;
    public List<QBWrappers.QBInvoiceDTO> Invoice;
    public List<QBWrappers.QBChartAccount> Account;
    public List<QBWrappers.QBCreditMemo> CreditMemo;
    public List<QBWrappers.QBPayment> Payment;

    public BQBQueryResponse(){
        Customer = new List<QBWrappers.QBCustomer>();
        Item = new List<QBWrappers.QBItem>();
        Invoice = new List<QBWrappers.QBInvoiceDTO>();
        Account = new List<QBWrappers.QBChartAccount>();
        CreditMemo = new List<QBWrappers.QBCreditMemo>();
        Payment = new List<QBWrappers.QBPayment>();
    }
}