/**
 * @Purpose            : Controller class for VF page : Invoice
 * @Created Date       : 16/08/2018
 */ 
public class InvoiceController {
    public InvoiceWrapper invoiceWrapper {get;  set;}
    public Boolean isPdf   {get;  set;}
    public String invoiceTitle {get; set;}
    public List<InvoiceLineWrapper> invLineWrapperList {get; set;}
    
    //Constructor
    public InvoiceController(){
        invoiceWrapper = new InvoiceWrapper();
        invLineWrapperList = new List<InvoiceLineWrapper>();
                
        //Check page has pdf parameter or not
        isPdf = String.isNotBlank(ApexPages.currentPage().getParameters().get('isPdf')) ? true : false;
                
        String urlPageParameter = Apexpages.currentpage().getparameters().get('id');
                
        if (String.isNotBlank(urlPageParameter) && urlPageParameter.contains('-')){
            List<String> pageParamList = urlPageParameter.split('-');
            
            //Get Invoice
            invoiceWrapper = getInvoice(pageParamList);
            System.debug('invoiceWrapper::'+invoiceWrapper);
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide a valid parameter in the form Account Id - Invoice Id.!');
            ApexPages.addMessage(errorMsg);
        }
    }
    
           
    /**
     * @Purpose : Method to fetch Invoice
     */
    public InvoiceWrapper getInvoice(List<String> pageParamList){
        Invoice__c invoice = new Invoice__c();
        Map<String, Invoice_Associated_Bank__c> footerMap = getFooter();
                
        if (!pageParamList.isEmpty() && pageParamList.size() == 2 && String.isNotBlank(pageParamList[0]) && String.isNotBlank(pageParamList[1])){
            try{
                //Fetch Invoice
                invoice = [SELECT Account__c, Amount__c, Invoice_Date__c, Invoice_Due_Date__c, Name, Invoice_Number__c, Account__r.Name,
                           Account__r.BillingStreet, Account__r.BillingCity, Account__r.BillingState, Account__r.BillingPostalCode,
                           Account__r.BillingCountry, QB_Terms_ID__c, Submission_Status__c, Account__r.Associated_Bank__c,
                           (SELECT Name, Price__c, Quantity__c, Description__c, Line_Item_Total__c, Product__r.Name, QBO_Product__r.Name  
                           FROM Invoice_Line_Items__r WHERE Line_Item_Total__c != 0)
                           FROM Invoice__c
                           WHERE Id =: pageParamList[1] AND Account__c =: pageParamList[0]];
            }catch (Exception e){
                System.debug('Exception occured at line no. '+e.getLineNumber()+ ' Error: '+e.getMessage());  
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to fetch Invoice.!');
                ApexPages.addMessage(errorMsg);      
            }
            
            if (invoice != null){
                invoiceWrapper.Invoice = invoice;
                if(invoice.Submission_Status__c == 'Approved'){
                    invoiceTitle = 'INVOICE';
                } else {
                    invoiceTitle = 'DRAFT INVOICE';
                }
                if(invoice.Account__c != null && String.isNotBlank(invoice.Account__r.Associated_Bank__c)
                   && footerMap.containsKey(invoice.Account__r.Associated_Bank__c.toUpperCase())){                    
                   invoiceWrapper.footer = footerMap.get(invoice.Account__r.Associated_Bank__c.toUpperCase()).Footer_Text__c;
                }else if(footerMap.containsKey('NONE')){
                   invoiceWrapper.footer = footerMap.get('NONE').Footer_Text__c;
                }
                
                List<Invoice_Line_Item__c> iliList = invoice.invoice_Line_Items__r;
               
                if(iliList.size() > 0)
                {
                    InvoiceLineWrapper memberInvLineWrap = new InvoiceLineWrapper();
                    memberInvLineWrap.productName = 'Membership Fee';
                    
                    for(Invoice_Line_Item__c ilItem :iliList)
                    {
                        InvoiceLineWrapper invLineWrap = new InvoiceLineWrapper();
                        if(ilItem.QBO_Product__r.Name.contains('Sedera') || ilItem.QBO_Product__r.Name.contains('Member'))
                        {
                           memberInvLineWrap.total = memberInvLineWrap.total + ilItem.Line_Item_Total__c;
                           memberInvLineWrap.quantity = memberInvLineWrap.quantity + ilItem.Quantity__c;
                        }
                        else if(ilItem.QBO_Product__r.Name.contains('Tobacco'))
                        {
                    		invLineWrap.productName = 'Tobacco';
                            invLineWrap.quantity = ilItem.Quantity__c;
                            invLineWrap.total = ilItem.Line_Item_Total__c;
                            invLineWrapperList.add(invLineWrap);
                        }
                        else
                        {
                    		invLineWrap.productName = ilItem.Description__c;
                            invLineWrap.quantity = ilItem.Quantity__c;
                            invLineWrap.total = ilItem.Line_Item_Total__c;
                            invLineWrapperList.add(invLineWrap);
                        }
                    }
					memberInvLineWrap.quantity = memberInvLineWrap.quantity/2; 
                    system.debug('memberInvLineWrap = ' + memberInvLineWrap);
                    system.debug('invLineWrapperList = ' + invLineWrapperList);
                    if(invLineWrapperList.size() > 0)
                    {
                    	invLineWrapperList.add(0,memberInvLineWrap);
                    }
                    else
                    {
                        invLineWrapperList.add(memberInvLineWrap);
                    }
                    
                }
            }
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide a valid page parameters.!');
            ApexPages.addMessage(errorMsg);    
        }
        
        return invoiceWrapper;
    }
    private static Map<String, Invoice_Associated_Bank__c> getFooter(){
        Map<String, Invoice_Associated_Bank__c> footerMap = new Map<String, Invoice_Associated_Bank__c>();
        
        Map<String, Invoice_Associated_Bank__c> customSettingMap = Invoice_Associated_Bank__c.getAll();
        for(String key :customSettingMap.keySet()){
            
            footerMap.put(key.toUpperCase(), customSettingMap.get(key));
        }
        return footerMap;
    }
    
    public class InvoiceLineWrapper
    {
        public String productName {get; set;}
        public Decimal quantity {get; set;}
        public Decimal total {get; set;}
        
        public InvoiceLineWrapper()
        {
            productName = '';
            quantity = 0;
            total = 0.0;
        }
    }
    
    //Wrapper class for Invoice
    public class InvoiceWrapper{
        public Invoice__c Invoice  {get; set;}
        public String footer {get; set;}
        
        public InvoiceWrapper(){
          Invoice = new Invoice__c(); 
          footer = '';  
        }
    }
}