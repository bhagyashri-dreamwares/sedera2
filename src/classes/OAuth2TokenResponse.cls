/*
 *@Author    : Dreamwares
 *@Purpose    : Wrapper class for OAuth2 access token request's response
 *@Created Date  : 18/07/2018
 */
public class OAuth2TokenResponse {
  public String refresh_token;
    public String token_type;
    public String access_token;
    public Integer expires_in;
    public Integer x_refresh_token_expires_in;
    public String realm_id;    
}