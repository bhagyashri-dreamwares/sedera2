public with sharing abstract class AQBQueryObject implements IPullGateway{
    
    protected IParser parserImpl;

    public BAPIResponseWrapper doGet(BRequestParams requestParams,String currentQuickbookOrg){
        
        BAPIResponseWrapper response = new BAPIResponseWrapper(false, '');

        QBConfigurationWrapper qbConfiguration = new QBConfigurationWrapper(currentQuickbookOrg);
        if(String.isNotBlank(requestParams.objectName)&& requestParams.startIndex != null 
            /* && requestParams.fromTime != null */){
                 
            String query = createQueryString(requestParams.objectName, 
                                                requestParams.startIndex, 
                                                requestParams.qbIds );
      
            QBAPIGateway qbGateway = new QBAPIGateway( qbConfiguration );
            //QuickBookDebugServices.trackSystemDebug('query '+query);
            system.debug('query '+query);
            
            String responseBody = qbGateway.query(query);
            
            system.debug('responseBody '+responseBody);
            //QuickBookDebugServices.trackSystemDebug('responseBody '+responseBody);
            
            if(String.isNotBlank(responseBody)){
                QBQueryResponseWrapper responseDTO = new QBQueryResponseWrapper();
                
                // parse QB Query response
                responseDTO = parseQBQueryResponse(responseBody);
                // Parse records to SObject records            
                List<Object> recordsList = getObjectRecords(responseDTO);
                system.debug('recordsList '+recordsList);
                response = new BAPIResponseWrapper(true, '');
                response.records = recordsList;
                response.recordsCount    = responseDTO.QueryResponse.maxResults;
                response.startIndex      = responseDTO.QueryResponse.startPosition;
                response.responseTime    = QBSyncHelper.dateFromString(responseDTO.time_qb);
            }
        }
        else{
            System.debug('Invalid request Parameters for Customer Pull');
        }
        return response;
    }
    @testVisible
    private QBQueryResponseWrapper parseQBQueryResponse(String jsonString){
        QBQueryResponseWrapper responseDTO = new QBQueryResponseWrapper();
        try{
            responseDTO = (QBQueryResponseWrapper)JSON.deserialize(jsonString,
                                                                    QBQueryResponseWrapper.class);
        }
        catch(Exception e){
            System.debug('Deserializing Exception :: ' + e.getMessage());
        }
        return responseDTO;
    } 

    abstract public List<Object> getObjectRecords(QBQueryResponseWrapper responseDTO);
    @testVisible
    private virtual String createQueryString(String objectName, Integer startIndex, List<String> qbIds){
    Set<String> objectsSet = new  Set<String>{'creditMemo','Payment'};
        String query = 'select * from ' + objectName + ' ';
        system.debug('In AQBQueryObject objectName  '+objectName);
        if(objectsSet.contains(objectName)){
            query += ' where CustomerRef IN';
            query += '(\''+String.join(qbIds,'\',\'')+'\')';
            query += ' STARTPOSITION ' + String.valueOf(startIndex) + ' MAXRESULTS 200';
        }
        else if(objectName.equalsIgnoreCase('Payment')){
            query += ' where Id IN';
            query += '(\''+String.join(qbIds,'\',\'')+'\')';
            query += ' STARTPOSITION ' + String.valueOf(startIndex) + ' MAXRESULTS 500';
            //query += '&minorversion=4';
        }
        else if(objectName.equalsIgnoreCase('Invoice')){
            query += ' where Id IN ';
            query += '(\''+String.join(qbIds,'\',\'')+'\')';
            query += ' STARTPOSITION ' + String.valueOf(startIndex) + ' MAXRESULTS 500';
        }
        else{
            query += ' where Active IN (true, false) AND Id IN ';
            query += '(\''+String.join(qbIds,'\',\'')+'\')';
            query += ' STARTPOSITION ' + String.valueOf(startIndex) + ' MAXRESULTS 500';
        }
         system.debug('In AQBQueryObject query before '+query);
        query = EncodingUtil.urlEncode(query, 'UTF-8').replaceAll('\\+','%20')
                                                    .replaceAll('\\*', '%2A');
         system.debug('In AQBQueryObject query after '+query);
        return query;
    }

    abstract public void save(List<Object> records, DateTime syncTime);
    

}