/**
*@Purpose: Batch class to send plaid email to customer using paubox API
*@Date: 24/09/2018
*/
global without sharing class PauBoxCustomerEmailBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{

   public List<SObject> recordList;
   public List<Paubox_Message__c> pauboxMessageList;   
   public String pauboxsMsgRecordType;
   public String templateType;
   public String objType;

   global PauBoxCustomerEmailBatch(List<SObject> recordList, String pauboxsMsgRecordType, String templateType,
                                   String objType){
      
      this.recordList = new List<SObject>();
      this.recordList.addAll(recordList); 
      this.pauboxsMsgRecordType = pauboxsMsgRecordType;
      this.templateType = templateType;  
      this.objType = objType;
      this.pauboxMessageList = new List<Paubox_Message__c>();  
   }

   global Iterable<SObject> start(Database.BatchableContext BC) {
        return recordList;
   }

   global void execute(Database.BatchableContext BC, List<SObject> scope){

      String recepientAddress = '';
      
      for(SObject record :scope){
          
          if(pauboxsMsgRecordType.equalsIgnoreCase('Funding Source Message') &&
             objType.equalsIgnoreCase('Account')){
              
              Account account = (Account)record;
              string accountID = account.ID;
              System.debug('account :::::::'+account);
              //retrieve the Plaid email address from the associated Dwolla Datasheet record
              //originally this was called from the Account record page, but we moved it to the Dwolla Datasheet record page
              recepientAddress = [select id, Plaid_Email_Address__c   from Dwolla_Datasheet__c where account__c = :accountID LIMIT 1].Plaid_Email_Address__c;
              
              //recepientAddress = account .Invoice_Email_Address__c;   
          }
            
          Paubox_Message__c pauboxMsg = sendEmailToRecipient(pauboxsMsgRecordType, record, templateType, recepientAddress,
                                                         objType, '');      
          if(pauboxMsg != null){
              
              pauboxMessageList.add(pauboxMsg);
          }
      } 
   }

   global void finish(Database.BatchableContext BC){
       
       System.debug('pauboxMessageList ::'+pauboxMessageList);
       if(!pauboxMessageList.isEmpty()){
           PauBoxEmailHelper.savepauboxMessageRecord(pauboxMessageList);
       }
   }
   
   
   
    /**
    * To send email for Customer
    **/    
    global static Paubox_Message__c sendEmailToRecipient(String pauboxsMsgRecordType, SObject objectRec, 
                                                         String templateType, String recepientAddress,
                                                         String obj, String attachmentName)
    {
        Paubox_Message__c pauboxMessage;
        PauBoxConfiguration__c config = PauBoxEmailHelper.getPauBoxDetails();
        Paubox_Message_Template__c template = PauBoxEmailHelper.getPauboxMessageTemplate(templateType);
        
        if(template != null && objectRec != null && String.isNotBlank(recepientAddress)){
            
            String senderAddress = String.isNotBlank(template.From_Address__c) ? 
                                   template.From_Address__c :
                                   config.From_Email__c;
            
            String emailHeader = PauBoxEmailHelper.getHtmlHeader();
            String emailFooter = PauBoxEmailHelper.getHtmlFooter(senderAddress);
            String emailBody = template.Template_Body__c+' <br/>';
            emailBody = emailBody.replaceAll('"','\\\\"');
            //emailBody = emailBody.replaceAll('<p[^>]*>','<div style=\\\\"padding-bottom:20px;text-align:left;\\\\">');
            //emailBody = emailBody.replaceAll('</p[^>]*>','</div>');
   
            
            if(obj.equalsIgnoreCase('Account')){
                
                Account account = (Account)objectRec; 
               
                //emailBody = emailBody + 'Please <a href=\\\"'+Label.Site_Url+'DwollaConfirmation?id='+account.Id+'\\\">click here</a> to link your bank.';
                
                if(templateType.equalsIgnoreCase('Same Day Auth')){
                    
                    Date todaysDate = Date.today();
                    String todayStr = todaysDate.month() + '-' + todaysDate.day() + '-' + todaysDate.year();
                    
                    emailBody = String.isNotBlank(emailBody) ? emailBody.replaceAll('TODAY', todayStr) : '';
                    emailBody = String.isNotBlank(emailBody) ? emailBody.replaceAll('SameDayAuth_Link__c',
                                                                    '<div style=\\\\"text-align:center;font-size:20px;\\\\"><strong>Please <a href=\\\\"'+Label.Site_Url+'fundingSource?id='+account.Id+'&customerId='+account.Dwolla_ID__c +'&accountName='+account.Name+'&authType=SameDayAuth \\\\">click here</a> to link your bank.</strong></div> ') : '';
                }else{
                    string associatedBank = account.Associated_Bank__c;
                	emailBody = String.isNotBlank(associatedBank) ?
                            emailBody.replaceAll('Account__c.Associated_Bank__c', associatedBank) :
                            emailBody;
                    emailBody = String.isNotBlank(emailBody) ? emailBody.replaceAll('Please click here to link your bank.',
                                                                    '<div style=\\\\"text-align:center;font-size:20px;\\\\"><strong>Please <a href=\\\\"'+Label.Site_Url+'DwollaConfirmation?id='+account.Id+'\\\\">click here</a> to link your bank.</strong></div> ') : '';
                }
            }
            
            String fullEmail = emailHeader + emailBody + emailFooter;
            fullEmail = fullEmail.replaceAll('>\\s*<', '><');

            System.debug('emailBody ::: \n'+fullEmail );
 
            String requestBody = '{"data": { "message": { "recipients": ["'+recepientAddress+'"], '
                                 +'"headers": { "subject": "'+template.Template_Subject__c+'", "from": "'+senderAddress+'", '
                                 +'"reply-to": "Sedera <'+senderAddress+'>" },"content": '
                                 +'{"text/html": "'+fullEmail+'"}}}}';  
            System.Debug('Request Body = ' + requestBody);   
            
            pauboxMessage = PauBoxEmailHelper.sendEmailRequest(requestBody, objectRec, config, '',attachmentName, 
                                                               pauboxsMsgRecordType, senderAddress);
         
        }
        return pauboxMessage;     
    }
}