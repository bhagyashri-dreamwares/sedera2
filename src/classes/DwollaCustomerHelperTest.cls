/*
*@Purpose : Test method for DwollaAccessTokenHelper class
*@Date : 23/01/2019
*/
@isTest
public class DwollaCustomerHelperTest {
    
    /*
* To initialise data
*/    
    @testSetup
    private static void initData(){
        Integer noOfRecords = 5;
        
        // insert Dwolla_Business_Class_Translation
        Dwolla_Business_Class_Translation__c translation = new Dwolla_Business_Class_Translation__c();
        translation.Dwolla_Business_Class_Name__c = 'Services - other - Storage';
        translation.Dwolla_Business_Class_Value__c = '100';
        insert translation;
        
        // insert Account
        List<Account> accountList = createAccount(noOfRecords);
        system.assertEquals(noOfRecords,accountList.size());
        
        //insert Dwolla sheet
        Dwolla_Datasheet__c datasheet = new Dwolla_Datasheet__c(SSN__c = '123-46-7890',
            EIN__c = '00-0000000', Controller_SSN__c = '123-46-7890',
            Controller_Date_of_Birth__c = System.today() - 3000,
            Date_of_Birth__c = System.today() - 3000,
            First_Name__c = 'test first',
            Last_Name__c = 'test last',
            Address_1__c = 'test',
            Address_2__c = 'test',
            City__c = 'test',
            State__c = 'AL',
            Postal_Code__c = '12345', 
            Email_Address__c = 'abc@abc.com', 
            Controller_First_Name__c = 'test',
            Controller_Last_Name__c = 'test', 
            Controller_Title__c = 'test', 
            Controller_Address_1__c = 'test', 
            Controller_Address_2__c = 'test', 
            Controller_Address_3__c = 'test', 
            Controller_City__c = 'test',
            Controller_State__c = 'NY', 
            Controller_Postal_Code__c = '12345', 
            Business_Classification__c = 'Services - other - Storage',
            Business_Type__c = 'LLC', 
            DBA_Name__c = 'test',
            account__c = accountList[0].Id);
        insert datasheet;
        
        
        // Insert Dwolla API Configuration Custom Setting
        DwollaAPIConfiguration__c dwollaAPIConfigurationObj = new DwollaAPIConfiguration__c();
        dwollaAPIConfigurationObj.Access_Token__c = 'MKGrMhOxWxMZt7OWrp98IFFopbQLjHrhd6Zlyj2kqK5w2dRKmO';
        dwollaAPIConfigurationObj.Access_Token_Expiry__c = DateTime.now();
        dwollaAPIConfigurationObj.Authorization_URL__c = 'https://sandbox.dwolla.com/oauth/v2/token';
        dwollaAPIConfigurationObj.Client_Key__c = 'UBUubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        dwollaAPIConfigurationObj.Is_Sandbox__c = true;
        dwollaAPIConfigurationObj.Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        dwollaAPIConfigurationObj.Sandbox_Endpoint_Url__c = 'https://api-sandbox.dwolla.com';
        dwollaAPIConfigurationObj.Secret_Key__c = '7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        
        insert dwollaAPIConfigurationObj;
    }
    
    /*
    * Positive test method for DwollaCustomerHelper 
    */
    public static testMethod void testPositiveDwollaCustomerHelper(){
        Account account = [SELECT Id, Name, QuickBooks_Customer_ID__c, QuickBooks_Customer_ID2__c
                           FROM Account
                           LIMIT 1];
        Dwolla_Datasheet__c datasheet = [SELECT Id, Name, account__c 
                                         FROM Dwolla_Datasheet__c 
                                         LIMIT 1];                   
          
        Test.setMock(HttpCalloutMock.class, new MockDwollaResponseGenerator(1));
        test.startTest();
        List<String> returnList = DwollaCustomerHelper.getCustomerFundingSource(String.valueOf(account.QuickBooks_Customer_ID__c));
        String status = DwollaCustomerHelper.getCustomerStatus(String.valueOf(account.QuickBooks_Customer_ID__c));
        DwollaCustomerHelper.CreateDwollaCustomer(datasheet.account__c);
        List<String> emptystring = new List<String>(); 
        system.assertNotEquals(emptystring, returnList);
        test.stopTest();
        system.assertEquals('verified', status);
    }
    
    
    public static testMethod void testPositiveDwollaCustomerHelper2(){
       
        Dwolla_Datasheet__c datasheet = [SELECT Id, Name, account__c 
                                         FROM Dwolla_Datasheet__c 
                                         LIMIT 1];                   
          
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        test.startTest();
        DwollaDatasheetCalloutHelper.isPositiveTest = true;
        DwollaCustomerHelper.CreateDwollaCustomer(datasheet.account__c);
        
        Account account = [SELECT Id, Name, Dwolla_ID__c, Dwolla_Beneficial_Owner_ID__c
                           FROM Account
                           WHERE Id = :datasheet.account__c];
        DwollaCustomerHelper.updateCustomer(account.Dwolla_ID__c, account.Id);
        DwollaCustomerHelper.updateRetryCustomer(account.Dwolla_ID__c, account.Id);
        test.stopTest();
        system.assertEquals('32131316', account.Dwolla_Beneficial_Owner_ID__c);
    }
    
    /*
    * Negative test method for DwollaCustomerHelper 
    */    
    public static testMethod void testNegDwollaCustomerHelper(){
        Account account = [SELECT Id, Name, QuickBooks_Customer_ID__c, QuickBooks_Customer_ID2__c
                           FROM Account
                           LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new MockDwollaResponseGenerator(0));
        test.startTest();
        List<String> returnList = DwollaCustomerHelper.getCustomerFundingSource(String.valueOf(account.QuickBooks_Customer_ID__c));
        List<String> emptystring = new List<String>(); 
        system.assertEquals(emptystring, returnList);
        test.stopTest();
    }
    
    /*
    * Method to create Account Record. 
    */    
    private static List<Account> createAccount(Integer noOfRecords){
        List<Account> accountList = new List<Account>();
        for(Integer index = 0; index < noOfRecords; index++){
            Account account = new Account();
            account.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
            account.Name = 'test Account';
            account.QuickBooks_Customer_ID__c = 'IN'+index;
            account.QuickBooks_Customer_ID2__c = 'DE'+index;
            accountList.add(account);
        }
        INSERT accountList;
        return accountList;
    }
    
}