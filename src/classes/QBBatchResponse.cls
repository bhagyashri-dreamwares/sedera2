/*
 *@Purpose    :
 *@Author     : Dreamwares
 *@Created Date : 19/07/2018
 */
public with sharing class QBBatchResponse {
  public List<QBBatchResponseObject> BatchItemResponse;
  public String time_qb;
  public QBBatchResponse(){
    BatchItemResponse = new List<QBBatchResponseObject>();
  }
}