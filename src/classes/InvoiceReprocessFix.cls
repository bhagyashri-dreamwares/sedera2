global class InvoiceReprocessFix implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful
{
    //Keep up with created invoices
    //Map of Employer Id to Invoice ID
    global final Map<ID,Invoice__c> accountInvoiceMap = new Map<ID,Invoice__c>();

    //Keep up with invoice lines
    //Map of Invoice Id to List of Maps of Product ID to Invoice Line Item ID
    global final Map<ID,Map<ID,Invoice_Line_Item__c>> finalInvoiceLines = new Map<ID,Map<ID,Invoice_Line_Item__c>>();
    
    //Keep up with existing Invoice Adjustments that need to be updated
    global final List<Invoice_Adjustment__c> invAdjustList = new List<Invoice_Adjustment__c>();
        
    public class InvoiceDataWrapper
    {
        public List<Invoice__c> invoices {get; set;}
        public List<Invoice_Line_Item__c> invLines {get; set;}
    }

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        Date dtf;
        if(system.today().day() == 1)
        {
            dtf = date.today();
        }
        else
        {
            dtf = date.today().addMonths(1).toStartOfMonth();
        }
                
        return Database.getQueryLocator([SELECT id,
                                             product2Id,
                                             Product2.name,
                                             Product2.productcode,
                                             product2.price__c,
                                             Employer_Account_ID__c,    
                                             AccountId,
                                             Account.Account_Employer_Name__c,
                                             Account.Health_Care_Sharing__c,
                                             Account.Dependent_Status__c,
                                             Account.Subscription_Status__c,
                                             Account.Primary_Age__c
                                             FROM Asset
                                             WHERE (Account.Subscription_Status__c='Pending Start Date' or Account.Subscription_Status__c='Active') and Account.enrollment_date__c<=:dtf
                                             ]);
    }

    public void execute(Database.BatchableContext context, List<Asset> scope)
    {
        Set<ID> accountSet = new Set<ID>();
        Set<ID> assetIds = new Set<ID>();
        
        for(Asset ast : scope)
        {
            accountSet.add(ast.Account.Account_Employer_Name__c);
            assetIds.add(ast.id);
        }
        
        Map<Id, Id> assetProductIdToSederaProductIdMap = getSederaProduct(scope);
        System.debug('Execute assetProductIdToSederaProductIdMap  :::'+assetProductIdToSederaProductIdMap.size()); 
        
        if(accountInvoiceMap.size() == 0)
        {
            List<Invoice__c> invoicesToFixCopy = [SELECT ID, Account__r.id, Invoice_Date__c, Submission_Status__c FROM Invoice__c WHERE Submission_Status__c = 'In Review'];
            for(Invoice__c inv : invoicesToFixCopy)
            {
                accountInvoiceMap.put(inv.Account__r.id,inv);
            }
            List<Invoice_Line_Item__c> invoicesLinesToDelete = [SELECT ID, Invoice__r.id, Invoice__r.Submission_Status__c FROM Invoice_Line_Item__c where Invoice__r.Submission_Status__c = 'In Review'];
			DELETE invoicesLinesToDelete; 
        }
        
        List<Sedera_Product_Report__c> sprDataToDelete = [SELECT ID FROM Sedera_Product_Report__c WHERE Invoice__r.Submission_Status__c = 'In Review' AND Sedera_Product__c IN :assetIds];
        DELETE sprDataToDelete;
 
        List<SObject> objectsToCreate = new List<SObject>();
        InvoiceDataWrapper invoiceData = buildInvoices(accountSet,scope, assetProductIdToSederaProductIdMap); System.debug('invoiceData::'+invoiceData);
        objectsToCreate.addAll(invoiceData.invoices);
        objectsToCreate.addAll(invoiceData.invLines);
        List<ID> invoicesCreated = new List<Id>();
        List<ID> invoiceLinesCreated = new List<Id>();

        List<Invoice__c> invoicesToInsert = new List<Invoice__c>();
        List<Invoice_Line_Item__c> invoiceLinesToInsert = new List<Invoice_Line_Item__c>();
        List<Invoice_Line_Item__c> invoiceLinesToUpdate = new List<Invoice_Line_Item__c>();
        if(objectsToCreate.size() > 0)
        {
            objectsToCreate.sort();
            for(SObject obj : objectsToCreate)
            {               
                //Schema.SObjectType InvoiceType = Schema.Invoice__c.getSObjectType();
                //Schema.SObjectType InvoiceLineItemType = Schema.Invoice_Line_Item__c.getSObjectType();

                /*if( obj.getSObjectType() == InvoiceType)
                {
                    invoicesToInsert.add((Invoice__c)obj);
                }
                else if(obj.getSObjectType() == InvoiceLineItemType)
                {
                   invoiceLinesToUpsert.add((Invoice_Line_Item__c)obj);
                }*/
                if(obj instanceOf Invoice__c)
                {
                    invoicesToInsert.add((Invoice__c)obj);
                }
                else if(obj instanceOf Invoice_Line_Item__c)
                {
                    if( obj.id != null)
                    {
                        invoiceLinesToUpdate.add((Invoice_Line_Item__c)obj);
                    }
                    else
                    {
                        invoiceLinesToInsert.add((Invoice_Line_Item__c)obj);
                    }

                }

            }
        }
        if(invoicesToInsert.size() > 0)
        {
            Database.SaveResult[] srList = Database.insert(invoicesToInsert,false);
            for (Database.SaveResult sr : srList)
            {
                if (sr.isSuccess())
                {
                    // Operation was successful, so get the ID of the record that was processed
                    if(String.valueof(sr.getId()).startsWith('a1dq') || String.valueof(sr.getId()).startsWith('a2s1'))
                    {
                        invoicesCreated.add(sr.getId());
                    }

                }
                else
                {
                    // Operation failed, so get all errors
                    for(Database.Error err : sr.getErrors())
                    {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Object fields that affected this error: ' + err.getFields());
                    }
                }
            }

            if(invoicesCreated.size() > 0)
            {
                for(Invoice__c inv:[SELECT id, Account__r.id, Account__c FROM Invoice__c WHERE id IN :invoicesCreated])
                {
                    accountInvoiceMap.put(inv.Account__r.id,inv);
                    
                    if(invoiceLinesToInsert.size() > 0)
                    {
                        for(Invoice_Line_Item__c invL : invoiceLinesToInsert)
                        {
                            if(invL.Account__c == inv.Account__c)
                            {
                               invL.Invoice__c = inv.id;
                            }
                        }
                    }
                }
            }
        }
        if(invoiceLinesToInsert.size() > 0)
        {
            Database.SaveResult[] srList = Database.insert(invoiceLinesToInsert,false);
            for (Database.SaveResult sr : srList)
            {
                if (sr.isSuccess())
                {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted object. Object ID: ' + sr.getId());
                    if(String.valueof(sr.getId()).startsWith('a1eq') || String.valueof(sr.getId()).startsWith('a2r1'))
                    {
                        invoiceLinesCreated.add(sr.getId());
                    }

                }
                else
                {
                    // Operation failed, so get all errors
                    for(Database.Error err : sr.getErrors())
                    {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Object fields that affected this error: ' + err.getFields());
                    }
                }
            }
                        
            if(invoiceLinesCreated.size() > 0)
            {
                for(Invoice_Line_Item__c invL:[SELECT id, Product__c, Quantity__c, Account__c FROM Invoice_Line_Item__c WHERE id IN :invoiceLinesCreated])
                {
                    Map<ID,Invoice_Line_Item__c> invoiceLineMap = finalInvoiceLines.get(invL.Account__c);
                                            
                    if(invoiceLineMap == null)
                    {
                        finalInvoiceLines.put(invL.Account__c, new Map<ID,Invoice_Line_Item__c>{invL.Product__c => invL});
                    }
                    else
                    {                
                     invoiceLineMap.put(invL.Product__c,invL);
                                  
                    }
                }
            }
        }
        if(invoiceLinesToUpdate.size() > 0)
        {
            Database.SaveResult[] srList = Database.update(invoiceLinesToUpdate,false);
            for (Database.SaveResult sr : srList)
            {
                if (sr.isSuccess())
                {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted object. Object ID: ' + sr.getId());
                    if(String.valueof(sr.getId()).startsWith('a1eq') || String.valueof(sr.getId()).startsWith('a2r1'))
                    {
                        invoiceLinesCreated.add(sr.getId());
                    }
                }
                else
                {
                    // Operation failed, so get all errors
                    for(Database.Error err : sr.getErrors())
                    {
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Object fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        
        List<Sedera_Product_Report__c> sprListToInsert = new List<Sedera_Product_Report__c>();
        for(Asset ast : scope)
        {
            Sedera_Product_Report__c spr = new Sedera_Product_Report__c();
            spr.Sedera_Product__c = ast.id;
            spr.Member_Account__c = ast.AccountId;
            spr.Employer_Account__c = ast.Account.Account_Employer_Name__c;
            spr.Amount__c = ast.product2.price__c;
            spr.Date_Captured__c = System.Today();
            spr.Subscription_Status__c = ast.Account.Subscription_Status__c;
            spr.Health_Care_Sharing__c = ast.Account.Health_Care_Sharing__c;
            spr.Dependent_Status__c = ast.Account.Dependent_Status__c;
            spr.Primary_Age__c = ast.Account.Primary_Age__c;
            spr.ProductName__c = ast.Product2.name;
            if(accountInvoiceMap.get(ast.Employer_Account_ID__c) != null)
            	spr.Invoice__c = accountInvoiceMap.get(ast.Employer_Account_ID__c).id;          
            
            sprListToInsert.add(spr);
        }
        
        if(sprListToInsert.size() > 0)
        {
            insert sprListToInsert;

        }
    }

    public void finish(Database.BatchableContext context)
    {
        if(system.today().day() == 1 || Test.isRunningTest())
        {
            Set<ID> invoiceIds = new Set<ID>();
            List<Invoice__c> existingInvoices = new List<Invoice__c>();
			for(Invoice__c inv:[SELECT id FROM Invoice__c WHERE Submission_Status__c = 'In Review'])
            {
                invoiceIds.add(inv.id);
                existingInvoices.add(inv);
            }
            
            if(invoiceIds.size() > 0)
            {
                List<Invoice_Adjustment__c> invAdjustListToUpdate = new List<Invoice_Adjustment__c>();
                List<Invoice_Adjustment__c> invAdjustList = new List<Invoice_Adjustment__c>();
                invAdjustList = [SELECT Employer_Account__r.id, 
                                        Invoice__r.id
                                        FROM Invoice_Adjustment__c
                                        WHERE Invoice__c IN :invoiceIds];
                if(invAdjustList.size() > 0)
                {
                    for(Invoice_Adjustment__c invAdj : invAdjustList)
                    {
                        if(accountInvoiceMap.get(invAdj.Employer_Account__r.id) != null)
                        {
                            invAdj.Invoice__c = accountInvoiceMap.get(invAdj.Employer_Account__r.id).id;
                            invAdj.Reprocessed__c = TRUE;
                            invAdjustListToUpdate.add(invAdj);
                        }
                    }
                }
                
                if(invAdjustListToUpdate.size() > 0 )
                    UPDATE invAdjustListToUpdate;
                	DELETE existingInvoices;
            }
            
            /*if(!accountInvoiceMap.isEmpty())
            {
                
                Set<Id> invoiceIdSet = new Set<Id>();
                for(Id accountId :accountInvoiceMap.keySet()){
                    invoiceIdSet.add(accountInvoiceMap.get(accountId).Id);
                }
                
                if(!invoiceIdSet.isEmpty()){
                    QBInvoicePushBatch qbInvoiceBatch = new QBInvoicePushBatch(invoiceIdSet);
                    Database.executeBatch(qbInvoiceBatch,10);
                    Database.executeBatch(new InvoiceEmailBatch(invoiceIdSet),5);                 
                }
            }*/
        }

    }

    public InvoiceDataWrapper buildInvoices(Set<ID> accountIds, List<Asset> assetList, Map<Id, Id> assetProductIdToSederaProductIdMap)
    {
        InvoiceDataWrapper invDataToCreate = new InvoiceDataWrapper();
        invDataToCreate.invoices = new List<Invoice__c>();
        invDataToCreate.invLines = new List<Invoice_Line_Item__c>();
        System.debug('assetProductIdToSederaProductIdMap :::'+assetProductIdToSederaProductIdMap);
        
        for(ID accId : accountIds)
        {
            //List<Invoice_Line_Item__c> invLinesToAdd = new List<Invoice_Line_Item__c>();
            List<Invoice_Line_Item__c> finalInvLinesToAdd = new List<Invoice_Line_Item__c>();
            Set<Invoice_Line_Item__c> existingInvLinesToUpdate = new Set<Invoice_Line_Item__c>();
            for(Asset ast : assetList)
            {
                if(ast.Account.Account_Employer_Name__c == accId)
                {
                    Boolean found = false;
                    Invoice_Line_Item__c invLine = new Invoice_Line_Item__c();
                    if(finalInvLinesToAdd.size() > 0)
                    {
                        for(Invoice_Line_Item__c currentInvL : finalInvLinesToAdd)
                        {
                            if(currentInvL != NULL)
                            {
                                if(ast.Product2Id == currentInvL.Product__c)
                                {
                                   found = true;
                                   currentInvL.Quantity__c = currentInvL.Quantity__c + 1;
                                }
                            }
                        }
                        if(found == false)
                        {
                            invLine.Description__c = ast.Product2.name;
                            invLine.Quantity__c = 1;
                            invLine.Account__c = accId;
                            invLine.Price__c = ast.Product2.price__c;
                            invLine.Product__c = ast.Product2Id;
                            //invLine.QBO_Product__c = ast.Product2Id;
                            if(assetProductIdToSederaProductIdMap.containsKey(ast.Product2Id)){
                                invLine.QBO_Product__c = assetProductIdToSederaProductIdMap.get(ast.Product2Id);
                            }
                        }
					}
                    else
                    {   
                        invLine.Description__c = ast.Product2.name;
                        invLine.Quantity__c = 1;
                        invLine.Account__c = accId;
                        invLine.Price__c = ast.Product2.price__c;
                        invLine.Product__c = ast.Product2Id;
                        
                        //invLine.QBO_Product__c = ast.Product2Id;
                        if(assetProductIdToSederaProductIdMap.containsKey(ast.Product2Id)){
                            invLine.QBO_Product__c = assetProductIdToSederaProductIdMap.get(ast.Product2Id);
                        }
                    }
                    
                    if(existingInvLinesToUpdate.size() > 0 && found == false)
                    {
                        for(Invoice_Line_Item__c createdInvL : existingInvLinesToUpdate)
                        {
                            if(createdInvL != NULL)
                            {
                                System.debug('createdInvL = ' + createdInvL+' invLine ='+ invLine);
                                //System.debug('invLine = ' + invLine);
                                if(createdInvL.Product__c == invLine.Product__c &&
                                   createdInvL.Account__c == invLine.Account__c)
                                {
                                    found = true;
                                    createdInvL.Quantity__c = createdInvL.Quantity__c + invLine.Quantity__c;
                                }
                            }
                        }
                    }
                    
                    if(found == false)
                    {   
                        Map<ID,Invoice_Line_Item__c> existingInvoiceLineMap = finalInvoiceLines.get(accId);
                       
                        if(existingInvoiceLineMap != null)
                        {	
                            Invoice_Line_Item__c existingInvoiceLine = existingInvoiceLineMap.get(invLine.product__c);
                         	
							if(existingInvoiceLine != null)
                            {	
                                found = true;
                                existingInvoiceLine.Quantity__c = existingInvoiceLine.Quantity__c + invLine.Quantity__c;
                                existingInvLinesToUpdate.add(existingInvoiceLine);
                            }
                        }
                    }

                    if(found == false)
                    {                                                  
                        if(assetProductIdToSederaProductIdMap.containsKey(ast.Product2Id)){
                            invLine.QBO_Product__c = assetProductIdToSederaProductIdMap.get(ast.Product2Id);
                        }
                        finalInvLinesToAdd.add(invLine);
                    }
                }
			}
            
            Invoice__c invoice;
                        
            if( accountInvoiceMap.get(accId) != null )
            {
                invoice = accountInvoiceMap.get(accId);
                if(finalInvLinesToAdd.size() > 0)
                {
                    for(Invoice_Line_Item__c invLine : finalInvLinesToAdd)
                    {
                        invLine.Invoice__c = invoice.id;
                    }
                }
            }
            else
            {

                // Trick to create a unique id to generate the master/detail relationship between Invoice and Invoice Line Items
                Integer len = 10;
                Blob blobKey = crypto.generateAesKey(128);
                String key = EncodingUtil.convertToHex(blobKey);
                String uId = key.substring(0,len);
                //Invoice__c invoiceRef = new Invoice__c(Helper_Id__c = uId);

                if(finalInvLinesToAdd.size() > 0)
                {
                    for(Invoice_Line_Item__c invLine : finalInvLinesToAdd)
                    {
                        //if(invLine.Invoice__c == NULL)
                            //invLine.Invoice__r = invoiceRef;
                            //
                        if(invLine != NULL)
                            invLine.Helper_Id__c = uid;

                    }
                }
                invoice = new Invoice__c();
                invoice.Helper_Id__c = uId;
                invoice.Account__c = accId;
                invoice.Invoice_Date__c = system.today();
                if(system.today().day() == 1)
                {
                    invoice.Invoice_Due_Date__c = system.today();
                    invoice.Submission_Status__c  = 'Approved';
                }
                else
                {
                    invoice.Invoice_Due_Date__c = system.today().addMonths(1).toStartOfMonth();
                    invoice.Submission_Status__c  = 'In Review';
                }
                invDataToCreate.invoices.add(invoice);
            }
          
            invDataToCreate.invLines.addAll(finalInvLinesToAdd);
            invDataToCreate.invLines.addAll(existingInvLinesToUpdate);
        }

        return invDataToCreate;
    }
    
    private static List<String> getProductCode(List<String> productNameSet){
        
        List<String> productCodeList = new List<String>();   
        Map<String, QuickBooksProductConfiguration__c> productMap = QuickBooksProductConfiguration__c.getAll();
        
        if(!productMap.isEmpty() && productNameSet != null){            
            
            for(String productName :productNameSet){
                
                for(QuickBooksProductConfiguration__c productConfig :productMap.values()){
                    System.debug('productConfig::'+productConfig);
                    System.debug('productName::'+productName) ;  
                    if(productName.containsIgnoreCase(productConfig.Name)){
                        
                        productCodeList.add(productConfig.Product_Code__c);
                        break;
                    }                    
                }
            }            
        } 
        return productCodeList;   
    }
    
    private static Map<String, String> getQBProducts(List<String> productNameSet){
        
        Map<String, String> productCodeToIdMap = new Map<String, String>();
        List<String> productCodeList = getProductCode(productNameSet);
        System.debug('productCodeList::'+productCodeList);
            
        if(!productCodeList.isEmpty()){
            
            try{
                for(Product2 product :[SELECT Id, Quickbooks_ID__c
                        FROM Product2
                        WHERE Quickbooks_ID__c IN :productCodeList
                        ]){
                    
                    productCodeToIdMap.put(product.Quickbooks_ID__c, product.Id);            
                }
            }catch(Exception exp){
                System.debug('Exception ::'+exp.getMessage());
            }
        }
        return productCodeToIdMap;
    }
    
    public static Map<Id, Id> getSederaProduct(List<Asset> assetList){
        
        Map<Id, Id> assetProductIdToSederaProductIdMap = new Map<Id, Id>{};
        Map<String,String> ProductIdToQBProductIdMap = new Map<String,String>();
        Map<String,String> ProductIdToNameMap = new Map<String,String>();
        Map<String, QuickBooksProductConfiguration__c> qBProductConfigurationMap = QuickBooksProductConfiguration__c.getAll();
        Map<String, String> productCodeToIdMap;
        
        for(Asset asset :assetList){
            
            ProductIdToNameMap.put(asset.Product2Id,asset.Product2.name);
        }
        
        if(!ProductIdToNameMap.isEmpty()){
            
            productCodeToIdMap = getQBProducts(ProductIdToNameMap.values());  
        }
        
        for(String productId :ProductIdToNameMap.keySet()){
            
            for(String qBProductName :qBProductConfigurationMap.keySet()){
                
                String productCode = qBProductConfigurationMap.get(qBProductName).Product_Code__c;
                
                if(ProductIdToNameMap.get(productId).containsIgnoreCase(qBProductName) 
                   && productCodeToIdMap.containsKey(productCode)){                    
                    
                    assetProductIdToSederaProductIdMap.put(productId, productCodeToIdMap.get(productCode));
                }
            }
        }
        
        return assetProductIdToSederaProductIdMap;
    }

}