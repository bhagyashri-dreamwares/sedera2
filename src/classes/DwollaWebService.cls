@RestResource(urlMapping='/Dwolla/v1/*')
global without sharing class DwollaWebService 
{    
    @HttpPost
    global static String processEvent()
    {
        RestRequest request = RestContext.request;
        Map<String, Object> data = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.tostring());
        
        Dwolla_Event__c de = new Dwolla_Event__c();
        for(String key : data.keySet()) 
        {
            switch on key {
                when '_links'{
                    Map<String, Object> links = (Map<String, Object>)data.get(key);
                    for(String key2 : links.keySet())
                    {
                        Map<String, Object> href = (Map<String, Object>)links.get(key2);
                        if(key2 == 'self')
                        {
                            de.self__c = (String)href.get('href');
                        } 
                        else if(key2 == 'resource')
                        {
                            de.resource__c = (String)href.get('href');
                        }
                        else if(key2 == 'account')
                        {
                            de.account__c = (String)href.get('href');
                        }
                    }
                }
                when 'id' {de.id__c = (String)data.get(key);}
                when 'created' {de.created__c = (datetime)json.deserialize('"'+(String)data.get(key)+'"', datetime.class);}
                when 'topic' {de.topic__c = (String)data.get(key);}
                when 'resourceId' {de.resourceId__c = (String)data.get(key);}
            }
        }
        
        System.debug('DwollaEvent = ' + de);

        //Ensure that the event hasn't already been captured in SF
        //Will raise exception and not run dwollaEventHelper if id a duplicate
        try {
            if ((CheckIfDwollaEventIDExists(de.id__c) == 0)) {
                dwollaEventHelper(de);
                insert de;
            }
   
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            FSMLogger.LogErrorMessage(e);
        }
        
        RestContext.response.statusCode = 200;
        return 'WebService Successfully Ran';
    }
    
    private static void dwollaEventHelper(Dwolla_Event__c de)
    {
        //need this delay in production, but it causes problems for test case CPU timeouts
        if (!Test.isRunningTest())
        {
            Long startingTime = System.now().getTime(); // Num milliseconds since Jan 1 1970
            Integer delayInMilliseconds = 10000; // One-second delay
            while (System.now().getTime() - startingTime < delayInMilliseconds)  {
                // Do nothing until desired delay has passed
            }
        }
        
        system.debug(de.topic__c);
        switch on de.topic__c {
            when 'customer_bank_transfer_created'{transferCreated(de);}
            when 'customer_bank_transfer_creation_failed'{}
            when 'customer_bank_transfer_completed'{transferCompleted(de);}
            when 'customer_bank_transfer_failed'{transferFailed(de);} 
            when 'customer_bank_transfer_cancelled'{transferCancelled(de);} 
            when 'customer_created' {customerCreated(de);}
            when 'customer_suspended' {customerSuspended(de);}
            when 'customer_verified' {customerVerified(de);}  
            when 'customer_funding_source_verified' {fundingSourceVerified(de);}
            when 'customer_funding_source_added' {fundingSourceAdded(de);}
            when 'customer_funding_source_removed' {fundingSourceRemoved(de);}
            when 'customer_verification_document_needed' {verificationDocumentNeeded(de);}
            when 'customer_verification_document_approved' {verificationDocumentApproved(de);}
            when 'customer_verification_document_uploaded' {verificationDocumentUploaded(de);}
            when 'customer_verification_document_failed' {verificationDocumentFailed(de);}
        }
    }
    
    private static void transferCreated(Dwolla_Event__c de)
    {
        try {
            Dwolla_Transfer__c dt = [SELECT Id, 
                                    Name, 
                                    Amount__c, 
                                    Destination_Funding_Source_ID__c,
                                    Destination_Dwolla_Funding_Source__r.Name, 
                                    Source_Funding_Source_ID__c,
                                    Source_Dwolla_Funding_Source__r.Name,
                                    Invoice__c,
                                    Invoice__r.id,
                                    Invoice__r.Name, 
                                    invoice__r.Account__c,
                                    invoice__r.Account__r.id, 
                                    invoice__r.account__r.name,
                                    Invoice__r.Quickbooks_Invoice_ID2__c, 
                                    Dwolla_Transfer_ID__c,
                                    Transfer_Submitted__c, 
                                    Transfer_Response__c,
                                    Transfer_Status__c,
                                    Transfer_Type__c,
                                    Transfer_Processed__c
                                    FROM Dwolla_Transfer__c
                                    WHERE Dwolla_Transfer_ID__c = :de.resourceId__c
                                    AND Transfer_Status__c = 'Initiated'];
           
            if(dt.Transfer_Type__c == 'T1')
            {
                PauboxEmailHelper.SendTransferCreatedEmail(dt);
            }

        }
        catch (exception e)
        {
            System.debug('Exception is:::'+e.getMessage());
            fsmlogger.logerrormessaGE(e, 'transferCreated');
        }
    }
    
    private static void transferCancelled(Dwolla_Event__c de)
    {
        try {
            Dwolla_Transfer__c dt = [SELECT Id, 
                                    Name, 
                                    Amount__c, 
                                    Destination_Funding_Source_ID__c,
                                    Destination_Dwolla_Funding_Source__r.Name, 
                                    Source_Funding_Source_ID__c,
                                    Source_Dwolla_Funding_Source__r.Name,
                                    Invoice__c,
                                    Invoice__r.id,
                                    Invoice__r.Name, 
                                    invoice__r.Account__c,
                                    invoice__r.Account__r.id,
                                    invoice__r.account__r.name,
                                    Invoice__r.Quickbooks_Invoice_ID2__c, 
                                    Dwolla_Transfer_ID__c,
                                    Transfer_Submitted__c, 
                                    Transfer_Response__c,
                                    Transfer_Status__c,
                                    Transfer_Type__c,
                                    Transfer_Processed__c
                                    FROM Dwolla_Transfer__c
                                    WHERE Dwolla_Transfer_ID__c = :de.resourceId__c];
           
                PauboxEmailHelper.SendTransferCancelledEmail(dt);
 
        }
        catch (exception e)
        {
            fsmlogger.logerrormessaGE(e, 'transferCancelled');
        }
    }
    
    private static void transferFailed(Dwolla_Event__c de)
    {
        try {
            Dwolla_Transfer__c dt = [SELECT Id, 
                                    Name, 
                                    Amount__c, 
                                    Destination_Funding_Source_ID__c,
                                    Destination_Dwolla_Funding_Source__r.Name, 
                                    Source_Funding_Source_ID__c,
                                    Source_Dwolla_Funding_Source__r.Name,
                                    Invoice__c,
                                    Invoice__r.id,
                                    Invoice__r.Name, 
                                    invoice__r.Account__c,
                                    invoice__r.Account__r.id,
                                    invoice__r.account__r.name,
                                    Invoice__r.Quickbooks_Invoice_ID2__c, 
                                    Dwolla_Transfer_ID__c,
                                    Transfer_Submitted__c, 
                                    Transfer_Response__c,
                                    Transfer_Status__c,
                                    Transfer_Type__c,
                                    Transfer_Processed__c
                                    FROM Dwolla_Transfer__c
                                    WHERE Dwolla_Transfer_ID__c = :de.resourceId__c];
           
                PauboxEmailHelper.SendTransferFailedEmail(dt);
        }
        catch (exception e)
        {
            fsmlogger.logerrormessaGE(e, 'transferFailed');
        }
    }
    
    private static void transferCompleted(Dwolla_Event__c de)
    {
    
        try{
            datetime now = datetime.now();
            Dwolla_Transfer__c dt = [SELECT Id, 
                                    Name, 
                                    Amount__c, 
                                    Destination_Funding_Source_ID__c,
                                    Destination_Dwolla_Funding_Source__r.Name, 
                                    Source_Funding_Source_ID__c,
                                    Source_Dwolla_Funding_Source__r.Name,
                                    Invoice__c,
                                    Invoice__r.id,
                                    Invoice__r.Name, 
                                    invoice__r.Account__c,
                                    invoice__r.Account__r.id,
                                    invoice__r.account__r.name,
                                    Invoice__r.Quickbooks_Invoice_ID2__c, 
                                    Dwolla_Transfer_ID__c,
                                    Transfer_Submitted__c, 
                                    Transfer_Response__c,
                                    Transfer_Status__c,
                                    Transfer_Type__c,
                                    Transfer_Processed__c
                                    FROM Dwolla_Transfer__c
                                    WHERE Dwolla_Transfer_ID__c = :de.resourceId__c
                                    AND Transfer_Status__c = 'Initiated'
                                    LIMIT 1];
                      
 
        dt.Transfer_Status__c = 'Succeeded';
        dt.Transfer_processed__c = now;

        //Check to see if this is transfer T1 or the initial transfer from customer to Dwolla balance
        //Send to DwollaTransferHelper to create and process T2
        if(dt.Transfer_Type__c == 'T1')
        {
            DwollaTransferHelper.createSubsequentTransfer(dt);
        }
        // Transfer completed is T2
        // The full transfer path has been completed
        // Send Dwolla transfer complete email and create Account Transaction record
        else if(dt.Transfer_Type__c == 'T2')
        {
            Pauboxemailhelper.SendTransferCompletedEmail(dt);
            DwollaTransferHelper.createAccountTransactions(dt);
        }
        
            update dt;
        }
        catch (Exception ex)
        {
            FSMLogger.LogErrorMessage(ex);
        }
    }
    

  
    private static void customerCreated(Dwolla_Event__c de)
    {
        system.debug('customerCreated');
        try {
            Account acct = [SELECT Id, 
                                    Name,
                                    Dwolla_Status__c
                                    FROM Account
                                    WHERE Dwolla_ID__c = :de.resourceId__c];
                                    
            PauboxEmailHelper.SendCustomerCreatedEmail(acct);
            string dwollaStatus = DwollaCustomerHelper.getCustomerStatus(de.resourceId__c);
            acct.dwolla_status__c = dwollaStatus;
            system.debug('CustomerCreated status - ' + dwollaStatus);
            update acct;
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'transferCreated');
        }
    }  
    
    private static void customerSuspended(Dwolla_Event__c de)
    {
        system.debug('customerSuspended');

        try {
            Account acct = [SELECT Id, 
                                    Name,
                                    dwolla_status__c
                                    FROM Account
                                    WHERE Dwolla_ID__c = :de.resourceId__c];
                                    
            PauboxEmailHelper.SendCustomerSuspendedEmail(acct);
            string dwollaStatus = DwollaCustomerHelper.getCustomerStatus(de.resourceId__c);
            acct.dwolla_status__c = dwollaStatus;
            system.debug('customerSuspended status - ' + dwollaStatus);
            update acct;
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'transferCreated');
        }
        
    }  
    
    private static void fundingSourceVerified(Dwolla_Event__c de)
    {
      
         // FSMLogger.loginformationalmessage('fundingsourceverified');
        
        try {
            Dwolla_Funding_Source__c dfs = [SELECT Id, 
                                    Name, 
                                    Bank_Name__c,
                                    Dwolla_Funding_Source_ID__c,
                                    Employer_Account__c
                                    FROM Dwolla_Funding_Source__c 
                                    WHERE Dwolla_Funding_Source_ID__c= :de.resourceId__c];
                                    
            PauboxEmailHelper.SendCustomerFundingSourceVerifiedEmail(dfs);

        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'fundingSourceVerified');
        }
    }
    
    private static void fundingSourceAdded(Dwolla_Event__c de)
    {
    
        try {
            Dwolla_Funding_Source__c dfs = [SELECT Id, 
                                    Name, 
                                    Bank_Name__c,
                                    Dwolla_Funding_Source_ID__c,
                                    Employer_Account__c
                                    FROM Dwolla_Funding_Source__c 
                                    WHERE Dwolla_Funding_Source_ID__c= :de.resourceId__c];
                                    
            PauboxEmailHelper.SendCustomerFundingSourceAddedEmail(dfs);

        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'fundingSourceAdded');
        }
    }
    
    private static void fundingSourceRemoved(Dwolla_Event__c de)
    {
    
        try {
            Dwolla_Funding_Source__c dfs = [SELECT Id, 
                                    Name, 
                                    Bank_Name__c,
                                    Dwolla_Funding_Source_ID__c,
                                    Employer_Account__c
                                    FROM Dwolla_Funding_Source__c 
                                    WHERE Dwolla_Funding_Source_ID__c= :de.resourceId__c];
                                    
            PauboxEmailHelper.SendCustomerFundingSourceRemovedEmail(dfs);

        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'fundingSourceAdded');
        }
    }
    
    private static void verificationDocumentNeeded(Dwolla_Event__c de)
    {
        system.debug('verificationDocumentNeeded');
        try {
            Account acct = [SELECT Id, 
                                    Name
                                    FROM Account
                                    WHERE Dwolla_ID__c = :de.resourceId__c];
                                    
            PauboxEmailHelper.SendVerificationDocumentNeededEmail(acct);
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'verificationDocumentNeeded');
        }
    }  
    
    private static void verificationDocumentUploaded(Dwolla_Event__c de)
    {
        system.debug('verificationDocumentUploaded');
        try {
                DwollaDatasheetAttachment__c ddsa = [SELECT 
                                                        Dwolla_Datasheet__r.account__r.id
                                                        FROM DwollaDatasheetAttachment__c
                                                        WHERE Dwolla_Document_Id__c = :de.resourceId__c];
                                                        
                Account acct = [SELECT 
                                ID, NAME
                                FROM account 
                                WHERE id = :ddsa.Dwolla_Datasheet__r.account__r.id];
                                    
                PauboxEmailHelper.SendVerificationDocumentUploadedEmail(acct);
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'verificationDocumentUploaded');
        }
    }
    
    private static void verificationDocumentFailed(Dwolla_Event__c de)
    {
        system.debug('verificationDocumentFailed');
        try {
                DwollaDatasheetAttachment__c ddsa = [SELECT 
                                                        Dwolla_Datasheet__r.account__r.id
                                                        FROM DwollaDatasheetAttachment__c
                                                        WHERE Dwolla_Document_Id__c = :de.resourceId__c];
                                                        
                Account acct = [SELECT 
                                ID, NAME
                                FROM account 
                                WHERE id = :ddsa.Dwolla_Datasheet__r.account__r.id];
                                    
                PauboxEmailHelper.SendVerificationDocumentFailedEmail(acct);
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'verificationDocumentFailed');
        }
    }
    
    private static void verificationDocumentApproved(Dwolla_Event__c de)
    {
        system.debug('verificationDocumentApproved');
        try {
                DwollaDatasheetAttachment__c ddsa = [SELECT 
                                                        Dwolla_Datasheet__r.account__r.id
                                                        FROM DwollaDatasheetAttachment__c
                                                        WHERE Dwolla_Document_Id__c = :de.resourceId__c];
                                                        
                Account acct = [SELECT 
                                ID, NAME
                                FROM account 
                                WHERE id = :ddsa.Dwolla_Datasheet__r.account__r.id];
                                    
                PauboxEmailHelper.SendVerificationDocumentApprovedEmail(acct);
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'verificationDocumentApproved');
        }
    }
    
    private static integer CheckIfDwollaEventIDExists(string DwollaEventID)
    {
        List<dwolla_Event__c> dwollaEvents = new List<dwolla_Event__c>();
        dwollaEvents = [select id from dwolla_event__c where id__c = :dwollaEventID];
        return dwollaEvents.size();

    }
    
    private static void customerVerified(Dwolla_Event__c de)
    {
        system.debug('customerVerified');

        try {
            string resourceID = de.resourceId__c;
            list<string> balanceFundingSources = dwollacustomerhelper.getCustomerFundingSource(resourceID);
            string balanceFundingSourceID = balanceFundingSources[0];
            balanceFundingSourceID = balanceFundingSourceID.replace('"', '');
            Account acct = [SELECT Id, 
                                    Name,
                                    dwolla_status__c
                                    FROM Account
                                    WHERE Dwolla_ID__c = :de.resourceId__c];
            
            //insert a new funding source record for the account with type balance for the new FS
            Dwolla_Funding_Source__c fs = new Dwolla_Funding_Source__c();
            fs.Dwolla_Funding_Source_ID__c = balanceFundingSourceID;
            fs.active__c = true;
            fs.Employer_Account__c = acct.id;
            fs.type__c = 'Balance';
            
            string dwollaStatus = 'verified';//DwollaCustomerHelper.getCustomerStatus(de.resourceId__c);
            acct.dwolla_status__c = dwollaStatus;
            system.debug('customerVerified status - ' + dwollaStatus);
            system.debug('about to commit');
            update acct;
            system.debug('updated acct');
            insert fs;
            system.debug('inserted fs');
            
            
        }
        catch (exception e)
        {
            fsmlogger.logerrormessage(e, 'customerVerified');
        }
    }
    

}