/**  
 * @Purpose      : Test Class for InvoiceGeneration 
 * @Created Date : 24/08/2018
 */
@isTest
public class InvoiceGenerationTest {
   
    /**
     * @Purpose: Test Invoice generation 
     */
    @isTest(SeeAllData=true)
    static void testInvoiceGeneration1() {
        
        Date dtf;
        if(system.today().day() == 1)
        {
            dtf = date.today();
        }
        else if(system.today().day() <= 16)
        {
            dtf = date.today().toStartOfMonth();
        }
        else
        {
            dtf = date.today().addMonths(1).toStartOfMonth();
        }

        Account acnt = [SELECT
                        id,	
                        MCS_Product_Code__c,
                        MCS_Product_Amount__c,
                        Account_Employer_Name__c,
                        Health_Care_Sharing__c,
                        Dependent_Status__c,
                        Subscription_Status__c,
                        Primary_Age__c,
                        Months_Enrolled__c,
                        Account_Employer_Name__r.MCS_Existing_Member_Revenue__c,
                        Account_Employer_Name__r.MCS_New_Member_Revenue__c,
                        Member_MEC_Product__c,
                        Parent_Product__c,
                        Tobacco_Use__c,
                        Liberty_Rx__c,
                        Strategic_Affiliates__c,
                        Referral_Affiliates__c,
                        Internal_Salesperson_Affiliate__c,
                        Lead_Generator_Affiliate__c,
                        Account_Employer_Name__r.Doing_Frontline_Support__c,
                        Account_Employer_Name__r.MS_Pricing__c        
             FROM Account
             WHERE (Subscription_Status__c='Pending Start Date' or Subscription_Status__c='Active')
             AND COBRA__c = FALSE
             AND Dependent_Status__c != null
             AND MCS_Product_Code__c != null
             AND MCS_Product_Amount__c != null
             AND enrollment_date__c<=:dtf
             LIMIT 1
            ];
        
        Invoice_Adjustment__c mcsNewInvAdj = new Invoice_Adjustment__c();
        mcsNewInvAdj.Employer_Account__c = acnt.Account_Employer_Name__c;
        mcsNewInvAdj.Amount__c = -200;
        mcsNewInvAdj.mcs_bucket__c  = 'MCS_new';
        mcsNewInvAdj.Description_of_Change__c = 'MCS New Adjustment';
        mcsNewInvAdj.Date_Change_Requested__c = date.parse('11/19/2018');
        mcsNewInvAdj.Invoice_Period__c = System.today().addMonths(1).toStartofMonth();
        INSERT mcsNewInvAdj;
        
        Test.startTest();
        List<ID> accountIds = new List<ID>();
        ID sampleId = null;
        InvoiceGeneration obj = new InvoiceGeneration(accountIds,sampleId);
        DataBase.executeBatch(obj);
        Test.stopTest();
        
        List<Invoice__c> invoices = [SELECT Id, Name FROM Invoice__c WHERE CreatedDate = THIS_MONTH];
        System.assert(invoices.size() > 1);
        List<Invoice_Line_Item__c> invoiceLineItems = [SELECT Id, Name FROM Invoice_Line_Item__c WHERE CreatedDate = TODAY];
        System.assert(invoiceLineItems.size() > 1);
        
        //List<Sedera_Product_Report__c> sederaProducts = [SELECT Id, Name FROM Sedera_Product_Report__c WHERE CreatedDate = TODAY];
        //System.assert(sederaProducts.size() > 1);
    }
    
    @isTest(SeeAllData=true)
    static void testInvoiceGeneration2() {
        
        Date dtf;
        if(system.today().day() == 1)
        {
            dtf = date.today();
        }
        else if(system.today().day() < 21)
        {
            dtf = date.today().toStartOfMonth();
        }
        else
        {
            dtf = date.today().addMonths(1).toStartOfMonth();
        }

        Account acnt = [SELECT
                        id,
                        MCS_Product_Code__c,
                        MCS_Product_Amount__c,
                        Account_Employer_Name__c,
                        Health_Care_Sharing__c,
                        Dependent_Status__c,
                        Subscription_Status__c,
                        Primary_Age__c,
                        Months_Enrolled__c,
                        Account_Employer_Name__r.MCS_Existing_Member_Revenue__c,
                        Account_Employer_Name__r.MCS_New_Member_Revenue__c,
                        Member_MEC_Product__c,
                        Parent_Product__c,
                        Tobacco_Use__c,
                        Liberty_Rx__c,
                        Strategic_Affiliates__c,
                        Referral_Affiliates__c,
                        Internal_Salesperson_Affiliate__c,
                        Lead_Generator_Affiliate__c,
                        Account_Employer_Name__r.Doing_Frontline_Support__c,
                        Account_Employer_Name__r.MS_Pricing__c  
             FROM Account
             WHERE (Subscription_Status__c='Pending Start Date' or Subscription_Status__c='Active')
             AND COBRA__c = FALSE
             AND Dependent_Status__c != null
             AND MCS_Product_Code__c != null
             AND MCS_Product_Amount__c != null
             AND enrollment_date__c<=:dtf
             LIMIT 1
            ];
  
        Test.startTest();
        List<ID> accountIds = new List<ID>();
        accountIds.add(acnt.id);
        ID sampleId = null;
        InvoiceGeneration obj = new InvoiceGeneration(accountIds,sampleId);
        DataBase.executeBatch(obj);
        Test.stopTest();
        
        List<Invoice__c> invoices = [SELECT Id, Name FROM Invoice__c WHERE CreatedDate = THIS_MONTH];
        System.assert(invoices.size() >= 1);
        List<Invoice_Line_Item__c> invoiceLineItems = [SELECT Id, Name FROM Invoice_Line_Item__c WHERE CreatedDate = TODAY];
        System.assert(invoiceLineItems.size() >= 1);
        
        //List<Sedera_Product_Report__c> sederaProducts = [SELECT Id, Name FROM Sedera_Product_Report__c WHERE CreatedDate = TODAY];
        //System.assert(sederaProducts.size() >= 1);
    }
    
    @isTest(SeeAllData=true)
    static void testReprocessSingleInvoice() {
        
        Date dtf;
        if(system.today().day() == 1)
        {
            dtf = date.today();
        }
        else if(system.today().day() < 21)
        {
            dtf = date.today().toStartOfMonth();
        }
        else
        {
            dtf = date.today().addMonths(1).toStartOfMonth();
        }
		
        Invoice__c inv = [SELECT ID, Account__r.id FROM Invoice__c WHERE Invoice_Due_Date__c = :dtf LIMIT 1];
        ID accId = inv.Account__r.id;
        Account acnt = [SELECT
                        id,
                        MCS_Product_Code__c,
                        MCS_Product_Amount__c,
                        Account_Employer_Name__c,
                        Health_Care_Sharing__c,
                        Dependent_Status__c,
                        Subscription_Status__c,
                        Primary_Age__c,
                        Months_Enrolled__c,
                        Account_Employer_Name__r.MCS_Existing_Member_Revenue__c,
                        Account_Employer_Name__r.MCS_New_Member_Revenue__c,
                        Account_Employer_Name__r.id,
                        Member_MEC_Product__c,
                        Parent_Product__c,
                        Tobacco_Use__c,
                        Liberty_Rx__c,
                        Strategic_Affiliates__c,
                        Referral_Affiliates__c,
                        Internal_Salesperson_Affiliate__c,
                        Lead_Generator_Affiliate__c,
                        Account_Employer_Name__r.Doing_Frontline_Support__c,
                        Account_Employer_Name__r.MS_Pricing__c  
             FROM Account
             WHERE (Subscription_Status__c='Pending Start Date' or Subscription_Status__c='Active')
             AND COBRA__c = FALSE
             AND Dependent_Status__c != null
             AND MCS_Product_Code__c != null
             AND MCS_Product_Amount__c != null
             AND enrollment_date__c<=:dtf
             AND Account_Employer_Name__r.id = :accId
             LIMIT 1
            ];
		List<ID> acctIds = new List<ID>();
        acctIds.add(acnt.id);
        system.debug('accId = ' + accId);
  		system.debug('inv.id = ' + inv.id);
        
        Test.startTest();
        ReprocessSingleInvoice.executeBatchForReprocess(acnt.id,inv.id);
        Test.stopTest();
        
    }
    
    @isTest(SeeAllData=true)
    static void testBatchPreCheckInvoicedata() {

        Sedera_Product_Report__c spr = [SELECT ID, Employer_Account__r.id, Invoice__r.id FROM Sedera_Product_Report__c WHERE Invoice__r.Submission_Status__c = 'In Review' LIMIT 1];
        List<ID> accountIds = new List<ID>();
        accountIds.add(spr.Employer_Account__r.id);
        ID sampleId = spr.Invoice__r.id;
        
        List<Sedera_Product_Report__c> sprList = [SELECT ID, Invoice__r.Invoice_Due_Date__c,Employer_Account__r.id FROM Sedera_Product_Report__c WHERE Invoice__r.id = :sampleId];
        
        Test.startTest();
        BatchPreCheckInvoicedata obj = new BatchPreCheckInvoicedata(accountIds,sampleId);
        DataBase.executeBatch(obj);
        obj.execute(null, sprList);
        Test.stopTest();
        
    }
}