public class QBInvoicePull extends AQBQueryObject{
     
    public QBInvoicePull(){
        parserImpl = new QBInvoiceParser();
    }
    
    public override List<Object> getObjectRecords(QBQueryResponseWrapper responseDTO){
        return responseDTO.QueryResponse.Invoice;
    }  
        
    public override void save(List<Object> records, DateTime syncTime){
        
    }
}