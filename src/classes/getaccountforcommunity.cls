public without sharing class getaccountforcommunity
{
     @AuraEnabled
     public Account x;

     @AuraEnabled
     public list<account> acc;
      
     @AuraEnabled
     public integer offst;
      
     @AuraEnabled
     public integer total;
      
     @AuraEnabled
     public boolean hasprev;
      
     @AuraEnabled
     public boolean hasnext;
 
     private static integer pagesize=8;
     private static integer offset;
      
     @AuraEnabled
     public static getaccountforcommunity getMemberAccountsfromsalesforce(boolean next,boolean prev,decimal off,id accid){
        offset = (integer)off;
         if(test.isrunningtest())
             pagesize=1;
        list<account> li = new list<account>();
        integer listlength = [Select count() from account where Account_Employer_Name__c=:accid ];
        if(!schema.sobjecttype.Account.isaccessible()){
            li = new list<account>();
        }else{
         if(next==false && prev==false){
         li = [Select id,Subscription_Status__c ,Name,Enrollment_Date__c,Dependent_Status__c from Account where Account_Employer_Name__c=:accid Order by name ASC LIMIT :pagesize OFFSET :offset];
         }else if(next==true && (offset+pagesize)<=listlength){
              offset=offset+pagesize;
          li = [Select id,Subscription_Status__c,Name,Enrollment_Date__c,Dependent_Status__c from Account where Account_Employer_Name__c=:accid  Order by name ASC lIMIT :pagesize OFFSET :offset];
         }else if(prev==true && offset>0){
          offset=offset-pagesize;
          li = [Select id,Subscription_Status__c,Name,Enrollment_Date__c,Dependent_Status__c from Account where Account_Employer_Name__c=:accid Order by name ASC  LIMIT :pagesize OFFSET :offset];
         }
         }
        getaccountforcommunity pg = new getaccountforcommunity();
       pg.acc = li;
        pg.offst = offset;
        pg.hasprev = hasprev(offset);   
        pg.hasnext = hasnxt(offset,listlength,pagesize);
             
            return pg;
         }
          
         private static boolean hasprev(integer off){
            if(off>0)
                return false;
            return true; 
        }
        private static boolean hasnxt(integer off,integer li,integer ps){
        if(off+ps<li)
            return false;
        return true;
    } 
    
    
    @Auraenabled
public static getaccountforcommunity getAccountfromsalesforce(id accountid)
{
List<Account> acc=[SELECT Name,Contract_Status__c,Start_Date__c,Renewal_Date__c,website,phone,Account_MEC_Administrator__c,Sedera_Rate_Structure__c,BillingAddress,Billingstreet,Billingcity,Billingstate,Billingcountry,ShippingAddress,Shippingstreet,shippingcity,shippingstate,shippingcountry FROM Account where id=:Accountid];
getaccountforcommunity y=new getaccountforcommunity();
y.x=acc[0];
system.debug('=========='+y);
// <div class="slds-align--absolute-center"><ui:button label="Edit" press="{!c.edit}"/></div><Br></Br><Br></Br>-->

return y;
}

@AuraEnabled
public static void setAccountinsalesforce(Account xA)
{
system.debug('====ffffffffffff======'+xA);
update xA;

}

 @Auraenabled
public static id getTopicidfromsalesforce()

{

List<Topic> t=[select id from Topic where name='My Resources' limit 1];


return t[0].id;
}

@AuraEnabled
public static List<Account> getAccountforAccountList()
{
List<User> u=[SELECT contactid FROM User where id=:userinfo.getuserid() limit 1];
Account a=new Account();

if(u[0].contactid!=null)
{
List<contact> c=[select Accountid,Account.name,Account.Start_Date__c,Account.NumberOfEmployees,Account.website,Account.phone,Account.Renewal_Date__c from contact where id=:u[0].contactid];

a.id=c[0].Accountid;
a.name=c[0].Account.name;
a.NumberOfEmployees=c[0].Account.NumberOfEmployees;
a.Start_Date__c=c[0].Account.Start_Date__c;
a.website=c[0].Account.website;
a.phone=c[0].Account.phone;
a.Renewal_Date__c=c[0].Account.Renewal_Date__c;
}
else
{
Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();

List<Account> c=[select id,Renewal_date__c,name,Start_Date__c,NumberOfEmployees,Website,phone from Account where recordtypeid=:devRecordTypeId  and createdbyid=:userinfo.getuserid() limit 1];

a.id=c[0].id;
a.name=c[0].name;
a.NumberOfEmployees=c[0].NumberOfEmployees;
a.Start_Date__c=c[0].Start_Date__c;
a.website=c[0].website;
a.phone=c[0].phone;
a.Renewal_Date__c=c[0].Renewal_Date__c;

}
List<Account> acclist=new list<Account>();
acclist.add(a);
return acclist;

}





}