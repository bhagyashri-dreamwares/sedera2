/*
* @PURPOSE				:	APEX HELPER CLASS FOR HorizonApplicationController APEX CLASS
* @AUTHOR				:	DWS
* @CREATED DATE			:	11 APR 2019
* @LAST UPDATED DATE	:	DATE			CHANGED BY		CHANGES 	
* 							11 APR 2019 	KULDIP			INITIAL CHANGES
*/ 
public class HorizonApplicationHelper {
    
    /*
    * @PURPOSE		:	GET CURRENT LOGGED-IN USER INFORMATION
    * @PARAM		:	
    * @RETURN		:	USER RECORD
    */
    public static ResponseDTO getUserInformation(){
        User userInfo = [SELECT Id, Name, TimeZoneSidKey, Username, Alias, Country, Email, FirstName, LastName, IsActive, IsPortalEnabled 
                         FROM User 
                         WHERE Id =: userInfo.getUserId()];
        System.debug('userInfo :: '+userInfo);
        if(userInfo != NULL) {
            System.debug('userInfo :: '+userInfo);
            return new ResponseDTO(TRUE, 'Success', userInfo);
        } else {
            return new ResponseDTO(false, 'Error', NULL);
        }
    }
    
    /*
    * @PURPOSE		:	GET CURRENT Bank_Data_User__c RECORD INFORMATION
    * @PARAM		:	RECORDID
    * @RETURN		:	USER RECORD
    */
    public static ResponseDTO getCurrenBankDataUserRec(String recordIdStr){
        List<String> bankDataUserFields =  getFields('Bank_Data_Users__c');
        
        String recordQueryStr = 'SELECT  '+String.join(bankDataUserFields, ', ')+' FROM Bank_Data_Users__c WHERE Id =: recordIdStr LIMIT 1';
         
        Bank_Data_Users__c bankDataUserRec = database.query(recordQueryStr);
        
        if(bankDataUserRec != NULL) {
            return new ResponseDTO(TRUE, 'Success', bankDataUserRec);
        } else {
            return new ResponseDTO(false, 'Error', NULL);
        }
    }
        
    /*
    @   fetch Field Set
    */
    public static List<String>  getFields(String objectNameStr) {
        List<String> sObjectFieldList = new List<String>(Schema.getGlobalDescribe().get(''+objectNameStr).getDescribe().fields.getMap().keyset());
        return sObjectFieldList;
    }
    
    // RESPONSE DTO
    public class ResponseDTO {
        @AuraEnabled 
        public boolean isSuccess;
        @AuraEnabled 
        public String messageStr ;
        @AuraEnabled 
        public Object data;
        public ResponseDTO(boolean isSuccess, String messageStr, Object data) {
            this.isSuccess = isSuccess;
            this.messageStr = messageStr;
            this.data = data;
        }   
    }
}