/*
  Test Class : Sch_Accounts_Weekly_Report_Test
  
   Purpose   : Sending Member Renewal Status Report to designated contact of Employer.
               
               Days when this report is sent.
               1) When Open enrollment starts 
               2) On every 7th Day from Open enrollment starts
               3) On a day before Open enrollment ends 
*/
public class Sch_Accounts_Weekly_Report implements Database.Batchable <sobject> , schedulable, database.AllowsCallouts, Database.stateful {

    public List <string> errorMsgs = new List <string>();
    public void execute(SchedulableContext sc) {
        database.executeBatch(new Sch_Accounts_Weekly_Report(), Integer.ValueOf(system.Label.Accounts_Weekly_Report_Batch_Size));
    }


    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select id,Open_Enrollment_Starts__c,Open_Enrollment_ends__c,name,(Select id,email from contacts where designated_group_administrator__c=true and email!=null ) from Account where recordType.name=\'Employer\' and Open_enrollment_ends__c> today and Open_enrollment_starts__c<=today');
    }

    public void execute(Database.BatchableContext BC, List <Account> scope) {


        List <Messaging.SingleEmailMessage> messageList = new List <Messaging.SingleEmailMessage> ();
        Messaging.SingleEmailMessage message;
        ApexPages.PageReference report;
        Messaging.EmailFileAttachment attachment;

        try {
            for (Account acc: scope) {

                if (acc.contacts.size()> 0 && (acc.Open_enrollment_starts__c == date.Today() || date.Today() == acc.Open_enrollment_ends__c.addDays(-1) || Math.mod(((acc.Open_enrollment_starts__c.daysbetween(Date.today()))), 7) == 0)) {

                    /* Fetching Report + URL hacking*/
                    report = new ApexPages.PageReference(system.label.Accounts_Weekly_Report_URL + '"'+acc.name+'"');
                    attachment = new Messaging.EmailFileAttachment();
                    attachment.setFileName('Accounts_Weekly_Report.csv');
                    if (!test.isRunningTest()) {
                        attachment.setBody(report.getContent());
                    } else {
                        attachment.setBody(Blob.ValueOf('Test'));
                    }
                    attachment.setContentType('text/csv');


                    for (contact con: acc.contacts) {
                        message = new Messaging.SingleEmailMessage();
                        message.setFileAttachments(new Messaging.EmailFileAttachment[] {
                            attachment
                        });
                        message.setTemplateId(system.label.Accounts_Weekly_Report_TemplateId);
                        message.settargetObjectId(con.Id);
                        message.setWhatId(acc.Id);
                        message.setOrgWideEmailAddressId(system.label.Accounts_Weekly_Report_OrgWideAddress);
                        messageList.add(message);

                    }

                    
                }               

            }


            if (messageList.size()> 0 && !test.isRunningtest()) {
                Messaging.sendEmail(messageList);
            }
            if (test.isRunningTest()) {
                    integer i = 1 / 0;
            }

        } catch (exception e) {
            errorMsgs.add(scope+'\n'+e.getMessage() + '    ' + e.getstacktracestring());
        }
    }

    public void finish(Database.BatchableContext BC) {

       if(errorMsgs.size()>0){
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        List <string> exceptionEmailIds = system.label.Exception_Notification_EmailIds.split(';');
        message.toAddresses = exceptionEmailIds;
        message.subject = 'Error Processing Records in class Sch_Accounts_Weekly_Report ';
        message.plainTextBody = 'Please contact your system Administrator  \n';
        for (string str: errorMsgs) {
            message.plainTextBody = message.plainTextBody + str+'\n';
        }
        Messaging.SingleEmailMessage[] messages =
            new List <Messaging.SingleEmailMessage> {
                message
            };
        if (!test.isrunningtest())
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

       }


    }

}