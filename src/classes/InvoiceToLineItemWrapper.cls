public class InvoiceToLineItemWrapper {
  public Invoice__c invoice;
    public List<Invoice_Line_Item__c> invoiceLineItems;
    public List<Invoice__c> invoiceItems;
    
    public InvoiceToLineItemWrapper(){
        invoiceLineItems = new List<Invoice_Line_Item__c>();
    }
    
    public InvoiceToLineItemWrapper( Invoice__c invoice, List<Invoice_Line_Item__c> invoiceLineItems ){
        this();
        this.invoice = invoice;
        this.invoiceLineItems = invoiceLineItems;
        
    }
    
}