@isTest
public class PlaidWebServiceTest{
    
    @testSetup
    public static void initData(){
    
        Id recoredTyId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account accountrecord = new Account(Name = 'testAccound', RecordTypeId = recoredTyId,
                                            Dwolla_ID__c='74d9b82a-2a54-4160-9f86-c718b8155b34',
                                            Invoice_Email_Address__c = 'test@test.com',
                                            Plaid_Access_Token__c = '23132131');
        
        Insert accountrecord;
        
        
        
        PlaidAPIConfiguration__c plaidApiRecord = new PlaidAPIConfiguration__c(Client_Key__c = '5b6c8018a06f890011076a0a',
                                                                               Endpoint__c = 'https://sandbox.plaid.com', 
                                                                               Is_Sandbox__c=true,
                                                                               Plaid_Link__c='https://cs21.salesforce.com/servlet/servlet.ExternalRedirect?url=https%3A%2F%2Fcdn.plaid.com%2Flink%2Fv2%2Fstable%2Flink-initialize.js',
                                                                               Public_Key__c='694fb48cf78d39ec9ae2069e4379eb',
                                                                               Secret_Key__c='ca1a06681cecdd1c65af35b9da79eb',
                                                                               Valid_Bank_Account_Type__c='');
        insert plaidApiRecord;
        
        DwollaAPIConfiguration__c dwollaApiRecord = new DwollaAPIConfiguration__c(Access_Token__c = '5DeH2odJRoqJA21i6GH8BHeS0cEOJR0JaMgG3d32Aa6xeZ5MfV',
                                                                                  Access_Token_Expiry__c = system.today(), 
                                                                                  Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token',
                                                                                  Client_Key__c = 'UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp',
                                                                                  Is_Sandbox__c=true,
                                                                                  Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize',
                                                                                  Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com',
                                                                                  Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150');
        insert dwollaApiRecord;
    }
    
    public static testMethod void testPostMethod(){
        
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c
                                 FROM Account
                                 LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerTestMock());
        Test.startTest();
        PlaidWebService.webhookResponse webhookResponse = new PlaidWebService.webhookResponse();
        webhookResponse.webhook_type = 'AUTH';
        webhookResponse.webhook_code = 'AUTOMATICALLY_VERIFIED';
        webhookResponse.item_id = '455454';
        webhookResponse.account_id = 'NvgWqbKaVVSeEqpVdgZMi73RkPVEraUWvvbarz';
        String responseJSON = JSON.serialize(webhookResponse);        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/PlaidWebService';  
        req.addParameter('id', ''+accountRecord.Id);
        req.httpMethod = 'Post';        
        req.requestBody = Blob.valueof(responseJSON);

        RestContext.request = req;
        RestContext.response = res;

        PlaidWebService.postPlaidWebService();
        FundingSourceControllerTestMock.flow = 1;
        PlaidWebService.postPlaidWebService();
        FundingSourceControllerTestMock.flow = 2;
        PlaidWebService.postPlaidWebService();
        
        Test.stopTest(); 
        Account updatedAccountRecord = [SELECT Id, Name, Plaid_Account_Status__c 
                                         FROM Account
                                         LIMIT 1];
        
        System.assertEquals('Verified', updatedAccountRecord.Plaid_Account_Status__c);
    }
    
     public static testMethod void testpostMethodNegative(){
        
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c
                                 FROM Account
                                 LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerTestMock());
        Test.startTest();
        PlaidWebService.webhookResponse webhookResponse = new PlaidWebService.webhookResponse();
        webhookResponse.webhook_type = 'AUTH';
        webhookResponse.webhook_code = 'UnVERIFIED';
        webhookResponse.item_id = '455454';
        webhookResponse.account_id = 'NvgWqbKaVVSeEqpVdgZMi73RkPVEraUWvvbarz';
        String responseJSON = JSON.serialize(webhookResponse);        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/PlaidWebService';  
        req.addParameter('id', ''+accountRecord.Id);
        req.httpMethod = 'Post';        
        req.requestBody = Blob.valueof(responseJSON);

        RestContext.request = req;
        RestContext.response = res;

        PlaidWebService.postPlaidWebService();
        
        Test.stopTest(); 
        Account updatedAccountRecord = [SELECT Id, Name, Plaid_Account_Status__c 
                                         FROM Account
                                         LIMIT 1];
        
        System.assertEquals('UnVerified', updatedAccountRecord.Plaid_Account_Status__c);        
    }
    
    public static testMethod void testNonParameterPostMethod(){
        
        Account accountRecord = [SELECT Id, Name, Plaid_Access_Token__c
                                 FROM Account
                                 LIMIT 1];
       
        
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerTestMock());
        Test.startTest();
        PlaidWebService.webhookResponse webhookResponse = new PlaidWebService.webhookResponse();
        webhookResponse.webhook_type = 'AUTH';
        webhookResponse.webhook_code = 'AUTOMATICALLY_VERIFIED';
        webhookResponse.item_id = '455454';
        webhookResponse.account_id = 'NvgWqbKaVVSeEqpVdgZMi73RkPVEraUWvvbarz';
        String responseJSON = JSON.serialize(webhookResponse);        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/PlaidWebService'; 
        req.httpMethod = 'Post';        
        req.requestBody = Blob.valueof(responseJSON);

        RestContext.request = req;
        RestContext.response = res;

        PlaidWebService.postPlaidWebService();
        
        Test.stopTest();        
    }
    
     public static testMethod void testNonAccessPostMethod(){
        
        Account accountRecord = [SELECT Id, Name, Dwolla_ID__c
                                 FROM Account
                                 LIMIT 1];
         accountRecord.Plaid_Access_Token__c = '';
        update accountRecord;
        Test.setMock(HttpCalloutMock.class, new FundingSourceControllerTestMock());
        Test.startTest();
        PlaidWebService.webhookResponse webhookResponse = new PlaidWebService.webhookResponse();
        webhookResponse.webhook_type = 'AUTH';
        webhookResponse.webhook_code = 'AUTOMATICALLY_VERIFIED';
        webhookResponse.item_id = '455454';
        webhookResponse.account_id = 'NvgWqbKaVVSeEqpVdgZMi73RkPVEraUWvvbarz';
        String responseJSON = JSON.serialize(webhookResponse);        
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/PlaidWebService'; 
        req.httpMethod = 'Post';        
        req.addParameter('id', ''+accountRecord.Id);
        req.requestBody = Blob.valueof(responseJSON);

        RestContext.request = req;
        RestContext.response = res;

        PlaidWebService.postPlaidWebService();
        FundingSourceHandler.updateDwollaCustomSetting(DwollaAPIConfiguration__c.getOrgDefaults(), new List<Log__c>());
        FundingSourceHandler.getPublicToken('321313', PlaidAPIConfiguration__c.getOrgDefaults(), 
                        new List<Dwolla_Error_Response__c>(), new List<Log__c>());
        Test.stopTest();        
    }
}