public class SederaReceiptWalletController_AWS {


    public transient string pdf {
        get;
        set;
    }
    public string CustomMessage {
        get;
        set;
    }
    public string ext {
        get;
        set;
    }
    public string ContentType {
        get;
        set;
    }
    public Id RecordId {
        get;
        set;
    }
    public Boolean IsDownload {
        get;
        set;
    }
    public string SelectedValue {
        get;
        set;
    }
    public string ReceiptWalletUrl {
        get;
        set;
    }

    public SederaReceiptWalletController_AWS(ApexPages.StandardController stdController) {
        RecordId = stdController.getRecord().Id;
        ReceiptWalletUrl ='';
        IsDownload = false;
        CustomMessage ='Processing';        
        ext = 'pdf';
          if (RecordId.getSobjectType() + '' == 'Wallet__c') {
           List <Wallet__c> wl = [Select Wallet_File_URL__c from Wallet__c where id =: RecordId];
            if (Wl.size() > 0 && wl[0].Wallet_File_URL__c != null){
              ext=wl[0].Wallet_File_URL__c.substringAfterLast('.');
              ext=ext.tolowercase();
              CallAWS();
              }
            else{
              CustomMessage ='There isn\'t any record linked to this record in AWS';
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'There isn\'t any file linked to this record in AWS');
              ApexPages.addMessage(myMsg);
             }
          }else{
          List <Receipts__c> Rc = [Select Receipt_Link__c from Receipts__c where id =: RecordId];
            if (Rc.size() > 0 && Rc[0].Receipt_Link__c != null){
              ext=Rc[0].Receipt_Link__c.substringAfterLast('.');
              ext=ext.tolowercase();
              CallAWS();
              }
            else{
              CustomMessage ='There isn\'t any file linked to this record in AWS';
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'There isn\'t any file linked to this record in AWS');
              ApexPages.addMessage(myMsg);
             }
         }
         
            
    }
    
    public pagereference CallAWS() {

        try {
            String ReceiptWalletUrl = '';
            ContentType = '';

            if (RecordId.getSobjectType() + '' == 'Wallet__c') {
                List <Wallet__c> wl = [Select Wallet_File_URL__c from Wallet__c where id =: RecordId];
                if (Wl.size() > 0 && wl[0].Wallet_File_URL__c != null)
                    ReceiptWalletUrl = wl[0].Wallet_File_URL__c;
                
                    if (ReceiptWalletUrl.substringAfterLast('.').tolowercase()== 'pdf')
                        ContentType = 'application/pdf';
                    else {
                        ContentType = 'image/png';
                    }
                
            } else {
                List < Receipts__c > Rc = [Select Receipt_Link__c from Receipts__c where id =: RecordId];
                if (Rc.size() > 0 && Rc[0].Receipt_Link__c != null)
                    ReceiptWalletUrl = Rc[0].Receipt_Link__c;
               
                    if (ReceiptWalletUrl.substringAfterLast('.').tolowercase()== 'pdf')
                        ContentType = 'application/pdf';
                    else {
                        ContentType = 'image/png';
                    }
               
            }

            if (ReceiptWalletUrl != '') {
                String formattedDateString = Datetime.now().formatgmt('yyyyMMdd\'T\'HHmmss\'Z\'');
                /*****  Canonical Request   **********/
                Blob mac = Crypto.generatedigest('SHA256', blob.valueof(''));
                String HashedPayload = EncodingUtil.converttohex(mac).tolowercase();
                String Curl='';
                If(ReceiptWalletUrl .split('sedera-application-data').size()>1)
                Curl=ReceiptWalletUrl .split('sedera-application-data')[1];
                else
                Curl='/'+ReceiptWalletUrl ;
                String CanonicalReq = 'GET\n' + Curl + '\n' +
                    '\n' + 'content-type:application/octet-stream\n' +
                    'host:sedera-application-data.s3-us-east-2.amazonaws.com\n' +
                    'x-amz-content-sha256:' + HashedPayload + '\n' +
                    'x-amz-date:' + formattedDateString + '\n\n' +
                    'content-type;host;x-amz-content-sha256;x-amz-date\n' +
                    HashedPayload;
                system.debug('CanonicalReq ' + CanonicalReq);

                /*********************/

                /*****  String To Sign  **********/


                String StringToSign = 'AWS4-HMAC-SHA256\n' + formattedDateString + '\n' + Datetime.now().formatgmt('yyyyMMdd') + '/us-east-2/s3/aws4_request\n' + EncodingUtil.converttohex(Crypto.generatedigest('SHA256', Blob.valueOf(CanonicalReq)));

                blob signingKey = Crypto.generateMac('hmacSHA256', Blob.valueOf('aws4_request'), Crypto.generateMac('hmacSHA256', Blob.valueOf('s3'), Crypto.generateMac('hmacSHA256', Blob.valueOf('us-east-2'), Crypto.generateMac('hmacSHA256', blob.valueof(Datetime.now().formatgmt('yyyyMMdd')), blob.valueOf('AWS4' + system.label.AWS_SecretKey)))));

                system.debug('stringtosign ' + stringtosign);

                /*********************/

                /************* Signature  ***********/
                String signed = EncodingUtil.convertToHex(Crypto.generateMac('hmacSHA256', Blob.valueOf(stringToSign), signingKey));
                system.debug('Signature ' + signed);
                /***********************************/



                /************  Request  **************/
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint('https://sedera-application-data.s3-us-east-2.amazonaws.com' + Curl);
                req.setHeader('Host', 'sedera-application-data.s3-us-east-2.amazonaws.com');
                req.setHeader('Content-Type', 'application/octet-stream');
                req.setHeader('X-Amz-Date', formattedDateString);
                req.setHeader('accept', '*/*');
                String authHeader = 'AWS4-HMAC-SHA256 Credential=' + system.label.AWS_AccessKey + '/' + Datetime.now().formatgmt('yyyyMMdd') + '/us-east-2/s3/aws4_request, SignedHeaders=content-type;host;x-amz-content-sha256;x-amz-date,Signature=' + signed;
                req.setHeader('Authorization', authHeader);
                req.setHeader('x-amz-content-sha256', HashedPayload);
                system.debug(signed);
                Http http = new Http();
                HTTPResponse res ;
                If(!test.isrunningtest()){
                    res= http.send(req);
                }else{
                    res=new HttpResponse();
                    res.setStatusCode(400);
                    res.setStatus('Test');
                    res.setBody('{"Message":"test"}');
                }
                
                System.debug('*Resp:' + String.ValueOF(res.getBody()));
                System.debug('RESPONSE STRING: ' + res.toString());
                System.debug('RESPONSE STATUS: ' + res.getStatus());
                System.debug('STATUS_CODE: ' + res.getStatusCode());
                /*************************************/
                if (res.getStatusCode() != 200 || test.isrunningtest()) {
                    CustomMessage = res.getBody();
                    if(CustomMessage .contains('The specified key does not exist.'))
                    CustomMessage='Cannot find the file in AWS. Possible causes could be as follows:<br/> 1) Incorrect URL provided in Wallet or Receipt link.<br/>2) The file is no longer available in AWS.';
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, CustomMessage);
                    ApexPages.addMessage(myMsg);
                } else{
                    pdf = EncodingUtil.base64encode(res.getbodyasblob()) ;                                   
                    
                    CustomMessage ='Loaded from AWS.Now rendering on VF so please wait as this might take few seconds, if nothing renders or loads endlessly, please report this to your system administrator '; 

                 }                  
                if(test.isRunningtest()){
                    integer i=9/0;
                }
            }
            return null;
        }
        Catch(Exception e) {

            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);

            return null;
        }



    }


}