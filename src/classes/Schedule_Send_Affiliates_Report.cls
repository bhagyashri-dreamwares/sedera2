public class Schedule_Send_Affiliates_Report implements Database.Batchable <String>,schedulable,Database.AllowsCallouts {
    
    
    public Boolean hasError;
    public Iterable<string> start(Database.BatchableContext BC) {
        
        hasError=false;
        
        List<string> AffliateRecTpes=system.label.Affilate_Reports_RecTypes.split(',');
        
         if(test.isRunningTest())
           return new list<string>{'test'};
           
         if(AffliateRecTpes!=null)
           return AffliateRecTpes;
         else
           return null;      
    }

    public void execute(Database.BatchableContext BC, List <string> scope) {

    
     try{
     
       PageReference ref = Page.AffiliatesReport;
        ref.getParameters().put('AffiliateRecordTypeName', scope[0]);
        Blob b;
         
         if(!test.isRunningtest())        
            b = ref.getContent();
         else
            b = blob.valueOF('test');
        

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName(scope[0]+'.xls');
        efa.setBody(b);

        list <string> Emailids = system.label.Affiliate_Reports_Emails.split(';');
        
        String addresses;
        email.setSubject('Monthly '+scope[0]+' Affiliate Reports');
        email.setToAddresses(EmailIds);
        email.setPlainTextBody('Please find attached, the monthly '+scope[0]+' report. In case of discrepancy, check the records and manually run the reports.');
        email.setFileAttachments(new Messaging.EmailFileAttachment[] {
            efa
        });
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            email
        });
        
        if(test.isRunningTest()){
          integer i=1/0;
           
        }
      }
      catch(Exception e){
      
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject('Error running '+scope[0]+' Affiliate Report in Production');
        email.setToAddresses(system.label.Affiliate_Reports_Emails.split(';'));
        email.setPlainTextBody('Please take appropriate action as there was some error processing Affiliate reports in production today .\n\n'+e.getMessage());
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            email
        });
        
      }  

    }
    public void execute(SchedulableContext sc) {

        database.executeBatch(new Schedule_Send_Affiliates_Report(),1);

        
    }
    
    public void finish(Database.BatchableContext BC) {
      
   
    
      
    }



}