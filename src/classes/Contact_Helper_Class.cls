/*
Testclass: Account_Trigger_test
*/
public class Contact_Helper_Class {

   /*** Assigning recordType based on Account's recordType ***/
   
    public static void defaultRecordType(List<contact> conList){
    
         for(contact con:conList){                     
             con.recordTypeId=con.ConRecordTypeId__c;                        
          }    
    }
    
    
     //Format all mobile numbers on change
    //Twilio and Teledoc need this specific format
    public static void formatContactNumber(List <Contact> conList, Map <id, Contact> conOldMap) {
    
        String num = '';
    
        for (contact c: conList) {
            num = '';
            if (c.home_phone__c != null && (conOldMap == null || (c.home_phone__c != conOldMap.get(c.Id).home_phone__c))) {
                num = formatcontactnumber.format(c.home_phone__c);
                If(num == 'Not a valid contact number')
                c.adderror('Not a valid contact number');
                Else
                c.home_phone__c = num;
            }
            if (c.mobile_phone__c != null && (conOldMap == null || (c.mobile_phone__c != conOldMap.get(c.Id).mobile_phone__c))) {
                num = formatcontactnumber.format(c.mobile_phone__c);
                If(num == 'Not a valid contact number')
                c.adderror('Not a valid contact number');
                Else
                c.mobile_phone__c = num;
            }
            if (c.Work_Phone__c != null && (conOldMap == null || (c.Work_Phone__c != conOldMap.get(c.Id).Work_Phone__c))) {
                num = formatcontactnumber.format(c.Work_Phone__c);
                If(num == 'Not a valid contact number')
                c.adderror('Not a valid contact number');
                Else
                c.Work_Phone__c = num;
            }


        }


    }
   
    // S2S connection with Knew Health requires to send Member name(Id) so that it could be linked with appropriate member in Salesforce
    public static void S2SmemberAccLinking(List <Contact> conList) {

        for (contact c: conList) {

            if (c.Partner_member_name__c != null) {

                c.AccountId = c.Partner_member_name__c;

            }

        }

    }


    // S2S connection with Knew Health - Updating Primary Contact's Id and recordMemberId(autonumber) on Mem acc 
    public static void S2SPrimaryContactIdOnmemAcc(Map<id,contact> conNewMap) {

        List<Account> accList=new List<Account>();
        Set<id> accIds=new Set<id>();
        for(contact con:conNewMap.values()){
          accIds.add(con.Accountid);
        }
        
        for (Account acc: [select id,(select id,createdById,Record_Member_id__c from contacts  order by createdDate limit 1) from Account where id in:accIds]) {

            if (acc.contacts.size()>0 && (acc.contacts[0].createdById == system.label.S2S_Connection_UserId || test.isRunningTest()) && conNewMap.containsKey(acc.contacts[0].id)) {
                acc.Primary_contact_id__c=acc.contacts[0].Record_Member_id__c; // AutoNumber
                acc.Primary_contactid__c=acc.contacts[0].Id;
                accList.add(acc);
            }

        }
        
        if(accList.size()>0){
          UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
          UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
          Update accList;
          UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
          UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 0;
        }

    }
 
     /* Create proprof user if the contact is of affiliate type and parent affiliate acc is approved */
    public static void proProfUserCreation(List<contact> conList){
    
        set<Id> accIds=new Set<Id>();
        set<Id> affIds=new set<id>();
        
       // Id rtconaff = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Affiliate').getRecordTypeId();

         for(contact con:conList){                     
             
             if(con.recordTypeId==system.label.recordTypeId_AffCon){
               accIds.add(con.AccountId);
             }
              
          }
          
          for(Account acc:[select id from Account where recordType.name='Affiliate' and id in:accIds and Affiliate_status__c='Approved'])    
           {
             affIds.add(acc.id);
           }   
    
          if(affIds.size()>0){
             system.enqueueJob(new ProProfsQueueable_CreateUser(affIds));
          }  
    }
    
    
    /*** Default value(Self)- relationShip To Affiliate for converted contact ***/
    
    public static void convertedConDefaultValues(List<contact> conList){
       
       for(Contact con:conList){
           if(con.isConvertedContact__c){
            con.relationShip_To_Affiliate__c='Self';
          }
       }
    
    }
    
    /*** Send Agreements Once Affiliate training is completed ***/
  /*  public static void sendAdobeAgreement(List<Contact> conList,Map<id,Contact> conOldMap){
        //Id Agid;
        for(Contact con:conList){
           if(con.recordTypeId==system.label.RecordTypeId_AffCon && con.Training_status__c=='Training Completed- Passed' && (conOldMap.get(con.Id).Training_status__c=='Training Sent' || conOldMap.get(con.Id).Training_status__c=='Training Completed- Failed')){
             //Invoking Adobe 
             // Make sure to check auto send in Agreement Template
          //   Agid=echosign_dev1.AgreementTemplateService.load(System.Label.AffiliateAgreementTempId,con.Id);       
            // con.AffiliateAgreementId__c=AgId;
             con.Agreement_status__c='Agreement Sent'; 
           }        
        }    
    }
  */

}