/**
 * Purpose : test class for QBCustomerPull Class
 * 
*/
@isTest
public class QuickbookDebugServicesTest {    
    /*
     * Purpose : method to test upsert exception 
    */
    @isTest
    static void testUpsert(){        
        
        // Create two accounts, one of which is missing a required field
        Account[] accts = new List<Account>{
            createAccount('acc 1'),
            new Account()};
        Database.upsertResult[] srList = Database.upsert(accts, false);
                
        QuickbookDebugServices.trackUpsertResult('Account', srList);
                
        List<QB_Sync_Errors__c> qbDebugList = [ SELECT Id from QB_Sync_Errors__c];
        
        //System.assertEquals(1, qbDebugList.size());
    } 
    
    /*
     * Purpose : method to test update exception
    */
    @isTest
    static void testUpdate(){        
        
        Account acc = createAccount('testAccount');
        insert acc;
        
        List<Account> acct = [ SELECT Id from Account WHERE name = 'testAccount' LIMIT 1];
        acct[0].name = null;
        
        
        Database.SaveResult[] srList = Database.update(acct, false);
                
        QuickbookDebugServices.trackUpdateResult('Account', srList);
        List<QB_Sync_Errors__c> qbDebugList = [ SELECT Id from QB_Sync_Errors__c];
        
        System.assertEquals(1, qbDebugList.size());
        //QuickbookDebugServices.trackPLIUpsertResult('Invoice_Line_Item__c', srList1, invoiceLineItems);        
    }
    
          
    /*
     * Purpose : method to test update exception
    */
    @isTest
    static void testException(){        
        
        Account acc = createAccount('testAccount');
        insert acc;
        
        List<Account> acct = [ SELECT Id from Account WHERE name = 'testAccount' LIMIT 1];
        acct[0].name = null;
        try{
            update(acct);
        }catch(Exception e){        
            QuickbookDebugServices.trackException(e);
        }
        
        QuickbookDebugServices.trackSystemDebug('Exceptions tested');
        
        List<QB_Sync_Errors__c> qbDebugList = [ SELECT Id from QB_Sync_Errors__c];
        
        System.assertEquals(2, qbDebugList.size());
    } 
    static Account createAccount(String accName){
        /*RecordType type = [SELECT Id FROM RecordType 
                           WHERE Name = 'Account' 
                               AND sObjectType = 'Account'];*/
        Account recordAccount = new Account(//RecordType = type,
                                            Name = accName,
                                            recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
                                            QuickBooks_Customer_ID__c='1',
                                            ShippingStreet = '230 NE US Hwy 27',
                                            ShippingCity = 'Williston',
                                            ShippingState = 'Florida',
                                            ShippingCountry = 'United States');
        return recordAccount;
    }
}