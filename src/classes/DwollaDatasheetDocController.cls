/**
* @Purpose: Controller for DwollaDatasheetDoc page
*/
public class DwollaDatasheetDocController {
    
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public static Boolean isPostiveTesting = false;
    public static Boolean isTestValidToken = false;
    /**
    * To store attachment related to DataSheet
    */
    @RemoteAction
    public static Response uploadAttachment(String parameters) {
    
        System.debug('parameters' +parameters);
        Map<String, String> paramMap = (Map<String, String>)JSON.deserialize(parameters, Map<String, String>.class);

        String parentId = paramMap.containsKey('parentId')? paramMap.get('parentId') : '';
        String attachmentId = paramMap.containsKey('attachmentId')?  paramMap.get('attachmentId') : '';
        String attachmentBody = paramMap.containsKey('attachmentBody')? paramMap.get('attachmentBody'): '';
        String attachmentName = paramMap.containsKey('attachmentName')? paramMap.get('attachmentName'): '';
        String attachmentType = paramMap.containsKey('attachmentType')? paramMap.get('attachmentType'): '';
        String contentType = paramMap.containsKey('contentType')? paramMap.get('contentType'): '';
        
        if(String.isNotBlank(parentId)) {
            
            // insert DataSheetAttachment record
            
            if(String.isBlank(attachmentId)){
                Response dataSheetResp = createDwollaDatasheetAttachment(parentId, attachmentType);
                if(!dataSheetResp.isSuccess){            
                    return dataSheetResp;
                }
                parentId = ((DwollaDatasheetAttachment__c)dataSheetResp.data).Id;                
            }
            
            if(attachmentBody != null){
            
                Attachment att = getAttachment(attachmentId);
                String newBody = '';
                /*if(att.Body != null) {
                    newBody = EncodingUtil.base64Encode(att.Body);
                }
                newBody += attachmentBody;
                att.Body = EncodingUtil.base64Decode(newBody);*/
                
                att.Body = att.Body != null ? 
                           EncodingUtil.base64Decode(EncodingUtil.base64Encode(att.Body) + attachmentBody) :
                           EncodingUtil.base64Decode(attachmentBody);
                att.contentType = contentType;
                if(attachmentId == null) {
                    att.Name = attachmentName;                    
                    att.parentId = parentId;
                }
                //set attachment type in description field
                att.Description = attachmentType;
               
                upsert att;
                
                //saveDataSheetInfo(new Dwolla_Datasheet__c(Id = parentId, Dwolla_Document_Type__c = attachmentType));
                
                Response finalResponse = new Response(true, '', att.Id);
                finalResponse.recordId = parentId; 
                return finalResponse;
            } else {
                return new Response(false, 'Attachment Body is null', null);
            }
            
        } else {
            return new Response(false, 'Could not found record with given Id', null);
        }
    }
    

    private static Attachment getAttachment(String attId){
    
        List<Attachment> attachments;
        try{
            attachments = [SELECT Id, Body, Name, ParentId, ContentType
                           FROM Attachment
                           WHERE Id =: attId];
        }catch(Exception ex){
            System.debug('Exception '+ ex.getMessage());
        }

        if(attachments == null || attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }    
    
    /**
    * To create DwollaDatasheetAttachment record
    */
    private static Response createDwollaDatasheetAttachment(String dataSheetRecordId, String documentType){
    
        if(String.isNotBlank(dataSheetRecordId) && String.isNotBlank(documentType)){
            
            DwollaDatasheetAttachment__c datasheetAttachment = new DwollaDatasheetAttachment__c();
            datasheetAttachment.Dwolla_Datasheet__c = dataSheetRecordId;
            datasheetAttachment.Dwolla_Document_Type__c = documentType;
            
            try{
                
                insert datasheetAttachment;
                return new Response(true, '', datasheetAttachment);
            }catch(Exception exp){
                System.debug('Exception :::'+exp.getMessage());
                return new Response(false, 'Error while saving DataSheetAttachment Info.\n'+exp.getMessage(), null);
            }
        }
        return new Response(false, 'Please provide DataSheet record id and DocumentType.', null);    
    }
    
    /**
    * To store DataSheetAttachment record info
    */
    public static Response saveDataSheetInfo(DwollaDatasheetAttachment__c dataSheetAttachmentRecord){
        
        try{
        
            update dataSheetAttachmentRecord;
            return new Response(true, 'File successfully Uploaded at Dwolla', dataSheetAttachmentRecord);
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage());
            return new Response(false, 'Exception occurred while saving information of DataSheetAttachment record. \n'+exp.getMessage(), null);
        }
    }
    
    /**
    * To upload Document for Dwolla Customer
    */
    @RemoteAction
    public static Response createDwollaDocument(String parameters){
               
       Map<String, String> paramMap = (Map<String, String>)JSON.deserialize(parameters, Map<String, String>.class);
       String dataSheetAttachmentId = paramMap.containsKey('parentId') ? paramMap.get('parentId') : '';
       String attId = paramMap.containsKey('attachmentId') ?  paramMap.get('attachmentId') : '';
       System.debug('attId  :::::'+attId);
       
       if(string.isNotBlank(attId) && string.isNotBlank(dataSheetAttachmentId)){
       
           Attachment attachment = getAttachment(attId);
           
           Response dataSheetAttachmentRes = getDataSheetAttachmentRecord(dataSheetAttachmentId);
           
           if(!dataSheetAttachmentRes.isSuccess){
               return dataSheetAttachmentRes;
           }
           
           DwollaDatasheetAttachment__c dataSheetRecord = (DwollaDatasheetAttachment__c)dataSheetAttachmentRes.data;
           
           if(attachment != null && dataSheetRecord != null){
               
               return makeCallouts(attachment, dataSheetRecord.Dwolla_Document_Type__c, dataSheetRecord.Dwolla_Datasheet__r.Dwolla_ID__c,
                                   dataSheetRecord.Id);    
           }else{
               return attachment != null ? 
                      new Response(false, 'DataSheet record related to '+dataSheetAttachmentId+' Id not found. Please contact to admin.', null) :
                      new Response(false, 'Attachment record related to '+attId+' Id not found. Please contact to admin.', null);   
           }
       }else{
           return new Response(false, 'Please provide Proper AttachmentId and DataSheetId', null);
       }  
    }
    
    /**
    * To get DataSheet record
    */
    private static Response getDataSheetAttachmentRecord(String dataSheetAttachmentId){
        
        List<DwollaDatasheetAttachment__c> dataSheetAttachmentList = new List<DwollaDatasheetAttachment__c>();
        try{
            
            dataSheetAttachmentList = [SELECT Id, Dwolla_Datasheet__c, Dwolla_Datasheet__r.Dwolla_ID__c, Dwolla_Document_Type__c
                                       FROM DwollaDatasheetAttachment__c
                                       WHERE Id = :dataSheetAttachmentId
                                       AND Dwolla_Datasheet__r.Dwolla_ID__c != null];
            
            return !dataSheetAttachmentList.isEmpty() ? 
                   new Response(true, '', dataSheetAttachmentList[0]): 
                   new Response(false, 'DataSheetAttachment record related to '+dataSheetAttachmentId+' Id is not found.', null);
            
        }catch(Exception exp){
            System.debug('Exception :::'+exp.getMessage());
            return new Response(false, 'Error while fetching DwollaDatasheetAttachment record.\n'+exp.getMessage(), null);
        }
    }
    
    /**
    * To make API callout for Dwolla Customer Document
    */
    public static Response makeCallouts(Attachment attachment, String documentType, String customerId, String dataSheetAttachmentId){
        
        DwollaAPIConfiguration__c config;
        Boolean isTokenLive = false;
        
        DwollaDocumentCalloutHelper dwollaCalloutHelper = new DwollaDocumentCalloutHelper();        
        if(Test.isRunningTest()){
            dwollaCalloutHelper.isPostiveTesting = isPostiveTesting;
            dwollaCalloutHelper.isTestValidToken = isTestValidToken;
        }
        
        if(dwollaCalloutHelper.config.Access_Token_Expiry__c == null || dwollaCalloutHelper.config.Access_Token_Expiry__c <= DateTime.now()){
            dwollaCalloutHelper.config = dwollaCalloutHelper.getLiveToken(dwollaCalloutHelper.config);
            if(dwollaCalloutHelper.config != null){
                isTokenLive = true;
                config = dwollaCalloutHelper.config;
            }
        }else{
            config = dwollaCalloutHelper.config;
            isTokenLive = true;
        }

        if(isTokenLive){
            if(customerId != Null){
                return dwollaCalloutHelper.uploadDwollaDocument(attachment, documentType, customerId, dataSheetAttachmentId);
            }else{
                return new Response(false, 'Dwolla customer not present. Please contact to Admin.', null);
            }
        }
        return new Response(false, 'Access token is not valid. Please contact to Admin.', null);
    }
    
    
    public class Response{
    
        public boolean isSuccess;
        public String msg;
        public object data;
        public String recordId;
        
        public Response(){
        }
        
        public Response( boolean isSuccess, String msg, object data ){
            this.isSuccess = isSuccess;
            this.msg = msg;
            this.data = data;
        }       

    }
}