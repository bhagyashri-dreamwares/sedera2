/*
* To handle operation related to Dwolla when Account trigger executed
*/
global class DwollaAccountTriggerHandler{
    
    public static Boolean isFirstTime = true;
    
    /*
    * To create customer record at Dwolla
    */
    /*public static void createDwollaCustomer(List<Account> accountList, Boolean isUpdate, Map<Id,Account> accountOldMap){
        
        List<Account> validDwollaCustomerList = new List<Account>();
        Map<Id, RecordType> recordTypeMap = getRecordtype();
        
        for(Account customer : accountList){
            
            System.debug('customer :::::'+customer);
            if(recordTypeMap.containsKey(customer.recordTypeId) &&
               recordTypeMap.get(customer.recordTypeId).Name.equalsIgnoreCase('Employer') && 
               customer.Create_Dwolla_Customer__c && String.isNotBlank(customer.Invoice_Email_Address__c) && 
               ((!isUpdate) || (isUpdate && String.isBlank(customer.Dwolla_ID__c) && accountOldMap != null 
                                && accountOldMap.containsKey(customer.Id) 
                                && !accountOldMap.get(customer.Id).Invoice_Email_Address__c.equalsIgnoreCase(customer.Invoice_Email_Address__c)))){
               
                validDwollaCustomerList.add(customer);
            }
        }
        System.debug('validDwollaCustomerList :::::'+validDwollaCustomerList);
        if(!validDwollaCustomerList.isEmpty()){
            
            Database.executeBatch(new DwollaCustomerBatch(validDwollaCustomerList), 1);
        }
    }*/
    
    /*
    * To get record type
    */
    /*private static Map<Id, RecordType> getRecordtype(){
        
        Map<Id, RecordType> recordTypeMap = new Map<Id, RecordType>();
        try{
            recordTypeMap.putAll([SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account']);
            
        }catch(Exception exp){
            
            System.debug('Exception :::'+exp.getMessage());
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaAccountTriggerHandler.getRecordtype', exp, '', 
                                                                 'SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = \'Account\'', 'Error'))}); 
        }
        return recordTypeMap;
    }*/
    
    /*
    * To send plaid Link to customer
    */
    webService  static void sendPlidLinkToCustomer(List<Account> accountList, Boolean isUpdate){
        
        List<Account> validPlaidCustomerList = new List<Account>();
            
       /* for(Account customer : accountList){
            
            if(customer.Send_Plaid_Link__c && string.isNotBlank(customer.Dwolla_ID__c) && 
               string.isNotBlank(customer.Invoice_Email_Address__c) &&
               ((!isUpdate) || (isUpdate && customerOldMap != null && customerOldMap.containsKey(customer.Id)
                 && ((customerOldMap.get(customer.Id).Dwolla_ID__c != customer.Dwolla_ID__c) ||
                     (!customerOldMap.get(customer.Id).Send_Plaid_Link__c))))){
                     
                validPlaidCustomerList.add(customer);    
            }    
        }*/
        System.debug('validPlaidCustomerList ::::'+accountList);
        if(!accountList.isEmpty()){
            
            Database.executeBatch(new PauBoxCustomerEmailBatch(accountList, 'Funding Source Message', 
                                                            'Funding Source','Account'), 1);
        }
    }    
   
}