public class Case_Helper_Class2 {


    public static List <case> caselist = new list <case>();
    public static List <Account> AccList = new List <Account> ();
    public static List <Contact> ConList = new List <contact> ();


    public static Void Setting_IUA_Attributes_On_Before_Insert(List <Case> triggernew) {

        //Sets IUA for both Select and Premier Cases
        //Sets Causeinnumberofneeds for Both types
        //Sets IUAafter3 for Premier and Prime Cases 

   Id NeedsRecType= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
   Boolean ConsiderNeed=false;

 
        for (case cas:triggernew) {
            //Insert trigger will run for cases where sharable need is Equal to true
          
            If(cas.sharable_need__c && cas.IUA_Met__C==true && cas.recordTypeid==NeedsRecType)
            ConsiderNeed=true;
            else
            ConsiderNeed=false;
            if (ConsiderNeed) {
            
     
                //System.assertequals(cas.Initial_Unsharable_Amount__c,2);
                //Used Causeinnumberofneeds_date__c  field and assigned it a value of Current time so that if Any old sharable need gets off we could track which need we need to consider in numberofneeds on Account and contact

                cas.Causeinnumberofneeds_date__c = system.now();

                //For Parent Account sedera Select 
                
                if (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access') {
                    system.debug('mmmmmmmmmmmmmmmmmmmmmmmmmmm');
                    if (cas.IUA_exception__c == true) {
                        //Numberofneeds is a class I have used which calculates the number of need on Contact ,if it comes out to be less than or equal to 3 it will be considered else not
                        Integer count = Numberofneeds.forcontactAccountAddOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                        if (count <= 3) {

                            //IUA2 is set 0 and cause in number of need date equal to true as it passes the numberofneeds less than 3 criteria

                            cas.Initial_unsharable_amount2__c = 0;
                            cas.cause_in_number_of_needs__c = true;

                        } else {
                            Integer x1 = Numberofneeds.forContactAccountMinusOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                            //cause in no of needs as false and IUA2=0
                            cas.cause_in_number_of_needs__c = false;
                            cas.Initial_unsharable_amount2__c = 0;



                        }
                    } else { //For cases having IUAexception and MD consultation equal to false
                        //Numberofneeds is a class I have used which calculates the number of need on Contact ,if it comes out to be less than equal to 3 it will be considered else not
                        Integer count = Numberofneeds.forcontactAccountAddOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);


                        if (count <= 3) {

                            //Numberofneeds class is used again to check  number of needs on Account,If it comes out to be greater than 5 IUA2 will be set  even when met the criteria of less than equal to 3 needs on contact
                            //if number of needs on Account comes out to be less than or Equal to 5,IUA2 will be Equal to
                            if (NumberOfneeds.MapOfCountOfAccountAndContactId.get(cas.accountid) <= 5) {
                                If(cas.X2nd_MD_Consultation__c == true) {
                              Date dt=  date.newinstance(2018,09,01);
                                  If(cas.createddate >= dt)
                                    cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c-250;
                                  Else       
                                  cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c/2;
                                  
                                    cas.cause_in_number_of_needs__c = true;
                                }
                                else {
                                    cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c;
                                    cas.cause_in_number_of_needs__c = true;
                                }
                              
                               if(!(cas.Total_Amount_Paid_By_Patient__c >= getIua(cas)))
                                    {//System.assertequals(cas.Total_Amount_Paid_By_Patient__c,getIua(cas));
                                     //cas.Initial_unsharable_amount2__c =0;
                                     Integer x1 = Numberofneeds.forContactAccountMinusOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                                     cas.cause_in_number_of_needs__c = false;   
                                     Cas.Causeinnumberofneeds_date__c = null;                                    
                                    }

                            } else {
                                cas.Initial_unsharable_amount2__c = 0;
                            }


    
                        } else { //If number of needs on contact is greater than 3 then dec the value increased at line  number 72 and do not consider this need
                            Numberofneeds.forContactAccountMinusOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                            cas.cause_in_number_of_needs__c = false;
                            cas.Initial_unsharable_amount2__c = 0;
                        }

                    }

                }

                //For cases whose parent Account is sedera premier /prime
                 if ((cas.Sedera_Parent_Account__c == 'sedera premier' || cas.Sedera_Parent_Account__c == 'sedera prime') || test.isrunningtest()) {
                    //Number of needs class is Used to check for no of needs already linked to the Account
                    Integer count = Numberofneeds.forAccountContactAddOne(cas.Accountid, (integer) cas.numberofneedsonAccount__c, cas.contactid, (integer) cas.numberofneedsonContact__c);

                    //if no. of needs already linked is greater than 3 than set IUA after 3 equal to true else false
                    cas.cause_in_number_of_needs__c = true;
                    //In case of prime and premier need will be considered if it's sharable need is Equal to true
                    if (count> 3) {
                        cas.IUAafter3__c = true;
                    }

                }
            }




        }




    }



    public static void Setting_IUA_attributes_On_Before_Update(List <case> triggernew, Map <id,case>triggeroldmap) {
       
       Id NeedsRecType= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
       Boolean ConsiderNeedNew=false;
       Boolean ConsiderNeedOld=false;

        for (case cas:triggernew) {
           If(cas.sharable_need__C && cas.IUA_Met__C )
            ConsiderNeedNew=true;
            else
            ConsiderNeedNew=false;
            
            If(triggeroldmap.get(cas.id).sharable_need__C && triggeroldmap.get(cas.id).IUA_Met__C )
            ConsiderNeedold=true;
            else
            ConsiderNeedOld=false;
            //For sedera select parent Account
            if (cas.recordtypeid==NeedsRecType && (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access')) {
                //set IUA2 equal to 0 if the need was involved in no. of needs of Account and contact and it's IUAexception value chnged from false to true

                if (cas.cause_in_number_of_needs__c == true && ConsiderNeedOld== true && ConsiderNeedNew== false) {

                    cas.cause_in_number_of_needs__c = false;
                    cas.Initial_unsharable_amount2__c = 0;
                    numberofneeds.MapOfCountOfAccountAndContactId.put(cas.accountid, 0);
                    numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, 0);


                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event

                    Numberofneeds.addAccountid(cas.Accountid);


                } else if (cas.cause_in_number_of_needs__c == false && ConsiderNeedOld== false && ConsiderNeedNew== true) {
                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event
                    Numberofneeds.addAccountid(cas.Accountid);
                    if (cas.Causeinnumberofneeds_date__c == null) {
                        cas.Causeinnumberofneeds_date__c = system.now();
                    }

                }

                //If sharable need changes from false to true set its Causeinnumberofneeds_date__c date to current time if it is not set earlier or its sharable need is checked for the first time

                //if a need involved in number of needs on Account and contact

                //need involved has undergone a change in its IUA exception value from true to false

                if (cas.cause_in_number_of_needs__c == true && ConsiderNeedNew== true && (triggeroldmap.get(cas.id).IUA_exception__c == true && cas.IUA_exception__c == false)) {

                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event
                    Numberofneeds.addAccountid(cas.Accountid);
                }

                //set IUA2 equal to 0 if the need was involved in no. of needs of Account and contact and it's X2nd_MD_Consultation__c  value chnged from false to true

                if (cas.cause_in_number_of_needs__c == true && ConsiderNeedNew== true && (cas.X2nd_MD_Consultation__c == true && triggeroldmap.get(cas.id).X2nd_MD_Consultation__c == false)) {
                    cas.Initial_unsharable_amount2__c = triggeroldmap.get(cas.id).Initial_unsharable_amount2__c -250;
                     if(!(cas.Total_Amount_Paid_By_Patient__c >=getIua(cas)))
                        {
                        // cas.Initial_unsharable_amount2__c =0;
                         Integer x1 = Numberofneeds.forContactAccountMinusOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                         cas.cause_in_number_of_needs__c = false;   
                        }
                }

                //need involved has undergone a change in its X2nd_MD_Consultation__c  value from true to false

                if (cas.cause_in_number_of_needs__c == true && ConsiderNeedNew== true && (cas.X2nd_MD_Consultation__c == false && triggeroldmap.get(cas.id).X2nd_MD_Consultation__c == true)) {
                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event
                    cas.Initial_unsharable_amount2__c = triggeroldmap.get(cas.id).Initial_unsharable_amount2__c +250;
                  if(!(cas.Total_Amount_Paid_By_Patient__c >= getIua(cas)))
                    {
                     //cas.Initial_unsharable_amount2__c =0;
                     Integer x1 = Numberofneeds.forContactAccountMinusOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                     cas.cause_in_number_of_needs__c = false;   
                    }
                }
                if (cas.cause_in_number_of_needs__c == true && ConsiderNeedNew== true && (triggeroldmap.get(cas.id).IUA_exception__c == false && cas.IUA_exception__c == true)) {
                    cas.Initial_unsharable_amount2__c = 0;
                }



            }

            //For cases whose paren Account is Prime/premier
             if ((cas.Sedera_Parent_Account__c == 'sedera premier' || cas.Sedera_Parent_Account__c == 'sedera prime') || test.isrunningtest()) {
                //If need's sharable field is set false from true
                if (ConsiderNeedNew== false && ConsiderNeedOld== true && cas.IUAafter3__c == false) {
                    cas.cause_in_number_of_needs__c = false;

                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event
                    Numberofneeds.addAccountid(cas.Accountid);
                    numberofneeds.MapOfCountOfAccountAndContactId.put(cas.accountid, 0);
                    numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, 0);

                } else if ((ConsiderNeedNew== false && ConsiderNeedOld == true && cas.IUAafter3__c == true))

                {
                    cas.cause_in_number_of_needs__c = false;

                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event
                    Numberofneeds.addAccountid(cas.Accountid);
                    numberofneeds.MapOfCountOfAccountAndContactId.put(cas.accountid, 0);
                    numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, 0);


                } else if ((ConsiderNeedNew == true && ConsiderNeedOld == false)) {
                    //Its Accountid is set using number of needs class so that we can process all the cases linked to this Account in after Update event

                    Numberofneeds.addAccountid(cas.Accountid);
                    cas.cause_in_number_of_needs__c = true;

                    //If sharable need changes from false to true set its Causeinnumberofneeds_date__c date to current time if it is not set earlier or its sharable need is checked for the first time

                    if (cas.Causeinnumberofneeds_date__c == null) {
                        cas.Causeinnumberofneeds_date__c = system.now();
                    }
                }

            }

        }

    }




    public static void Setting_IUA_attributes_On_After_Events() {


        //SOQL Query to pull all the case records linked to Accounts(only those Accounts whose ids we collect using numberof needs class in before Update and delete events) where sharable need equal to true and yearly reset equal to true,And the cases coming will be in Ascending order of causeinnumberofneedsdate field,or in the order  they caused the no. of needs to increase(or was considered in number of needs)
        Map <id, Account> MapOfAccountsAndRelatedCases;
        if (Numberofneeds.SetofAccountids != null) {
            MapOfAccountsAndRelatedCases = new map <id, Account> ([select id, Number_of_Cases__c, Parentid, (select id,numberofneedsoncontact__c,numberofneedsonaccount__c, IUAafter3__c,iua_met__c, case_locked__c,createddate, Sharable_Need__c, cause_in_number_of_needs__c,Type_of_Need__c, ChosenonAcc__c, ContactId, Accountid, Sedera_Parent_Account__c, IUA_exception__c, X2nd_MD_Consultation__c, Causeinnumberofneeds_date__c,total_amount_paid_by_patient__c,initial_unsharable_amount2__c,Third_Party_Paid_Amount__c,third_party_payer__c from cases where Yearly_Reset_Flag__c = TRUE and sharable_need__C = true and Iua_met__c=true and recordtype.name='Need Sharing' order by Causeinnumberofneeds_date__c ASC) from Account where id in: Numberofneeds.SetofAccountids]);

        }



        if (MapOfAccountsAndRelatedCases.size()> 0) {
            for (Account acc: MapOfAccountsAndRelatedCases.values()) {
                //traversing of case records will happen and all the case records associated to concerned Accountids from start will be processed,order of processing will be ASC causeinnumberofdate__c

                for (case cas:acc.cases) {

                    //For cases where parent Account is sedera premier/prime
                    if (((cas.Sedera_Parent_Account__c == 'sedera premier' || cas.Sedera_Parent_Account__c == 'sedera prime') && !test.isrunningtest() ) && cas.sharable_need__c == true && cas.Iua_met__c==true) {
                        //number of needs class used to calculate the needs for prime and premier Accounts
                        Integer num = Numberofneeds.Account5Contact3(cas.Accountid);
                        if (numberofneeds.MapOfCountOfAccountAndContactId.containskey(cas.contactid)) {
                            numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, numberofneeds.MapOfCountOfAccountAndContactId.get(cas.contactid) + 1);
                        } else {
                            numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, 1);
                        }

                        if (num <= 3) {
                            //if less than 3 IUA after 3 as false
                            cas.IUAafter3__c = false;
                            caseList.add(cas);
                        } else {
                            //greater than 3 set true
                            cas.IUAafter3__c = true;
                            caseList.add(cas);
                        }

                    } else if (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access') {

                        if (cas.sharable_need__c == true && cas.Iua_met__C==true) {
                            //Numberofneeds class used which calculates the number of need on Contact ,if it comes out to be less than or equal to 3 it will be considered else not


                            integer num3 = Numberofneeds.Account5Contact3(cas.contactid);
                            if (num3 <= 3) {
                                //Numberofneeds class used which calculates the number of need on Account,if it comes out to be less than or equal to 5 IUA2 will be equal to IUAchosen on Account at line no 317 provided IUA_exception__C and md consultation both are false else IUA2 equal 0

                                Integer num2 = Numberofneeds.Account5Contact3(cas.Accountid);
                                if (cas.cause_in_number_of_needs__c == false) { //set cause in needs equal to true if it was not
                                    cas.cause_in_number_of_needs__c = true;
                                }

                                if (num2 <= 5) {
                                    if (cas.IUA_exception__C != true && cas.Case_Locked__C == false) {

                                        if (cas.X2nd_MD_Consultation__c == true) {
                                            Date dt=  date.newinstance(2018,09,01);
                                            If(cas.createddate >= dt)
                                             cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c-250;
                                            Else       
                                             cas.Initial_unsharable_amount2__c = cas.ChosenonAcc__c/2;
                                  
                                        } else {
                                            cas.initial_unsharable_Amount2__c = cas.ChosenonAcc__c;
                                        }
                                       if(!(cas.Total_Amount_Paid_By_Patient__c >= getIua(cas)))
                                         {
                                          //cas.Initial_unsharable_amount2__c =0;
                                          Integer x1 = Numberofneeds.forContactAccountMinusOne(cas.contactid, (integer) cas.numberofneedsoncontact__c, cas.Accountid, (integer) cas.numberofneedsonAccount__c);
                                          cas.cause_in_number_of_needs__c = false;  
                                         }
                                    }
                                } else if (cas.Case_Locked__C == false) {
                                    cas.initial_unsharable_Amount2__c = 0;
                                }

                                caseList.add(cas);

                            }
                            //if the need on the contact is coming as greater than 3 than it will not be considered as a need on contact and Account
                            else {
                                Numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, Numberofneeds.MapOfCountOfAccountAndContactId.get(cas.contactid) - 1);
                                cas.cause_in_number_of_needs__c = false;
                                if (cas.Case_Locked__C == false) {
                                    cas.initial_unsharable_Amount2__c = 0;
                                }
                                caseList.add(cas);
                            }

                        }


                    }


                }

            }

        }




    }


    public static void Setting_IUA_attributes_On_Before_Delete(List <Case> TriggerOld) {
     Id NeedsRecType= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();

        for (case cas:TriggerOld) {
            if (cas.recordtypeid==NeedsRecType && cas.cause_in_number_of_needs__c == true && (cas.Sedera_Parent_Account__c == 'sedera select' || cas.Sedera_Parent_Account__c == 'sedera access')) {
                //Its Accountid is set using number of needs class so that we can process all the other remaining cases linked to this Account in after delete event

                numberofneeds.addAccountid(cas.accountid);
                numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, 0);
                numberofneeds.MapOfCountOfAccountAndContactId.put(cas.accountid, 0);

            }
            if (cas.recordtypeid==NeedsRecType && cas.sharable_need__C == true && cas.Iua_met__C==true && (test.isrunningtest() || cas.Sedera_Parent_Account__c == 'sedera premier' || cas.Sedera_Parent_Account__c == 'sedera prime')) {
                //Its Accountid is set using number of needs class so that we can process all the other remaining cases linked to this Account in after delete event

                numberofneeds.addAccountid(cas.accountid);
                numberofneeds.MapOfCountOfAccountAndContactId.put(cas.contactid, 0);
                numberofneeds.MapOfCountOfAccountAndContactId.put(cas.accountid, 0);


            }

        }
    }




    public static void Setting_IUA_Attributes_for_Undelete(List<case> triggernew) {
       Id NeedsRecType= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();

        for (case cas:triggernew) {
            if (cas.sharable_need__C == true && cas.IUA_met__C==true && cas.recordtypeid==NeedsRecType) {
                numberofneeds.addAccountid(cas.accountid);
            }
        }
    }




    public static void Setting_Number_Of_Needs_On_Accounts_And_Contacts() {


        //Processing all the Accounts and contacts where no. of needs needs to be updated as in above code we set a number corresponding to an accountid and contactid to which cases are linked to,added 1 if any gets assosciated and subtracted 1 if any need gets away
        for (id id: Numberofneeds.MapOfCountOfAccountAndContactId.keyset()) {

            String AccOrconId = id;
            if (AccOrconId.startswith('003')) {

                Contact con;

                con = new contact(id = AccOrconId, number_of_needs__C = Numberofneeds.MapOfCountOfAccountAndContactId.get(id));
                conList.add(con);
            } else if (AccOrconId.startswith('001')) {
                Account acc;
                acc = new Account(id = AccOrconId, number_of_cases__C = Numberofneeds.MapOfCountOfAccountAndContactId.get(id));
                AccList.add(acc);
            }
        }




        if (accList.size()> 0) {
            NumberOfNeeds.clearStaticvariables();
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
            Update accList;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
            accList.clear();

        }
        if (conList.size()> 0) {
            update conList;
            conList.clear();

        }
        if (caseList.size()> 0) { //All the cases we added in after events of delete/update which needed update
            UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 1;
            update caseList;
            UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
            caseList.clear();
        }
    }



    public static void Setting_Bills_On_Change_Of_IUA(Map <Id, Case> triggeroldmap, Set <Id> CaseIds) {

        list <case> caseList = [select id, (select id from bills__r), Initial_Unsharable_Amount__c from case where id = :CaseIds];
        list <bill__c> billstoupdate = new List <bill__c> ();
        for (case cas:caseList) {
            if (cas.Initial_Unsharable_Amount__c != triggeroldmap.get(cas.id).Initial_Unsharable_Amount__c && cas.bills__r.size()> 0)
                billstoupdate.add(cas.bills__r[0]);
        }
        if (billstoupdate.size()> 0) {
            UtilityClass_For_Static_Variables.CheckRecursiveForBillTrigger = 0;
            UtilityClass_For_Static_Variables.changeinIUA = 1;
            Update billstoupdate;
        }


    }
public static decimal getIua(case cas)
{
If(cas.Type_of_Need__c!=null && cas.Type_of_Need__c.contains('Maternity')){
   If(cas.Sedera_Parent_Account__c == 'sedera select') {
   if(cas.Initial_unsharable_amount2__c==null)
   cas.Initial_unsharable_amount2__c=0;
        return 2*cas.Initial_unsharable_amount2__c;
}else
Return 5000;

}else
{
If(cas.Third_Party_Payer__c)
                  {
                  if(cas.Initial_unsharable_amount2__c==null)
   cas.Initial_unsharable_amount2__c=0;
   if(cas.Third_Party_Paid_Amount__c==null)
   cas.Third_Party_Paid_Amount__c=0;
                  
                       Return Math.max(0,cas.Initial_unsharable_amount2__c-cas.Third_Party_Paid_Amount__c);
                   }
               Else{
               if(cas.Initial_unsharable_amount2__c==null)
   cas.Initial_unsharable_amount2__c=0;
                Return cas.Initial_unsharable_amount2__c;
}


}

/*Else{

IF(cas.iuA_Exception__c)
return 0;
Else
{
if(cas.X2nd_MD_Consultation__c && cas.iuaafter3__c)
return 150;
else if(cas.X2nd_MD_Consultation__c && !cas.iuaafter3__c)
return 250;
if(!cas.X2nd_MD_Consultation__c && cas.iuaafter3__c)
return 300;
else if(!cas.X2nd_MD_Consultation__c && !cas.iuaafter3__c)
return 500;

return 0;
}
}*/


}


}