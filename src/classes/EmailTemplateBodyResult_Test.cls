@isTest(SeeAllData=true)
public class EmailTemplateBodyResult_Test {
	static testMethod void testBenefitUpdate()
    {
        Test.startTest();
        
       	String tname = 'Bill Status - Closed-IUA - Patient';
        Bill__c bill = [SELECT ID, Patient_Name__r.id FROM BILL__c LIMIT 1];
        
        EmailTemplateBodyResult.emailTemplateInput input = new EmailTemplateBodyResult.emailTemplateInput();
        input.templateName = tname;
        input.whatId = bill.id;
        input.whoId = bill.Patient_Name__r.id;
        
        EmailTemplateBodyResult.fillEmailTemplate(
            new List<EmailTemplateBodyResult.emailTemplateInput>{input}
        );
        Test.stopTest();
    }
}