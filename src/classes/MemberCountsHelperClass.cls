/**

   Active/Pending Cancellation members are counted in Total Counts of members at Affilate and Employer level

**/

public class MemberCountsHelperClass{

    private static Map <id, empDependentStatusCountClass> empDependentStatusCountMap = new Map <id, empDependentStatusCountClass> ();
    private static Map <id, Integer> AffMembersCountMap = new Map <id, Integer> ();
    

    public static void calcEmpAffActiveMembers(Map <Id, Account> NewMapofAccounts, Map <Id, Account> OldMapofAccounts) {


        List <Account> EmpAccsToUpdate = new List <Account> ();
        List <Account> AffAccsToUpdate = new List <Account> ();
        Boolean empFlag = false;
        
        if (NewMapofAccounts != null && OldMapofAccounts != null) {

            for (Account acc: NewMapofAccounts.values()) {
              
              empFlag=false;
              
              
                if (acc.recordTypeid == System.label.RecordTypeId_MemAcc) {

                    if ((OldMapofAccounts.get(acc.id).Subscription_Status__c == 'Active' || OldMapofAccounts.get(acc.id).Subscription_Status__c == 'Pending Cancellation') && (acc.Subscription_Status__c == 'Active' || acc.Subscription_Status__c == 'Pending Cancellation')) {
                        If(OldMapofAccounts.get(acc.id).Dependent_Status__c != acc.Dependent_Status__c && acc.Dependent_Status__c != null) {
                            
                            empFlag=true;
                            if (OldMapofAccounts.get(acc.id).Dependent_Status__c == null) { 

                                calculateEmpMembers(acc.Account_Employer_Name__c, acc.Dependent_Status__c, 1);
                                

                            } else {

                                calculateEmpMembers(acc.Account_Employer_Name__c, acc.Dependent_Status__c, 1);

                                calculateEmpMembers(OldMapofAccounts.get(acc.id).Account_Employer_Name__c, OldMapofAccounts.get(acc.id).Dependent_Status__c, -1);
                            }

                        }

                        else If(OldMapofAccounts.get(acc.id).Dependent_Status__c != acc.Dependent_Status__c && acc.Dependent_Status__c == null) {
                            empFlag=true;
                            calculateEmpMembers(OldMapofAccounts.get(acc.id).Account_Employer_Name__c, OldMapofAccounts.get(acc.id).Dependent_Status__c, -1);

                        }
                        
                        If(!empFlag && acc.Dependent_Status__c!=null && OldMapofAccounts.get(acc.id).Account_Employer_name__c!=acc.Account_Employer_name__c){
                            calculateEmpMembers(OldMapofAccounts.get(acc.id).Account_Employer_Name__c, acc.Dependent_Status__c, -1);
                            calculateEmpMembers(acc.Account_Employer_Name__c, acc.Dependent_Status__c, 1);
                        }
                        
                        If( OldMapofAccounts.get(acc.id).Referral_Affiliate__c!=acc.Referral_Affiliate__c){
                            
                            if(acc.Referral_Affiliate__c!=null)
                            calculateAffMembers(acc.Referral_Affiliate__c,1);
                            
                            if(OldMapofAccounts.get(acc.id).Referral_Affiliate__c!=null)
                            calculateAffMembers(OldMapofAccounts.get(acc.id).Referral_Affiliate__c,-1);
                        
                        }
                        If( OldMapofAccounts.get(acc.id).Strategic_Affiliate__c!=acc.Strategic_Affiliate__c){
                            
                            if(acc.Strategic_Affiliate__c!=null)
                            calculateAffMembers(acc.Strategic_Affiliate__c,1);
                            
                            if(OldMapofAccounts.get(acc.id).Strategic_Affiliate__c!=null)
                            calculateAffMembers(OldMapofAccounts.get(acc.id).Strategic_Affiliate__c,-1);
                        
                        }
                        If( OldMapofAccounts.get(acc.id).Lead_Generator__c!=acc.Lead_Generator__c){
                            
                            if(acc.Lead_Generator__c!=null)
                            calculateAffMembers(acc.Lead_Generator__c,1);
                            
                            if(OldMapofAccounts.get(acc.id).Lead_Generator__c!=null)
                            calculateAffMembers(OldMapofAccounts.get(acc.id).Lead_Generator__c,-1);
                            
                        }
                        If( OldMapofAccounts.get(acc.id).Internal_SalesPerson__c!=acc.Internal_SalesPerson__c){
                            
                            if(acc.Internal_SalesPerson__c!=null)
                            calculateAffMembers(acc.Internal_SalesPerson__c,1);
                            
                            if(OldMapofAccounts.get(acc.id).Internal_SalesPerson__c!=null)
                            calculateAffMembers(OldMapofAccounts.get(acc.id).Internal_SalesPerson__c,-1);
                        
                        }
                        
                        

                    } else {
                        if ((OldMapofAccounts.get(acc.id).Subscription_Status__c != 'Active' && OldMapofAccounts.get(acc.id).Subscription_Status__c != 'Pending Cancellation') && (acc.Subscription_Status__c == 'Active' || acc.Subscription_Status__c == 'Pending Cancellation')) {
                            system.debug('>>>>>Qwerty '+acc.Subscription_Status__c);
                             if (acc.Dependent_Status__c != null)
                                calculateEmpMembers(acc.Account_Employer_Name__c, acc.Dependent_Status__c, 1);
                            
                            
                            if(acc.Referral_Affiliate__c!=null)
                            calculateAffMembers(acc.Referral_Affiliate__c,1);
                            
                            if(acc.Strategic_Affiliate__c!=null)
                            calculateAffMembers(acc.Strategic_Affiliate__c,1);
                            
                            if(acc.Internal_SalesPerson__c!=null)
                            calculateAffMembers(acc.Internal_SalesPerson__c,1);
                            
                            if(acc.Lead_Generator__c!=null)
                            calculateAffMembers(acc.Lead_Generator__c,1);
                            
                            
                            


                        } else if ((OldMapofAccounts.get(acc.id).Subscription_Status__c == 'Active' || OldMapofAccounts.get(acc.id).Subscription_Status__c == 'Pending Cancellation') && (acc.Subscription_Status__c != 'Active' && acc.Subscription_Status__c != 'Pending Cancellation')) {

                            if (OldMapofAccounts.get(acc.id).Dependent_Status__c != null)
                                calculateEmpMembers(OldMapofAccounts.get(acc.id).Account_Employer_Name__c, OldMapofAccounts.get(acc.id).Dependent_Status__c, -1);

                           
                            if(acc.Referral_Affiliate__c!=null)
                            calculateAffMembers(acc.Referral_Affiliate__c,-1);
                            
                            if(acc.Strategic_Affiliate__c!=null)
                            calculateAffMembers(acc.Strategic_Affiliate__c,-1);
                            
                            if(acc.Internal_SalesPerson__c!=null)
                            calculateAffMembers(acc.Internal_SalesPerson__c,-1);
                            
                            if(acc.Lead_Generator__c!=null)
                            calculateAffMembers(acc.Lead_Generator__c,-1);
                            
                        
                        }
                    }

                }

            }
        } else if (NewMapofAccounts == null) {

            for (Account acc: OldMapofAccounts.values()) {

                if(acc.recordTypeid == System.label.RecordTypeId_MemAcc && (acc.Subscription_Status__c == 'Active' || acc.Subscription_Status__c == 'Pending Cancellation')) {
                    calculateEmpMembers(acc.Account_Employer_Name__c, acc.Dependent_Status__c, -1);
                    
                if(acc.Referral_Affiliate__c!=null)
                    calculateAffMembers(acc.Referral_Affiliate__c,-1);
                            
                if(acc.Strategic_Affiliate__c!=null)
                    calculateAffMembers(acc.Strategic_Affiliate__c,-1);
                            
                if(acc.Internal_SalesPerson__c!=null)
                    calculateAffMembers(acc.Internal_SalesPerson__c,-1);
                            
                if(acc.Lead_Generator__c!=null)
                    calculateAffMembers(acc.Lead_Generator__c,-1);
               }

            }

        } else {

            for (Account acc: NewMapofAccounts.values()) {

                if (acc.recordTypeid == System.label.RecordTypeId_MemAcc && (acc.Subscription_Status__c == 'Active' || acc.Subscription_Status__c == 'Pending Cancellation') && acc.Dependent_Status__c!=null) {
                    calculateEmpMembers(acc.Account_Employer_Name__c, acc.Dependent_Status__c, 1);
                    
                if(acc.Referral_Affiliate__c!=null)
                    calculateAffMembers(acc.Referral_Affiliate__c,1);
                            
                if(acc.Strategic_Affiliate__c!=null)
                    calculateAffMembers(acc.Strategic_Affiliate__c,1);
                            
                if(acc.Internal_SalesPerson__c!=null)
                    calculateAffMembers(acc.Internal_SalesPerson__c,1);
                            
                if(acc.Lead_Generator__c!=null)
                    calculateAffMembers(acc.Lead_Generator__c,1);
                
                }

            }

        }


        if (empDependentStatusCountMap.size()> 0) {

            for (Account acc: [select id, Number_of_EO__c, Number_of_ES__c, Number_of_EF__c, Number_of_EC__c from Account where id in: empDependentStatusCountMap.keySet()]) {
                 system.debug('Qwerty '+acc.id+'    '+acc.Number_of_EO__c+'  '+empDependentStatusCountMap.get(acc.Id).EO);
                acc.Number_of_EO__c = (acc.Number_of_EO__c==null?0:acc.Number_of_EO__c) + empDependentStatusCountMap.get(acc.Id).EO;
                acc.Number_of_EF__c = (acc.Number_of_EF__c==null?0:acc.Number_of_EF__c) + empDependentStatusCountMap.get(acc.Id).EF;
                acc.Number_of_ES__c = (acc.Number_of_ES__c==null?0:acc.Number_of_ES__c) + empDependentStatusCountMap.get(acc.Id).ES;
                acc.Number_of_EC__c = (acc.Number_of_EC__c==null?0:acc.Number_of_EC__c) + empDependentStatusCountMap.get(acc.Id).EC;
                acc.active_member_count__c = acc.Number_of_EO__c + acc.Number_of_EC__c + acc.Number_of_EF__c + acc.Number_of_ES__c;

                EmpAccsToUpdate.add(acc);
            }

            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
            Update EmpAccsToUpdate;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 0;

            
            empDependentStatusCountMap.clear();
        }
        
        if(AffMembersCountMap.size()>0){
          
             for (Account acc: [select id, Total_Member_Accounts_Sold__c from Account where id in: AffMembersCountMap.keySet()]) {
                //system.assertEquals(AffMembersCountMap.get(acc.Id),88);
                acc.Total_Member_Accounts_Sold__c = (acc.Total_Member_Accounts_Sold__c==null?0:acc.Total_Member_Accounts_Sold__c) +AffMembersCountMap.get(acc.Id);
                AffAccsToUpdate.add(acc);
            }

            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 1;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 1;
            Update AffAccsToUpdate;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
            UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB = 0;

            AffMembersCountMap.clear();
        }
          
        
        

    }


    private static void calculateEmpMembers(Id empId, string depStatus, integer num) {

        if (!empDependentStatusCountMap.containsKey(empId)) {
            empDependentStatusCountMap.put(empId, new empDependentStatusCountClass());
        }

        if (depStatus == 'EO')
           empDependentStatusCountMap.get(empId).EO = empDependentStatusCountMap.get(empId).EO + num;

        else if (depStatus == 'EF')
           empDependentStatusCountMap.get(empId).EF = empDependentStatusCountMap.get(empId).EF + num;

        else if (depStatus == 'ES')
           empDependentStatusCountMap.get(empId).ES = empDependentStatusCountMap.get(empId).ES + num;

        else if (depStatus == 'EC')
           empDependentStatusCountMap.get(empId).EC = empDependentStatusCountMap.get(empId).EC + num;
    }
    
    private static void calculateAffMembers(Id AffId, integer num) {

        if (!AffMembersCountMap.containsKey(AffId)) {
            AffMembersCountMap.put(AffId,num);
        }else{
           AffMembersCountMap.put(affId,AffMembersCountMap.get(AffId) + num);
        }
    }




    private class empDependentStatusCountClass {

        integer EO = 0;
        integer ES = 0;
        integer EC = 0;
        integer EF = 0;

    }


}