/*
 * @Purpose       : Fetch the Dwolla Transfer for the DisplayDwollaTransferController
 * @Created Date  : 26/12/2018
 */
@isTest
public class DwollaTransferListControllerTest {
    
    /*
     * @Purpose : Create data required for testing
     */
    @TestSetup
    public Static void createTestData(){ 
        
        // Insert Account
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account account = new Account(RecordtypeId = recordtypeId,
                                      Name = 'Test Account');
        insert account;	
        
        // Insert Invoice
        Invoice__c invoice = new Invoice__c(Account__c = account.Id,
                                            Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 50000);
        insert invoice;
        
        // Insert Dwolla Funding Source
        List<Dwolla_Funding_Source__c> dwollaFundingSourceList = new List<Dwolla_Funding_Source__c>();
        for(integer index = 0; index < 3; index++){
        	dwollaFundingSourceList.add(new Dwolla_Funding_Source__c(Employer_Account__c = account.id));    
        }
        insert dwollaFundingSourceList;
        
        // Insert Dwolla Transfer
        Dwolla_Transfer__c dwollaTransfer = new Dwolla_Transfer__c(Amount__c = 20,
                                                                   Invoice__c = invoice.id,
                                                                   Source_Dwolla_Funding_Source__c = dwollaFundingSourceList[0].id,
                                                                   Destination_Dwolla_Funding_Source__c = dwollaFundingSourceList[1].id,
                                                                   Transfer_Processed__c = DateTime.now(),
                                                                   Transfer_Status__c = 'Pending');
        insert dwollaTransfer;
    } 
    
   /*
    * @Purpose : To Test functionality(Positive Testing)
    */
    @isTest
    public static void functionalityPositiveTest(){   
        
        Account account = [SELECT id FROM Account LIMIT 1];
        Invoice__c invoice = [SELECT id FROM Invoice__c];
        
        // Setting the DwolloTransferList page for testing
        PageReference pageRef = Page.DwollaTransferList;
        pageRef.getParameters().put('id', account.id+'-'+invoice.id);
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        	DwollaTransferListController dwollaTransferListControllerObj = new DwollaTransferListController();
            dwollaTransferListControllerObj.fetchDwollaTransfer(invoice.id);
        Test.stopTest();
        	
        System.assertNotEquals(0, dwollaTransferListControllerObj.dwollaTransferWrapperList.size());
    }
    
   /*
    * @Purpose : To Test functionality(Negative Testing)
    */
    @isTest
    public static void functionalityNegativeTest(){ 
        
         Account account = [SELECT id FROM Account LIMIT 1];
        
        // Setting the DwolloTransferList page for testing
        PageReference pageRef = Page.DwollaTransferList;
        pageRef.getParameters().put('id', account.id); 
        Test.setCurrentPage(pageRef); 
        
        Test.startTest();
        	DwollaTransferListController dwollaTransferListControllerObj = new DwollaTransferListController();
            dwollaTransferListControllerObj.fetchDwollaTransfer('');
        Test.stopTest();
        
        System.assertEquals(null, dwollaTransferListControllerObj.dwollaTransferWrapperList); 
    }
}