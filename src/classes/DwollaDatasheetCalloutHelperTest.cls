@isTest
public class DwollaDatasheetCalloutHelperTest {

    @testSetup
    static void makeData(){
        
        Account account = new Account(Name = 'test account', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId());
        insert account;
        
        Dwolla_Datasheet__c datasheet = new Dwolla_Datasheet__c(SSN__c = '123-46-7890',
            EIN__c = '00-0000000', Controller_SSN__c = '123-46-7890',
            Controller_Date_of_Birth__c = System.today() - 3000,
            Date_of_Birth__c = System.today() - 3000,
            First_Name__c = 'test first',
            Last_Name__c = 'test last',
            Address_1__c = 'test',
            Address_2__c = 'test',
            City__c = 'test',
            State__c = 'AL',
            Postal_Code__c = '12345', 
            Email_Address__c = 'abc@abc.com', 
            Controller_First_Name__c = 'test',
            Controller_Last_Name__c = 'test', 
            Controller_Title__c = 'test', 
            Controller_Address_1__c = 'test', 
            Controller_Address_2__c = 'test', 
            Controller_Address_3__c = 'test', 
            Controller_City__c = 'test',
            Controller_State__c = 'NY', 
            Controller_Postal_Code__c = '123456', 
            Business_Classification__c = 'Services - other - Storage',
            Business_Type__c = 'LLC', DBA_Name__c = 'test');
        insert datasheet;
        
        Dwolla_Business_Class_Translation__c translationRecord = new Dwolla_Business_Class_Translation__c();
        translationRecord.Dwolla_Business_Class_Name__c = 'Services - other - Storage';
        translationRecord.Dwolla_Business_Class_Value__c = 'Value';
        
        insert TranslationRecord;
        
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today() + 600;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj;
    }

    @isTest
    static void createCustomerTest(){
        
        Dwolla_Datasheet__c datasheet = [SELECT Id, 
                Controller_Date_of_Birth__c, 
                Date_of_Birth__c,
                First_Name__c, 
                Last_Name__c, 
                Address_1__c, 
                Address_2__c, 
                City__c, 
                State__c, 
                Postal_Code__c, 
                SSN__c, 
                EIN__c,
                Email_Address__c,
                Controller_First_Name__c,
                Controller_Last_Name__c,
                Controller_Title__c,
                Controller_SSN__c,
                Controller_Address_1__c,
                Controller_Address_2__c,
                Controller_Address_3__c,
                Controller_City__c,
                Controller_State__c,
                Controller_Postal_Code__c,
                Business_Classification__c,
                Business_Type__c,
                DBA_Name__c
            FROM Dwolla_Datasheet__c 
            LIMIT 1];
        DwollaDatasheetCalloutHelper helper = new DwollaDatasheetCalloutHelper();
        Test.startTest();
        
        helper.createCustomer(datasheet, true);
        
        Test.stopTest();
    }
    
    @isTest
    static void createBeneficialOwnersTest(){
        
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        Dwolla_Datasheet__c datasheet = [SELECT Id, 
                Controller_Date_of_Birth__c, 
                Date_of_Birth__c,
                First_Name__c, 
                Last_Name__c, 
                Address_1__c, 
                Address_2__c, 
                City__c, 
                State__c, 
                Postal_Code__c, 
                SSN__c, 
                EIN__c,
                Email_Address__c,
                Controller_First_Name__c,
                Controller_Last_Name__c,
                Controller_Title__c,
                Controller_SSN__c,
                Controller_Address_1__c,
                Controller_Address_2__c,
                Controller_Address_3__c,
                Controller_City__c,
                Controller_State__c,
                Controller_Postal_Code__c,
                Business_Classification__c,
                Business_Type__c,
                DBA_Name__c
            FROM Dwolla_Datasheet__c 
            LIMIT 1];
        DwollaDatasheetCalloutHelper helper = new DwollaDatasheetCalloutHelper();
        Test.startTest();
        
        helper.createBeneficialOwners(datasheet, '123456');
        
        Test.stopTest();
    }
    
    @isTest
    static void CertifyBeneficialOwnersTest(){
        
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        Dwolla_Datasheet__c datasheet = [SELECT Id, 
                Controller_Date_of_Birth__c, 
                Date_of_Birth__c,
                First_Name__c, 
                Last_Name__c, 
                Address_1__c, 
                Address_2__c, 
                City__c, 
                State__c, 
                Postal_Code__c, 
                SSN__c, 
                EIN__c,
                Email_Address__c,
                Controller_First_Name__c,
                Controller_Last_Name__c,
                Controller_Title__c,
                Controller_SSN__c,
                Controller_Address_1__c,
                Controller_Address_2__c,
                Controller_Address_3__c,
                Controller_City__c,
                Controller_State__c,
                Controller_Postal_Code__c,
                Business_Classification__c,
                Business_Type__c,
                DBA_Name__c
            FROM Dwolla_Datasheet__c 
            LIMIT 1];
        DwollaDatasheetCalloutHelper helper = new DwollaDatasheetCalloutHelper();
        Test.startTest();
        
        helper.CertifyBeneficialOwners(datasheet, 'test');
        
        Test.stopTest();
    }
    
    @isTest
    static void getLiveTokenTest(){
        
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        DwollaDatasheetCalloutHelper helper = new DwollaDatasheetCalloutHelper();
        
        DwollaAPIConfiguration__c config = [SELECT Id,
                Secret_Key__c,
                Authorization_URL__c,
                Client_Key__c
            FROM DwollaAPIConfiguration__c 
            LIMIT 1];
        
        Test.startTest();
        
        helper.getLiveToken(config);
        
        Test.stopTest();        
    }
    
    @isTest
    static void saveCustomerTest(){
        
        Account acount = [SELECT Id 
            FROM Account 
            LIMIT 1];
            
        Test.startTest();
        
        DwollaDatasheetCalloutHelper.saveCustomer(new List<Account>{acount});
        
        Test.stopTest();
    }
}