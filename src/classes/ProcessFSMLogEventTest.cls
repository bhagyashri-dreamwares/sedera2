@IsTest
public class ProcessFSMLogEventTest {
    @IsTest public static void ProcessFSMLogEventTest () {
        Test.startTest();
        FSMLogEvent__e log_event = new FSMLogEvent__e();
        log_event.source__c = 'Test Source';
        log_event.message__c = 'Test Message';
        log_event.fsmlogtype__c = 'Informational';
        log_event.fsmlogdatetime__c = datetime.now();
        Database.SaveResult sr = EventBus.publish(log_event);
        Test.stopTest();
    }
}