/**
* @Purpose : Handler for SDocTrigger
* @Date : 30/07/2018
*/
public class SDocTriggerHandler{
    
    /**
    * To update SDOc fields
    */
    public static void updateFields(List<SDOC__SDoc__c> sDocList, Boolean isInsert){
        
        Map<String, List<String>> attachmentIdToSDocMap = new Map<String, List<String>>();
        Map<String, Attachment> sDocToAttachmentMap = new Map<String, Attachment>();
        List<SDOC__SDoc__c> sDocNewList = new List<SDOC__SDoc__c>();
        
        System.debug('new sDocList ::::'+sDocList);
        for(SDOC__SDoc__c sDocRecord :sDocList){
            
            System.debug('sDocRecord.SDOC__Attachment_ID__c ::::'+sDocRecord.SDOC__Attachment_ID__c);
            if(String.isNotBlank(sDocRecord.SDOC__Attachment_ID__c)){
                
                if(!attachmentIdToSDocMap.containsKey(sDocRecord.SDOC__Attachment_ID__c.substring(0, 15))){
                    
                    attachmentIdToSDocMap.put(sDocRecord.SDOC__Attachment_ID__c.substring(0, 15), new List<String>());     
                }               
                attachmentIdToSDocMap.get(sDocRecord.SDOC__Attachment_ID__c.substring(0, 15)).add(sDocRecord.Id); 
            }
        }
        
        System.debug('attachmentIdToSDocMap ::'+attachmentIdToSDocMap);
        if(!attachmentIdToSDocMap.isEmpty()){
            sDocToAttachmentMap = getAttachment(attachmentIdToSDocMap);
        }
        
        System.debug('sDocToAttachmentMap ::'+sDocToAttachmentMap);
        if(!sDocToAttachmentMap.isEmpty()){ 
            for(SDOC__SDoc__c sDocRecord :sDocList){
            
               if(sDocToAttachmentMap.containsKey(sDocRecord.Id)){
                   sDocRecord.SDOC__Attachment_Name__c = sDocToAttachmentMap.get(sDocRecord.Id).Name;
                   sDocNewList.add(sDocRecord);
               }     
            }
        }
        
        System.debug('sDocNewList ::'+sDocNewList);
        if(!sDocNewList.isEmpty() && isInsert){
            update sDocNewList;
        }
    }
    
    /**
    * To get SDoc records
    */
    public static List<SDOC__SDoc__c> getSDocList(List<SDOC__SDoc__c> sDocList){
        
        return [SELECT Id, Name, SDOC__Attachment_Name__c, SDOC__Attachment_ID__c
                FROM SDOC__SDoc__c
                WHERE Id IN :sDocList];
    }
    
    /**
    * To get Attachment records
    */
    private static Map<String, Attachment> getAttachment(Map<String, List<String>> attachmentIdToSDocMap){
        
        Map<String, Attachment> sDocToAttachmentMap = new Map<String, Attachment>();
        
        try{
            for(Attachment attachment :[SELECT Id, Name
                                        FROM Attachment
                                        WHERE Id = :attachmentIdToSDocMap.keySet()]){
                                        
                System.debug('attachment ::::'+attachment );
                if(attachmentIdToSDocMap.containsKey(String.valueOf(attachment.Id).substring(0, 15))){
                    
                    for(String sDocId : attachmentIdToSDocMap.get(String.valueOf(attachment.Id).substring(0, 15))){
                        sDocToAttachmentMap.put(sDocId, attachment);
                    }
                }               
            }
        }catch(Exception exp){
            System.debug('Exception exp');
        }
        return sDocToAttachmentMap;
    }
}