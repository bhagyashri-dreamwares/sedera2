/**  
 * @Purpose      : Test Class for InvoiceHelper
 * @Author       : Dreamwares 
 * @Created Date : 31/10/2018
 */
@isTest
public class InvoiceHelperTest {
    /**
     * @Purpose : Method to create test data
     */
    @TestSetup
    public static void createTestData(){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account account = new Account(Name = 'Test Account', RecordtypeId = recordtypeId, Phone = '9898989890', Invoice_Email_Address__c = 'test@sf.com');
        insert account;
        
        Invoice__c invoice = new Invoice__c(Account__c = account.Id, Invoice_Date__c = Date.today(), Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 50000);
        insert invoice;
        
        Paubox_Message_Template__c pauboxMsgTemplate = new Paubox_Message_Template__c(Name = 'Invoice', Paubox_Message_Type__c = 'Invoice',
                                                                                      From_Address__c = 'testMail@sf.com', Template_Body__c = 'Hello, Testing!',
                                                                                      Template_Subject__c = 'Test');
        insert pauboxMsgTemplate;
        
        //Create custom setting 
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'Test@test.com';
        config.Site_Url__c = 'Test@test.com';
        insert config;
    }
    
    /**
     * @Purpose: Test InvoiceHelper 
     */
    static testMethod void testInvoiceHelper() {
        Invoice__c invoice = [SELECT Id FROM Invoice__c LIMIT 1];
        
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailHelperMock());
        
        Test.startTest();
        InvoiceHelper.sendInvoice(invoice.Id);
        Test.stopTest();
        
        List<Paubox_Message__c> pauboxMsg = [SELECT Id, Name FROM Paubox_Message__c ];
        System.assertNotEquals(0, pauboxMsg.size());
    }
    
    //Mock class
    public class  PauBoxEmailHelperMock implements HttpCalloutMock {
        
        //Method to return mock reponse
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse response = new HTTPResponse();
            
            if (req != null){
                response.setHeader('Content-Type', 'application/json');
                response.setBody('{"sourceTrackingId": "3d38ab13-0af8-4028-bd45-52e882e0d584","data": "Service OK"}');
                response.setStatusCode(200);
            }
            
            return response;
        }
    }
}