@istest
public class Prepareforrenewalaction_test {


    public static testmethod Void Test_Method_For_MemberRecords_count_On_Employer_Test1() {
        Test.startTest();
        TestDataFactory.Create_Pricing(null,1,true);
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass1);
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Premier', true);

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.parentid = PlanAccountList[0].id;
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        update emp;


        List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(1, false);
        for (integer i = 0; i < MemberAccountList.size(); i++) {
            MemberAccountList[i].Account_Employer_name__C = emp.id;
            MemberAccountList[i].subscription_status__c='Active';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        
        insert MemberAccountList;
        
        
        Prepareforrenewalaction.Inrenewalmethod(emp.id);
        Test.stoptest();
        }}