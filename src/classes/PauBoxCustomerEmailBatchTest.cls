/*
* @Purpose         : PauBoxCustomerEmailBatch Test class
* @author          : Navin
* @since           : 1 Oct 2018
* @date            : 1 Oct 2018 - Navin: created initial version
* @see      
*/

@isTest
public class PauBoxCustomerEmailBatchTest {
    // Create testsetup records
    @TestSetup
    private static void createTestRecord(){
        
        PauBoxConfiguration__c pauBoxConfigRecord = new PauBoxConfiguration__c(Access_Token__c = '  26263260550a74343b9126d45b484057',
                                                                               Email_Subject__c = 'Sedera - Explanation of Needs Shared Report',
                                                                               EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/message_receipt?sourceTrackingId=',
                                                                               Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/messages',
                                                                               From_Email__c = 'needs@sedera.com',
                                                                               Site_Url__c = 'https://partial-partial-sedera.cs21.force.com'
                                                                               );
        Insert pauBoxConfigRecord;
        
        Paubox_Message_Template__c pauboxMessageTemplate = new Paubox_Message_Template__c(Name = 'Funding Source',
                                                                                          From_Address__c = 'needs@sedera.com',
                                                                                          Paubox_Message_Type__c = 'Funding Source',
                                                                                          Template_Body__c = 'Hi,Please link your Bank:There are a lot of templates for proposal as there are various types of proposals we do have. One of the purposes for creating a Project Proposal Templates is to describe the important points of why it has been planned, objectives and benefits that we can get from pursuing the proposed project or work. The common type of financial proposal template that people can use is the funding proposal.',
                                                                                          Template_Subject__c = 'Get Plaid Link');
        Insert pauboxMessageTemplate;
        
        // Create Account record
        
        List<Account> accountrecord = new List<Account>();
        
        for(Integer i=0;i<1;i++)
        {
            accountrecord.add(new Account(Name = 'testAccount'+i, RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
                                          Invoice_Email_Address__c = 'test@test.com',
                                          Dwolla_ID__c='74d9b82a-2a54-4160-9f86-c718b8155b3'+i,
                                          Associated_Bank__c = 'Amegy'));
        }
        Insert accountrecord;
        
        //add a dwolla datasheet for the test account
        
        Dwolla_Datasheet__c datasheet = new Dwolla_Datasheet__c();
        datasheet.Plaid_Email_Address__c = 'evan.koch@fastslowmotion.com';
        datasheet.account__c = accountrecord[0].id;
        datasheet.ein__c = '11-2365897';
        datasheet.ssn__c = '123-21-2222';
        datasheet.controller_ssn__c = '123-22-2365';
        
        insert datasheet;
    }
    
    
    // POsitive test case
    @isTest
    private static void positiveTest() {
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailBatchTestMock());
        Test.startTest(); 
        List<Account> accountRecordList = [SELECT Id, Invoice_Email_Address__c, 
                                                  Dwolla_ID__c, Name, Associated_Bank__c
                                           FROM Account 
                                           LIMIT 100];
        PauBoxCustomerEmailBatch pauBoxCustomerEmailObject = new PauBoxCustomerEmailBatch(accountRecordList,'Funding Source Message','Funding Source','Account');
        DataBase.executeBatch(pauBoxCustomerEmailObject,1);
        Test.stopTest();
    }
}