/**
*@Purpose : Handler for FundingSourceTrigger
*@Date : 25-09-2018
*/
public class FundingSourceTriggerHandler{

    /**
    *@Purpose: To create Funding source at Dwolla API
    */
    public static void createFundindingSource(List<Dwolla_Funding_Source__c> fundingSourceList){
        
        Set<Id> validFundingSourceIdSet = new Set<Id>();
        
        for(Dwolla_Funding_Source__c fundingSource :fundingSourceList){
            
            if(fundingSource.Sync_to_Dwolla__c && fundingSource.Active__c && 
               String.isNotBlank(fundingSource.Routing_Number__c) && String.isNotBlank(fundingSource.Bank_Account_Number__c) && 
               String.isBlank(fundingSource.Dwolla_Funding_Source_ID__c)){
               
                validFundingSourceIdSet.add(fundingSource.Id);
            }        
        }
        
        if(!validFundingSourceIdSet.isEmpty()){
            
            Database.executeBatch(new FundingSourceBatch(validFundingSourceIdSet), 1);
        }
    }
    
}