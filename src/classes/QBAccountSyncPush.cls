/*
*@Purpose      : post the salesforce updated accounts to quickbook
*@Author       : Dreamwares 
*@Created Date : 19/07/2018
*/
public with sharing class QBAccountSyncPush {
    IParser parserImpl = new QBCustomerParser(); 
    Map<String, String> batchIdAccountIdMap = new Map<String, String>();
    public final Integer MAXRECORDS = 150;
    public Integer batchNo =0;
    public String currentQuickBookOrgName = '';
    
    public QBAccountSyncPush(String currentQuickBoocOrg){
        currentQuickBookOrgName = currentQuickBoocOrg;
    }
    
    // update records in Third party App
    public BAPIResponseWrapper doPost(List<SObject> recordsList, String currentQuickBoocOrg){
        currentQuickBookOrgName = currentQuickBoocOrg;
        BAPIResponseWrapper response = new BAPIResponseWrapper(false, '');
        if(recordsList != null && recordsList.size() > 0){
            batchNo++;
            List<QBWrappers.QBCustomer> customersList = (List<QBWrappers.QBCustomer>)parserImpl.parseToDTOList(recordsList, currentQuickBookOrgName);
            
            QBAccountBatchRequestWrapper qbBatchRequestObj = new QBAccountBatchRequestWrapper();
            Integer i= 1;
            if(customersList != null && !customersList.isEmpty()){
                for(QBWrappers.QBCustomer customer : customersList){
                    String batchId = 'bid' + batchNo +  i;
                    
                    QBAccountBatchRequestWrapper.QBBatchAccountObject batchObject;
                    if(String.isNotBlank(customer.Id)){
                        batchObject = new QBAccountBatchRequestWrapper.QBBatchAccountObject(batchId, 'update');
                    }
                    else {
                        batchObject = new QBAccountBatchRequestWrapper.QBBatchAccountObject(batchId, 'create');
                    }
                    
                    batchIdAccountIdMap.put(batchId, recordsList[i-1].Id);
                    batchObject.Customer = customer;
                    qbBatchRequestObj.BatchItemRequest.add(batchObject);
                    i++;
                }
                
                QBConfigurationWrapper qbConfiguration = new QBConfigurationWrapper(currentQuickBookOrgName);
                QBAPIGateway qbGateway = new QBAPIGateway(qbConfiguration);
                String responseBody    = qbGateway.batch(JSON.serialize(qbBatchRequestObj));
                
                if(String.isNotBlank(responseBody)){
                    QBBatchResponse batchResponse = new QBBatchResponse();
                    try{
                        batchResponse = (QBBatchResponse)JSON.deserialize(responseBody, QBBatchResponse.class);
                        response                = new BAPIResponseWrapper(true, '');
                        response.records        = batchResponse.BatchItemResponse;
                        response.responseTime   = QBSyncHelper.dateFromString(batchResponse.time_qb);
                    }
                    catch(Exception e){ QuickbookDebugServices.trackException(e);
                        System.debug('Error deserializing batch response :: ' + e.getMessage());
                    }
                }
                else{
                    response.message ='Blank response from QB';
                }
            }
        }   
        return response;    
    }
    
    public void save(List<Object> records){
        
    }
    
    public void updateRecords(List<Object> records, DateTime lastSyncDate, String currentQuickBoocOrg){
        currentQuickBookOrgName = currentQuickBoocOrg;
        if(records != null && !records.isEmpty()){
            List<QBBatchResponseObject> qBBatchResponseList = (List<QBBatchResponseObject>)records;
            List<QB_Sync_Errors__c> quickbookDebugList = new List<QB_Sync_Errors__c>();
            if(qBBatchResponseList != null && !qBBatchResponseList.isEmpty()){
                
                Map<String, QBWrappers.QBCustomer> batchIdCustomerMap = new Map<String, QBWrappers.QBCustomer>();
                List<String> faultRecordsList = new List<String>();
                List<Account> accounts = new List<Account>();
                Map<String,QBBatchFault> faultRecordMap = new Map<String,QBBatchFault>();
                for(QBBatchResponseObject record :qBBatchResponseList){
                    if(record.Customer != null){
                        // Customer record
                        batchIdCustomerMap.put(record.bId, record.Customer);
                    }
                    else if(record.Fault != null){
                        // fault
                        faultRecordsList.add(record.bId);
                        faultRecordMap.put(record.bId,record.Fault);
                    }
                }
                
                if(!batchIdCustomerMap.isEmpty()){
                    accounts = (List<Account>)parserImpl.parseToObjectList(batchIdCustomerMap.values(),currentQuickBookOrgName);
                    if(!accounts.isEmpty()){Integer index = 0;
                        for(String bId :batchIdCustomerMap.keySet()){accounts[index].Id = this.batchIdAccountIdMap.get(bId);
                            if(String.isNotBlank(currentQuickBookOrgName) && currentQuickBookOrgName == 'Quickbooks' ){ accounts[index].Qb_Sync_Status__c = 'Success';
                            } 
                            if(String.isNotBlank(currentQuickBookOrgName) && currentQuickBookOrgName == 'Quickbooks2' ){ accounts[index].QB_Sync_Status2__c = 'Success';
                            }
                            index++;
                        }
                    }   
                }       
                List<account> faultAccountList = new List<account>();
                
                for(String bId :faultRecordsList){
                    if(batchIdAccountIdMap.containsKey(bId)){
                        account aacountRecord = new account(); aacountRecord.Id = batchIdAccountIdMap.get(bId);
                        if(String.isNotBlank(currentQuickBookOrgName) && currentQuickBookOrgName == 'Quickbooks' ){aacountRecord.Qb_Sync_Status__c = 'Failed';
                        } 
                        if(String.isNotBlank(currentQuickBookOrgName) && currentQuickBookOrgName == 'Quickbooks2' ){aacountRecord.QB_Sync_Status2__c = 'Failed';}
                        faultAccountList.add(aacountRecord);
                    }
                    
                    if(faultRecordMap.containsKey(bId)){
                        
                        QB_Sync_Errors__c qbDebugRecord = new QB_Sync_Errors__c();
                        qbDebugRecord.Name = 'Failed Account During Sync Id::::'+batchIdAccountIdMap.get(bId);
                        qbDebugRecord.Type__c = 'Failed In Sync';
                        if(faultRecordMap.get(bId) != null){
                            QBBatchFault errorRecord = faultRecordMap.get(bId);
                            if(errorRecord.Error[0] != null){                                
                                qbDebugRecord.ErrorMessage__c = errorRecord.Error[0].Detail+errorRecord.Error[0].Message;
                                System.debug('Error Message Recotd :: '+qbDebugRecord.ErrorMessage__c);
                            }
                            quickbookDebugList.add(qbDebugRecord);
                        }   
                    }
                    
                } 
                if(!faultAccountList.isEmpty()){
                    accounts.addAll(faultAccountList);
                }            
                try{
                    if(accounts.size() > 0){
                        
                        Database.SaveResult[] accountUpdateResult = Database.update(accounts, false); QuickbookDebugServices.trackUpdateResult('account', accountUpdateResult);
                    }
                    
                }
                catch(Exception e){QuickbookDebugServices.trackException(e);
                    System.debug('Error while updating Account Records, ' + e.getMessage());
                }
                try{
                    if(!quickbookDebugList.isEmpty()){
                        Insert quickbookDebugList;
                    }
                }
                catch(Exception e){QuickbookDebugServices.trackException(e);
                    System.debug('Error in inserting error record :: ' + e.getMessage());
                }
            }   
        }
    }
}