/**
 *@Purpose      :
 *@Author       : Dreamwares
 *@Created Date : 19/07/2018
 */
public virtual class QBBatchResponseObject {
    public String bId;
    public QBWrappers.QBCustomer Customer;
    public QBWrappers.QBItem item;
    public QBWrappers.QBInvoiceDTO Invoice;
    public QBWrappers.QBSalesReceipt SalesReceipt;
    public QBWrappers.QBChartAccount Account;
    public QBWrappers.QBCreditMemo CreditMemo;
    public QBWrappers.QBPayment Payment;

    public QBBatchFault Fault;

    public QBBatchResponseObject(){
        Customer = new QBWrappers.QBCustomer();
        item = new QBWrappers.QBItem();
        Fault = new QBBatchFault();
        Invoice = new QBWrappers.QBInvoiceDTO();
        SalesReceipt = new QBWrappers.QBSalesReceipt();
        Account =  new QBWrappers.QBChartAccount();
        CreditMemo = new QBWrappers.QBCreditMemo();
        Payment = new QBWrappers.QBPayment();
    }
}