public class PlaidDTO{
    
    public class PlaidAccountDetails{
        
        public List<AccountInfo> accounts; 
        
        public PlaidAccountDetails(){           
            accounts = new List<accountInfo>();
        }
    }
    
    public class AccountInfo{
        
        public String account_id;
        public String subtype;
    }
    
    public class ProcessorTokenDetails{
        public String processor_token;
    }
    
    public class PublicTokenDetails{
        public String expiration;
        public String public_token;
        public String request_id;
    }
    
    public class FundingSourceDetails{
        
        public String id;
        public String status;
        public String bankAccountType;
        public String name;
        public String bankName;
        public String fingerprint;    
    }
}