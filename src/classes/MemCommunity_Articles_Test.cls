/*
  Covers MemCommunity_Articles
*/
@IsTest
public class MemCommunity_Articles_Test {

    public static testMethod void Test1() {

        Employer__kav article = new Employer__kav();
        article.put('title', 'test');
        article.put('language', 'en_US');
        //article.put('publishStatus','Online');
        String Urlname = 'test' + '' + System.now().getTime();
        article.put('UrlName', Urlname);

        insert article;

        Employer__kav article1 = new Employer__kav();
        article1.put('title', 'test');

        article1.put('language', 'en_US');
        //article1.put('publishStatus','Online');
        String Urlname1 = 'test' + '' + System.now().getTime();
        article1.put('UrlName', Urlname1);

        insert article1;
        list <employer__kav> knowledgearticlelist = [SELECT Id, KnowledgeArticleid FROM employer__kav WHERE PublishStatus = 'Draft'
            AND Language = 'en_US'
            limit 2
        ];
        for (employer__kav k: knowledgearticlelist) {
            KbManagement.PublishingService.publishArticle(k.KnowledgeArticleid, true);
        }
        Employer__DataCategorySelection dc1 = new Employer__DataCategorySelection();
        dc1.DataCategoryGroupName = 'Employer_Knowledge_Base';
        dc1.DataCategoryName = 'All';
        dc1.parentid = article1.id;
        insert dc1;
        Employer__DataCategorySelection dc = new Employer__DataCategorySelection();
        dc.DataCategoryGroupName = 'Employer_Knowledge_Base';
        dc.DataCategoryName = 'All';
        dc.parentid = article.id;
        insert dc;
        Topic Tx = new Topic(name = 'My Resources');
        insert Tx;
        TopicAssignment Ty = new TopicAssignment();
        Ty.EntityId = article.id;
        Ty.topicid = Tx.id;
        insert Ty;
        TopicAssignment Ty1 = new TopicAssignment();
        Ty1.EntityId = article1.id;
        Ty1.topicid = Tx.id;
        insert Ty1;
        //KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        //KbManagement.PublishingService.publishArticle(article1.KnowledgeArticleId, true);

        MemCommunity_Articles.getArticleandAttachment(Tx.id, false, false, 0);
        MemCommunity_Articles.getArticleandAttachment(Tx.id, true, false, 1);
        system.debug('???????????');
        MemCommunity_Articles.getArticle(article1.id);
        system.debug('???????????');

        MemCommunity_Articles.getArticleandAttachment(Tx.id, false, true, 0);




    }

}