global with sharing class ReprocessSingleInvoice 
{
    webservice static void executeBatchForReprocess(ID accountId, ID invoiceId) 
    {
        system.debug('AcountID + InvoiceID = ' + accountId + ' - ' + invoiceId);     
        List<ID> accountIds = new List<ID>();
        accountIds.add(accountId);
        BatchPreCheckInvoicedata b = new BatchPreCheckInvoicedata(accountIds,invoiceId);    
        Database.executeBatch(b);
    }
}