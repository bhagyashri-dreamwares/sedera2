public class CommissionDetailController 
{
    public List<SPRWrapper> sprList {get; set;}
    //public String affiliateId {get; set;}
    public Boolean isAffiliateValid  {get; set;} 
    public String affiliateName {get; set;}
    public Decimal totalCommission {get; set;}
    public Date reportDate {get; set;}
    public Date nextInvoiceDate {get; set;}
    public Date paymentDate {get; set;}
    public Date cancellationDate {get; set;}

    
    public CommissionDetailController()
    {
        String idPageParameter = Apexpages.currentpage().getparameters().get('id');
        String monthParameter = Apexpages.currentpage().getparameters().get('p1');
        String yearParameter = Apexpages.currentpage().getparameters().get('p2');
        
        system.debug('p1= ' + monthParameter);
        system.debug('p2= ' + yearParameter);
        
        if (String.isNotBlank(idPageParameter) && isValidSalesforceId(idPageParameter, Affiliate__c.class) && String.isNotBlank(monthParameter) && String.isNotBlank(yearParameter))
        {
            reportDate = Date.valueOf(yearParameter + '-' + monthParameter + '-01');
            
            Integer month = Integer.valueOf(monthParameter) + 1;
            nextInvoiceDate = Date.valueOf(yearParameter + '-' + String.valueOf(month) + '-01');
            
            cancellationDate = nextInvoiceDate.addDays(-1);
            paymentDate = reportDate.addMonths(1).addDays(14);
            
            system.debug('reportDate = ' + reportDate);
            
            sprList = getSPRList(idPageParameter);
            totalCommission = 0;
            for(SPRWrapper spr :sprList)
            {
                totalCommission = spr.amount + totalCommission;
            }
        }
        else
        {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid parameters! Please provide a valid parameter in the form Account Id - Invoice Id.!');
            ApexPages.addMessage(errorMsg);
        }
    }
    
    public List<SPRWrapper> getSPRList(String affId)
    {
        List<Sedera_Product_Report__c> newList = new List<Sedera_Product_Report__c>();
        List<SPRWrapper> sList = new List<SPRWrapper>();
        
        Affiliate__c aff = [SELECT ID, Name FROM Affiliate__c WHERE ID = :affId];
		affiliateName = aff.Name;        
        
        Affiliate_Pricing__c apRecord = new Affiliate_Pricing__c();
        
        String query = '';
        String SobjectApiName = 'Affiliate_Pricing__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        
        query = 'select Affiliate__r.id, ' + commaSepratedFields + ' from ' + SobjectApiName + ' WHERE Affiliate__c = :affId ORDER BY Effective_Date__c DESC LIMIT 1';
        
        apRecord = Database.query(query);
        
        Boolean isInternalSales = false;
        
        Map<String, List<Sedera_Product_Report__c>> productSprMap = new Map<String, List<Sedera_Product_Report__c>>();
        
        for(Sedera_Product_Report__c spr : [SELECT Id, Enrollment_Date__c, Subscription_Status__c, Dependent_Status__c, Amount__c,
                                                Date_of_Birth__c, ProductName__c, Health_Care_Sharing__c, Primary_Age__c,
                                                Member_Account__c, Member_Account__r.id, Member_Account__r.Name, Invoice__r.Name,
                                                Member_Account__r.Primary_Contact_id__c, Member_Account__r.IUA_Chosen__c, 
                                                Invoice__c, Invoice__r.Account__c, Employer_Account__r.Name, Product__c, Pricing__c,
                                             	Doing_Frontline_Support__c, Member_Account__r.Months_Enrolled__c, Member_Account__r.Enrollment_Date__c,
                                            	Member_Account__r.Cancellation_Date__c,Internal_Salesperson__c,Months_Enrolled__c 
                                                FROM Sedera_Product_Report__c
                                                WHERE (Invoice__r.Invoice_Due_Date__c = :nextInvoiceDate
                                                    AND Product__c != NULL
                                                    AND Pricing__c != NULL
                                                    AND (Referral_Affiliate__c = :affId
                                                        OR Strategic_Affiliate__c = :affId
                                                        OR Lead_Generator__c = :affId
                                                        OR Internal_Salesperson__c = :affId
                                                        OR Brokerage_Firm__c = :affId)
                                                    AND Member_Account__r.Enrollment_Date__c < :nextInvoiceDate
                                                    AND Member_Account__r.Cancellation_Date__c != :cancellationDate)
                       							OR (Invoice__r.Invoice_Due_Date__c = :reportDate
                                                    AND Product__c != NULL
                                                    AND Pricing__c != NULL
                                                    AND (Referral_Affiliate__c = :affId
                                                        OR Strategic_Affiliate__c = :affId
                                                        OR Lead_Generator__c = :affId
                                                        OR Internal_Salesperson__c = :affId
                                                        OR Brokerage_Firm__c = :affId)
                                                    AND Member_Account__r.Cancellation_Date__c = :cancellationDate)
                                                ORDER BY Enrollment_Date__c, Employer_Account__r.Name, Member_Account__r.Name])
        {
            if(spr.Internal_Salesperson__c != affId)
                isInternalSales = TRUE;
            
            String key = spr.Product__c + spr.Pricing__c;
            System.debug('KEY = ' + key);
            if(productSprMap.get(key) == null)
            {
                List<Sedera_Product_Report__c> mapList = new List<Sedera_Product_Report__c>();
                mapList.add(spr);
                productSprMap.put(key,mapList);
            }
            else
            {
                List<Sedera_Product_Report__c> mapList = productSprMap.get(key);
                mapList.add(spr);
                productSprMap.put(key,mapList);
            }
        }
        
        for (String key : productSprMap.keySet()) 
        {
            List<Sedera_Product_Report__c> mapList = productSprMap.get(key);
            if(isInternalSales == FALSE)
            {
                switch on key
                {
                    when 'ACCESSNEW'{sList.addAll(configureAccessNew(mapList,apRecord));}
                    when 'SELECTNEW'{sList.addAll(configureSelectNew(mapList,apRecord));}
                    when 'SELECTOLD'{sList.addAll(configureSelectOld(mapList,apRecord));}
                    when else {}
                }
            }
            else
            {
            	configureInternalCommission(mapList,apRecord,key);
            }
            
            
        }
        
        return sList;
        
        
    }
    
    private void configureInternalCommission(List<Sedera_Product_Report__c> mapList,Affiliate_Pricing__c apRecord, String productType)
    {
        List<SPRWrapper> sList = new List<SPRWrapper>();
        for(Sedera_Product_Report__c spr :mapList)
        {
            if(productType.contains('ACCESS') && spr.Months_Enrolled__c == 0)
            {
                sList.add(new SPRWrapper(spr,apRecord.New_Lives_in_Territory_Access__c));
            }
            else if(productType.contains('SELECT') && spr.Months_Enrolled__c == 0)
            {
                sList.add(new SPRWrapper(spr,apRecord.New_Lives_in_Territory_Select__c));
            }
            else
            {
                sList.add(new SPRWrapper(spr,0.0));
            }
        }
    }
    
    private List<SPRWrapper> configureAccessNew(List<Sedera_Product_Report__c> mapList, Affiliate_Pricing__c apRecord)
    {
        List<SPRWrapper> sList = new List<SPRWrapper>();
        for(Sedera_Product_Report__c spr :mapList)
        {
            if(spr.Doing_Frontline_Support__c == TRUE)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_EC__c));}
                }
            } 
            else
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_NOT_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_NOT_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_NOT_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.ACCESS_RA_NOT_EC__c));}
                }
            }
            
        }
        
        
        return sList;
    }
    
    private List<SPRWrapper> configureSelectNew(List<Sedera_Product_Report__c> mapList, Affiliate_Pricing__c apRecord)
    {
        List<SPRWrapper> sList = new List<SPRWrapper>();
        Integer sprCount = 0;
        for(Sedera_Product_Report__c spr :mapList)
        {
            ++sprCount;
            if(sprCount <= 100)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_100_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_100_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_100_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_100_EC__c));}
                }
            }
            else
            {
               	switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_101_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_101_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_101_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.SELECT_NEW_101_EC__c));}
                } 
            }
            
        }
        
        return sList;
    }
    
    private List<SPRWrapper> configureSelectOld(List<Sedera_Product_Report__c> mapList, Affiliate_Pricing__c apRecord)
    {
        List<SPRWrapper> sList = new List<SPRWrapper>();
        Integer sprCount = 0;
        for(Sedera_Product_Report__c spr :mapList)
        {
            ++sprCount;
            if(sprCount <= 100)
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_100_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_100_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_100_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_100_EC__c));}
                }
            }
            else if(sprCount <= 250)
            {
               	switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_250_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_250_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_250_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_250_EC__c));}
                } 
            }
            else if(sprCount <= 500)
            {
               	switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_500_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_500_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_500_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_500_EC__c));}
                } 
            }
            else
            {
                switch on spr.Dependent_Status__c
                {
                    when 'EF'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_501_EF__c));}
                    when 'EO'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_501_EO__c));}
                    when 'ES'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_501_ES__c));}
                    when 'EC'{sList.add(new SPRWrapper(spr,apRecord.SELECT_OLD_501_EC__c));}
                } 
            }
        }
        
        return sList;
    }
    
    public Boolean isValidSalesforceId(String sfdcId, System.Type objectType){
        try {
            if (Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(sfdcId).matches()){
                //Assign it to an Id before checking the type
                Id id = sfdcId;
                //Construct an instance of this sObject
                sObject sObj = (sObject) objectType.newInstance();
                //Set the ID of the new object to the value to test
                sObj.Id = id;
                
                return true;
            } 
        }catch ( Exception e ){
            isAffiliateValid = false;
            System.debug('Exception occured at line no. :'+e.getLineNumber()+' Error:'+e.getMessage());
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Id. Please provide a valid Id.!');
            ApexPages.addMessage(errorMsg);
        }
        
        return false;
    }
    
    public class SPRWrapper{
        public String EmployerName {get; set;}
        public String MemberName {get; set;}
        public String DependentStatus {get; set;}
        public Date EnrollmentDate {get; set;}
        public Decimal amount {get; set;}
        
        public SPRWrapper(Sedera_Product_Report__c spr, Decimal amount){
            this.EmployerName = spr.Employer_Account__r.Name;
            this.MemberName = spr.Member_Account__r.Name;
            this.EnrollmentDate = spr.Enrollment_Date__c;
            this.DependentStatus = spr.Dependent_Status__c;
            this.amount = amount;
        }
    }
}