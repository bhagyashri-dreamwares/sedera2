@isTest
public class DwollaFundingSourceTriggerHandlerTest {
    
    @TestSetup
    static void testFactory (){
        Account acc = new Account();
        Id EmployerId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        acc.Name='Testing100';
        acc.RecordTypeId = EmployerId;
        insert acc;       
        List<Dwolla_Funding_Source__c> DFSList = new List<Dwolla_Funding_Source__c>();
        DFSList.add(new Dwolla_Funding_Source__c(Type__c = 'Customer', Active__c = true, Dwolla_Funding_Source_Name__c = 'Test100', Dwolla_Funding_Source_ID__c = String.valueOf(Crypto.getRandomInteger()), Employer_Account__c = acc.ID));
        DFSList.add(new Dwolla_Funding_Source__c(Type__c = 'Sedera', Active__c = true, Dwolla_Funding_Source_Name__c = 'TEst100', Dwolla_Funding_Source_ID__c	= String.valueOf(Crypto.getRandomInteger()), Employer_Account__c = acc.ID));
        insert DFSList; 
    }
    
    static testmethod void testDeactivateRest(){
        
        Account acc = [SELECT ID, Name FROM Account WHERE Name = 'Testing100'];
        
        List<Dwolla_Funding_Source__c> newDFSList = new List<Dwolla_Funding_Source__c>();
        newDFSList.add(new Dwolla_Funding_Source__c(Type__c = 'Customer', Active__c = true, Dwolla_Funding_Source_ID__c = String.valueOf(Crypto.getRandomInteger()), Employer_Account__c = acc.ID));
        newDFSList.add(new Dwolla_Funding_Source__c(Type__c = 'Sedera', Active__c = true, Dwolla_Funding_Source_ID__c	= String.valueOf(Crypto.getRandomInteger()), Employer_Account__c = acc.ID));
        insert newDFSList; 
        
        List<Dwolla_Funding_Source__c> oldDFSList = [SELECT ID, Active__c, Employer_Account__c, Type__c
                                                  FROM Dwolla_Funding_Source__c 
                                                  WHERE Employer_Account__c = :acc.ID AND Dwolla_Funding_Source_Name__c Like 'Test100'];   
        
        update oldDFSList;
        
       	for(Dwolla_Funding_Source__c dfs : oldDFSList){
         	//System.assert(!dfs.Active__c);
        }    
        
    }
    
}