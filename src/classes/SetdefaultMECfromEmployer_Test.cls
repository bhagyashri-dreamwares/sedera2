@Istest
public class SetdefaultMECfromEmployer_Test
{

  public static testmethod void test1()
{


        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
        AccessPricing[0].name='Default Access Pricing';
        AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        insert AccessPricing[0];
        
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        List < Account > PlanAccountList = TestDataFactory.Create_Account_Of_Plan_type(1, 'Sedera Select', true);

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, false);


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        Account emp = EmployerAccountList[0];
        emp.parentid = PlanAccountList[0].id;
        insert emp;
        
        
        List <MEC_Product__c> Mec_Product_List=TestDataFactory.Create_Mec_Product(3,true);
        Mec_Product_List[1].Discount_Tier__c ='T2';
        update Mec_Product_List[1];
        
        List <AccountMECAssociation__c> Mec_Assosciation_List=new List<AccountMECAssociation__c>();
        
        for(Integer i=0;i<3;i++)
        {
            Mec_Assosciation_List.addAll(TestDataFactory.Create_Mec_Assosciation(emp.Id,Mec_Product_List[i].Id,1,false));           
        }
        
        Mec_Assosciation_List[1].Default_MEC_Product__c=true;
        insert Mec_Assosciation_List;
                 
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        List <Account> MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(3, false);
        for(integer i=0;i<MemberAccountList.size();i++)
         {
           MemberAccountList[i].Account_Employer_name__C=emp.id;
         }

 
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger= 0;
        insert MemberAccountList;
        
        for(Account a:[Select id,Member_Discount_Tier_Manual__c  from Account where recordType.Name='Member'])
        system.assertEquals(a.Member_Discount_Tier_Manual__c,'T2');
        
        PageReference page=new PageReference('/apex/ChoseMECdefaultVF');
        Test.SetCurrentPage(Page);
        
        page.getParameters().put('id',emp.Id);
        
        ApexPages.standardController sc=new ApexPages.standardController(Emp);
        SetdefaultMECfromEmployer ext=new SetdefaultMECfromEmployer(sc);
        
        ext.selectedValue=Mec_Assosciation_List[2].Id;
        
        ext.UpdateDefaultMEC();
        
        for(Account a:[Select id,Member_Discount_Tier_Manual__c  from Account where recordType.Name='Member'])
        system.assertEquals(a.Member_Discount_Tier_Manual__c,'T1');
        
}




}