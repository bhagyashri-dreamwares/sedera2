/*
* @description      : Negative mock response
* @author       : Navin (Dreamwares)
* @since        : 28 Sep 2018
* @date         : 28 Sep 2018 - Navin: created initial version
* @date         ${date} - ${user}: Change history
* @see      
*/
@isTest
public class FundingSourceControllerNegTestMock implements HttpCalloutMock{
    
    public HTTPResponse respond(HTTPRequest req) {
        HTTPResponse response = new HTTPResponse(); 
        if(req.getEndpoint().endsWith('exchange'))
        {                    
            response.setStatusCode(200);
            //response.setBody('{ "access_token": "access-sandbox-1486021f-fbb7-4789-8b5b-fd8daa3da4c7", "item_id": "jBKRByQvX8SkVAjRXdDktD1K16yzawu15eAX7", "request_id": "YkOzNGxGBgHh400" }');
            
        } else if (req.getEndpoint().endsWith('create')) {         
            response.setStatusCode(400);
            //response.setBody('{ "processor_token": "processor-sandbox-7451e68e-0005-48bc-8b5b-fdb196122d29", "request_id": "IId5rIB1g9fr1ad" }');            
        }
        else if (req.getEndpoint().contains('https://sandbox.dwolla.com/oauth/v2/token')) {         
            response.setStatusCode(400);
            //response.setBody('{ "processor_token": "processor-sandbox-7451e68e-0005-48bc-8b5b-fdb196122d29", "request_id": "IId5rIB1g9fr1ad" }');            
        }
         else if (req.getEndpoint().endsWith('funding-sources')) {         
            response.setStatusCode(401);
             //response.setHeader('Location', 'https://api-sandbox.dwolla.com/funding-sources/375c6781-2a17-476c-84f7-db7d2f6ffb31');
        }
        else if (req.getEndpoint().contains('funding-sources')) {         
            response.setStatusCode(400);
            //response.setBody('{ "id": "49dbaa24-1580-4b1c-8b58-24e26656fa31","status": "unverified","type": "bank","bankAccountType": "checking","name": "Test checking account","created": "2017-09-26T14:14:08.000Z","removed": false,"bankName": "SANDBOX TEST BANK","fingerprint": "5012989b55af15400e8102f95d2ec5e7ce3aef45c01613280d80a236dd8d6c3a"}');
             //response.setHeader('Location', 'https://api-sandbox.dwolla.com/funding-sources/375c6781-2a17-476c-84f7-db7d2f6ffb31');
        }
        return response;
        
    }
    
}