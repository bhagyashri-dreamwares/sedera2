global with sharing class ProcessCommissionReport 
{ 
    Webservice static void executeBatchForReprocess(ID reportId) 
    {
        ProcessCommission b = new ProcessCommission(reportId);    
        Database.executeBatch(b);         
    }
}