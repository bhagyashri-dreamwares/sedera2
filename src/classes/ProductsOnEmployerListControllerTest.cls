@Istest

public class ProductsOnEmployerListControllerTest {

    public static testmethod void Testmethod1() {
        Test.startTest();
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        list < Product2 > ProductToBeInserted = new List < Product2 > ();
        product2 productA = new Product2();
        productA.name = 'Select U30 EF IUA 1000';
        productA.productcode = 'Select U30 EF IUA 1000';
        productA.price__C = 500;
        ProductToBeInserted.add(productA);

        product2 productB = new Product2();
        productB.name = 'Select U30 EF IUA 1000';
        productB.productcode = 'Select U30 EF IUA 1000';
        productB.price__C = 500;
        ProductToBeInserted.add(productB);

        insert ProductToBeInserted;

        List < Account > PlanAcc = TestDataFactory.Create_Account_Of_Plan_type(1, 'sedera Select', true);
        List < Account > EmployerAcc = TestDataFactory.Create_Account_Of_Employer_type(1, true);
        List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAcc[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;

        List < Account > MemberAcc = TestDataFactory.Create_Account_Of_Member_type(1, false);

        MemberAcc[0].Account_Employer_Name__c = EmployerAcc[0].id;
        MemberAcc[0].Subscription_Status__c = 'Active';
        MemberAcc[0].Enrollment_Date__c = system.now().date();
        MemberAcc[0].C_MEC__c = true;
        MemberAcc[0].Health_Care_Sharing__c = true;
        MemberAcc[0].HSA__c = true;
        MemberAcc[0].Teladoc_Direct__c = true;
        MemberAcc[0].SecondMD__c = true;
        MemberAcc[0].parentid = PlanAcc[0].id;
        MemberAcc[0].MS_Select__c = true;
        MemberAcc[0].PMA__c = true;
        MemberAcc[0].Liberty_Rx__c = true;
        MemberAcc[0].IUA_Chosen__C = 1000;
        MemberAcc[0].Date_of_Birth__c = system.now().date().adddays(-222);
        MemberAcc[0].Dependent_Status__c = 'EF';
        insert MemberAcc;
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Update MemberAcc;

        PageReference pg = Page.ProductListOnEmployerPage;
        Test.setCurrentPage(pg);

        Apexpages.standardController sc = new Apexpages.standardController(EmployerAcc[0]);
        ProductListOnEmployerController Controller = new ProductListOnEmployerController(sc);
        controller.DownloadAsExcel();

        test.stoptest();


    }



}