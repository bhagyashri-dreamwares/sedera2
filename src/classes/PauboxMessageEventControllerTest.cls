/*
*@Purpose : Test method for pauboxMessageEventController class
*@Date : 31/07/2018
*/
@isTest
public class PauboxMessageEventControllerTest{
    
    /*
    * To initialise data
    */   
    @testSetup
    private static void initData(){
        
        List <Pricing__c> PriceList= Create_Pricing(null,2);
        insert PriceList;
        
        
        //insert custom setting record
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'test@test.com';
        config.Site_Url__c = 'test@test.com';
        insert config;
        
        //insert Account
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account emp = new Account();
        emp.recordTypeId = accRecordTypeId;
        emp.Default_Product__c = 'Sedera Access';
        emp.MS_Pricing__c = 'New Pricing';
        emp.Available_IUA_Options__c = '500';
        emp.Name = 'Test';
        emp.Invoice_Email_Address__c = 'test@test.com';         
        insert emp;
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        Account account = new Account();
        account.recordTypeId = devRecordTypeId;
        account.Name = 'Test AccountName';
        account.First_Name__c = 'FName';
        account.Last_Name__c = 'LName';
        account.Account_Employer_Name__c = emp.Id;
        account.Primary_Email_Address__c = 'test@test.com';
        account.Default_Product__c = 'Sedera Access';
        
        insert account;
        
        // insert contact
        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.LastName = 'Test Name';
        contact.Email = 'test@test.com';
        insert contact;
        
        // insert Case
        Case caseRecord = new Case();
        caseRecord.ContactId = contact.Id;
        caseRecord.Status = 'New';
        caseRecord.Origin = 'Phone';
        caseRecord.Type = 'HCS';
        caseRecord.Correspondence_Email__c = 'test@test.com';
        insert caseRecord;
        
        // insert Attachment
        Attachment attach = new Attachment(); 
        attach.Name='Test.pdf'; 
        attach.body= Blob.valueOf('Unit Test Attachment Body'); 
        attach.parentId = caseRecord.Id; 
        attach.ContentType = 'application/pdf'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        insert attach;
        
        //insert SDoc
        SDOC__SDoc__c sDoc = new SDOC__SDoc__c();
        sDoc.SDOC__Attachment_ID__c = attach.Id;
        sDoc.SDOC__Attachment_Name__c = attach.Name;
        sDoc.SDOC__ObjectID__c = caseRecord.ParentId;
        insert sDoc;
        
        //insert pauboxMessage Shared record
        Paubox_Message__c pauboxMessage = new Paubox_Message__c();
        pauboxMessage.Name = 'Test';
        pauboxMessage.Case__c = caseRecord.Id;
        pauboxMessage.SDoc__c = sDoc.Id;
        pauboxMessage.Tracking_ID__c = 'Test123';
        pauboxMessage.Date_Sent__c = System.now();
        pauboxMessage.From_Email_Address__c = 'Test@test.com';
        pauboxMessage.To_Email_Address__c = 'test@test.com'; 
        insert pauboxMessage;
        
        
        
    }
    
    public static List <Pricing__c> Create_Pricing(Id EmployerId,Integer NumberOfPricing) {
        List <Pricing__c> PricingList = new List <Pricing__c> ();
        
        for (integer i = 0; i<NumberOfPricing; i++) {
           Pricing__c x=new Pricing__c(Employer_Name__c=EmployerId,EC_Tier__c=1.90, EF_Tier__c=2.90, EO_Tier__c=1.00, ES_Tier__c=2.00, Is_Base_Price_Record__c=true, Liberty_Rx__c=45.00, MCS_Additional__c=1.05, MCS_Base__c=168.00, MSEC__c=100.00, MSEF__c=150.00, MSEO__c=55.00, MSES__c=100.00, Name='default access pricing', PSEC__c=49.82, PSEF__c=68.23, PSEO__c=31.41, PSES__c=49.82, RecordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Select Pricing').getRecordTypeId(),  Teladoc_Reduction__c=-4.35, Tobacco_Use__c=75.00, U30IUA500__c=0.70, U30IUA1000__c=0.65, U30IUA1500__c=0.55, U30IUA2500__c=0.45, U30IUA5000__c=0.25, U40IUA500__c=0.85, U40IUA1000__c=0.80, U40IUA1500__c=0.70, U40IUA2500__c=0.52, U40IUA5000__c=0.40, U50IUA500__c=1.00, U50IUA1000__c=0.85, U50IUA1500__c=0.75, U50IUA2500__c=0.62, U50IUA5000__c=0.49, U60IUA500__c=1.30, U60IUA1000__c=1.10, U60IUA1500__c=1.00, U60IUA2500__c=0.87, U60IUA5000__c=0.62, U65IUA500__c=2.65, U65IUA1000__c=2.30, U65IUA1500__c=2.15, U65IUA2500__c=1.92, U65IUA5000__c=1.49, X2nd_MD_Reduction__c=-6.00, X500T2__c=0.920, X500T3__c=0.840, X1000T2__c=0.925, X1000T3__c=0.850, X1500T2__c=0.925, X1500T3__c=0.850, X2500T2__c=0.925, X2500T3__c=0.850, X5000T2__c=0.913, X5000T3__c=0.825,X1500NewO30T1__c=6,X1500NewU30T1__c=7,Old1000T3__c=8,Old500T3__c=9,U30EOIUA500__c=3, U30ECIUA500__c=3, U30EFIUA500__c=3,U30ESIUA500__c=3, O30EOIUA500__c=3, O30ESIUA500__c=3, O30ECIUA500__c=3, O30EFIUA500__c=3, U30EOIUA1000__c=3, U30ESIUA1000__c=3, U30ECIUA1000__c=3, U30EFIUA1000__c=3, O30EOIUA1000__c=3, O30ESIUA1000__c=3, O30ECIUA1000__c=3, O30EFIUA1000__c=9);
            PricingList.add(x);
        }
        return PricingList;
    }
    
    /*
    * To test fetch Email statusFunctionality
    */
    private static testMethod void testFetchEmailStatusFunctionality(){
        
        Paubox_Message__c pauboxMessage = [SELECT Id
                             FROM Paubox_Message__c 
                             LIMIT 1];
        
        
        
        Test.setMock(HttpCalloutMock.class, new pauboxMessageEventMock ());
        Test.startTest();
        
        pauboxMessageEventController.storeEmailStatus(new List<String>{''+pauboxMessage.Id});
         
        Test.stopTest();
        
        Paubox_Message__c updatedpauboxMessage = [SELECT Id, Email_Status__c 
                                     FROM Paubox_Message__c 
                                     LIMIT 1];
       // System.assertEquals('delivered', updatedpauboxMessage.Email_Status__c);
    }
    
    public class  pauboxMessageEventMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{ "sourceTrackingId": "Test123", "data": { "message": { "id": "<Test123@authorized_domain.com>", "message_deliveries": [ { "recipient": "test@test.com", "status": { "deliveryStatus":"delivered", "deliveryTime":"Mon, 23 Apr 2018 13:27:34 -0700", "openedStatus": "opened", "openedTime": "Mon, 23 Apr 2018 13:27:51 -0700" } } ] } } }');
            response.setStatusCode(200);
            
            return response;
        }
    }
}