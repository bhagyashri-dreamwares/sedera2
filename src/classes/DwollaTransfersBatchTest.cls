/*
 * @Purpose       :Test Class for add DwollaTransfersBatch
 * @Created Date  :28/12/2018
 */
@IsTest
public class DwollaTransfersBatchTest {    
    
    /*
     * @Purpose : Create data required for testing
     */
    @TestSetup
    public Static void createTestData(){ 
        
        // Insert Account
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account account = new Account(RecordtypeId = recordtypeId,
                                      Name = 'Test Account');
        insert account;	
        
        // Insert Invoice
        Invoice__c invoice = new Invoice__c(Account__c = account.Id,
                                            Invoice_Date__c = Date.today(), 
                                            Invoice_Due_Date__c = Date.today().addMonths(30),
                                            Paid_Amount__c = 50000);
        insert invoice;
        
        Dwolla_Funding_Source__c sourceDwollaTrnasfer = new Dwolla_Funding_Source__c(Type__c = 'Customer',
                                                                                     Active__c = true,
                                                                                     Employer_Account__c = account.id,
                                                                                     Dwolla_Funding_Source_Name__c = 'Test100',
                                                                                     Dwolla_Funding_Source_ID__c = String.valueOf(Crypto.getRandomInteger()));
        insert sourceDwollaTrnasfer;
        
        Dwolla_Funding_Source__c destinationDwollaTrnasfer = new Dwolla_Funding_Source__c(Type__c = 'Balance',
                                                                                          Active__c = true, 
                                                                                          Employer_Account__c = account.id,
                                                                                          Dwolla_Funding_Source_Name__c = 'Test101',
                                                                                          Dwolla_Funding_Source_ID__c = String.valueOf(Crypto.getRandomInteger()));
        insert destinationDwollaTrnasfer;
        
        Dwolla_Transfer__c dwollaTransfer = new Dwolla_Transfer__c(Amount__c = 2, 
                                                                       Invoice__c = invoice.id,
                                                                       Source_Dwolla_Funding_Source__c = sourceDwollaTrnasfer.id,
                                                                       Destination_Dwolla_Funding_Source__c = destinationDwollaTrnasfer.id,
                                                                       Transfer_Processed__c = DateTime.now(),
                                                                       Transfer_Status__c = 'Pending');
        insert dwollaTransfer;
        
		
        // Insert Dwolla API Configuration Custom Setting
        DwollaAPIConfiguration__c dwollaAPIConfigurationObj = new DwollaAPIConfiguration__c(Access_Token__c = 'MKGrMhOxWxMZt7OWrp98IFFopbQLjHrhd6Zlyj2kqK5w2dRKmO',
                                                                                           	Access_Token_Expiry__c = DateTime.now(),
                                                                                            Authorization_URL__c = 'https://sandbox.dwolla.com/oauth/v2/token',
                                                                                            Client_Key__c = 'UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp',
                                                                                            Is_Sandbox__c = true,
                                                                                            Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize',
                                                                                            Sandbox_Endpoint_Url__c = 'https://api-sandbox.dwolla.com',
                                                                                            Secret_Key__c = '7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150');
        insert dwollaAPIConfigurationObj;
    }    
    
   /*
    * @Purpose : To Test functionality
    */
    @isTest
    public static void functionalityTest(){  
     
       List<String> dwollaTransferIdList = new List<String>();
       for(Dwolla_Transfer__c dwollaTransferObj :[SELECT Id FROM Dwolla_Transfer__c LIMIT 1]){
       		dwollaTransferIdList.add(dwollaTransferObj.id);    
       }
        
       DwollaAPIConfiguration__c dwollaAPIConfigurationObj = DwollaAPIConfiguration__c.getOrgDefaults();
       System.debug('dwollaAPIConfigurationObj:::::'+dwollaAPIConfigurationObj); 
	
       // Set the Mock repsonse class for testing purpose
       Test.setMock(HttpCalloutMock.class, new DwollaTrasnferBatchMockRsponse()); 
        
       Test.startTest();
            DwollaTransfersBatch dwollaTransfersBatchObj = new DwollaTransfersBatch(dwollaTransferIdList);
            Database.executeBatch(dwollaTransfersBatchObj,1);
        
        	dwollaTransfersBatchObj.getLiveToken(dwollaAPIConfigurationObj);
       Test.stopTest();
    }
}