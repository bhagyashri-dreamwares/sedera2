public class DwollaDatasheetController {

    public DwollaDatasheetController() {
        String accountId = ApexPages.currentPage().getParameters().get('id');
        
    }

    @RemoteAction
    Public static Response getPagedata(String accountId){
        System.debug('accountId'+ accountId);

        PageDataWrapper pageData = new PageDataWrapper();
        try{
            if(String.isNotBlank(accountId)){

                List<Account> accountList = [SELECT Id, Name, Dwolla_ID__c
                                             FROM Account
                                             WHERE Id =: accountId];

                if(accountList != null && !accountList.isEmpty()){

                    pageData.accountName = accountList[0].Name;
                    System.debug('account'+ accountList);
                    pageData.PickValues = getPickValues();
                    pageData.redirectURL = Label.Site_Url+'fundingSource?id='+accountList[0].Id+'&customerId='+accountList[0].Dwolla_ID__c+
                        '&accountName='+accountList[0].Name;
                    return new Response(true, '', pageData);
                }
                else{

                    return new Response(false, 'Your account is not valid', null);
                }
            }
            else{
                return new Response(false, 'Page should have Account Id.', null);
            }
        }
        catch (Exception e) {
            String msg = e.getMessage();
            System.debug('The following exception has occurred' + e.getMessage());
            return new Response(false, 'The following exception has occurred'+ msg, null);
        }

    }

    private static Map<string, List<selectOption>> getPickValues() {

        Map<String,string> sobjectoptionsMap = new Map<String,string>();
        sobjectoptionsMap.put('Business_Classification__c','Dwolla_Datasheet__c');
        sobjectoptionsMap.put('Business_Type__c','Dwolla_Datasheet__c');
        sobjectoptionsMap.put('Controller_State__c','Dwolla_Datasheet__c');
        sobjectoptionsMap.put('State__c','Dwolla_Datasheet__c');

        Map<string, List<selectOption>> optionsMap = new Map<string,List<selectOption>>();

        for(String field_name : sobjectoptionsMap.keySet()){
            String objectName = sobjectoptionsMap.get(field_name);
            Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get(objectName);
            Schema.DescribeSObjectResult descResult = sobject_type.getDescribe();
            Map<String, Schema.SObjectField> field_map = descResult.fields.getMap();
            List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues();
            List<selectOption> optionsList = new List<selectOption>();
            for (Schema.PicklistEntry picklistEntryRecord : pick_list_values) {
                optionsList.add(new selectOption(picklistEntryRecord.getValue(), picklistEntryRecord .getLabel()));
            }
            optionsMap.put(field_name,optionsList);
        }

        return optionsMap;
    }

    @RemoteAction
    public static Response createDataSheetRecord(String dataSheetStr){
        System.debug('dataSheetStr '+ dataSheetStr);
        DwollaDataSheetWrappper dataSheetWrappper;
        try{
            if(String.isNotBlank(dataSheetStr)){
                dataSheetWrappper = (DwollaDataSheetWrappper)json.deserialize(dataSheetStr, DwollaDataSheetWrappper.Class);
                System.debug('dataSheetWrappper ::: '+dataSheetWrappper);
                if(dataSheetWrappper != null){
                    if( String.isBlank( dataSheetWrappper.recordId ) && isDuplicateDataSheetFound(dataSheetWrappper.emailAddress) ){
                        return new Response(false, 'Email Address should be unique!', null);
                    }

                    Dwolla_Datasheet__c datasheet = new Dwolla_Datasheet__c();

                    if(String.isNotBlank( dataSheetWrappper.recordId)){
                        datasheet.Id = dataSheetWrappper.recordId;
                    }

                    if(!Test.isRunningTest()){
                        datasheet.Account__c = (Id) dataSheetWrappper.accountId;
                    }
                    
                    // Updated For validations
                    if(!dataSheetWrappper.controllerSSN.contains('-')){
                        String controllerSSN = dataSheetWrappper.controllerSSN;
                        dataSheetWrappper.controllerSSN = controllerSSN.substring(0, 3) + '-' + controllerSSN.substring(3, 5) + '-' + controllerSSN.substring(5, controllerSSN.length());
                        System.debug('dataSheetWrappper.controllerSSN ::: '+dataSheetWrappper.controllerSSN);
                    }
                    
                    if(!dataSheetWrappper.SSN.contains('-')){
                        String ssn = dataSheetWrappper.SSN;
                        dataSheetWrappper.SSN = ssn.substring(0, 3) + '-' + ssn.substring(3, 5) + '-' + ssn.substring(5, ssn.length());
                        System.debug('dataSheetWrappper.SSN ::: '+dataSheetWrappper.SSN);
                    }
                    
                    if(!dataSheetWrappper.EIN.contains('-')){
                        String ein = dataSheetWrappper.EIN;
                        dataSheetWrappper.EIN = ein.substring(0, 2) + '-' +  ein.substring(2, ein.length());
                        System.debug('dataSheetWrappper.EIN ::: '+dataSheetWrappper.EIN);
                    }
                    
                    // updation end
                    
                    datasheet.Address_1__c = dataSheetWrappper.address1;
                    datasheet.Address_2__c = dataSheetWrappper.address2;
                    datasheet.Business_Classification__c = dataSheetWrappper.businessClassification;
                    datasheet.Business_Type__c = dataSheetWrappper.businessType;
                    datasheet.Certified__c = dataSheetWrappper.certified;
                    datasheet.Certified_Datetime__c = datetime.now();
                    datasheet.City__c = dataSheetWrappper.city;
                    datasheet.Controller_Address_1__c = dataSheetWrappper.controllerAddress1;
                    datasheet.Controller_Address_2__c = dataSheetWrappper.controllerAddress2;
                    datasheet.Controller_Address_3__c = dataSheetWrappper.controllerAddress3;
                    datasheet.Controller_City__c = dataSheetWrappper.controllerCity;
                    datasheet.Controller_Date_of_Birth__c = dataSheetWrappper.controllerDateofBirth;
                    datasheet.Controller_First_Name__c = dataSheetWrappper.controllerFirstName;
                    datasheet.Controller_Last_Name__c = dataSheetWrappper.controllerLastName;
                    datasheet.Controller_Postal_Code__c = dataSheetWrappper.controllerPostalCode;
                    datasheet.Controller_SSN__c = dataSheetWrappper.controllerSSN;
                    datasheet.Controller_State__c = dataSheetWrappper.controllerState;
                    datasheet.Controller_Title__c = dataSheetWrappper.controllerTitle;
                    datasheet.Date_of_Birth__c = dataSheetWrappper.dateofBirth;
                    datasheet.DBA_Name__c = dataSheetWrappper.DBAName; //dataSheetWrappper.DBAName;
                    datasheet.EIN__c = dataSheetWrappper.EIN;
                    datasheet.Email_Address__c = dataSheetWrappper.emailAddress;
                    datasheet.Plaid_Email_Address__c = dataSheetWrappper.emailAddress;
                    datasheet.First_Name__c = dataSheetWrappper.firstName;
                    datasheet.Last_Name__c = dataSheetWrappper.lastName;
                    datasheet.Postal_Code__c = dataSheetWrappper.postalCode;
                    datasheet.SSN__c = dataSheetWrappper.SSN;
                    datasheet.State__c = dataSheetWrappper.State;

                    CalloutResponse calloutResponseRecord = makeCallouts(datasheet);
                    UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
                    if(calloutResponseRecord != NULL && calloutResponseRecord.isAllCalloutSuccess
                       && String.isNotBlank(calloutResponseRecord.customerIdString)
                       && String.isNotBlank(calloutResponseRecord.benificialOwnerId)){
                           Upsert datasheet;
                           Response responseRec = updateAccountRecord(dataSheetWrappper.accountId,
                                                                      calloutResponseRecord.customerIdString,
                                                                      calloutResponseRecord.benificialOwnerId,
                                                                      dataSheetWrappper.accountName,
                                                                      datasheet.Id);
                           return responseRec;
                    }else{
                        return new Response(false, calloutResponseRecord.errorMessage,  null);
                    }
                }
                else{
                    return new Response(false, 'Something went wrong, Please contact to your Administrator',  null);
                }
            }
        }
        catch(Exception ex){
            System.debug('Error '+ex.getLineNumber()+' '+ex.getMessage());
            System.debug('Error  == '+ex);
            return new Response(false, 'Something went wrong '+ex.getMessage(),  null);
        }
        return null;
    }

    public static Response updateAccountRecord(String accountId, String customerId, String beneficalId, String accountName, String dataSheetId) {
        if(String.isNotBlank(accountId) && String.isNotBlank(customerId) && String.isNotBlank(beneficalId) && String.isNotBlank(dataSheetId)) {
             Account accRecord = new Account();
             accRecord.Id = accountId;
             accRecord.Dwolla_ID__c = customerId;
             accRecord.Dwolla_Beneficial_Owner_ID__c = beneficalId;
             String redirectUrl = Label.Site_Url+'fundingSource?id='+accountId+'&customerId='+customerId+
                                  '&accountName='+accountName;
            try{
                upsert accRecord;
                return new Response(true, 'Dwolla Customer is created',  accRecord, redirectUrl, dataSheetId);
            }catch(Exception ex){
                return new Response(false, 'Error: '+ex.getMessage(),  null);
            }
        }
        return new Response(false, 'Error: Missing Ids',  null);
    }

    /*
    */
    public static CalloutResponse makeCallouts(Dwolla_Datasheet__c datasheetRec) {
        DwollaAPIConfiguration__c config = DwollaAccessTokenHelper.getAPIConfiguration();
        String customerIdString = '', benificialOwnerId = '';
        Boolean isAccessTokenUpdated = false;
        Boolean isTokenLive = false;
        Boolean isCertify = false;
        Integer statusCode;
        DwollaDatasheetCalloutHelper dwollaCalloutHelper = new DwollaDatasheetCalloutHelper();
        DwollaDatasheetCalloutHelper.calloutResponse calloutResponseCustomerRec = new DwollaDatasheetCalloutHelper.calloutResponse();
        DwollaDatasheetCalloutHelper.calloutResponse calloutResponseBeneficalRec = new DwollaDatasheetCalloutHelper.calloutResponse();
        DwollaDatasheetCalloutHelper.calloutResponse calloutResponseCertifyRec = new DwollaDatasheetCalloutHelper.calloutResponse();
        
        System.debug('calloutResponseCustomerRec :::'+calloutResponseCustomerRec);
        
        System.debug('calloutResponseCertifyRec :::'+calloutResponseCertifyRec);
        
        
        if(config.Access_Token__c != null){            
            if(datasheetRec != Null){
                calloutResponseCustomerRec = dwollaCalloutHelper.createCustomer(datasheetRec, false);
                customerIdString = calloutResponseCustomerRec.customerId;
                if(String.isNotBlank(customerIdString)){
                    calloutResponseBeneficalRec = dwollaCalloutHelper.createBeneficialOwners(datasheetRec, customerIdString);
                    System.debug('calloutResponseBeneficalRec :::'+calloutResponseBeneficalRec);
                    
                    benificialOwnerId = calloutResponseBeneficalRec.beneficaliOwnerId;
                    if(String.isNotBlank(benificialOwnerId)){
                        isCertify = dwollaCalloutHelper.CertifyBeneficialOwners(datasheetRec, customerIdString);
                    }else{
                        statusCode = calloutResponseBeneficalRec.statusCode;
                    }
                }else{
                    statusCode = calloutResponseCustomerRec.statusCode;
                }
            }
        }else{
            return new CalloutResponse('', '', false, 'Access Token not found..!');
        }

        String errorMessage = 'Something went wrong.Please try again';
        if(statusCode == 400 ){
            errorMessage = 'Duplicate funding source or validation error Please check all the detials';
        }else if(statusCode == 403){
            errorMessage = 'Not authorized to create customers';
        }

        if(isCertify && String.isNotBlank(customerIdString) && String.isNotBlank(benificialOwnerId)){
            return new CalloutResponse(customerIdString, benificialOwnerId, true, '');
        }
        System.debug('Callout failed :::'+errorMessage);
        return new CalloutResponse('', '', false, errorMessage);
    }

    private static Boolean isDuplicateDataSheetFound(String email){

        if(String.isNotBlank(email)){
            try{
                List<Dwolla_Datasheet__c> dataSheetList = [SELECT Id, Email_Address__c
                                                           FROM Dwolla_Datasheet__c
                                                           WHERE Email_Address__c = :email];
                if(dataSheetList.size() > 0){
                    return true;
                }
            }catch(Exception exp){
                System.debug('Exception :::'+exp.getMessage());
            }
        }
        return false;
    }


    @RemoteAction
    public static Response uploadAttachment(String parameters) {
        System.debug('parameters' +parameters);
        Map<String, String> paramMap = (Map<String, String>)JSON.deserialize(parameters, Map<String, String>.class);

        String parentId = paramMap.containsKey('parentId')? paramMap.get('parentId') : '';
        String attachmentId = paramMap.containsKey('attachmentId')?  paramMap.get('attachmentId') : '';
        String attachmentBody = paramMap.containsKey('attachmentBody')? paramMap.get('attachmentBody'): '';
        String attachmentName = paramMap.containsKey('attachmentName')? paramMap.get('attachmentName'): '';
        String attachmentType = paramMap.containsKey('attachmentType')? paramMap.get('attachmentType'): '';

        if(String.isNotBlank(parentId)) {
            //Account acct = getAccount(acctId);
            //if(acct != null) {
            if(attachmentBody != null) {
                Attachment att = getAttachment(attachmentId);
                String newBody = '';
                if(att.Body != null) {
                    newBody = EncodingUtil.base64Encode(att.Body);
                }
                newBody += attachmentBody;
                att.Body = EncodingUtil.base64Decode(newBody);
                if(attachmentId == null) {
                    if(String.isNotBlank(attachmentType) && attachmentType == 'emailAttachment'){
                        att.Name = 'emailAttach_'+ attachmentName;
                    }else{
                        att.Name = attachmentName;
                    }
                    att.parentId = parentId;
                }
                upsert att;
                return new Response(true, '', att.Id);
            } else {
                return new Response(false, 'Attachment Body was null', null);
            }
            /*} else {
return 'Account could not be found';
}*/
        } else {
            return new Response(false, 'Could not found record with given Id', null);
        }
    }

    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments;
        try{
            attachments = [SELECT Id, Body, Name, ParentId
                           FROM Attachment
                           WHERE Id =: attId];
        }catch(Exception ex){
            System.debug('Exception '+ ex.getMessage());
        }

        if(attachments == null || attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }


    Public class Response{
        public boolean isSuccess;
        public String msg;
        public object data;
        public String redirectURL;
        public String datsheetId;

        Public Response(boolean isSuccess, String msg, object data){
            this.isSuccess = isSuccess;
            this.msg = msg;
            this.data = data;
        }

        Public Response(boolean isSuccess, String msg, object data, String redirectURL, String datsheetId){
            this.isSuccess = isSuccess;
            this.msg = msg;
            this.data = data;
            this.redirectURL = redirectURL;
            this.datsheetId = datsheetId;
        }

    }

    public class PageDataWrapper{
        public Map<string,List<selectOption>> PickValues;
        public String accountName;
        public String redirectURL;
    }

    Public class DwollaDataSheetWrappper{
        Public String recordId;
        Public String accountId;
        Public String address1;
        Public String address2;
        Public String  businessClassification;
        Public String businessType;
        Public boolean certified;
        Public Datetime certifiedDatetime;
        Public String city;
        Public String controllerAddress1;
        Public String controllerAddress2;
        Public String controllerAddress3;
        Public String controllerCity;
        Public Date controllerDateofBirth;
        Public String controllerFirstName;
        Public String controllerLastName;
        Public String controllerPostalCode;
        Public String controllerSSN;
        Public String controllerState;
        Public String controllerTitle;
        Public Date dateofBirth;
        Public String DBAName;
        Public String EIN;
        Public String emailAddress;
        Public String firstName;
        Public String lastName;
        Public String postalCode;
        Public String SSN;
        Public String State;
        public String accountName;
    }

    public class CalloutResponse {
        public String customerIdString;
        public String benificialOwnerId;
        public Boolean isAllCalloutSuccess;
        public String errorMessage;

        public CalloutResponse(String customerIdString,  String benificialOwnerId,
                               Boolean isAllCalloutSuccess, String errorMessage){
            this.customerIdString = customerIdString;
            this.benificialOwnerId = benificialOwnerId;
            this.isAllCalloutSuccess = isAllCalloutSuccess;
            this.errorMessage = errorMessage;
        }

    }
}