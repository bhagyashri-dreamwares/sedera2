@Istest
public class case_trigger_test2{

   @testSetup
   public static void insertData(){
      
      List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
         PriceList[0].name='Default Select Pricing';
         Update PriceList[0];
         List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
         AccessPricing[0].name='Default Access Pricing';
         AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
         insert AccessPricing[0];
         HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
         Test.setMock(HttpCalloutMock.class, Mockclass1);
         
        
         List <Account> EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(6, false);


         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         
         insert EmployerAccountList;

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         List <Account> MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(36, false);
         Integer memCount = 0;
         for (Account Employer: EmployerAccountList) {
             for (integer i = 1; i <= 6; i++) {
                 MemberAccountList[memCount].Account_Employer_name__C = Employer.id;
                 MemberAccountList[memCount].Date_of_Birth__c = date.today().addDays(-10);
                 MemberAccountList[memCount].subscription_status__c = 'Active';
                 MemberAccountList[memCount].IuA_Chosen__c = 500;
                 MemberAccountList[memCount].Dependent_status__C = 'EF';

                 memCount++;
                 if (Math.Mod(memCount, 6) == 0) {
                     break;
                 }
             }
         }

         UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
         insert MemberAccountList;
   
   
   
   }
   
   public static testmethod void test(){
   List<case> caseList=new List<case>();
   Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
   
    for (Contact con: [Select id, accountId from Contact where Account.recordType.name='Member']) {
            case cas;
            for (integer i = 0; i < 2; i++) {
                if (Math.mod(i, 2) == 0)
                    cas = new
                case (recordTypeId=NeedsRecType ,Third_Party_Payer__c = true, Initial_unsharable_amount2__c = 77, X2nd_MD_Consultation__c = true, iua_exception__c = true, status = 'new', Accountid = Con.Accountid, sharable_need__c = true, origin = 'email', contactid = con.id, Type = 'MEC', yearly_reset_flag__c = true);
                else
                    cas = new
                case (recordTypeId=NeedsRecType,Third_Party_Payer__c = true, Initial_unsharable_amount2__c = 80, IUA_exception__c = true, status = 'new', Accountid = Con.Accountid, sharable_need__c = false, origin = 'email', contactid = con.id, Type = 'MEC', yearly_reset_flag__c = true);

                caseList.add(cas);
            }
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        insert caseList;
         
         caseList.clear();
        
        for(Case cas:[Select id,sharable_need__c from case]){
           if(cas.sharable_need__C)
             cas.sharable_need__c=false;
           else
             cas.sharable_need__c=true;
             
             caseList.add(cas);
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        Update caseList;
   
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        delete caseList;
     
   }
   
    public static testmethod void test4(){
   List<case> caseList=new List<case>();
   Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
   Id PreventiveRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Preventive').getRecordTypeId();

   
    for (Contact con: [Select id, accountId from Contact where Account.recordType.name='Member']) {
            case cas;
            for (integer i = 0; i < 2; i++) {
                if (Math.mod(i, 2) == 0)
                    cas = new
                case (recordTypeId=NeedsRecType ,Type_of_Need__c ='Maternity',Third_Party_Payer__c = true, Initial_unsharable_amount2__c = 77, X2nd_MD_Consultation__c = true, iua_exception__c = true, status = 'new', Accountid = Con.Accountid, sharable_need__c = true, origin = 'email', contactid = con.id, Type = 'MEC', yearly_reset_flag__c = true);
                else
                    cas = new
                case (recordTypeId=PreventiveRecType,Type_of_Need__c ='Maternity',Third_Party_Payer__c = true, Initial_unsharable_amount2__c = 80, X2nd_MD_Consultation__c = true, status = 'new', Accountid = Con.Accountid, sharable_need__c = false, origin = 'email', contactid = con.id, Type = 'MEC', yearly_reset_flag__c = true);

                caseList.add(cas);
            }
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        insert caseList;
         
         caseList.clear();
        
        for(Case cas:[Select id,sharable_need__c from case]){
           if(cas.sharable_need__C){
             cas.recordTypeId=PreventiveRecType;
             cas.X2nd_MD_Consultation__c =false;
           } 
           else{
             cas.recordTypeId=NeedsRecType;
           }           
          caseList.add(cas);
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        Update caseList;
   
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        delete caseList;
        
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        Undelete caseList;
     
   }
   
       public static testmethod void test5(){
   List<case> caseList=new List<case>();
   List < bill__c > billList = new List < bill__c > ();

   Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
   Id PreventiveRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Preventive').getRecordTypeId();

   
    for (Contact con: [Select id, accountId from Contact where Account.recordType.name='Member'limit 2]) {
            case cas;
            for (integer i = 0; i < 6; i++) {
                if (Math.mod(i, 2) == 0)
                    cas = new
                case (recordTypeId=NeedsRecType ,Type_of_Need__c ='Maternity',Third_Party_Payer__c = true, Initial_unsharable_amount2__c = 77, X2nd_MD_Consultation__c = true, iua_exception__c = true, status = 'new', Accountid = Con.Accountid, sharable_need__c = true, origin = 'email', contactid = con.id, Type = 'MEC', yearly_reset_flag__c = true);
                else
                    cas = new
                case (recordTypeId=NeedsRecType,Type_of_Need__c ='Maternity',Third_Party_Payer__c = true, Initial_unsharable_amount2__c = 80, X2nd_MD_Consultation__c = true, status = 'new', Accountid = Con.Accountid, sharable_need__c = false, origin = 'email', contactid = con.id, Type = 'MEC', yearly_reset_flag__c = true);

                caseList.add(cas);
            }
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        insert caseList;
         
         caseList.clear();
        
        for(Case cas:[Select id,sharable_need__c from case]){
           if(cas.sharable_need__C){
             cas.recordTypeId=PreventiveRecType;
             cas.X2nd_MD_Consultation__c =false;
           }             
          caseList.add(cas);
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        Update caseList;
   
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        delete caseList;
        
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;
        Undelete caseList;
        
        for (case cas: [Select id,contactId,AccountId from case]) {
            bill__c bil = new bill__c(patient_name__c=cas.contactId,member__c=cas.AccountId,approval_status__c ='Approved',Balance_Payable_by_Member__c = 100, case__c = cas.id, Paid_by_Patient_Amount__c = 200, Paid_by__c = 100, status__c = 'Received-Not to be Negotiated');
            BillList.add(bil);
        }
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;

        insert BillList;
        BillList.clear();
        
        for(bill__c bill:[select id,approval_status__c  from bill__c]){       
            bill.approval_status__c ='Not Approved';          
            BillList.add(bill);
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForBillTrigger=0;
        UtilityClass_For_Static_Variables.CheckRecursiveForCaseTrigger = 0;

        Update BillList;
   }
   
}