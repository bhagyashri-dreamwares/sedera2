/**
 * Purpose : Class to generate mock response for http callout
*/
@isTest    
  
public class MockResponseForQBAccountSyncPushFault implements HttpCalloutMock {
    public HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        if( req.getEndpoint() == 'https://oauth.platform.intuit.com/oauth2/v1/token' ) {
            res.setBody('{"refresh_token":"refresh_tokenhjbhsbvhbsdvhjbdsvh","token_type":"token_typedgdd","access_token":"access_tokendgdd","expires_in":100,"x_refresh_token_expires_in":100,"realm_id":"realm_iddgdd"}');
        }
        else{
            
            res.setBody('{"BatchItemResponse":[{"Fault":{"Error":[{"Message":"Duplicate Name Exists Error","Detail":"The name supplied already exists. : Another customer, vendor or employee is already using this name. Please use a different name.","code":"6240","element":""}],"type":"ValidationFault"},"bId":"bid11"}],"time":"2017-10-16T07:15:32.859-07:00"}');
        }        
        res.setStatusCode(200);
        System.debug('Mock respose :::'+res);
        return res;
    }    
}