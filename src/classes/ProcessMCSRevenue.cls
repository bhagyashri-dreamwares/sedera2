global class ProcessMCSRevenue implements Database.Batchable<SObject>, Database.Stateful
{
    //Keep up with created invoices
    //Map of Employer Id to Invoice ID
    global final Map<ID,Invoice__c> accountInvoiceMap = new Map<ID,Invoice__c>();
    
    //Map MCS Additonal Product Base - Teledoc & 2nd MD
    global final Map<String,Decimal> mcsProductMap = new Map<String,Decimal>();

    public Database.QueryLocator start(Database.BatchableContext context)
    {
        Date dtf;
        if(system.today().day() == 1)
        {
            dtf = date.today();
        }
        else
        {
            dtf = date.today().addMonths(1).toStartOfMonth();
        }
                
        return Database.getQueryLocator([SELECT id,
                                             product2Id,
                                             Product2.name,
                                             Product2.productcode,
                                             product2.price__c,
                                             Employer_Account_ID__c,    
                                             AccountId,
                                             Account.Account_Employer_Name__c,
                                             Account.Health_Care_Sharing__c,
                                             Account.Dependent_Status__c,
                                             Account.Subscription_Status__c,
                                             Account.Primary_Age__c,
                                             Account.Months_Enrolled__c,
                                         	 Account.Account_Employer_Name__r.MCS_Existing_Member_Revenue__c,
                                             Account.Account_Employer_Name__r.MCS_New_Member_Revenue__c 
                                             FROM Asset
                                             WHERE (Account.Subscription_Status__c='Pending Start Date' or 
                                                    Account.Subscription_Status__c='Active') 
                                         	 AND Account.COBRA__c = FALSE 
                                         	 AND Account.enrollment_date__c<=:dtf
                                             ]);
    }

    public void execute(Database.BatchableContext context, List<Asset> scope)
    {
        Set<ID> accountSet = new Set<ID>();
        Set<ID> assetIds = new Set<ID>();
        
        for(Asset ast : scope)
        {
            accountSet.add(ast.Account.Account_Employer_Name__c);
            assetIds.add(ast.id);
        }
 
        if(accountInvoiceMap.size() == 0)
        {
            Date dtf;
            String statusStr;
            if(system.today().day() == 1)
            {
                statusStr = 'Approved';
                dtf = date.today().addDays(-13);
            }
            else
            {
                statusStr = 'In Review';
                dtf = date.today().addDays(-13);
            }
            
            List<Invoice__c> invoicesToUpdate = [SELECT ID, 
                                                  Account__r.id,
                                                  ENA__c,
                                                  Invoice_Date__c, 
                                                  Submission_Status__c,
                                                  Gross_Monthly_Total_Existing__c ,
                                                  Gross_Monthly_Total_New__c ,
                                                  MCS_Existing_Member_Revenue_Percent__c,
                                                  MCS_New_Member_Revenue_Percent__c,
                                                  Member_Services_Revenue__c, 
                                                  X2nd_MD_Total__c,
                                                  Teledoc_Total__c 
                                                  FROM Invoice__c 
                                                  WHERE Invoice_Date__c >= :dtf
                                                  AND Submission_Status__c = :statusStr];
            
            for(Invoice__c inv : invoicesToUpdate)
            {
                inv.Gross_Monthly_Total_Existing__c   = 0;
                inv.Gross_Monthly_Total_New__c   = 0;
                inv.X2nd_MD_Total__c = 0;
                inv.Teledoc_Total__c = 0;
                inv.Member_Services_Revenue__c  = 0;
                inv.ENA__c = 15;
                
                accountInvoiceMap.put(inv.Account__r.id,inv);
            }

        }
        mcsProductMap.putAll(getMCSProductBase());
        processInvoices(scope);

    }

    public void finish(Database.BatchableContext context)
    {
        List<Invoice__c> invoicesToUpdate = accountInvoiceMap.values();
        update invoicesToUpdate;

    }

    public void processInvoices(List<Asset> assetList)
    {
        Decimal x2ndMd = mcsProductMap.get('2nd MD');
        Decimal teledoc = mcsProductMap.get('Teledoc');
		for(Asset ast :assetList)
        {
            if( accountInvoiceMap.get(ast.Employer_Account_ID__c) != null )
            {
                Invoice__c inv = accountInvoiceMap.get(ast.Employer_Account_ID__c);
                Decimal months = ast.Account.Months_Enrolled__c + 1;
                String productName = ast.Product2.name;
                
                if(inv.MCS_Existing_Member_Revenue_Percent__c == null)
                	inv.MCS_Existing_Member_Revenue_Percent__c = ast.Account.Account_Employer_Name__r.MCS_Existing_Member_Revenue__c;
                if(inv.MCS_New_Member_Revenue_Percent__c == null)
                	inv.MCS_New_Member_Revenue_Percent__c = ast.Account.Account_Employer_Name__r.MCS_New_Member_Revenue__c;
                if(inv.Member_Services_Revenue__c  == null)
                    inv.Member_Services_Revenue__c  = 0;
                if(inv.Gross_Monthly_Total_New__c   == null)
                	inv.Gross_Monthly_Total_New__c  = 0;
                if(inv.Gross_Monthly_Total_Existing__c  == null)
                	inv.Gross_Monthly_Total_Existing__c  = 0;
                if(inv.X2nd_MD_Total__c == null)
                	inv.X2nd_MD_Total__c = 0;
                if(inv.Teledoc_Total__c == null)
                	inv.Teledoc_Total__c = 0;
                if(inv.ENA__c == null)
                	inv.ENA__c = 15;
                
                if(months <= 3 && (productName.contains('Select') || productName.contains('Access') ) ) 
                {
                	inv.Gross_Monthly_Total_New__c  = inv.Gross_Monthly_Total_New__c  + ast.product2.price__c; 
                	inv.X2nd_MD_Total__c = inv.X2nd_MD_Total__c + x2ndMD;
                    inv.Teledoc_Total__c = inv.Teledoc_Total__c + teledoc;
                }
                else if(months <= 3 && (productName.contains('Preventive') || productName.contains('Tobacco')) )
                {
                    inv.Gross_Monthly_Total_New__c  = inv.Gross_Monthly_Total_New__c  + ast.product2.price__c;
                }
                else if(productName.contains('Select') || productName.contains('Access'))
                {
                    //system.debug('price  =' + ast.product2.price__c);
                    //system.debug('multiply  =' + inv.MCS_Existing_Member_Revenue_Percent__c/100);
                    inv.Gross_Monthly_Total_Existing__c  = inv.Gross_Monthly_Total_Existing__c  + ast.product2.price__c;
                	inv.X2nd_MD_Total__c = inv.X2nd_MD_Total__c + x2ndMD;
                    inv.Teledoc_Total__c = inv.Teledoc_Total__c + teledoc;
                }
                else if(productName.contains('Preventive') || productName.contains('Tobacco'))
                {
                    inv.Gross_Monthly_Total_Existing__c  = inv.Gross_Monthly_Total_Existing__c  + ast.product2.price__c;
                }
                else if(productName.contains('Member'))
                {
                    inv.Member_Services_Revenue__c  = inv.Member_Services_Revenue__c  + ast.product2.price__c;       
                }
                
            }
        }
    }
    
    private static Map<String,Decimal> getMCSProductBase()
    {
        Map<String, MCS_Products__c > mcsProductMap = MCS_Products__c.getAll();
        Map<String, Decimal> productMap = new Map<String, Decimal>();
        
        if(!mcsProductMap.isEmpty())
        {            
            
            for(MCS_Products__c productConfig :mcsProductMap.values())
            {
                productMap.put(productConfig.Product_Name__c, productConfig.Base_Amount__c);
            }
        }
        
        return productMap;
    }

}