@IsTest
public class SchedulerToRefreshQBAccessTokenTest {
	
    /*
     * Purpose : method to create required data
    */
    @testSetup
    static void dataCreation(){
         Quick_Book_App_Configuration__c setting = new Quick_Book_App_Configuration__c(
                Name = 'Quickbooks', 
                Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
                Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/token',
                Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
                Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
                Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
                Is_Sandbox__c = true,
                Production_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com', 
                QB_Company_Id__c = '123145860463979',
                Redirect_URI__c = 'https://air--fsmsandbox--c.cs14.visual.force.com',
                Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
                Refresh_Token_Expiry__c = DateTime.now().addDays(1),                
                Sandbox_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com'
        );
        insert setting;    
        
        setting = new Quick_Book_App_Configuration__c(
                Name = 'Quickbooks2', 
                Access_Token_Part_1__c = 'qwertyuiopsdcfvgbhnjm', 
                Access_Token_Part_2__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_3__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_4__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_Part_5__c = 'qwertyuiopsdcfvgbhnjm',
                Access_Token_URL__c = 'https://oauth.platform.intuit.com/oauth2/v1/token',
                Authorization_URL__c = 'https://appcenter.intuit.com/connect/oauth2',
                Consumer_Key__c  = 'qwertyuiopsdcfvgbhnjm', 
                Consumer_Secrete__c  = 'qwertyuiopsdcfvgbhnjm', 
                Is_Sandbox__c = true,
                Production_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com', 
                QB_Company_Id__c = '123145860463979',
                Redirect_URI__c = 'https://air--fsmsandbox--c.cs14.visual.force.com',
                Refresh_Token__c  = 'qwertyuiopsdcfvgbhnjm',
                Refresh_Token_Expiry__c = DateTime.now().addDays(1),                
                Sandbox_Endpoint_Url__c = 'https://air--fsmsandbox--c.cs14.visual.force.com'
        );
        insert setting;    
    }
    
    @isTest
    static void testScheduler(){
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class, new MockResponseForQBAccountSyncPush());      
        String strCronExp = '0 0 1 * * ?'; 
        String jobID = system.schedule('Test scheduler', strCronExp, new SchedulerToRefreshQBAccessToken());
          
        SchedulerToRefreshQBAccessToken.schedule();
        Test.stopTest();
    }
    
    @isTest
    static void testScheduler2(){
        Test.startTest(); 
        Test.setMock(HttpCalloutMock.class, new MockResponseForQBAccountSyncPush());      
        String strCronExp = '0 0 1 * * ?'; 
        String jobID = system.schedule('Test scheduler', strCronExp, new SchedulerToRefreshQBAccessToken2());
          
        SchedulerToRefreshQBAccessToken2.schedule();
        Test.stopTest();
    }
}