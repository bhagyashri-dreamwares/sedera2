/*  
@Purpose      : Handler class of AccountTrigger
- Invoked from trigger
@Created Date : 19/07/2018
*/
public class AccountTriggerHandler  {
    /* 
@Purpose : Method to handle insert Account
- Invoked from Account trigger
@Parameter : List<Account> (Trigger.new)
@returns   : -
*/  
    public void insertMethod(List<Account> newAccountRecordList) {
        Boolean isFirstBatchExicute = false;
        Set<Id> newAccountIdSet = new Set<Id>();
        System.debug('insertMethod is : ');
        //Check Sync_to_QBO__c field
        for (Account accountRecord : newAccountRecordList) {
            //If Sync_to_QBO__c is true then add account id into the set newaccountIdSet
            if (accountRecord.Sync_to_QBO__c ) {
                newaccountIdSet.add(accountRecord.Id);      
            }
        }
        if (!newaccountIdSet.isEmpty()) {
            QBAccountSyncPushBatch qbAccountSync= new QBAccountSyncPushBatch(newaccountIdSet,'Quickbooks');
            qbAccountSync.setAccountIds = newaccountIdSet;
            Database.executeBatch(qbAccountSync, 30);
            isFirstBatchExicute = true;
        } 
        if (!newaccountIdSet.isEmpty() && isFirstBatchExicute != false) {
            QBAccountSyncPushBatch qbAccountSync= new QBAccountSyncPushBatch(newaccountIdSet,'Quickbooks2');
            qbAccountSync.setAccountIds = newaccountIdSet;
            Database.executeBatch(qbAccountSync, 30);
        } 
        
    }
    
    /* 
@Purpose : Method to handle update Account
- Invoked from Account trigger
@Parameter : Trigger.newMap & Trigger.oldMap
@returns   : -
*/  
    public void updateMethod(Map<Id, Account> newaccountRecordMap, Map<Id, Account> oldaccountRecordMap){
        // for testing only
        /*Account oldAccountRecord;  
for (Account accountRecord : newaccountRecordMap.values()) {
oldAccountRecord = oldaccountRecordMap.get(accountRecord.Id);
system.debug((accountRecord.Total_Billing_Contacts__c == oldAccountRecord.Total_Billing_Contacts__c)+' Total_Billing_Contacts__c old '+
oldAccountRecord.Total_Billing_Contacts__c+'  new '+accountRecord.Total_Billing_Contacts__c );

if(accountRecord.Total_Billing_Contacts__c == oldAccountRecord.Total_Billing_Contacts__c){
system.debug('Total_Billing_Contacts__c is changed of '+accountRecord.Id);
}
}*/
        
        
        Set<Id> accountIdSet = new Set<Id>();
        Set<Id> accountIdSet2 = new Set<Id>();
        Account oldAccountRecord;  
        Boolean isFirstBatchExicute = false;
        for (Account accountRecord : newaccountRecordMap.values()) {
            oldAccountRecord = oldaccountRecordMap.get(accountRecord.Id);
            if (accountRecord.Sync_to_QBO__c  && 
                (accountRecord.QB_Sync_Token__c == oldaccountRecordMap.get(accountRecord.Id).QB_Sync_Token__c || 
                 (String.isBlank(accountRecord.QuickBooks_Customer_ID__c)  
                  && String.isBlank(accountRecord.QB_Sync_Token__c)))) {
                      accountIdSet.add(accountRecord.Id);          
                  }
            
            if(oldAccountRecord.QB_Sync_Token__c != NULL &&  
               (accountRecord.QB_Sync_Token__c == NULL || 
                ( Integer.valueOf(oldAccountRecord.QB_Sync_Token__c) > Integer.valueOf(accountRecord.QB_Sync_Token__c))) ) {
                    accountRecord.addError('Please refresh page once before saving.');
                }
            
            // FOR QUICKBOOK ORG 2
            
            if (accountRecord.Sync_to_QBO__c  && 
                (accountRecord.QB_Sync_Token2__c == oldaccountRecordMap.get(accountRecord.Id).QB_Sync_Token2__c ||
                 (String.isBlank(accountRecord.QuickBooks_Customer_ID2__c)  && 
                  String.isBlank(accountRecord.QB_Sync_Token2__c)))) {
                      accountIdSet2.add(accountRecord.Id);          
                  }
            
            if(oldAccountRecord.QB_Sync_Token2__c != NULL &&  
               (accountRecord.QB_Sync_Token2__c == NULL || 
                ( Integer.valueOf(oldAccountRecord.QB_Sync_Token2__c) > Integer.valueOf(accountRecord.QB_Sync_Token2__c))) ) {
                    accountRecord.addError('Please refresh page once before saving.');
                }
        }
        
        if (!accountIdSet.isEmpty()) {
            QBAccountSyncPushBatch qbAccountSync= new QBAccountSyncPushBatch(accountIdSet,'Quickbooks');
            Database.executeBatch(qbAccountSync, 30);
            isFirstBatchExicute = true;
        }   
        if (!accountIdSet2.isEmpty() && isFirstBatchExicute != false) {
            QBAccountSyncPushBatch qbAccountSync= new QBAccountSyncPushBatch(accountIdSet2,'Quickbooks2');
            Database.executeBatch(qbAccountSync, 30);
        }   
    } 
    
    /* 
@Purpose : To maintain Hiearchy from parent to child in QB.
@Parameter : Trigger.newMap & Trigger.oldMap
@returns   : -
*/
    public void updateQBHierarchy(Map<Id, Account> newaccountRecordMap, Map<Id, Account> oldaccountRecordMap){
        Set<Id> accountIdSet = new Set<Id>();
        Set<Id> accountIdSet2 = new Set<Id>();
        Account oldRecord;
        Boolean isFirstBatchExicute = false;
        for(Account accountRecord : newaccountRecordMap.values()){
            oldRecord = oldaccountRecordMap.get(accountRecord.Id);  if(accountRecord.Sync_to_QBO__c &&  String.isNotBlank(accountRecord.QuickBooks_Customer_ID__c) &&  (accountRecord.Qb_Sync_Status__c.equalsIgnoreCase('Success') &&  (oldRecord.Qb_Sync_Status__c == NULL ||  !oldRecord.Qb_Sync_Status__c.equalsIgnoreCase('Success'))) ){ accountIdSet.add(accountRecord.ID);
              }
            
            // FOR QUICKBOOK ORG 2
            
            if(accountRecord.Sync_to_QBO__c && String.isNotBlank(accountRecord.QuickBooks_Customer_ID2__c) && (accountRecord.QB_Sync_Status2__c.equalsIgnoreCase('Success') &&  (oldRecord.QB_Sync_Status2__c == NULL ||  !oldRecord.QB_Sync_Status2__c.equalsIgnoreCase('Success'))) ){  accountIdSet2.add(accountRecord.ID);
              }
        }
        Set<Id> childIdsToBeSync = new Set<Id>();
        childIdsToBeSync = getChildIds(accountIdSet);
        if(childIdsToBeSync != NULL && !childIdsToBeSync.isEmpty()){
            System.debug('isFirstBatchExicute :: '+isFirstBatchExicute);
            QBAccountSyncPushBatch qbAccountSync= new QBAccountSyncPushBatch(childIdsToBeSync,'Quickbooks'); Database.executeBatch(qbAccountSync, 30); isFirstBatchExicute = true;
        }
        childIdsToBeSync = getChildIds(accountIdSet2);
        if(childIdsToBeSync != NULL && !childIdsToBeSync.isEmpty()&& isFirstBatchExicute != false) {
            System.debug('isFirstBatchExicute :: '+isFirstBatchExicute);
            System.debug('In Second Quickbook org'); QBAccountSyncPushBatch qbAccountSync= new QBAccountSyncPushBatch(childIdsToBeSync,'Quickbooks2'); 
        }
    }
    
    /* 
@Purpose : To get the child record ids of account records
@Parameter : Set<Id> parentAccountIds
@returns   : Set<Id>
*/
    public Set<Id> getChildIds(Set<Id> parentAccountIds){
        Set<Id> childIds = new Set<Id>();
        if(parentAccountIds != NULL && !parentAccountIds.isEmpty()){
            Map<Id,Account> accounts = new Map<Id,Account>();
            
            accounts = new Map<Id,Account>([SELECT Id, Name 
                                            FROM Account 
                                            WHERE ParentId IN : parentAccountIds]);
            childIds = accounts.keySet();
        }
        
        return childIds;
    }    
}