@isTest
public class DwollaWebServiceTest {

    @TestSetup
    static void makeData(){
      
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
        
        Account account = new Account(Name = 'test account', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId());
        insert account;
        
        Dwolla_Datasheet__c dwollaDataSheet = new Dwolla_Datasheet__c(SSN__c = '123-46-7890',
            EIN__c = '00-0000000', Controller_SSN__c = '123-46-7890', First_Name__c = 'TestFirst', Last_name__c = 'TestLast',
            DBA_Name__c = 'Test DBA',Email_Address__c = 'abc@test.com', Account__c = account.Id);
        insert dwollaDataSheet;
        
        //Name, Account__c, First_Name__c, Last_Name__c, Email_Address__c, DBA_Name__c

        List<Dwolla_Funding_Source__c> dwollaFundingSourceList = new List<Dwolla_Funding_Source__c>();

        Dwolla_Funding_Source__c dwollaFundingSource1 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
            Type__c = 'Customer');
        dwollaFundingSourceList.add(dwollaFundingSource1);

        Dwolla_Funding_Source__c dwollaFundingSource2 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
            Type__c = 'Balance');
        dwollaFundingSourceList.add(dwollaFundingSource2);

        insert dwollaFundingSourceList;
        
        Invoice__c invoice = new Invoice__c(Account__c = account.id);
        insert invoice;

        List<Dwolla_Transfer__c> dwollaTransferList = new List<Dwolla_Transfer__c>();
        
        Dwolla_Transfer__c dwollaTransferType1 = new Dwolla_Transfer__c(Source_Dwolla_Funding_Source__c = dwollaFundingSource1.Id,
            Dwolla_Transfer_ID__c = 'testresourceId2', 
            Transfer_Status__c = 'Initiated',
            Invoice__c = invoice.id);
        dwollaTransferList.add(dwollaTransferType1);
        
         

        Dwolla_Transfer__c dwollaTransferType2 = new Dwolla_Transfer__c(Source_Dwolla_Funding_Source__c = dwollaFundingSource2.Id,
            Dwolla_Transfer_ID__c = 'testresourceId3',
            Transfer_Status__c = 'Initiated',
            Invoice__c = invoice.id);
        dwollaTransferList.add(dwollaTransferType2);

        insert dwollaTransferList;
        
    }

    @isTest
    static void processEventTest1(){

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"testtopic","resourceId":"testresourceId1"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        DwollaWebService.processEvent();

        Test.stopTest();
    }

    @isTest
    static void processEventTest2(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_bank_transfer_completed","resourceId":"testresourceId2"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        DwollaWebService.processEvent();

        Test.stopTest();
    }

    @isTest
    static void processEventTest3(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_bank_transfer_completed","resourceId":"testresourceId3"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        DwollaWebService.processEvent();

        Test.stopTest();
    }
    
    /*
     * @Purpose : To check functionality (Negative Testing)
     */ 
    
    @isTest
    static void processEventTest4(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_bank_transfer_created","resourceId":"testresourceId4"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Dwolla_Funding_Source__c dwollaFundingSource1 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
                                                                                      Type__c = 'Customer');
        insert dwollaFundingSource1;
        
        Dwolla_Transfer__c dwollaTransferType1 = new Dwolla_Transfer__c(Source_Dwolla_Funding_Source__c = dwollaFundingSource1.Id,
                                                                        Dwolla_Transfer_ID__c = 'testresourceId4', 
                                                                        Transfer_Status__c = 'Initiated');
        insert dwollaTransferType1;

        DwollaWebService.processEvent();
        
        Test.stopTest();
    }
    
    /*
     * @Purpose : To check functionality (Positive Testing)
     */ 
    @isTest
    static void processEventTest5(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_bank_transfer_created","resourceId":"testresourceId5"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();
        
        Account account = [SELECT Id FROM Account LIMIT 1];
        Dwolla_Funding_Source__c dwollaFundingSource1 = new Dwolla_Funding_Source__c(Employer_Account__c = account.Id,
                                                                                      Type__c = 'Customer');
        insert dwollaFundingSource1;
        
        Dwolla_Transfer__c dwollaTransferType1 = new Dwolla_Transfer__c(Source_Dwolla_Funding_Source__c = dwollaFundingSource1.Id,
                                                                        Dwolla_Transfer_ID__c = 'testresourceId5', 
                                                                        Transfer_Status__c = 'Initiated');
        insert dwollaTransferType1;
        
        String result = DwollaWebService.processEvent();
        
        Test.stopTest();
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
   /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest6(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_created","resourceId":"testresourceId6"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest();
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
   /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest7(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_funding_source_verified","resourceId":"testresourceId7"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
   /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest8(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_funding_source_added","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    } 
        
    /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest9(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_bank_transfer_cancelled","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }    
    
    /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest10(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_bank_transfer_failed","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }  
    
     /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest11(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_suspended","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
    /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest12(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_funding_source_removed","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
    /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest13(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_verification_document_needed","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
     /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest14(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_verification_document_uploaded","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
     /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest15(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_verification_document_failed","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
    
     /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest16(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_verification_document_approved","resourceId":"testresourceId8"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest(); 
        
        System.assertEquals('WebService Successfully Ran', result);
    }
        
    /*
    * @Purpose : To check functionality (Positive Testing)
    */
    @isTest
    static void processEventTest17(){

        System.debug([SELECT Id, Dwolla_Transfer_ID__c, Transfer_Type__c FROM Dwolla_Transfer__c]);

        RestRequest request = new RestRequest();
        request.requestBody = Blob.valueOf('{"_links":{"self":{"href":"testself"},"resource":{"href":"testresource"},"account":{"href":"testaccount"}},"id":"testid","created":"2019-01-02T00:00:00.000Z","topic":"customer_verified","resourceId":"testresourceId6"}');

        RestContext.request = request;
        RestContext.response = new RestResponse();

        Test.startTest();

        String result = DwollaWebService.processEvent();

        Test.stopTest();
        
        System.assertEquals('WebService Successfully Ran', result);
    }
}