/**
*@Purpose: Batch class to fetch EONS related email status from paubox API
*@Date: 21/08/2018
*/
public without sharing class EONSEventBatch implements Database.Batchable<EONS_Shared__c>, Database.AllowsCallouts, Database.Stateful{

   public List<EONS_Shared__c> eONSRecordList;
   public List<EONS_Shared__c> eonsList;

   public EONSEventbatch(List<EONS_Shared__c> eONSRecordList){
      
      this.eonsList = new List<EONS_Shared__c>();
      this.eONSRecordList = new List<EONS_Shared__c>();
      this.eONSRecordList.addAll(eONSRecordList);       
   }

   public Iterable<EONS_Shared__c> start(Database.BatchableContext BC) {
        return eONSRecordList;
   }

   public void execute(Database.BatchableContext BC, List<EONS_Shared__c> eONSRecordList){
   
       for(EONS_Shared__c eONSRecord :eONSRecordList){            
            
            EONS_Shared__c eons = PauBoxEmailHelper.getEmailStatus(eONSRecord.Tracking_ID__c, eONSRecord.Id);
            if(eons != null){
                eonsList.add(eons);
            }            
       }  
   }

   public void finish(Database.BatchableContext BC){
       
       if(!eonsList.isEmpty()){
           PauBoxEmailHelper.saveEONSRecord(eonsList);
       }
   }
}