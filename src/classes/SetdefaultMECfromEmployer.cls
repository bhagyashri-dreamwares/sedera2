/*
TestClass:SetdefaultMECfromEmployer_test
*/

public without sharing class SetdefaultMECfromEmployer {
    public List <AvailableMEC> availableMEC {
        get;
        set;
    }
    public String selectedValue {
        get;
        set;
    }
    public Id Empid {
        get;
        set;
    }

    public Boolean IsError{
       get;
       set;
    }
    
    public string CurrentDefaultMec{
       get;
       set;
    
    }
    
    public string CurrentDefaultMecAss{
       get;
       set;
    
    }

    public SetdefaultMECfromEmployer(ApexPages.standardcontroller sc) {
        
        
        CurrentDefaultMec='None';
        
        Empid = ApexPages.currentPage().getParameters().get('id');
        List <AccountMECAssociation__c> MEClist = [select id, Name,MEC_Product__r.Name,Default_MEC_Product__c from AccountMECAssociation__c where Employer_Account__c =: Empid];


        availableMEC = new List <AvailableMEC> ();
        if (MEClist.size()> 0) {
            for (AccountMECAssociation__c a: MEClist) {
                availableMEC.add(new availableMEC(a.id, a.MEC_Product__r.Name));
                if(a.Default_MEC_Product__c==true)
                {                 
                  CurrentDefaultMec=a.MEC_Product__r.Name;
                  CurrentDefaultMecAss=a.id;
                }
            }
            
        }
        IsError=false;
    }

    public PageReference UpdateDefaultMEC() {
    
       try{
        List <AccountMECAssociation__c> AmecToUpdate = new list <AccountMECAssociation__c> ();
        if (selectedValue != '' && selectedValue != NULL && CurrentDefaultMecAss!=selectedValue) {
            for (AccountMECAssociation__c Amec: [select id, Name, Default_MEC_Product__c from AccountMECAssociation__c where Employer_Account__c =: Empid]) {
                if (selectedValue == Amec.id) {
                    Amec.Default_MEC_Product__c = true;
                } else {
                    Amec.Default_MEC_Product__c = false;
                }
                AmecToUpdate.add(Amec);
            }

            if (AmecToUpdate.size()> 0)
                update AmecToUpdate;
            //Below code reloads the page
            list <Account> MembersToUpdate = new list <Account> ();
                       
            for (Account EmpAcc: [Select id, (Select MEC_Product__r.Name, MEC_Product__r.Id, MEC_Product__r.Discount_Tier__c from AccountsMECsAssociation__r where Default_MEC_Product__c = true limit 1), (Select id,parent_product__c,Account_employer_name__c,teladoc_direct__c,RecordTypeId,subscription_status__c,Dependent_status__c,member_discount_tier_manual__c,SecondMD__c,Iua_chosen__c,primary_Age__c  from Accounts__r) from Account where id =: Empid]) {                                  
                                 
                  for (Account memAcc: EmpAcc.Accounts__r) {             
                       memAcc.Member_Discount_Tier_Manual__c = EmpAcc.AccountsMECsAssociation__r[0].MEC_Product__r.Discount_Tier__c;
                       memAcc.Member_MEC_Product__c=EmpAcc.AccountsMECsAssociation__r[0].MEC_Product__r.name;
                       MembersToUpdate.add(memAcc);
                    }             
                 
            }
            if(MembersToUpdate.size()>0){
            Account_Helper_class.MCSproductcalculation(MembersToUpdate,null);  
            }
             
            if (MembersToUpdate.size()>100) {
               system.debug('Triggering Queueable');              
               system.EnqueueJob(new Queueable_UpdateAccountsinbulk(MembersToUpdate,true,true));
            } else if(MembersToUpdate.size()>0){     
               UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
               UtilityClass_For_Static_Variables.CheckRecursiveForAccountTriggerQB=1;
               system.debug(MembersToUpdate);
               Update MembersToUpdate;
            }


            return null;
        }


        }catch(Exception e)
        {
            IsError=true;
            System.debug('Exception >>>>>> '+e.getMessage());
        }
        
               return null;
 
    }

    /*** Class to Leverage show Row Data on VF ***/

    public class availableMEC {
        public String value {
            get;
            set;
        }
        public string name {
            get;
            set;
        }

        public availableMEC(String Value, string name) {
            this.Value = Value;
            this.name = name;
        }
    }
}