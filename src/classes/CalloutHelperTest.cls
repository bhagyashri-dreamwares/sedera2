/* 
	* @purpose : Test Class for CalloutHelper.
*/

@isTest
public class CalloutHelperTest {
	/* 
	* @purpose : create Test Data.
	*/
    @TestSetup
    public static void setup(){
        Id recordTypeIdAccountEmp = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
            Account accountRecord1 = new Account();
            accountRecord1.Name='Test Account';
            accountRecord1.Create_Dwolla_Customer__c=true;
            accountRecord1.Invoice_Email_Address__c='test@test.com';
            accountRecord1.Dwolla_ID__c='12345';
            accountRecord1.Enrollment_Date__c=system.today();
            accountRecord1.Member_Discount_Tier_Manual__c='T2';
            accountRecord1.Member_MEC_Product__c='test';
            accountRecord1.recordtypeid = recordTypeIdAccountEmp;
        
          Account accountRecord2 = new Account();
            accountRecord2.Name='Test Account';
            accountRecord2.Create_Dwolla_Customer__c=true;
            accountRecord2.Invoice_Email_Address__c='test@test.com';
            accountRecord2.Dwolla_ID__c='123450';
            accountRecord2.Enrollment_Date__c=system.today();
            accountRecord2.Member_Discount_Tier_Manual__c='T2';
            accountRecord2.Member_MEC_Product__c='test';
            accountRecord2.recordtypeid = recordTypeIdAccountEmp;
            
        insert accountRecord1;
        insert accountRecord2;
        
        //custom setting 
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today()+6;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj; 
    }

    /* 
	* @purpose : test method for httpPostRequest.
	*/
    
    public static testmethod void httpPostRequestTest(){
        
        string retunString;
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        Account account = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                           Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                           Member_MEC_Product__c
                           FROM Account LIMIT 1];
        Test.startTest();
        DwollaCalloutHelper classObj = new DwollaCalloutHelper();
        retunString = classObj.createCustomer(account);
        Test.stopTest();
        system.assertEquals('', retunString);        
    }
    
	/* 
	* @purpose : test method for httpPostRequest.
	*/
    
    public static testmethod void httpPostRequestNegativeTest(){
        
        string retunString;
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(0));
        
        Account account = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                           Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                           Member_MEC_Product__c
                           FROM Account LIMIT 1];
        Test.startTest();
        DwollaCalloutHelper classObj = new DwollaCalloutHelper();
        retunString = classObj.createCustomer(account);
        Test.stopTest();
        system.assertEquals(' test set header', retunString);
    }
    
    public static testmethod void test23(){
        
        Account account = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                           Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                           Member_MEC_Product__c
                           FROM Account LIMIT 1];
        String endPoint ='http://test.com';
        Map<String,String> endPointParameterMap = new Map<String,String>{'a' => 'b', 'c' => 'd'};
        Map<String,String> headerParameterMap = new Map<String,String>{'a' => 'b', 'c' => 'd'};
        
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
            
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstName', account.Name);
        gen.writeStringField('lastName', account.Name);
        gen.writeStringField('email', account.Invoice_Email_Address__c);
       //  gen.writeStringField('type', 'unverified customer');
        gen.writeStringField('businessName', account.Name);
        gen.writeEndObject();
        
        test.startTest();
        HttpResponse actualResponse = CalloutHelper.httpPostRequest( endPoint, endPointParameterMap, headerParameterMap, gen);
        HttpResponse actualResponseGet = CalloutHelper.httpGetRequest( endPoint, endPointParameterMap, headerParameterMap);
        test.stopTest();
        system.debug('actualResponse.getStatusCode()'+actualResponse.getStatusCode());
        system.debug('actualResponse'+actualResponse);
        system.assertEquals(200, actualResponse.getStatusCode());
    }
    

}