/*
*@Purpose : Test method for CommissionEmailBatch class
*/
@isTest
public class CommissionEmailBatchTest
{
    
    /*
    * To initialise data
    */    
    @testSetup
    private static void initData(){
        
        //insert custom setting record
        PauBoxConfiguration__c config = new PauBoxConfiguration__c();
        config.Access_Token__c = 'Test';
        config.Email_Subject__c = 'Test';
        config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
        config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
        config.From_Email__c = 'Test@test.com';
        config.Site_Url__c = 'Test@test.com';
        insert config;
        
        // insert Paubox_Message_Template
        Paubox_Message_Template__c template = new Paubox_Message_Template__c();
        template.Paubox_Message_Type__c = 'Invoice';
        template.Template_Body__c = 'Test';
        template.Template_Subject__c = 'Invoice Test';
        
        insert template;
        /*
        //insert Account
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account emp = new Account();
        emp.recordTypeId = accRecordTypeId;
        emp.Default_Product__c = 'Sedera Access';
        emp.MS_Pricing__c = 'New Pricing';
        emp.Available_IUA_Options__c = '500';
        emp.Name = 'Test'; 
        emp.Default_Product__c = 'Sedera Access';
        emp.Invoice_Email_Address__c = 'test@test.com';
         
        insert emp;
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        Account account = new Account();
        account.recordTypeId = devRecordTypeId;
        account.Name = 'Test AccountName';
        account.First_Name__c = 'FName';
        account.Last_Name__c = 'LName';
        account.Account_Employer_Name__c = emp.Id;
        account.Primary_Email_Address__c = 'test@test.com';
        account.Invoice_Email_Address__c = 'test@test.com';
        insert account;
        
        // insert contact
        Contact contact = new Contact();
        contact.AccountId = account.Id;
        contact.LastName = 'Test Name';
        contact.Email = 'test@test.com';
        insert contact;
        */
        
        UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 1;
        List <Pricing__c> PriceList=TestDataFactory.Create_Pricing(null,2,true);
        PriceList[0].name='Default Old Select Pricing';
        Update PriceList[0];
        List <Pricing__c> AccessPricing=TestDataFactory.Create_Pricing(null,1,false);
        AccessPricing[0].name='Default Access Pricing';
        AccessPricing[0].recordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId();
        insert AccessPricing[0];
        UtilityClass_For_Static_Variables.CheckRecursiveForPricingTrigger = 0;
        
        HttpCalloutMock Mockclass1 = new Test_MockSendSMS();
        Test.setMock(HttpCalloutMock.class, Mockclass1);
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > EmployerAccountList = TestDataFactory.Create_Account_Of_Employer_type(1, true);

        Date myTestDate = date.newinstance(date.today().year() - 1, date.today().month(), date.today().day());

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        Account emp = EmployerAccountList[0];
        emp.Enrollment_Date__c = myTestdate;
        emp.iua_chosen__c = 500;
        emp.Primary_Email_Address__c = 'test@test.com';
        emp.Invoice_Email_Address__c = 'test@test.com';
        update emp;


        List < MEC_Product__c > Mec_Product_List = TestDataFactory.Create_Mec_Product(3, true);
        Mec_Product_List[1].Discount_Tier__c = 'T2';
        update Mec_Product_List[1];

        List < AccountMECAssociation__c > Mec_Assosciation_List = new List < AccountMECAssociation__c > ();


        for (integer j = 0; j < 1; j++) {

            for (Integer i = 0; i < 3; i++) {

                List < AccountMECAssociation__c > MecRec = TestDataFactory.Create_Mec_Assosciation(EmployerAccountList[j].Id, Mec_Product_List[i].Id, 1, false);
                if (i == 1) {
                    MecRec[0].Default_MEC_Product__c = true;
                }
                Mec_Assosciation_List.addAll(MecRec);

            }
        }


        insert Mec_Assosciation_List;


        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        List < Account > MemberAccountList = TestDataFactory.Create_Account_Of_Member_type(1, false);
        for (integer i = 0; i < MemberAccountList.size(); i++) {
            MemberAccountList[i].Account_Employer_name__C = emp.id;
            MemberAccountList[i].subscription_status__c='Active';
        }

        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger = 0;
        
        insert MemberAccountList;

        paubox_message_template__c pauboxTemplateUpdate = new paubox_message_template__c();
        pauboxTemplateUpdate.name = 'Commission';
        pauboxTemplateUpdate.Paubox_Message_Type__c = 'Commission';
        pauboxTemplateUpdate.Template_Subject__c = 'Sedera - Affiliate Fee Statement -';
        pauboxTemplateUpdate.From_Address__c = 'accounting@sedera.com';
        insert pauboxTemplateUpdate;
        
 		Affiliate__c aff = new Affiliate__c(Name = 'test affiliate');
        aff.Receive_Affiliate_Fee_emails__c = 'test@test.com';
        insert aff;
        
        Commission_Report__c cr = new Commission_Report__c();
        insert cr;
        
        Commission_Report_Item__c cri = new Commission_Report_Item__c();
        cri.Affiliate__c = aff.id;
        cri.Commission_Report__c = cr.id;
        cri.Member_Count__c = 10;
        cri.Total_Commission__c = 500.00;
        insert cri;
        
    }
    
    /*
    * To test Email sending Functionality
    */
    private static testMethod void testEmailFunctionality()
    {
        
        List<Commission_Report__c> crList = [SELECT Id FROM Commission_Report__c];        
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailHelperMock());
        
        Test.startTest();        
        Database.executeBatch(new CommissionEmailBatch(crList[0].id));
        Test.stopTest();
        
        List<Paubox_Message__c> pauboxMessageRecordList = [SELECT Id FROM Paubox_Message__c];        
        system.debug('Select ID from Paubox returned messages: ' +pauboxMessageRecordList.size()); 
        //System.assert(pauboxMessageRecordList.size() > 0);

    }
    
    public class  PauBoxEmailHelperMock implements HttpCalloutMock 
    {
        
        public HTTPResponse respond(HTTPRequest req) 
        {
            
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"sourceTrackingId": "3d38ab13-0af8-4028-bd45-52e882e0d584","data": "Service OK"}');
            response.setStatusCode(200);
            
            return response;
        }
    }
}