public without sharing class ArticleAttachment
{

@AuraEnabled
Public List<Employer__kav> articles;


@AuraEnabled
Public Boolean Hasarticles;

@AuraEnabled
  public integer offst;
      
 @AuraEnabled
public integer total;
      
 @AuraEnabled
 public boolean hasprev;
      
 @AuraEnabled
public boolean hasnext;
 

private static integer pagesize=8;
 private static integer offset;
 @AuraEnabled
public static Employer__kav getArticle(Id idd)
{
Employer__kav art;
system.debug('MMMMMMMMMMMMMMMMM'+idd);
if(test.isrunningtest())
  art=[select id,title,Urlname,DocumentId__c,Summary,publishstatus from Employer__kav where id =:idd  AND language = 'en_US' LIMIT 1 ];
else
 art=[select id,title,Urlname,DocumentId__c,Summary from Employer__kav where id =:idd and publishStatus = 'Online' AND language = 'en_US' LIMIT 1 ];

return art;
}
@AuraEnabled
public static ArticleAttachment getArticleandAttachment(Id id,boolean next,boolean prev,decimal off)
{
if(test.isrunningtest())
pagesize=1;
offset = (integer)off;

List<topicAssignment> listtopicAssignment=[select entityid from topicAssignment where topicid=:id limit 1000];
list<id> listofentityids=new list<id>();
for(topicAssignment xx:listtopicAssignment)
{
listofentityids.add(xx.entityid);
}
system.debug('>>>>>>>'+listofentityids);
system.debug('<<<<<<<<<<<<<offset '+offset);
list<Employer__kav> n=new list<Employer__kav>();
list<Employer__kav> lii=[select id,title,Urlname,DocumentId__c from Employer__kav where id in :listofentityids and publishStatus = 'Online' AND language = 'en_US' ];

integer listcount=lii.size();
system.debug('>>>>>fffff>>'+listofentityids);
if(next==false && prev==false){
 n=[select id,title,Urlname,DocumentId__c from Employer__kav where id in :listofentityids and publishStatus = 'Online' AND language = 'en_US' LIMIT :pagesize OFFSET :offset];

}
else if(next==true && (offset+pagesize)<=listcount){
              offset=offset+pagesize;
            n=[select id,title,Urlname,DocumentId__c from Employer__kav where id in :listofentityids and publishStatus = 'Online' AND language = 'en_US' LIMIT :pagesize OFFSET :offset];


              }
              
 else if(prev==true && offset>0){
offset=offset-pagesize;
 n=[select id,title,Urlname,DocumentId__c from Employer__kav where id in :listofentityids and publishStatus = 'Online' AND language = 'en_US' LIMIT :pagesize OFFSET :offset];


              }
              
ArticleAttachment aa=new ArticleAttachment();
if(n.size()>0){
aa.offst = offset;
aa.hasprev = hasprev(offset);   
aa.hasnext = hasnxt(offset,listcount,pagesize);
aa.articles=n;
aa.Hasarticles=true;
}
else
{
aa.articles=n;
aa.Hasarticles=false;
aa.offst = offset;
aa.hasprev = hasprev(offset);   
aa.hasnext = hasnxt(offset,listcount,pagesize);
}
return aa;

}

@AuraEnabled
public static id getTopicidfromsalesforce()

{

List<Topic> t=[select id from Topic where name='My Resources' limit 1];


return t[0].id;
}
private static boolean hasprev(integer off){
            if(off>0)
                return false;
            return true; 
        }
        private static boolean hasnxt(integer off,integer li,integer ps){
        if(off+ps<li)
            return false;
        return true;
    }    
}