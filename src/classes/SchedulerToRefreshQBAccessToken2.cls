global class SchedulerToRefreshQBAccessToken2 implements Schedulable {
    public static void schedule(){
        SchedulerToRefreshQBAccessToken2 qbJob = new SchedulerToRefreshQBAccessToken2();
        if(!Test.isRunningTest()){
            if(!Test.isRunningTest()){
                system.schedule('JobToRefreshQBAccessToken3','0 15 * * * ?', qbJob);
                system.schedule('JobToRefreshQBAccessToken4','0 45 * * * ?', qbJob);
            }
        }
    }    
    global void execute(SchedulableContext SC) {
        initiateProcess();
    }
    @future(callout=true) 
    global static void initiateProcess(){
/*        QBConfigurationWrapper config = new QBConfigurationWrapper('Quickbooks');
        OAuth2 oAuth2Obj = new OAuth2(config);
        Response response1 = oAuth2Obj.refreshToken();*/
        
        QBConfigurationWrapper config = new QBConfigurationWrapper('Quickbooks2');
        OAuth2 oAuth2Obj2 = new OAuth2(config);
        Response response2 = oAuth2Obj2.refreshToken();     
    }
}