/*
Test class: case_trigger_test
*/

public class Bill_Helper_Class {
   /*A member is payable only when total amount paid by patient exceeds case IUA provided case is involved in needs or is a need.

     total amount paid by patient is the sum of paid by patient amount from all Approved bills.Non Approved bills are not considered. 

     Example:
     Case (Need Case) => IUA 500
     Bill 1 paid by patient 400, Approval status-Approved,payableToMember=0
     Bill 2 paid by patient 300, Approval status-Not Approved,payableToMember=0
     Bill 3 paid by patient 300, Approval status-Approved,payableToMember=200
   */
    public static payableToMemFlags pf = new payableToMemFlags();
    public static Id NeedsRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Need Sharing').getRecordTypeId();
    public static Id PreventiveRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Preventive').getRecordTypeId();


    public static void calculatePayableToMember(List <bill__c> billNew, Map <id, bill__c> billnewmap, Map <id, bill__c> billOldMap) {

        set <id> billIds = new set <id> ();
        Map <Id, List <Bill__c>> casBillMap = new Map <id, List <bill__c>> ();

        for (bill__c bill: billNew) {
            if (billOldMap == null && bill.approval_status__c == 'APPROVED') {
                casBillMap = calculateCaseBillMap(bill, casBillMap);
            }
          }
               
        list <case> cases = [select Initial_Unsharable_Amount__c, sharable_need__C, (select id,case__r.recordTypeId,case__r.Initial_Unsharable_Amount__c,case__r.sharable_need__C, Total_Patient_Payable_Amount__c, Approval_Status__c, case__c, Payable_to_member__c from bills__r order by createddate asc) from
                case where id in: casBillMap.keySet()];



        Decimal IUAsum = 0;
        Boolean flag = false;
        list <Bill__c> billList;
        list<bill__c> billsToUpdate=new List<bill__c>();

        for (Case cas: cases) {

            IUAsum = 0;
            flag = false;

            billList = cas.bills__r;

            if (billList != null && billList.Size()> 0 && casBillMap.containsKey(cas.Id))
                billList.addAll(casBillMap.get(cas.Id));
            else if (casBillMap.containsKey(cas.Id)) {
                billList = new list <bill__c> ();
                billList.addAll(casBillMap.get(cas.Id));
            }

            for (bill__c bill: billList) {
  
                calculateBill(bill,IUAsum,flag);
                if(bill.id!=null){
                  billsToUpdate.add(bill);
                }
                IUAsum = pf.IUAsum;
                flag = pf.flag;

            }
          

        }
        
        if(billsToUpdate.size()>0){
          Update billsToUpdate;
        }




    }

    public static void calculatePayableToMember(List <case> cases) {

        Decimal IUAsum = 0;
        Boolean flag = false;
        List<bill__c> billsToUpdate=new List<bill__c>();

        for (Case cas: cases) {
            IUAsum = 0;
            flag = false;
            for (bill__c bill: cas.bills__r) {
                calculateBill(bill,IUAsum,flag);
                billsToUpdate.add(bill);
                IUAsum = pf.IUAsum;
                flag = pf.flag;
            }

        }
        
        if(billsToUpdate.size()>0)
         Update billsToUpdate;


    }

    public static void calculateBill(Bill__c bill, Decimal IUAsum, Boolean flag) {

        if (bill.Approval_status__c == 'Approved' && (( bill.case__r.recordTypeId == NeedsRecType && bill.case__r.sharable_need__C == true) || bill.case__r.recordTypeId==PreventiveRecType)) {

            if (bill.Total_Patient_Payable_Amount__c == null) {
                IUAsum = IUAsum + 0;
            } else {
                IUAsum = IUAsum + bill.Total_Patient_Payable_Amount__c;
            }

            if (flag) {
                bill.Payable_to_member__c = bill.Total_Patient_Payable_Amount__c;
            } else if (IUAsum>= bill.case__r.Initial_Unsharable_Amount__c && !flag) {
                bill.Payable_to_member__c = IUAsum - bill.case__r.Initial_Unsharable_Amount__c;
                flag = true;
            } else {
                bill.Payable_to_member__c = 0;
            }
        } else {
            bill.Payable_to_member__c = 0;
        }

        pf.IUAsum = IUAsum;
        pf.flag = flag;

    }

    public static Map <Id, List <Bill__c>> calculateCaseBillMap(Bill__c bill, Map <Id, List <Bill__c>> casBillMap) {
        List <bill__c> billList;
        if (casBillMap.containsKey(bill.case__c)) {
            billList = casBillMap.get(bill.case__c);
            billList.add(bill);
            casBillMap.put(bill.case__c, billList);
        } else {
            casBillMap.put(bill.case__c, new list <bill__c> {
                bill
            });
        }
         
        return casBillMap;
    }

    public class payableToMemFlags {

        Decimal IUAsum;
        Boolean flag;
    }
}