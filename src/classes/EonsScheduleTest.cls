/**
* @Purpose : To test EonsSchedule class functionality
* @Date : 03/08/2018
*/
@isTest
public class EonsScheduleTest{
    
    /**
    * To set initial data
    */
    @testSetup
    public static void initData(){
        
        //insert Account
        Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        Account emp = new Account();
        emp.recordTypeId = accRecordTypeId;
        emp.Default_Product__c = 'Sedera Access';
        emp.MS_Pricing__c = 'New Pricing';
        emp.Available_IUA_Options__c = '500';
        emp.Name = 'Test'; 
        emp.Default_Product__c = 'Sedera Access';       
        insert emp;
        
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        Account account = new Account();
        account.recordTypeId = devRecordTypeId;
        account.Name = 'Test AccountName';
        account.First_Name__c = 'FName';
        account.Last_Name__c = 'LName';
        account.Account_Employer_Name__c = emp.Id;
        account.Primary_Email_Address__c = 'test@test.com';
        insert account;
        
        // insert contact
        Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Sedera Health Contact').getRecordTypeId();
        Contact contact = new Contact();
        contact.recordTypeId = conRecordTypeId;
        contact.AccountId = account.Id;
        contact.LastName = 'Test Name';
        contact.Email = 'test@test.com';
        insert contact;
        
        // insert Case
        List<Case> caseList = new List<Case>();
        
        for(Integer i = 0; i < 10; i++){
            Case caseRecord = new Case();
            caseRecord.ContactId = contact.Id;
            caseRecord.Status = 'New';
            caseRecord.Origin = 'Phone';
            caseRecord.Type = 'HCS';
            caseRecord.Correspondence_Email__c = 'test@test.com';
            caseList.add(caseRecord);
        }
        insert caseList;
        System.debug('Test caseList :::: '+caseList);
        // insert bill
        List<Bill__c> billingList = new List<Bill__c>();
        for(Case caseRecord :caseList){
            
            Bill__c bill = new Bill__c(); 
            bill.Case__c = caseRecord.Id;
            bill.Patient_Name__c = contact.Id;
            bill.Provider_Name__c = 'Test';
            bill.Patient_s_Account_Number__c = '1111';
            bill.Balance_Payable_by_Member__c = 10.10;
            bill.Amount_Shared_This_Week__c = 10;
            bill.Status__c = 'Received-Not to be Negotiated';
            bill.member__c = account.id;
            bill.Total_Shares_This_Week__c = 1;
            
            billingList.add(bill); 
        }
        insert billingList;
        
        for(Bill__c bill :billingList){
            bill.Status__c = 'Received-Not to be Negotiated';
        }
        update billingList;
        System.debug('Test billingList :::: '+billingList);
        
        // insert SDTemplate
        SDOC__SDTemplate__c sDTemplate = new SDOC__SDTemplate__c();
        sDTemplate.Name = 'EONS';
        sDTemplate.SDOC__Base_Object__c = 'Case';
        sDTemplate.SDOC__Template_Format__c = 'PDF';
        
        insert sDTemplate;
    }
    
    /**
    * To test EonsScheduler functionality
    */
    public static testMethod void testEonsScheduler(){
        
        Test.startTest();
        
        String strCronExp = '0 0 1 * * ?'; 
        String jobID = system.schedule('Test scheduler', strCronExp, new EonsSchedule());
        Test.stopTest();
        
        List<SDOC__SDJob__c> jobList = [SELECT Id FROM SDOC__SDJob__c];
        
        System.assert(jobList.size() > 0);
    }
}