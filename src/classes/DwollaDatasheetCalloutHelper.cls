/**
*@Purpose : Helper class having all Dwolla API related methods
*@Date : 31/12/2018
*/
public class DwollaDatasheetCalloutHelper {
    
    public static Boolean isPositiveTest = false;
    public DwollaAPIConfiguration__c config;
    public DwollaDatasheetCalloutHelper(){
        this.config = getAPIConfiguration();
    }
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public Boolean isPostiveTesting = false;

    /**
    *@Purpose :Create customr record at Dwolla
    */
    public calloutResponse createCustomer(Dwolla_Datasheet__c datasheetRecord, Boolean isInsert){
        calloutResponse calloutResponseRec = new calloutResponse();
        String customerId = '';
        Integer statusCode;
        if(datasheetRecord != null && config != null){
           
            Map<String,String> endPointParameterMap = new Map<String,String>();
            Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json',
                                                                           'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                           'Authorization'=>'Bearer '+config.Access_Token__c};
            
            JSONGenerator gen = getCustomerJson(datasheetRecord);
            System.debug('gen @@@ '+ gen);
            String endPoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c : config.Production_Endpoint_Url__c;
            endPoint = String.isNotBlank(endPoint) ? endPoint+'/customers' : '';
            try{
                HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
                 if(Test.isRunningTest()){
                    res.setStatusCode(201);
                    res.setHeader('Location', 'customers/123654789');
                }
                statusCode = res.getStatusCode();
                if(res.getStatusCode() == 201){
                    customerId = res.getHeader('Location').substringAfter('customers/');
                }else{
                     DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                     errorWrapper.errorMsg = 'Status Code :\n'+res.getStatusCode()+'\n \n Error :\n'+res.getBody();
                     errorWrapper.requestBody = 'endPoint: \n'+endPoint+' \n\n endPointParameterMap :\n'+endPointParameterMap+
                                                '\n \n headerParameterMap:\n'+headerParameterMap+'\n\n Json :\n'+gen.getAsString();
                     errorWrapper.source = 'DwollaDatasheetCalloutHelper.createCustomer';
                     errorWrapper.accountId = datasheetRecord.Account__c;
                     DwollaErrorResponseHandler.saveDwollaErrorResponse(
                                new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
                }
            }catch(Exception exp){
                system.debug('Exception'+exp);
                LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCalloutHelper.createCustomer', exp, 'Account: '+datasheetRecord, 
                                                                 '', 'Error'))});   
            }
        } 
        calloutResponseRec.customerId = customerId;
        calloutResponseRec.statusCode = statusCode;
        return calloutResponseRec;       
    }
    
     /**
        *@Purpose :Create Beneficial Owner record at Dwolla
    */
    public calloutResponse createBeneficialOwners(Dwolla_Datasheet__c datasheetRecord, String dwollaId){
        calloutResponse calloutResponseRec = new calloutResponse();
        String beneficaliOwnerId = '';
        Integer statusCode;
        if(datasheetRecord != null && config != null && String.isNotBlank(dwollaId)){
            Map<String,String> endPointParameterMap = new Map<String,String>();
            Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json',
                                                                           'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                           'Authorization'=>'Bearer '+config.Access_Token__c};
            
            JSONGenerator gen = getBeneficialJson(datasheetRecord);
            String endPoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c : config.Production_Endpoint_Url__c;
            endPoint = String.isNotBlank(endPoint) ? endPoint+'/customers/'+dwollaId+'/beneficial-owners' : '';
            try{
                HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
                System.debug('createBeneficialOwners --> res :: '+res.getBody()+', Statuscode ::'+res.getStatusCode());
                
                if(Test.isRunningTest()){
                    
                    if(isPositiveTest){
                      res.setStatusCode(201);  
                      res.setHeader('Location', 'beneficial-owners/32131316');
                    }else{
                    	res.setStatusCode(400);    
                    }
                }
                statusCode = res.getStatusCode();
                System.debug('HttpResponse @@@@ '+ res.getStatusCode()); 
                if(res.getStatusCode() == 201){
                    beneficaliOwnerId = res.getHeader('Location').substringAfter('beneficial-owners/');
                }else{
                    
                     DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                     errorWrapper.errorMsg = 'Status Code :\n'+res.getStatusCode()+'\n \n Error :\n'+res.getBody();
                     errorWrapper.requestBody = 'endPoint: \n'+endPoint+' \n\n endPointParameterMap :\n'+endPointParameterMap+
                                                '\n \n headerParameterMap:\n'+headerParameterMap+'\n\n Json :\n'+gen.getAsString();
                     errorWrapper.source = 'DwollaDatasheetCalloutHelper.createBeneficialOwners';
                     errorWrapper.accountId = datasheetRecord.Account__c;
                     DwollaErrorResponseHandler.saveDwollaErrorResponse(
                                new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
                }
            }catch(Exception exp){
                system.debug('Exception @@@ == '+exp);
                LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCalloutHelper.createCustomer', exp, 'Account: '+datasheetRecord, 
                                                                 '', 'Error'))});   
            }
        } 
        calloutResponseRec.beneficaliOwnerId = beneficaliOwnerId;
        calloutResponseRec.statusCode = statusCode;
        return calloutResponseRec;         
    }
    
     /**
        *@Purpose :Certify Beneficial Ownership at Dwolla
    */
    public Boolean CertifyBeneficialOwners(Dwolla_Datasheet__c datasheetRecord, String dwollaId){
        if(datasheetRecord != null && config != null && String.isNotBlank(dwollaId)){
            Map<String,String> endPointParameterMap = new Map<String,String>();
            Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/json',
                                                                           'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                           'Authorization'=>'Bearer '+config.Access_Token__c};
            
            JSONGenerator gen = getCertifyJson(datasheetRecord);
            String endPoint = config.Is_Sandbox__c ? config.Sandbox_Endpoint_Url__c : config.Production_Endpoint_Url__c;
            endPoint = String.isNotBlank(endPoint) ? endPoint+'/customers/'+dwollaId+'/beneficial-ownership' : '';
            try{
                HttpResponse res = calloutHelper.httpPostRequest(endPoint, endPointParameterMap, headerParameterMap, gen);
                System.debug('HttpResponse @@@@ '+ res.getStatusCode()); 
                if(res.getStatusCode() == 200){
                    //return res.getHeader('Location').substringAfter('beneficial-owners/');
                    return true;
                }else{
                     DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                     errorWrapper.errorMsg = 'Status Code :\n'+res.getStatusCode()+'\n \n Error :\n'+res.getBody();
                     errorWrapper.requestBody = 'endPoint: \n'+endPoint+' \n\n endPointParameterMap :\n'+endPointParameterMap+
                                                '\n \n headerParameterMap:\n'+headerParameterMap+'\n\n Json :\n'+gen.getAsString();
                     errorWrapper.source = 'DwollaDatasheetCalloutHelper.CertifyBeneficialOwners';
                     errorWrapper.accountId = datasheetRecord.Account__c;
                     
                     DwollaErrorResponseHandler.saveDwollaErrorResponse(
                                new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});
                }
            }catch(Exception exp){
                system.debug('Exception'+exp);
                LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCalloutHelper.createCustomer', exp, 'Account: '+datasheetRecord, 
                                                                 '', 'Error'))});   
            }
        } 
        return false;
    }
    
    /**
    *@Purpose :Save Account record Information
    */
    public static void saveCustomer(List<Account> customerDatasheetList){
        try{        
            upsert customerDatasheetList;
        }catch(Exception exp){
            System.debug('Exception :::'+exp.getMessage());
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCalloutHelper.saveCustomer', exp, 'customerList: '+customerDatasheetList, 
                                                                 '', 'Error'))});  
        }
    }
    
    /**
    *@Purpose :Create JSON generater for Certify Beneficail Ownership API request.
    */
    private JSONGenerator getCertifyJson(Dwolla_Datasheet__c datasheetRecord){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('status', 'certified');
        gen.writeEndObject();
        return gen;
    }
    
    /**
    *@Purpose :Create JSON generater for Create Customer API request
    */
    private JSONGenerator getCustomerJson(Dwolla_Datasheet__c datasheetRecord){
        Datetime DOB;
        String DOBString = '';
        if(datasheetRecord.Controller_Date_of_Birth__c != null) {
            DOB = datasheetRecord.Controller_Date_of_Birth__c;
            DOBString = DOB.formatGmt('yyyy-MM-dd');
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstName', datasheetRecord.First_Name__c != Null ? datasheetRecord.First_Name__c : '');
        gen.writeStringField('lastName', datasheetRecord.Last_Name__c != Null ? datasheetRecord.Last_Name__c : '');
        gen.writeStringField('email', datasheetRecord.Email_Address__c != Null ? datasheetRecord.Email_Address__c : '');
        gen.writeStringField('type', 'business');
        gen.writeStringField('address1', datasheetRecord.Address_1__c != Null ? datasheetRecord.Address_1__c : '');
        gen.writeStringField('address2', datasheetRecord.Address_2__c != Null ? datasheetRecord.Address_2__c : '');
        gen.writeStringField('city', datasheetRecord.City__c != Null ? datasheetRecord.City__c : '');
        gen.writeStringField('state', datasheetRecord.State__c != Null ? datasheetRecord.State__c : ''); 
        gen.writeStringField('postalCode', datasheetRecord.Postal_Code__c != Null ? datasheetRecord.Postal_Code__c : '');
        
        gen.writeFieldName('controller');
        gen.writeStartObject();
        gen.writeStringField('firstName', datasheetRecord.Controller_First_Name__c != Null ? datasheetRecord.Controller_First_Name__c : '');
        gen.writeStringField('lastName', datasheetRecord.Controller_Last_Name__c != Null ? datasheetRecord.Controller_Last_Name__c : '');
        gen.writeStringField('title', datasheetRecord.Controller_Title__c != Null ? datasheetRecord.Controller_Title__c : '');
        gen.writeStringField('dateOfBirth', String.isNotBlank(DOBString) ? DOBString : ''); 
        gen.writeStringField('ssn', datasheetRecord.Controller_SSN__c != Null ? datasheetRecord.Controller_SSN__c : '');
        
        gen.writeFieldName('address');
        gen.writeStartObject();
        gen.writeStringField('address1', datasheetRecord.Controller_Address_1__c != Null ? datasheetRecord.Controller_Address_1__c : '');
        gen.writeStringField('address2', datasheetRecord.Controller_Address_2__c != Null ? datasheetRecord.Controller_Address_2__c : '');
        gen.writeStringField('address3', datasheetRecord.Controller_Address_3__c != Null ? datasheetRecord.Controller_Address_3__c : '');
        gen.writeStringField('city', datasheetRecord.Controller_City__c != Null ? datasheetRecord.Controller_City__c : ''); 
        gen.writeStringField('stateProvinceRegion', datasheetRecord.Controller_State__c != Null ? datasheetRecord.Controller_State__c : '');
        gen.writeStringField('postalCode', datasheetRecord.Controller_Postal_Code__c != Null ? datasheetRecord.Controller_Postal_Code__c : ''); 
        gen.writeStringField('country', 'US');
        gen.writeEndObject();
        gen.writeEndObject();
        
        if(datasheetRecord.business_classification__c != NULL)
            gen.writeStringField('businessClassification', DwollaBusinessClassTranslationHelper.translate(datasheetRecord.business_classification__c));
        
        gen.writeStringField('businessType', datasheetRecord.Business_Type__c != Null ? datasheetRecord.Business_Type__c : '');
        gen.writeStringField('businessName', datasheetRecord.DBA_Name__c != Null ? datasheetRecord.DBA_Name__c : ''); 
        gen.writeStringField('ein', datasheetRecord.EIN__c != Null ? datasheetRecord.EIN__c : '');

        gen.writeEndObject();
        return gen;
    }
    
     /**
    *@Purpose :Create JSON generater for creating Benefical Owner API request
    */
    private JSONGenerator getBeneficialJson(Dwolla_Datasheet__c datasheetRecord){
        Datetime DOB;
        String DOBString ;
        
        if(datasheetRecord.Date_of_Birth__c != null) {
            DOB = datasheetRecord.Date_of_Birth__c;
            DOBString = DOB.formatgmt('yyyy-MM-dd');
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstName', datasheetRecord.First_Name__c != Null ? datasheetRecord.First_Name__c : '');
        gen.writeStringField('lastName', datasheetRecord.Last_Name__c != Null ? datasheetRecord.Last_Name__c : '');
        gen.writeStringField('ssn', datasheetRecord.SSN__c != Null ? datasheetRecord.SSN__c : '');
        gen.writeStringField('dateOfBirth',String.isNotBlank(DOBString) ? DOBString : ''); 
       
        gen.writeFieldName('address');
        gen.writeStartObject();
        gen.writeStringField('address1', datasheetRecord.Address_1__c != Null? datasheetRecord.Address_1__c : '');
        gen.writeStringField('address2', datasheetRecord.Address_2__c != Null? datasheetRecord.Address_2__c : '');
        gen.writeStringField('city', datasheetRecord.City__c != Null ? datasheetRecord.City__c : ''); 
        gen.writeStringField('stateProvinceRegion', datasheetRecord.State__c != Null ? datasheetRecord.State__c : '');
        gen.writeStringField('postalCode', datasheetRecord.Postal_Code__c != Null ? datasheetRecord.Postal_Code__c : ''); 
        gen.writeStringField('country', 'US');
        gen.writeEndObject();
        
        gen.writeEndObject();
        return gen;
    }
    
    /**
    *@Purpose :get custom setting record
    */
    public static DwollaAPIConfiguration__c getAPIConfiguration(){
        return DwollaAPIConfiguration__c.getOrgDefaults();
    }
    
    /*
     * @Purpose :get Live Token.
    */
    public DwollaAPIConfiguration__c getLiveToken(DwollaAPIConfiguration__c config){
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        // Get Access Token
        Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
            'client_secret' => config.Secret_Key__c,
            'grant_type' => 'client_credentials'};
                Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
                    Response response;
        if(Test.isRunningTest()){
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
            System.debug('response @@@ '+ response);
            if(isPostiveTesting){
                response.Success =  true;    
            }
            if(response.Success){
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(true, '', oAuthResponse);   
            }else{
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(false, '', oAuthResponse);
            }
        } else{                                                            
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
        }
        
        if(response != null){
            if(response.Success){
                OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                config.Access_Token__c = authTokenResponse.access_token;
                config.Access_Token_Expiry__c = Datetime.now().addMinutes(55);
                return config;
            }else{
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+response.Message;
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                    '\n\n headerParameterMap :\n'+headerParameterMap;
                errorWrapper.source = 'DwollaCustomerBatch.getLiveToken';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
            }
        }
        return null;
    }
    
    public class calloutResponse{
        public String customerId;
        public String beneficaliOwnerId;
        public Integer statusCode;
        public String message;
    }
}