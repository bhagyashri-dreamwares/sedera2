/*
  Test class : Case_Trigger_Test
*/
public class Case_Trigger_Handler {

    public static void handleBeforeInsert(List <case> caseList) {

        Case_Helper_Class.Setting_Need_Attributes_On_Before_Insert(caseList);
        Case_Helper_Class.clearMaps();

    }


    public static void handleBeforeUpdate(Map <Id,case> caseNewMap, Map <Id,case> caseOldMap) {

        Case_Helper_Class.Setting_Need_attributes_On_Before_Update(caseNewMap.values(), caseOldMap);
    }

    public static void handleAfterUpdate(Map <Id,case>caseNewMap, Map <Id,case> caseOldMap) {

        Case_Helper_Class.Setting_Need_attributes_On_After_Events();
        Case_Helper_Class.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
        Case_Helper_Class.SetBillsOnChngeOfttlAmtPaidPatient(caseOldMap, caseOldMap.keyset());
        Case_Helper_Class.clearMaps();

    }


    public static void handleAfterUnDelete(Map <Id, Case> caseNewMap) {

        Case_Helper_Class.Setting_Need_Attributes_for_Undelete(caseNewMap.values());
        Case_Helper_Class.Setting_Need_attributes_On_After_Events();
        Case_Helper_Class.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
        Case_Helper_Class.clearMaps();

    }


    public static void handleBeforeDelete(Map <Id, case> caseOldMap) {

        Case_Helper_Class.Setting_Need_attributes_On_Before_Delete(caseOldMap.values());

    }

    public static void handleAfterDelete(Map <Id, case> caseOldMap) {

        Case_Helper_Class.Setting_Need_attributes_On_After_Events();
        Case_Helper_Class.Setting_Number_Of_Needs_On_Accounts_And_Contacts();
        Case_Helper_Class.clearMaps();


    }

}