/**
* @Author      : Dreamwares
* @Purpose      : - Callback handler for OAuth 2.0
*            - Assumption : Provider name is in state paramater
* @Created Date  : 09/06/2018
*/
public class DwollaOAuthCallbackController {
    public Boolean isRedirected{get;set;}
    public DwollaAPIConfiguration__c config;
    
    public DwollaOAuthCallbackController(){
        
        config = DwollaCalloutHelper.getAPIConfiguration();
    }
    
    public void handleOAuthCallback(){
        
        List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
        
        Map<String, String> authorizationParametersMap = ApexPages.currentPage().getParameters();
        
        System.debug('authorizationParametersMap ::::'+authorizationParametersMap);
        // check if verification callback
        if(authorizationParametersMap.containsKey('state')){
        
               isRedirected = TRUE;
               DwollaOAuth2 auth;
               if(authorizationParametersMap.get('state') == 'Dowlla'){               
                   
                   auth = new DwollaOAuth2(config);
               }
               System.debug('auth ::::'+auth );
               
               if(auth != null){
               
                   // Get Access Token
                   Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
                                                                                      'client_secret' => config.Secret_Key__c,
                                                                                      'grant_type' => 'client_credentials'};
                   
                   Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
                                                                                      
                   Response response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
                   System.debug('response  ::::'+response );
                   
                  
                   if(response.Success){
                       
                       OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                       
                       // Update Access Token, refresh token expiry
                       response = saveTokens(authTokenResponse, config);
                   }
                   if(response.Success){
                       
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Authorized Successfully!'));
                   }else{
                       
                       ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,response.Message));
                       system.debug('response.Message'+response.Message);
                   }
              
                   
               }else{
                   
                   DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                   errorWrapper.errorMsg = 'Unable to authenticate Dwolla from DwollaAuthorize Page';
                   errorWrapper.requestBody = 'Input : \n'+config;
                   errorWrapper.source = 'DwollaOAuthCallbackController.handleOAuthCallback';
                   errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
               }
               
           }else{
            // no call back redirect
            isRedirected = false;
        }
        
        if(!errorList.isEmpty()){
            DwollaErrorResponseHandler.saveDwollaErrorResponse(errorList);
        }
    }
    
    /*
* @Purpose      : Quickbooks Login(Authorization redirection)
*/
    public Pagereference redirectPage(){
        
        Map<String, String> aPIConfig = new Map<String, String>{'state' => 'Dowlla'};
        
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        
        return auth.authorize(aPIConfig , config.redirect_URI__c);
    }
    
    public void enableAuth(){
        isRedirected = false;
    }
    
        
    public static Response saveTokens(OAuth2TokenResponse authTokenResponse, DwollaAPIConfiguration__c config){
        
        config.Access_Token_Expiry__c = Datetime.now().addMinutes(55);
        config.Access_Token__c = authTokenResponse.access_token;
        
        return saveConfiguration(config);        
    }
    
    public static Response saveConfiguration(DwollaAPIConfiguration__c config){
        
        Response res = new Response(true, '', '');
        
        try{
            update config;
        }catch(Exception exp){
            System.debug('Exception ::'+exp.getMessage());
            res = new Response(false, exp.getMessage(), '');  
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaOAuthCallbackController.saveConfiguration', exp, 'DwollaAPIConfiguration: '+config, 
                                                                 '', 'Error'))});          
        }
        return res;
    }
}