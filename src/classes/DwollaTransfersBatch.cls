/**
* @Purpose : Batch class for DwollaTransfer
* @Date : 21/12/2018
*/
public without sharing class DwollaTransfersBatch implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public List<String> dwollaTransferIdList = new List<String>();
    public Boolean isAccessTokenUpdated = false;
    public DwollaAPIConfiguration__c config;
    public List<Dwolla_Error_Response__c> dwollaErrorList = new List<Dwolla_Error_Response__c>();
    
    
    public DwollaTransfersBatch(List<String> dwollaTransferIdList){
        
        if(dwollaTransferIdList != null && !dwollaTransferIdList.isEmpty()){
            
            this.dwollaTransferIdList.addAll(dwollaTransferIdList);
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        Date currentStartOfMonth = Date.today().toStartOfMonth();
        String query = 'SELECT Id, Name, Amount__c, Destination_Funding_Source_ID__c, Source_Funding_Source_ID__c, '+
                              'Invoice__c, Invoice__r.Name, Invoice__r.Quickbooks_Invoice_ID2__c, Dwolla_Transfer_ID__c, '+
                              'Transfer_Submitted__c, Transfer_Response__c  '+
                       'FROM Dwolla_Transfer__c '+
                       'WHERE Destination_Funding_Source_ID__c != null AND Source_Funding_Source_ID__c != null AND ID IN :dwollaTransferIdList';
        //query =  dwollaTransferIdList.isEmpty() ? query  :   query + 'AND ID IN :dwollaTransferIdList';
        
        return Database.getQueryLocator(query);
    }
    
    public DwollaAPIConfiguration__c getLiveToken(DwollaAPIConfiguration__c config){
        
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        
        // Get Access Token
        Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
            'client_secret' => config.Secret_Key__c,
            'grant_type' => 'client_credentials'};
                
                Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
                    Response response;
        if(Test.isRunningTest()){
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
            
            if(response.Success){
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(true, '', oAuthResponse);   
            }else{
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(false, '', oAuthResponse);
            }
            
        } else{                                                            
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
        }
        
        if(response != null){
            if(response.Success){
                
                OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                
                config.Access_Token__c = authTokenResponse.access_token;
                config.Access_Token_Expiry__c = Datetime.now().addMinutes(55);
                
                return config;
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+response.Message;
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                    '\n\n headerParameterMap :\n'+headerParameterMap;
                errorWrapper.source = 'DwollaCustomerBatch.getLiveToken';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
            }
        }
        return null;
    }
    
    public void execute(Database.BatchableContext BC, List<SObject> scope){       
       
        Boolean isTokenLive = false;
        DwollaCalloutHelper dwollaCalloutHelper = new DwollaCalloutHelper();
        
        if(dwollaCalloutHelper.config.Access_Token_Expiry__c == null || 
           dwollaCalloutHelper.config.Access_Token_Expiry__c <= DateTime.now()){
               
           dwollaCalloutHelper.config = getLiveToken(dwollaCalloutHelper.config);
           if(dwollaCalloutHelper.config != null){
               isTokenLive = true;
               isAccessTokenUpdated = true;
               config = dwollaCalloutHelper.config;
           }
       }else{
           config = dwollaCalloutHelper.config;
           isTokenLive = true;
       }
       
        if(isTokenLive){
            
            TransfersBatchHandler.transferAmount(scope, config, dwollaErrorList );  
        }  
        
       
    }
    
    public void finish(Database.BatchableContext BC){
        
        // update access token
        if(isAccessTokenUpdated && config != null){
            DwollaOAuthCallbackController.saveConfiguration(config);
        }
        
        // craete error logs
        if(!errorList.isEmpty()){
            DwollaErrorResponseHandler.saveDwollaErrorResponse(errorList);
        }  
        
        if(!dwollaErrorList.isEmpty()){
             DwollaErrorResponseHandler.saveDwollaErrorResponse(dwollaErrorList);
        }
    }
}