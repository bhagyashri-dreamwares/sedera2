/**
 * @Purpose            : Class to be executed from detail page button on Invoice
 * @Created Date       : 31/10/2018
 */ 
global class InvoiceHelper {
    
    /**
     * @Purpose    : Method to call batch class "InvoiceEmailBatch" to send Invoice using Paubox API 
     * @Parameters : Invoice ID
     * @Return     : -
     */
    webservice static Boolean sendInvoice(String invoiceId) {
    	//System.debug('In sendInvoice, invoiceId::'+invoiceId);	
        
        //Call batch class to send Invoice
        if (String.isNotBlank(invoiceId)){
            Database.executeBatch(new InvoiceEmailBatch(new Set<Id>{invoiceId}), 1);
            return true;
        }
        return false;
    }
}