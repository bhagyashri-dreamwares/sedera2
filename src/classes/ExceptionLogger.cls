Public class ExceptionLogger
{

    public static void LogErrorMessage(Exception ex,Boolean IsNotifyAdminChecked)
    {
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(),null,null,IsNotifyAdminChecked);
        throwError(ex);

    }
    
    public static void LogErrorMessage(Exception ex)
    {
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(),null,null,false);
        throwError(ex);

    }
    
    public static void LogErrorMessage(Exception ex,List<Sobject> NewRecords,Boolean IsNotifyAdminChecked)
    {
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(), JSON.serialize(NewRecords),null,IsNotifyAdminChecked);
        throwError(ex);

    }
    
    public static void LogErrorMessage(Exception ex,List<Sobject> NewRecords, List<Sobject> OldRecords,Boolean IsNotifyAdminChecked)
    {
       if(OldRecords!=null){
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(), JSON.serialize(NewRecords), JSON.serialize(OldRecords),IsNotifyAdminChecked);
       }
       else{ 
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(), JSON.serialize(NewRecords),null,isNotifyAdminChecked);
       }
        throwError(ex);

    }
    
    public static void LogErrorMessage(Exception ex,List<Sobject> NewRecords, List<Sobject> OldRecords)
    {
       if(OldRecords!=null){
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(), JSON.serialize(NewRecords), JSON.serialize(OldRecords),false);
       }
       else{ 
        LogMessage(ex.getStackTraceString(),  ex.GetMessage() , ex.getTypeName(), JSON.serialize(NewRecords),null,false);
       }
        throwError(ex);

    }
    
    
    private static void LogMessage(string source, string message, string type,string newRecs,string oldRecs,Boolean IsNotifyAdminChecked)
    {
        Exception_Log_Event__e loggingevent = new Exception_Log_Event__e ();
        loggingevent.Exception_DateTime__c=datetime.now();
        loggingevent.Exception_Message__c=message;
        loggingevent.Exception_Source__c=source;
        loggingevent.Exception_Type__c=type;
        loggingevent.Logged_User__c=userinfo.getName()+' '+'('+userinfo.getuserid()+')';
        loggingevent.New_Record__c=newRecs;
        loggingevent.Old_Record__c=oldRecs;
      //  loggingevent.StackTrace_String__c=stackTraceStr;
        loggingevent.Notify_Admin__c=IsNotifyAdminChecked;
        Database.SaveResult sr = EventBus.publish(loggingevent);
    }
    
    private static void throwError(Exception ex){
      throw ex;
    }
    
    
}