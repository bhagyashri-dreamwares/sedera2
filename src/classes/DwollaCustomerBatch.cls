/**
*@Purpose : Batch class to create Account related Customer at Dwolla 
*@Date : 07/09/2018
*/
public class DwollaCustomerBatch implements Database.Batchable<Account>, Database.AllowsCallouts, Database.Stateful{
    
    public List<Account> customerList = new List<Account>();
    public List<Account> updatedAccountList = new List<Account>();
    public List<Dwolla_Error_Response__c> errorList = new List<Dwolla_Error_Response__c>();
    public DwollaAPIConfiguration__c config = DwollaAccessTokenHelper.getAPIConfiguration();
    public Boolean isAccessTokenUpdated = false;
    
    public DwollaCustomerBatch(List<Account> customerList){
        this.customerList.addAll(customerList);
    }
    public DwollaCustomerBatch(){
    }
    
    public Iterable<Account> start(Database.BatchableContext BC){
        return customerList;
        
    }
   
    public DwollaAPIConfiguration__c getLiveToken(DwollaAPIConfiguration__c config){
        
        DwollaOAuth2 auth = new DwollaOAuth2(config);
        // Get Access Token
        Map<String, string> endPointParameterMap = new Map<String, string>{'client_id' => config.Client_Key__c,
            'client_secret' => config.Secret_Key__c,
            'grant_type' => 'client_credentials'};
                
                Map<String, string> headerParameterMap = new Map<String, string>{'Content-Type'=> 'application/x-www-form-urlencoded'};
                    Response response;
        if(Test.isRunningTest()){
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
            if(response.Success){
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(true, '', oAuthResponse);   
            }else{
                OAuth2TokenResponse oAuthResponse = (OAuth2TokenResponse)JSON.deserialize('{"access_token": "SF8Vxx6H644lekdVKAAHFnqRCFy8WGqltzitpii6w2MVaZp1Nw","token_type": "bearer","expires_in": 3600 }', OAuth2TokenResponse.class);
                response  = new Response(false, '', oAuthResponse);
            }
            
        } else{                                                            
            response = auth.getAccessToken(config.Authorization_URL__c, endPointParameterMap, headerParameterMap);
        }
        System.debug('response 111  ::::'+response );
        
        if(response != null){
            if(response.Success){
                
                OAuth2TokenResponse authTokenResponse = (OAuth2TokenResponse)response.Data;
                
                config.Access_Token__c = authTokenResponse.access_token;
                config.Access_Token_Expiry__c = Datetime.now().addMinutes(55);
                
                return config;
            }else{
                
                DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                errorWrapper.errorMsg = '\n Error:'+response.Message;
                errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                    '\n\n headerParameterMap :\n'+headerParameterMap;
                errorWrapper.source = 'DwollaCustomerBatch.getLiveToken';
                errorList.add(DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper));
            }
        }
        return null;
    }
 
    public void execute(Database.BatchableContext bc, List<Account> accountList){
        
        Boolean isTokenLive = false;
        DwollaCalloutHelper dwollaCalloutHelper = new DwollaCalloutHelper();
               
        if(config.Access_Token__c != null){
            for(Account account :accountList){
                String dwollaId;
                if(Test.isRunningTest()){
                    dwollaId = dwollaCalloutHelper.createCustomer(account);
                    dwollaId = 'test running';  
                }
                else{
                    dwollaId = dwollaCalloutHelper.createCustomer(account);
                    System.debug('dwollaId  :::'+dwollaId );
                }
                if(String.isNotBlank(dwollaId)){
                    
                    Account accRecord = new Account();
                    accRecord.Id = account.Id;
                    accRecord.Dwolla_ID__c = dwollaId;
                    
                    updatedAccountList.add(accRecord);
                }
            }
        }
    }    
    public void finish(Database.BatchableContext BC){
        
        System.debug('updatedAccountList ::::'+updatedAccountList .size());
        if(isAccessTokenUpdated && config != null){
            DwollaOAuthCallbackController.saveConfiguration(config);
        }
        
        if(!updatedAccountList.isEmpty()){
            
            DwollaCalloutHelper.saveCustomer(updatedAccountList);
        }  
        
        if(!errorList.isEmpty()){
            DwollaErrorResponseHandler.saveDwollaErrorResponse(errorList);
        }  
    }
}