global class BatchPreCheckInvoicedata implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful
{
    private List<ID> incomingAccountIDs = new List<ID>();
    private String incomingInvoiceId;
    
    private Date invoiceDate;
    private Date invoiceDateNext;
    
    global final Set<ID> idList = new Set<ID>();
    
    
    public BatchPreCheckInvoicedata(List<ID> accountIdsPassed, ID invoiceId){
       incomingAccountIDs = accountIdsPassed;
       incomingInvoiceId = invoiceId;
       system.debug('incomingAccountIDs + incomingInvoiceId = ' + incomingAccountIDs + ' - ' + incomingInvoiceId);
    }
    
    public Database.QueryLocator start(Database.BatchableContext context)
    {
        if(incomingInvoiceId != null && incomingInvoiceId != '')
        {
        	return Database.getQueryLocator([SELECT ID, Invoice__r.Invoice_Due_Date__c,Employer_Account__r.id FROM Sedera_Product_Report__c WHERE Invoice__r.id = :incomingInvoiceId]);
        } 
        else
        {
            return Database.getQueryLocator([SELECT ID, Invoice__r.Invoice_Due_Date__c,Employer_Account__r.id FROM Sedera_Product_Report__c WHERE Invoice__r.Submission_Status__c = 'In Review']);
        }
    }
    
    public void execute(Database.BatchableContext context, List<Sedera_Product_Report__c> scope)
    { 
        for(Sedera_Product_Report__c spr :scope)
        {
            idList.add(spr.Employer_Account__r.id);
            invoiceDate = spr.Invoice__r.Invoice_Due_Date__c;        
        	invoiceDateNext = invoiceDate.addMonths(1);
        }

		delete scope;
    }
    
    public void finish(Database.BatchableContext context)
    {
        system.debug('invoiceDate & invoiceDateNext' + invoiceDate + ' - ' + invoiceDateNext);
        system.debug('idList = ' + idList);
        
        List<Invoice_Adjustment__c> iaList = new List<Invoice_Adjustment__c>();
        iaList = [SELECT ID FROM Invoice_Adjustment__c WHERE Invoice_Period__c = :invoiceDateNext AND Prior_Invoice_Adjustment__c != null AND Employer_Account__r.id IN :idList];
        
        system.debug('iaList size = ' + iaList.size());
        
        delete iaList;
        
        InvoiceGeneration b = new InvoiceGeneration(incomingAccountIDs, incomingInvoiceId); 
        database.executebatch(b,200);
    }
}