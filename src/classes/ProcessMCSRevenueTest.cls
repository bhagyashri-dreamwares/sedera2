@isTest
public class ProcessMCSRevenueTest {
	
    @TestSetup
    public static void createTestData()
    {
        //Create custom setting MCS_Products__c
        List<MCS_Products__c> mcsConfigList = new List<MCS_Products__c>();
        MCS_Products__c mcsConfig = new MCS_Products__c(Name = '2nd MD',
                                                              Product_Name__c = '2nd MD',
                                                              Base_Amount__c = 6.5);
        mcsConfigList.add(mcsConfig);
        
        MCS_Products__c mcsConfig2 = new MCS_Products__c(Name = 'Teledoc',
                                                              Product_Name__c = 'Teledoc',
                                                              Base_Amount__c = 3.75);
        mcsConfigList.add(mcsConfig2);
        
        insert mcsConfigList;
        
        //Create Account
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        
        //Pricing__c x=new Pricing__c(Employer_Name__c=null,O30IUA500__c=30,EC_Tier__c=1.90, EF_Tier__c=2.90, EO_Tier__c=1.00, ES_Tier__c=2.00, Is_Base_Price_Record__c=true, Liberty_Rx__c=45.00, MCS_Additional__c=1.05, MCS_Base__c=168.00, MSEC__c=100.00, MSEF__c=150.00, MSEO__c=55.00, MSES__c=100.00, Name='Default Access Pricing', PSEC__c=49.82, PSEF__c=68.23, PSEO__c=31.41, PSES__c=49.82, RecordTypeId=Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Access Pricing').getRecordTypeId(),  Teladoc_Reduction__c=-4.35, Tobacco_Use__c=75.00, U30IUA500__c=0.70, U30IUA1000__c=0.65, U30IUA1500__c=0.55, U30IUA2500__c=0.45, U30IUA5000__c=0.25, U40IUA500__c=0.85, U40IUA1000__c=0.80, U40IUA1500__c=0.70, U40IUA2500__c=0.52, U40IUA5000__c=0.40, U50IUA500__c=1.00, U50IUA1000__c=0.85, U50IUA1500__c=0.75, U50IUA2500__c=0.62, U50IUA5000__c=0.49, U60IUA500__c=1.30, U60IUA1000__c=1.10, U60IUA1500__c=1.00, U60IUA2500__c=0.87, U60IUA5000__c=0.62, U65IUA500__c=2.65, U65IUA1000__c=2.30, U65IUA1500__c=2.15, U65IUA2500__c=1.92, U65IUA5000__c=1.49, X2nd_MD_Reduction__c=-6.00, X500T2__c=0.920, X500T3__c=0.840, X1000T2__c=0.925, X1000T3__c=0.850, X1500T2__c=0.925, X1500T3__c=0.850, X2500T2__c=0.925, X2500T3__c=0.850, X5000T2__c=0.913, X5000T3__c=0.825);
        //insert x;
        
        Account employerAccount = new Account(Name = 'Sofsyst', RecordtypeId = recordtypeId, Phone = '9898989890',
                                              Default_Product__c = 'Sedera Access', Available_IUA_Options__c = '1000',
                                              MS_Pricing__c = 'New Pricing', Subscription_Status__c = 'Active',
                                              Invoice_Email_Address__c = 'test@test.com', Enrollment_date__c = Date.newInstance(2018,1,7));
        insert employerAccount;
		
        
        //Create Contact
        recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();       
        
        Contact contact = new Contact(FirstName = 'Test', LastName = 'Sample', AccountId = employerAccount.Id, RecordtypeId = recordtypeId);
        insert contact;
        
        Contact contact2 = new Contact(FirstName = 'Test2', LastName = 'Sample2', AccountId = employerAccount.Id, RecordtypeId = recordtypeId);
        insert contact2;
        
        recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        
        Account memAccount = new Account(Name='Test', RecordtypeId = recordtypeId, First_Name__c = 'Ali', Last_Name__c = 'Jorg', Phone = '9898989890', 
                                         Account_Employer_Name__c = employerAccount.Id, Subscription_Status__c = 'Active',
                                         Enrollment_date__c = Date.newInstance(2018,1,10), Health_Care_Sharing__c = true, Dependent_Status__c = 'EO',
                                         Date_of_Birth__c = Date.newInstance(1995, 5, 20));
        insert memAccount;
        
        Account memAccount2 = new Account(Name='Test', RecordtypeId = recordtypeId, First_Name__c = 'Ali2', Last_Name__c = 'Jorg2', Phone = '9898989890', 
                                         Account_Employer_Name__c = employerAccount.Id, Subscription_Status__c = 'Active',
                                         Enrollment_date__c = Date.newInstance(2018,1,7), Health_Care_Sharing__c = true, Dependent_Status__c = 'EO',
                                         Date_of_Birth__c = Date.newInstance(1995, 5, 20));
        insert memAccount2;
        
        Product2 product1 = new Product2(Name = 'Select Services EF', ProductCode = 'Pro-X12', isActive = true, Family='Assure Voice-Bus',
                                         price__c = 15000, Quickbooks_ID__c = '28');
        insert product1;
        
        Product2 product2 = new Product2(Name = 'Preventive Sharing EO', ProductCode = 'Pro-X10', isActive = true, Family='Assure Voice-Bus',
                                         price__c = 25000, Quickbooks_ID__c='26');
        insert product2;
        
        //Create Assets
        List<Asset> assetList = new List<Asset>();
        Asset sederaPrduct = new Asset(Name = 'Test Asset1', AccountId = memAccount.Id, Product2Id = product2.Id,
                                       Product2 = product2, Employer_Account__c = employerAccount.Id);
        assetList.add(sederaPrduct);
        
        sederaPrduct = new Asset(Name = 'Test Asset2', AccountId = memAccount.Id, Product2Id = product1.Id,
                                 Product2 = product1, Employer_Account__c = employerAccount.Id);
        assetList.add(sederaPrduct);
                
        sederaPrduct = new Asset(Name = 'Test Asset3', AccountId = memAccount.Id, Product2Id = product1.Id,
                                 Product2 = product1, Employer_Account__c = employerAccount.Id);
        assetList.add(sederaPrduct);
                
        sederaPrduct = new Asset(Name = 'Test Asset4', AccountId = memAccount.Id, Product2Id = product1.Id,
                                 Product2 = product1, Employer_Account__c = employerAccount.Id);
        assetList.add(sederaPrduct);
        
        sederaPrduct = new Asset(Name = 'Test Asset5', AccountId = memAccount.Id, Product2Id = product1.Id,
                                 Product2 = product1, Employer_Account__c = employerAccount.Id);
        assetList.add(sederaPrduct);
        
        sederaPrduct = new Asset(Name = 'Test Asset1', AccountId = memAccount2.Id, Product2Id = product2.Id,
                                       Product2 = product2, Employer_Account__c = employerAccount.Id);
        assetList.add(sederaPrduct);
        
        insert assetList;
        
        Invoice__c inv = new Invoice__c();
        inv.Account__c = employerAccount.id;
        inv.Submission_Status__c = 'In Review';
        inv.Invoice_Date__c = Date.newInstance(2018,27,11);
        inv.MCS_Existing_Member_Revenue_Percent__c = 9.9;
        inv.MCS_New_Member_Revenue_Percent__c = 100;
        INSERT inv;
        
    }
    
    static testMethod void tesMCSRevenue() {
        
        Test.startTest();
        ProcessMCSRevenue obj = new ProcessMCSRevenue();
        DataBase.executeBatch(obj);
        Test.stopTest();
        
    }
}