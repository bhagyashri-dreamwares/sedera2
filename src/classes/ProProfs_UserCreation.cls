public class ProProfs_UserCreation {

    
    public Contact con {
        get;
        set;
    }


    public Contact create() {
        Http h = new Http();
        HttpRequest req = new httpRequest();
        HTTPResponse res;
        Map <string, string> mapResponse;
        string otp=EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(0,6);
        req.setEndpoint('https://www.proprofs.com/api/classroom/v1/user/register/');
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('conept', 'application/json');
        req.setBody('{"token": "' + system.label.Proprofs_Apitoken + '","username": "' + system.label.Proprofs_UserName + '","email": "' + con.Email+ '","fname": "' + con.FirstName+ '","lname":"'+con.Lastname+'","group": [' + system.label.Proprofs_DefaultGroupsAssignation + '],"password":"'+otp+'","id":"'+con.Id+'"}');
                                                                      /** "group": ["sales","finance","marketing"]    **/
                                                                          
        Organization Org = [select isSandbox from organization limit 1];

        if ((Org.isSandbox)){
            
            if(!test.isRunningTest())
            res = h.send(req);
            else{
              res=new Httpresponse();
              res.setbody('{"status":"success","token": "gsgagdsgdsahdsadhsdaa","username": "test200","email": "test@gmail.com","id": "'+[select id from contact limit 1].id+'","fname": "Test2","lname": "test200"}');
              res.setstatusCode(200);
            }
            system.debug(res.getbody());
            mapResponse =  (Map <string, string>)JSON.deserializeStrict(res.getBody(),Map <string, string>.class);
            
            if(res.getStatuscode()==200 && mapResponse.get('status')=='SUCCESS'){
             con.Proprofs_Id__c = con.id;             
             con.Proprofs_OTP__c=otp;     
             con.ProProfs_IsUserCreated__c=true;
             con.Training_status__c='Training Sent';
             con.Proprofs_Log__c = (con.Proprofs_Log__c==null?'':con.Proprofs_Log__c) + '\n\nCreatedDateTime: '+string.valueOf(system.now())+'\n';
             con.Proprofs_UserCreatedDateTime__c=system.now();
            }
            else{
             con.Proprofs_Log__c = (con.Proprofs_Log__c==null?'':con.Proprofs_Log__c) + '\n\nDateTime: '+string.valueOf(system.now())+'\n'+res.getBody()+'\n';
            }
        }
        else{
           system.debug('No ProProfs User Creation');
        }
        return con;

    }
    
    
}