/*
* @PURPOSE				:	APEX CONTROLLER CLASS FOR HorizonApplication
* @AUTHOR				:	DWS
* @CREATED DATE			:	11 APR 2019
* @LAST UPDATED DATE	:	DATE			CHANGED BY		CHANGES 	
* 							11 APR 2019 	KULDIP			INITIAL CHANGES
*/ 
public class HorizonApplicationController {
    
    // GET CURRENT LOGGED-IN USER INFORMATION
    @AuraEnabled 
    public static HorizonApplicationHelper.ResponseDTO getUserInformation(){
        return HorizonApplicationHelper.getUserInformation();
    }
    
    // GET CURRENT LOGGED-IN USER INFORMATION
    @AuraEnabled 
    public static HorizonApplicationHelper.ResponseDTO getCurrenBankDataUserRec(String currentBankDataUserId){
        return HorizonApplicationHelper.getCurrenBankDataUserRec(currentBankDataUserId);
    }
}