public class PdfGeneration
{
    List<Case> cases = new List<Case>();
    public pdfGeneration()
    {
        //set<id> ids = new set<id> {'5001D000000gB4MQAU'};
        //cases = new List<Case>([SELECT ID FROM Case WHERE ID IN :ids]);
        
        set<id> ids = new set<id>();
        cases = new List<Case>(
                [SELECT ID FROM Case WHERE ID IN 
                    (SELECT Case__c 
                     FROM Bill__c 
                     WHERE Amount_Shared_This_Week__c > 0
                    )
                ]);
        
    }
    
    public void executeJobs()
    {
        List<SDOC__SDJob__c> jobList = new List<SDOC__SDJob__c>();
        List<SDOC__SDTemplate__c> template = [SELECT ID FROM SDOC__SDTemplate__c WHERE Name = 'EONS' LIMIT 1]; 
        
        for( Case cse : cases)
        {
            SDOC__SDJob__c job = 
            new SDOC__SDJob__c(SDOC__Oid__c = cse.Id, 
                               SDOC__Start__c = TRUE,
                               SDOC__runAs__c = UserInfo.getUserName(),
                               SDOC__Status__c = 'Selected',
                               SDOC__ObjApiName__c = 'Case', 
                               SDOC__SendEmail__c = '0', 
                               SDOC__Doclist__c = template[0].id, //'a2N1D0000000S3BUAU',
                               SDOC__type__c = 'Bulk Job');
            jobList.add(job);
        }
        insert jobList;
    }
}