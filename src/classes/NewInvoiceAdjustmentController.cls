/*
 * @purpose: Controller of NewInvoiceAdjustment VF page
 */ 
public class NewInvoiceAdjustmentController {
    public String selectedAccountId { get; set; }
    public Date selectedInvoiceDate { get; set; }
    public Decimal selectedTotalAdjustment { get; set; }
    public String employerRecordTypeId { get; set; }
    
    public NewInvoiceAdjustmentController() {
        employerRecordTypeId = ((String) Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId());
    }
   /*
    * @purpose: Method to fetch the invoice and invoice adjustment for it
    */
    @RemoteAction
    public static Response fetchInvoiceAdjustments(String accountId, String invoiceDate, String totalAdjustment) {
        
        
        List<InvoiceWrapper> listInvoiceWrapper = new List<InvoiceWrapper>();
        Response response = new Response(true,'',NULL);
        
        Date invoiceDateVal = Date.valueOf(invoiceDate); 
        String strDefaultProductType;
       
        if(String.isNotBlank(accountId)){
            
            try{
                
                Account account = [SELECT Id, Default_Product__c
                                   FROM Account 
                                   WHERE Id = :accountId
                                   LIMIT 1];
                if(account != NULL && String.isNotBlank(account.Default_Product__c)){
                    strDefaultProductType = account.Default_Product__c;    
                }
                
                String invoiceQueryCondition;
                String invoiceAdjQueryCondition;
                List<Id> invoiceIdList = new List<Id>();
                if(String.isNotBlank(strDefaultProductType)){
                    
                    if(strDefaultProductType.equals('Sedera Select')){
                       
                        Date invoicePreviousMaonthDate = invoiceDateVal.addMonths(-1);
                        invoiceQueryCondition = 'AND (Invoice_Date__c = :invoiceDateVal '+
                                                ' OR  Invoice_Date__c = :invoicePreviousMaonthDate)';
                        
                        invoiceAdjQueryCondition = 'AND (Invoice_Period__c = :invoiceDateVal '+
                                                   ' OR  Invoice_Period__c = :invoicePreviousMaonthDate) AND Invoice__r.Invoice_Date__c != NULL';
                    }else if(strDefaultProductType.equals('Sedera Access')){
                        
                        invoiceQueryCondition =  'AND Invoice_Date__c = :invoiceDateVal'; 
                        
                        invoiceAdjQueryCondition = 'AND Invoice_Period__c = :invoiceDateVal AND Invoice__r.Invoice_Date__c != NULL';
                    }    
                    
                    String invoiceQuery = ' SELECT Gross_Monthly_Total_Existing__c, Gross_Monthly_Total_New__c, Id, Member_Services_Revenue__c, '+
                                          ' Liberty_Rx_Total__c, Invoice_Date__c, Name '+
                                          ' FROM Invoice__c '+
                                          ' WHERE Account__c = :accountId ' + invoiceQueryCondition ;
                    
                    for(Invoice__c invoice : database.query(invoiceQuery)){        
                        listInvoiceWrapper.add(new InvoiceWrapper(invoice));
                        invoiceIdList.add(invoice.Id);
                    }                    
                    
                    String invoiceAdjustmentQuery = ' SELECT Id, Invoice__r.Name, Description_of_Change__c, Date_Change_Requested__c, Invoice__c, '+
                                                    ' MCS_Bucket__c, Amount__c '+
                                                    ' FROM Invoice_Adjustment__c '+
                                                    ' WHERE Employer_Account__c = :accountId '+ invoiceAdjQueryCondition +' AND Invoice__c IN :invoiceIdList';   
                   
                    List<Invoice_Adjustment__c> invoiceAdjustmentList =  database.query(invoiceAdjustmentQuery);                  	
                   
                    if(!listInvoiceWrapper.isEmpty()){
                        
                        if(!invoiceAdjustmentList.isEmpty()){
                            
                            for(Invoice_Adjustment__c invoiceAdjustment :invoiceAdjustmentList) {  
                               
                                listInvoiceWrapper[0].listInvoiceAdjustment.add(new InvoiceAdjustmentWrapper(invoiceAdjustment));  
                            }    
                        }
                    }
                }
                response = new Response(true,'Data is Sucessfully retrived',listInvoiceWrapper);
                
            }catch(Exception ex){
                 System.debug('Error occured while fetching Invoive Details is:::'+ex.getMessage());
                 response = new Response(false,'Error occured while fetching Invoive Details is:::'+ex.getMessage(),NULL);    
            }
        }
        return response;
    }
    
   /*
    * @purpose: Upsert the Invoice Adjustment for the invoices
    */
    @RemoteAction
    public static Response updateInvoiceAdjustment(String strInvoiceAdjustmentData, List<String> invoiceToBeDeleteList, String accountId){
        
      
        List<finalInvocieAdjustmentWrapper> invoiceAdjWrapperList = (List<finalInvocieAdjustmentWrapper>) JSON.deserialize(strInvoiceAdjustmentData, List<finalInvocieAdjustmentWrapper>.class);
        List<Invoice_Adjustment__c> invoiceAdjustmentToBeUpsert = new List<Invoice_Adjustment__c>();
        Response response = new Response(true,'',NULL);
        
        try{
            if(!invoiceAdjWrapperList.isEmpty()){
                
                for(finalInvocieAdjustmentWrapper finalInvoiceAdjustmentWrapperObj :invoiceAdjWrapperList){
                    
                    String invocieId = finalInvoiceAdjustmentWrapperObj.Id;
                    
                    if(invocieId.contains('checkbox')){                        
                        
                        Invoice_Adjustment__c invoiceAdjustmentObj =  new Invoice_Adjustment__c();
                        
                        invoiceAdjustmentObj.Amount__c = String.isNotBlank(finalInvoiceAdjustmentWrapperObj.amount) ? Decimal.valueOf(finalInvoiceAdjustmentWrapperObj.amount) : NULL;
                        invoiceAdjustmentObj.Date_Change_Requested__c = setStringToDateFormat(finalInvoiceAdjustmentWrapperObj.dateChangedRequested) != NULL ? setStringToDateFormat(finalInvoiceAdjustmentWrapperObj.dateChangedRequested) : NULL;
                        if(!(finalInvoiceAdjustmentWrapperObj.mscBucket).equals('None')){
                            invoiceAdjustmentObj.MCS_Bucket__c = finalInvoiceAdjustmentWrapperObj.mscBucket;
                        }else{
                            invoiceAdjustmentObj.MCS_Bucket__c = '';    
                        }
                        invoiceAdjustmentObj.Description_of_Change__c =  String.isNotBlank(finalInvoiceAdjustmentWrapperObj.description) ? finalInvoiceAdjustmentWrapperObj.description : '';
                        invoiceAdjustmentObj.Invoice__c = String.isNotBlank(finalInvoiceAdjustmentWrapperObj.invoiceName) ? finalInvoiceAdjustmentWrapperObj.invoiceName : '';
                        invoiceAdjustmentObj.Employer_Account__c = String.isNotBlank(accountId) ? accountId : '';                        
                        
                        invoiceAdjustmentToBeUpsert.add(invoiceAdjustmentObj);
                        
                    }else{
                        Id invoiceIdToBeUpsert = invocieId.substringAfter('_'); 
                        invoiceAdjustmentToBeUpsert.add(new Invoice_Adjustment__c(Id = String.isNotBlank(invoiceIdToBeUpsert) ? invoiceIdToBeUpsert : '',
                                                                                  Amount__c = String.isNotBlank(finalInvoiceAdjustmentWrapperObj.amount) ? Decimal.valueOf(finalInvoiceAdjustmentWrapperObj.amount) : NULL,
                                                                                  Date_Change_Requested__c = setStringToDateFormat(finalInvoiceAdjustmentWrapperObj.dateChangedRequested) != NULL ? setStringToDateFormat(finalInvoiceAdjustmentWrapperObj.dateChangedRequested) : NULL,
                                                                                  MCS_Bucket__c = finalInvoiceAdjustmentWrapperObj.mscBucket != 'None' ? finalInvoiceAdjustmentWrapperObj.mscBucket : '',
                                                                                  Description_of_Change__c = String.isNotBlank(finalInvoiceAdjustmentWrapperObj.description) ? finalInvoiceAdjustmentWrapperObj.description : '',
                                                                                  Employer_Account__c = String.isNotBlank(accountId) ? accountId : ''));        
                    }
                }
                upsert invoiceAdjustmentToBeUpsert;
            }
            
            if(!invoiceToBeDeleteList.isEmpty()){
                
                List<Id> invoiceToBeDeletedIdList = new List<Id>();
                for(String strInvoiceId :invoiceToBeDeleteList){
                    if(!strInvoiceId.contains('_checkbox')){
                        invoiceToBeDeletedIdList.add(strInvoiceId.substringAfter('_'));    
                    }
                }
                
                List<Invoice_Adjustment__c> invoiceToBeDeletedList = [SELECT Id FROM Invoice_Adjustment__c WHERE Id IN :invoiceToBeDeletedIdList];
                if(invoiceToBeDeletedList.size() > 0){
                    delete invoiceToBeDeletedList;
                }
            }  
            response = new Response(true,'Invoice Adjustment is Updated Sucessfully',NULL);
        }catch(Exception ex){
            System.debug('Error occured while updating Invoice and Invoice Adjustment::::'+ex.getMessage());
            response = new Response(false,'Error occured while updating Invoice and Invoice Adjustment::::'+ex.getMessage(), NULL);
        }
        return response;
    }
    
   /*
    * @purpose: Wrapper class to hold Invoice details
    */
    public class InvoiceWrapper {
        
        public String invoiceId;
        public String invoiceName;
        public String mcsExisting;
        public String mcsNew;
        public String memeberServiceRevenue;
        public String libertyRx; 
        List<InvoiceAdjustmentWrapper> listInvoiceAdjustment;
        
        public InvoiceWrapper() {
            invoiceName = '';
            mcsExisting = '';
            mcsNew = '';
            memeberServiceRevenue = '';
            libertyRx = '';
            listInvoiceAdjustment = new List<InvoiceAdjustmentWrapper>();
        }
        
        public InvoiceWrapper(Invoice__c invoice){
         
            invoiceId = invoice.Id;
            String strFormatedDate = String.isNotBlank(String.valueOf(invoice.Invoice_Date__c)) ?  invoice.Invoice_Date__c.month()+'-'+invoice.Invoice_Date__c.day()+'-'+invoice.Invoice_Date__c.year() : '';
            invoiceName = invoice.Name +' '+ strFormatedDate;
            mcsExisting = invoice.Gross_Monthly_Total_Existing__c != NULL ? '$ ' +invoice.Gross_Monthly_Total_Existing__c : '$ 0.0';
            mcsNew = invoice.Gross_Monthly_Total_New__c != NULL ? '$ ' +invoice.Gross_Monthly_Total_New__c : '$ 0.0';
            memeberServiceRevenue = invoice.Member_Services_Revenue__c != NULL ? '$ ' +invoice.Member_Services_Revenue__c : '$ 0.0';
            libertyRx = invoice.Liberty_Rx_Total__c != NULL ? '$ ' +invoice.Liberty_Rx_Total__c : '$ 0.0';
            listInvoiceAdjustment = new List<InvoiceAdjustmentWrapper>();
        }
    }
    
   /*
    * @purpose: Wrapper class to hold Invoice Adjustment details
    */
    public class InvoiceAdjustmentWrapper {
        
        public String parnetId;
        public String id;
        public Boolean isSelected;
        public String strInvoice;
        public String strDescription;
        public String strDateChangeRequest;
        public String strMCSBucket;
        public Decimal amount;
        
        public InvoiceAdjustmentWrapper() {
            isSelected = false;
            strInvoice = '';
            strDescription = '';
            strMCSBucket = '';
            amount = 0;
        }
        
        public InvoiceAdjustmentWrapper(Invoice_Adjustment__c invoiceAdjustment) {
            
            isSelected = false;
            id = invoiceAdjustment.Id;
            parnetId = invoiceAdjustment.Invoice__c;
            strInvoice = invoiceAdjustment.Invoice__r.Name;
            strDescription = String.isNotBlank(invoiceAdjustment.Description_of_Change__c) ? invoiceAdjustment.Description_of_Change__c : '';
            strMCSBucket = String.isNotBlank(invoiceAdjustment.MCS_Bucket__c) ? invoiceAdjustment.MCS_Bucket__c : 'None';
            amount = invoiceAdjustment.Amount__c != NULL ? invoiceAdjustment.Amount__c : 0;
            String tempDate = invoiceAdjustment.Date_Change_Requested__c != NULL ? invoiceAdjustment.Date_Change_Requested__c.year() + '-' +
                                                                                   (invoiceAdjustment.Date_Change_Requested__c.month() <= 9 ? String.valueOf('0'+invoiceAdjustment.Date_Change_Requested__c.month()) : String.valueOf(invoiceAdjustment.Date_Change_Requested__c.month())) + '-' +                                                                                   
                                                                                   (invoiceAdjustment.Date_Change_Requested__c.day() <= 9 ? String.valueOf('0'+invoiceAdjustment.Date_Change_Requested__c.day()) : String.valueOf(invoiceAdjustment.Date_Change_Requested__c.day()))  : '';  
                                                                          
            strDateChangeRequest = invoiceAdjustment.Date_Change_Requested__c != NULL ? tempDate : '';
        }
    }
    
   /*
    * @purpose: Wrapper class to hold Invoice Adjustment details that comes from Visualforce Page
    */
    public class finalInvocieAdjustmentWrapper{
        
        public String id;
        public String invoiceName;
        public String description;
        public String dateChangedRequested;
        public String mscBucket;
        public String amount;
    }
       
   /*
    * @purpose: Create the Date from the String Date
    */
    private static Date setStringToDateFormat(String myDate) {
        
        if(String.isNotBlank(myDate)){
            
            String[] strDate = myDate.split('-');
            Integer intDate = integer.valueOf(strDate[2]);
            Integer intMonth = integer.valueOf(strDate[1]);
            Integer intYear = integer.valueOf(strDate[0]);
            Date dateObj = Date.newInstance(intYear, intMonth, intDate);
            return dateObj;    
        }
        return NULL;
    }
}