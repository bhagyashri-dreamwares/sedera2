/**
 * @Purpose            : Controller class for VF page : InvoiceDetail
 * @Created Date       : 16/08/2018
 */ 
public class InvoiceDetailController {
    public List<AccountWrapper> memberAccountWrapper {get;  set;}
    public Boolean isAccountValid  {get;  set;} 
    public String invoiceName {get; set;}
    public String accountId {get; set;}
    public String invoiceId {get; set;}
    public Boolean isDownload {get;  set;}
   
    //Constructor
    public InvoiceDetailController(){
        memberAccountWrapper = new List<AccountWrapper>();
        invoiceName = '';
        
        String urlPageParameter = Apexpages.currentpage().getparameters().get('id');
        
        isDownload = String.isNotBlank(ApexPages.currentPage().getParameters().get('isDownload')) ? true : false;
                 
        if (String.isNotBlank(urlPageParameter) && urlPageParameter.contains('-') && urlPageParameter.split('-').size() == 2){
            List<String> pageParamList = urlPageParameter.split('-');
            
            if (String.isNotBlank(pageParamList[0]) && String.isNotBlank(pageParamList[1]) && isValidSalesforceId(pageParamList[0], Account.class) && isValidSalesforceId(pageParamList[1], Invoice__c.class)){
                //Account is valid now fetch Member Accounts details
                memberAccountWrapper = getMemberAccountsInfo(pageParamList);
                System.debug('Final memberAccountWrapper::'+memberAccountWrapper);
            }else{
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid parameters! Please provide a valid parameter in the form Account Id - Invoice Id.!');
                ApexPages.addMessage(errorMsg);
            }
        }else{
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide a valid parameter in the form Account Id - Invoice Id.!');
            ApexPages.addMessage(errorMsg);
        }
    }   
    
    
    /**
     * @Purpose   : Method to check sfdcId is valid Id of sObject
     * @Parameter : Id and sObject type
     * @Return    : Returns true if Id is valid otherwise false
     */
    public Boolean isValidSalesforceId(String sfdcId, System.Type objectType){
        try {
            if (Pattern.compile('[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}').matcher(sfdcId).matches()){
                //Assign it to an Id before checking the type
                Id id = sfdcId;
                //Construct an instance of this sObject
                sObject sObj = (sObject) objectType.newInstance();
                //Set the ID of the new object to the value to test
                sObj.Id = id;
                
                return true;
            } 
        }catch ( Exception e ){
            isAccountValid = false;
            System.debug('Exception occured at line no. :'+e.getLineNumber()+' Error:'+e.getMessage());
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid Id. Please provide a valid Id.!');
            ApexPages.addMessage(errorMsg);
        }
        
        return false;
    }
    
    
    /**
     * @Purpose    : Method to fetch Member Account Subscription Info for the Invoice
     * @Parameters : Account Id and Invoice ID
     * @Return     : List<AccountWrapper>
     */
    public List<AccountWrapper> getMemberAccountsInfo(List<String> pageParamList){
        List<AccountWrapper> accountWrapperList = new List<AccountWrapper>();
        
        accountId = pageParamList[0];
        invoiceId = pageParamList[1];
        
        Map<String,List<Sedera_Product_Report__c>> sprMap = new Map<String,List<Sedera_Product_Report__c>>();
        
        if(accountId != null && invoiceId != null){
            try{
                //Check accountId is related to InvoiceId or not If yes, process further otherwise 
                Invoice__c invoice = [SELECT Id, Name, Account__c 
                                      FROM Invoice__c 
                                      WHERE Id =: invoiceId AND Account__c =: accountId];
     
                invoiceName = invoice.Name;

            }catch ( Exception e ){
                isAccountValid = false;
                System.debug('Exception occured at line no. :'+e.getLineNumber()+' Error:'+e.getMessage());
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to fetch Invoice.!');
                ApexPages.addMessage(errorMsg);
                return accountWrapperList;
            }
            
            isAccountValid = true;
            
            for(Sedera_Product_Report__c spr : [SELECT Id, Enrollment_Date__c, Subscription_Status__c, Dependent_Status__c, Amount__c,
                                                Date_of_Birth__c, ProductName__c, Health_Care_Sharing__c, Primary_Age__c,
                                                Member_Account__c, Member_Account__r.id, Member_Account__r.Name, Invoice__r.Name,
                                                Member_Account__r.Primary_Contact_id__c, Member_Account__r.IUA_Chosen__c, 
                                                Invoice__c, Invoice__r.Account__c, Employer_Account__c, Member_Account__r.Sub_Employer__c,
                                                Member_Account__r.Sub_Employer__r.Name
                                                FROM Sedera_Product_Report__c
                                                WHERE Invoice__c = : invoiceId 
                                                ORDER BY Member_Account__r.Sub_Employer__r.Name, Member_Account__r.Last_Name__c]){
                //If member account present
                if (spr.Member_Account__r != null){                                      
                    String name = spr.Member_Account__r.Name.replaceAll('\\s+', '_');
                    String key = name + ':' + String.valueOf(spr.Member_Account__r.id).right(3);
                                                               
                    if (sprMap.get(key) != null){
                        List<Sedera_Product_Report__c> sprList = sprMap.get(key);
                        sprList.add(spr);
                    }else{
                        sprMap.put(key, new List<Sedera_Product_Report__c>{spr});
                    }
                }
            }
                                                
            for (String key : sprMap.keySet()){
                List<Sedera_Product_Report__c> sprs = sprMap.get(key);
                Decimal totalAmount = calculateTotalAmount(sprs);
                AccountWrapper accountWrapper = new AccountWrapper(key,sprs, totalAmount);
               
                accountWrapperList.add(accountWrapper); 
            }
        }
                        
        return accountWrapperList;
    }
    
    
    /**
     * @Purpose    : Method to calculate sum of Amount of Sedera product report
     * @Parameters : List<Sedera_Product_Report__c> 
     * @Return     : Total amount
     */
    public Decimal calculateTotalAmount(List<Sedera_Product_Report__c> sederaProductReportList){
        Decimal totalAmount = 0.0;
                
        for (Sedera_Product_Report__c sederaProduct : sederaProductReportList){
            totalAmount += sederaProduct.Amount__c;
        }
                
        return totalAmount;
    }
    
    
    //Wrapper class for Member Account
    public class AccountWrapper{
        public String name {get; set;}
        public List<Sedera_Product_Report__c> sprList {get; set;}
        public Decimal amount {get; set;}
        
        public AccountWrapper(String name, List<Sedera_Product_Report__c> sprList, Decimal amount){
            this.name = name.replaceAll('_', ' ').substring(0,name.length()-4);
            this.sprList = sprList;
            this.amount = amount;
        }
    }
}