/*
*@Purpose : Test method for PauBoxEmailHelper class
*@Date : 31/07/2018
*/
@isTest
public class PauBoxEmailHelperTest{
    
    /*
* To initialise data
*/    
    /*@testSetup
private static void initData(){

//insert custom setting record
PauBoxConfiguration__c config = new PauBoxConfiguration__c();
config.Access_Token__c = 'Test';
config.Email_Subject__c = 'Test';
config.EndPoint_To_Get_Status__c = 'https://api.paubox.net/v1/sedera/';
config.Endpoint_To_Send_Email__c = 'https://api.paubox.net/v1/sedera/';
config.From_Email__c = 'test@test.com';
config.Site_Url__c = 'Test@test.com';
insert config;

// insert Paubox_Message_Template
Paubox_Message_Template__c template = new Paubox_Message_Template__c();
template.Paubox_Message_Type__c = 'EONS Shared';
template.Template_Body__c = 'Test';
template.Template_Subject__c = 'EONS Shared Test';

insert template;

// insert Paubox_Message_Template
Paubox_Message_Template__c template2 = new Paubox_Message_Template__c();
template2.Paubox_Message_Type__c = 'Invoice';
template2.Template_Body__c = 'Test';
template2.Template_Subject__c = 'Invoice Test';

insert template2;

//insert Account
Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
Account emp = new Account();
emp.recordTypeId = accRecordTypeId;
emp.Default_Product__c = 'Sedera Access';
emp.MS_Pricing__c = 'New Pricing';
emp.Available_IUA_Options__c = '500';
emp.Name = 'Test'; 
emp.Default_Product__c = 'Sedera Access';  
emp.Invoice_Email_Address__c = 'test@test.com, test2@test.com';        
insert emp;

Pricing__c price = new Pricing__c();
Id priceRecordTypeId = Schema.SObjectType.Pricing__c.getRecordTypeInfosByName().get('Select Pricing').getRecordTypeId();
price.RecordTypeId = priceRecordTypeId;
price.name = 'test';
price.Teladoc_Reduction__c = -4.35;
price.X2nd_MD_Reduction__c = -6.00;
price.EC_Tier__c = 1.09;
price.EF_Tier__c = 1.09; 
price.EO_Tier__c = 1.09; 
price.ES_Tier__c = 1.09;
price.Employer_Name__c = emp.id;

Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
Account account = new Account();
account.recordTypeId = devRecordTypeId;
account.Name = 'Test AccountName';
account.First_Name__c = 'FName';
account.Last_Name__c = 'LName';
account.Account_Employer_Name__c = emp.Id;
account.Primary_Email_Address__c = 'test@test.com';
insert account;

// insert contact
Contact contact = new Contact();
contact.AccountId = account.Id;
contact.LastName = 'Test Name';
contact.Email = 'test@test.com';
insert contact;

// insert Case
Case caseRecord = new Case();
caseRecord.ContactId = contact.Id;
caseRecord.Status = 'New';
caseRecord.Origin = 'Phone';
caseRecord.Type = 'HCS';
caseRecord.Correspondence_Email__c = 'test@test.com';
insert caseRecord;

// insert Attachment
Attachment attach = new Attachment(); 
attach.Name='Test.pdf'; 
attach.body= Blob.valueOf('Unit Test Attachment Body'); 
attach.parentId = caseRecord.Id; 
attach.ContentType = 'application/pdf'; 
attach.IsPrivate = false; 
attach.Description = 'Test'; 
insert attach;

Invoice__c inv = new Invoice__c();
inv.Account__c = emp.id;
inv.Submission_Status__c = 'In Review';
INSERT inv;       
}*/
    
    /*
* To test Email sending Functionality
*/
    @isTest(SeeAllData=true)
    private static void testEonsEmailFunctionality(){
        
        Attachment attachment = [SELECT Id, Name, ParentId
                                 FROM Attachment
                                 LIMIT 1];
        
        SDOC__SDoc__c sDoc = new SDOC__SDoc__c();
        sDoc.SDOC__Attachment_ID__c = attachment.Id;
        sDoc.SDOC__Attachment_Name__c = attachment.Name;
        sDoc.SDOC__ObjectID__c = attachment.ParentId;
        
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailHelperMock());
        
        Test.startTest();
        
        insert sDoc;
        PauBoxEmailHelper.sendEmail(new List<String>{sDoc.id});
        
        Test.stopTest();
        List<Paubox_Message__c> pauboxMessageRecordList = [SELECT Id FROM Paubox_Message__c];
        
        System.assert(pauboxMessageRecordList.size() > 0);
        
    }
    @isTest(SeeAllData=true)
    private static void testInvoiceEmailFunctionality(){
        
        Invoice__c inv = [SELECT ID, Name, Account__c, Account__r.Name, Account__r.Invoice_Email_Address__c FROM Invoice__c WHERE Account__c!='' and Account__r.Invoice_Email_Address__c!='' LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailHelperMock());
        
        Test.startTest();
        PauBoxEmailHelper.sendInvoiceEmail('Invoice', inv);
        Test.stopTest();
        
        List<Paubox_Message__c> pauboxMessageRecordList = [SELECT Id FROM Paubox_Message__c];
        
        //System.assert(pauboxMessageRecordList.size() > 0);
    }
    public class  PauBoxEmailHelperMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest req) {
            
            HTTPResponse response = new HTTPResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"sourceTrackingId": "3d38ab13-0af8-4028-bd45-52e882e0d584","data": "Service OK"}');
            response.setStatusCode(200);
            
            return response;
        }
    }
    
    @isTest
    public static void testFunctionality() {
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
        Test.setMock(HttpCalloutMock.class, new PauBoxEmailHelperMock());
        
        
        //Paubox_Message__c pauboxMessageRecord = [SELECT Id FROM Paubox_Message__c LIMIT 1];
        Paubox_Message__c pauboxMessageRecord = new Paubox_Message__c(Name = 'test');
        insert pauboxMessageRecord;
        
        Account account = new Account(Name = 'test');
        insert account;
        
        System.debug('Account = ' + account);
        
        Dwolla_Datasheet__c dwollaDataSheet = new Dwolla_Datasheet__c(SSN__c = '123-46-7890',
            EIN__c = '00-0000000', Controller_SSN__c = '123-46-7890', First_Name__c = 'TestFirst', Last_name__c = 'TestLast',
            DBA_Name__c = 'Test DBA',Email_Address__c = 'abc@test.com', Account__c = account.Id);
        insert dwollaDataSheet;
        
        Invoice__c inv = new Invoice__c();
        inv.Account__c = account.id;
        inv.Submission_Status__c = 'In Review';
        INSERT inv;
        
        List<String> pauboxIdList = new List<String>();
        pauboxIdList.add(pauboxMessageRecord.Id);
        PauBoxEmailHelper.getpauboxMessageRecord(pauboxIdList);
        PauBoxEmailHelper.getEmailStatus('testing', pauboxMessageRecord.Id);
        
        dwolla_Transfer__c dwollaRecord = new dwolla_Transfer__c(Invoice__c = inv.id,
                                                                 Amount__c = 123.23, 
                                                                 Transfer_Submitted__c = System.today(), 
                                                                 Transfer_Processed__c = System.today());
        insert dwollaRecord;
        
        Dwolla_Transfer__c dt = [SELECT Id, 
                                    Name, 
                                    Amount__c, 
                                    Destination_Funding_Source_ID__c,
                                    Destination_Dwolla_Funding_Source__r.Name, 
                                    Source_Funding_Source_ID__c,
                                    Source_Dwolla_Funding_Source__r.Name,
                                    Invoice__c,
                                    Invoice__r.id,
                                    Invoice__r.Name, 
                                    invoice__r.Account__c,
                                    invoice__r.Account__r.id,
                                    invoice__r.account__r.name,
                                    Invoice__r.Quickbooks_Invoice_ID2__c, 
                                    Dwolla_Transfer_ID__c,
                                    Transfer_Submitted__c, 
                                    Transfer_Response__c,
                                    Transfer_Status__c,
                                    Transfer_Type__c,
                                    Transfer_Processed__c
                                    FROM Dwolla_Transfer__c
                                    WHERE id = :dwollaRecord.id
                                    LIMIT 1];
        
        //dwolla_Transfer__c dwollaRecord = [Select Id from dwolla_Transfer__c LIMIT 1];
        PauBoxEmailHelper.SendTransferCreatedEmail(dt);
        PauBoxEmailHelper.SendTransferCompletedEmail(dt);
    }
    
    @isTest
    static void SendCustomerEmailTest(){
        UtilityClass_For_Static_Variables.CheckRecursiveForAccountTrigger=1;
        Account account = new Account(Name = 'test');
        insert account;
        
        Dwolla_Datasheet__c dwollaDataSheet = new Dwolla_Datasheet__c(SSN__c = '123-46-7890',
            EIN__c = '00-0000000', Controller_SSN__c = '123-46-7890', First_Name__c = 'TestFirst', Last_name__c = 'TestLast',
            DBA_Name__c = 'Test DBA',Email_Address__c = 'abc@test.com', Account__c = account.Id);
        insert dwollaDataSheet;
        
        PauBoxEmailHelper.SendCustomerCreatedEmail(account);
        PauBoxEmailHelper.SendCustomerSuspendedEmail(account);
        PauBoxEmailHelper.SendVerificationDocumentNeededEmail(account);
        PauBoxEmailHelper.SendVerificationDocumentUploadedEmail(account);
        PauBoxEmailHelper.SendVerificationDocumentFailedEmail(account);
    }
    
    @isTest
    static void codecoverage(){
        PauBoxEmailHelper.codeCoverage();
        PauBoxEmailBatch.codecoverage();
    }
}