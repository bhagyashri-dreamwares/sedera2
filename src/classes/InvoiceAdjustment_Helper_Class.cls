global  class InvoiceAdjustment_Helper_Class {
    @InvocableMethod(label='Process an InvoiceAdjustment and update associated invoices. Action can be either insert or delete.')
    public static void ProcessInvoiceAdjustment(List<InvoiceAdjustmentHelperInput> values) {
    
        //Lookup Adjustment product
        Product2 adjustmentProduct = [select id from Product2 where name = 'Adjustment'];
        //All Inv Adj Ids
        Set<ID> invAdjIds = new Set<ID>();
        //Inv Adj Ids of deletes to lookup additional adjustments and invoice line items
        Set<ID> invAdjParentDeleteIDs = new Set<ID>();
        //All invoice ids
        Set<ID> invIds = new Set<ID>();
        
        for(InvoiceAdjustmentHelperInput invAdjHelperInput : values)
        {
            invAdjIds.add(invAdjHelperInput.InvoiceAdjustmentId);
            if(invAdjHelperInput.action == 'delete')
            {
                invAdjParentDeleteIDs.add(invAdjHelperInput.InvoiceAdjustmentId);
            }
        }
        
        //FOR INSERT
        //Map InvAdj Id to InvAdju
        Map<ID,Invoice_Adjustment__c> invAdjMap = new Map<ID,Invoice_Adjustment__c>();
        //Map Invoice ID to Invoice record
        Map<ID,invoice__c> invMap = new Map<ID,Invoice__c>();

        for(Invoice_Adjustment__c invoiceAdj : [SELECT id, 
                                                          amount__c, 
                                                          applied__c, 
                                                          Description_of_Change__c, 
                                                          invoice__c, 
                                                        MCS_Bucket__c, 
                                                          Employer_Account__c, 
                                                          Invoice_Period__c,
                                                          Invoice_Line_Item__c, 
                                                          Reprocessed__c
                                                          FROM Invoice_Adjustment__c 
                                                          WHERE id IN :invAdjIds])
        {
            invAdjMap.put(invoiceAdj.id,invoiceAdj);
            invMap.put(invoiceAdj.invoice__c,new Invoice__c(id = invoiceAdj.invoice__c));
            system.debug('trying to add to InvIds');
            invIds.add(invoiceAdj.invoice__c);
        }
        
 
        for(Invoice__c inv : [select id, 
                              Gross_Monthly_Total_Existing__c, 
                              Gross_Monthly_Total_New__c, 
                              X2nd_MD_Total__c, 
                              Teledoc_Total__c, 
                              Member_Services_Revenue__c, 
                              Liberty_Rx_Total__c, 
                              Account__c
                              from Invoice__c 
                              where id IN :invIds])
        {
          for(ID key : invMap.KeySet())
            {
                Invoice__c invInMap = invMap.get(key);
                if(invInMap.id == inv.id)
                  invMap.put(key,inv);
            }
        }
        
        system.debug('keysey');
        system.debug(invmap.keyset());
        //FOR DELETE
        List<Invoice_Adjustment__c> invAdjDeleteList = new List<Invoice_Adjustment__c>();
        List<Invoice_Line_Item__c> invLineItemDeleteList = new List<Invoice_Line_Item__c>();
        if(invAdjParentDeleteIDs.size() > 0)
        {
            system.debug('keysey2');
            invAdjDeleteList = [Select id from Invoice_Adjustment__c where Prior_Invoice_Adjustment__c IN :invAdjParentDeleteIDs or id in :invAdjPArentDeleteIDs];
            Set<ID> invLineIds = new Set<ID>();
            for(ID invAdjId : invAdjParentDeleteIDs)
            {
                system.debug('keysey3');
                Invoice_Adjustment__c invAdj = invAdjMap.get(invAdjId);
                if (invAdj.Invoice_Line_Item__c != null)
                {
                    system.debug('invlitem not null');
                	invLineIds.add(invAdj.Invoice_Line_Item__c);
                }
                

            }
            if(invLineIds.size() > 0)
            {
                system.debug('test');
              invLineItemDeleteList = [select id from Invoice_Line_Item__c where id IN :invLineIds];
            }
        }

        Map<ID, Invoice_Line_Item__c> newInvLineItemsMap = new Map<ID, Invoice_Line_Item__c>();
        List<Invoice_Adjustment__c> newInvAdjList = new list<Invoice_Adjustment__c>();
        for (InvoiceAdjustmentHelperInput invAdjHelperInput : values)
        {
            //retrieve values from the invoice adjustment
            //retrieve values from the invoice it affects
            Invoice_Adjustment__c invAdj = invAdjMap.get(invAdjHelperInput.InvoiceAdjustmentId); 
            Invoice__c inv  = invMap.get(invAdj.invoice__c); 

            if (invAdjHelperInput.action == 'insert')
            {
                //determine which bucket to take from
                //deduct what you can from the bill
                //create an additional invoiceadjument for next month if necessary
                boolean createAnotherInvAdj = false;
                if (invAdj.MCS_Bucket__c == 'MCS_new')
                {
                                system.debug(invAdj.amount__c);
                system.debug(inv.Gross_Monthly_Total_New__c);

                    if (inv.Gross_Monthly_Total_New__c >= -1*invAdj.amount__c)
                    {
                        invadj.applied__c = invadj.amount__c;
                    }
                    else
                    {
                        invadj.applied__c = -1*inv.Gross_Monthly_Total_New__c;
                        createAnotherInvAdj = true;
                    }
                    inv.Gross_Monthly_Total_New__c = inv.Gross_Monthly_Total_New__c + invadj.applied__c;
                }
                else if (invAdj.MCS_Bucket__c == 'MCS_existing')
                {
                    if (inv.Gross_Monthly_Total_Existing__c >= -1*invAdj.amount__c)
                    {
                        invadj.applied__c = invadj.amount__c;
                    }
                    else
                    {
                        invadj.applied__c = -1*inv.Gross_Monthly_Total_Existing__c;
                        createAnotherInvAdj = true;
                    }
                    inv.Gross_Monthly_Total_Existing__c = inv.Gross_Monthly_Total_Existing__c + invadj.applied__c;
                }
                else if (invAdj.MCS_Bucket__c == 'Member Services')
                {
                    if (inv.Member_Services_Revenue__c >= -1*invAdj.amount__c)
                    {
                        invadj.applied__c = invadj.amount__c;
                    }
                    else
                    {
                        invadj.applied__c = -1*inv.Member_Services_Revenue__c;
                        createAnotherInvAdj = true;
                    }
                    inv.Member_Services_Revenue__c = inv.Member_Services_Revenue__c + invadj.applied__c;
                }
                else if (invAdj.MCS_Bucket__c == 'LibertyRx')
                {
                    if (inv.Liberty_Rx_Total__c >= -1*invAdj.amount__c)
                    {
                        invadj.applied__c = invadj.amount__c;
                    }
                    else
                    {
                        invadj.applied__c = -1*inv.Liberty_Rx_Total__c;
                        createAnotherInvAdj = true;
                    }
                    inv.Liberty_Rx_Total__c = inv.Liberty_Rx_Total__c + invadj.applied__c;
                }
                else if (invAdj.MCS_Bucket__c == '2nd.MD')  
                {
                    if (inv.X2nd_MD_Total__c >= -1*invAdj.amount__c)
                    {
                        invadj.applied__c = invadj.amount__c;
                    }
                    else
                    {
                        invadj.applied__c = -1*inv.X2nd_MD_Total__c;
                        createAnotherInvAdj = true;
                    }
                    inv.X2nd_MD_Total__c = inv.X2nd_MD_Total__c + invadj.applied__c;
                }

                //check the createAnotherInvAdj to see if we need to create another adjustment for next month
                if (createAnotherInvAdj == true)
                {
                    Invoice_Adjustment__c newInvAdj = new Invoice_Adjustment__c ();
                    newInvAdj.Prior_Invoice_Adjustment__c= invAdj.id;
                    newInvAdj.amount__c = invAdj.amount__c - invAdj.applied__c;
                    newInvAdj.Description_of_Change__c = 'Remaining credit from prior invoice - ' + invAdj.Description_of_Change__c;
                    newInvAdj.Employer_Account__c = invAdj.Employer_Account__c;
                    newInvAdj.Invoice_Period__c = invAdj.Invoice_Period__c.AddMonths(1);
                    newInvAdj.MCS_Bucket__c = invAdj.MCS_Bucket__c;

                    //insert newInvAdj;
                    //insert/update will happen at the end
                    newInvAdjList.add(newInvAdj);
                }

                //create new invoice line item
                Invoice_Line_Item__c invL = new Invoice_Line_Item__c();
                invL.account__c = inv.Account__c;
                invL.Description__c = invAdj.Description_of_Change__c;
                invL.invoice__c = invadj.invoice__c;
                invL.price__c = invadj.applied__c;
                invL.Quantity__c = 1;
                invL.QBO_Product__c = adjustmentProduct.id;
                invL.Product__c = adjustmentProduct.id;
                invL.Product_Code__c = 'Adjustment';

                newInvLineItemsMap.put(invAdj.id, invL);
                invadj.Reprocessed__c = FALSE;
             }

            else //Delete scenario
            {
                if (invAdj.Applied__c != null)
                {
                    if (invAdj.MCS_Bucket__c == 'MCS_new')
                    {
                        inv.Gross_Monthly_Total_New__c = inv.Gross_Monthly_Total_New__c - invadj.applied__c;
                    }
                    else if (invAdj.MCS_Bucket__c == 'MCS_existing')
                    {
                        inv.Gross_Monthly_Total_Existing__c = inv.Gross_Monthly_Total_Existing__c - invadj.applied__c;
                    }
                    else if (invAdj.MCS_Bucket__c == 'Member Services')
                    {
                        inv.Member_Services_Revenue__c = inv.Member_Services_Revenue__c - invadj.applied__c;
                    }
                    else if (invAdj.MCS_Bucket__c == 'LibertyRx')
                    {
                        inv.Liberty_Rx_Total__c = inv.Liberty_Rx_Total__c - invadj.applied__c;
                    }
                    //else if (invAdj.MCS_Bucket__c == '2nd.MD')  
                    //{
                    //        inv.X2nd_MD_Total__c = inv.X2nd_MD_Total__c - invadj.applied__c;
                    //}
                }
                    
             }
        }

        //DML outside of the for loop
        //insert all the new invoice line items
        insert newInvLineItemsMap.values();

        //update the invoice adjustments with the invoice line item ids
        for (id invAdjID : newInvLineItemsMap.keyset())
        {
            Invoice_Adjustment__c invAdj = invAdjMap.get(invAdjID);
            Invoice_Line_Item__c invL = newInvLineItemsMap.get(invAdjID);
            invAdj.Invoice_Line_Item__c = invl.id;
        }

        //update the invoice adjustments
        update invAdjMap.values();

        //insert any newly created invoice adjustments
        insert newInvAdjList;

        //update the invoices
        for(String key:invMap.keySet()){ System.debug(invMap.get(key));}
        
        update invMap.values();

        //delete the invoice adjustments and associated line items  
        if (invLineItemDeleteList.size() > 0)
        {
            delete invLineItemDeleteList;
        }
            
        delete invAdjDeleteList;
    }

     WebService static void DeleteButtonCall(ID InvAdjID)
    {
        List<InvoiceAdjustmentHelperInput> values = new List<InvoiceAdjustmentHelperInput>();
        InvoiceAdjustmentHelperInput invAdjHelpInput = new InvoiceAdjustmentHelperInput();
        invAdjHelpInput.InvoiceAdjustmentId = InvAdjID;
        invAdjHelpInput.action = 'delete';

        values.add(invAdjHelpInput);

        ProcessInvoiceAdjustment(values);


    }
    public class InvoiceAdjustmentHelperInput{
        @InvocableVariable
        public Id InvoiceAdjustmentId;
        
        @InvocableVariable
        public string action;
    }
}