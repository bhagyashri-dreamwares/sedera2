/**
*@Author       : Dreamwares
*@Created Date : 19/07/2018
*@Purpose      : Handler for filtering records should be processed in the batch
*/
public with sharing class QBAccountSyncPushBatchHelper{
    
    /* 
    @Purpose : Filter account Ids for parent and child records..
    @Parameter : Set<Id> inputIds
    @returns   : Map<String,Set<Id>>
    */
    public String currentQuickbookOrg = '';
    public QBAccountSyncPushBatchHelper(String currentQBOrg){
        currentQuickbookOrg = currentQBOrg;
        
    }
    public Map<String,Set<Id>> getFilterIdsToSync(Set<Id> inputIds, String currentQBOrg){
        List<Account> accounts = new List<Account>();
        
        Map<String,Set<Id>> regNextMap = new Map<String,Set<Id>>();
        regNextMap.put('Regular', new Set<Id>());
        regNextMap.put('Next', new Set<Id>());
        if(currentQBOrg == 'Quickbooks'){
            accounts = [ SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, Qb_Sync_Status__c, Parent.Qb_Sync_Status__c, 
                        Parent.QuickBooks_Customer_ID__c, Parent.Sync_to_QBO__c 
                    FROM Account 
                    WHERE Id IN:inputIds ];
        }
        
        if(currentQBOrg == 'Quickbooks2'){
            accounts = [ SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId,  QB_Sync_Status2__c, Parent.QB_Sync_Status2__c,
                        Parent.QuickBooks_Customer_ID2__c, Parent.Sync_to_QBO__c 
                    FROM Account 
                    WHERE Id IN:inputIds ];
        }       
        
        regNextMap = getFilteredMap(regNextMap, accounts, currentQBOrg);  
        return regNextMap;
    }
    
    /* 
    @Purpose : Filter account Ids for parent and child records..
    @Parameter : Map<String,Set<Id>> regNextMap, List<Account> accounts
    @returns   : Map<String,Set<Id>>
    */
    private Map<String,Set<Id>> getFilteredMap(Map<String,Set<Id>> regNextMap, List<Account> accounts, String currentQBOrgName){
        
        if(!accounts.isEmpty()){
            for(Account accountRecord : accounts){
                if(currentQBOrgName == 'Quickbooks'){
                    if(accountRecord.ParentId != NULL && String.isNotBlank(accountRecord.Parent.QuickBooks_Customer_ID__c)){
                        regNextMap.get('Regular').add(accountRecord.Id);
                    }
                    else if(accountRecord.ParentId != NULL && String.isBlank(accountRecord.Parent.QuickBooks_Customer_ID__c)){
                        if(accountRecord.Parent.Sync_to_QBO__c && (accountRecord.Parent.Qb_Sync_Status__c == NULL  || !accountRecord.Parent.Qb_Sync_Status__c.equalsIgnoreCase('Failed'))){ regNextMap.get('Next').add(accountRecord.Id); regNextMap.get('Regular').add(accountRecord.ParentId);
                                                                   }else{regNextMap.get('Regular').add(accountRecord.Id);
                                                                   }                    
                    }else{
                        regNextMap.get('Regular').add(accountRecord.Id);
                    }
                }
                
                if(currentQBOrgName == 'Quickbooks2'){
                    if(accountRecord.ParentId != NULL && String.isNotBlank(accountRecord.Parent.QuickBooks_Customer_ID2__c)){
                        regNextMap.get('Regular').add(accountRecord.Id);
                    }
                    else if(accountRecord.ParentId != NULL && String.isBlank(accountRecord.Parent.QuickBooks_Customer_ID2__c)){
                        if(accountRecord.Parent.Sync_to_QBO__c && (accountRecord.Parent.Qb_Sync_Status2__c == NULL  || !accountRecord.Parent.Qb_Sync_Status2__c.equalsIgnoreCase('Failed'))){ regNextMap.get('Next').add(accountRecord.Id);regNextMap.get('Regular').add(accountRecord.ParentId);
                                                                   }else{ regNextMap.get('Regular').add(accountRecord.Id);
                                                                   }                    
                    }else{
                        regNextMap.get('Regular').add(accountRecord.Id);
                    }
                }
                
            }
        }
        return regNextMap;                                                                                   
    }  
}