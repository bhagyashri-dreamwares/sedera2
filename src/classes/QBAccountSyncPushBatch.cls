/**
*@Author       : Dreamwares
*@Created Date : 19/07/2018 
*@Purpose      : Batch to get customers from Quick Book
*/
public with sharing class QBAccountSyncPushBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    public Integer batchcount = 0;
    public Set<ID> setAccountIds;
    public Set<ID> regularSyncIds;
    public Set<ID> nextSyncIds;
    List<QBBatchResponseObject> reponseRecordsList;
    DateTime responseTime;
    QBAccountSyncPush customersSyncPush;
    public String currentQuickbookOrg = '';
    
    public QBAccountSyncPushBatch(Set<ID> setAccountIds,String quickbookOrg){
        currentQuickbookOrg = quickbookOrg;
        System.debug('currentQuickbookOrg :: '+currentQuickbookOrg);
        reponseRecordsList = new List<QBBatchResponseObject>();
        responseTime = System.now();
        customersSyncPush = new QBAccountSyncPush(currentQuickbookOrg);
        
        //this.setAccountIds = setAccountIds;
        System.debug('Account Ids' + setAccountIds );
        if(setAccountIds != NULL && !setAccountIds.isEmpty()){
            Map<String,Set<Id>> regularAndNextSyncIdMap = new Map<String,Set<Id>>();
            QBAccountSyncPushBatchHelper qbAccountBatchHelper = new QBAccountSyncPushBatchHelper(currentQuickbookOrg);
            regularAndNextSyncIdMap = qbAccountBatchHelper.getFilterIdsToSync(setAccountIds,currentQuickbookOrg);
            
            if(regularAndNextSyncIdMap.containsKey('Regular')){
                regularSyncIds = regularAndNextSyncIdMap.get('Regular');
            }
            if(regularAndNextSyncIdMap.containsKey('Next')){
                nextSyncIds = regularAndNextSyncIdMap.get('Next');
            }
            System.debug('Map :: ' + regularAndNextSyncIdMap);
        }    
    }
    
    public Database.QueryLocator start(Database.BatchableContext info){
        String query;
       // if(!test.isrunningtest()){
            query = 'SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, Parent.Qb_Sync_Status__c,Parent.QuickBooks_Customer_ID__c, '+
                'BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, '+
                'BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, ShippingStreet, ShippingCity, ShippingState, '+
                'ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, '+
                'ShippingGeocodeAccuracy, Phone, Fax, AccountNumber, Website, PhotoUrl, Industry, AnnualRevenue,'+
                ' Description,  CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp,'+
                'QuickBooks_Customer_ID__c, QB_Sync_Token2__c, QB_Sync_Status2__c, QuickBooks_Customer_ID2__c, '+
                'Sync_to_QBO__c,QB_Sync_Token__c '+
                'FROM Account WHERE Sync_to_QBO__c = TRUE AND Id IN:regularSyncIds';
       /* }else{
            query = 'SELECT Id, IsDeleted, MasterRecordId, Name, Type, ParentId, Parent.Qb_Sync_Status__c,Parent.QuickBooks_Customer_ID__c, '+
                'BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, '+
                'BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, ShippingStreet, ShippingCity, ShippingState, '+
                'ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, '+
                'ShippingGeocodeAccuracy, Phone, Fax, AccountNumber, Website, PhotoUrl, Industry, AnnualRevenue,'+
                ' Description,  CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp,'+
                'QuickBooks_Customer_ID__c, '+
                'Sync_to_QBO__c,QB_Sync_Token__c '+
                'FROM Account WHERE Sync_to_QBO__c = TRUE AND Id IN:regularSyncIds';
        }*/
        
        return Database.getQueryLocator(query);                         
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> accountList){        
        
        if(accountList.size() > 0){            
            //QBAccountSyncPush customersSyncPush = new QBAccountSyncPush();
            customersSyncPush = new QBAccountSyncPush(currentQuickbookOrg);
            List<Account> accounts = accountList;
            // create request parameter
            BRequestParams reqParams = new BRequestParams();
            reqParams.objectName = 'Customer'; 
            reqParams.startIndex = 0;
            reqParams.qbIds = new List<String>();
            
            QBCustomerPull custPull = new QBCustomerPull();
            BAPIResponseWrapper responseOfGetOpt;
            // populate Ids
            reqParams.qbIds = getListOfIds(accounts,currentQuickbookOrg);
            
            if(reqParams.qbIds != null && !reqParams.qbIds.isEmpty()){
                // fetch QB sync tokens
                if(currentQuickbookOrg != NULL){
                    responseOfGetOpt = custPull.doGet(reqParams,currentQuickbookOrg);
                }
                
                
                QBCustomerParser parser = new QBCustomerParser();
                
                List<Account> parsedAccounts = (List<Account>)parser.parseToObjectList((List<Object>)responseOfGetOpt.records);
                // create map of id to sync token
                Map<String,String> mapOfSyncToken = getMapOfSyncToken(parsedAccounts,currentQuickbookOrg);
                
                // modify account list
                for(Account accRec : accounts){
                    if(currentQuickbookOrg == 'Quickbooks'){
                        if(String.isNotBlank(accRec.QuickBooks_Customer_ID__c) && mapOfSyncToken.containsKey(accRec.QuickBooks_Customer_ID__c)){
                            accRec.QB_Sync_Token__c = mapOfSyncToken.get(accRec.QuickBooks_Customer_ID__c);
                        }
                    }
                    if(currentQuickbookOrg == 'Quickbooks2'){
                        if(String.isNotBlank(accRec.QuickBooks_Customer_ID2__c) && mapOfSyncToken.containsKey(accRec.QuickBooks_Customer_ID2__c)){
                            accRec.QB_Sync_Token2__c = mapOfSyncToken.get(accRec.QuickBooks_Customer_ID2__c);
                        }
                    }                    
                }
            }
            
            
            //List<QBBatchResponseObject> reponseRecordsList = new List<QBBatchResponseObject>();       
            responseTime = System.now();
            
            BAPIResponseWrapper response = new BAPIResponseWrapper();System.debug('doPost:: ' +currentQuickbookOrg);
            response = customersSyncPush.doPost(accounts,currentQuickbookOrg);
            
            if(response != null && response.records != null ){
                reponseRecordsList.addAll((List<QBBatchResponseObject>)response.records);               
            }   
            //customersSyncPush.updateRecords(reponseRecordsList, responseTime); 
        }        
    }
    
    public void finish(Database.BatchableContext BC){
        customersSyncPush.updateRecords(reponseRecordsList, responseTime, currentQuickbookOrg);
        System.debug('In Batch currentQuickbookOrg ::: '+currentQuickbookOrg);
        if(nextSyncIds != NULL && !nextSyncIds.isEmpty()){
            System.debug('In Batch currentQuickbookOrg ::: '+currentQuickbookOrg);
            QBAccountSyncPushBatch pushBatch = new QBAccountSyncPushBatch(nextSyncIds,currentQuickbookOrg);
            Database.executeBatch(pushBatch, 30);
        }
        /*else{
            if(batchcount < 2){
                batchcount++;
                QBAccountSyncPushBatch pushBatch = new QBAccountSyncPushBatch(setAccountIds,'Quickbooks');
                Database.executeBatch(pushBatch, 30);
            }            
        }*/
    }
    
    public List<String> getListOfIds(List<Account> accounts, String currentQBOrg){
        List<String> idsList = new List<String>();
        if(accounts != null && !accounts.isEmpty()){
            for(Account accountRec : accounts){
                if(currentQBOrg == 'Quickbooks'){
                    if(String.isNotBlank(accountRec.QuickBooks_Customer_ID__c)){
                        idsList.add(accountRec.QuickBooks_Customer_ID__c);
                    }
                }
                
                if(currentQBOrg == 'Quickbooks2'){
                    if(String.isNotBlank(accountRec.QuickBooks_Customer_ID2__c)){
                        idsList.add(accountRec.QuickBooks_Customer_ID2__c);
                    }
                }                
            }
        }
        return idsList;
    }
    
    public Map<String,String> getMapOfSyncToken(List<Account> parsedAccounts, String currentQuickbookOrg){
        System.debug('parsedAccounts ::: '+parsedAccounts);
        Map<String,String> mapOfSyncToken = new Map<String,String>();
        if(parsedAccounts != NULL){
            for(Account accRec : parsedAccounts){
                if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks' ){
                    mapOfSyncToken.put(accRec.QuickBooks_Customer_ID__c,accRec.QB_Sync_Token__c);
                } 
                
                if(String.isNotBlank(currentQuickBookOrg) && currentQuickBookOrg == 'Quickbooks2' ){
                    mapOfSyncToken.put(accRec.QuickBooks_Customer_ID2__c,accRec.QB_Sync_Token2__c);                    
                }
                
            }
        }        
        System.debug('mapOfSyncToken ::: '+mapOfSyncToken);
        return mapOfSyncToken; 
    }
}