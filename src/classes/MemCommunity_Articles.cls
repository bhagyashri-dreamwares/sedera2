/*
   Test Class : MemCommunity_Articles_Test
*/
public without sharing class MemCommunity_Articles {

    @AuraEnabled
    Public List <Employer__kav> articles;

    @AuraEnabled
    Public Boolean hasArticles;

    @AuraEnabled
    public integer offst;

    @AuraEnabled
    public integer total;

    @AuraEnabled
    public boolean noPrev;

    @AuraEnabled
    public boolean noNext;


    private static integer pagesize = Integer.valueOf(system.label.MemCommunity_No_of_Articles);
    
    private static integer offset;
    
    @AuraEnabled
    public static Employer__kav getArticle(Id id) {
        Employer__kav art;
         system.debug('mmmm '+id);
        if (test.isrunningtest() || id!=null)
            art = [select id, title, Urlname, DocumentId__c, Summary, publishstatus from Employer__kav where id =: id AND language = 'en_US'
                LIMIT 1
            ];
        else if(id!=null)
            art = [select id, title, Urlname, DocumentId__c, Summary from Employer__kav where id =: id and publishStatus = 'Online'
                AND language = 'en_US'
                LIMIT 1
            ];

        
        return art;
    }
    @AuraEnabled
    public static MemCommunity_Articles getArticleandAttachment(Id id, boolean next, boolean prev, decimal off) {
        system.debug('mmmm '+id);
        if (test.isrunningtest())
            pagesize = 1;
        offset = (integer) off;

        list <id> listofentityids = new list <id> ();
        for (topicAssignment ta: [select entityid from topicAssignment where topicid =: id limit 1000]) {
            listofentityids.add(ta.entityid);
        }
        
        list <Employer__kav> empKavs=new list<Employer__kav>(); 
        list <Employer__kav> empKavsList = [select id, title, Urlname, DocumentId__c from Employer__kav where id in: listofentityids and publishStatus = 'Online'
            AND language = 'en_US'
        ];

        
        if (next == false && prev == false) {
            empKavs = [select id, title, Urlname, DocumentId__c from Employer__kav where id in: listofentityids and publishStatus = 'Online'
                AND language = 'en_US'
                LIMIT: pagesize OFFSET: offset
            ];

        } else if (next == true && (offset + pagesize) <= empKavsList.size()) {
            offset = offset + pagesize;
            empKavs = [select id, title, Urlname, DocumentId__c from Employer__kav where id in: listofentityids and publishStatus = 'Online'
                AND language = 'en_US'
                LIMIT: pagesize OFFSET: offset
            ];


        } else if (prev == true && offset> 0) {
            offset = offset - pagesize;
            empKavs = [select id, title, Urlname, DocumentId__c from Employer__kav where id in: listofentityids and publishStatus = 'Online'
                AND language = 'en_US'
                LIMIT: pagesize OFFSET: offset
            ];


        }

        MemCommunity_Articles art = new MemCommunity_Articles();

        if (empKavs!=null && empKavs.size()> 0) {
            art.offst = offset;
            art.noNext = hasnxt(offset,empKavsList.size(), pagesize);
            art.articles = empKavs;
            art.hasarticles = true;
            art.noPrev= hasprev(offset);
        } else {
            art.articles = empKavs;
            art.Hasarticles = false;
            art.offst = offset;
            art.noPrev= hasprev(offset);
            art.noNext = hasnxt(offset, 0, pagesize);
        }
        return art;

    }

   
    private static boolean hasprev(integer off) {
        if (off> 0)
            return false;
        return true;
    }
    
    
    private static boolean hasnxt(integer off, integer li, integer ps) {
        if (off + ps <li)
            return false;
        return true;
    }
}