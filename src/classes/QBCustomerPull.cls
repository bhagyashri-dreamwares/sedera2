/**
*@Author       : Dreamwares
*@Created Date : 19/07/2018
*@Purpose      : Batch to get customers from Quick Book
*/

public without sharing class QBCustomerPull extends AQBQueryObject {
    

   
    Map<String, String> qbIdToRecordId = new Map<String, String>();
 
    
    public QBCustomerPull() {
       
        parserImpl = new QBCustomerParser();
    }
    
    public override List<Object> getObjectRecords(QBQueryResponseWrapper responseDTO) {
        return responseDTO.QueryResponse.Customer;
    }
    
    public override void save(List<Object> records, DateTime syncTime) {
        
        //QuickBookDebugServices.trackSystemDebug('## records from Customer Pull :'+JSON.serialize(records));
       
    }
    
    
}