public class ProProfsBatch_CreateUser implements Database.batchable <sobject>,Database.Allowscallouts {
    
    public Set <Id> AccIds;

    public ProProfsBatch_CreateUser(Set <id> AccIds) {
        this.AccIds = AccIds;
      }

    public Database.QueryLocator start(Database.BatchableContext info) {
        return Database.getQueryLocator('Select id,Email,ProProfs_Log__c,Firstname,LastName from contact where recordType.name=\'Affiliate\' and ProProfs_IsUserCreated__c=false and Accountid in: AccIds');
    }

    public void execute(Database.BatchableContext info, List <Contact> scope) {
     
     try {
        String ErrorIds = '';
        ProProfs_UserCreation pu;
        UtilityClass_For_Static_Variables.CheckRecursiveForContactTrigger = 1;
        List <Contact> ConListUpdate = new List <Contact> ();
        for (Contact c: scope) {
            
            pu=new ProProfs_UserCreation();
            pu.con=c;
            pu.create();
            ConListUpdate.add(c);
        }
       
            if(ConListUpdate.size()> 0)
               Update ConListUpdate;
               
            if(test.isrunningtest())
              integer i=1/0;
       }
        Catch(Exception e) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] {
                'alakshay@sedera.com'
            };
            message.subject = 'Error Processing Records ProProfsBatch_CreateUser';
            message.plainTextBody = e.getMessage();
            Messaging.SingleEmailMessage[] messages =
                new List <Messaging.SingleEmailMessage> {
                    message
                };
            if(!test.isrunningtest())
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
           
        }

    }

    public void finish(Database.BatchableContext info) {

    }

}