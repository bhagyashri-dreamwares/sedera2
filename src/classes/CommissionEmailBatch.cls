/**
* @Purpose: Batch class to send emails related to commissions using Paubox API 
*/
global class CommissionEmailBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful
{
     
     global final ID commReportID;
     global final List<Paubox_Message__c> pauboxMessageList;
     
     public CommissionEmailBatch (ID incomingID)
     {
         this.pauboxMessageList = new List<Paubox_Message__c>();
         this.commReportID = incomingID;
     }
         
     public Database.QueryLocator start(Database.BatchableContext info)
     {    
         return Database.getQueryLocator([SELECT ID, 
                                          Affiliate__c,
                                          Affiliate__r.Name,
                                          Affiliate__r.Id,
                                          Affiliate__r.Receive_Affiliate_Fee_emails__c, 
                                          Commission_Report__r.Month__c,
                                          Commission_Report__r.Year__c,
                                          Report_Link__c,
                                          Member_Count__c,
                                          Total_Commission__c
                                          FROM Commission_Report_Item__c 
                                          WHERE Commission_Report__c = :commReportID
                                         ]); 
     }
     
	public void execute(Database.BatchableContext bc, List<Commission_Report_Item__c> criList)
	{
         for(Commission_Report_Item__c cri :criList)
         {
             Paubox_Message__c pauboxMsg = PauBoxEmailHelper.sendCommissionEmail('Commission', cri);
             
             system.debug(pauboxMsg);
             
             if(pauboxMsg != null){
                 pauboxMessageList.add(pauboxMsg);
             } 
         }                  
     }
     
     public void finish(Database.BatchableContext BC){
     
         if(!pauboxMessageList.isEmpty()){
           
           System.debug('pauboxMessageList :::'+pauboxMessageList);
           PauBoxEmailHelper.savepauboxMessageRecord(pauboxMessageList);
         }
         
         /*QBInvoicePushBatch qbInvoiceBatch = new QBInvoicePushBatch(invoiceIdSet);
         Database.executeBatch(qbInvoiceBatch,30);*/
     }
}