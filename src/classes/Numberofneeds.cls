public class Numberofneeds
{
public static map<id,Integer> MapOfCountOfAccountAndContactId=new map<id,integer>();
public static set<id> SetofAccountids=new set<id>();


public static Integer forContactAccountAddOne(Id conId,integer NoOfContacts,id accId,integer NoOfAccounts)
   {
      if(MapOfCountOfAccountAndContactId.containskey(conId))
      {
        MapOfCountOfAccountAndContactId.put(conId,MapOfCountOfAccountAndContactId.get(conId)+1);
        MapOfCountOfAccountAndContactId.put(accId,MapOfCountOfAccountAndContactId.get(accId)+1);
        return MapOfCountOfAccountAndContactId.get(conId);
      }

      else
      {
        MapOfCountOfAccountAndContactId.put(conId,(integer)(NoOfContacts+1));
         if(MapOfCountOfAccountAndContactId.containskey(accId))
         {
          MapOfCountOfAccountAndContactId.put(accId,(integer)(MapOfCountOfAccountAndContactId.get(accId)+1));
         }
         else
         {
          MapOfCountOfAccountAndContactId.put(accId,(integer)(NoOfAccounts+1));
         }

        return MapOfCountOfAccountAndContactId.get(conId);
      }
   }


   public static Integer forContactAccountMinusOne(Id conId,integer NoOfContacts,id accId,integer NoOfAccounts)
   {
     if(MapOfCountOfAccountAndContactId.containskey(conId))
      {
       MapOfCountOfAccountAndContactId.put(conId,(integer)(MapOfCountOfAccountAndContactId.get(conId)-1));
       MapOfCountOfAccountAndContactId.put(accId,(integer)(MapOfCountOfAccountAndContactId.get(accId)-1));
       return MapOfCountOfAccountAndContactId.get(conId);
      }
     else
      {
       MapOfCountOfAccountAndContactId.put(conId,(integer)(NoOfContacts-1));
       MapOfCountOfAccountAndContactId.put(accId,(integer)(NoOfAccounts-1));
       return MapOfCountOfAccountAndContactId.get(conId);
      }
   }

   public static Integer Account5Contact3(id id)
   {
    if(MapOfCountOfAccountAndContactId.containskey(id))
     {
      MapOfCountOfAccountAndContactId.put(id,MapOfCountOfAccountAndContactId.get(id)+1);
      return MapOfCountOfAccountAndContactId.get(id);
     }
    else
     {
      MapOfCountOfAccountAndContactId.put(id,1);
      return MapOfCountOfAccountAndContactId.get(id);
     }

   }

   public static void addaccountid(id accId)
   { 
    SetofAccountids.add(accId);
   }

   public static Integer forAccountContactAddOne(Id accId,integer NoOfAccounts,id conId,integer NoOfContacts)
   {
    if(MapOfCountOfAccountAndContactId.containskey(accId))
    {
     MapOfCountOfAccountAndContactId.put(accId,(integer)(MapOfCountOfAccountAndContactId.get(accId)+1));
    }
    else
    {
     MapOfCountOfAccountAndContactId.put(accId,(integer)(NoOfAccounts+1));
    }
    
    
    if(MapOfCountOfAccountAndContactId.containskey(conId))
    {
     MapOfCountOfAccountAndContactId.put(conId,(integer)(MapOfCountOfAccountAndContactId.get(conId)+1));
    }
    else
    {
     MapOfCountOfAccountAndContactId.put(conId,(integer)(NoOfContacts+1));
    }
   
    return MapOfCountOfAccountAndContactId.get(accId);
   }

   
   public static Void forAccountMinusOne(Id accId,integer NoOfAccounts)
   {
    if(MapOfCountOfAccountAndContactId.containskey(accId))
    {
     MapOfCountOfAccountAndContactId.put(accId,(integer)(MapOfCountOfAccountAndContactId.get(accId)-1));
    }
    else
    {
     MapOfCountOfAccountAndContactId.put(accId,(integer)(NoOfAccounts-1));
    }


   }
   
   Public static void clearStaticvariables()
   {
     MapOfCountOfAccountAndContactId.clear();
     SetofAccountids.clear();
   }
   
}