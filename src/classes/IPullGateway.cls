public interface IPullGateway {
    /*
     * - Get Records from Quick Books
     * - Process and parse them
     * - Save in salesforce
     */

    BAPIResponseWrapper doGet(BRequestParams requestParams,String currentQuickbookOrg);
    //List<SObject> processRecords(List<Object> records);
    void save(List<Object> records, DateTime syncTime);
}