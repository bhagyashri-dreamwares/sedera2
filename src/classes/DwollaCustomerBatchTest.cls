/* 
* @purpose : Test Method For DwollaCustomerBatch. 
*/
@isTest
public class DwollaCustomerBatchTest {
    /* 
	* @purpose : create Test Data.
	*/
    @TestSetup
    public static void setup(){
        Id recordTypeIdAccountEmp = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account accountRecord1 = new Account();
        accountRecord1.Name='Test Account';
        accountRecord1.Create_Dwolla_Customer__c=true;
        accountRecord1.Invoice_Email_Address__c='test@test.com';
        accountRecord1.Dwolla_ID__c='12345';
        accountRecord1.Enrollment_Date__c=system.today();
        accountRecord1.Member_Discount_Tier_Manual__c='T2';
        accountRecord1.Member_MEC_Product__c='test';
        accountRecord1.recordtypeid = recordTypeIdAccountEmp;
        insert accountRecord1;
        
        DwollaAPIConfiguration__c customSettingObj = new DwollaAPIConfiguration__c();
        customSettingObj.Name='test';
        customSettingObj.Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token';
        customSettingObj.Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com';
        customSettingObj.Access_Token__c='lLM6ZX3FYsDC1Z8t88rmX6d60l6dkkzC5gBbsiDMxJvxSnDbEz';
        customSettingObj.Access_Token_Expiry__c=system.today() + 600;
        customSettingObj.Is_Sandbox__c=true;
        customSettingObj.Redirect_URI__c='https://c.cs21.visual.force.com/apex/DwollaAuthorize';
        customSettingObj.Client_Key__c='UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp';
        customSettingObj.Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150';
        insert customSettingObj;
    }

    /* 
	* @purpose : Positive Test Method for dwollaCustomerBatch.
	*/
    
    public static testmethod void dwollaCustomerBatchPositiveTest(){
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(1));
        
        List<Account> accountList = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                                     Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                                     Member_MEC_Product__c
                                     FROM Account LIMIT 1];
        Test.startTest();
        DwollaCustomerBatch batchObj = new DwollaCustomerBatch(accountList);
        Id batchId = Database.executeBatch(batchObj);
        Test.stopTest();
    }

    /* 
	* @purpose : Negative Test Method for dwollaCustomerBatch.
	*/

    public static testmethod void dwollaCustomerBatchNegativeTest(){
        
        string retunString;
        Test.setMock(HttpCalloutMock.class, new MockDwollaCalloutHelperTest(0));
        
        List<Account> account = [SELECT Id, Name, Create_Dwolla_Customer__c, Invoice_Email_Address__c,
                           Dwolla_ID__c, Enrollment_Date__c, Member_Discount_Tier_Manual__c,  
                           Member_MEC_Product__c
                           FROM Account LIMIT 1];
       
       Test.startTest();
        DwollaCustomerBatch batchObj = new DwollaCustomerBatch(account);
        Id batchId = Database.executeBatch(batchObj);
        Test.stopTest();      
    }
}