/*
 *@Purpose    :
 *@Author     : Dreamwares
 *@Created Date : 19/07/2018
 */
public class QBBatchFault {
  public List<QBBatchError> Error;

  public QBBatchFault(){
    Error = new List<QBBatchError>();
  }

  public class QBBatchError{
    public String Message;
    public String Detail;
    public String code;
    public String element;
  }
}