/*
* @Purpose         : FundingSourceTriggerHandler Test class
* @author          : Navin
* @since           : 1 Oct 2018
* @date            : 1 Oct 2018 - Navin: created initial version
* @see      
*/

@isTest
public class FundingSourceTriggerHandlerTest {
    // Create testsetup records for Opportunity and Opportunity_Line__c
    @TestSetup
    private static void createTestRecord(){
        
        // Create Account record. Lookup field on Company
        Account accountrecord = new Account(Name = 'testAccound',RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId(),
                                            Dwolla_ID__c='74d9b82a-2a54-4160-9f86-c718b8155b34',
                                            Invoice_Email_Address__c = 'test@test.com');
        
        Insert accountrecord;
        
        List<Dwolla_Funding_Source__c> dwollaFundingSourceRecord = new List<Dwolla_Funding_Source__c>();
        
        for(Integer i=0;i<200;i++)
        {
            dwollaFundingSourceRecord.add(new Dwolla_Funding_Source__c( Active__c = true,
                                                                        Bank_Account_Number__c = '000000000',
                                                                        Dwolla_Funding_Source_Name__c = 'test name'+i,
                                                                        Employer_Account__c = accountRecord.Id,
                                                                        Funding_Account_Type__c = 'Savings',
                                                                        Routing_Number__c = 'Test0'+i,
                                                                        Sync_to_Dwolla__c = true,
                                                                        Type__c = 'Customer'
                                                                       ));
        }
        insert dwollaFundingSourceRecord;
        
        DwollaAPIConfiguration__c dwollaApiRecord = new DwollaAPIConfiguration__c(Access_Token__c = '5DeH2odJRoqJA21i6GH8BHeS0cEOJR0JaMgG3d32Aa6xeZ5MfV',
                                                                                  Access_Token_Expiry__c = system.today(), 
                                                                                  Authorization_URL__c='https://sandbox.dwolla.com/oauth/v2/token',
                                                                                  Client_Key__c = 'UBUbubikyDpv3ckSKwg2ux3PKV9M5mU0AOJTwATWKnoLyubrLp',
                                                                                  Is_Sandbox__c=true,
                                                                                  Redirect_URI__c = 'https://c.cs21.visual.force.com/apex/DwollaAuthorize',
                                                                                  Sandbox_Endpoint_Url__c='https://api-sandbox.dwolla.com',
                                                                                  Secret_Key__c='7cQqY5Zyq04EL8s484ZMKLnwAQ8oYY3YnyVid8qIiWqyOz5150');
        insert dwollaApiRecord;
        
    }
    
    
    // POsitive test case
    @isTest
    private static void positiveTest() {
       
        Test.startTest(); 
        List<Dwolla_Funding_Source__c> recordList = [ SELECT ID, Active__c, Bank_Account_Number__c,
                                                             Dwolla_Funding_Source_Name__c, Routing_Number__c,
                                                             Sync_to_Dwolla__c
                                                      FROM Dwolla_Funding_Source__c 
                                                      LIMIT 100];
        FundingSourceTriggerHandler.createFundindingSource(recordList);
        Test.stopTest();
    }
}