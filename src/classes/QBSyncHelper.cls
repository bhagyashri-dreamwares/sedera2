public with sharing class QBSyncHelper {
  public static DateTime dateFromString(String dateDtr){
        DateTime dateTimeInst;
        if(!String.isBlank(dateDtr) && !String.isEmpty(dateDtr)){      
      Pattern patternInst = Pattern.compile('^([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2})\\.[0-9]+([\\+\\-]{1})([0-9]{2}):([0-9]{2})$');
      Matcher matcherInst = patternInst.matcher(dateDtr);
      
      if(matcherInst.groupCount() == 9 && matcherInst.matches()){
        //System.debug(matcherInst.group(0));
        dateTimeInst = DateTime.newInstanceGmt( Integer.valueOf(matcherInst.group(1)),     // Year
                            Integer.valueOf(matcherInst.group(2)),     // Month
                            Integer.valueOf(matcherInst.group(3)),     // Day
                            Integer.valueOf(matcherInst.group(4)),     // Hour
                            Integer.valueOf(matcherInst.group(5)),     // Minutes
                            Integer.valueOf(matcherInst.group(6)));     // Seconds
        Integer timeZoneUnitMultiplier = -1;
        if(matcherInst.group(7).equals('-')){
          timeZoneUnitMultiplier = 1;
        }
        
        dateTimeInst = dateTimeInst.addHours(Integer.valueOf(matcherInst.group(8)) * timeZoneUnitMultiplier);
        dateTimeInst = dateTimeInst.addMinutes(Integer.valueOf(matcherInst.group(9)) * timeZoneUnitMultiplier);        
      }
    }
        return dateTimeInst;
    }
/*
    public static void updateResponseTime(String objectName, DateTime lastResponseTime){
        if(QB_Sync_Data__c.getAll().containsKey(objectName)){
            try{
                QB_Sync_Data__c qbSyncData = new QB_Sync_Data__c();
    
                qbSyncData = QB_Sync_Data__c.getAll().get(objectName);
                qbSyncData.Last_Sync_DateTime__c = lastResponseTime;
                update qbSyncData;
            }catch(Exception e){
                 //QuickbookDebugServices.trackException(e);
            }   
        }
        else{
            System.debug('Object not found in Sync management custom setting');
        }
    }

    public static DateTime getLastResponseTime(String objectName){
        DateTime lastResponseTime = null;

        if(QB_Sync_Data__c.getAll().containsKey(objectName)){
            QB_Sync_Data__c qbSyncData = new QB_Sync_Data__c();

            qbSyncData = QB_Sync_Data__c.getAll().get(objectName);
            lastResponseTime = qbSyncData.Last_Sync_DateTime__c;
            if(lastResponseTime == null){
                lastResponseTime = DateTime.now().addHours(-1);
            }
        }
        else{
            System.debug('Object not found in Sync management custom setting');
        }
        return lastResponseTime;
    }
    */
   /**
     * Purpose : Method to get the customerId to customer record Map 
     * Parameters : List of Customer Id(Account Id)
    */
    public Static Map<Id, Account> getRelatedCustomer(List<Id> customerIdList){
        Map<Id, Account> accountIdToRecordMap = new Map<Id, Account>();
        if( customerIdList != NULL ){
            try{
                for(Account account : [ SELECT Id,QuickBooks_Customer_ID__c, Sync_to_QBO__c 
                                        FROM Account
                                        WHERE Id IN : customerIdList
                                        LIMIT 50000 ])
                    accountIdToRecordMap.put(account.Id, account);                                
            }catch( Exception e ){
                //QuickbookDebugServices.trackException(e);
                System.debug('Exception occured while creating customer number to customer Id map.'+e);
            }       
        }
        return accountIdToRecordMap;
    }
    
        
    /**
     * Purpose : Method to generate Invoice Id to related Line Item List
     * Parameters : List of Invoice Ids  
    */    
    public static Map<Id,List<Invoice_Line_Item__c>> getInvoiceIdToLineItemsMap(List<Id> invIdList){
        Map<Id,List<Invoice_Line_Item__c>> invIdToLineItemMap = new Map<Id,List<Invoice_Line_Item__c>>();
        
        if( invIdList != NULL ){
            for( Invoice__c invoice : [ SELECT Id, 
                                            ( SELECT Id, Name
                                              FROM Invoice_Line_Items__r ) 
                                        FROM Invoice__c
                                        WHERE Id IN : invIdList 
                                        LIMIT 50000]){
                List<Invoice_Line_Item__c> lineItemList = new List<Invoice_Line_Item__c>();
                for( Invoice_Line_Item__c item : invoice.Invoice_Line_Items__r ){
                    //invIdToLineItemMap.put(invoice.Id, item); 
                    lineItemList.add(item);                    
                }
                invIdToLineItemMap.put(invoice.Id, lineItemList);
            }
        }
        return invIdToLineItemMap;
    }
    
    /**
     * Purpose : Method to get the QB customer to SF customer Map 
     * Parameters : List of Customer number(from QB)
    */
    public static Map<String, Id> getCustomerMap( List<String> customerNoList ){
        Map<String, Id> customerNoToCustomerIdMap = new Map<String, Id>();
        if ( customerNoList != NULL ){
            try{
                for(Account account : [ SELECT Id, QuickBooks_Customer_ID__c, Sync_to_QBO__c 
                                        FROM Account
                                        WHERE QuickBooks_Customer_ID__c IN : customerNoList
                                        LIMIT 50000 ])
                    customerNoToCustomerIdMap.put(account.QuickBooks_Customer_ID__c, account.Id);                                
            }catch( Exception e ){
               // QuickbookDebugServices.trackException(e);
                System.debug('Exception occured while creating customer number to customer Id map.'+e);
            }                                        
        }
        return customerNoToCustomerIdMap;
    }
    
   public static Map<String, Product2> getItemIdToItemMap(List<String> itemIdList){
        Map<String, Product2> itemIdToItem = new Map<String, Product2>();

        if ( itemIdList != NULL && itemIdList.size() > 0 ){
            try{
                for(Product2 item : [ SELECT Id, Name
                                        FROM Product2
                                        WHERE Id IN : itemIdList
                                        LIMIT 1000 ])
                    itemIdToItem.put(item.Id, item);                                
            }catch( Exception e ){
               // QuickbookDebugServices.trackException(e);
                System.debug('Exception occured while creating customer number to customer Id map.'+e);
            }                                        
        }
        return itemIdToItem;
    }
    
    public static Map<String, Id> getQBItemIdToItemIdMap(List<String> qbItemIdList){
    Map<String, Id> itemIdExternalIdMap = new Map<String, Id>();

        if ( qbItemIdList != NULL && qbItemIdList.size() > 0 ){
            try{
                for(Product2 item : [ SELECT Id FROM Product2   LIMIT 1000 ]){
                    system.debug('Test');
                }
                  //  itemIdExternalIdMap.put(item.QB_Product_Id__c, item.Id);                                
            }catch( Exception e ){
                System.debug('Exception occured while creating customer number to customer Id map.'+e);
            }                                        
        }
        return itemIdExternalIdMap;
    }
}