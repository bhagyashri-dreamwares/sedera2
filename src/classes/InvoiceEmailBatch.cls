/**
* @Purpose: Batch class to send email related to invoice using Paubox API 
*/
global class InvoiceEmailBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
     
     global final Set<Id> invoiceIdSet = new Set<Id>();
     global final List<Paubox_Message__c> pauboxMessageList;
     
     public InvoiceEmailBatch (Set<Id> invoiceIdSet){
     
         this.pauboxMessageList = new List<Paubox_Message__c>();
         this.invoiceIdSet.addAll(invoiceIdSet);
     }
         
     public Database.QueryLocator start(Database.BatchableContext info){
         
         String query = 'SELECT Id, Name, Account__c, Account__r.Invoice_Email_Address__c, Account__r.Name, Account__r.RecordType.Name '+
                        'FROM Invoice__c '+
                        'WHERE Id IN :invoiceIdSet AND Account__c != null AND Account__r.RecordType.Name = \'Employer\' '+
                        'AND Account__r.Invoice_Email_Address__c != null '+
                        'Order By Account__c';
         
         return Database.getQueryLocator(query); 
     }
     
     public void execute(Database.BatchableContext bc, List<Invoice__c> invoiceList){
         
         //template is based off the date
        integer dayOfMonth = Integer.valueof(datetime.now().day());
        string templateTypeName;
        string activeTemplateName;
        if (dayOfmonth == 1){
            templateTypeName = 'Invoice Final';
        }
        else if (dayOfMonth == 21){
            templateTypeName = 'Invoice Draft';
        }
        else{
            templateTypeName = 'Invoice Update';  
        }
        
        //query the ActiveTemplates custom setting to get the activeTemplateName for that type
        
        activeTemplateName = [select active_template_name__c from ActiveTemplates__c where name = :templateTypeName].active_template_name__c ;

         for(Invoice__c invoice :invoiceList){
             
             system.debug('Invoice template ' + activeTemplateName + ' for invoice ' + invoice.id);
             
             Paubox_Message__c pauboxMsg = PauBoxEmailHelper.sendInvoiceEmail(activeTemplateName, invoice);
             
             system.debug(pauboxMsg);
             
             if(pauboxMsg != null){
                 pauboxMessageList.add(pauboxMsg);
             } 
         }                  
     }
     
     public void finish(Database.BatchableContext BC){
     
         if(!pauboxMessageList.isEmpty()){
           
           System.debug('pauboxMessageList :::'+pauboxMessageList);
           PauBoxEmailHelper.savepauboxMessageRecord(pauboxMessageList);
         }
         
         /*QBInvoicePushBatch qbInvoiceBatch = new QBInvoicePushBatch(invoiceIdSet);
         Database.executeBatch(qbInvoiceBatch,30);*/
     }
}