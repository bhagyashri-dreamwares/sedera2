/**
*@Purpose : Helper class to create subsequent transfers and account transfers once the subsequent transfer is successful
*@Date : 12/28/2018
*/
public without sharing class DwollaTransferHelper {

    /**
    *@Purpose : This method will create the second transfer (balance to sedera) after the initial transfer (customer to balance) is completed
    */
    public static void createSubsequentTransfer(Dwolla_Transfer__c initialTransfer){
        try{
        
            //PauboxEmailHelper.SendTransferCreatedEmail(transfer);
            FSMLogger.LogInformationalMessage('in createSubsequentTransfer');
            //ID accountID = initialTransfer.invoice__r.account__c;
            FSMLogger.LogInformationalMessage('InitialTransfer.Invoice__c - ' + initialtransfer.invoice__c);
            Invoice__c invoice = [select id, account__C from invoice__C where id = :initialtransfer.invoice__c limit 1];
            FSMLogger.LogInformationalMessage('got invoice');
            ID accountID = invoice.account__c;
            FSMLogger.LogInformationalMessage('Retrieved account - ' + accountID);
            
            //retrieve the active Balance and Sedera funding sources for the account associated with the initial transfer
            string balanceFundingSourceID = [select id from Dwolla_Funding_Source__c where Employer_Account__c = :accountID and type__c = 'Balance' AND Dwolla_Funding_Source_ID__c != null AND Active__c = TRUE].id;
            FSMLogger.LogInformationalMessage('Balance - ' + balanceFundingSourceID );
            string sederaFundingSourceID = [select id from Dwolla_Funding_Source__c where Employer_Account__c = :accountID and type__c = 'Sedera' AND Dwolla_Funding_Source_ID__c != null AND Active__c = TRUE].id;
                    FSMLogger.LogInformationalMessage('Sedera - ' + sederaFundingSourceID );
                    
            //create the new transfer for the same amount
            Dwolla_Transfer__c secondTransfer = new Dwolla_Transfer__c();
            secondTransfer.Source_Dwolla_Funding_Source__c = balanceFundingSourceID ;
            secondTransfer.Destination_Dwolla_Funding_Source__c = sederaFundingSourceID ;
            secondTransfer.invoice__c = initialTransfer.invoice__c;
            secondTransfer.amount__c = initialTransfer.amount__c;
            secondTransfer.Transfer_Status__c = 'Pending';
            insert secondTransfer;
            
            //invoke the DwollaTransferBatch for the newly created transfer
            List<ID> transferIDs = new list<id>();
            transferIDs.add(secondTransfer.ID);
            DwollaTransfersBatch dwollaTransferGenBatch = new DwollaTransfersBatch (transferIDs);
            Database.executeBatch(dwollaTransferGenBatch,30);
        }
        catch (Exception ex)
        {
            FSMLogger.LogErrorMessage(ex);
                      FSMException e = new FSMException ();
                      throw e;
        }
            
    }
    
    /**
    *@Purpose : This method will create the account transactions for the funding sources once the second Dwolla transfer (T2) is completed successfully
    */
    public static void createAccountTransactions(Dwolla_Transfer__c transfer){
        FSMLogger.LogInformationalMessage('in createAccountTransactions');
        //get the invoice associated with the transfer
        //retrieve the ENA and MCS amounts
        //create the account transactions and insert them

        Invoice__c invoice = [select ENA_Total__c, Standard_Total__c, account__c from Invoice__c where id = :transfer.invoice__c];
        Account_Balance__c accountBalance = [select ID, name from Account_Balance__c where Account__c = :invoice.account__c];
        FSMLogger.LogInformationalMessage('in createAccountTransactions, got accountbalance');
        if (invoice.ENA_Total__c > 0) {
            Account_Transaction__c enaTransaction = new Account_Transaction__c();
            enaTransaction.amount__c = invoice.ENA_Total__c;
            enaTransaction.Dwolla_Transfer__c = transfer.id;
            enaTransaction.bucket__c = 'ENA Fund';
            enaTransaction.type__c = 'Invoice';
            enaTransaction.Account_Balance__c = accountBalance.ID;
            
            insert enaTransaction;
            FSMLogger.LogInformationalMessage('in createAccountTransactions, inserted enatrans');
        }
        
        if (invoice.Standard_Total__c > 0) {
            Account_Transaction__c standardTransaction = new Account_Transaction__c();
            standardTransaction.amount__c = invoice.Standard_Total__c;
            standardTransaction.Dwolla_Transfer__c = transfer.id;
            standardTransaction.bucket__c = 'Standard Fund';
            standardTransaction.type__c = 'Invoice';
            standardTransaction.Account_Balance__c = accountBalance.ID;
            
            insert standardTransaction;
            FSMLogger.LogInformationalMessage('in createAccountTransactions, inserted standardtrans');
        }

        //PauboxEmailHelper.SendTransferCompletedEmail(transfer);
        
        
    }
    
}