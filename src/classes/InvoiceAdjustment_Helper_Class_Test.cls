@isTest(SeeAllData=true)
public class InvoiceAdjustment_Helper_Class_Test {
    @isTest(SeeAllData=true) static void testInvoiceAdustmentHelperClass() {
              //create employer account
        
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Employer').getRecordTypeId();
        
        Account employerAccount = new Account(Name = 'Sofsyst', RecordtypeId = recordtypeId, Phone = '9898989890',
                                              Default_Product__c = 'Sedera Access', Available_IUA_Options__c = '1000',
                                              MS_Pricing__c = 'New Pricing', Subscription_Status__c = 'Active',
                                              Invoice_Email_Address__c = 'test@test.com', Enrollment_date__c = Date.today());
        insert employerAccount;

        //create invoice
        
        Invoice__c inv = new Invoice__c();
        inv.Account__c = employerAccount.id;
        inv.Submission_Status__c = 'In Review';
        inv.X2nd_MD_Total__c = 15;
        inv.Gross_Monthly_Total_Existing__c = 100;
        inv.Gross_Monthly_Total_New__c = 196;
        inv.Liberty_Rx_Total__c = 11;
        inv.Member_Services_Revenue__c = 13;
        INSERT inv;

        //create invadj
        //one for each type

        Invoice_Adjustment__c mcsNewInvAdj = new Invoice_Adjustment__c();
        mcsNewInvAdj.Invoice__c = inv.id;
        mcsNewInvAdj.Employer_Account__c = employerAccount.id;
        mcsNewInvAdj.Amount__c =  -200;
        mcsNewInvAdj.mcs_bucket__c  = 'MCS_new';
        mcsNewInvAdj.Description_of_Change__c = 'MCS New Adjustment';
        mcsNewInvAdj.Date_Change_Requested__c = date.parse('11/19/2018');
        mcsNewInvAdj.Invoice_Period__c = date.parse('12/1/2018');
        mcsNewInvAdj.completed__c=true;
        mcsNewInvAdj.Applied__c = 0;
        INSERT mcsNewInvAdj;

        Invoice_Adjustment__c mcsExistingInvAdj = new Invoice_Adjustment__c();
        mcsExistingInvAdj.Invoice__c = inv.id;
        mcsExistingInvAdj.Employer_Account__c = employerAccount.id;
        mcsExistingInvAdj.Amount__c =  -101;
        mcsExistingInvAdj.mcs_bucket__c  ='MCS_existing';
        mcsExistingInvAdj.Description_of_Change__c = 'MCS Existing Adjustment';
        mcsExistingInvAdj.Date_Change_Requested__c = date.parse('11/19/2018');
        mcsExistingInvAdj.Invoice_Period__c = date.parse('12/1/2018');
        mcsExistingInvAdj.completed__c=true;
        mcsExistingInvAdj.Applied__c = 0;
        INSERT mcsExistingInvAdj;

        Invoice_Adjustment__c msInvAdj = new Invoice_Adjustment__c();
        msInvAdj.Invoice__c = inv.id;
        msInvAdj.Employer_Account__c = employerAccount.id;
        msInvAdj.Amount__c =   -130;
        msInvAdj.mcs_bucket__c  ='Member Services';
        msInvAdj.Description_of_Change__c = 'Member Services Adjustment';
        msInvAdj.Date_Change_Requested__c = date.parse('11/19/2018');
        msInvAdj.Invoice_Period__c = date.parse('12/1/2018');
        msInvAdj.completed__c=true;
        msInvAdj.Applied__c = 0;
        INSERT msInvAdj;

        /*
        Invoice_Adjustment__c x2ndMDInvAdj = new Invoice_Adjustment__c();
        x2ndMDInvAdj.Invoice__c = inv.id;
        x2ndMDInvAdj.Employer_Account__c = employerAccount.id;
        x2ndMDInvAdj.Amount__c =   -150;
        x2ndMDInvAdj.mcs_bucket__c  ='2nd.MD';
        x2ndMDInvAdj.Description_of_Change__c = '2nd MD Adjustment';
        x2ndMDInvAdj.Date_Change_Requested__c = date.parse('11/19/2018');
        x2ndMDInvAdj.Invoice_Period__c = date.parse('12/1/2018');
        x2ndMDInvAdj.completed__c=true;
        x2ndMDInvAdj.Applied__c = 0;
        INSERT x2ndMDInvAdj;
        */
        
        Invoice_Adjustment__c libertyRXInvAdj = new Invoice_Adjustment__c();
        libertyRXInvAdj.Invoice__c = inv.id;
        libertyRXInvAdj.Employer_Account__c = employerAccount.id;
        libertyRXInvAdj.Amount__c =   -110;
        libertyRXInvAdj.mcs_bucket__c  ='LibertyRx';
        libertyRXInvAdj.Description_of_Change__c = 'LibertyRx Adjustment';
        libertyRXInvAdj.Date_Change_Requested__c = date.parse('11/19/2018');
        libertyRXInvAdj.Invoice_Period__c = date.parse('12/1/2018');
        libertyRXInvAdj.completed__c=true;
        libertyRXInvAdj.Applied__c = 0;
        INSERT libertyRXInvAdj;

        //process the invoice adjustments
        List<InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput> invoiceAdjustments = new List<InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput>();

        InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput mcsNewInvAdjHelpInput = new InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput();
        mcsNewInvAdjHelpInput.InvoiceAdjustmentId = mcsNewInvAdj.id;
        mcsNewInvAdjHelpInput.action = 'insert';
        invoiceAdjustments.add(mcsNewInvAdjHelpInput);

        InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput mcsExistingInvAdjHelpInput = new InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput();
        mcsExistingInvAdjHelpInput.InvoiceAdjustmentId = mcsExistingInvAdj.id;
        mcsExistingInvAdjHelpInput.action = 'insert';
        invoiceAdjustments.add(mcsExistingInvAdjHelpInput);

        InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput msInvAdjHelpInput = new InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput();
        msInvAdjHelpInput.InvoiceAdjustmentId = msInvAdj.id;
        msInvAdjHelpInput.action = 'insert';
        invoiceAdjustments.add(msInvAdjHelpInput);

        /*
        InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput x2ndMDInvAdjHelpInput = new InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput();
        x2ndMDInvAdjHelpInput.InvoiceAdjustmentId = x2ndMDInvAdj.id;
        x2ndMDInvAdjHelpInput.action = 'insert';
        invoiceAdjustments.add(x2ndMDInvAdjHelpInput);
        */

        InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput libertyRXInvAdjHelpInput = new InvoiceAdjustment_Helper_Class.InvoiceAdjustmentHelperInput();
        libertyRXInvAdjHelpInput.InvoiceAdjustmentId = mcsExistingInvAdj.id;
        libertyRXInvAdjHelpInput.action = 'insert';
        invoiceAdjustments.add(libertyRXInvAdjHelpInput);
        
        Test.startTest();
        InvoiceAdjustment_Helper_Class.ProcessInvoiceAdjustment(invoiceAdjustments);
        
        //validate the invoice adjustments
        //invoice line items, inv adj for future invoice, and invoice amounts
        list<Invoice__c> invoicesInsertTest =[select id, name from invoice__c where X2nd_MD_Total__c = 0 and Gross_Monthly_Total_Existing__c = 0 
                            and Gross_Monthly_Total_New__c = 0 and Liberty_Rx_Total__c = 0 and Member_Services_Revenue__c = 0 and id = :inv.id];

        list<Invoice_Line_Item__c> invoicelineitemsInsertTest = [select id, name from Invoice_Line_Item__c where invoice__c = :inv.id];

        list<Invoice_Adjustment__c> mcsNewInvAdjInsertTest = [select id from Invoice_Adjustment__c where Prior_Invoice_Adjustment__c = :mcsNewInvAdj.id];
        list<Invoice_Adjustment__c> mcsExistingInvAdjInsertTest = [select id from Invoice_Adjustment__c where Prior_Invoice_Adjustment__c = :mcsExistingInvAdj.id];
        list<Invoice_Adjustment__c> msInvAdjInsertTest = [select id from Invoice_Adjustment__c where Prior_Invoice_Adjustment__c = :msInvAdj.id];
        list<Invoice_Adjustment__c> libertyRXInvAdjInsertTest = [select id from Invoice_Adjustment__c where Prior_Invoice_Adjustment__c = :libertyRXInvAdj.id];
//        list<Invoice_Adjustment__c> x2ndMDInvAdjInsertTest = [select id from Invoice_Adjustment__c where Prior_Invoice_Adjustment__c = :x2ndMDInvAdj.id];
        
        /*
        System.assertEquals(1, invoicesInsertTest.size());
        system.assertEquals(5, invoicelineitemsInsertTest.size());
        System.assertEquals(1, mcsNewInvAdjInsertTest.size());
        System.assertEquals(1, mcsExistingInvAdjInsertTest.size());
        System.assertEquals(1, msInvAdjInsertTest.size());
        System.assertEquals(1, libertyRXInvAdjInsertTest.size());
//        System.assertEquals(1, x2ndMDInvAdjInsertTest.size());
        */
        //delete (call delete for each inv adj)
        //assert that there are no Adjustment invlineitem

        InvoiceAdjustment_Helper_Class.DeleteButtonCall(libertyRXInvAdj.id);
        InvoiceAdjustment_Helper_Class.DeleteButtonCall(mcsNewInvAdj.id);
        InvoiceAdjustment_Helper_Class.DeleteButtonCall(mcsExistingInvAdj.id);
        InvoiceAdjustment_Helper_Class.DeleteButtonCall(msInvAdj.id);
//        InvoiceAdjustment_Helper_Class.DeleteButtonCall(x2ndMDInvAdj.id);
        
        list<Invoice__c> invoicesDeleteTest =[select id, name from invoice__c where X2nd_MD_Total__c = 15 and Gross_Monthly_Total_Existing__c = 100 
                            and Gross_Monthly_Total_New__c = 196 and Liberty_Rx_Total__c = 11 and Member_Services_Revenue__c = 13 and id = :inv.id];
        
        list<Invoice_Line_Item__c> invoicelineitemsDeleteTest = [select id, name from Invoice_Line_Item__c where invoice__c = :inv.id];

        list<Invoice_Adjustment__c> invoiceAdjustmentDeleteTest = [select id from Invoice_Adjustment__c where invoice__c = :mcsNewInvAdj.id];
        
        /*
        System.assertEquals(1, invoicesDeleteTest.size());
        System.assertEquals(0, invoicelineitemsDeleteTest.size());
        System.assertEquals(0, invoiceAdjustmentDeleteTest.size());
        */
        Test.stopTest();
    }
}