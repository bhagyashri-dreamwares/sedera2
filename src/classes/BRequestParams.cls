public with sharing virtual class BRequestParams {
    public Integer startIndex;
    public Integer pageNumber;
    public DateTime fromTime;
    public DateTime toTime;
    public String objectName;
    public List<String> qbIds = new List<String>();
}