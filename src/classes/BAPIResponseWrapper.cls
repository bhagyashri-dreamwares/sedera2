/**
 *@Purpose      :
 *@Author       : Dreamwares
 *@Created Date : 19/07/2018
 *
 */
 
public with sharing class BAPIResponseWrapper {
    public Boolean isSuccess;
    public String Message;
    public Object records;
    public DateTime responseTime;
    public Integer recordsCount;
    public Integer startIndex;
    
    public BAPIResponseWrapper(){
        
    }
    
    public BAPIResponseWrapper(Boolean isSuccess, String message){
        this.isSuccess = isSuccess;
        this.message = message;
        //this.records = new Object();
    }
}