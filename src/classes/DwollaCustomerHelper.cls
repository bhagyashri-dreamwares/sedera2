/**
*@Purpose : Helper class to get customer related entities
*@Date : 22/01/2019
*/
global class DwollaCustomerHelper{
    
   
   /**
    * To get customer related funding source from Dwolla
    */ 
    public static List<String> getCustomerFundingSource(String customerId){
        
        List<String> balanceFundingSourceIdList = new List<String>();
        DwollaAPIConfiguration__c config = DwollaAccessTokenHelper.getAPIConfiguration();
        
        DwollaAccessTokenHelper accessTokenHelper = new DwollaAccessTokenHelper(); 

        if(config.Access_Token__c != null){
            balanceFundingSourceIdList = getFundingSource(config, customerId); 
        }
        return balanceFundingSourceIdList;    
    }
    
    /**
    * To funding source from Dwolla
    */    
    private static List<String> getFundingSource(DwollaAPIConfiguration__c config, string customerId){
        
        List<String> balanceFundingSourceIdList = new List<String>();
        String endPoint = config.Is_Sandbox__c ? 
                          config.Sandbox_Endpoint_Url__c+'/customers/'+customerId+'/funding-sources' :
                          config.Production_Endpoint_Url__c+'/customers/'+customerId+'/funding-sources' ;
        Map<String,String> endPointParameterMap = new Map<String,String>{};
        Map<String,String> headerParameterMap = new Map<String,String>{'Accept'=>'application/vnd.dwolla.v1.hal+json', 
                                                                       'Authorization'=>'Bearer '+config.Access_Token__c};
        
        HttpResponse respose = CalloutHelper.httpGetRequest(endPoint,endPointParameterMap, headerParameterMap);
        System.debug(respose.getBody());
        if(respose.getStatusCode() == 200){
            
           System.debug(':::::::\n'+respose.getBody());
           Map<String,object> fundingSourcesDTO =  (Map<String,object>) JSON.deserializeUntyped(respose.getBody());
           
           if(fundingSourcesDTO.containsKey('_embedded')){
               System.debug('111111111111111111111111');
               Map<String,object> embeddedObjMap = (Map<String,object>)fundingSourcesDTO.get('_embedded');
               
               if(embeddedObjMap.containsKey('funding-sources')){
                   System.debug('222222222222222222');
                   List<object> fundingSourceObjList = (List<object>)embeddedObjMap.get('funding-sources');
                   for(object obj :fundingSourceObjList){
                       
                       Map<String, Object> fundingSourceInfoMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(obj));
                       Object type = fundingSourceInfoMap.get('type');
                       
                       if(type != null && Json.serialize(type).remove('"').endsWithIgnoreCase('balance')){
                          
                          if(fundingSourceInfoMap.containsKey('id')){
                              System.debug('33333333');
                              balanceFundingSourceIdList.add(Json.serialize(fundingSourceInfoMap.get('id'))); 
                          }                  
                       }
                   }
               }
           }
        }
        System.debug('balanceFundingSourceIdList ::::'+balanceFundingSourceIdList);
        return balanceFundingSourceIdList;
    }
    
    /**
    * To get customer status from Dwolla
    */ 
    public static String getCustomerStatus(String customerId){
        
        String status = '';
        DwollaAPIConfiguration__c config = DwollaAccessTokenHelper.getAPIConfiguration();
        
        DwollaAccessTokenHelper accessTokenHelper = new DwollaAccessTokenHelper(); 

        if(config.Access_Token__c != null){
            CustomerInfo customerInfo = getCustomerInfo(config, customerId); 
            status = customerInfo != null ? customerInfo.status : status;
        }
        return status;  
    }
    
    /**
    * Update Customer Info if status is Retry
    */
    webService static void updateRetryCustomer(String customerId, String accountId){
        
        String status = getCustomerStatus(customerId);
        System.debug('status ::::'+status);
        if(status.toLowerCase().equalsIgnoreCase('retry')){
            
            updateCustomer(customerId, accountId);
        }
    }
    
    
     /**
    * To get Customer details from Dwolla
    */    
    private static CustomerInfo getCustomerInfo(DwollaAPIConfiguration__c config, string customerId){
        
        CustomerInfo customerInfo = new CustomerInfo();
        String endPoint = config.Is_Sandbox__c ? 
                          config.Sandbox_Endpoint_Url__c+'/customers/'+customerId :
                          config.Production_Endpoint_Url__c+'/customers/'+customerId;
        Map<String,String> endPointParameterMap = new Map<String,String>{};
        Map<String,String> headerParameterMap = new Map<String,String>{'Accept'=>'application/vnd.dwolla.v1.hal+json', 
                                                                       'Authorization'=>'Bearer '+config.Access_Token__c};
        
        HttpResponse respose = CalloutHelper.httpGetRequest(endPoint,endPointParameterMap, headerParameterMap);
        System.debug(respose);
        if(Test.isRunningTest()){
            respose = new HttpResponse();
            respose.setStatusCode(200);
            respose.setBody('{"id": "dc82ca5d-8c6f-466d-ba16-8fbbf389f128", "firstName": "sdfsdf", "lastName": "wqeq", "email": "bhagyashri+1004@dreamwares.com", "type": "business", "status": "verified", "created": "2019-01-18T14:09:49.570Z", "address1": "fsdf", "city": "dfs", "state": "KY", "postalCode": "34324"}');
        }
        if(respose != null && respose.getStatusCode() == 200){           
            CustomerInfo = (CustomerInfo)JSON.deserialize(respose.getBody(),CustomerInfo.class);
        }
        return CustomerInfo;
    }
    /**
    * Update Dwolla Customer information when customer status is "Retry"
    */
    webService static void updateCustomer(String customerId, String accountId){
        
        DwollaAPIConfiguration__c config = DwollaAccessTokenHelper.getAPIConfiguration();
        if(String.isNotBlank(customerId) && config != null){
            
             if(String.isNotBlank(config.Access_Token__c)){
                 
                 String endPoint = config.Is_Sandbox__c ? 
                          config.Sandbox_Endpoint_Url__c+'/customers/'+customerId :
                          config.Production_Endpoint_Url__c+'/customers/'+customerId;
                 Map<String,String> endPointParameterMap = new Map<String,String>{};
                 Map<String,String> headerParameterMap = new Map<String,String>{'Content-Type'=>'application/vnd.dwolla.v1.hal+json', 
                                                                                'Accept'=>'application/vnd.dwolla.v1.hal+json',
                                                                               'Authorization'=>'Bearer '+config.Access_Token__c};    
                 
                 Dwolla_Datasheet__c dataSheetRecord = getDataSheetRecord(accountId);
                 JSONGenerator gen = getJsonForUpdateCustomer(dataSheetRecord);
                 System.debug('Json :::\n'+gen.getAsString());
                 System.debug('headerParameterMap :::\n'+headerParameterMap);
                 HttpResponse response = CalloutHelper.httpPostRequest(endPoint,endPointParameterMap, headerParameterMap, gen); 
                 
                 if(response != null && response.getStatusCode() != 200){
                 
                     dataSheetRecord.IsDwollaCustomerUpdated__c = false;
                     dataSheetRecord.Customer_Update_Time__c = null;
                     
                     DwollaErrorResponseHandler.DwollaErrorWrapper errorWrapper = new DwollaErrorResponseHandler.DwollaErrorWrapper();
                     errorWrapper.errorMsg = '\n Error:'+response.getBody();
                     errorWrapper.requestBody = 'EndPoint :\n'+config.Authorization_URL__c+'\n \n endPointParameterMap :\n'+endPointParameterMap+
                        '\n\n headerParameterMap :\n'+headerParameterMap+'\n\nJSON:\n'+gen.getAsString();
                     errorWrapper.source = 'DwollaCustomerHelper.updateCustomer';
                     DwollaErrorResponseHandler.saveDwollaErrorResponse(new List<Dwolla_Error_Response__c>{DwollaErrorResponseHandler.createDwollaErrorResponse(errorWrapper)});    
                 }else{
                     dataSheetRecord.IsDwollaCustomerUpdated__c = true;
                     dataSheetRecord.Customer_Update_Time__c = DateTime.now();  
                 } 
                 updateDataSheetRecord(dataSheetRecord);       
             }else{
                 LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCustomerHelper.updateCustomer', null, 'config.Access_Token__c:'+config.Access_Token__c, 
                                                                 'Please provide proper access token in custom setting', 'Error'))});
             }
        }else{
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCustomerHelper.updateCustomer', null, 'accountId: '+accountId+'\n customerId :'+customerId, 
                                                                 'Please provide proper Customer Id', 'Error'))});
        }    
    }
    
    /**
    * Create JSON for customer update
    */
    private static JSONGenerator getJsonForUpdateCustomer(Dwolla_Datasheet__c datasheetRecord){
        
        Datetime DOB;
        String DOBString = '';
        if(datasheetRecord.Controller_Date_of_Birth__c != null) {
            DOB = datasheetRecord.Controller_Date_of_Birth__c;
            DOBString = DOB.formatGmt('yyyy-MM-dd');
        }
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('firstName', datasheetRecord.First_Name__c != Null ? datasheetRecord.First_Name__c : '');
        gen.writeStringField('lastName', datasheetRecord.Last_Name__c != Null ? datasheetRecord.Last_Name__c : '');
        gen.writeStringField('email', datasheetRecord.Email_Address__c != Null ? datasheetRecord.Email_Address__c : '');
        gen.writeStringField('type', 'business');
        gen.writeStringField('address1', datasheetRecord.Address_1__c != Null ? datasheetRecord.Address_1__c : '');
        gen.writeStringField('address2', datasheetRecord.Address_2__c != Null ? datasheetRecord.Address_2__c : '');
        gen.writeStringField('city', datasheetRecord.City__c != Null ? datasheetRecord.City__c : '');
        gen.writeStringField('state', datasheetRecord.State__c != Null ? datasheetRecord.State__c : ''); 
        gen.writeStringField('postalCode', datasheetRecord.Postal_Code__c != Null ? datasheetRecord.Postal_Code__c : '');
        
        gen.writeFieldName('controller');
        gen.writeStartObject();
        gen.writeStringField('firstName', datasheetRecord.Controller_First_Name__c != Null ? datasheetRecord.Controller_First_Name__c : '');
        gen.writeStringField('lastName', datasheetRecord.Controller_Last_Name__c != Null ? datasheetRecord.Controller_Last_Name__c : '');
        gen.writeStringField('title', datasheetRecord.Controller_Title__c != Null ? datasheetRecord.Controller_Title__c : '');
        gen.writeStringField('dateOfBirth', String.isNotBlank(DOBString) ? DOBString : ''); 
        gen.writeStringField('ssn', datasheetRecord.Controller_SSN__c != Null ? datasheetRecord.Controller_SSN__c : '');
        
        gen.writeFieldName('address');
        gen.writeStartObject();
        gen.writeStringField('address1', datasheetRecord.Controller_Address_1__c != Null ? datasheetRecord.Controller_Address_1__c : '');
        gen.writeStringField('address2', datasheetRecord.Controller_Address_2__c != Null ? datasheetRecord.Controller_Address_2__c : '');
        gen.writeStringField('address3', datasheetRecord.Controller_Address_3__c != Null ? datasheetRecord.Controller_Address_3__c : '');
        gen.writeStringField('city', datasheetRecord.Controller_City__c != Null ? datasheetRecord.Controller_City__c : ''); 
        gen.writeStringField('stateProvinceRegion', datasheetRecord.Controller_State__c != Null ? datasheetRecord.Controller_State__c : '');
        gen.writeStringField('postalCode', datasheetRecord.Controller_Postal_Code__c != Null ? datasheetRecord.Controller_Postal_Code__c : ''); 
        gen.writeStringField('country', 'US');
        gen.writeEndObject();
        gen.writeEndObject();
        
        if(datasheetRecord.business_classification__c != NULL)
            gen.writeStringField('businessClassification', DwollaBusinessClassTranslationHelper.translate(datasheetRecord.business_classification__c));
        
        gen.writeStringField('businessType', datasheetRecord.Business_Type__c != Null ? datasheetRecord.Business_Type__c : '');
        gen.writeStringField('businessName', datasheetRecord.DBA_Name__c != Null ? datasheetRecord.DBA_Name__c : ''); 
        gen.writeStringField('ein', datasheetRecord.EIN__c != Null ? datasheetRecord.EIN__c : '');

        gen.writeEndObject();
        return gen;   
    }
    
    /**
    * Get DataSheet record
    */
    private static Dwolla_Datasheet__c getDataSheetRecord(String accountId){
        
        System.debug('accountId :::::'+accountId);
        List<Dwolla_Datasheet__c> dataSheetRecordList = new List<Dwolla_Datasheet__c>();
        try{
            
            dataSheetRecordList = [SELECT Id, First_Name__c, Last_Name__c, Email_Address__c, Address_1__c,Address_2__c, City__c, State__c, 
                                          Postal_Code__c, Controller_First_Name__c, Controller_Last_Name__c, Controller_Title__c, Date_of_Birth__c,
                                          SSN__c, Controller_Address_1__c, Controller_Address_2__c,Controller_Address_3__c, Controller_City__c, Controller_State__c, Controller_Postal_Code__c,
                                          Controller_Date_of_Birth__c, Business_Classification__c,Controller_SSN__c, Business_Type__c, EIN__c,DBA_Name__c
                                   FROM Dwolla_Datasheet__c
                                   WHERE Account__c = :accountId];    
            return !dataSheetRecordList.isEmpty() ? dataSheetRecordList[0] : new Dwolla_Datasheet__c();
        }catch(Exception exp){
             LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCustomerHelper.getDataSheetRecord', exp, 'accountId: '+accountId, 
                                                                 '', 'Error'))});
        }
        return new Dwolla_Datasheet__c();
    }
    
    
    /**
    * To create Dwolla customer account from the Dwolla datasheet record page
    */ 
    webService static void CreateDwollaCustomer(string accountID)
    {

        dwolla_datasheet__c datasheet = [select id,
            Account__c,
            Account__r.name,
            Address_1__c,
            Address_2__c,
            Business_Classification__c,
            Business_Type__c,
            Certified__c,
            Certified_Datetime__c,
            City__c,
            Controller_Address_1__c,
            Controller_Address_2__c,
            Controller_Address_3__c,
            Controller_City__c,
            Controller_Date_of_Birth__c,
            Controller_First_Name__c,
            Controller_Last_Name__c,
            Controller_Postal_Code__c,
            Controller_SSN__c,
            Controller_State__c,
            Controller_Title__c,
            Date_of_Birth__c,
            DBA_Name__c,
            EIN__c,
            Email_Address__c,
            First_Name__c,
            Last_Name__c,
            Postal_Code__c,
            SSN__c,
            State__c 
            from Dwolla_Datasheet__c 
            where account__c = :accountid limit 1];

        DwollaDatasheetController.CalloutResponse calloutResponseRecord = DwollaDatasheetController.makeCallouts(datasheet);
        system.debug(calloutresponserecord);

        DwollaDatasheetController.Response responseRec = DwollaDatasheetController.updateAccountRecord(datasheet.account__c,
        calloutResponseRecord.customerIdString,
        calloutResponseRecord.benificialOwnerId,
        datasheet.Account__r.name,
        datasheet.Id);
    }
    
    private static void updateDataSheetRecord(Dwolla_Datasheet__c dataSheetRecord){
        
        try{
            update dataSheetRecord;    
        }catch(Exception exp){
            LogUtil.saveLogs(new List<Log__c>{(LogUtil.createLog('DwollaCustomerHelper.updateDataSheetRecord', exp, 
                                                                 'dataSheetRecord: '+dataSheetRecord, 
                                                                 '', 'Error'))});
        }
    }
    
    public class CustomerInfo{
        String id;
        String firstName;
        String lastName;
        String status;
        String type;
    }
}