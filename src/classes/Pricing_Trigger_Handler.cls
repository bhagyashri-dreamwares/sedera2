/*
Test class: Pricing_Trigger_Test
*/

public class Pricing_Trigger_Handler {


    public static void handleAfterInsert(Map<id,Pricing__c> priceNewMap){
    
    /*** Error if second pricing is getting linked to an Employer ***/
     Pricing_Helper_Class.errorOnTwoPricings(priceNewMap.values());
 
   }

     public static void handleAfterUpdate(Map<id,Pricing__c> priceNewMap,Map<id,Pricing__c> priceOldMap){
       
    /*** If any of the concerned field's(controls pricing at member level) value changes, then all the assosciated members are Updated for new Product code and amount ***/   
       Pricing_Helper_Class.changeInPricingFieldsValue(priceNewMap.values(),priceOldMap);
        
    }

 }