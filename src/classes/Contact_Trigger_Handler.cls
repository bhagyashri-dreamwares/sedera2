/*
Testclass: Account_Trigger_test
*/
public class Contact_Trigger_Handler {

    public static void handleBeforeInsert(List <contact> conList) {
        
        /*** Assigning recordType based on Account's recordType ***/
        Contact_Helper_Class.defaultRecordType(conList);
        
        /*** Default values for converted contacts ***/
        Contact_Helper_Class.convertedConDefaultValues(conList);
        
        /*** Format Contact Number ***/
        Contact_Helper_Class.formatContactNumber(conList, null);

        /***  S2S member Account linking  ***/
        Contact_Helper_Class.S2SmemberAccLinking(conList);


    }
    
    
    public static void handleAfterInsert( Map <Id, contact> conNewMap) {

        /*** S2S connection with Knew Health - Updating Primary Contact's Id and recordMemberId(autonumber) onMem Acc ***/
        Contact_Helper_Class.S2SPrimaryContactIdOnmemAcc(conNewMap);
        
        /***  proProfUserCreation if parent Affiliate is approved ***/
        Contact_Helper_Class.proProfUserCreation(conNewMap.values());

    }


    public static void handleBeforeUpdate( Map <Id, contact> conNewMap,Map <Id, contact> conOldMap) {

        /*** Format Contact Number ***/
        Contact_Helper_Class.formatContactNumber(conNewMap.values(), conOldMap);
        
         /*** Send Agreements Once Affiliate training is completed ***/
        //Contact_Helper_Class.sendAdobeAgreement(conNewMap.values(), conOldMap);

    }


}