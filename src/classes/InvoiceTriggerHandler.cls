/*  
    @Purpose      : Handler class of InvoiceTrigger
                    - Invoked from trigger
    @Author       : Dreamwares
    @Created Date : 20/07/2018
*/
public class InvoiceTriggerHandler {
    /* 
    @Purpose : Method to handle insert Invoice__c
                   - Invoked from Invoice__c trigger
    @Parameter : List<Invoice__C> (Trigger.new)
    @returns   : -
    */  
    public void insertMethod(List<Invoice__c> newInvoiceRecordList) {
    Set<Id> newInvoiceIdSet = new Set<Id>();
       
        //Check Submission_Status__c field
        for (Invoice__c invoiceRecord : newInvoiceRecordList) {
            //If Submission_Status__c field is 'Approved' then add invoice id into the set newInvoiceIdSet
            if (invoiceRecord.Submission_Status__c == 'Approved' && 
                invoiceRecord.Amount__c != null &&  
                invoiceRecord.Amount__c != 0) {
                
                newInvoiceIdSet.add(invoiceRecord.Id);      
            }
        }
        
        //If newInvoiceIdSet in not empty then call handler method for further processing
        if (!newInvoiceIdSet.isEmpty()) {
            System.debug('newInvoiceIdSet is : '+newInvoiceIdSet);
            QBInvoicePushBatch qbInvoiceBatch = new QBInvoicePushBatch(newInvoiceIdSet);
            Database.executeBatch(qbInvoiceBatch,30);
            
        }
    }
    
    /* 
        @Purpose : Method to handle update Invoice__c
                   - Invoked from Invoice__c trigger
    @Parameter : Trigger.newMap & Trigger.oldMap
    @returns   : -
    */  
    public void updateMethod(Map<Id, Invoice__c> newInvoiceRecordMap, Map<Id, Invoice__c> oldInvoiceRecordMap){
      Set<Id> invoiceIdSet = new Set<Id>();
                
        for (Invoice__c invoiceRecord : newInvoiceRecordMap.values()) {
        
           
            if(invoiceRecord.Submission_Status__c == 'Approved' && 
              (invoiceRecord.QB_Sync_Token__c == 
               oldInvoiceRecordMap.get(invoiceRecord.Id).QB_Sync_Token__c || (String.isBlank(invoiceRecord.Quickbooks_Invoice_ID__c) && String.isBlank(invoiceRecord.QB_Sync_Token__c)))) {
                invoiceIdSet.add(invoiceRecord.Id);          
            }
        }
        
        //If invoiceIdSet in not empty then call handler method for further processing
        if (!invoiceIdSet.isEmpty() && !System.isBatch()) {
               
            QBInvoicePushBatch qbInvoiceBatch = new QBInvoicePushBatch(invoiceIdSet);
            Database.executeBatch(qbInvoiceBatch,30);
        }      
    }
    
}